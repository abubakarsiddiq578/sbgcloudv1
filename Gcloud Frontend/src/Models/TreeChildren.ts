﻿export class TreeChildren {

    public id: number;
    public name: string;
    public value: string;
    public NodeId: number;
    public IsActive: boolean = true;

}