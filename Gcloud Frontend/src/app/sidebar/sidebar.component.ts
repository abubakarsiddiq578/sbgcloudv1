import { Component, OnInit, ViewChild } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { AppSessionService } from 'app/shared/session/app-session.service';
import { UserMediaContentServiceProxy, UserMediaContentDto } from '@app/shared/service-proxies/service-proxies';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';

import { AddUserComponent } from "../security/users/add-user/add-user.component";

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [{
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    },////////////////////
    /*{
        path: '/components',
        title: 'Components',
        type: 'sub',
        icontype: 'apps',
        collapse: 'components',
        children: [
            {path: 'buttons', title: 'Buttons', ab:'B'},
            {path: 'grid', title: 'Grid System', ab:'GS'},
            {path: 'panels', title: 'Panels', ab:'P'},
            {path: 'sweet-alert', title: 'Sweet Alert', ab:'SA'},
            {path: 'notifications', title: 'Notifications', ab:'N'},
            {path: 'icons', title: 'Icons', ab:'I'},
            {path: 'typography', title: 'Typography', ab:'T'}
        ]
    },
    {
        path: '/forms',
        title: 'Forms',
        type: 'sub',
        icontype: 'content_paste',
        collapse: 'forms',
        children: [
            {path: 'regular', title: 'Regular Forms', ab:'RF'},
            {path: 'extended', title: 'Extended Forms', ab:'EF'},
            {path: 'validation', title: 'Validation Forms', ab:'VF'},
            {path: 'wizard', title: 'Wizard', ab:'W'}
        ]
    },*/

    
    
    
    // {
    //     path: '/tables',
    //     title: 'Tables',
    //     type: 'sub',
    //     icontype: 'grid_on',
    //     collapse: 'tables',
    //     children: [
    //         {path: 'regular', title: 'Regular Tables', ab:'RT'},
    //         {path: 'extended', title: 'Extended Tables', ab:'ET'},
    //         {path: 'datatables.net', title: 'Datatables.net', ab:'DT'}
    //     ]
    // },{
    //     path: '/maps',
    //     title: 'Maps',
    //     type: 'sub',
    //     icontype: 'place',
    //     collapse: 'maps',
    //     children: [
    //         {path: 'google', title: 'Google Maps', ab:'GM'},
    //         {path: 'fullscreen', title: 'Full Screen Map', ab:'FSM'},
    //         {path: 'vector', title: 'Vector Map', ab:'VM'}
    //     ]
    // },{
    //     path: '/widgets',
    //     title: 'Widgets',
    //     type: 'link',
    //     icontype: 'widgets'

    // },{
    //     path: '/charts',
    //     title: 'Charts',
    //     type: 'link',
    //     icontype: 'timeline'

    // },{
    //     path: '/calendar',
    //     title: 'Calendar',
    //     type: 'link',
    //     icontype: 'date_range'
    // },{
    //     path: '/pages',
    //     title: 'Pages',
    //     type: 'sub',
    //     icontype: 'image',
    //     collapse: 'pages',
    //     children: [
    //         {path: 'pricing', title: 'Pricing', ab:'P'},
    //         {path: 'timeline', title: 'Timeline Page', ab:'TP'},
    //         {path: 'login', title: 'Login Page', ab:'LP'},
    //         {path: 'register', title: 'Register Page', ab:'RP'},
    //         {path: 'lock', title: 'Lock Screen Page', ab:'LSP'},
    //         {path: 'user', title: 'User Page', ab:'UP'}
    //     ]
    // }
    //,{
    //     path: '/forms',
    //     title: 'Forms',
    //     type: 'sub',
    //     icontype: 'content_paste',
    //     collapse: 'forms',
    //     // children: [
    //     //     {path: 'regular', title: 'Regular Forms', ab:'RF'},
    //     //     {path: 'extended', title: 'Extended Forms', ab:'EF'},
    //     //     {path: 'validation', title: 'Validation Forms', ab:'VF'},
    //     //     {path: 'wizard', title: 'Wizard', ab:'W'}
    //     // ]
    // },{
    //     path: '/tables',
    //     title: 'Tables',
    //     type: 'sub',
    //     icontype: 'grid_on',
    //     collapse: 'tables',
    //     // children: [
    //     //     {path: 'regular', title: 'Regular Tables', ab:'RT'},
    //     //     {path: 'extended', title: 'Extended Tables', ab:'ET'},
    //     //     {path: 'datatables.net', title: 'Datatables.net', ab:'DT'}
    //     // ]
    // },{
    //     path: '/maps',
    //     title: 'Maps',
    //     type: 'sub',
    //     icontype: 'place',
    //     collapse: 'maps',
    //     // children: [
    //     //     {path: 'google', title: 'Google Maps', ab:'GM'},
    //     //     {path: 'fullscreen', title: 'Full Screen Map', ab:'FSM'},
    //     //     {path: 'vector', title: 'Vector Map', ab:'VM'}
    //     // ]
    // },
    // {
    //     path: '/widgets',
    //     title: 'Widgets',
    //     type: 'link',
    //     icontype: 'widgets'

    // },{
    //     path: '/charts',
    //     title: 'Charts',
    //     type: 'link',
    //     icontype: 'timeline'

    // },{
    //     path: '/calendar',
    //     title: 'Calendar',
    //     type: 'link',
    //     icontype: 'date_range'
    // },{
    //     path: '/pages',
    //     title: 'Pages',
    //     type: 'sub',
    //     icontype: 'image',
    //     collapse: 'pages',
    //     children: [
    //         {path: 'pricing', title: 'Pricing', ab:'P'},
    //         {path: 'timeline', title: 'Timeline Page', ab:'TP'},
    //         {path: 'login', title: 'Login Page', ab:'LP'},
    //         {path: 'register', title: 'Register Page', ab:'RP'},
    //         {path: 'lock', title: 'Lock Screen Page', ab:'LSP'},
    //         {path: 'user', title: 'User Page', ab:'UP'}
    //     ]
    //  }
    //////////////////////////

    {
        path: '/hr',
        title: 'Production',
        type: 'sub', //sub
        icontype: 'apps',
        collapse: 'production',
        children: [
            {path: 'Category', title:'Category', ab:''},
            {path:'SubCategory', title:'Sub Category', ab:''},
            {path:'Item', title:'Item', ab:''},
            {path:'ItemType', title:'Item Type', ab:''},
            {path:'ItemWiseProduction', title:'Item Wise Production', ab:''},
            {path:'WorkGroup', title:'Work Group', ab:''}

            // {path: 'group', title: 'Group', ab:'G'},
            // {path: 'item-wise-pro', title: 'Item Wise Production', ab:'IW'},
            // {path: 'production-rate', title: 'Production Rate', ab:'PR'}
            // {path: '', title: 'Finish Goods Production', ab:'PS'},
            // {path: '', title: 'Store Issuance', ab:'SI'},
            // {path: '', title: 'Consumption', ab:'C'},
            // {path: '', title: 'Return Store Issuance', ab:'SI'},
            // {path: '', title: 'Department Wise Production', ab:'DW'},
            // {path: '', title: 'Close Batch', ab:'CB'},
            // {path: '', title: 'Production Control', ab:'PC'},
            // {path: '', title: 'Material Decomposition', ab:'MD'},
            // {path: '', title: 'Production Order', ab:'PO'},
            // {path: '', title: 'Production Entry', ab:'PE'},
            // {path: '', title: 'Stock Dispatch', ab:'SD'},
            // {path: '', title: 'Stock Receiving', ab:'SR'},
            // {path: '', title: 'Define Cost Sheet', ab:'CS'},
            // {path: '', title: 'Warranty Claim', ab:'WC'}
           
        ]
    },
     
    {
        path: '/hr',
        title: 'HRM',
        type: 'sub',
        icontype: 'content_paste',
        collapse: 'hr',
        children: [
            {path: 'employeeWizard', title:'Employee Enrollment', ab:''},
            {path: 'employee-list', title:'Employee List', ab:''},
            {path: 'department', title: 'Department', ab:' '},
            // {path: '', title:'________________________________________', ab:''},
            {path: 'JobType', title:'Job Type', ab:''},
            {path: 'LeaveType', title: 'Leave Type', ab:''},
            {path: 'leavequota', title: 'Leave Quota', ab:''},
            {path:'AdditionalLeave', title:'Additional Leave', ab:''},
            {path: 'leaverequest', title: 'Leave Request', ab: ''},
            {path: 'promotion', title: 'Employee Promotion', ab:''},
            {path: 'transfer', title: 'Transfer', ab:''},
            {path: 'Designation', title: 'Designation', ab:''},
            {path: 'Attendance', title: 'Attendance', ab:''},
            {path: 'AttendanceSetup', title: 'Attendance Setup', ab:''},
            {path:'holiday', title:'Holiday', ab:'',}, 
            {path:'Achievement', title:'Achievement', ab:''},
            {path: 'warning', title: 'Warning', ab: ''},
            {path: 'termination', title:'Termination', ab: ''},
            {path:'Resignation', title:'Resignation', ab:''},
            
        ]
    },

    {
        path: '/hr',
        title: 'Payroll',
        type: 'sub',
        icontype: 'apps',
        collapse: 'payroll',
         children: [

            {path: 'LoanRequest', title:'Loan Request', ab:''},
            {path:'AdvanceRequest', title:'Advance Request', ab:''},
            {path: 'SalaryGroup', title: 'Salary Group', ab:''},
            {path: 'SalaryType', title:'Salary Type', ab:''},
            {path: 'Salary', title:'Salary', ab:''},
            {path: 'tax-slab', title:'Tax Slab', ab:''},
            {path: 'AllowanceType', title:'Allowance Type', ab:''},           
            {path: 'allowance', title:'Allowance', ab:''},
            {path:'BonusType', title:'Bonus Type', ab:''},
            {path:'bonus', title:'Bonus', ab:''},
            {path:'FineType', title:'Fine Type', ab:''},
            {path:'fine', title:'Fine', ab:''},
            {path: 'DeductionType', title:'Deduction Type', ab:''},
            {path: 'deduction', title:'Deduction', ab:''},
            {path: 'SalaryWizard', title:'Salary Wizard', ab:''},
            {path: 'salaryList', title:'Salary List', ab:''},
            {path: 'Gratuity', title:'Graduity', ab:''},
            
        //     {path: '', title: 'Auto Salary Generate', ab:'SG'},
        //     {path: '', title: 'Salary Slip', ab:'SS'},
        //     {path: '', title: 'Over Time', ab:'OT'},
        //     {path: '', title: 'New Salary', ab:'NS'},
        //     {path: '', title: 'Approve Advances', ab:'AA'},
        //     {path: '', title: 'Advance Deduction', ab:'AD'},
        //     {path: '', title: 'Auto Overtime', ab:'AO'},
        //     {path: '', title: 'Define Shift', ab:'DS'},
        //     {path: '', title: 'Shift Group', ab:'SG'},
        //     {path: '', title: 'Late Time Slot', ab:'LT'},
        //     {path: '', title: 'Attendance Setup', ab:'AS'},
        //     {path: '', title: 'Tax Slabs', ab:'TS'},
        //     {path: '', title: 'Daily Salary', ab:'DS'},
        //     {path: '', title: 'Employee Enrollment', ab:'EE'},
        //     {path: '', title: 'Salary Related Configuration', ab:'RC'},
         ]
    },
    {
        path: '/Security',
        title: 'Administration',
        type: 'sub',
        icontype: 'apps',
        collapse: 'administration',
        children: [
             {path: 'UserRights', title: 'User Rights', ab:''}

        ]
    },
    // {
    //     path: '/hr',
    //     title: 'Configurations',
    //     type: 'sub',
    //     icontype: 'apps',
    //     collapse: 'configurations',
    //     children: [
    //         // {path: '', title: 'Main', ab:'M'},
    //         // {path: '', title: 'Human Resource', ab:'HR'},
    //         // {path: '', title: 'Payroll', ab:'PR'},
    //         // {path: '', title: 'Item/Production', ab:'IP'},
    //         // {path: '', title: 'Salary', ab:'S'}
    //         {path:'State', title:'State', ab:''},
    //         {path:'Province', title:'Province', ab:''},
    //         {path:'Region', title:'Region', ab:''},
    //         {path:'Zone', title:'Zone', ab:''},
    //         {path:'Belt', title:'Belt', ab:''},
    //         {path:'City', title:'City', ab:''},
    //         {path:'Territory', title:'Territory', ab:''},
    //         {path:'configuration'+'/'+'hr-configuration'+'/'+'hr-configuration', title:'Hr', ab:''}



    //     ]
    // },
    ,
   {
       path: '/config',
       title: 'Configurations',
       type: 'sub',
       icontype: 'apps',
       collapse: 'configurations',
       children: [
           {path: 'Overtime', title: 'Overtime Configuration', ab:''},
           {path: 'Hr', title: 'Hr Configuration', ab:''},
           {path: 'Country', title: 'Country Configuration', ab:''},
           {path: 'City', title: 'City Configuration', ab:''},
           {path: 'Belt', title: 'Belt Configuration', ab:''},
           {path: 'Zone', title: 'Zone Configuration', ab:''},
           {path: 'State', title: 'State Configuration', ab:''},
           {path: 'Province', title: 'Province Configuration', ab:''},
           {path: 'Region', title: 'Region Configuration', ab:''},
           {path: 'Territory', title: 'Territory Configuration', ab:''}
       ]
   }
    
];


@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
    providers: [AppAuthService , UserMediaContentServiceProxy]
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];

    loginUserId: number = 0;
    loginUserName: string = "";
    loginUserImage: string = "";

    @ViewChild('createUserModal') createUserModal: AddUserComponent;

    userMediaContent: UserMediaContentDto[] = [];

    constructor(private _authService: AppAuthService , private _sessionService: AppSessionService,
        private userMediaContentServiceProxy: UserMediaContentServiceProxy,  private router: Router){
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);

        this.loginUserName = this._sessionService.user.name
        this.loginUserId = this._sessionService.userId;

        this.getUserMediaContent();
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            let ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }

    logout(): void {
        this._authService.logout();
    }

    // get user media contents
    getUserMediaContent(): void {
        this.userMediaContentServiceProxy.getAllMediaContent()
            .subscribe((result) => {
                this.userMediaContent = result;

                this.getUserPicture(this.loginUserId);
            });
    }

    getUserPicture(userId: number) {
        this.loginUserImage = this.userMediaContent.find(function (obj) { return obj.userId === userId; }).imageUrl;
    }

    // createUser(){
    //     alert("hello");
    //     this.router.navigate(['../security/users/add-user/add-user.component']);
    // }

     // For create new user 
     createUser(): void {
        this.createUserModal.show();
    }
}
