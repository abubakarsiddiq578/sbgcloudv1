﻿import { Injectable, Inject } from '@angular/core';
import { AppConsts } from 'app/shared/AppConsts';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Injectable()
export class AppAuthService {

    constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService) {
        
    }

    logout(reload?: boolean): void {
        abp.auth.clearToken();

        this.storage.remove('TenantId');

        if (reload !== false) {
            location.href = AppConsts.appBaseUrl;
        }
    }
}