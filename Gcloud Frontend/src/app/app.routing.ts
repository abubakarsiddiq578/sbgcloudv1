import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { CompanyInfoComponent } from './company-info/company-info.component';
import { AppRouteGuard } from '@app/shared/auth/auth-route-guard';

export const AppRoutes: Routes = [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    }, {
      path: '',
      component: AdminLayoutComponent,
      canActivate: [AppRouteGuard],
      children: [
          {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    }, {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
    }, {
        path: 'forms',
        loadChildren: './forms/forms.module#Forms'
    }, {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
    }, {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule'
    }, {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
    }, {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule'
    }, {
        path: 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    }, {
        path: '',
        loadChildren: './userpage/user.module#UserModule'
    }, {
        path: '',
        loadChildren: './timeline/timeline.module#TimelineModule'
    },
    {
        path: 'hr',
        loadChildren: './hrm/hr/hr.module#Hr'
    },
    
    {
        path: 'config',
        loadChildren: './configurations/configurations.module#ConfigurationsModule'
    },
    {
        path: 'company-info',
        loadChildren: './company-info/companyInfo.module#CompanyInfo'
        
    },{
        path: 'costcenter',
        loadChildren: './costcenter/costCenter.module#CostCenter'
    },{
        path: 'chartOfAccount',
        loadChildren: './chartOfAccount/chartOfAccount.module#ChartOfAccount'
    },{
        path:'locationinfo',
        loadChildren: './locationinfo/locationinfo.module#LocationInfo'
    },
    {
        path:'chartOfAccount',
        loadChildren: './chartOfAccount/chartOfAccount.module#ChartOfAccount'
    },{
        path: 'Security',
        loadChildren: './security/security.module#Security'
    },{
        path: 'Tenant',
        loadChildren: './tenants/tenant.module#Tenant'
    }
  ]}, {
      path: '',
      component: AuthLayoutComponent,
      children: [{
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
      },{
        path: 'account',
        loadChildren: './account/account.module#AccountModule'
      }
    ]
    }
];
