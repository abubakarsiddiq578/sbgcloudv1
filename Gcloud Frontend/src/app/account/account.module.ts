import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { FlexLayoutModule } from '@angular/flex-layout';

import { AccountRoutes } from './account.routing';

import { ModalModule } from 'ngx-bootstrap';

import { SocialLoginModule, AuthServiceConfig, LoginOpt } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from "angularx-social-login";

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import { SocialComponent } from "./login/social/social.component";
import { TenantChangeComponent } from './tenant/tenant-change.component';
import { TenantChangeModalComponent } from './tenant/tenant-change-modal.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { LinkedInSdkModule } from 'angular-linkedin-sdk';


let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("675930889955-2nec075l5bullj47d69hsbtfpkvtc759.apps.googleusercontent.com")   // , googleLoginOptions
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("226109201519566" )   // , fbLoginOptions
  },
  {
      id: LinkedInLoginProvider.PROVIDER_ID,
      provider: new LinkedInLoginProvider("812defjvqogxjr")  // 811mhtdamk866k, true, 'en_US'    
  }
]);

export function provideConfig() {
  return config;
}


var firebaseConfig = {
  apiKey: "AIzaSyA9dCbhjqODbH34yBpB2aWGjnhqG1-SGO4",
  authDomain: "gluonclouderp.firebaseapp.com",
  databaseURL: "https://gluonclouderp.firebaseio.com",
  projectId: "gluonclouderp",
  storageBucket: "",
  messagingSenderId: "466411369664"
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AccountRoutes),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    ModalModule,
    SocialLoginModule, //.initialize(config),
    AngularFireModule.initializeApp(firebaseConfig),
    LinkedInSdkModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    SocialComponent,
    TenantChangeComponent,
    TenantChangeModalComponent,
  ],
  providers: [
    // Inject apiKey and, optionally, authorize to integrate with LinkedIN official API
    { provide: 'apiKey', useValue: '812defjvqogxjr' },
    { provide: 'authorize', useValue: 'false' }, // OPTIONAL by default: false
    { provide: 'isServer', useValue: 'false' },  // OPTIONAL by default: false
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
})

export class AccountModule {}
