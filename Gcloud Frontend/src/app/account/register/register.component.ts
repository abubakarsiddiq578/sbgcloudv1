﻿import { Component, Injector, ElementRef, AfterViewInit, ViewChild, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AccountServiceProxy, RegisterInput, RegisterOutput } from '../../shared/service-proxies/service-proxies'
//import { AppComponentBase } from '@shared/app-component-base';
import { LoginService } from '../login/login.service';
import swal from 'sweetalert2';
import { SocialComponent } from "../login/social/social.component";
import { AppSessionService } from '@app/shared/session/app-session.service';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from 'angularx-social-login';
//import { accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './register.component.html',
    //animations: [accountModuleAnimation()],
    /*styleUrls: [
        './register.component.less'
    ],*/

    providers:[
        LoginService,AccountServiceProxy
    ]
})

export class RegisterComponent /*extends AppComponentBase*/ implements AfterViewInit , OnInit, OnDestroy {

    @ViewChild('cardBody') cardBody: ElementRef;

    model: RegisterInput = new RegisterInput();

    saving: boolean = false;

    test: Date = new Date();

    constructor(
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private _router: Router,
        private readonly _loginService: LoginService,
        private afAuth: AngularFireAuth,
        private authService: AuthService,
        private sessionService: AppSessionService,
        @Inject(LOCAL_STORAGE) private storage: WebStorageService
    ) {
        //super(injector);
    }

    ngAfterViewInit(): void {
        //$(this.cardBody.nativeElement).find('input:first').focus();
    }

    back(): void {
        this._router.navigate(['/login']);
    }

    save(): void {
        debugger;
        this.saving = true;
        this._accountService.register(this.model)
            .finally(() => { this.saving = false; })
            .subscribe((result:RegisterOutput) => {
                debugger;
                if (!result.canLogin) {
                    swal({
                        title: "SuccessfullyRegistered",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-success"
                    }).catch(swal.noop)
                    this._router.navigate(['/login']);
                    return;
                }

                //Autheticate
                // this.saving = true;
                // this._loginService.authenticateModel.userNameOrEmailAddress = this.model.userName;
                // this._loginService.authenticateModel.password = this.model.password;
                // this._loginService.authenticate(() => { this.saving = false; });
            });
    }

    ngOnInit() {
        if (this.storage.get('TenantId') > 0) {
            this._router.navigate(['/']);
        }
        else{
            const body = document.getElementsByTagName('body')[0];
            body.classList.add('register-page');
            body.classList.add('off-canvas-sidebar');
        }
      }

      ngOnDestroy(){
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('register-page');
        body.classList.remove('off-canvas-sidebar');
      }
}
