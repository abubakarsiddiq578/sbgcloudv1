import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
/*import { UserServiceProxy, CreateUserDto, RoleDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';*/
//import { LoginComponent } from "account/login/login.component";

@Component({
  selector: 'social',
  templateUrl: './social.component.html'
})
export class SocialComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('createSocialModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    //user: CreateUserDto = null;
    //roles: RoleDto[] = null;

    socialMediaUser: SocialMediaUser  = new SocialMediaUser();

    isUSignup: boolean = false;
    isESignup: boolean = false;
    isUSignin: boolean = false;
    isESignin: boolean = false;
    isURead: boolean = false;
    isERead: boolean = false;

    btnText: string = "";

    userEmail: string = "Saad";

    constructor(
        injector: Injector,
        //private loginComponent: LoginComponent
        //private _userService: UserServiceProxy,
    ) {
        //super(injector);
    }

    ngOnInit(): void {
        /*this._userService.getRoles()
        .subscribe((result) => {
            this.roles = result.items;
        });*/
    }

     show(): void {debugger;
         this.active = true;
        //  this.isESignup = true;
        //  this.isUSignup = true;
        //  this.isUSignin = true;
        //  this.isESignin = true;

         this.modal.show();
         //this.user = new CreateUserDto();
         //this.user.init({ isActive: true });
     }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        //TODO: Refactor this, don't use jQuery style code
        /*var roles = [];
        $(this.modalContent.nativeElement).find("[name=role]").each((ind:number, elem:Element) => {
            if($(elem).is(":checked") == true){
                roles.push(elem.getAttribute("value").valueOf());
            }
        });

        this.user.roleNames = roles;
        this.saving = true;
        this._userService.create(this.user)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });*/
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(false);
    }

    // Click on SignIn or SignUp button we emitt socialMediaUser object that contain user information

    socialMediaLogin(): void{
        this.modalSave.emit(this.socialMediaUser);
    }

    // set boolean value of bool type variable perform diffrent task in different condition

    setBoolValue(boolName: string): void{
        switch (boolName)
        {
            // Case when Signup and Email that get from particlur social media account

            case "isESignup" : 
            this.isESignup = false;
            this.isUSignup = true;
            this.isUSignin = true;
            this.isESignin = true;
            this.isERead = true;
            break;

            // Case when Signup and Username that get from particlur social media account

            case "isUSignup" : 
            this.isESignup = true;
            this.isUSignup = false;
            this.isUSignin = true;
            this.isESignin = true;
            this.isURead = true;
            break;

            // Case when SignIn and Username that get from particlur social media account

            case "isUSignin" : 
            this.isESignup = false;
            this.isUSignup = false;
            this.isUSignin = true;
            this.isESignin = false;
            this.isURead = true;
            break;

            // Case when SignIn and Email that get from particlur social media account

            case "isESignin" : 
            this.isESignup = false;
            this.isUSignup = false;
            this.isUSignin = false;
            this.isESignin = true;
            this.isERead = true;
            break;

            // case when set all bool fields to false;

            default:
            this.isESignup = false;
            this.isUSignup = false;
            this.isUSignin = false;
            this.isESignin = false;
            this.isERead = false;
            this.isURead = false;
        }
    }

    // set socialMediaUser object properties to null and also bool value to false also

    reset(): void{
        this.socialMediaUser.Email = "";
        this.socialMediaUser.Password = "";
        this.socialMediaUser.Username = "";
        
        this.setBoolValue(null);
    }
}

class SocialMediaUser
{
    Email: string; 
    Username: string;
    Password: string;
}
