﻿import { Component, Injector, ElementRef, ViewChild , OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from "@angular/common/http";
//import { AppComponentBase } from '@shared/app-component-base';
import { LoginService } from './login.service';
//import { accountModuleAnimation } from '@shared/animations/routerTransition';
//import { AbpSessionService } from '@abp/session/abp-session.service';*/
import { SocialComponent } from "./social/social.component";
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider  } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { AccountServiceProxy , UserDto, PagedResultDtoOfUserDto , RegisterInput , RegisterOutput } from '../../shared/service-proxies/service-proxies'
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import swal from 'sweetalert2';
import { LinkedInService } from 'angular-linkedin-sdk';
import { AppComponentBase } from '@app/shared/app-component-base';
import { AppSessionService } from '@app/shared/session/app-session.service';

@Component({
    templateUrl: './login.component.html',
    /*styleUrls: [
        './login.component.less'
    ],*/
    //animations: [accountModuleAnimation()]
    providers: [ AngularFireAuth , LinkedInService , AccountServiceProxy , LoginService]
})
export class LoginComponent extends AppComponentBase implements OnInit, OnDestroy {

    public isUserAuthenticated;

    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;

    @ViewChild('createSocialModal') createSocialModal: SocialComponent;
    
    @ViewChild('cardBody') cardBody: ElementRef;

    private user: SocialUser;
    private twitUser: any;

    fieldType: string;

    submitting: boolean = false;

    isVerifyEmail: boolean = false;
    
    userRegisterModel: RegisterInput = new RegisterInput();

    constructor(
        injector: Injector,
        public loginService: LoginService,
        private _router: Router,
        //private _sessionService: AbpSessionService,
        private authService: AuthService,
        private _accountService: AccountServiceProxy,
        private http: HttpClient,
        private afAuth: AngularFireAuth,
        private element: ElementRef,
        private _linkedInService: LinkedInService,
        private sessionService: AppSessionService,
    ){
        super(injector);
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    ngAfterViewInit(): void {
        //$(this.cardBody.nativeElement).find('input:first').focus();
    }

    /*get multiTenancySideIsTeanant(): boolean {
        return this._sessionService.tenantId > 0;
    }*/

    get isSelfRegistrationAllowed(): boolean {
        /*if (!this._sessionService.tenantId) {
            return false;
        }*/

        return true;
    }

    login(): void {

        if(this.loginService.authenticateModel.userNameOrEmailAddress == "" || this.loginService.authenticateModel.password == ""){
            swal({
                title: "UsernameOrEmail And Password is Required!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-danger"
            }).catch(swal.noop)
        }
        else{
            //this.loginService.rememberMe = true;
            this.submitting = true;
            this.loginService.authenticate(
                () => this.submitting = false
            );
        }
    }

    // For SignIn with Facebook using FacebookLoginProvider ID

    signInWithFB(): void {
          this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
            (userData) => {

              //console.log(" sign in data : " , userData);

              this.FB_Google_LinkedIn_Twitter_Authorization(userData , null , "FB");  
         },
         error => {
            //console.log(error)
            //abp.message.error("Network error plz try again!");
        });
        
    }

    // For SignIn with Google using GoogleLoginProvider ID

    signInWithGoogle(): void{
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
          (userData) => {
            //console.log(" sign in data : " , userData);
            this.FB_Google_LinkedIn_Twitter_Authorization(userData , null , "Google");  
          },
          error => {
              //console.log(error)
              //abp.message.error("Network error plz try again!");
          });
    }

    // For SignIn with LinkedIn using LinkedInLoginProvider ID

    signInWithLinkedIn(): void{

        this.authService.signIn(LinkedInLoginProvider.PROVIDER_ID).then(
            (userData) => {
              //console.log(" sign in data : " , userData);

              this.FB_Google_LinkedIn_Twitter_Authorization(userData , null , "LinkedIn");  
            },
            error => {

                swal({
                    title: error,
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-success"
                }).catch(swal.noop)

                console.log(error)
                //abp.message.error("Network error plz try again!");
            }); 
    }

    // For SignIn with Twitter using firebase TwitterAuthProvider

    signInWithTwitter(): void{ 
        let provider = new firebase.auth.TwitterAuthProvider();
        this.afAuth.auth.signInWithPopup(provider).then(res => {
            //console.log(res.user.displayName);
            this.FB_Google_LinkedIn_Twitter_Authorization(null , res , "Twitter");
        }, 
        err => {
            //console.log(err);
            //abp.message.error("Network error plz try again!");
        });
    }
    
    //For All social media result authorization get particular user info 

    FB_Google_LinkedIn_Twitter_Authorization(user: SocialUser , twitUser: any , type: string): void
    {
        if (type == "FB" || type == "Google"  || type == "LinkedIn") 
        {
            //this.authService.authState.subscribe((user) => {
                this.user = user;

                swal({
                    title: this.user.firstName + "---" + this.user.lastName + "---" + this.user.name + "---" + this.user.email,
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-success"
                }).catch(swal.noop)

                //console.log(this.user.firstName + "---" + this.user.lastName + "---" + this.user.name + "---" + this.user.email)
                /*if (this.user.email != null)
                {
                    this.checkEmailOrUername(this.user.email , "email");
                }
                else
                {
                    abp.message.error("Some error occur due to network issue plz try again!");
                }*/
            //});
        }

        else if(type == "Twitter")
        {
            this.twitUser = twitUser;

            swal({
                title: this.twitUser.user.displayName + "---" + this.twitUser.additionalUserInfo.username,
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success"
            }).catch(swal.noop)

            /*if(this.twitUser.additionalUserInfo.username != null)
            {
                this.checkEmailOrUername(this.twitUser.additionalUserInfo.username , "username");
            }
            else
            {
                abp.message.error("Some error occur due to network issue plz try again!");
            }*/
        }
    }

    //For check email aur username of user whether they exists in our database 

    /*checkEmailOrUername(usernameOremail: string , field: string): void{
        this._accountService.verifyUsernNameOrEmail(usernameOremail)
        .subscribe((result) => {
            //console.log(result);
            if (result.normalizedEmailAddress !== undefined)
            {
                this.isVerifyEmail = true;
                this.resultcheckEmailOrUsername(field);
            }
            else
            {
                this.isVerifyEmail = false;
                this.resultcheckEmailOrUsername(field);
            }
        },
        error => {
            //console.log(error)
            abp.message.error(error);
            this.isVerifyEmail = false;
        }) 
    }*/

    // get result of checkEmailOrUername function to find and match email and username with our database records

    resultcheckEmailOrUsername(field: string): void{

        //this.createSocialModal.reset();

        // Set fieldType whether its username or email 

        this.fieldType = field;

        // Its shows user record is exist and user have alrady account on our site 

        if(this.isVerifyEmail == true)
        {

            // Set email that get from user info on particular social media account

            if(field == "email")
            {
                //this.createSocialModal.socialMediaUser.Email = this.user.email;
                
                //this.createSocialModal.setBoolValue("isESignin");
            }

            // Set username that get from user info on particular social media account

            else if(field == "username")
            {
                //this.createSocialModal.socialMediaUser.Username = this.twitUser.additionalUserInfo.username;
                
                //this.createSocialModal.setBoolValue("isUSignin");
            }

            //this.createSocialModal.btnText = "SignIn";
            this.createSocialPopup();
        }

        // Its shows user record does not exist and user have not an account on our site 

        else
        {
             // Set email that get from user info on particular social media account

            if(field == "email")
            {
                //this.createSocialModal.socialMediaUser.Email = this.user.email;

                //this.createSocialModal.setBoolValue("isESignup");
            }

            // Set username that get from user info on particular social media account

            else if(field == "username")
            {
                //this.createSocialModal.socialMediaUser.Username = this.twitUser.additionalUserInfo.username;

                //this.createSocialModal.setBoolValue("isUSignup");
            }

            //abp.message.error("Your Account is not created on our app plz first create your account!");

            //this.createSocialModal.btnText = "SignUp";
            this.createSocialPopup();
        }
    }

    // For SignOut from particular social media account after successfully signIn and retrive user information 

    FB_Google_LinkedIn_Twitter_SignOut(): void {

        //if(this.fieldType == "username")
        {
            this.afAuth.auth.signOut();
        }
       // else if(this.fieldType == "email")
        {
            this.authService.signOut();
        }
    }

    //Use to show Social Medial Form PopUp 

    createSocialPopup(): void
    {
        //this.createSocialModal.show()
    }

    //get result from Social Medial Form PopUp when we click SignIn button or SignUp button after provide some information 

    socialMediaLogin(SocialMedialUser)
    {
        // When click on Cancel button then call signout function

        if (SocialMedialUser == false)
        {
            this.FB_Google_LinkedIn_Twitter_SignOut();
        }

        // Else then identify from button text whether user need signIn or SignUp

        else
        {
            // If Social Medial Form PopUp text is SignIn then user sign in 

            /*if (this.createSocialModal.btnText == "SignIn")
            {
                //console.log(SocialMedialUser);

                if(this.fieldType == "username")
                {
                    this.loginService.authenticateModel.userNameOrEmailAddress = SocialMedialUser.Username;
                }
                else if(this.fieldType == "email")
                {
                    this.loginService.authenticateModel.userNameOrEmailAddress = SocialMedialUser.Email;
                }

                this.loginService.authenticateModel.password = SocialMedialUser.Password;
                this.loginService.rememberMe = true;

                this.login();
            }*/

            // If Social Medial Form PopUp text is SignUp then user sign up 

            /*else if (this.createSocialModal.btnText == "SignUp")
            {
                this.fillUserRegisterModel(SocialMedialUser);
                this.registerUser();
                this.FB_Google_LinkedIn_Twitter_SignOut();
            } */
        }
    }

    // Use to fill register model for signup or account creation on our site

    fillUserRegisterModel(socialMedialUser: any): void{

        /*if(this.fieldType == "username")
        {
            this.userRegisterModel.name = this.twitUser.user.displayName;
            this.userRegisterModel.surname = this.twitUser.user.displayName;
            this.userRegisterModel.userName = this.twitUser.additionalUserInfo.username;
            this.userRegisterModel.emailAddress = socialMedialUser.Email
        }
        else if(this.fieldType == "email")
        {
            this.userRegisterModel.name = this.user.name;
            this.userRegisterModel.surname = this.user.firstName;
            this.userRegisterModel.userName = socialMedialUser.Username;
            this.userRegisterModel.emailAddress = this.user.email;
        }
        
        this.userRegisterModel.password = socialMedialUser.Password;*/
    }

    // Call register function api to register user

    registerUser(): void {
        /*this._accountService.register(this.userRegisterModel)
            .subscribe((result:RegisterOutput) => {
                //if (!result.canLogin) {
                    this.createSocialModal.close();
                    this.notify.success(this.l('SuccessfullyRegistered'));
                    //this._router.navigate(['/login']);
                    return;
                //}
            });*/
    }

    ngOnInit() {

        if (this.sessionService.user) {
            this._router.navigate(['/']);
        }
        else{
            var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
        }
    }
    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function() {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
    ngOnDestroy(){
      const body = document.getElementsByTagName('body')[0];
      body.classList.remove('login-page');
      body.classList.remove('off-canvas-sidebar');
    }

    public subscribeToisInitialized(){
        debugger;
        this._linkedInService.isInitialized$.subscribe({
        next: (state) => {
            debugger;
          // state will always return true when API finishes loading
        },
        complete: () => {
          // Completed
        }
      });
    }

    public subscribeToLogin(){
        debugger;
        this.subscribeToisInitialized();

        this._linkedInService.isUserAuthenticated$.subscribe({
            next: (state) => {
              this.isUserAuthenticated = state;
            }
        });

        this._linkedInService.login().subscribe({
          next: (state) => {
              debugger;
              console.log(state);
            // state will always return true when login completed 
          },
          complete: () => {
            // Completed
            debugger;
            this.rawApiCall();
          },
          error(error){
              debugger;
              console.log(error);
          }
        });
    }

    public rawApiCall(){
        debugger;
        const url = '/people/~?format=json';
        this._linkedInService.raw(url)
          .asObservable()
            .subscribe({
              next: (data) => {
                debugger;
                console.log(data);
              },
              error: (err) => {
                console.log(err);
              },
              complete: () => {
                console.log('RAW API call completed');
              }
            });
    }
}



