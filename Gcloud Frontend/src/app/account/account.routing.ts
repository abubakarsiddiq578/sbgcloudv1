import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AppRouteGuard } from '@app/shared/auth/auth-route-guard';


export const AccountRoutes: Routes = [

    {
        path: '',
        children: [ {
            path: 'login',
            component: LoginComponent
        }, {
            path: 'register',
            component: RegisterComponent,
            canActivate: [AppRouteGuard]
        }]
    }
];
