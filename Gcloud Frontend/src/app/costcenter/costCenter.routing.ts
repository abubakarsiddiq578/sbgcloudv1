import { Routes } from '@angular/router';
import { CostCenterComponent } from './costcenter.component';

export const CostCenterRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'CostCenter',
        component: CostCenterComponent
    }]}
];  
