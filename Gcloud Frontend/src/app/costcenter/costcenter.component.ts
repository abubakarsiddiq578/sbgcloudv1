import { Component, Injector, AfterViewInit , ViewChild, OnInit} from '@angular/core';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';
// import { AppAuthService } from '@shared/auth/app-auth.service';
import { AddCostCenterComponent } from "./addcostcenter/addcostcenter.component";
import { EditCostCenterComponent } from "./editcostcenter/editcostcenter.component";
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
// import { PagedRequestDto } from '@shared/paged-listing-component-base';
// import { CostCenterServiceProxy, TenantServiceProxy, CostCenterDto, PagedResultDtoOfCostCenterDto } from '@shared/service-proxies/service-proxies';
// import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
// import { Subject } from 'rxjs/Subject';

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: string[][];
  }
  
  declare const $: any;

@Component({
    templateUrl: './costcenter.component.html',
    styleUrls: [
        './costcenter.component.less'
    ],
    // animations: [appModuleAnimation()],
    // providers: [CostCenterServiceProxy]
})
export class CostCenterComponent implements OnInit, AfterViewInit {
    @ViewChild('AddCostCenterModal') public AddCostCenterModal: AddCostCenterComponent;
    @ViewChild('EditCostCenterModal') EditCostCenterModal: EditCostCenterComponent;

    public dataTable: DataTable;

//     result: CostCenterDto[]=[];
//   IsEdit: boolean = abp.auth.isGranted("Pages.Administration.Edit_Samples");;
//   IsDelete: boolean = abp.auth.isGranted("Pages.Administration.Delete_Samples");
//   IsCreate: boolean = abp.auth.isGranted("Pages.Administration.Create_Samples");

//   dtOptions: DataTables.Settings = {};  
//   dtTrigger: Subject<any> = new Subject();

    constructor(injector: Injector , private _router: Router
        // private _authService: AppAuthService,
        // private _costCenterServiceProxy: CostCenterServiceProxy    
    ) 
    {
        //super(injector);
    }

    // protected list(request: PagedRequestDto, pageNumber:number, finishedCallback:Function): void{
    //     this._costCenterServiceProxy.getAll(request.skipCount, request.maxResultCount)
    //     .finally(() => {
    //         finishedCallback();
    //     })
    //     .subscribe((result:PagedResultDtoOfCompanyInfoDto)=>{
    //         this.result = result.items;
    //         this.showPaging(costCenter, pageNumber);
    //     });
    // }

    // protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
    //     this._costCenterServiceProxy.getAll(request.skipCount, request.maxResultCount)
    //         .finally(()=>{
    //             finishedCallback();
    //         })
    //         .subscribe((result:PagedResultDtoOfCostCenterDto)=>{
    //     this.result = result.items;
    //     this.dtTrigger.next();
    //     this.showPaging(result, pageNumber);
    //         });
    // }


    // protected delete(entity: CostCenterDto): void {
    //     abp.message.confirm(
    //   "Delete Sample '"+ entity.name +"'?",
    //   (result:boolean) => {
    //     if(result) {
    //       this._costCenterServiceProxy.delete(entity.id)
    //         .finally(() => {
    //               abp.notify.info("Deleted Sample: " + entity.name );
    //           this.refresh();
    //         })
    //         .subscribe(() => { });
    //     }
    //   });
    //   }

      //ngOnInit(){
        // this.dtOptions = {
        //     pagingType: 'full_numbers',
        //     pageLength: 2
        //   };
      //}


globalFunction : GlobalFunctions = new GlobalFunctions

      ngOnInit() {
        if(!this.globalFunction.hasPermission('View','CostCenter'))
        {
          this.globalFunction.showNoRightsMessage('View')
          this._router.navigate([''])
        }
        this.dataTable = {
          headerRow: [ 'Name', 'Code', 'Sort Order', 'Group', 'Active', 'Outward gatepass', 'Day Shift', 'LC ID', 'Logical','Actions'],
          footerRow: [ 'Name', 'Code', 'Sort Order', 'Group', 'Active', 'Outward gatepass', 'Day Shift', 'LC ID', 'Logical', 'Actions'],
    
          dataRows: [
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
              ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
              
              
          ]
       };
      }



      ngAfterViewInit(){
        // this.refresh();
        
      }

    open(): void{
        // abp.message.error("Open");
    }

    AddCostCenter(): void {
        if(!this.globalFunction.hasPermission('Create','CostCenter'))
        {
          this.globalFunction.showNoRightsMessage('Create')
          return
        }
        this.AddCostCenterModal.show();
    }

    
    EditCostCenter(): void {
        if(!this.globalFunction.hasPermission('Edit','CostCenter'))
        {
          this.globalFunction.showNoRightsMessage('Edit')
          return
        }
        this.EditCostCenterModal.show();    
    }
}