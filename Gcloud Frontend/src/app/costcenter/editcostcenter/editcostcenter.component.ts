import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { CostCenterServiceProxy, CostCenterDto, RoleDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'editcostcenter',
  templateUrl: './editcostcenter.component.html',
//   animations: [appModuleAnimation()],
//   providers: [CostCenterServiceProxy]
})

export class EditCostCenterComponent  implements OnInit {

    @ViewChild('createEditCostCenterModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    // costCenter: CostCenterDto = new CostCenterDto();

    constructor(
        injector: Injector
        // private _costCenterService: CostCenterServiceProxy,
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    show(): void {
        this.active = true; 
        this.modal.show();
        // this._costCenterService.get(id)
        //     .finally(() => {
        //         this.active = true;
        //         this.modal.show();
        //     })
        //     .subscribe((result: CostCenterDto) => {
        //         this.costCenter = result;
        //     });
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    // save(): void {
    //     this.saving = true;
    //     this._costCenterService.update(this.costCenter)
    //       .finally(() => {this.saving = false;})
    //       .subscribe(() => {
    //         this.notify.info(this.l('Record Updated Successfully'));
    //         this.close();
    //         this.modalSave.emit(null);
    //       });
    //   }
   

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
