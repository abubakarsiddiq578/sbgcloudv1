import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { CostCenterRoutes } from './costCenter.routing';
import { ModalModule } from 'ngx-bootstrap';

import { CostCenterComponent } from './costcenter.component';
import {AddCostCenterComponent} from './addcostcenter/addcostcenter.component';
import {EditCostCenterComponent} from './editcostcenter/editcostcenter.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CostCenterRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule.forRoot()
  ],
  declarations: [
    CostCenterComponent,
    AddCostCenterComponent,
    EditCostCenterComponent
  ]
})

export class CostCenter {}
