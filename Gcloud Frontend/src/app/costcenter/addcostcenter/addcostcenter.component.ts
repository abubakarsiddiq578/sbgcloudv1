import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { CostCenterServiceProxy, CostCenterDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';

@Component({
  selector: 'addcostcenter',
  templateUrl: './addcostcenter.component.html'
})
export class AddCostCenterComponent implements OnInit {

    @ViewChild('AddCostCenterModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    // costCenter: CostCenterDto = new CostCenterDto();

    constructor(
        injector: Injector
        // private _costCenterService: CostCenterServiceProxy,
    ) {
        //super(injector);
    }

    ngOnInit(): void {
        // this._userService.getRoles()
        // .subscribe((result) => {
        //     this.roles = result.items;
        // });
    }

    show(): void {
        this.active = true; 
        this.modal.show();
    //     this.costCenter = new CostCenterDto();
    // this.costCenter.init({ isActive: true });
        // this.user = new CreateUserDto();
        // this.user.init({ isActive: true });
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    // save(): void {
    //     //TODO: Refactor this, don't use jQuery style code
    //   this.saving = true;
    //     this._costCenterService.create(this.costCenter)
    //         .finally(() => { this.saving = false; })
    //         .subscribe(() => {
    //             this.notify.info(this.l('Saved Successfully'));
    //             this.close();   
    //             this.modalSave.emit(null);
    //         });
    //   }
      


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
