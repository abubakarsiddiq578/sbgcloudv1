import { Component, OnInit, ViewChild, AfterViewInit, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { RegionDto, RegionServiceProxy, ZoneDto, ZoneServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'add-zone',
  templateUrl: './add-zone.component.html',
  styleUrls: ['./add-zone.component.scss'],
  providers: [RegionServiceProxy, ZoneServiceProxy]
})
export class AddZoneComponent implements OnInit {


  @ViewChild('addZoneModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  Zone: ZoneDto = new ZoneDto();
  Region: RegionDto[];

  isActive: boolean;

  public regionName: string;

  _zone: FormGroup

  constructor(injector: Injector,
    private zoneService: ZoneServiceProxy,
    private regionService: RegionServiceProxy,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.getAllRegion();
    this.initValidation();
  }


  initValidation() {

    this._zone = this.formBuilder.group({
      regionName: [null, Validators.required],
      zoneName: [null, Validators.required]
    });

  }

 /* onType() {

    if (this._zone.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._zone);
    }
  }*/

  onType() {
 
    if (this._zone.valid) {
    
      let a,p=0 ;
      for(a=0;a<this.Region.length;a++){
        if(this.Zone.regionId== this.Region[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.regionName = null; // null empName 
      
        this.validateAllFormFields(this._zone);
        this.regionDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._zone);
       
    }
  }
  

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }



  getAllRegion() {

    this.regionService.getAllRegions().subscribe((result) => {

      this.Region = result;

    })
  }

  show(): void {
    this.isActive = true
    this.active = true;
    this.modal.show();
    this.Zone = new ZoneDto();

    this.regionName = null;

    this.regionDropdownSearch();

    this._zone.markAsUntouched({ onlySelf: true }); // to make it untouched
  }


  /////////////////////////////////filter Search/////////////////

  myControl = new FormControl();

  filteredOptions: Observable<RegionDto[]>;


  public regionDropdownSearch() {
    debugger;
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | RegionDto>(''),
        map(value => typeof value === 'string' ? value : value.regionName),
        map(name => name ? this._filter(name) : this.Region.slice())
      );
  }

  private _filter(value: string): RegionDto[] {

    const filterValue = value.toLowerCase();

    return this.Region.filter(option => option.regionName.toString().toLowerCase().includes(filterValue));

  }
  // using function expression//
  displayRegion = (region: RegionDto): any => {

    if (region instanceof RegionDto) {
      this.Zone.regionId = region.id;
      return region.regionName;
    }
    return region;

  }

///////////////////////////////////////////////////////////
  save(): void {

    this.Zone.isActive = this.isActive;
    this.saving = true;

    this.zoneService.create(this.Zone)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

  onShown(): void {

  }

}
