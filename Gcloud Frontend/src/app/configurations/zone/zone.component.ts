import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddZoneComponent } from './add-zone/add-zone.component';
import { EditZoneComponent } from './edit-zone/edit-zone.component';
import {ZoneDto , ZoneServiceProxy  } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.scss'],
  providers :[ZoneServiceProxy]
})

export class ZoneComponent implements OnInit, AfterViewInit {



  @ViewChild('addZoneModal') addZoneModal: AddZoneComponent;
  @ViewChild('editZoneModal') editZoneModal: EditZoneComponent;

  constructor(private zoneService: ZoneServiceProxy ,private _router : Router) { }


  public dataTable: DataTable;
  Zone: ZoneDto[];
  data: string[] = [];

  globalFunction: GlobalFunctions = new GlobalFunctions


  ngOnInit() {
    if(!this.globalFunction.hasPermission("View","Zone")){
      this.globalFunction.showNoRightsMessage("View")
      this._router.navigate(['']);
    } 
    this.getAllZones();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllZones() {

    this.zoneService.getAllZones().subscribe((result) => {

      this.intializeDatatable();
      this.Zone = result;
      this.fillDatatable();

    });

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Region Name', 'Zone Name', 'SortOrder', 'Active', 'Comments', 'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.Zone.length; i++) {

      this.data.push(this.Zone[i].region.regionName)
      this.data.push(this.Zone[i].zoneName)
      this.data.push(this.Zone[i].sortOrder.toString())
      this.data.push(this.Zone[i].isActive.toString())

      this.data.push(this.Zone[i].comments)

      this.data.push(this.Zone[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddZone(): void {
    if(!this.globalFunction.hasPermission("Create","Zone")){
      this.globalFunction.showNoRightsMessage("Create")
      return
    } 
    this.addZoneModal.show();
  }

  EditZone(id: string): void {
    if(!this.globalFunction.hasPermission("Edit","Zone")){
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    
   this.editZoneModal.show(parseInt(id));
  }



  protected delete(id: string): void {
    if(!this.globalFunction.hasPermission("Delete","Zone")){
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.zoneService.delete(parseInt(id))
          .finally(() => {
            this.getAllZones();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Zone has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Zone Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

}
