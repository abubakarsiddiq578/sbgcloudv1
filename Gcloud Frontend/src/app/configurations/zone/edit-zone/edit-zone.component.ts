import { Component, OnInit, ViewChild, AfterViewInit, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { RegionDto, RegionServiceProxy, ZoneDto, ZoneServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-zone',
  templateUrl: './edit-zone.component.html',
  styleUrls: ['./edit-zone.component.scss'],
  providers: [RegionServiceProxy, ZoneServiceProxy]
})
export class EditZoneComponent implements OnInit {

  @ViewChild('editZoneModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  Zone: ZoneDto = new ZoneDto();
  Region: RegionDto[];

  public regionName : string ;

  isActive: boolean;
  _zone: FormGroup

  constructor(injector: Injector,
    private zoneService: ZoneServiceProxy,
    private regionService: RegionServiceProxy,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.getAllRegion();
    this.initValidation();
  }


  initValidation() {

    this._zone = this.formBuilder.group({
      regionId: [null, Validators.required],
      zoneName: [null, Validators.required]
    });

  }

  onType() {

    if (this._zone.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._zone);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
///////////////////////////////
 /////////////////////////////////filter Search/////////////////

 myControl = new FormControl();

 filteredOptions: Observable<RegionDto[]>;


 public regionDropdownSearch() {
   debugger;
   this.filteredOptions = this.myControl.valueChanges
     .pipe(
       startWith<string | RegionDto>(''),
       map(value => typeof value === 'string' ? value : value.regionName),
       map(name => name ? this._filter(name) : this.Region.slice())
     );

 }

 private _filter(value: string): RegionDto[] {

   const filterValue = value.toLowerCase();

   return this.Region.filter(option => option.regionName.toString().toLowerCase().includes(filterValue));

 }
 // using function expression//
 displayRegion = (region: RegionDto): any => {

   if (region instanceof RegionDto) {
     this.Zone.regionId = region.id;
     return region.regionName;
   }
   return region;

 }


//////////////////////////////
  show(id: number): void {

    debugger;
    this.zoneService.get(id).finally(() => {

      this.active = true;
      this.modal.show();

    }).subscribe((result: ZoneDto) => {
      this.Zone = result;
      
      this.regionService.get(result.regionId).subscribe((result)=>{

         this.regionName = result.regionName ;

      })

    });
    this.regionDropdownSearch();
    this._zone.markAsUntouched({onlySelf:true});
  }

  onShown(): void {


  }


  getAllRegion() {

    this.regionService.getAllRegions().subscribe((result) => {

      this.Region = result;

    })
  }

  save(): void {


    this.saving = true;

    this.zoneService.update(this.Zone)
      .finally(() => { this.saving = false; })
      .subscribe(() => {

        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }




  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }





}
