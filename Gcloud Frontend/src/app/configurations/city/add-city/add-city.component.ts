import { Component, OnInit, ViewChild, AfterViewInit, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { CityDto, CityServiceProxy, ProvinceDto, ProvinceServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith, count } from 'rxjs/operators';
import { debug } from 'util';
import { SerachDropdownComponent } from '../serach-dropdown/serach-dropdown.component';

@Component({
  selector: 'add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss'],
  providers: [CityServiceProxy, ProvinceServiceProxy]
})
export class AddCityComponent implements OnInit {

  @ViewChild('searchDropdownModal') searchDropdownModel: SerachDropdownComponent;
  @ViewChild('addCityModal') modal: ModalDirective;
  @ViewChild(SerachDropdownComponent) SearchDD;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  City: CityDto = new CityDto();

  public Province: ProvinceDto[];

  isActive: boolean;

  provinceName: string;

  _city: FormGroup

  constructor(injector: Injector,
    private provinceService: ProvinceServiceProxy,
    private cityService: CityServiceProxy,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.getAllProvince();
    this.initValidation();
  }


  initValidation() {

    this._city = this.formBuilder.group({
      provinceId: [null, Validators.required],
      cityName: [null, Validators.required]

    });

  }


//   onType() {

//     if (this._city.valid) {
//       this.save();
//     } else {
//       this.validateAllFormFields(this._city);
//     }
//   }

//   validateAllFormFields(formGroup: FormGroup) {
// }

onType() {
  debugger;
  if (this._city.valid) {
    debugger;
    let a,p=0 ;
    for(a=0;a<this.Province.length;a++){
      if(this.City.provinceId == this.Province[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.provinceName = null; // null empName 
      this.validateAllFormFields(this._city);
      this.provinceDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._city);
     
  }
}


validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });

  }


  ////////////////////////////////filter Method///////////////////////////


  myControl = new FormControl();

  filteredOptions: Observable<ProvinceDto[]>;


  public provinceDropdownSearch() {
    debugger;
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | ProvinceDto>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.Province.slice())
      );
  }



  displayProvince = (province: ProvinceDto): any => {
    debugger;

    if (province instanceof ProvinceDto) {
      this.City.provinceId = province.id;
      return province.name;
    }
    return province;

  }



  private _filter(value: string): ProvinceDto[] {

    debugger;
    const filterValue = value.toLowerCase();

    return this.Province.filter(option => option.name.toString().toLowerCase().includes(filterValue));

  }


  //////////////////////////////////////End of Filter Method//////////////////////////////////////////



  getAllProvince() {

    this.provinceService.getAllProvinces().subscribe((result) => {

      debugger;
      this.Province = result;

    })
  }
//smart search
  addSearchDD(): void {
    this.searchDropdownModel.show()
  }

  show(): void{
    this.isActive = true;
    this.active = true;
    this.modal.show();
    this.City = new CityDto();

    /*this.myControl = new FormControl();
    this.filteredOptions = new Observable<ProvinceDto[]>();*/
    
    this.provinceName = null;

    this.provinceDropdownSearch();

    this._city.markAsUntouched({onlySelf:true});
}



  save(): void {

    debugger;

    this.City.isActive = this.isActive;
    this.saving = true;

    this.cityService.create(this.City)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }
  

  //smart search compare id
  ProvinceId(id): void {
    debugger
    
    for(let i = 0; i< this.Province.length; i++)
    {
      if(this.Province[i].id == id)
      {
        this.provinceName = this.Province[i].name;
        this.City.provinceId = this.Province[i].id;
      }
    }

    // this.provinceName == id
  }

  onShown(): void {

  }

}
