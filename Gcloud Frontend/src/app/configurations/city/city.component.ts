import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddCityComponent } from './add-city/add-city.component';
import { EditCityComponent } from './edit-city/edit-city.component';
import { CityDto, CityServiceProxy, ProvinceDto, ProvinceServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss'],
  providers: [CityServiceProxy]
})



export class CityComponent implements OnInit, AfterViewInit {



  @ViewChild('addCityModal') addCityModal: AddCityComponent;
  @ViewChild('editCityModal') editCityModal: EditCityComponent;



  constructor(private cityService: CityServiceProxy,
    private _router: Router
    ) { }


  public dataTable: DataTable;
  City: CityDto[];
  data: string[] = [];

  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "City")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);
    }
    this.getAllCities();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllCities() {

    this.cityService.getAllCities().subscribe((result) => {

      this.intializeDatatable();
      this.City = result;
      this.fillDatatable();

    });

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Province Name', 'City Name', 'SortOrder', 'Active', 'Comments', 'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.City.length; i++) {

      this.data.push(this.City[i].province.name)
      this.data.push(this.City[i].cityName)
      this.data.push(this.City[i].sortOrder.toString())
      this.data.push(this.City[i].isActive.toString())

      this.data.push(this.City[i].comments)

      this.data.push(this.City[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddCity(): void {
    if (!this.globalFunction.hasPermission("Create", "City")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
    debugger;
    this.addCityModal.show();
  }

  EditCity(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "City")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    debugger;
    this.editCityModal.show(parseInt(id));
  }



  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "City")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.cityService.delete(parseInt(id))
          .finally(() => {
            this.getAllCities();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'City has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'City Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

}
