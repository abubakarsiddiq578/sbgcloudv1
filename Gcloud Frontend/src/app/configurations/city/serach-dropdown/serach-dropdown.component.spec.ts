import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerachDropdownComponent } from './serach-dropdown.component';

describe('SerachDropdownComponent', () => {
  let component: SerachDropdownComponent;
  let fixture: ComponentFixture<SerachDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerachDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerachDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
