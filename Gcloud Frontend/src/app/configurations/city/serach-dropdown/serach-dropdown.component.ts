import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { CityDto, CityServiceProxy, ProvinceServiceProxy, ProvinceDto } from '@app/shared/service-proxies/service-proxies';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}
@Component({
  selector: 'app-serach-dropdown',
  templateUrl: './serach-dropdown.component.html',
  styleUrls: ['./serach-dropdown.component.scss']
})
export class SerachDropdownComponent implements OnInit {
  @ViewChild('searchDropdownModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  City : CityDto = new CityDto();
  public dataTable: DataTable;
    data: string[] = [];

    result: ProvinceDto[] = [];

    ProId: any;

  _city : FormGroup
  constructor(private provinceService:ProvinceServiceProxy,
    private _router: Router) { }

  ngOnInit() {
    this.getAllRecord();
  }
  getAllRecord(): void {
    debugger;
    this.provinceService.getAllProvinces()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        this.paging();
        this.result = result;
        this.getAllData();

      });
  }
  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].id.toString());
      this.data.push(this.result[i].name);
      this.data.push(this.result[i].country.name);
     

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }
  paging(): void {
    this.dataTable = {
      headerRow: [ 'Province', 'Country' ],
        footerRow: [ 'Province', 'Country' ],
        dataRows: [
        
      ]

  };
}

ngAfterViewInit() {
  $('#datatabless').DataTable({
    "pagingType": "full_numbers",
    "lengthMenu": [
      [10, 25, 50, -1],
      [10, 25, 50, "All"]
    ],
    responsive: true,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search records",
    }

  });

  const table = $('#datatabless').DataTable();

  $('.card .material-datatables label').addClass('form-group');
}

  show(){
    this.active = true;
    this.modal.show();

  }
  close(): void {
    this.active = false;
    this.modal.hide();
}
//smart Search
@Input()
Ivalue: any 

//smart search
Data(ProvId : any) : void{
  debugger
this.Ivalue = ProvId
this.active =  true
this.close();
this.modalSave.emit(this.Ivalue);

}



 

}


