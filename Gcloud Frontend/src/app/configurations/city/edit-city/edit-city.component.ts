import { Component, OnInit, ViewChild, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { CityDto, CityServiceProxy, ProvinceDto, ProvinceServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

////////////////imports for filter //
import {Observable} from 'rxjs';
import {map, startWith, count} from 'rxjs/operators';
import { debug } from 'util';


@Component({
  selector: 'edit-city',
  templateUrl: './edit-city.component.html',
  styleUrls: ['./edit-city.component.scss'],
  providers: [CityServiceProxy, ProvinceServiceProxy]
})
export class EditCityComponent implements OnInit {

  @ViewChild('editCityModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();


  active: boolean = false;
  saving: boolean = false;

  City: CityDto = new CityDto();
  Province: ProvinceDto[];
  public provinceName : string ;
  _city : FormGroup

  constructor(injector: Injector,
    private provinceService: ProvinceServiceProxy,
    private cityService: CityServiceProxy,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() : void{

    this.getAllProvince();
    this.initValidation();

  }


  initValidation() {

    this._city = this.formBuilder.group({
        provinceId: [null, Validators.required],
        cityName: [null, Validators.required]
    
      });

}


/*onType() {

  if (this._city.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._city);
  }
}*/
onType() {
  debugger;
  if (this._city.valid) {
    debugger;
    let a,p=0 ;
    for(a=0;a<this.Province.length;a++){
      if(this.City.provinceId == this.Province[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.provinceName = null; // null empName 
      this.validateAllFormFields(this._city);
      this.provinceDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._city);
     
  }
}


validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}

////////////////////////////////filter Method///////////////////////////


myControl = new FormControl();
  
filteredOptions: Observable<ProvinceDto[]>;


public provinceDropdownSearch(){
  debugger;
 this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith<string | ProvinceDto >(''),
    map(value => typeof value === 'string' ? value : value.name),
    map(name => name ? this._filter(name) : this.Province.slice())
  );
}


displayProvince = (province: ProvinceDto):any => {
  debugger;
  
  if(province instanceof ProvinceDto)
  {
    this.City.provinceId = province.id;
    return province.name;
  }
  return province;

}



private _filter(value: string): ProvinceDto[]{

  debugger;
  const filterValue = value.toLowerCase();

  return this.Province.filter(option => option.name.toString().toLowerCase().includes(filterValue));
  
}


//////////////////////////////////////End of Filter Method//////////////////////////////////////////

  show(id: number): void {

    debugger;
    this.cityService.get(id).finally(() => {

      this.active = true;
      this.modal.show();

    }).subscribe((result: CityDto) => {
      this.City = result;

      this.provinceService.get(result.id).subscribe((result : ProvinceDto)=>{
      
        this.provinceName = result.name; // to display country Name 
        
      })

    });
    this.provinceDropdownSearch();
    this._city.markAsUntouched({onlySelf:true});
  }


  onShown(): void {

  }



  getAllProvince() {

    this.provinceService.getAllProvinces().subscribe((result) => {

      this.Province = result;

    });
  }


  save(): void {

    
    this.saving = true;
  
    this.cityService.update(this.City)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
       
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

}
