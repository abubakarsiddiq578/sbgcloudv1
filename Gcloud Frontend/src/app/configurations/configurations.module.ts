import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { MaterialModule } from '../app.module';
import { ConfigurationRoutes } from './configurations.routing';
import { ModalModule } from 'ngx-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { MatMomentDatetimeModule } from "@mat-datetimepicker/moment";
import { MatDatetimepickerModule } from "@mat-datetimepicker/core";

import { HrConfigurationComponent } from '@app/configurations/hr-configuration/hr-configuration.component';
import { EditHrConfigurationComponent } from '@app/configurations/hr-configuration/edit-hr-configuration/edit-hr-configuration.component';
import { EditOvertimeConfigurationComponent } from '@app/configurations/overtime-configuration/edit-overtime-configuration/edit-overtime-configuration.component';
import { OvertimeConfigurationComponent } from '@app/configurations/overtime-configuration/overtime-configuration.component';
import { BeltComponent } from '@app/configurations/belt/belt.component';
import { StateComponent } from '@app/configurations/state/state.component';
import { ZoneComponent } from '@app/configurations/zone/zone.component';
import { ProvinceComponent } from '@app/configurations/province/province.component';
import { TerritoryComponent } from '@app/configurations/territory/territory.component';
import { CityComponent } from '@app/configurations/city/city.component';
import { EditBeltComponent } from '@app/configurations/belt/edit-belt/edit-belt.component';
import { AddBeltComponent } from '@app/configurations/belt/add-belt/add-belt.component';
import { AddStateComponent } from '@app/configurations/state/add-state/add-state.component';
import { EditStateComponent } from '@app/configurations/state/edit-state/edit-state.component';
import { AddZoneComponent } from '@app/configurations/zone/add-zone/add-zone.component';
import { EditZoneComponent } from '@app/configurations/zone/edit-zone/edit-zone.component';
import { AddProvinceComponent } from '@app/configurations/province/add-province/add-province.component';
import { EditProvinceComponent } from '@app/configurations/province/edit-province/edit-province.component';
import { AddTerritoryComponent } from '@app/configurations/territory/add-territory/add-territory.component';
import { EditTerritoryComponent } from '@app/configurations/territory/edit-territory/edit-territory.component';
import { AddCityComponent } from '@app/configurations/city/add-city/add-city.component';
import { EditCityComponent } from '@app/configurations/city/edit-city/edit-city.component';
import { RegionComponent } from '@app/configurations/region/region.component';
import { EditRegionComponent } from '@app/configurations/region/edit-region/edit-region.component';
import { AddRegionComponent } from '@app/configurations/region/add-region/add-region.component';
import { SerachDropdownComponent } from '@app/configurations/city/serach-dropdown/serach-dropdown.component';
import { CountryComponent } from './country/country.component';
import { EditCountryComponent } from './country/edit-country/edit-country.component';
import { AddCountryComponent } from './country/add-country/add-country.component';







@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ConfigurationRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule.forRoot(),
    AgGridModule.withComponents([]),
    MatMomentDatetimeModule,
    MatDatetimepickerModule
  ],
  declarations: [
    OvertimeConfigurationComponent,
    EditOvertimeConfigurationComponent,
    EditHrConfigurationComponent,
    HrConfigurationComponent,
    BeltComponent,
    EditBeltComponent,
    AddBeltComponent,
    StateComponent,
    AddStateComponent,
    EditStateComponent,
    ZoneComponent,
    AddZoneComponent,
    EditZoneComponent,
    ProvinceComponent,
    AddProvinceComponent,
    EditProvinceComponent,
    TerritoryComponent,
    AddTerritoryComponent,
    EditTerritoryComponent,
    CityComponent,
    AddCityComponent,
    EditCityComponent,
    RegionComponent,
    EditRegionComponent,
    AddRegionComponent,
    SerachDropdownComponent,
    CountryComponent,
    EditCountryComponent,
    AddCountryComponent
  ]
})
export class ConfigurationsModule { }
