import { Component, OnInit, ViewChild, AfterViewInit,OnDestroy, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { StateDto, StateServiceProxy, CountryDto, CountryServiceProxy, Country } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';

///////////////////////////////////////////
//import { FormControl } from '@angular/forms';

import {Observable} from 'rxjs';
import {map, startWith, count} from 'rxjs/operators';
import { debug } from 'util';
///////////////////////////////////////////

///////////////////imports for validation////

import { FormBuilder, AbstractControl } from '@angular/forms';
import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';

////////////////////////////////////////////


@Component({
  selector: 'add-state',
  templateUrl: './add-state.component.html',
  providers: [StateServiceProxy, CountryServiceProxy]
})
export class AddStateComponent implements OnInit {


  @ViewChild('addStateModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  State: StateDto;// = new StateDto();
  public Country: CountryDto[];

  isActive: boolean;

  public countryName:string;
  
  public cid : number;
  /////////////////////////////

  validTextType: boolean = false; // for validating text
  validNumberType: boolean = false; // for validating number
  type : FormGroup;

  ///////////////////////



  constructor(injector: Injector,
    private stateService: StateServiceProxy,
    private countryService: CountryServiceProxy,

    private formBuilder: FormBuilder
  ) {
    
   }

  ngOnInit() {

    this.getAllCountries();
/*    
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | CountryDto >(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.Country.slice())
      );
  */
 this.initValidation();
}
 ////////////////////////////////code for form validation ///////////


 initValidation() {
  this.type = this.formBuilder.group({
    //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
    state_Name: [null, Validators.required],
   // sortOrder :[null, Validators.required],
    countryName : [null , Validators.required]
   
    
   })


 }



/*
 textValidationType(e){
  if (e) {
      this.validTextType = true;
  }else{
    this.validTextType = false;
  }
}


 isFieldValid(form: FormGroup, field: string) {
  return !form.get(field).valid && form.get(field).touched;
}

displayFieldCss(form: FormGroup, field: string) {
  return {
    'has-error': this.isFieldValid(form, field),
    'has-feedback': this.isFieldValid(form, field)
  };
}

*/

onType() {
  debugger;
 
  if (this.type.valid) {  
    this.save();
  } else {
     this.validateAllFormFields(this.type);
     
  }
}

/*
onType() {
  debugger;
  if (this.type.valid) {
    debugger;
    let a,p=0 ;
    for(a=0;a<this.Country.length;a++){
      if(this.State.countryId == this.Country[a].id) ///
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      this.validateAllFormFields(this.type);
     
    }
    
  } else {
     this.validateAllFormFields(this.type);
     
  }
}
*/

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
}


 ////////////////////////////////////////////////////////


  getAllCountries(): void {

    this.countryService.getAllCountries().subscribe((result) => {

      this.Country = result;
    
    })
  }


  show(): void {
    //debugger;
  
   debugger;
    this.active = true;
    this.modal.show();
    this.State = new StateDto();
    debugger;
    this.myControl = new FormControl();
    this.filteredOptions = new Observable<CountryDto[]>();

    this.countryDropdownSearch();

    this.type.markAsUntouched({onlySelf:true});

  
  }

  onShown():void{}


  /////////////////////////////
  ////////////////////////////////filter Method///////////////////////////


  myControl = new FormControl();
  
  filteredOptions: Observable<CountryDto[]>;


  public countryDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | CountryDto >(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filter(name) : this.Country.slice())
    ); 
      
/*    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | CountryDto >(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(abc => abc ? this._filter(abc) : this.Country.slice())
    );
*/
  }


  private _filter(value: string): CountryDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.Country.filter(option => option.name.toString().toLowerCase().includes(filterValue));
    
  }
  /* displayCountry(country: any): any  {
  
    let a;
    debugger;
    a = localStorage.getItem("counterr");
    
    if(a=="1"){
    localStorage.setItem("countryid", country.id);

    localStorage.setItem("counterr","0");
    return country.name;
    }
    else{}

   // this.State.countryId = country.id;
  }*/


  /*displayCountry = (country: CountryDto):any => {
    debugger;
    
    if(country.name!=null)
    {
      this.State.countryId = country.id;
      return country.name;
    }
    return country;

 }*/


 displayCountry = (country: CountryDto):any => {
  debugger;
  
  if(country instanceof CountryDto)
  {
    this.State.countryId = country.id;
    return country.name;
  }
  return country;

}

 


  ////////////////////////////////////////End of filter Method///////////////////////////


  save(): void {

   debugger;
   
   this.State.isActive = this.isActive;

    this.cid = this.State.countryId;
    //this.State.countryId = parseInt(localStorage.getItem("countryid"));
    this.saving = true;
    
    

   this.stateService.create(this.State)
      .finally(() => { this.saving = false; 
        
      })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
        
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

}
