import { Component, OnInit, ViewChild, AfterViewInit, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { StateDto, StateServiceProxy, CountryDto, CountryServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';

///////////////////imports for validation////

import { FormBuilder, AbstractControl } from '@angular/forms';
import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith, count} from 'rxjs/operators';

////////////////////////////////////////////

@Component({
  selector: 'edit-state',
  templateUrl: './edit-state.component.html',
  styleUrls: ['./edit-state.component.scss'],
  providers:[StateServiceProxy , CountryServiceProxy]
})
export class EditStateComponent implements OnInit {
  @ViewChild('editStateModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  State: StateDto = new StateDto();
  Country: CountryDto[];

  isActive: boolean;
  cName : string ;

  cId : number ;

  //validTextType: boolean = false; // for validating text
  
  type : FormGroup;


  constructor(injector: Injector,
    private stateService: StateServiceProxy,
    private countryService: CountryServiceProxy,
    private formBuilder :FormBuilder
  ) { }

  ngOnInit() {

    this.getAllCountries();
    this.initValidation();


  }

  getAllCountries(): void {

    this.countryService.getAllCountries().subscribe((result) => {

      this.Country = result;
    })
  }
 /////////////////////////////////filter Search/////////////////
 
  myControl = new FormControl();
  
  filteredOptions: Observable<CountryDto[]>;


 public countryDropdownSearch(){
  debugger;
 this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith<string | CountryDto >(''),
    map(value => typeof value === 'string' ? value : value.name),
    map(name => name ? this._filter(name) : this.Country.slice())
  );

  /*this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith(''),
    map(value => this._filter(value))
  );*/


}

private _filter(value: string): CountryDto[]{

  const filterValue = value.toLowerCase();

  return this.Country.filter(option => option.name.toString().toLowerCase().includes(filterValue));
  
}
    // using function expression//
 displayCountry = (country: CountryDto):any => {
   
  if(country instanceof CountryDto)
    {
      this.State.countryId = country.id;
      return country.name;
    }
    return country;

 }

////////////////////////////////////////////////////////////////////

initValidation() {
  this.type = this.formBuilder.group({
    //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
    state_Name: [null, Validators.required],
    //sortOrder :[null, Validators.required],
    countryName : [null , Validators.required]
       
   })

 }

 onType() {
 
  if (this.type.valid) {  
    this.save();
  } else {
     this.validateAllFormFields(this.type);
     
  }
}

validateAllFormFields(formGroup: FormGroup) {
  
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
}


///////////////////////////////////////////////////////////////////////

show(id: number): void {

    
    this.stateService.get(id).finally(() => {

      this.active = true;
      this.modal.show();

    }).subscribe((result: StateDto) => {
      this.State = result;
     
      this.countryService.get(result.countryId).subscribe((result : CountryDto)=>{
      
        this.cName = result.name; // to display country Name 
        
      })
       
    });
    this.countryDropdownSearch();
    this.type.markAsUntouched({onlySelf:true});
  }

  onShown():void{

  }


  save(): void {

    
    this.saving = true;
  
    this.stateService.update(this.State)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
       
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }


  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }
  

}
