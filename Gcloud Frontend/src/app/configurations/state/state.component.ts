import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddStateComponent } from './add-state/add-state.component';
import { EditStateComponent } from './edit-state/edit-state.component';
import { StateDto , StateServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}


@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss'],

  providers : [StateServiceProxy] 

})
export class StateComponent implements OnInit,AfterViewInit {

  @ViewChild('addStateModal') addStateModal: AddStateComponent;
  @ViewChild('editStateModal') editStateModal: EditStateComponent;



  constructor(private StateService: StateServiceProxy  ,private _router : Router) { }


  public dataTable: DataTable;
  State: StateDto[];
  data: string[] = [];

  globalFunction: GlobalFunctions = new GlobalFunctions
  ngOnInit() {
    if(!this.globalFunction.hasPermission("View","State")){
      this.globalFunction.showNoRightsMessage("View")
      this._router.navigate(['']);
    }
   
    this.getAllStates();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllStates() {

    this.StateService.getAllStates().subscribe((result) => {

      this.intializeDatatable();
      this.State = result;
      this.fillDatatable();

    });

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Country Name', 'State Name', 'SortOrder', 'Active', 'Comments', 'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }
  fillDatatable() {
    let i;
    for (i = 0; i < this.State.length; i++) {

      this.data.push(this.State[i].country.name)
      this.data.push(this.State[i].state_Name)
      this.data.push(this.State[i].sortOrder.toString())
      this.data.push(this.State[i].isActive.toString())

      this.data.push(this.State[i].comments)

      this.data.push(this.State[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }

  AddState(): void {
    if(!this.globalFunction.hasPermission("Create","State")){
      this.globalFunction.showNoRightsMessage("Create")
      return
    }
    
    this.addStateModal.show();
  }

  EditState(id: string): void {
    if(!this.globalFunction.hasPermission("Edit","State")){
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    
    this.editStateModal.show(parseInt(id));
  }

  protected delete(id: string): void {
    if(!this.globalFunction.hasPermission("Delete","State")){
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }
    
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.StateService.delete(parseInt(id))
          .finally(() => {
            this.getAllStates();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'State has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'State Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }
}
