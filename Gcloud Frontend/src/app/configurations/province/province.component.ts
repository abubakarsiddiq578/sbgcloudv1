import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddProvinceComponent } from './add-province/add-province.component';
import { EditProvinceComponent } from './edit-province/edit-province.component';
import {ProvinceDto, ProvinceServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-province',
  templateUrl: './province.component.html',
  styleUrls: ['./province.component.scss'],
  providers:[ProvinceServiceProxy]
})
export class ProvinceComponent implements OnInit, AfterViewInit {



  @ViewChild('addProvinceModal') addProvinceModal: AddProvinceComponent;
  @ViewChild('editProvinceModal') editProvinceModal: EditProvinceComponent;


  constructor(private provinceService:ProvinceServiceProxy ,private _router : Router) { }

  public dataTable:DataTable;
   Province:ProvinceDto[];
   data : string[] = [];


   globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Province")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate([''])
    }
    this.getAllProvince();
  }


  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllProvince(){

    this.provinceService.getAllProvinces().subscribe((result)=>{

      this.intializeDatatable();
      this.Province = result;
      this.fillDatatable();

    })

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Country Name', 'Province Name','Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.Province.length; i++) {

      this.data.push(this.Province[i].country.name)
   
      this.data.push(this.Province[i].name)
   
      this.data.push(this.Province[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddProvince(): void {
    if (!this.globalFunction.hasPermission("Create", "Province")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
    debugger;
    this.addProvinceModal.show();
   }
 
   EditProvince(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Province")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    this.editProvinceModal.show(parseInt(id));
   }
 


  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Province")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.provinceService.delete(parseInt(id))
          .finally(() => {
            this.getAllProvince();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Province has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Province Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

}
