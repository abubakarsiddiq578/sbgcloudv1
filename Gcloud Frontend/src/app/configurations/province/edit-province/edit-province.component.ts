import { Component, OnInit , Injector , ViewChild, ElementRef, EventEmitter, Output} from '@angular/core';
import {ProvinceDto, ProvinceServiceProxy, CountryServiceProxy, CountryDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-province',
  templateUrl: './edit-province.component.html',
  styleUrls: ['./edit-province.component.scss'],
  providers:[ProvinceServiceProxy,CountryServiceProxy]
})
export class EditProvinceComponent implements OnInit {

  
  @ViewChild('editProvinceModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  active: boolean = false;
  saving: boolean = false;

  Province: ProvinceDto = new ProvinceDto();

  Country: CountryDto[];

  _province : FormGroup

  myControl = new FormControl();

  filteredOptions: Observable<CountryDto[]>;

  public countryName : string ;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  
  constructor(injector:Injector,
    private provinceService : ProvinceServiceProxy,
    private countryService:CountryServiceProxy,
    private formBuilder : FormBuilder
    ) {


   }

  ngOnInit() {

    this.getAllCountries();
    this.initValidation();
  }


  initValidation() {


    this._province = this.formBuilder.group({
        countryId: [null, Validators.required],
        provinceName : [null , Validators.required]
    });

}

/*
onType() {

  if (this._province.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._province);
  }
}*/

onType() {
 
  if (this._province.valid) {
  
    let a,p=0 ;
    for(a=0;a<this.Country.length;a++){
      if(this.Province.countryId == this.Country[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.countryName = null; // null empName 
    
      this.validateAllFormFields(this._province);
      this.countryDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._province);
     
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}
//////////////////////////////////////////////////////////////////
///////////////////////////////Search Code//////////////////////////////////
 public countryDropdownSearch(){
  
  this.filteredOptions = this.myControl.valueChanges
   .pipe(
     startWith<string | CountryDto >(''),
     map(value => typeof value === 'string' ? value : value.name),
     map(name => name ? this._filter(name) : this.Country.slice())
   ); 
  
 }
 
 private _filter(value: string): CountryDto[]{
 
  
   const filterValue = value.toLowerCase();
 
   return this.Country.filter(option => option.name.toString().toLowerCase().includes(filterValue)); 
 }
 
 
 displayCountry = (country: CountryDto):any => {
   debugger;
   
   if(country instanceof CountryDto)
   {
       this.Province.countryId = country.id ;
       return country.name ;
   }
   this.Province.countryId = null;
   return country;
 
 }
///////////////////////////////////////////////////////////////////////////////////////


  //get all Countries

  getAllCountries(){

    this.countryService.getAllCountries().subscribe((result)=>{

        this.Country = result;
    });

  }





  show(id:number):void{

    this.provinceService.get(id).finally(()=>{

      this.active = true;
      this.modal.show();

    }).subscribe((result : ProvinceDto)=>{

      this.Province = result;
      this.countryService.get(result.countryId).subscribe((result)=>{
        this.countryName = result.name;
      });
    });

    this.countryDropdownSearch();
    this._province.markAsUntouched({onlySelf:true});
  }

  save(): void {

    this.saving = true;
  
    this.provinceService.update(this.Province)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
       
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }






  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
   this.active = false;
   this.modal.hide();

  }

  onShown():void{
    
  }
}
