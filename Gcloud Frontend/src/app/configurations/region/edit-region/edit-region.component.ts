import { Component, OnInit, ViewChild, AfterViewInit, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { RegionDto, RegionServiceProxy, ProvinceDto, ProvinceServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'edit-region',
  templateUrl: './edit-region.component.html',
  styleUrls: ['./edit-region.component.scss'],
  providers:[RegionServiceProxy,ProvinceServiceProxy]
})
export class EditRegionComponent implements OnInit {

  @ViewChild('editRegionModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  Region: RegionDto = new RegionDto();
  Province: ProvinceDto[];

  _region : FormGroup
  myControl = new FormControl();

  filteredOptions: Observable<ProvinceDto[]>;

  public provinceName : string ;

  constructor(injector: Injector,
    private provinceService: ProvinceServiceProxy,
    private regionService: RegionServiceProxy,
    private formBuilder : FormBuilder
  ) { }


  ngOnInit() {
  
   this.getAllProvince();
   this.initValidation();
  }


  initValidation() {


    this._region = this.formBuilder.group({
        provinceId: [null, Validators.required],
        regionName : [null , Validators.required]
    });

}
/*
onType() {

  if (this._region.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._region);
  }
}*/

onType() {
 
  if (this._region.valid) {
  
    let a,p=0 ;
    for(a=0;a<this.Province.length;a++){
      if(this.Region.provinceId == this.Province[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.provinceName = null; // null empName 
    
      this.validateAllFormFields(this._region);
      this.provinceDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._region);
     
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}
//////////////////////////////////////////////////////////////

/////////////////////////////Search Code//////////////////////////////////
public provinceDropdownSearch(){
  
  this.filteredOptions = this.myControl.valueChanges
   .pipe(
     startWith<string | ProvinceDto >(''),
     map(value => typeof value === 'string' ? value : value.name),
     map(name => name ? this._filter(name) : this.Province.slice())
   ); 
  
 }
 
 private _filter(value: string): ProvinceDto[]{
 
  
   const filterValue = value.toLowerCase();
 
   return this.Province.filter(option => option.name.toString().toLowerCase().includes(filterValue)); 
 }
 
 
 displayProvince = (province: ProvinceDto):any => {
   debugger;
   
   if(province instanceof ProvinceDto)
   {
       this.Region.provinceId = province.id ;
       return province.name ;
   }
   this.Region.provinceId= null;
   return province;
 
 }
///////////////////////////////////////////////////////////////////////////////////////

  getAllProvince() {

    this.provinceService.getAllProvinces().subscribe((result) => {

      this.Province = result;

    })
  }


  show(id : number): void{

    this.regionService.get(id).finally(()=>{

      this.active = true ;
      this.modal.show();

    }).subscribe((result : RegionDto)=>{

      this.Region = result;

    })



  }


  onShown(){

  }


  save(): void{


    this.saving = true;
    this.regionService.update(this.Region).finally(()=>{

        this.saving = false;
    }).subscribe(()=>{

      this.notify();
      this.close();
      this.modalSave.emit(null);

    })
    
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

}
