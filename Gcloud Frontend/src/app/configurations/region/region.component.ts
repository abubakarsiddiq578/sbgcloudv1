import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddRegionComponent } from './add-region/add-region.component';
import { EditRegionComponent } from './edit-region/edit-region.component';
import { RegionDto,RegionServiceProxy, ProvinceDto, ProvinceServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';


declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}


@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  providers:[RegionServiceProxy]
})
export class RegionComponent implements OnInit {


  @ViewChild('addRegionModal') addRegionModal: AddRegionComponent;
  @ViewChild('editRegionModal') editRegionModal: EditRegionComponent;

  public dataTable: DataTable;
  Region: RegionDto[];
  data: string[] = [];



  constructor(private regionService : RegionServiceProxy,private _router : Router) { }

  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
  if (!this.globalFunction.hasPermission("View", "Region")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate([''])
    }
    this.getAllRegions();
  }


  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllRegions() {

    this.regionService.getAllRegions().subscribe((result) => {

      this.intializeDatatable();
      this.Region = result;
      this.fillDatatable();

    });

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Province Name', 'Region Name', 'SortOrder', 'Active', 'Comments', 'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.Region.length; i++) {

      this.data.push(this.Region[i].province.name)
      this.data.push(this.Region[i].regionName)
      this.data.push(this.Region[i].sortOrder.toString())
      this.data.push(this.Region[i].isActive.toString())

      this.data.push(this.Region[i].comments)

      this.data.push(this.Region[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddRegion(): void {
    if (!this.globalFunction.hasPermission("Create", "Region")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
    
    this.addRegionModal.show();
  }

  EditRegion(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Region")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    
    this.editRegionModal.show(parseInt(id));
  }



  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Region")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.regionService.delete(parseInt(id))
          .finally(() => {
            this.getAllRegions();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Region has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Region Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }



}
