import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Injector } from '@angular/core';

import { CountryServiceProxy, CountryDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
@Component({
  selector: 'add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss'],
  providers: [CountryServiceProxy]
})
export class AddCountryComponent implements OnInit {

  @ViewChild('addCountryModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  country: CountryDto = new CountryDto();

  _country : FormGroup

  constructor(private countryService: CountryServiceProxy , private formBuilder : FormBuilder ) {

  }

  initValidation() {

    this._country = this.formBuilder.group({
        countryName: [null, Validators.required]
    });

}


  ngOnInit() {
    this.initValidation();
  }

  onType() {

    if (this._country.valid) {
        this.save();
    } else {
        this.validateAllFormFields(this._country);
    }
  }
  
  validateAllFormFields(formGroup: FormGroup) {
  
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
  }
  
  show(): void {

    this.active = true;
    this.modal.show();
    this.country = new CountryDto();
    this._country.markAsUntouched({onlySelf:true});
  }


  onShown(): void {

  }


  save(): void {

    debugger;
    this.saving = true;

    this.countryService.create(this.country)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }



  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }



}
