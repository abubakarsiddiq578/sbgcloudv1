import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Injector } from '@angular/core';
import { CountryServiceProxy, CountryDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
@Component({
  selector: 'edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.scss'],
  providers : [CountryServiceProxy]
})
export class EditCountryComponent implements OnInit {


  @ViewChild('editCountryModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;


  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  country: CountryDto = new CountryDto();

  _country: FormGroup

  constructor(Injector: Injector,
    private countryService: CountryServiceProxy,
    private formBuilder : FormBuilder
  ) { 

  }

  ngOnInit() {
    this.initValidation();
  }
  
  initValidation() {

    this._country = this.formBuilder.group({
        countryName: [null, Validators.required]
    });

}

onType() {

  if (this._country.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._country);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}






  show(id:number):void{

    this.countryService.get(id).finally(()=>{

      this.active = true;
      this.modal.show();

    }).subscribe((result : CountryDto)=>{

      this.country = result;
    })

  }

  save(): void {

    this.saving = true;
  
    this.countryService.update(this.country)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
       
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

  onShown():void{
    
  }

}
