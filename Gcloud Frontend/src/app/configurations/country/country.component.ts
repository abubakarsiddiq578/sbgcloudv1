import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddCountryComponent } from './add-country/add-country.component';
import { EditCountryComponent } from './edit-country/edit-country.component';
import { CountryServiceProxy, CountryDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss'],
  providers:[CountryServiceProxy]
})


export class CountryComponent implements OnInit, AfterViewInit {


  @ViewChild('addCountryModal') addCountryModal: AddCountryComponent;
  @ViewChild('editCountryModal') editCountryModal: EditCountryComponent;

  constructor(private countryService:CountryServiceProxy,
    private _router : Router) {


   }

   public dataTable:DataTable;
   Country:CountryDto[];
   data : string[] = [];

  globalFuntion : GlobalFunctions = new GlobalFunctions;
  ngOnInit() {
      if(!this.globalFuntion.hasPermission("View", "Country")){
        this.globalFuntion.showNoRightsMessage("View")
        this._router.navigate(['']);
      }
    this.getAllCountries();

  }
  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllCountries(){

    this.countryService.getAllCountries().subscribe((result)=>{

      this.intializeDatatable();
      this.Country = result;
      this.fillDatatable();

    })

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Name','Actions'],
      footerRow: [/* 'Code', 'Name', 'Sort Order', 'Remarks', 'Actions'*/],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.Country.length; i++) {

      this.data.push(this.Country[i].name)
   
      this.data.push(this.Country[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddCountry(): void {
    if(!this.globalFuntion.hasPermission("Create","Country")){
        this.globalFuntion.showNoRightsMessage("Create")
      return
      
    }
   this.addCountryModal.show();
  }

  EditCountry(id: string): void {
    if(!this.globalFuntion.hasPermission("Edit","Country")){
          this.globalFuntion.showNoRightsMessage("Edit")
          return
        }
   this.editCountryModal.show(parseInt(id));
  }

  protected delete(id: string): void {
    if(!this.globalFuntion.hasPermission("Delete","Country")){
      this.globalFuntion.showNoRightsMessage("Delete")
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.countryService.delete(parseInt(id))
          .finally(() => {
            this.getAllCountries();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Country has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Country Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }
}
