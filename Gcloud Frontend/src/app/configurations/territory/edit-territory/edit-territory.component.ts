import { Component, OnInit, ViewChild, AfterViewInit, EventEmitter, ElementRef, Output, Injector } from '@angular/core';
import { TerritoryServiceProxy, TerritoryDto, CityDto, CityServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

////////////////imports for filter //
import {Observable} from 'rxjs';
import {map, startWith, count} from 'rxjs/operators';

@Component({
  selector: 'edit-territory',
  templateUrl: './edit-territory.component.html',
  styleUrls: ['./edit-territory.component.scss'],
  providers: [TerritoryServiceProxy, CityServiceProxy]
})
export class EditTerritoryComponent implements OnInit {

  @ViewChild('editTerritoryModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  Territory: TerritoryDto = new TerritoryDto();

  City: CityDto[];
  isActive: boolean;

  _territory: FormGroup;

  public cityName  : string;
  myControl = new FormControl();
  
  filteredOptions: Observable<CityDto[]>;
  

  constructor(injector: Injector,
    private territoryService: TerritoryServiceProxy,
    private cityService: CityServiceProxy,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.getAllCity();
    this.initValidation();
  }


  initValidation() {

    this._territory = this.formBuilder.group({
      //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      cityId: [null, Validators.required],
      territoryName: [null, Validators.required]
    });

  }



  getAllCity() {

    this.cityService.getAllCities().subscribe((result) => {

      this.City = result;

    })

  }
/*  onType() {
    debugger;
    if (this._territory.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._territory);

    }
  }*/

  onType() {
 
    if (this._territory.valid) {
    
      let a,p=0 ;
      for(a=0;a<this.City.length;a++){
        if(this.Territory.cityId == this.City[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.cityName = null; // null empName 
      
        this.validateAllFormFields(this._territory);
        this.cityDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._territory);
       
    }
  }
  

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ////////////////////////////////filter Method///////////////////////////

public cityDropdownSearch(){
  debugger;
  this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith<string | CityDto >(''),
    map(value => typeof value === 'string' ? value : value.cityName),
    map(a => this.cityName ? this._filter(a) : this.City.slice())
  );

  /*this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => value ?this._filter(value) : this.City.slice())
    ); */
}


displayCity = (City: CityDto):any => {
  debugger;
  
  if(City instanceof CityDto)
  {
    this.Territory.cityId = City.id;
    return City.cityName;
  }
  return City;

}



private _filter(value: string): CityDto[]{

  debugger;
  const filterValue = value.toLowerCase();

  return this.City.filter(option => option.cityName.toString().toLowerCase().includes(filterValue));
  
}


//////////////////////////////////////End of Filter Method//////////////////////////////////////////


  show(id: number): void {

    this.territoryService.get(id).finally(() => {

      this.active = true;
      this.modal.show();


    }).subscribe((result: TerritoryDto) => {

      this.Territory = result;

    })

  }



  onShown(): void {


  }




  save(): void {


    this.saving = true;
    this.territoryService.update(this.Territory).finally(() => {

      this.saving = false;
    }).subscribe(() => {

      this.notify();
      this.close();
      this.modalSave.emit(null);

    })

  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }


}
