import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddTerritoryComponent } from './add-territory/add-territory.component';
import { EditTerritoryComponent } from './edit-territory/edit-territory.component';
import { TerritoryServiceProxy, TerritoryDto} from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}


@Component({
  selector: 'app-territory',
  templateUrl: './territory.component.html',
  styleUrls: ['./territory.component.scss'],
  providers:[TerritoryServiceProxy]
})
export class TerritoryComponent implements OnInit, AfterViewInit {


  @ViewChild('addTerritoryModal') addTerritoryModal: AddTerritoryComponent;
  @ViewChild('editTerritoryModal') editTerritoryModal: EditTerritoryComponent;

  public dataTable: DataTable;
  Territory: TerritoryDto[];
  data: string[] = [];



  constructor(private terrritoryService : TerritoryServiceProxy ,private _router : Router) { }

  globalFunction: GlobalFunctions = new GlobalFunctions
 
  ngOnInit() {
    if(!this.globalFunction.hasPermission("View","Territory")){
      this.globalFunction.showNoRightsMessage("View")
      this._router.navigate(['']);
    }

    this.getAllTerritory();
  }


  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllTerritory(){

    this.terrritoryService.getAllTerritories().subscribe((result)=>{

        this.intializeDatatable()
        this.Territory = result;
        this.fillDatatable();
    })

  }

  intializeDatatable() {

    this.dataTable = {
      headerRow: ['City Name', 'Territory Name', 'SortOrder', 'Active', 'Comments', 'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.Territory.length; i++) {

      this.data.push(this.Territory[i].city.cityName)
      this.data.push(this.Territory[i].territoryName)
      this.data.push(this.Territory[i].sortOrder.toString())
      this.data.push(this.Territory[i].isActive.toString())

      this.data.push(this.Territory[i].comments)

      this.data.push(this.Territory[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddTerritory(): void {
    if(!this.globalFunction.hasPermission("Create","Territory")){
      this.globalFunction.showNoRightsMessage("Create")
      return
    }
    debugger;
    this.addTerritoryModal.show();
  }

  EditTerritory(id: string): void {
    if(!this.globalFunction.hasPermission("Edit","Territory")){
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    
    this.editTerritoryModal.show(parseInt(id));
  }


  protected delete(id: string): void {
    if(!this.globalFunction.hasPermission("Delete","Territory")){
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.terrritoryService.delete(parseInt(id))
          .finally(() => {
            this.getAllTerritory();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Territory has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Territory Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }


}
