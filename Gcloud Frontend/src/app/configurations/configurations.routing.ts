import { Routes } from '@angular/router';

import { AppRouteGuard } from '@app/shared/auth/auth-route-guard';
import { HrConfigurationComponent } from '@app/configurations/hr-configuration/hr-configuration.component';
import { OvertimeConfigurationComponent } from '@app/configurations/overtime-configuration/overtime-configuration.component';
import { BeltComponent } from './belt/belt.component';
import { CityComponent } from './city/city.component';
import { RegionComponent } from './region/region.component';
import { ZoneComponent } from './zone/zone.component';
import { TerritoryComponent } from './territory/territory.component';
import { ProvinceComponent } from './province/province.component';
import { StateComponent } from './state/state.component';
import { CountryComponent } from './country/country.component';


export const ConfigurationRoutes: Routes = [
    {
        path: '',
        children: [{
          path: 'Overtime',
          component: OvertimeConfigurationComponent,
          canActivate: [AppRouteGuard]
      }]
    },

    {
        path: '',
        children: [{
          path: 'Hr',
          component: HrConfigurationComponent,
          canActivate: [AppRouteGuard]
      }]
    },
    {
        path: '',
        children: [{
          path: 'Belt',
          component: BeltComponent,
          canActivate: [AppRouteGuard]
      }]
    },
    {
        path: '',
        children: [{
          path: 'City',
          component: CityComponent,
          canActivate: [AppRouteGuard]
      }]
    },
    {
        path: '',
        children: [{
          path: 'Country',
          component: CountryComponent,
          canActivate: [AppRouteGuard]
      }]
    },
    {
        path: '',
        children: [{
          path: 'Region',
          component: RegionComponent,
          canActivate: [AppRouteGuard]
      }]
    }
    ,
    {
        path: '',
        children: [{
          path: 'Zone',
          component: ZoneComponent,
          canActivate: [AppRouteGuard]
      }]
    }
    ,
    {
        path: '',
        children: [{
          path: 'Territory',
          component: TerritoryComponent,
          canActivate: [AppRouteGuard]
      }]
    }
    ,
    {
        path: '',
        children: [{
          path: 'Province',
          component: ProvinceComponent,
          canActivate: [AppRouteGuard]
      }]
    }
    ,
    {
        path: '',
        children: [{
          path: 'State',
          component: StateComponent,
          canActivate: [AppRouteGuard]
      }]
    }

]