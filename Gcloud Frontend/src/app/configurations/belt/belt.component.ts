import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddBeltComponent } from './add-belt/add-belt.component';
import { EditBeltComponent } from './edit-belt/edit-belt.component';
import { BeltDto, BeltServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'app-belt',
  templateUrl: './belt.component.html',
  styleUrls: ['./belt.component.scss'],
  providers: [BeltServiceProxy]
})
export class BeltComponent implements OnInit, AfterViewInit {


  
  @ViewChild('addBeltModal') addBeltModal: AddBeltComponent;
  @ViewChild('editBeltModal') editBeltModal: EditBeltComponent;





  constructor(private beltService: BeltServiceProxy , private _router: Router) { }
  public dataTable: DataTable;
  Belt: BeltDto[];
  data: string[] = [];
  globalFunction: GlobalFunctions = new GlobalFunctions


  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Belt")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);
    }
    this.getAllBelt();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllBelt() {

    this.beltService.getAllBelts().subscribe((result) => {

      this.intializeDatatable();
      this.Belt = result;
      this.fillDatatable();

    });

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Zone Name', 'Belt Name', 'SortOrder', 'Active', 'Comments', 'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.Belt.length; i++) {

      this.data.push(this.Belt[i].zone.zoneName)
      this.data.push(this.Belt[i].belt_Name)
      this.data.push(this.Belt[i].sortOrder.toString())
      this.data.push(this.Belt[i].isActive.toString())

      this.data.push(this.Belt[i].comments)

      this.data.push(this.Belt[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddBelt(): void {
    if (!this.globalFunction.hasPermission("Create", "Belt")) {
      this.globalFunction.showNoRightsMessage("Create")
      return
    }

    this.addBeltModal.show();
  }

  EditBelt(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Belt")) {
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    this.editBeltModal.show(parseInt(id));
  }



  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Belt")) {
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.beltService.delete(parseInt(id))
          .finally(() => {
            this.getAllBelt();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Belt has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Belt Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }



}
