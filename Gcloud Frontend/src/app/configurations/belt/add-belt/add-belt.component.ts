import { Component, OnInit, Injector, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { BeltDto, BeltServiceProxy, ZoneDto, ZoneServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
@Component({
  selector: 'add-belt',
  templateUrl: './add-belt.component.html',
  styleUrls: ['./add-belt.component.scss'],
  providers: [BeltServiceProxy, ZoneServiceProxy]
})
export class AddBeltComponent implements OnInit {


  @ViewChild('addBeltModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  Belt: BeltDto = new BeltDto();

  Zone: ZoneDto[];

  isActive: boolean;
  _belt : FormGroup; // for form validation 

  myControl = new FormControl();

  filteredOptions: Observable<ZoneDto[]>;

  public zoneName : string ;

  constructor(injector: Injector,
    private beltService: BeltServiceProxy,
    private zoneService: ZoneServiceProxy,
    private formBuilder : FormBuilder

  ) { }

  ngOnInit() {
    this.getAllZone();
    this.initValidation();
  }


  initValidation() {

    this._belt = this.formBuilder.group({
        zoneId: [null, Validators.required],
        beltName : [null , Validators.required]
    });

}

//check form validation if it is valid then save it else throw error message in form //
/*onType() {
 
  debugger;
  if (this._belt.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._belt);
  }
}*/
onType() {
  debugger;
  if (this._belt.valid) {
    debugger;
    let a,p=0 ;
    for(a=0;a<this.Zone.length;a++){
      if(this.Belt.zoneId == this.Zone[a].id) ///
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.zoneName = null;
      
      this.validateAllFormFields(this._belt);
      this.zoneDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._belt);
     
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}
///////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
public zoneDropdownSearch(){
  debugger;
 this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith<string | ZoneDto >(''),
    map(value => typeof value === 'string' ? value : value.zoneName),
    map(name => name ? this._filter(name) : this.Zone.slice())
  ); 
 
}

private _filter(value: string): ZoneDto[]{

  debugger;
  const filterValue = value.toLowerCase();

  return this.Zone.filter(option => option.zoneName.toString().toLowerCase().includes(filterValue)); 
}


displayZone = (zone: ZoneDto):any => {
  debugger;
  
  if(zone instanceof ZoneDto)
  {
      this.Belt.zoneId = zone.id ;
      return zone.zoneName ;
  }
  this.Belt.zoneId= null; //to keep it null for first selected value and then pass unknow zone //
  return zone;

}

/////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////

  getAllZone() {

    this.zoneService.getAllZones().subscribe((result) => {

      this.Zone = result;
    })

  }

  show(): void {

    this.isActive = true;
    this.active = true;
    this.modal.show();
    this.Belt = new BeltDto();
    
    this.zoneName = null;
    this.zoneDropdownSearch();
    this._belt.markAsUntouched({onlySelf:true}); // to untouched the required form field // 
  }



  save(): void {

    this.Belt.isActive = this.isActive;
    this.saving = true;

    this.beltService.create(this.Belt)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

  onShown():void{
    
  }

}
