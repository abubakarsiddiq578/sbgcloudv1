import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOvertimeConfigurationComponent } from './edit-overtime-configuration.component';

describe('EditOvertimeConfigurationComponent', () => {
  let component: EditOvertimeConfigurationComponent;
  let fixture: ComponentFixture<EditOvertimeConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOvertimeConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOvertimeConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
