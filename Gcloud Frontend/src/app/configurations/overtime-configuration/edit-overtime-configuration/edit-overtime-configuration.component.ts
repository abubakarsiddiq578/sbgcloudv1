import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';


import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { OvertimeConfigurationServiceProxy, OvertimeConfigurationDto } from '@app/shared/service-proxies/service-proxies';
import { calendar } from 'ngx-bootstrap/chronos/moment/calendar';
import { calendarFormat } from 'moment';



@Component({
  selector: 'app-edit-overtime-configuration',
  templateUrl: './edit-overtime-configuration.component.html',
  styleUrls: ['./edit-overtime-configuration.component.scss'],
  providers: [OvertimeConfigurationServiceProxy]
})
export class EditOvertimeConfigurationComponent implements OnInit {

  @ViewChild('editOvertimeConfigurationModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  active: boolean = false;
  saving: boolean = false;
  overtimeConfiguration: OvertimeConfigurationDto = new OvertimeConfigurationDto();
  weekDays : string[]  = ["Monday", "Tuesday", "Wednesday" ,"Thrusday" ,"Friday" , "Saturday" , "Sunday"];
  savedDays : string[] //= ["Monday","Tues"];


  constructor(injector: Injector,
    // private _userService: UserServiceProxy,
    private _overtimeConfigurationService: OvertimeConfigurationServiceProxy) { }


  show(id: number): void {
    this._overtimeConfigurationService.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();
      })
      .subscribe((result: OvertimeConfigurationDto) => {
        this.overtimeConfiguration = result;
        this.showSelected();
      })
  }




  showSelected(){

    //this.overtimeConfiguration.workingDays;
    let p=0;
     this.savedDays = [];
     debugger;
     debugger;
     this.savedDays = this.overtimeConfiguration.workingDays.split(",");

  }

  SavingWorkingDays( wrkdys : string[]){

    let a = 0;
    
    this.overtimeConfiguration.workingDays = "";
    
    for(a=0; a<wrkdys.length; a++){


      this.overtimeConfiguration.workingDays = this.overtimeConfiguration.workingDays + wrkdys[a] + "," ;

    }

    //this.overtimeConfiguration.workingDays.TrimEnd(',');
    this.overtimeConfiguration.workingDays = this.overtimeConfiguration.workingDays.slice(0,-1);
  }

  save(): void {
    debugger;

    
    this.SavingWorkingDays(this.savedDays);

    this.saving = true;
    this._overtimeConfigurationService.update(this.overtimeConfiguration)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        // this.notify.info(this.l('Record Updated Successfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }


  ngOnInit() {
  }

  onShown():void{
  }

}
