import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { ChartOfAccountRoutes } from './chartOfAccount.routing';
import { ModalModule } from 'ngx-bootstrap';

import { mainAccountComponent } from './mainAccount/main-account.component';
import {AddMainAccountComponent} from './mainAccount/add-main-account/add-main-account.component';
import {EditMainAccountComponent} from './mainAccount/edit-main-account/edit-main-account.component';

import {SubSubMainComponent} from './sub-sub-main/sub-sub-main.component';
import {AddSubSubMainComponent} from './sub-sub-main/add-sub-sub-main/add-sub-sub-main.component';
import {EditSubSubMainComponent} from './sub-sub-main/edit-chart-account-sub-sub-main/edit-chart-account-sub-sub-main.component';
import { AddSubSubDetailComponent } from './sub-sub-detail/add-sub-sub-detail/add-sub-sub-detail.component';
import { SubSubDetailComponent } from './sub-sub-detail/sub-sub-detail.component';
import { EditSubSubDetailComponent } from './sub-sub-detail/edit-sub-sub-detail/edit-sub-sub-detail.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ChartOfAccountRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule.forRoot()
  ],
  declarations: [
    mainAccountComponent,
    AddMainAccountComponent,
    EditMainAccountComponent,



    SubSubMainComponent,
    AddSubSubMainComponent,
    EditSubSubMainComponent,

    AddSubSubDetailComponent,
    SubSubDetailComponent,
    EditSubSubDetailComponent,
  ]
})

export class ChartOfAccount {}
