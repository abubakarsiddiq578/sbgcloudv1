import { Routes } from '@angular/router';
import { mainAccountComponent } from './mainAccount/main-account.component';
import {SubSubMainComponent} from './sub-sub-main/sub-sub-main.component';
import { SubSubDetailComponent } from './sub-sub-detail/sub-sub-detail.component';

export const ChartOfAccountRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'MainAccount',
        component: mainAccountComponent
    }]},
    {
        path: '',
        children: [ {
          path: 'SubSubMain',
          component: SubSubMainComponent
      }]},
    {
        path: '',
        children: [ {
          path: 'SubSubDetail',
          component: SubSubDetailComponent
      }]}
];  