import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { ChartOfAccountSubSubMainServiceProxy, ChartOfAccountSubSubMainDto, PagedResultDtoOfChartOfAccountSubSubDetailDto, PagedResultDtoOfChartOfAccountSubSubMainDto,  ChartOfAccountSubSubDetailDto, ChartOfAccountSubMainServiceProxy, ChartOfAccountSubSubDetailServiceProxy } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { PagedRequestDto } from '@shared/paged-listing-component-base';
// import { subscribeOn } from '../../../../node_modules/rxjs/operator/subscribeOn';



@Component({
  selector: 'app-add-chart-account-sub-sub-detail',
  templateUrl: './add-sub-sub-detail.component.html',
  styleUrls: ['./add-sub-sub-detail.component.css']
})
// export class AddSubSubDetailComponent extends AppComponentBase implements OnInit {
export class AddSubSubDetailComponent implements OnInit {

  @ViewChild('addChartAccountSubSubDetailModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();


  active: boolean = false;
  saving: boolean = false;
  // coaSubSubDetailAccount: ChartOfAccountSubSubDetailDto = new ChartOfAccountSubSubDetailDto();
  // chartOfSubSubMainAccount: ChartOfAccountSubSubMainDto = new ChartOfAccountSubSubMainDto();
  CoASubSubDDList = [];
  companyDDList = [];
  bsNoteDDList = [];
  plNoteDDList = [];
  subSubDetailAcc: any[];
 

    //start declaring variables for getting auto increment code
    AllSubSubAccount = [];
    code  = [];
    // _abc: abc[] = [];
    subcodesList = [];
    //ending declaring variables for getting auto increment code


  constructor(injector: Injector, 
      // private _addChartAccountSubSubDetailService: ChartOfAccountSubSubDetailServiceProxy,
      // private _chartaccountsubsubMainService: ChartOfAccountSubSubMainServiceProxy  
    ) { 
      //super(injector);
    }

  ngOnInit() {


  //   this._chartaccountsubsubMainService.getAllAccount()
  //    .subscribe((result) => {
  //      this.CoASubSubDDList = result;
  //  });

  //   //start getting dropdown list for chart of account main 
  //   this._addChartAccountSubSubDetailService.getAllAccount()
  //   .subscribe((result) => {
  //     this.subSubDetailAcc = result;
  // });

  // //getting all the values from sub sub account
  // this._addChartAccountSubSubDetailService.getAllAccount().subscribe((result) => {
  //   this.AllSubSubAccount = result;
    
  
  // })


  }
  show(): void {
    this.active = true; 
    this.modal.show();
    // this.coaSubSubDetailAccount = new ChartOfAccountSubSubDetailDto();
    // this.coaSubSubDetailAccount.init({ isActive: true });
}


track(/*id:number*/): void {
// this._chartaccountsubsubMainService.get(id)
//     .finally(() => {
//         this.active = true; 
//         this.modal.show();
//     })
//     .subscribe((result: ChartOfAccountSubSubMainDto ) => {
//         // this.chartOfMainAccDDList = result;

//         this.coaSubSubDetailAccount.mainSubSubId = result.id; 
//         this.onChartSubChange()
//     });
this.active = true;
this.modal.show();
}


onShown(): void {
  //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
}

// save(): void {
//   //TODO: Refactor this, don't use jQuery style code
//   debugger
// this.saving = true;
//   this._addChartAccountSubSubDetailService.create(this.coaSubSubDetailAccount)
//       .finally(() => { this.saving = false; })
//       .subscribe(() => {
//           this.notify.info(this.l('Saved Successfully'));
//           this.close();   
//           this.modalSave.emit(null);
//       });
// }

close(): void {
  this.active = false;
  this.modal.hide();
}


// onChartSubChange(){
  
//   let selectedSubAcc = this.CoASubSubDDList.find(m => m.id == this.coaSubSubDetailAccount.mainSubSubId);

//   //make an api call to get all sub accounts agains selectedMainAcc
// this.subcodesList = this.AllSubSubAccount.filter(a => a.mainSubSubId == this.coaSubSubDetailAccount.mainSubSubId);
// console.log("All the sub sub codes:", this.subcodesList);
// let i = 0;
// for (i= 0; i < this.subcodesList.length; i++) {
//   let abc_;
//   abc_ = new abc();
//   abc_.code = this.subcodesList[i].detailCode
//   this._abc.push(abc_);
// }
// console.log("List of All Sub Sub Codes:", this._abc);
// let code:any;
// if(this._abc.length == 0)
// {
//   code = "1";
//   this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_sub_code +"-00"+ code ;
// }
// else
// {
//   if(this._abc[this._abc.length - 1].code != null)
//       code = this._abc[this._abc.length - 1].code;
//       let splittedcode = parseInt(code.split('-')[2]);
//       splittedcode++
//         if(splittedcode <= 9)
//             this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_sub_code  +"-00"+ splittedcode ;
//         else if( splittedcode <= 99)
//             this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_sub_code  +"-0"+ splittedcode ;
//         else if(splittedcode < 999)
//             this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_sub_code  +"-"+ splittedcode ;
// }
// this._abc = [];
// }
}
// class for storing all the codes against selected account
// class abc{
//   public code: string;
// }