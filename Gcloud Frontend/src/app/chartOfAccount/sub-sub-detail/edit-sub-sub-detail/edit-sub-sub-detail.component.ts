import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { ChartOfAccountSubSubMainServiceProxy, ChartOfAccountSubSubMainDto, ChartOfAccountSubSubDetailServiceProxy, ChartOfAccountSubSubDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-edit-chart-account-sub-sub-detail',
  templateUrl: './edit-sub-sub-detail.component.html',
  styleUrls: ['./edit-sub-sub-detail.component.css'],
  providers: [/*ChartOfAccountSubSubDetailServiceProxy*/]
})
// export class EditChartAccountSubSubDetailComponent extends AppComponentBase implements OnInit {
export class EditSubSubDetailComponent  implements OnInit {

  @ViewChild('editChartAccountSubSubDetailModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  // coaSubSubDetailAccount: ChartOfAccountSubSubDetailDto = new ChartOfAccountSubSubDetailDto();
  // chartOfSubSubMainAccount: ChartOfAccountSubSubMainDto = new ChartOfAccountSubSubMainDto();
  CoASubSubDDList = [];
  companyDDList = [];
  bsNoteDDList = [];
  plNoteDDList = [];
  subSubDetailAcc: any[];
  // subMainAccRecList: any = {};
  // checkVal: any[];

  AllSubAccount = [];
  code  = [];
  _abc: abc[] = [];
  subcodesList=[];
  splitsubCode;
  codecode;



  constructor(injector: Injector,
    // private _chartAccountSubSubDetailService: ChartOfAccountSubSubDetailServiceProxy,
    // private _chartAccountSubSubMainService: ChartOfAccountSubSubMainServiceProxy
  ) { 
    //super(injector);
  }

  ngOnInit() {

    //start getting dropdown list for chart of account main 
//     this._chartAccountSubSubDetailService.getAll(0, 1000)
//     .subscribe((result) => {
//     this.CoASubSubDDList = result.items;
// });

// this._chartAccountSubSubMainService.getAllAccount()
// .subscribe((result)=>{
//   this.subSubDetailAcc = result; 
// })

// //getting all the values from chart of account sub 
// this._chartAccountSubSubDetailService.getAllAccount().subscribe((result) => {
//   this.AllSubAccount = result;

// })

  }
  show(/*id:number*/): void {

  // this._chartAccountSubSubDetailService.get(id)
  //     .finally(() => {
  //         this.active = true;
  //         this.modal.show();
  //     })
  //     .subscribe((result: ChartOfAccountSubSubDetailDto) => {
  //         this.coaSubSubDetailAccount = result;
  //         this.codecode = this.coaSubSubDetailAccount.detailCode;
  //         this.splitsubCode = parseInt(this.coaSubSubDetailAccount.detailCode.split('-')[3]);
  //         if(this.splitsubCode <= 9){
  //           this.coaSubSubDetailAccount.detailCode = "00" + this.splitsubCode.toString();
  //         }
  //         else if(this.splitsubCode <= 99)
  //         {
  //           this.coaSubSubDetailAccount.detailCode = "0" + this.splitsubCode.toString();
  //         }
  //         else if(this.splitsubCode <= 999)
  //         {
  //           this.coaSubSubDetailAccount.detailCode = ""+ this.splitsubCode.toString();
  //         }
  //     });
  this.active = true;
  this.modal.show();
}

onShown(): void {
  //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
}

// save(): void {
//   this.saving = true;

//   this.coaSubSubDetailAccount.detailCode = this.codecode.split('-')[3] + "-" + this.coaSubSubDetailAccount.detailCode;
//   this.subcodesList = this.AllSubAccount.filter(a => a.mainSubSubId == this.coaSubSubDetailAccount.mainSubSubId);
//   let i = 0;
//   for (i= 0; i < this.subcodesList.length; i++) {
//     let abc_;
//     abc_ = new abc();
//     abc_.code = this.subcodesList[i].sub_sub_code
//     this._abc.push(abc_);
//   }
//   let j = 0;
//   let ab : boolean = true;
//   for(j = 0; j< this._abc.length; j++)
//   {
//     if(this.coaSubSubDetailAccount.detailCode == this._abc[j].code)
//     {
//       console.log("This code is already exist.");
//       ab = false;
//     }
//     if(ab == true)
//     {
//       this._chartAccountSubSubDetailService.update(this.coaSubSubDetailAccount)
//     .finally(() => {this.saving = false;})
//     .subscribe(() => {
//       this.notify.info(this.l('Record Updated Successfully'));
//       this.close();
//       this.modalSave.emit(null);
//     });
//     }
//  }

  
//}


close(): void {
  this.active = false;
  this.modal.hide();
}


// onChartSubChange(){
//  // var selectedMainAcc = this.subMainAcc.find(m => m.chartOfAccMainID == this.chartOfSubAccount.chartOfAccMainID);
//  let selectedSubAcc = this.CoASubSubDDList.find(m => m.id == this.coaSubSubDetailAccount.mainSubSubId);

// //make an api call to get all sub accounts agains selectedMainAcc
// this.subcodesList = this.AllSubAccount.filter(a => a.mainSubSubId == this.coaSubSubDetailAccount.mainSubSubId);
// let i = 0;
// for (i= 0; i < this.subcodesList.length; i++) {
//   let abc_;
//   abc_ = new abc();
//   abc_.code = this.subcodesList[i].sub_sub_code
//   this._abc.push(abc_);
// }
// debugger
// console.log("Codes:", this._abc);

// let code:any;

// if(this._abc.length == 0)
// {
//   code = "1";
//   if(code <= 9)
//         this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_sub_code +"-00"+ code ;
//   else if( code <= 99)
//         this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_sub_code +"-0"+ code ;
//   else if(code < 999)
//         this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_sub_code +"-"+ code ;
// }
// else
// {
//   if(this._abc[this._abc.length - 1].code != null)
//       code = this._abc[this._abc.length - 1].code;
//       let splittedcode = parseInt(code.split('-')[3]);
//       splittedcode++
//         if(splittedcode <= 9)
//             this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_code +"-00"+ splittedcode ;
//         else if( splittedcode <= 99)
//             this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_code +"-0"+ splittedcode ;
//         else if(splittedcode < 999)
//             this.coaSubSubDetailAccount.detailCode = selectedSubAcc.sub_code +"-"+ splittedcode ;
// }
// this._abc = [];


// }

}
class abc{
  public code: string;
}