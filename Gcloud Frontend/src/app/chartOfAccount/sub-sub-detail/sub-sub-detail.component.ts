import { Component, OnInit, ViewChild, Injector, AfterViewInit } from '@angular/core';
import { AddSubSubDetailComponent } from './add-sub-sub-detail/add-sub-sub-detail.component';
import { EditSubSubDetailComponent } from './edit-sub-sub-detail/edit-sub-sub-detail.component';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
//import { ChartOfAccountSubSubDetailServiceProxy, PagedResultDtoOfChartOfAccountSubSubDetailDto, ChartOfAccountSubSubDetailDto, ChartOfAccountSubSubMainServiceProxy } from '@shared/service-proxies/service-proxies';
// import { PagedRequestDto } from '@shared/paged-listing-component-base';
// import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
// import { Subject } from 'rxjs/Subject';
// import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-chart-account-sub-sub-detail',
  templateUrl: './sub-sub-detail.component.html',
  styleUrls: ['./sub-sub-detail.component.css'],
  animations: [/*appModuleAnimation()*/
  // providers: [ChartOfAccountSubSubDetailServiceProxy,
  //   ChartOfAccountSubSubMainServiceProxy
  ]

})
// export class SubSubDetailComponent extends PagedListingComponentBase<ChartOfAccountSubSubDetailDto> {
export class SubSubDetailComponent implements OnInit {

  @ViewChild('addChartAccountSubSubDetailModal') public addChartAccountSubSubDetailModal: AddSubSubDetailComponent;
  @ViewChild('editChartAccountSubSubDetailModal') editChartAccountSubSubDetailModal: EditSubSubDetailComponent;

  // result: ChartOfAccountSubSubDetailDto[]=[];
  // IsEdit: boolean = abp.auth.isGranted("Pages.Administration.Edit_Samples");;
  // IsDelete: boolean = abp.auth.isGranted("Pages.Administration.Delete_Samples");
  // IsCreate: boolean = abp.auth.isGranted("Pages.Administration.Create_Samples");

  // dtOptions: DataTables.Settings = {};  
  // dtTrigger: Subject<any> = new Subject();



  constructor(injector: Injector, private _router: Router
    // private _chartofAccountSubSubDetailService: ChartOfAccountSubSubDetailServiceProxy
  ) { 
    //super(injector);
   }


//   protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
//     this._chartofAccountSubSubDetailService.getAll(request.skipCount, request.maxResultCount)
//         .finally(()=>{
//             finishedCallback();
//         })
//         .subscribe((result:PagedResultDtoOfChartOfAccountSubSubDetailDto)=>{
//     // this.result = result.items;
//     // this.dtTrigger.next();
//     // this.showPaging(result, pageNumber);
//         });
// }

// protected delete(entity: ChartOfAccountSubSubDetailDto): void {
//   abp.message.confirm(
// "Delete Sample '"+ entity.detailTitle +"'?",
// (result:boolean) => {
//   if(result) {
//     this._chartofAccountSubSubDetailService.delete(entity.id)
//       .finally(() => {
//             abp.notify.info("Deleted Sample: " + entity.detailTitle);
//         this.refresh();
//       })
//       .subscribe(() => { });
//   }
// });
// }
globalFunction : GlobalFunctions = new GlobalFunctions
  ngOnInit() {

    if(!this.globalFunction.hasPermission('View','ChartOfAccountSubSubDetail'))
    {
      this.globalFunction.showNoRightsMessage('View')
      this._router.navigate([''])
    }

    // this._chartofAccountSubSubDetailService.getAllAccount()
    //   .subscribe((result)=>{
    //     debugger
    //     this.result = result; 
    //     this.dtTrigger.next();
    //     this.refresh();
    //   })

    //   this.dtOptions = {
    //     pagingType: 'full_numbers',
    //     pageLength: 2
    //   };
  }

  ngAfterViewInit(){
    //this.refresh();
  }


  open(): void{
    //abp.message.error("Open");
  }


    
//     AddChartOfAccountSubSubDetail(): void {
//   this.addChartAccountSubSubDetailModal.show();
// }


//   EditChartOfAccountSubSubMain(tenant:ChartOfAccountSubSubDetailDto): void {
//   this.editChartAccountSubSubDetailModal.show(tenant.id);    
// }

}
