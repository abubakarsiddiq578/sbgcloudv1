import { Component, OnInit, ViewChild, Injector } from '@angular/core';
// import { ChartOfAccountSubMainServiceProxy, PagedResultDtoOfChartOfAccountSubSubMainDto, ChartOfAccountSubSubMainDto, ChartOfAccountSubSubMainServiceProxy, CompanyInfoDropDownServiceProxy, GLNoteServiceProxy } from '@shared/service-proxies/service-proxies';
// import { PagedRequestDto } from '@shared/paged-listing-component-base';
// import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
// import { Subject } from 'rxjs/Subject';
// import { appModuleAnimation } from '@shared/animations/routerTransition';

 import {AddSubSubMainComponent} from './add-sub-sub-main/add-sub-sub-main.component';
 import { EditSubSubMainComponent } from './edit-chart-account-sub-sub-main/edit-chart-account-sub-sub-main.component';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
 


 declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;


@Component({
  selector: 'app-chart-account-sub-sub-main', 
  templateUrl: './sub-sub-main.component.html',
  styleUrls: ['./sub-sub-main.component.css'],
  // animations: [appModuleAnimation()],
  // providers: [ChartOfAccountSubSubMainServiceProxy, ChartOfAccountSubMainServiceProxy, CompanyInfoDropDownServiceProxy,
  // GLNoteServiceProxy
  // ]
})
// export class ChartAccountSubSubMainComponent extends PagedListingComponentBase<ChartOfAccountSubSubMainDto> {
export class SubSubMainComponent implements OnInit {

  @ViewChild('AddChartOfAccountSubSubMainModal') public AddChartOfAccountSubSubMainModal: AddSubSubMainComponent;
    @ViewChild('EditChartOfAccountSubSubMainModal') EditChartOfAccountSubSubMainModal: EditSubSubMainComponent;

    public dataTable: DataTable;


    // result: ChartOfAccountSubSubMainDto[]=[];
  // IsEdit: boolean = abp.auth.isGranted("Pages.Administration.Edit_Samples");;
  // IsDelete: boolean = abp.auth.isGranted("Pages.Administration.Delete_Samples");
  // IsCreate: boolean = abp.auth.isGranted("Pages.Administration.Create_Samples");

  // dtOptions: DataTables.Settings = {};  
  // dtTrigger: Subject<any> = new Subject();


  constructor(injector: Injector, private _router : Router
    /*private chartOfAccountSubSubMainService: ChartOfAccountSubSubMainServiceProxy*/)
   {
     //super(injector);
    }

  //   protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
  //     this.chartOfAccountSubSubMainService.getAll(request.skipCount, request.maxResultCount)
  //         .finally(()=>{
  //             finishedCallback();
  //         })
  //         .subscribe((result:PagedResultDtoOfChartOfAccountSubSubMainDto)=>{
  //     // this.result = result.items;
  //     // this.dtTrigger.next();
  //     // this.showPaging(result, pageNumber);
  //         });
  // }
  // protected delete(entity: ChartOfAccountSubSubMainDto): void {
  //   abp.message.confirm(
  // "Delete Sample '"+ entity.sub_sub_title +"'?",
  // (result:boolean) => {
  //   if(result) {
  //     this.chartOfAccountSubSubMainService.delete(entity.id)
  //       .finally(() => {
  //             abp.notify.info("Deleted Sample: " + entity.sub_sub_title);
  //         // this.refresh();
  //       })
  //       .subscribe(() => { });
  //   }
  // });
  // }
    globalFunction : GlobalFunctions = new GlobalFunctions

  ngOnInit() {

    if(!this.globalFunction.hasPermission('View','ChartOfAccountSubSubMain'))
    {
      this.globalFunction.showNoRightsMessage('View')
      this._router.navigate([''])
    }


    // this.chartOfAccountSubSubMainService.getAllAccount()
    //   .subscribe((result)=>{
    //     this.result = result; 
    //     this.dtTrigger.next();
    //     this.refresh();
    // })


    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 2
    // };

    this.dataTable = {
      headerRow: [ 'Sub A/C', 'Sub Sub Code', 'Sub Sub Title', 'Sub Sub Type', 'PL Note', 'BS Note', 'Company','Actions'],
      footerRow: [ 'Sub A/C', 'Sub Sub Code', 'Sub Sub Title', 'Sub Sub Type', 'PL Note', 'BS Note', 'Company','Actions'],

      dataRows: [
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike',]
      ]
   };
      
  }

  ngAfterViewInit(){
    //this.refresh();
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function(e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function(e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function(e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  
  }


  open(): void{
    //abp.message.error("Open");
}

AddChartOfAccountSubSubMain(): void {
  if(!this.globalFunction.hasPermission('Create','ChartOfAccountSubSubMain'))
    {
      this.globalFunction.showNoRightsMessage('Create')
      return
    }
    this.AddChartOfAccountSubSubMainModal.show();
}


EditChartOfAccountSubSubMain(/*tenant:ChartOfAccountSubSubMainDto*/): void {

  if(!this.globalFunction.hasPermission('Edit','ChartOfAccountSubSubMain'))
    {
      this.globalFunction.showNoRightsMessage('Edit')
      return
    }
    this.EditChartOfAccountSubSubMainModal.show(/*tenant.id*/);    
}

}
