import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { ChartOfAccountSubSubMainServiceProxy, ChartOfAccountSubSubMainDto, ChartOfAccountMainServiceProxy, GLNoteServiceProxy, CompanyInfoDropDownServiceProxy, ChartOfAccountSubMainServiceProxy } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';
 
@Component({
  selector: 'app-edit-chart-account-sub-sub-main',
  templateUrl: './edit-chart-account-sub-sub-main.component.html',
  styleUrls: ['./edit-chart-account-sub-sub-main.component.css'],
  providers: [/*ChartOfAccountSubSubMainServiceProxy*/]
})
// export class EditSubSubMainComponent extends AppComponentBase implements OnInit {
export class EditSubSubMainComponent implements OnInit {

  @ViewChild('EditChartOfAccountSubSubMainModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  // coaSubSubAccount: ChartOfAccountSubSubMainDto = new ChartOfAccountSubSubMainDto();
  CoASubMainDDList = [];
  companyDDList=[];
  bsNoteDDList = [];
  plNoteDDList=[];
  subSubAcc : any[];


  constructor(injector: Injector, /*private _SubSubMainServiceProxy: ChartOfAccountSubSubMainServiceProxy,
    // private _chartOfAccountMainServiceProxy: ChartOfAccountMainServiceProxy,
    private _companyInfoDDServiceProxy: CompanyInfoDropDownServiceProxy,
    private _glNoteServiceProxy: GLNoteServiceProxy,
    private _chartOfAccountSubMainAccount: ChartOfAccountSubMainServiceProxy
  */
  ) {
    //super(injector)
   }

  ngOnInit() {

      //start getting dropdown list for chart of account main 
    //   this._chartOfAccountSubMainAccount.getAll(0, 1000)
    //     .subscribe((result) => {
    //     this.CoASubMainDDList = result.items;
    // });
    //end getting dropdown list for chart of account main 
    
    // this._companyInfoDDServiceProxy.getCompanyInfoDropdown()
    //   .subscribe((result) => { this.companyDDList = result.items;});  
     
    //  this._glNoteServiceProxy.getBSGLDropdown()
    //    .subscribe((result)=>{
    //      this.bsNoteDDList = result.items;
    //    });
 
    //    this._glNoteServiceProxy.getPLGLDropdown()
    //    .subscribe((result)=>{
    //      this.plNoteDDList = result.items;
    //    });
 
 
    // this._SubSubMainServiceProxy.getAllAccount()
    // .subscribe((result)=>{
    //   this.subSubAcc = result; 
    // })  

  }

  show(/*id:number*/): void {
  // this._SubSubMainServiceProxy.get(id)
  //     .finally(() => {
  //         this.active = true;
  //         this.modal.show();
  //     })
  //     .subscribe((result: ChartOfAccountSubSubMainDto) => {
  //         this.coaSubSubAccount = result;
  //     });
  this.active = true;
  this.modal.show();
}

onShown(): void {
  //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
}

// save(): void {
//   this.saving = true;
//   this._SubSubMainServiceProxy.update(this.coaSubSubAccount)
//     .finally(() => {this.saving = false;})
//     .subscribe(() => {
//       this.notify.info(this.l('Record Updated Successfully'));
//       this.close();
//       this.modalSave.emit(null);
//     });
// }


close(): void {
  this.active = false;
  this.modal.hide();
}


// onChartSubChange(){
//   debugger
//   // var selectedMainAcc = this.subMainAcc.find(m => m.chartOfAccMainID == this.chartOfSubAccount.chartOfAccMainID);
//   let selectedSubAcc = this.CoASubMainDDList.find(m => m.id == this.coaSubSubAccount.subMainId);
//   if(selectedSubAcc.id <= 9)
//     this.coaSubSubAccount.sub_sub_code = selectedSubAcc.sub_code +"-00"+ selectedSubAcc.id ;
//   else if( selectedSubAcc.id <= 99)
//     this.coaSubSubAccount.sub_sub_code = selectedSubAcc.sub_code +"-0"+ selectedSubAcc.id ;
//   else if(selectedSubAcc.id < 999)
//     this.coaSubSubAccount.sub_sub_code = selectedSubAcc.sub_code +"-"+ selectedSubAcc.id ;
// }


}
