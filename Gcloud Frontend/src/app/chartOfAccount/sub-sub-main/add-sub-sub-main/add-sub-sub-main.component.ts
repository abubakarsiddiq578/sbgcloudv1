import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { ChartOfAccountSubSubMainServiceProxy, ChartOfAccountSubSubMainDto, PagedResultDtoOfChartOfAccountSubSubMainDto, PagedResultDtoOfChartOfAccountMainDto, ChartOfAccountSubMainDto, ChartOfAccountSubMainServiceProxy, CompanyInfoDropDownServiceProxy, GLNoteServiceProxy } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { PagedRequestDto } from '@shared/paged-listing-component-base';
// import { subscribeOn } from '../../../../node_modules/rxjs/operator/subscribeOn';
 

@Component({
  selector: 'app-add-chart-account-sub-sub-main',
  templateUrl: './add-sub-sub-main.component.html',
  styleUrls: ['./add-sub-sub-main.component.css']
})
// export class SubSubMainComponent extends AppComponentBase implements OnInit {
export class AddSubSubMainComponent implements OnInit {

  @ViewChild('AddChartOfAccountSubSubMainModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  // coaSubSubAccount: ChartOfAccountSubSubMainDto = new ChartOfAccountSubSubMainDto();
  // chartOfSubMainAccount: ChartOfAccountSubMainDto = new ChartOfAccountSubMainDto();
  // CoASubMainDDList = [];
  // companyDDList = [];
  // bsNoteDDList = [];
  // plNoteDDList = [];
  // subMainAcc: any[];
  // subMainAccRecList: any = {};
  // checkVal: any[];



  // constructor(injector: Injector, private _chartOfAccountSubSubMainService: ChartOfAccountSubSubMainServiceProxy,
  //   private _chartOfAccountSubMainService: ChartOfAccountSubMainServiceProxy,
  //   private _companyDDServiceProxy: CompanyInfoDropDownServiceProxy,
  //   private _glNoteServiceProxy: GLNoteServiceProxy
  // ) { 
  //   super(injector);
  // }
  constructor(injector: Injector)
  {
    
  }

  ngOnInit() {
     //start getting dropdown list for chart of account main 
  //    this._chartOfAccountSubMainService.getAllSubAccount()
  //    .subscribe((result) => {
  //      this.CoASubMainDDList = result;
  //  });
   //end getting dropdown list for chart of account main 
   
  //  this._companyDDServiceProxy.getCompanyInfoDropdown()
  //   .subscribe((result) => { this.companyDDList = result.items;});  
    
  //   this._glNoteServiceProxy.getBSGLDropdown()
  //     .subscribe((result)=>{
  //       this.bsNoteDDList = result.items;
  //     });

  //     this._glNoteServiceProxy.getPLGLDropdown()
  //     .subscribe((result)=>{
  //       this.plNoteDDList = result.items;
  //     });


  //  this._chartOfAccountSubSubMainService.getAllAccount()
  //  .subscribe((result)=>{
  //    this.subMainAcc = result; 
  //  })  
  }

  show(): void {
    this.active = true; 
    this.modal.show();
    // this.coaSubSubAccount = new ChartOfAccountSubSubMainDto();
    // this.coaSubSubAccount.init({ isActive: true });
}

onShown(): void {
    //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
}

// save(): void {
//   //TODO: Refactor this, don't use jQuery style code
//   debugger
// this.saving = true;
//   this._chartOfAccountSubSubMainService.create(this.coaSubSubAccount)
//       .finally(() => { this.saving = false; })
//       .subscribe(() => {
//           this.notify.info(this.l('Saved Successfully'));
//           this.close();   
//           this.modalSave.emit(null);
//       });
// }

close(): void {
  this.active = false;
  this.modal.hide();
}

// onChartSubChange(){
//   debugger
//   // var selectedMainAcc = this.subMainAcc.find(m => m.chartOfAccMainID == this.chartOfSubAccount.chartOfAccMainID);
//   let selectedSubAcc = this.CoASubMainDDList.find(m => m.id == this.coaSubSubAccount.subMainId);
//   if(selectedSubAcc.id <= 9)
//     this.coaSubSubAccount.sub_sub_code = selectedSubAcc.sub_code +"-00"+ selectedSubAcc.id ;
//   else if( selectedSubAcc.id <= 99)
//     this.coaSubSubAccount.sub_sub_code = selectedSubAcc.sub_code +"-0"+ selectedSubAcc.id ;
//   else if(selectedSubAcc.id < 999)
//     this.coaSubSubAccount.sub_sub_code = selectedSubAcc.sub_code +"-"+ selectedSubAcc.id ;
// }


}
