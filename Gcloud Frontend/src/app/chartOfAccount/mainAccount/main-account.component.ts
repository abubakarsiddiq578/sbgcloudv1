import { Component, OnInit, ViewChild, Injector, AfterViewInit } from '@angular/core';
import { AddMainAccountComponent } from './add-main-account/add-main-account.component';
import { EditMainAccountComponent } from './edit-main-account/edit-main-account.component';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
// import { ChartOfAccountMainServiceProxy, ChartOfAccountMainDto, PagedResultDtoOfChartOfAccountMainDto } from '@shared/service-proxies/service-proxies';
// import { PagedRequestDto } from '@shared/paged-listing-component-base';
// import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
// import { Subject } from 'rxjs/Subject';
// import { appModuleAnimation } from '@shared/animations/routerTransition';
 

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-chart-account-main',
  templateUrl: './main-account.component.html',
  styleUrls: ['./main-account.component.css']
  // animations: [appModuleAnimation()],
  // providers: [ChartOfAccountMainServiceProxy]
})
export class mainAccountComponent implements OnInit, AfterViewInit {

  @ViewChild('AddChartOfAccountMainModal') public AddChartOfAccountMainModal: AddMainAccountComponent;
    @ViewChild('EditChartOfAccountMainModal') EditChartOfAccountMainModal: EditMainAccountComponent;


    public dataTable: DataTable;


  //   result: ChartOfAccountMainDto[]=[];
  // IsEdit: boolean = abp.auth.isGranted("Pages.Administration.Edit_Samples");;
  // IsDelete: boolean = abp.auth.isGranted("Pages.Administration.Delete_Samples");
  // IsCreate: boolean = abp.auth.isGranted("Pages.Administration.Create_Samples");

  // dtOptions: DataTables.Settings = {};  
  // dtTrigger: Subject<any> = new Subject();


  constructor(injector: Injector, private _router: Router) 
  { 
      //super(injector);
  }



  globalFunction : GlobalFunctions = new GlobalFunctions


//   protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
//     this._chartOfAccountMainServiceProxy.getAll(request.skipCount, request.maxResultCount)
//         .finally(()=>{
//             finishedCallback();
//         })
//         .subscribe((result:PagedResultDtoOfChartOfAccountMainDto)=>{
//     this.result = result.items;
//     this.dtTrigger.next();
//     this.showPaging(result, pageNumber);
//         });
// }

// protected delete(entity: ChartOfAccountMainDto): void {
  
//   abp.message.confirm(
// "Delete Sample '"+ entity.main_Title +"'?",
// (result:boolean) => {
//   if(result) {
//     this._chartOfAccountMainServiceProxy.delete(entity.id)
//       .finally(() => {
//             abp.notify.info("Deleted Sample: " + entity.main_Title );
//         this.refresh();
//       })
//       .subscribe(() => { });
//   }
// });
// }

  ngOnInit() {
    if(!this.globalFunction.hasPermission('View','ChartOfAccountMain'))
    {
      this.globalFunction.showNoRightsMessage('View')
      this._router.navigate([''])
    }

    this.dataTable = {
      headerRow: [ 'Name', 'Code', 'Sort Order', 'Group', 'Active', 'Outward gatepass', 'Day Shift', 'LC ID', 'Logical','Actions'],
      footerRow: [ 'Name', 'Code', 'Sort Order', 'Group', 'Active', 'Outward gatepass', 'Day Shift', 'LC ID', 'Logical', 'Actions'],

      dataRows: [
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop',  'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'btn-simple'],
          
          
      ]
   };



    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 2
    // };
  }


  ngAfterViewInit(){
    // this.refresh();
    
  }

  open(): void{
    //abp.message.error("Open");
}

AddChartOfAccountMain(): void {

  if(!this.globalFunction.hasPermission('Create','ChartOfAccountMain'))
  {
    this.globalFunction.showNoRightsMessage('Create')
    return
  }


    this.AddChartOfAccountMainModal.show();
}


EditChartOfAccountMain(): void {
  if(!this.globalFunction.hasPermission('Edit','ChartOfAccountMain'))
  {
    this.globalFunction.showNoRightsMessage('Edit')
    return
  }
    this.EditChartOfAccountMainModal.show();    
}


}
