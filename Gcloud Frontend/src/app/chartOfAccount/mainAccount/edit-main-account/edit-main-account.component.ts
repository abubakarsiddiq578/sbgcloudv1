import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { ChartOfAccountMainServiceProxy, ChartOfAccountMainDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';

 


@Component({
  selector: 'app-edit-chart-account-main',
  templateUrl: './edit-main-account.component.html',
  styleUrls: ['./edit-main-account.component.css']
//   providers: [ChartOfAccountMainServiceProxy]
})
export class EditMainAccountComponent implements OnInit {

  @ViewChild('EditChartOfAccountMainModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
//   chartOfAccount: ChartOfAccountMainDto = new ChartOfAccountMainDto();


  constructor(injector: Injector) 
  {
    //super(injector); 
  }

  ngOnInit() {
  }

  show(): void {
      this.active = true;
      this.modal.show();
    // this._chartOfAccountMainServiceProxy.get(id)
    //     .finally(() => {
    //         this.active = true;
    //         this.modal.show();
    //     })
    //     .subscribe((result: ChartOfAccountMainDto) => {
    //         this.chartOfAccount = result;
    //     });
}

onShown(): void {
    //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
}

// save(): void {
//     this.saving = true;
//     this._chartOfAccountMainServiceProxy.update(this.chartOfAccount)
//       .finally(() => {this.saving = false;})
//       .subscribe(() => {
//         this.notify.info(this.l('Record Updated Successfully'));
//         this.close();
//         this.modalSave.emit(null);
//       });
//   }


close(): void {
    this.active = false;
    this.modal.hide();
}

}
