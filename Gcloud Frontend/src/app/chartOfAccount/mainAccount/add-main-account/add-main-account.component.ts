import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { ChartOfAccountMainServiceProxy, ChartOfAccountMainDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';

 

@Component({
  selector: 'app-add-chart-account-main',
  templateUrl: './add-main-account.component.html',
  styleUrls: ['./add-main-account.component.css']
})
export class AddMainAccountComponent implements OnInit {

  @ViewChild('AddChartOfAccountMainModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    // chartOfAccount: ChartOfAccountMainDto = new ChartOfAccountMainDto();

  constructor(injector: Injector) { 
    //super(injector);
  }

  ngOnInit() {
  }

  show(): void {
    this.active = true; 
    this.modal.show();
    // this.chartOfAccount = new ChartOfAccountMainDto();
    // this.chartOfAccount.init({ isActive: true });
}

onShown(): void {
    //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
}


// save(): void {
//   //TODO: Refactor this, don't use jQuery style code
// this.saving = true;
//   this._chartOfAccountMainServiceProxy.create(this.chartOfAccount)
//       .finally(() => { this.saving = false; })
//       .subscribe(() => {
//           this.notify.info(this.l('Saved Successfully'));
//           this.close();   
//           this.modalSave.emit(null);
//       });
// }

close(): void {
  this.active = false;
  this.modal.hide();
}
}
