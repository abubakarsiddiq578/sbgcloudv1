import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { TenantRoutes } from './tenant.routing';
import { ModalModule } from 'ngx-bootstrap';

import { TenantsComponent} from '../tenants/tenants.component';
import {CreateTenantComponent} from '../tenants/create-tenant/create-tenant.component';
import {EditTenantComponent} from '../tenants/edit-tenant/edit-tenant.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TenantRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule.forRoot()

    
  ],
  declarations: [
    TenantsComponent,
    CreateTenantComponent,
    EditTenantComponent
  ]
})

export class Tenant {}
