import { Routes } from '@angular/router';

import { AppRouteGuard } from '@app/shared/auth/auth-route-guard';
import { TenantsComponent } from '@app/tenants/tenants.component';


export const TenantRoutes: Routes = [
    {
      path: '',
      
      children: [ {
        path: '',
        component: TenantsComponent, data: { permission: 'Pages.Administration.Tenants' }, 
        canActivate: [AppRouteGuard]
    }]},
    
];  
