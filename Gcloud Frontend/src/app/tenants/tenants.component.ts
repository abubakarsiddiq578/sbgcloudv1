﻿import { Component, Injector, ViewChild } from '@angular/core';
// import { appModuleAnimation } from '@shared/animations/routerTransition';
import { TenantServiceProxy, TenantDto, PagedResultDtoOfTenantDto } from '@shared/service-proxies/service-proxies';

// import { PagedListingComponentBase, PagedRequestDto } from "shared/paged-listing-component-base";
import { EditTenantComponent } from "app/tenants/edit-tenant/edit-tenant.component";
import { CreateTenantComponent } from "app/tenants/create-tenant/create-tenant.component";
import swal from 'sweetalert2';
import { AppComponentBase } from '@app/shared/app-component-base';

declare const $: any;

declare interface DataTable {
    headerRow: string[];
    //footerRow: string[];
    dataRows: string[][];
}

@Component({
    templateUrl: './tenants.component.html',
    animations: [/*appModuleAnimation()*/]
})
export class TenantsComponent extends AppComponentBase  /*extends PagedListingComponentBase<TenantDto>*/ {

    public dataTable: DataTable;

    @ViewChild('createTenantModal') createTenantModal: CreateTenantComponent;
    @ViewChild('editTenantModal') editTenantModal: EditTenantComponent;

    IsEdit: boolean = abp.auth.isGranted("Pages.Administration.Edit_Tenants");;
    IsDelete: boolean = abp.auth.isGranted("Pages.Administration.Delete_Tenants");
    IsCreate: boolean = abp.auth.isGranted("Pages.Administration.Create_Tenants");

    tenants: TenantDto[] = [];

    data: string[] = [];

    constructor(
        injector: Injector,
        private _tenantService: TenantServiceProxy
    ) {
         super(injector);
    }

    ngOnInit() {
        this.getAllTenants();
    }
    
    // Show modals
    createTenant(): void {
        this.createTenantModal.show();
    }

    editTenant(id: string): void {
        this.editTenantModal.show(parseInt(id));
    }

    getAllTenants(): void {
        this._tenantService.getAll(0, 10000)
            .finally(() => {
            })
            .subscribe((result: PagedResultDtoOfTenantDto) => {
                this.initDataTable()
                this.tenants = result.items;
                this.fillDataTable();

            });
    }

    initDataTable(): void {

        this.dataTable = {
            headerRow: ['TenancyName', 'Name', 'Actine', 'Actions'],
            //footerRow: [ 'Doc No', 'Doc Date', 'Allowance Date', 'Employee', 'Allowance Type', 'Amount', 'Actions' ],

            dataRows: [
                //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Medical','8000', ''],
                //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Medical','8000', ''],
                //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Medical','8000', ''],
            ]
        };
    }

    fillDataTable() {

        let i;
        for (i = 0; i < this.tenants.length; i++) {

            this.data.push(this.tenants[i].tenancyName)
            this.data.push(this.tenants[i].name)
            this.data.push(this.tenants[i].isActive.toString())
            this.data.push(this.tenants[i].id.toString())

            this.dataTable.dataRows.push(this.data)

            this.data = [];
        }
    }

    protected delete(id: string): void {
        swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this Tenant!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it',
          confirmButtonClass: "btn btn-success",
          cancelButtonClass: "btn btn-danger",
          buttonsStyling: false
        }).then((result) => {
        if (result.value) {
            this._tenantService.delete((parseInt(id)))
            .finally(() => {
              this.getAllTenants();
            })
            .subscribe(() => {
              swal({
                title: 'Deleted!',
                text: 'Your Tenant has been deleted.',
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
            }).catch(swal.noop)
             });
        } else {
          swal({
              title: 'Cancelled',
              text: 'Your Tenant is safe :)',
              type: 'error',
              confirmButtonClass: "btn btn-info",
              buttonsStyling: false
          }).catch(swal.noop)
        }
      })
    }
}