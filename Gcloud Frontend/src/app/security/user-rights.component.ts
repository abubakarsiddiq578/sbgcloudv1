// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Injector, Inject } from '@angular/core';
import { UserAccountRightsComponent } from "./rights/user-account-rights/user-account-rights.component";
import { UserCompanyRightsComponent } from "./rights/user-company-rights/user-company-rights.component";
import { UserCostCenterRightsComponent } from "./rights/user-costcenter-rights/user-costcenter-rights.component";
import { UserLocationRightsComponent } from "./rights/user-location-rights/user-location-rights.component";
import { UserVoucherTypeRightsComponent } from "./rights/user-vouchertype-rights/user-vouchertype-rights.component";
import { UserImgComponent } from "./user-img/user-img.component";
import { AddUserComponent } from "./users/add-user/add-user.component";
import { UpdateUserComponent } from "./users/edit-user/edit-user.component";
import { AddRoleComponent } from "./roles/add-role/add-role.component";
import { EditRoleComponent } from "./roles/edit-role/edit-role.component";
import { UserServiceProxy, UserDto, PagedResultDtoOfUserDto, RoleServiceProxy, RoleDto, PagedResultDtoOfRoleDto, ListResultDtoOfPermissionDto, UserCompanyRightServiceProxy, PagedResultDtoOfUserCompanyRightDto, UserCompanyRightDto, CompanyInfoServiceProxy, UserCostCenterRightServiceProxy, UserCostCenterRightDto, CostCenterServiceProxy, UserLocationRightServiceProxy, LocationInfoServiceProxy, UserLocationRightDto, UserVoucherTypeRightServiceProxy, VoucherTypeServiceProxy, UserVoucherTypeRightDto, UserAccountRightServiceProxy, UserAccountRightDto, UserMediaContentServiceProxy, UserMediaContentDto, TenantDto, TenantServiceProxy } from 'app/shared/service-proxies/service-proxies';
//import { AppSessionService } from 'app/shared/session/app-session.service';
//import { LocalizationService } from '@abp/localization/localization.service';
import { TreeChildren } from 'Models/TreeChildren';
import { TreeRoot } from 'Models/TreeRoot';
import { AppComponentBase } from '@app/shared/app-component-base';
import { AbpSessionService } from '@abp/session/abp-session.service';
import { AppConsts } from 'app/shared/AppConsts';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
declare var $: any;

@Component({
    selector: 'user-rights',
    templateUrl: 'user-rights.component.html',
    providers: [
        UserCompanyRightServiceProxy,
        CompanyInfoServiceProxy,
        CostCenterServiceProxy,
        UserCostCenterRightServiceProxy,
        UserLocationRightServiceProxy,
        LocationInfoServiceProxy,
        UserVoucherTypeRightServiceProxy,
        VoucherTypeServiceProxy,
        UserAccountRightServiceProxy,
        UserMediaContentServiceProxy,
        UserServiceProxy,
        RoleServiceProxy
        //AppSessionService, 
        //LocalizationService
    ]
})

export class UserRightsComponent extends AppComponentBase implements OnInit, AfterViewInit {

    @ViewChild('useraccountrightsModal') useraccountrightsModal: UserAccountRightsComponent;
    @ViewChild('usercompanyrightsModal') usercompanyrightsModal: UserCompanyRightsComponent;
    @ViewChild('usercostcenterrightsModal') usercostcenterrightsModal: UserCostCenterRightsComponent;
    @ViewChild('userlocationrightsModal') userlocationrightsModal: UserLocationRightsComponent;
    @ViewChild('uservouchertyperightsModal') uservouchertyperightsModal: UserVoucherTypeRightsComponent;
    @ViewChild('userimgModal') userImgModal: UserImgComponent;
    @ViewChild('createUserModal') createUserModal: AddUserComponent;
    @ViewChild('editUserModal') editUserModal: UpdateUserComponent;
    @ViewChild('createRoleModal') createRoleModal: AddRoleComponent;
    @ViewChild('editRoleModal') editRoleModal: EditRoleComponent;

    @ViewChild('jstreearea') jsTree: ElementRef;

    isActive: Boolean = false

    isUsersList: boolean = true;
    isRolesList: boolean = false;

    // users objects
    users: UserDto[] = [];
    userInfo: any;
    roleNames: string[];
    userData: UserData[] = [];

    // user media content objects
    userMediaContent: UserMediaContentDto[] = [];
    specificUserMediaContent: UserMediaContentDto = null;

    //roles objects
    roles: RoleDto[] = [];
    roleInfo: any;

    // permission objects
    permissions: string[];
    permissionsList: ListResultDtoOfPermissionDto = null;
    rootnode: any[];
    public root1: TreeRoot = new TreeRoot();
    children: TreeChildren = new TreeChildren();

    // user company rights objects
    _userCompanyRights: UserCompanyRightDto[] = [];
    _specificUserCompanyRights: UserCompanyRightDto[] = [];
    companyRights: Array<string> = [];

    // user cost center rights objects
    _userCostCenterRights: UserCostCenterRightDto[] = [];
    _specificUserCostCenterRights: UserCostCenterRightDto[] = [];
    costcenterRights: Array<string> = [];

    // user account rights objects
    _userAccountRights: UserAccountRightDto[] = [];
    _specificUserAccountRights: UserAccountRightDto[] = [];
    accountRights: Array<string> = [];

    // user location rights objects
    _userLocationRights: UserLocationRightDto[] = [];
    _specificUserLocationRights: UserLocationRightDto[] = [];
    locationRights: Array<string> = [];

    // user voucher type rights objects

    _userVoucherTypeRights: UserVoucherTypeRightDto[] = [];
    _specificUserVoucherTypeRights: UserVoucherTypeRightDto[] = [];
    voucherTypeRights: Array<string> = [];

    // Tenant 
    TenantId: number;
    TenantName: string;

    data: Object;

    //userRights

    IsViewCompany: Boolean = abp.auth.isGranted("Pages.Security.Company.View_Company");
    IsViewCostCenter: Boolean = abp.auth.isGranted("Pages.Security.CostCenter.View_CostCenter");
    IsViewAccount: Boolean = abp.auth.isGranted("Pages.Security.Account.View_Account");
    IsViewVoucherType: Boolean = abp.auth.isGranted("Pages.Security.VoucherType.View_VoucherType");
    IsViewLocation: Boolean = abp.auth.isGranted("Pages.Security.Location.View_Location");

    
    IsCreateRole: Boolean = abp.auth.isGranted("Pages_Administration_Create_Roles");
    IsEditRole: Boolean = abp.auth.isGranted("Pages_Administration_Edit_Roles");

    IsCreateUser: Boolean = abp.auth.isGranted("Pages_Administration_Create_Users");
    IsEditUser: Boolean = abp.auth.isGranted("Pages_Administration_Edit_Users");
    

        globalFunction :GlobalFunctions = new GlobalFunctions;
    constructor(private _userService: UserServiceProxy, private rolesService: RoleServiceProxy,
        private userCompanyRightServiceProxy: UserCompanyRightServiceProxy,
        private userCostCenterRightServiceProxy: UserCostCenterRightServiceProxy,
        private userLocationRightServiceProxy: UserLocationRightServiceProxy,
        private userVoucherTypeRightServiceProxy: UserVoucherTypeRightServiceProxy,
        private userAccountRightServiceProxy: UserAccountRightServiceProxy,
        private userMediaContentServiceProxy: UserMediaContentServiceProxy,
        private _tenantService: TenantServiceProxy,
        private injector: Injector,
        private _sessionService: AbpSessionService,
        private http: HttpClient,
        private _router: Router,
        @Inject(LOCAL_STORAGE) private storage: WebStorageService) {
        
            //initailize empty user media content
        super(injector)
        this.initEmptyUserMediaContent();

        debugger;

        this.getIpAddress().subscribe(data => {
            debugger;
            //console.log(JSON.stringify(data));
            this.data = JSON.stringify(data.propertyIsEnumerable.name)
            console.log(this.data)
        });
    }

    getIpAddress() {
        return this.http
              .get('https://ipinfo.io')          //https://freegeoip.net/json/?callback
              .map(response => response || {})
              //.catch(this.handleError);
    }

    ngAfterViewInit(){}

    ngOnInit() {

      /*  if(!this.IsView){
            this.globalFunction.showNoRightsMessage('View');
            this._router.navigate([''])
            return
        }
*/
        this.getUserRolePermissions();

        /*this.TenantId = this.storage.get('TenantId');

        if(this.TenantId > 0){
            this.getSpecificTenant(this.TenantId)
        }
        else{
            this.TenantName = "SoftBeats";
        }*/
    }

    getUserRolePermissions() {
        //get all users
        this.getAllUsers();

        //get all roles
        this.getAllRoles();

        //get all role permissions
        this.rolePermissions();
    

      $('.card .material-datatables label').addClass('form-group');
    }

    // Show User Account Rights
    userAccountRights(): void {
        if(!this.IsViewAccount){
            this.globalFunction.showNoRightsMessage('View');
            
            return
        }
        this.filterUserRights(this.userInfo.id, "Account");
        this.useraccountrightsModal.show(this.userInfo.id, this._specificUserAccountRights);
    }

    // Show User Company Rights
    userCompanyRights(): void {
        debugger
        if(!this.IsViewCompany){
            this.globalFunction.showNoRightsMessage('View');
            
            return
        }

        this.filterUserRights(this.userInfo.id, "Company");
        this.usercompanyrightsModal.show(this.userInfo.id, this._specificUserCompanyRights);
    }

    // Show User CostCenter Rights
    userCostCenterRights(): void {
        debugger
        if(!this.IsViewCostCenter){
            this.globalFunction.showNoRightsMessage('View');
            
            return
        }

        this.filterUserRights(this.userInfo.id, "CostCenter");
        this.usercostcenterrightsModal.show(this.userInfo.id, this._specificUserCostCenterRights);
    }

    // Show User Location Rights
    userLocationRights(): void {
        if(!this.IsViewLocation){
            this.globalFunction.showNoRightsMessage('View');
            
            return
        }
        this.filterUserRights(this.userInfo.id, "Location");
        this.userlocationrightsModal.show(this.userInfo.id, this._specificUserLocationRights);
    }

    // Show User VoucherType Rights
    userVoucherTypeRights(): void {
        if(!this.IsViewVoucherType){
            this.globalFunction.showNoRightsMessage('View');
            
            return
        }
        this.filterUserRights(this.userInfo.id, "VoucherType");
        this.uservouchertyperightsModal.show(this.userInfo.id, this._specificUserVoucherTypeRights);
    }

    // Open User Img Form 
    openUserImgForm(id) {
        this.userImgModal.show(id);
    }

    // For create new user 
    createUser(): void {
       /* if(!this.IsCreateUser){
            this.globalFunction.showNoRightsMessage('Create');
            
            return
        }*/
        this.createUserModal.show();
    }

    // For Edit user
    editUser(user: UserDto): void {
      /*  if(!this.IsEditUser){
            this.globalFunction.showNoRightsMessage('Edit');
            
            return
      }*/
        this.editUserModal.show(user.id);
    }

    // For Create Roles
    createRole(): void {

    /*    if(!this.IsCreateRole){
            this.globalFunction.showNoRightsMessage('Create');
            
            return
        }*/
        this.createRoleModal.show();
    }

    // For Edit Roles
    editRole(role: RoleDto): void {
    
        this.editRoleModal.show(role.id);
    }

    //initailize empty user media content
    initEmptyUserMediaContent() {
        this.specificUserMediaContent = new UserMediaContentDto();
        this.specificUserMediaContent.userId = 0;
        this.specificUserMediaContent.imageUrl = "";
    }

    checkTenant(TenantId): boolean {
        if (TenantId > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    // get all users

    getAllUsers() {
        this._userService.getAll(0, 10000)         //getAllUsers(this.TenantId)    //getAll(0, 10000)              //
            .finally(() => {
            })
            .subscribe((result: PagedResultDtoOfUserDto) => {    // 
                //let TenantId = this.storage.get('TenantId');

                //if (this.checkTenant(TenantId)) {
                    //this.users = result.items.filter(function (obj) { return obj.tenantId === TenantId; });
                //}
                //else {
                    this.users = result.items;
                //}

                this.userInfo = this.users[0];
                this.roleNames = this.userInfo.roleNames;

                // get user media contents
                this.getUserMediaContent();

                // get user companies rights
                this.getUserCompanyRights();

                // get user costcenter rights
                this.getUserCostCenterRights();

                // get user location rights
                this.getUserLocationRights();

                // get user vouchertype rights
                this.getUserVoucherTypeRights();

                // get user account rights
                this.getUserAccountRights();
            });
    }

    // For display user list
    displayUserBtn(): void {
        this.isUsersList = true;
        this.isRolesList = false;
    }

    // For Display roles list
    displayRoleBtn(): void {
        this.isRolesList = true;
        this.isUsersList = false;

        setTimeout(() => {
            this.showJSTree();
        }, 0.01);
    }

    // Show User Details
    showUserDetails(id: any) {
        let user = this.users.find(function (obj) { return obj.id === id; });
        this.filterUserRights(user.id, "Company");
        this.filterUserRights(user.id, "CostCenter");
        this.filterUserRights(user.id, "Location");
        this.filterUserRights(user.id, "VoucherType");
        this.filterUserRights(user.id, "Account");
        this.userInfo = user;
        this.filterUserMediaContent(this.userInfo.id);
        this.roleNames = this.userInfo.roleNames;
    }

    // filter user to get particular user pic 
    filterUserMediaContent(userId: number) {
        this.specificUserMediaContent = this.userMediaContent.find(function (obj) { return obj.userId === userId; });

        if (this.specificUserMediaContent == undefined) {
            this.initEmptyUserMediaContent();
        }
    }

    // get user media contents
    getUserMediaContent(): void {
        this.userMediaContentServiceProxy.getAllMediaContent()
            .subscribe((result) => {
                this.userMediaContent = result;

                this.fillUserData();

                this.filterUserMediaContent(this.userInfo.id);
            });
    }

    fillUserData() {
        let i = 0;

        this.userData = [];

        for (i = 0; i < this.users.length; i++) {
            let individual = new UserData();

            individual.id = this.users[i].id;
            individual.name = this.users[i].name;
            individual.username = this.users[i].userName;

            if (this.getUserPic(this.users[i].id) == undefined) {
                individual.imgUrl = ""
            }
            else {
                individual.imgUrl = this.getUserPic(this.users[i].id).imageUrl;
            }

            this.userData.push(individual);
        }
    }

    getUserPic(userId: number): UserMediaContentDto {
        return this.userMediaContent.find(function (obj) { return obj.userId === userId; });
    }

    // get all roles

    getAllRoles() {
        this.rolesService.getAll(0, 10000)
            .finally(() => {
            })
            .subscribe((result: PagedResultDtoOfRoleDto) => {
                //let TenantId = this.storage.get('TenantId');

                //if (this.checkTenant(TenantId)) {
                    //this.roles = result.items.filter(function (obj) { return obj.tenantId === TenantId; });
                //}
                //else {
                    this.roles = result.items;
                //}

                this.roleInfo = this.roles[0];
                this.permissions = this.roleInfo.permissions
            });
    }

    // for get all role permissions

    rolePermissions() {
        this.rolesService.getAllPermissions()
            .subscribe((permissions: ListResultDtoOfPermissionDto) => {

                this.permissionsList = permissions;
                console.log(this.permissionsList);
                this.rootnode = [];

                let v = 0;
                this.root1.children = [];

                for (v = 2; v < this.permissionsList.items.length; v++) {
                    if (this.permissionsList.items[v].name.indexOf("_") > 0) {

                        this.children = new TreeChildren();
                        this.children.name = this.permissionsList.items[v].displayName;
                        this.children.value = this.permissionsList.items[v].name;
                        this.children.NodeId = this.permissionsList.items[v].id;
                        this.root1.children.push(this.children);
                    }
                    else {
                        if (this.root1.children.length > 0) {
                            this.rootnode.push(this.root1);
                        }

                        this.root1 = new TreeRoot();
                        this.root1.children = [];
                        this.root1.name = this.permissionsList.items[v].displayName;
                        this.root1.value = this.permissionsList.items[v].name;
                        this.root1.NodeId = this.permissionsList.items[v].id;
                    }
                }

                this.rootnode.push(this.root1);
                console.log(this.rootnode);
                this.root1 = new TreeRoot();
            });
    }

    // Set JSTree 
    showJSTree() {

        $(this.jsTree.nativeElement).jstree(
            {
                "plugins": ["checkbox"]
            });
    }

    // Set Permission value role wise
    checkPermission(permissionName: string): boolean {
        if (this.roleInfo.permissions.indexOf(permissionName) != -1) {
            return true;
        }
        else {
            return false;
        }
    }

    // Show Role Details
    showRoleDetails(id: any) {
        let role = this.roles.find(function (obj) { return obj.id === id; });
        this.roleInfo = role;

        this.displayUserBtn();

        setTimeout(() => {
            this.displayRoleBtn();
        }, 0.01);
    }

    // get all UserCompanyRights

    getUserCompanyRights() {
        this.userCompanyRightServiceProxy.getCompanyRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this._userCompanyRights = result;
                this.filterUserRights(this.userInfo.id, "Company");
                console.log(this._userCompanyRights);
            });
    }

    // Filter specific user all rights 

    filterUserRights(userId: number, rightsType: string): void {

        if (rightsType == "Company") {
            this._specificUserCompanyRights = this._userCompanyRights.filter(function (obj) { return obj.userId === userId; });
            this.showUserRightsCompany();
        }
        else if (rightsType == "CostCenter") {
            this._specificUserCostCenterRights = this._userCostCenterRights.filter(function (obj) { return obj.userId === userId; });
            this.showUserRightsCostCenter();
        }
        else if (rightsType == "Location") {
            this._specificUserLocationRights = this._userLocationRights.filter(function (obj) { return obj.userId === userId; });
            this.showUserRightsLocation();
        }
        else if (rightsType == "VoucherType") {
            this._specificUserVoucherTypeRights = this._userVoucherTypeRights.filter(function (obj) { return obj.userId === userId; });
            this.showUserRightsVoucherType();
        }
        else if (rightsType == "Account") {
            this._specificUserAccountRights = this._userAccountRights.filter(function (obj) { return obj.userId === userId; });
            this.showUserRightsAccount();
        }
    }

    // Diplay user companies rights against which rights are true 

    showUserRightsCompany(): void {
        let i = 0;
        let y = 0;

        this.companyRights = [];

        if (this._specificUserCompanyRights.length <= 0) {
            this.companyRights.push("No Rights");
        }
        else {
            for (i = 0; i < this._specificUserCompanyRights.length; i++) {
                if (this._specificUserCompanyRights[i].rights == true) {
                    if (y < 2) {
                        y++;
                        this.companyRights.push(this._specificUserCompanyRights[i].companyInfo.companyName);
                    }
                    else {
                        y++;
                    }
                }
            }

            if (this.companyRights.length > 0 && y > 1) {
                this.companyRights.push("(+" + (y - 2).toString() + ")");
            }
            else if (this.companyRights.length <= 0) {
                this.companyRights.push("No Rights");
            }
        }
    }

    // get all UserCostCenterRights

    getUserCostCenterRights() {
        this.userCostCenterRightServiceProxy.getCostCenterRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this._userCostCenterRights = result;
                this.filterUserRights(this.userInfo.id, "CostCenter");
                //console.log(this._userCostCenterRights);
            });
    }

    // Diplay user costcenter rights against which rights are true 

    showUserRightsCostCenter(): void {
        let i = 0;
        let y = 0;

        this.costcenterRights = [];

        if (this._specificUserCostCenterRights.length <= 0) {
            this.costcenterRights.push("No Rights");
        }
        else {
            for (i = 0; i < this._specificUserCostCenterRights.length; i++) {
                if (this._specificUserCostCenterRights[i].rights == true) {
                    if (y < 2) {
                        y++;
                        this.costcenterRights.push(this._specificUserCostCenterRights[i].costCenter.name);
                    }
                    else {
                        y++;
                    }
                }
            }

            if (this.costcenterRights.length > 0 && y > 1 && y != 2) {
                this.costcenterRights.push("(+" + (y - 2).toString() + ")");
            }
            else if (this.costcenterRights.length <= 0) {
                this.costcenterRights.push("No Rights");
            }
        }
    }

    // get all UserAccountRights

    getUserAccountRights() {
        this.userAccountRightServiceProxy.getAccountRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this._userAccountRights = result;
                this.filterUserRights(this.userInfo.id, "Account");
                //console.log(this._userCostCenterRights);
            });
    }

    // Diplay user account rights against which rights are true 

    showUserRightsAccount(): void {
        let i = 0;
        let y = 0;

        this.accountRights = [];

        if (this._specificUserAccountRights.length <= 0) {
            this.accountRights.push("No Rights");
        }
        else {
            for (i = 0; i < this._specificUserAccountRights.length; i++) {
                if (this._specificUserAccountRights[i].rights == true) {
                    if (y < 2) {
                        y++;
                        this.accountRights.push(this._specificUserAccountRights[i].chartOfAccountSubSubDetail.detailTitle);
                    }
                    else {
                        y++;
                    }
                }
            }

            if (this.accountRights.length > 0 && y > 1 && y != 2) {
                this.accountRights.push("(+" + (y - 2).toString() + ")");
            }
            else if (this.accountRights.length <= 0) {
                this.accountRights.push("No Rights");
            }
        }
    }

    // get all UserLocationRights

    getUserLocationRights() {
        this.userLocationRightServiceProxy.getLocationRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this._userLocationRights = result;
                this.filterUserRights(this.userInfo.id, "Location");
                //console.log(this._userCostCenterRights);
            });
    }

    // Diplay user location rights against which rights are true 

    showUserRightsLocation(): void {
        let i = 0;
        let y = 0;

        this.locationRights = [];

        if (this._specificUserLocationRights.length <= 0) {
            this.locationRights.push("No Rights");
        }
        else {
            for (i = 0; i < this._specificUserLocationRights.length; i++) {
                if (this._specificUserLocationRights[i].rights == true) {
                    if (y < 2) {
                        y++;
                        this.locationRights.push(this._specificUserLocationRights[i].locationInfo.location_Name);
                    }
                    else {
                        y++;
                    }
                }
            }

            if (this.locationRights.length > 0 && y > 1 && y != 2) {
                this.locationRights.push("(+" + (y - 2).toString() + ")");
            }
            else if (this.locationRights.length <= 0) {
                this.locationRights.push("No Rights");
            }
        }
    }

    // get all UserVoucherTypeRights

    getUserVoucherTypeRights() {
        this.userVoucherTypeRightServiceProxy.getVoucherTypeRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this._userVoucherTypeRights = result;
                this.filterUserRights(this.userInfo.id, "VoucherType");
                //console.log(this._userCostCenterRights);
            });
    }

    // Diplay user vouchertype rights against which rights are true 

    showUserRightsVoucherType(): void {
        let i = 0;
        let y = 0;

        this.voucherTypeRights = [];

        if (this._specificUserVoucherTypeRights.length <= 0) {
            this.voucherTypeRights.push("No Rights");
        }
        else {
            for (i = 0; i < this._specificUserVoucherTypeRights.length; i++) {
                if (this._specificUserVoucherTypeRights[i].rights == true) {
                    if (y < 2) {
                        y++;
                        this.voucherTypeRights.push(this._specificUserVoucherTypeRights[i].voucherType.voucher_type);
                    }
                    else {
                        y++;
                    }
                }
            }

            if (this.voucherTypeRights.length > 0 && y > 1 && y != 2) {
                this.voucherTypeRights.push("(+" + (y - 2).toString() + ")");
            }
            else if (this.voucherTypeRights.length <= 0) {
                this.voucherTypeRights.push("No Rights");
            }
        }
    }

    // get specific tenant

    getSpecificTenant(id: number): void{
        this._tenantService.get(id)
			.subscribe((result: TenantDto)=>{
				this.TenantName = result.tenancyName;
		});
    }

}

class UserData {
    id: number;
    name: string;
    username: string;
    imgUrl: string;
}




interface ItemsResponse {
    ip: string;
}