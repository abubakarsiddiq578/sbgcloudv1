import { Routes } from '@angular/router';

import { UserRightsComponent } from '../security/user-rights.component';
import { AppRouteGuard } from '@app/shared/auth/auth-route-guard';


export const SecurityRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'UserRights',
        component: UserRightsComponent,
        canActivate: [AppRouteGuard]
    }
]}
];  
