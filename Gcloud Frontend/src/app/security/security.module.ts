import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';

import { ModalModule } from 'ngx-bootstrap';

import { SecurityRoutes } from './security.routing';

import { UserRightsComponent } from '../security/user-rights.component';
import { UserAccountRightsComponent } from '../security/rights/user-account-rights/user-account-rights.component';
import { UserCompanyRightsComponent } from '../security/rights/user-company-rights/user-company-rights.component';
import { UserCostCenterRightsComponent } from '../security/rights/user-costcenter-rights/user-costcenter-rights.component';
import { UserLocationRightsComponent } from '../security/rights/user-location-rights/user-location-rights.component';
import { UserVoucherTypeRightsComponent } from '../security/rights/user-vouchertype-rights/user-vouchertype-rights.component';
import { UserImgComponent } from '../security/user-img/user-img.component';
import { AddUserComponent } from "./users/add-user/add-user.component";
import { UpdateUserComponent } from "./users/edit-user/edit-user.component";
import { AddRoleComponent } from "./roles/add-role/add-role.component";
import { EditRoleComponent } from "./roles/edit-role/edit-role.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SecurityRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule
  ],
  declarations: [
    UserRightsComponent,
    UserAccountRightsComponent,
    UserCompanyRightsComponent,
    UserCostCenterRightsComponent,
    UserLocationRightsComponent,
    UserVoucherTypeRightsComponent,
    UserImgComponent,
    AddUserComponent,
    UpdateUserComponent,
    AddRoleComponent,
    EditRoleComponent
  ]
})

export class Security {}
