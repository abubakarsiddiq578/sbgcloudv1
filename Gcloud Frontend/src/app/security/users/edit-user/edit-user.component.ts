import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserServiceProxy, UserDto, RoleDto } from '@shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
//import { AppComponentBase } from '@shared/app-component-base';

@Component({
    selector: 'edit-user-modal',
    templateUrl: './edit-user.component.html'
})
export class UpdateUserComponent  /*extends AppComponentBase*/ {

    @ViewChild('editUserModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    // roles = [
    //     {name: 'Admin'},
    //     {name: 'Super Admin'},
    //     {name: 'User'},
    //     {name: 'Client'},
    //     {name: 'Test'},
    //     {name: 'Writer'},
    // ];

    user: UserDto = null;
    roles: RoleDto[] = null;

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy
    ) {
        //super(injector);
    }

    userInRole(role: RoleDto, user: UserDto): string {
        if (user.roleNames.indexOf(role.normalizedName) !== -1) {
            return "checked";
        }
        else {
            return "";
        }
    }

    show(id: number): void {

        this._userService.getRoles()
            .subscribe((result) => {
                this.roles = result.items;
            });

        this._userService.get(id)
            .subscribe(
            (result) => {
                this.user = result;
                this.user.password = ""
                this.active = true;
                this.modal.show();
            }
            );
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        debugger;
        var roles = [];
        $(this.modalContent.nativeElement).find("[name=role]").each(function (ind: number, elem: Element) {
            if ($(elem).is(":checked")) {
                roles.push(elem.getAttribute("value").valueOf());
            }
        });

        this.user.roleNames = roles;

        this.saving = true;
        this._userService.update(this.user)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notifyMsg('Success!' , 'SavedSuccessfully');
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    notifyMsg(tag: string , msg: string){
        swal({
            title: tag,
            text: msg,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}
