import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserMediaContentServiceProxy , UserMediaContentDto } from '@shared/service-proxies/service-proxies';
//import { AppComponentBase } from '@shared/app-component-base';
//import { TreeChildren } from 'Models/TreeChildren';
//import { TreeRoot } from 'Models/TreeRoot';
import {FormBuilder, FormGroup , Validator} from "@angular/forms";
import { Http } from '@angular/http';
import swal from 'sweetalert2';
declare var $: any;

@Component({
    selector: 'user-img-modal',
    templateUrl: './user-img.component.html'
})
export class UserImgComponent /*extends AppComponentBase*/ implements OnInit{
    @ViewChild('createUserImgModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('jstreearea') jsTree: ElementRef;
    @ViewChild('userPic') userPic: ElementRef;
    form: FormGroup;
    active: boolean = false;
    saving: boolean = false;

    imagefile: any;
    imageurl: any

    userId: number;

    isDisable: boolean = true;

    ProfilePicUrl: string

    userMediaContent: UserMediaContentDto = null;

    mediaContent: UserMediaContentDto[] = [];

    specificMediaContent: UserMediaContentDto = null;

    @ViewChild("userPic") fileInput;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    constructor(
        injector: Injector,
        private http: Http,
        private userMediaContentServiceProxy: UserMediaContentServiceProxy,
        private fb: FormBuilder
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    show(userId: number): void {
        this.active = true;
        this.modal.show();

        this.userId = userId;

        // get user media content
        this.getUserMediaContent()

        //console.log($('#data'));
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    close(): void {
        this.active = true;
        this.modal.hide();
    }

    onSelectFile(event) { // called each time file input changes
      this.isDisable=false;
        //abp.message.info(event.target.files[0].name);    
    }

    upload(fileToUpload: any) {
        let input = new FormData();
        input.append("file", fileToUpload);

        return this.http.post("http://localhost:21021/api/services/app/Account/CreateProfilePic", input);
    }

   uploadUserPic(): void
   {
        let fi = this.fileInput.nativeElement;
        if (fi.files && fi.files[0]) {
            let fileToUpload = fi.files[0];
            console.log(fileToUpload);
            this.upload(fileToUpload)
                .subscribe(result => {
                    //console.log("I am here in returning image");
                    debugger;

                    //let data = result.json();

                    this.ProfilePicUrl = result.text();   //data.result
           

            if(this.ProfilePicUrl != ""){

                this.userMediaContent = new UserMediaContentDto();
                this.userMediaContent.userId = this.userId;
                this.userMediaContent.imageUrl = this.ProfilePicUrl

                this.specificMediaContent = this.filterUserMediaContent(this.userId)

                if(this.specificMediaContent != undefined){
                    this.userMediaContent.id = this.specificMediaContent.id

                    this.userMediaContentServiceProxy.update(this.userMediaContent)
                        .finally(() => { this.saving = false; })
                        .subscribe(() => {
                           this.notifyMsg("Success!" , 'UpdateSuccessfully');
                           this.resetUserPic();
                           this.close();
                           this.modalSave.emit(null);
                    });   
                }
                else{
                    this.userMediaContentServiceProxy.create(this.userMediaContent)
                        .finally(() => { this.saving = false; })
                        .subscribe(() => {
                            this.notifyMsg("Success" , 'SavedSuccessfully');
                            this.resetUserPic();
                            this.close();
                            this.modalSave.emit(null);
                    });
                } 
            }
            else
            {
                this.notifyMsg("Error" , 'Profile Picture is not Save Successfully');
                //abp.message.error("Profile Picture is not Save Successfully");
            }
          });
        }
        else{
            this.notifyMsg("Error" , ('Plz Select Image'));
            this.isDisable = true;
        }   
   }

   filterUserMediaContent(userId: number): UserMediaContentDto{
    return this.mediaContent.find(function (obj) { return obj.userId === userId; });
   }

   resetUserPic(): void{
    this.userPic.nativeElement.value = "";
   }

   // get user media contents
   getUserMediaContent(): void{
    this.userMediaContentServiceProxy.getAllMediaContent()
        .subscribe((result) => {
            this.mediaContent = result
    });
  }

  notifyMsg(tag: string , msg: string){
    swal({
        title: tag,
        text: msg,
        timer: 2000,
        showConfirmButton: false
    }).catch(swal.noop)
  }

}