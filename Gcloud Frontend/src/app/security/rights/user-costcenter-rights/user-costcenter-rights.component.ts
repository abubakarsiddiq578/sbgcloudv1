import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { CostCenterDto, UserCostCenterRightDto , UserCostCenterRightServiceProxy , CostCenterServiceProxy, PagedResultDtoOfCostCenterDto} from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
//import { AppComponentBase } from '@shared/app-component-base';
//import { TreeChildren } from 'Models/TreeChildren';
//import { TreeRoot } from 'Models/TreeRoot';
//import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
declare var $: any;

@Component({
  selector: 'user-costcenter-rights',
  templateUrl: './user-costcenter-rights.component.html',
  providers: [UserCostCenterRightServiceProxy , CostCenterServiceProxy]
})
export class UserCostCenterRightsComponent  /*extends PagedListingComponentBase<CostCenterDto>*/ implements OnInit{    

    @ViewChild('createUserCostCenterRightsModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    savedUserCostCenterRights: boolean = false;
    
    costcenterRights: CostCenterRights[] = [];
    costcenter: CostCenterDto[] = [];
    userCostCenterRights: UserCostCenterRightDto[] = [];
    
    filterCostCenter: CostCenterDto[] = [];

    userId: number;

    globalFunction : GlobalFunctions = new GlobalFunctions

    CostCenters = [
        {name: 'SoftBeats' , right: true},
        {name: 'Lumensoft' , right: false},
        {name: 'Mentor Graphics' , right: true},
        {name: 'Senses' , right: true},
        {name: 'DevC' , right: true},
        {name: 'Siemens' , right: false},
    ];

    IsCreateCostCenter: Boolean = abp.auth.isGranted("Pages.Security.Create.Create_CostCenter");

    userCostCenterRightsModel: UserCostCenterRightDto = null;

    disableBtn: boolean =  false;

    constructor(
        injector: Injector,
        private userCostCenterRightServiceProxy: UserCostCenterRightServiceProxy,
        private costcenterServiceProxy: CostCenterServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    // set costcenter rights value
    setCheckedValue(value: boolean): string {
        if (value == true) {
            return "checked";
        }
        else {
            return "";
        }
    }

    // open form popup
    show(id: number , specificUserCostCenterRights: UserCostCenterRightDto[]): void {
        this.active = true;    
        this.modal.show();

        debugger;
        
        this.userId = id;

        //if rights are exist 
        if(specificUserCostCenterRights.length > 0)
        {
            this.userCostCenterRights = specificUserCostCenterRights;
            this.getNewCostCenter(this.userId);
            //this.backupCostCenterRights();
        }
        // else if rights are not exist 
        else
        {
            this.disableBtn = true;
            this.userCostCenterRights = [];
             //get all costcenters
             this.getAllCostCenters();
        }
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
            //console.log(this.userCostCenterRights);

        let i = 0;

        // update user costcenter rights 
        for (i= 0; i < this.userCostCenterRights.length; i++) {
            this.saving = true;
            this.userCostCenterRightServiceProxy.update(this.userCostCenterRights[i]) 
            .finally(() => {
                this.saving = false; 
                i++;
                if(i == this.userCostCenterRights.length + 1)
                {
                    this.notifyMsg('Success!' , 'SavedSuccessfully');                    
                    this.close();
                    this.modalSave.emit(null);   
                }
                })
            .subscribe(() => {
               console.log('SavedSuccessfully');
            });
        }
    }
    CreateRights(): void{

        if(!this.IsCreateCostCenter){
            this.globalFunction.showNoRightsMessage("Create");
            return
        }
    }
    close(): void {
        let i = 0;

        // reset user company rights if no any user rights are update
        for (i= 0; i < this.costcenterRights.length; i++) {
            let rightId = this.costcenterRights[i].id
            this.userCostCenterRights.find(function (obj) { return obj.id === rightId; }).rights = this.costcenterRights[i].right;
        }
       
        this.active = false;
        this.modal.hide();

        // call refresh method to recall all services
        if(this.savedUserCostCenterRights == true)
        {
            this.savedUserCostCenterRights = false;
            this.modalSave.emit(null);  
        } 
    }

    protected delete(/*user: CostCenterDto*/): void {     
        /*abp.message.confirm(
            "Delete user '" + user.fullName + "'?",
            (result: boolean) => {
                if (result) {
                    this._userService.delete(user.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted User: " + user.fullName);
                            this.refresh();
                        });
                }
            }
        );*/
    }

    // get all costcenter data
    getAllCostCenters()
    {
        this.costcenterServiceProxy.getAllCostCenters()    
            .finally(() => {
            })
            .subscribe((result) => {    
               
                this.costcenter = result;
                console.log(this.costcenter);
                debugger;
                this.fillUserCostCenterRightsModel();
            
            });
    }

    // create user costcenter if they are not exist
    createUserCostCenterRights(userCostCenterRight: UserCostCenterRightDto , iteration: number): void
    {
        this.userCostCenterRightServiceProxy.create(userCostCenterRight)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                console.log("SavedSuccessfully");

                if(iteration == this.costcenter.length - 1){
                    
                    this.getUserCostCenterRights(this.userId);
                }

            });
    }

    // set value of user costcenter rights 
    fillUserCostCenterRightsModel(): void{
        let i;
        for (i= 0; i < this.costcenter.length; i++) {
           
            this.userCostCenterRightsModel = new UserCostCenterRightDto();
            this.userCostCenterRightsModel.init({ isActive: true });
            this.userCostCenterRightsModel.costCenterId = this.costcenter[i].id;
            this.userCostCenterRightsModel.userId = this.userId;
            this.userCostCenterRightsModel.rights = true;

            this.createUserCostCenterRights(this.userCostCenterRightsModel , i)
        }
    }


    // get all user costcenter rights 
    getUserCostCenterRights(userId: number)
    {
        this.userCostCenterRightServiceProxy.getCostCenterRights()    
            .finally(() => {
            })
            .subscribe((result) => {
                this.userCostCenterRights = result.filter(function (obj) { return obj.userId === userId; });
                this.savedUserCostCenterRights = true;
                this.disableBtn = false;
                
                this.backupCostCenterRights();
                console.log(this.userCostCenterRights);
            });
    }

    // back user costcenter rights to restore these rights
    backupCostCenterRights(){
        this.costcenterRights = [];
        
        let i = 0;

        for (i= 0; i < this.userCostCenterRights.length; i++) {
            let _costcenterRights = new CostCenterRights();
            _costcenterRights.id = this.userCostCenterRights[i].id;
            _costcenterRights.right = this.userCostCenterRights[i].rights;
            this.costcenterRights.push(_costcenterRights);
        }
    }

    getNewCostCenter(userId) : void{

        this.userCostCenterRightServiceProxy.getNewCostCenters(userId)   
        .finally(() => {
        })
        .subscribe((result) => {
            debugger;
            this.filterCostCenter = result

            if (this.filterCostCenter.length > 0){
                this.costcenter = this.filterCostCenter
                this.fillUserCostCenterRightsModel();
            }
            else{
                this.backupCostCenterRights();
            }

        });
    }

    notifyMsg(tag: string , msg: string){
        swal({
            title: tag,
            text: msg,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}

class CostCenterRights {
    public id: number;
    public right: boolean;
}