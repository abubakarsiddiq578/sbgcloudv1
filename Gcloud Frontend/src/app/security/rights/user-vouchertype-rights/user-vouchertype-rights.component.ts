import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { VoucherTypeDto, UserVoucherTypeRightDto, VoucherTypeServiceProxy, UserVoucherTypeRightServiceProxy, PagedResultDtoOfVoucherTypeDto} from '@shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
//import { AppComponentBase } from '@shared/app-component-base';
//import { TreeChildren } from 'Models/TreeChildren';
//import { TreeRoot } from 'Models/TreeRoot';
//import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
declare var $: any;

@Component({
  selector: 'user-vouchertype-rights',
  templateUrl: './user-vouchertype-rights.component.html',
  providers: [VoucherTypeServiceProxy , UserVoucherTypeRightServiceProxy]
})
export class UserVoucherTypeRightsComponent  /*extends PagedListingComponentBase<VoucherTypeDto>*/ implements OnInit{

    @ViewChild('createUserVoucherTypeRightsModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    savedUserVoucherTypeRights: boolean = false;
    
    voucherTypeRights: VoucherTypeRights[] = [];
    voucherType: VoucherTypeDto[] = [];
    userVoucherTypeRights: UserVoucherTypeRightDto[] = [];
    userId: number;


    globalFunction : GlobalFunctions = new GlobalFunctions

    IsCreateVouchertype: Boolean = abp.auth.isGranted("Pages.Security.Create.Create_VoucherType");

    userVoucherTypeRightsModel: UserVoucherTypeRightDto = null;

    disableBtn: boolean =  false;

    voucherTypes = [
        {name: 'JV' , right: true},
        {name: 'CPV' , right: false},
        {name: 'CRV' , right: true},
        {name: 'BPV' , right: true},
        {name: 'BRV' , right: true},
        {name: 'PV' , right: false},
    ];

    constructor(
        injector: Injector,
        private voucherTypeServiceProxy: VoucherTypeServiceProxy,
        private userVoucherTypeRightServiceProxy: UserVoucherTypeRightServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    // set vouchertype rights value
    setCheckedValue(value: boolean): string {
        if (value == true) {
            return "checked";
        }
        else {
            return "";
        }
    }

    CreateRights(): void{

        if(!this.IsCreateVouchertype){
            this.globalFunction.showNoRightsMessage("Create");
            return
        }
    }

    // open form popup
    show(id: number , specificUserVoucherTypeRights: UserVoucherTypeRightDto[]): void {
        this.active = true;    
        this.modal.show();
        
        this.userId = id;

        //if rights are exist 
        if(specificUserVoucherTypeRights.length > 0)
        {
            this.userVoucherTypeRights = specificUserVoucherTypeRights;
            this.backupVoucherTypeRights();
        }
        // else if rights are not exist 
        else
        {
            this.disableBtn = true;
            this.userVoucherTypeRights = [];
           //get all VoucherTypes
           this.getAllVoucherTypes();
        }
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        let i = 0;

        // update user vouchertype rights 
        for (i= 0; i < this.userVoucherTypeRights.length; i++) {
            this.saving = true;
            this.userVoucherTypeRightServiceProxy.update(this.userVoucherTypeRights[i])
            .finally(() => {
                this.saving = false; 
                i++;
                if(i == this.userVoucherTypeRights.length + 1)
                {
                    this.notifyMsg('Success!' , 'SavedSuccessfully');
                    this.close();
                    this.modalSave.emit(null);   
                }
                })
            .subscribe(() => {
               console.log('SavedSuccessfully');
            });
        }
    }

    close(): void {
        let i = 0;

        // reset user vouchertype rights if no any user rights are update
        for (i= 0; i < this.voucherTypeRights.length; i++) {
            let rightId = this.voucherTypeRights[i].id
            //this.userVoucherTypeRights.find(function (obj) { return obj.id === rightId; }).rights = this.voucherTypeRights[i].right;
        }
       
        this.active = false;
        this.modal.hide();

        // call refresh method to recall all services
        if(this.savedUserVoucherTypeRights == true)
        {
            this.savedUserVoucherTypeRights = false;
            this.modalSave.emit(null);  
        } 
    }

    protected delete(/*user: VoucherTypeDto*/): void {
        /*abp.message.confirm(
            "Delete user '" + user.fullName + "'?",
            (result: boolean) => {
                if (result) {
                    this._userService.delete(user.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted User: " + user.fullName);
                            this.refresh();
                        });
                }
            }
        );*/
    }

    // get all vouchertype data
    getAllVoucherTypes()
    {
        this.voucherTypeServiceProxy.getAllVoucherTypes()
        .finally(() => {
        })
        .subscribe((result) => {
            this.voucherType = result;
            console.log(this.voucherType);
            this.fillUserVoucherTypeRightsModel();
         });   
    }

    // create user vouchertype if they are not exist
    createUserVoucherTypeRights(userVoucherTypeRight: UserVoucherTypeRightDto , iteration: number): void
    {
        this.userVoucherTypeRightServiceProxy.create(userVoucherTypeRight)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                console.log("SavedSuccessfully");

                if(iteration == this.voucherType.length - 1){
                    
                    this.getUserVoucherTypeRights(this.userId);
                }

            });
    }

    // set value of user vouchertype rights 
    fillUserVoucherTypeRightsModel(): void{
        let i;
        for (i= 0; i < this.voucherType.length; i++) {
           
            this.userVoucherTypeRightsModel = new UserVoucherTypeRightDto();
            this.userVoucherTypeRightsModel.init({ isActive: true });
            this.userVoucherTypeRightsModel.voucherTypeId = this.voucherType[i].id;
            this.userVoucherTypeRightsModel.userId = this.userId;
            this.userVoucherTypeRightsModel.rights = true;

            this.createUserVoucherTypeRights(this.userVoucherTypeRightsModel , i)
        }
    }


    // get all user vouchertype rights 
    getUserVoucherTypeRights(userId: number)
    {
        this.userVoucherTypeRightServiceProxy.getVoucherTypeRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this.userVoucherTypeRights = result.filter(function (obj) { return obj.userId === userId; });
                this.savedUserVoucherTypeRights = true;
                this.disableBtn = false;
                
                this.backupVoucherTypeRights();
                console.log(this.userVoucherTypeRights);
            });
    }

    // back user vouchertype rights to restore these rights
    backupVoucherTypeRights(){
        this.voucherTypeRights = [];
        
        let i = 0;

        for (i= 0; i < this.userVoucherTypeRights.length; i++) {
            let _voucherTypeRights = new VoucherTypeRights();
            _voucherTypeRights.id = this.userVoucherTypeRights[i].id;
            _voucherTypeRights.right = this.userVoucherTypeRights[i].rights;
            this.voucherTypeRights.push(_voucherTypeRights);
        }
    }

    notifyMsg(tag: string , msg: string){
        swal({
            title: tag,
            text: msg,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}

class VoucherTypeRights {
    public id: number;
    public right: boolean;
}