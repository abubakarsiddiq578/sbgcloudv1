import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserAccountRightDto, UserAccountRightServiceProxy, ChartOfAccountSubSubDetailDto, ChartOfAccountSubSubDetailServiceProxy} from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
//import { AppComponentBase } from '@shared/app-component-base';
//import { TreeChildren } from 'Models/TreeChildren';
//import { TreeRoot } from 'Models/TreeRoot';
//import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
declare var $: any;

@Component({
  selector: 'user-account-rights',
  templateUrl: './user-account-rights.component.html',
  providers: [UserAccountRightServiceProxy , ChartOfAccountSubSubDetailServiceProxy]
})
export class UserAccountRightsComponent  /*extends PagedListingComponentBase<ChartOfAccountSubSubDetailDto>*/ implements OnInit{

    @ViewChild('createUserAccountRightsModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    savedUserAccountRights: boolean = false;
    
    accountRights: AccountRights[] = [];
    ChartOfAccountSubSubDetail: ChartOfAccountSubSubDetailDto[] = [];
    userAccountRights: UserAccountRightDto[] = [];
    userId: number;

    userAccountRightsModel: UserAccountRightDto = null;

   
    IsCreateAccount: Boolean = abp.auth.isGranted("Pages.Security.Account.Create_Account");


    Accounts = [
        {name: 'Bank Alfalah' , right: true},
        {name: 'UBL' , right: false},
        {name: 'HBL' , right: true},
        {name: 'Mezzan Bank' , right: true},
        {name: 'MCB' , right: true},
        {name: 'NBP' , right: false},
    ];

    disableBtn: boolean =  false;

    constructor(
        injector: Injector,
        private chartOfAccountSubSubDetailServiceProxy: ChartOfAccountSubSubDetailServiceProxy,
        private userAccountRightServiceProxy: UserAccountRightServiceProxy
    ) {
        //super(injector);
    }
globalFunction : GlobalFunctions = new GlobalFunctions
    ngOnInit(): void {
    }

    // set account rights value
    setCheckedValue(value: boolean): string {
        if (value == true) {
            return "checked";
        }
        else {
            return "";
        }
    }

    // open form popup
    show(id: number , specificUserAccountRights: UserAccountRightDto[]): void {
        debugger;

        this.active = true;    
        this.modal.show();
        
        this.userId = id;

        //if rights are exist 
        if(specificUserAccountRights.length > 0)
        {
            this.userAccountRights = specificUserAccountRights;
            this.backupAccountRights();
        }
        // else if rights are not exist 
        else
        {
            this.disableBtn = true;
            this.userAccountRights = [];
            //get all ChartOfAccountSubSubMain
            this.getAllAccounts();
        }
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        let i = 0;

        // update user account rights 
        for (i= 0; i < this.userAccountRights.length; i++) {
            this.saving = true;
            this.userAccountRightServiceProxy.update(this.userAccountRights[i])
            .finally(() => {
                this.saving = false; 
                i++;
                if(i == this.userAccountRights.length + 1)
                {
                    this.notifyMsg('Success!' , 'SavedSuccessfully');
                    this.close();
                    this.modalSave.emit(null);   
                }
                })
            .subscribe(() => {
               console.log('SavedSuccessfully');
            });
        }
    }
    CreateRights(): void{

        if(!this.IsCreateAccount){
            this.globalFunction.showNoRightsMessage("Create");
            return
        }
    }
    close(): void {
        let i = 0;

        // reset user account rights if no any user rights are update
        for (i= 0; i < this.accountRights.length; i++) {
            let rightId = this.accountRights[i].id
            this.userAccountRights.find(function (obj) { return obj.id === rightId; }).rights = this.accountRights[i].right;
        }
       
        this.active = false;
        this.modal.hide();

        // call refresh method to recall all services
        if(this.savedUserAccountRights == true)
        {
            this.savedUserAccountRights = false;
            this.modalSave.emit(null);  
        } 
    }

    protected delete(/*user: ChartOfAccountSubSubDetailDto*/): void {
        /*abp.message.confirm(
            "Delete user '" + user.fullName + "'?",
            (result: boolean) => {
                if (result) {
                    this._userService.delete(user.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted User: " + user.fullName);
                            this.refresh();
                        });
                }
            }
        );*/
    }

    // get all accounts data
    getAllAccounts()
    {
        this.chartOfAccountSubSubDetailServiceProxy.getAllCashBankAccounts()
            .finally(() => {
            })
            .subscribe((result) => {
               result;
                this.ChartOfAccountSubSubDetail = result;
                console.log(this.ChartOfAccountSubSubDetail);
                this.fillUserAccountRightsModel();
            });
    }

    // create user account if they are not exist
    createUserAccountRights(userAccountRight: UserAccountRightDto , iteration: number): void
    {
        this.userAccountRightServiceProxy.create(userAccountRight)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                console.log("SavedSuccessfully");

                if(iteration == this.ChartOfAccountSubSubDetail.length - 1){
                    
                    this.getUserAccountRights(this.userId);
                }

            });
    }

    // set value of user account rights 
    fillUserAccountRightsModel(): void{
        let i;
        for (i= 0; i < this.ChartOfAccountSubSubDetail.length; i++) {
           
            this.userAccountRightsModel = new UserAccountRightDto();
            this.userAccountRightsModel.init({ isActive: true });
            this.userAccountRightsModel.accountId = this.ChartOfAccountSubSubDetail[i].id;
            this.userAccountRightsModel.userId = this.userId;
            this.userAccountRightsModel.rights = true;

            this.createUserAccountRights(this.userAccountRightsModel , i)
        }
    }


    // get all user account rights 
    getUserAccountRights(userId: number)
    {
        this.userAccountRightServiceProxy.getAccountRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this.userAccountRights = result.filter(function (obj) { return obj.userId === userId; });
                this.savedUserAccountRights = true;
                this.disableBtn = false;
                
                this.backupAccountRights();
                console.log(this.userAccountRights);
            });
    }

    // back user account rights to restore these rights
    backupAccountRights(){
        this.accountRights = [];
        
        let i = 0;

        for (i= 0; i < this.userAccountRights.length; i++) {
            let _accountRights = new AccountRights();
            _accountRights.id = this.userAccountRights[i].id;
            _accountRights.right = this.userAccountRights[i].rights;
            this.accountRights.push(_accountRights);
        }
    }

    notifyMsg(tag: string , msg: string){
        swal({
            title: tag,
            text: msg,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}

class AccountRights {
    public id: number;
    public right: boolean;
}