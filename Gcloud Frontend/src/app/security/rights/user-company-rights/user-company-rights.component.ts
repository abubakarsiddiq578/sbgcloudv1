import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserCompanyRightDto , CompanyInfoServiceProxy , PagedResultDtoOfCompanyInfoDto , CompanyInfoDto , UserCompanyRightServiceProxy, PagedResultDtoOfUserCompanyRightDto} from '@shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
//import { AppComponentBase } from '@shared/app-component-base';
//import { TreeChildren } from 'Models/TreeChildren';
//import { TreeRoot } from 'Models/TreeRoot';
//import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
declare var $: any;

@Component({
  selector: 'user-company-rights',
  templateUrl: './user-company-rights.component.html',
  providers: [CompanyInfoServiceProxy , UserCompanyRightServiceProxy]
})
export class UserCompanyRightsComponent /*extends PagedListingComponentBase<CompanyInfoDto>*/ implements OnInit{

    @ViewChild('createUserCompanyRightsModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('jstreearea') jsTree: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    savedUserCompanyRights: boolean = false;
    //
    IsCreateCompany: Boolean = abp.auth.isGranted("Pages.Security.Company.Create_Company");
    //
    companyRights: CompanyRights[] = [];
    companyInfo: CompanyInfoDto[] = [];
    userCompanyRights: UserCompanyRightDto[] = [];
    userId: number;

    Companies = [
        {name: 'OTC' , right: true},
        {name: 'UDL' , right: false},
        {name: 'FRP' , right: true},
        {name: 'The Mall' , right: true},
        {name: 'TUV' , right: true},
        {name: 'Smart Telecom' , right: false},
    ];

    userCompanyRightsModel: UserCompanyRightDto = null;

    disableBtn: boolean =  false;

    /*Role = [
        {"name": "Car"},
        {"name": "Bike"},
        {"name": "Boat"},
        {"name": "Plane"}
    ];*/


   // IsView: Boolean = abp.auth.isGranted("Pages.Security.Company.View_Company");
    constructor(
        injector: Injector,
        private companyInfoServiceProxy: CompanyInfoServiceProxy,
        private userCompanyRightServiceProxy: UserCompanyRightServiceProxy, 
        private _router: Router
    ) {
        //super(injector);
    }

    globalFunction : GlobalFunctions = new GlobalFunctions

    ngOnInit(): void {

      
      
    }

    /*protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {

        //get all companies
        this.getAllCompanies(finishedCallback);
    }*/

    // set company rights value
    setCheckedValue(value: boolean): string {
        if (value == true) {
            return "checked";
        }
        else {
            return "";
        }
    }

    // open form popup
    show(id: number , specificUserCompanyRights: UserCompanyRightDto[]): void {
        this.active = true;    
        this.modal.show();
        
        this.userId = id;

        //if rights are exist 
        if(specificUserCompanyRights.length > 0)
        {
            this.userCompanyRights = specificUserCompanyRights;
            this.backupCompanyRights();
        }
        // else if rights are not exist 
        else
        {
            this.disableBtn = true;
            this.userCompanyRights = [];
            
            //get all companies
            this.getAllCompanies();
        }
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

        CreateRights(): void{

            if(!this.IsCreateCompany){
                this.globalFunction.showNoRightsMessage("Create");
                return
            }

            
        }
    save(): void {       

        let i = 0;

        // update user company rights 
        for (i= 0; i < this.userCompanyRights.length; i++) {
            this.saving = true;
            this.userCompanyRightServiceProxy.update(this.userCompanyRights[i])
            .finally(() => {
                this.saving = false; 
                i++;
                if(i == this.userCompanyRights.length + 1)
                {
                    this.notifyMsg('Success!' , 'SavedSuccessfully');
                    this.close();
                    this.modalSave.emit(null);   
                }
                })
            .subscribe(() => {
               console.log('SavedSuccessfully');
            });
        }
    }

    close(): void {
        let i = 0;

        // reset user company rights if no any user rights are update
        for (i= 0; i < this.companyRights.length; i++) {
            let rightId = this.companyRights[i].id
            this.userCompanyRights.find(function (obj) { return obj.id === rightId; }).rights = this.companyRights[i].right;
        }
       
        this.active = false;
        this.modal.hide();

        // call refresh method to recall all services
        if(this.savedUserCompanyRights == true)
        {
            this.savedUserCompanyRights = false;
            this.modalSave.emit(null);  
        } 
    }

    protected delete(/*user: CompanyInfoDto*/): void {
        /*abp.message.confirm(
            "Delete user '" + user.fullName + "'?",
            (result: boolean) => {
                if (result) {
                    this._userService.delete(user.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted User: " + user.fullName);
                            this.refresh();
                        });
                }
            }
        );*/
    }

    // get all companies data
    getAllCompanies()
    {
        this.companyInfoServiceProxy.getAllCompanies()
            .finally(() => {
            })
            .subscribe((result) => {
                this.companyInfo = result;
                console.log(this.companyInfo);
                this.fillUserCompanyRightsModel();
            });
    }

    // create user comapny if they are not exist
    createUserCompanyRights(userCompanyRight: UserCompanyRightDto , iteration: number): void
    {
        this.userCompanyRightServiceProxy.create(userCompanyRight)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                //this.notify.info(this.l('SavedSuccessfully'));
                //this.close();
                //this.modalSave.emit(null);
                console.log("SavedSuccessfully");

                if(iteration == this.companyInfo.length - 1){
                    
                    this.getUserCompanyRights(this.userId);
                }

            });
    }

    // set value of user company rights 
    fillUserCompanyRightsModel(): void{
        let i;
        for (i= 0; i < this.companyInfo.length; i++) {
           
            this.userCompanyRightsModel = new UserCompanyRightDto();
            this.userCompanyRightsModel.init({ isActive: true });
            this.userCompanyRightsModel.companyId = this.companyInfo[i].id;
            this.userCompanyRightsModel.userId = this.userId;
            this.userCompanyRightsModel.rights = true;

            this.createUserCompanyRights(this.userCompanyRightsModel , i)
        }
    }


    // get all user company rights 
    getUserCompanyRights(userId: number)
    {
        this.userCompanyRightServiceProxy.getCompanyRights()
            .finally(() => {
            })
            .subscribe((result) => {
                
                this.userCompanyRights = result.filter(function (obj) { return obj.userId === userId; });
                this.savedUserCompanyRights = true;
                this.disableBtn = false;
                //this.userCompanyRights = result;
                this.backupCompanyRights();
                console.log(this.userCompanyRights);
            });
    }

    // back user company rights to restore these rights
    backupCompanyRights(){
        this.companyRights = [];
        
        let i = 0;

        for (i= 0; i < this.userCompanyRights.length; i++) {
            let _companyRights = new CompanyRights();
            _companyRights.id = this.userCompanyRights[i].id;
            _companyRights.right = this.userCompanyRights[i].rights;
            this.companyRights.push(_companyRights);
        }
    }

    notifyMsg(tag: string , msg: string){
        swal({
            title: tag,
            text: msg,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}

class CompanyRights {
    public id: number;
    public right: boolean;
}