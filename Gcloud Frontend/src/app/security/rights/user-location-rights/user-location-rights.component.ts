import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { LocationInfoDto, UserLocationRightDto, LocationInfoServiceProxy, UserLocationRightServiceProxy, PagedResultDtoOfLocationInfoDto} from '@shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
//import { AppComponentBase } from '@shared/app-component-base';
//import { TreeChildren } from 'Models/TreeChildren';
//import { TreeRoot } from 'Models/TreeRoot';
//import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
declare var $: any;

@Component({
  selector: 'user-location-rights',
  templateUrl: './user-location-rights.component.html',
  providers: [LocationInfoServiceProxy , UserLocationRightServiceProxy]
})
export class UserLocationRightsComponent  /*extends PagedListingComponentBase<LocationInfoDto>*/ implements OnInit{

    @ViewChild('createUserLocationRightsModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    savedUserLocationRights: boolean = false;
    
    locationRights: LocationRights[] = [];
    locationInfo: LocationInfoDto[] = [];
    userLocationRights: UserLocationRightDto[] = [];
    userId: number;

    IsCreateLocation: Boolean = abp.auth.isGranted("Pages.Security.Location.Create_Location");
    globalFunction : GlobalFunctions = new GlobalFunctions
    Locations = [
        {name: 'Harbanspura' , right: true},
        {name: 'Aamir Town' , right: false},
        {name: 'Johar Town' , right: true},
        {name: 'Akbar Chowk' , right: true},
        {name: 'Iqbal Town' , right: true},
        {name: 'Hunza' , right: false},
    ];

    userLocationRightsModel: UserLocationRightDto = null;

    disableBtn: boolean =  false;

    constructor(
        injector: Injector,
        private locationInfoServiceProxy: LocationInfoServiceProxy,
        private userLocationRightServiceProxy: UserLocationRightServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    // set location rights value
    setCheckedValue(value: boolean): string {
        if (value == true) {
            return "checked";
        }
        else {
            return "";
        }
    }

    CreateRights(): void{

        if(!this.IsCreateLocation){
            this.globalFunction.showNoRightsMessage("Create");
            return
        }
    }

    // open form popup
    show(id: number , specificUserLocationRights: UserLocationRightDto[]): void {
    
        this.active = true;    
        this.modal.show();
        
        this.userId = id;

        //if rights are exist 
        if(specificUserLocationRights.length > 0)
        {
            this.userLocationRights = specificUserLocationRights;
            this.backupLocationRights();
        }
        // else if rights are not exist 
        else
        {
            this.disableBtn = true;
            this.userLocationRights = [];
            
            //get all locations
            this.getAllLocations();
        }
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        let i = 0;

        // update user location rights 
        for (i= 0; i < this.userLocationRights.length; i++) {
            this.saving = true;
            this.userLocationRightServiceProxy.update(this.userLocationRights[i])
            .finally(() => {
                this.saving = false; 
                i++;
                if(i == this.userLocationRights.length + 1)
                {
                    this.notifyMsg('Success!' , 'SavedSuccessfully');
                    this.close();
                    this.modalSave.emit(null);   
                }
                })
            .subscribe(() => {
               console.log('SavedSuccessfully');
            });
        }
    }

    close(): void {
        let i = 0;

        // reset user location rights if no any user rights are update
        for (i= 0; i < this.locationRights.length; i++) {
            let rightId = this.locationRights[i].id
            this.userLocationRights.find(function (obj) { return obj.id === rightId; }).rights = this.locationRights[i].right;
        }
       
        this.active = false;
        this.modal.hide();

        // call refresh method to recall all services
        if(this.savedUserLocationRights == true)
        {
            this.savedUserLocationRights = false;
            this.modalSave.emit(null);  
        } 
    }

    protected delete(/*user: LocationInfoDto*/): void {
        /*abp.message.confirm(
            "Delete user '" + user.fullName + "'?",
            (result: boolean) => {
                if (result) {
                    this._userService.delete(user.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted User: " + user.fullName);
                            this.refresh();
                        });
                }
            }
        );*/
    }

    // get all locations data
    getAllLocations()
    {
        this.locationInfoServiceProxy.getAllLocation()
            .finally(() => {
            })
            .subscribe((result) => {
                this.locationInfo = result;
                console.log(this.locationInfo);
                this.fillUserLocationRightsModel();
            });
    }

    // create user location if they are not exist
    createUserLocationRights(userLocationRight: UserLocationRightDto , iteration: number): void
    {
        this.userLocationRightServiceProxy.create(userLocationRight)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                console.log("SavedSuccessfully");

                if(iteration == this.locationInfo.length - 1){
                    
                    this.getUserLocationRights(this.userId);
                }

            });
    }

    // set value of user location rights 
    fillUserLocationRightsModel(): void{
        let i;
        for (i= 0; i < this.locationInfo.length; i++) {
           
            this.userLocationRightsModel = new UserLocationRightDto();
            this.userLocationRightsModel.init({ isActive: true });
            this.userLocationRightsModel.locationId = this.locationInfo[i].id;
            this.userLocationRightsModel.userId = this.userId;
            this.userLocationRightsModel.rights = true;

            this.createUserLocationRights(this.userLocationRightsModel , i)
        }
    }


    // get all user location rights 
    getUserLocationRights(userId: number)
    {
        this.userLocationRightServiceProxy.getLocationRights()
            .finally(() => {
            })
            .subscribe((result) => {
                this.userLocationRights = result.filter(function (obj) { return obj.userId === userId; });
                this.savedUserLocationRights = true;
                this.disableBtn = false;
                
                this.backupLocationRights();
                console.log(this.userLocationRights);
            });
    }

    // back user location rights to restore these rights
    backupLocationRights(){
        this.locationRights = [];
        
        let i = 0;

        for (i= 0; i < this.userLocationRights.length; i++) {
            let _locationRights = new LocationRights();
            _locationRights.id = this.userLocationRights[i].id;
            _locationRights.right = this.userLocationRights[i].rights;
            this.locationRights.push(_locationRights);
        }
    }

    notifyMsg(tag: string , msg: string){
        swal({
            title: tag,
            text: msg,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}

class LocationRights {
    public id: number;
    public right: boolean;
}