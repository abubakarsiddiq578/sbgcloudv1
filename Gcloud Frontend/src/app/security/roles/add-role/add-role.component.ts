import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { RoleServiceProxy, CreateRoleDto, ListResultDtoOfPermissionDto } from '@shared/service-proxies/service-proxies';
//import { AppComponentBase } from '@shared/app-component-base';
import { TreeChildren } from 'Models/TreeChildren';
import { TreeRoot } from 'Models/TreeRoot';
import swal from 'sweetalert2';
declare var $: any;

@Component({
    selector: 'add-role-modal',
    templateUrl: './add-role.component.html'
})
export class AddRoleComponent /*extends AppComponentBase*/ implements OnInit {
    @ViewChild('createRoleModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('jstreearea') jsTree: ElementRef;

    active: boolean = false;
    saving: boolean = false;

    permissions: ListResultDtoOfPermissionDto = null;
    role: CreateRoleDto = null;

    rootnode: any[];
    public root1: TreeRoot = new TreeRoot();
    children: TreeChildren = new TreeChildren();

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    constructor(
        injector: Injector,
        private _roleService: RoleServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
        this._roleService.getAllPermissions()
            .subscribe((permissions: ListResultDtoOfPermissionDto) => {
                this.permissions = permissions;
                this.permissions = permissions;
                console.log(this.permissions);
                this.rootnode = [];
                this.permissions = permissions;
                console.log(this.permissions.items);

                let v = 0;
                this.root1.children = [];

                for (v = 2; v < this.permissions.items.length; v++) {
                    if (this.permissions.items[v].name.indexOf("_") > 0) {

                        this.children = new TreeChildren();
                        this.children.name = this.permissions.items[v].displayName;
                        this.children.value = this.permissions.items[v].name;
                        this.children.NodeId = this.permissions.items[v].id;
                        this.root1.children.push(this.children);
                    }
                    else {
                        if (this.root1.children.length > 0) {
                            this.rootnode.push(this.root1);
                        }

                        this.root1 = new TreeRoot();
                        this.root1.children = [];
                        this.root1.name = this.permissions.items[v].displayName;
                        this.root1.value = this.permissions.items[v].name;
                        this.root1.NodeId = this.permissions.items[v].id;
                    }
                }

                this.rootnode.push(this.root1);
                console.log(this.rootnode);
                this.root1 = new TreeRoot();
                this.active = true;
        this.role = new CreateRoleDto();
        this.role.init({ isStatic: false });
            });
    }

    show(): void {

        //this.active = true;     // temporary

        $(this.jsTree.nativeElement).jstree(
            {
                "plugins": ["checkbox"]
            });
        console.log(this.modalContent);
        this.modal.show();

        console.log($('#data'));
      
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        debugger;
        var permissions = [];
        var editpermission= $(this.jsTree.nativeElement).jstree(true).get_selected('full',true);
        let j=0;
        for(j=0;j<editpermission.length;j++)
        {
            if(permissions.indexOf(editpermission[j].parent)<0)
            {
                permissions.push(editpermission[j].parent);
            }
            permissions.push(editpermission[j].id);
        }

        console.log(permissions);

        this.role.permissions = permissions;

        this.role.permissions = permissions;

        this.saving = true;
        this._roleService.create(this.role)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notifyMsg('Success!' , 'SavedSuccessfully');
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = true;
        this.modal.hide();
    }

    notifyMsg(tag: string , msg: string){
        swal({
            title: tag,
            text: msg,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}
