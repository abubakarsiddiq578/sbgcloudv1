import { NgModule, APP_INITIALIZER, Injector, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { JsonpModule} from '@angular/http';
import { ServiceProxyModule } from '../app/shared/service-proxies/service-proxies.module';
import { BrowserModule }    from '@angular/platform-browser';
import { HttpClientModule , HttpResponse, HTTP_INTERCEPTORS} from '@angular/common/http';
import { StorageServiceModule} from 'angular-webstorage-service';
 import { MatFormFieldModule } from '@angular/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AgGridModule } from 'ag-grid-angular';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
  
} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { AppComponent } from './app.component';

import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedpluginModule} from './shared/fixedplugin/fixedplugin.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

import { MatMomentDatetimeModule } from "@mat-datetimepicker/moment";
import { MatDatetimepickerModule } from "@mat-datetimepicker/core";

import { AppRoutes } from './app.routing';

//import { AbpModule } from '../../node_modules/abp-ng2-module/src/abp.module';
//import { AbpModule } from 'abp-ng2-module/src/abp.module';
 import { AbpModule } from '@abp/abp.module';
 import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';
 import { AbpUserConfigurationService } from '@abp/abp-user-configuration.service'; 
//import { AbpHttpInterceptor } from 'abp-ng2-module/src/abpHttpInterceptor';
 //import { AbpHttpInterceptor } from '../../node_modules/abp-ng2-module/src/abpHttpInterceptor';

 import { AppConsts } from 'app/shared/AppConsts';
 import { AppSessionService } from 'app/shared/session/app-session.service';
 import { API_BASE_URL } from 'app/shared/service-proxies/service-proxies';

 import { AppPreBootstrap } from './AppPreBootstrap';
 import { AppRouteGuard } from '@app/shared/auth/auth-route-guard';



export function appInitializerFactory(injector: Injector) {
  return () => {
    abp.ui.setBusy();
    return new Promise<boolean>((resolve, reject) => {
      AppPreBootstrap.run(() => {
        abp.event.trigger('abp.dynamicScriptsInitialized');
        var appSessionService: AppSessionService = injector.get(AppSessionService);
        appSessionService.init().then(
          (result) => {
            abp.ui.clearBusy();
            resolve(result);
          },
          (err) => {
            abp.ui.clearBusy();
            reject(err);
          }
        );
      });
    });
  }
}

export function getRemoteServiceBaseUrl(): string {
  return AppConsts.remoteServiceBaseUrl;
}

export function getCurrentLanguage(): string {
    return abp.localization.currentLanguage.name;
}

@NgModule({
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule
  ],
  declarations: [
    
  ]
})
export class MaterialModule {}

@NgModule({
    imports:      [
        CommonModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule.forRoot(AppRoutes),
        HttpModule,
        MaterialModule,
        MatNativeDateModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedpluginModule,
        ModalModule.forRoot(),
        BrowserModule,
        HttpClientModule,  
        JsonpModule,
        ServiceProxyModule,
        MatMomentDatetimeModule,
        MatDatetimepickerModule,
        AbpModule,
        StorageServiceModule,
        MatSelectModule,
        MatFormFieldModule,
        NgxMatSelectSearchModule,
        AgGridModule.withComponents([])
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent
    ],

    providers: [
      { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true },
      { provide: API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
      {
        provide: APP_INITIALIZER,
        useFactory: appInitializerFactory,
        deps: [Injector],
        multi: true
      },
      
      AppSessionService,
      AppRouteGuard,
      {
            provide: LOCALE_ID,
            useFactory: getCurrentLanguage
      }
    ],

    bootstrap:    [ AppComponent ]
})
export class AppModule { }
