import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../../app.module';
import { HrRoutes } from './hr.routing';
import { ModalModule } from 'ngx-bootstrap';
import { AgGridModule } from 'ag-grid-angular';

import { DesignationComponent } from './Designation/designation.component';
import { AddDesignationComponent } from './Designation/add-designation/add-designation.component';
import { EditDesignationComponent } from './Designation/edit-designation/edit-designation.component';

import { DepartmentComponent } from './department/department.component';
import { AddDepartmentComponent } from './department/add-department/add-department.component';
import { EditDepartmentComponent } from './department/edit-department/edit-department.component';
import { LeaveRequestComponent } from './leave-request/leave-request.component';
import { AddLeaveRequestComponent } from './leave-request/add-leave-request/add-leave-request.component';
import { EditLeaveRequestComponent } from './leave-request/edit-leave-request/edit-leave-request.component';
import { HolidayComponent } from './holiday/holiday.component';
import { AddHolidayComponent } from './holiday/add-holiday/add-holiday.component';
import { EditHolidayComponent } from './holiday/edit-holiday/edit-holiday.component';
import { DeductionComponent } from './deduction/deduction.component';
import { AddDeductionComponent } from './deduction/add-deduction/add-deduction.compnent';
import { EditDeductionComponent } from './deduction/edit-deduction/edit-deduction.component';
import { TransferComponent } from './transfer/transfer.component';
import { AddTransferComponent } from './transfer/add-transfer/add-transfer.component';
import { EditTransferComponent } from './transfer/edit-transfer/edit-transfer.component';
import { LeaveQuotaComponent } from './leave-quota/leave-quota.component';
import { AddLeaveQuotaComponent } from './leave-quota/add-leave-quota/add-leave-quota.component';
import { EditLeaveQuotaComponent } from './leave-quota/edit-leave-quota/edit-leave-quota.component';
import { PromotionComponent } from './promotion/promotion.component';
import { AddPromotionComponent } from './promotion/add-promotion/add-promotion.component';
import { EditPromotionComponent } from './promotion/edit-promotion/edit-promotion.component';

import { TaxSlabComponent } from './tax-slab/tax-slab.component';
import { AddTaxSlabComponent } from './tax-slab/add-tax-slab/add-tax-slab.component';
import { EditTaxSlabComponent } from './tax-slab/edit-tax-slab/edit-tax-slab.component';
import { AllowanceComponent } from './allowance/allowance.component';
import { AddAllowanceComponent } from './allowance/add-allowance/add-allowance.component';
import { EditAllowanceComponent } from './allowance/edit-allowance/edit-allowance.component';
import { BonusComponent } from './bonus/bonus.component';
import { AddBonusComponent } from './bonus/add-bonus/add-bonus.component';
import { EditBonusComponent } from './bonus/edit-bonus/edit-bonus.component';
import { FineComponent } from './fine/fine.component';
import { AddFineComponent } from './fine/add-fine/add-fine.component';
import { EditFineComponent } from './fine/edit-fine/edit-fine.component';
import { WarningComponent } from './warning/warning.component';
import { AddWarningComponent } from './warning/add-warning/add-warning.component';
import { EditWarningComponent } from './warning/edit-warning/edit-warning.component';
import { TerminationComponent } from './termination/termination.component';
import { AddTerminationComponent } from './termination/add-termination/add-termination.component';
import { EditTerminationComponent } from './termination/edit-termination/edit-termination.component';
import { EmployeeExitComponent } from './employee-exit/employee-exit.component';
import { AddEmployeeExitComponent } from './employee-exit/add-employee-exit/add-employee-exit.component';
import { EditEmployeeExitComponent } from './employee-exit/edit-employee-exit/edit-employee-exit.component';
import { EmployeeWizardComponent } from './employee-wizard/employee-wizard.component';

import { JobTypeComponent } from './job-type/job-type.component';
import { AddJobTypeComponent } from './job-type/add-job-type/add-job-type.component';
import { EditJobTypeComponent } from './job-type/edit-job-type/edit-job-type.component';




import { LeaveTypeComponent } from './leave-type/leave-type.component';
import { AddLeaveTypeComponent } from './leave-type/add-leave-type/add-leave-type.component';
import { EditLeaveTypeComponent } from './leave-type/edit-leave-type/edit-leave-type.component';

import { AdditionalLeaveComponent } from './additional-leave/additional-leave.component';
import { AddAdditionalLeaveComponent } from './additional-leave/add-additional-leave/add-additional-leave.component';
import { EditAdditionalLeaveComponent } from './additional-leave/edit-additional-leave/edit-additional-leave.component';

import { AllowanceTypeComponent } from './allowance-type/allowance-type.component';
import { AddAllowanceTypeComponent } from './allowance-type/add-allowance-type/add-allowance-type.component';
import { EditAllowanceTypeComponent } from './allowance-type/edit-allowance-type/edit-allowance-type.component';

import { BonusTypeComponent } from './bonus-type/bonus-type.component';
import { AddBonusTypeComponent } from './bonus-type/add-bonus-type/add-bonus-type.component';
import { EditBonusTypeComponent } from './bonus-type/edit-bonus-type/edit-bonus-type.component';

import { AttendanceComponent } from './attendance/attendance.component';
import { AddAttendanceComponent } from './attendance/add-attendance/add-attendance.component';
import { EditAttendanceComponent } from './attendance/edit-attendance/edit-attendance.component';

import { AttendanceTypeComponent } from './attendanceType/attendanceType.component';
import { AddAttendanceTypeComponent } from './attendanceType/add-attendanceType/add-attendanceType.component';
import { EditAttendanceTypeComponent } from './attendanceType/edit-attendanceType/edit-attendanceType.component';

import { AttendanceSetupComponent } from './attendance-setup/attendance-setup.component';

import { ItemComponent } from './item/item.component';
import { AddItemComponent } from './item/add-item/add-item.component';
import { EditItemComponent } from './item/edit-item/edit-item.component';

import { ItemTypeComponent } from './item-type/item-type.component';
import { AddItemTypeComponent } from './item-type/add-item-type/add-item-type.component';
import { EditItemTypeComponent } from './item-type/edit-item-type/edit-item-type.component';

import { CategoryComponent } from './category/category.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';

import { SubCategoryComponent } from './sub-category/sub-category.component';
import { AddSubCategoryComponent } from './sub-category/add-sub-category/add-sub-category.component';
import { EditSubCategoryComponent } from './sub-category/edit-sub-category/edit-sub-category.component';
import { FineTypeComponent } from './fine-type/fine-type.component';
import { AddFineTypeComponent } from './fine-type/add-fine-type/add-fine-type.component';
import { EditFineTypeComponent } from './fine-type/edit-fine-type/edit-fine-type.component';

import { DeductionTypeComponent } from './deduction-type/deduction-type.component';
import { AddDeductionTypeComponent } from './deduction-type/add-deduction/add-deduction.component';
import { EditDeductionTypeComponent } from './deduction-type/edit-deduction/edit-deduction.component';




import { SalaryTypeComponent } from './Salary-Type/salary-type.component';
import { AddSalaryTypeComponent } from './Salary-Type/add-salary-type/add-salary-type.component';
import { EditSalaryTypeComponent } from './Salary-Type/edit-salary-type/edit-salary-type.component';

import { LoanRequestComponent } from './Loan-Request/loan-request.component';
import { AddLoanRequestComponent } from './Loan-Request/add-loan-request/add-loan-request.component';
import { EditLoanRequestComponent } from './Loan-Request/edit-loan-request/edit-loan-request.component';

import { SalaryComponent } from "./salary/salary.component";
import { EditSalaryItemComponent } from "./salary/edit-salary-item/edit-salary-item.component";

import { SalaryGroupComponent } from "./salary-group/salary-group.component";
import { EditSalaryGroupComponent } from "./salary-group/edit-salary-group/edit-salary-group.component";

import { ComingSoonComponent } from '../coming-soon/coming-soon.component';
import { ComingSoon2Component } from '../coming-soon2/coming-soon2.component';
import { ComingSoon3Component } from '../coming-soon3/coming-soon3.component';
import { ComingSoon4Component } from '../coming-soon4/coming-soon4.component';
import { ComingSoon5Component } from '../coming-soon5/coming-soon5.component';
import { ComingSoon6Component } from '../coming-soon6/coming-soon6.component';
import { ComingSoon7Component } from '../coming-soon7/coming-soon7.component';
import { ComingSoon8Component } from '../coming-soon8/coming-soon8.component';
import { ComingSoon9Component } from '../coming-soon9/coming-soon9.component';
import { ComingSoon10Component } from '../coming-soon10/coming-soon10.component';
import { ComingSoon11Component } from '../coming-soon11/coming-soon11.component';
import { ComingSoon12Component } from '../coming-soon12/coming-soon12.component';
import { ComingSoon13Component } from '../coming-soon13/coming-soon13.component';
import { ComingSoon14Component } from '../coming-soon14/coming-soon14.component';
import { ComingSoon15Component } from '../coming-soon15/coming-soon15.component';
import { ComingSoon16Component } from '../coming-soon16/coming-soon16.component';
import { ComingSoon17Component } from '../coming-soon17/coming-soon17.component';
import { ComingSoon18Component } from '../coming-soon18/coming-soon18.component';
import { ComingSoon19Component } from '../coming-soon19/coming-soon19.component';
import { ComingSoon20Component } from '../coming-soon20/coming-soon20.component';
import { ComingSoon21Component } from '../coming-soon21/coming-soon21.component';
import { EmployeeListComponent } from '@app/hrm/hr/employee-list/employee-list.component';
import { OvertimeConfigurationComponent } from './configuration/overtime-configuration/overtime-configuration.component';
import { EditOvertimeConfigurationComponent } from './configuration/overtime-configuration/edit-overtime-configuration/edit-overtime-configuration.component';




//import {  } from '../hr/Configurations/edit-overtimeConfiguration/edit-overtimeConfiguration.component';


///Resignation Component//


import { ResignationComponent } from './resignation/resignation.component';
import { AddResignationComponent } from './resignation/add-resignation/add-resignation.component';
import { EditResignationComponent } from './resignation/edit-resignation/edit-resignation.component';
import { AchievementComponent } from './achievement/achievement.component';
import { AddAchievementComponent } from './achievement/add-achievement/add-achievement.component';
import { EditAchievementComponent } from './achievement/edit-achievement/edit-achievement.component';

import { MatMomentDatetimeModule } from "@mat-datetimepicker/moment";
import { MatDatetimepickerModule } from "@mat-datetimepicker/core";
import { AdvanceRequestComponent } from './advance-request/advance-request.component';
import { AddAdvanceRequestComponent } from './advance-request/add-advance-request/add-advance-request.component';
import { EditAdvanceRequestComponent } from './advance-request/edit-advance-request/edit-advance-request.component';

import { CostCenterComponent } from './cost-center/cost-center.component';
import { AddCostCenterComponent } from './cost-center/add-cost-center/add-cost-center.component';
import { EditCostCenterComponent } from './cost-center/edit-cost-center/edit-cost-center.component';
import { AddOvertimeComponent } from './overtime/add-overtime/add-overtime.component';
import { ChildEditOvertimeComponent } from './overtime/edit-overtime/child-edit-overtime/child-edit-overtime.component';
import { OvertimeComponent } from './overtime/overtime.component';
import { EditOvertimeComponent } from '@app/hrm/hr/overtime/edit-overtime/edit-overtime.component';
import { SalaryWizardComponent } from '@app/hrm/hr/salary-wizard/salary-wizard.component';
import { GroupComponent } from '@app/hrm/hr/group/group.component';
import { EditGroupComponent } from '@app/hrm/hr/group/edit-group/edit-group.component';

import { SalaryListComponent } from './salary-list/salaryList.component';
import { OvertimeNewComponent } from './overtime-new/overtime-new.component';
//import { ProductionRateComponent } from '@app/hrm/hr/production-rate/production-rate.component';
import { EditRepairRateComponent } from '@app/hrm/hr/production-rate/edit-repair-rate/edit-repair-rate.component';
import { ItemWiseProductionComponent } from './item-wise-production/item-wise-production.component';
import { EditItemWiseProductionComponent } from './item-wise-production/edit-item-wise-production/edit-item-wise-production.component';


//import { CompanyComponent } from './company/company.component';
//import { AddCompanyComponent } from './company/add-company/add-company.component';
//import { EditCompanyComponent } from './company/edit-company/edit-company.component';
import { SearchItemTypeComponent } from './item/search-item-type/search-item-type.component';
import { SearchCategoryComponent } from './item/search-category/search-category.component';
import { SearchSubCategoryComponent } from './item/search-sub-category/search-sub-category.component';
import { SearchItemComponent } from './production-rate-new/search-item/search-item.component';
import { SearchEmployeeComponent } from './promotion/search-employee/search-employee.component';
import { SearchItemGroupComponent } from './group/search-item-group/search-item-group.component';
import { GratuityComponent } from './gratuity/graduity.component';
import { GratuityConfigComponent } from './gratuity-config/gratuity-config.component';
import { ConfigDropdownComponent } from './gratuity-config/config-dropdown/config-dropdown.component';

// import { EditCompanyComponent } from './company/edit-company/edit-company.component';
import { ProductionRateNewComponent } from './production-rate-new/production-rate-new.component';
import { AddProductionRateNewComponent } from './production-rate-new/add-production-rate-new/add-production-rate-new.component';
import { EditProductionRateNewComponent } from './production-rate-new/edit-production-rate-new/edit-production-rate-new.component';
import { EditHrConfigurationComponent } from './configuration/hr-configuration/edit-hr-configuration/edit-hr-configuration.component';
import { HrConfigurationComponent } from './configuration/hr-configuration/hr-configuration.component';
import { SearchEditEmployeeComponent } from './employee-list/search-edit-employee/search-edit-employee.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(HrRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule.forRoot(),
    AgGridModule.withComponents([]),
    MatMomentDatetimeModule,
    MatDatetimepickerModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    DesignationComponent,
    AddDesignationComponent,
    EditDesignationComponent,
    DepartmentComponent,
    AddDepartmentComponent,
    EditDepartmentComponent,
    LeaveRequestComponent,
    AddLeaveRequestComponent,
    EditLeaveRequestComponent,
    HolidayComponent,
    AddHolidayComponent,
    EditHolidayComponent,
    DeductionComponent,
    AddDeductionComponent,
    EditDeductionComponent,
    TransferComponent,
    AddTransferComponent,
    EditTransferComponent,
    LeaveQuotaComponent,
    AddLeaveQuotaComponent,
    EditLeaveQuotaComponent,
    PromotionComponent,
    AddPromotionComponent,
    EditPromotionComponent,
    TaxSlabComponent,
    EditTaxSlabComponent,
    AddTaxSlabComponent,
    AllowanceComponent,
    AddAllowanceComponent,
    EditAllowanceComponent,
    BonusComponent,
    AddBonusComponent,
    EditBonusComponent,
    FineComponent,
    AddFineComponent,
    EditFineComponent,
    WarningComponent,
    AddWarningComponent,
    EditWarningComponent,
    TerminationComponent,
    AddTerminationComponent,
    EditTerminationComponent,
    EmployeeExitComponent,
    AddEmployeeExitComponent,
    EditEmployeeExitComponent,
    EmployeeWizardComponent,
    EmployeeListComponent,

    //Job Type
    JobTypeComponent,
    AddJobTypeComponent,
    EditJobTypeComponent,



    //Deduction type
    DeductionTypeComponent,
    AddDeductionTypeComponent,
    EditDeductionTypeComponent,



    //Leave Type
    LeaveTypeComponent,
    AddLeaveTypeComponent,
    EditLeaveTypeComponent,

    //Additional Leave
    AdditionalLeaveComponent,
    AddAdditionalLeaveComponent,
    EditAdditionalLeaveComponent,

    //Allowance Type
    AllowanceTypeComponent,
    AddAllowanceTypeComponent,
    EditAllowanceTypeComponent,

    //Bonus Type
    BonusTypeComponent,
    AddBonusTypeComponent,
    EditBonusTypeComponent,

    //Attendance
    AttendanceComponent,
    AddAttendanceComponent,
    EditAttendanceComponent,


    //Attendance Type
    AttendanceTypeComponent,
    AddAttendanceTypeComponent,
    EditAttendanceTypeComponent,

    //Attendance Setup
    AttendanceSetupComponent,

    //Item
    ItemComponent,
    AddItemComponent,
    EditItemComponent,

    //Item Type
    ItemTypeComponent,
    AddItemTypeComponent,
    EditItemTypeComponent,

    //Category
    CategoryComponent,
    EditCategoryComponent,
    AddCategoryComponent,

    //Sub Category
    SubCategoryComponent,
    AddSubCategoryComponent,
    EditSubCategoryComponent,
    FineTypeComponent,
    AddFineTypeComponent,
    EditFineTypeComponent,


    SalaryTypeComponent,
    AddSalaryTypeComponent,
    EditSalaryTypeComponent,
    LoanRequestComponent,
    AddLoanRequestComponent,
    EditLoanRequestComponent,
     //Job Type
     JobTypeComponent,
     AddJobTypeComponent,
     EditJobTypeComponent,
 
     
 
     //Deduction type
     DeductionTypeComponent,
     AddDeductionTypeComponent,
     EditDeductionTypeComponent,
 


     //Leave Type
     LeaveTypeComponent,
     AddLeaveTypeComponent,
     EditLeaveTypeComponent,
 
     //Additional Leave
     AdditionalLeaveComponent,
     AddAdditionalLeaveComponent,
     EditAdditionalLeaveComponent,
 
     //Allowance Type
     AllowanceTypeComponent,
     AddAllowanceTypeComponent,
     EditAllowanceTypeComponent,
 
     //Bonus Type
     BonusTypeComponent,
     AddBonusTypeComponent,
     EditBonusTypeComponent,
 
     //Attendance
     AttendanceComponent,
     AddAttendanceComponent,
     EditAttendanceComponent,
    
     //Item
     ItemComponent,
     AddItemComponent,
     EditItemComponent,
 
     //Item Type
     ItemTypeComponent,
     AddItemTypeComponent,
     EditItemTypeComponent,
 
     //Category
     CategoryComponent,
     EditCategoryComponent,
     AddCategoryComponent,
 
     //Sub Category
     SubCategoryComponent,
     AddSubCategoryComponent,
     EditSubCategoryComponent,
     FineTypeComponent,
     AddFineTypeComponent,
     EditFineTypeComponent,


     SalaryTypeComponent,
     AddSalaryTypeComponent,
     EditSalaryTypeComponent,
     LoanRequestComponent,
     AddLoanRequestComponent,
     EditLoanRequestComponent,
    //Resignation 

    ResignationComponent,


    AddResignationComponent,


    EditResignationComponent,


    AchievementComponent,


    AddAchievementComponent,


    EditAchievementComponent,


    //Coming Soon Component
    ComingSoonComponent,
    ComingSoon2Component,
    ComingSoon3Component,
    ComingSoon4Component,
    ComingSoon5Component,
    ComingSoon6Component,
    ComingSoon7Component,
    ComingSoon8Component,
    ComingSoon9Component,
    ComingSoon10Component,
    ComingSoon11Component,
    ComingSoon12Component,
    ComingSoon13Component,
    ComingSoon14Component,
    ComingSoon15Component,
    ComingSoon16Component,
    ComingSoon17Component,
    ComingSoon18Component,
    ComingSoon19Component,
    ComingSoon20Component,
    ComingSoon21Component,
    AdvanceRequestComponent,
    AddAdvanceRequestComponent,
    EditAdvanceRequestComponent,


    //Salary
    SalaryComponent,
    EditSalaryItemComponent,

    //Salary Group
    SalaryGroupComponent,
    EditSalaryGroupComponent,

    //Work Group
    GroupComponent,
    EditGroupComponent,



        //Coming Soon Component
   ComingSoonComponent,
   ComingSoon2Component,
   ComingSoon3Component,
   ComingSoon4Component,
   ComingSoon5Component,
   ComingSoon6Component,
   ComingSoon7Component,
   ComingSoon8Component,
   ComingSoon9Component,
   ComingSoon10Component,
   ComingSoon11Component,
   ComingSoon12Component,
   ComingSoon13Component,
   ComingSoon14Component,
   ComingSoon15Component,
   ComingSoon16Component,
   ComingSoon17Component,
   ComingSoon18Component,
   ComingSoon19Component,
   ComingSoon20Component,
   ComingSoon21Component,
   AdvanceRequestComponent,
   AddAdvanceRequestComponent,
   EditAdvanceRequestComponent,
   
   
   
  
     //Salary
     SalaryComponent,
     EditSalaryItemComponent,

     //Salary Group
     SalaryGroupComponent,
     EditSalaryGroupComponent,

     //Work Group
     GroupComponent,
     EditGroupComponent,


     //Production Rate

     ProductionRateNewComponent,
     //EditRepairRateComponent,
     AddProductionRateNewComponent,
     EditProductionRateNewComponent,

     //ProductionRateComponent,
     //EditRepairRateComponent,

    //salary Wizard
    // SalaryWizardComponent,

    OvertimeConfigurationComponent,
    EditOvertimeConfigurationComponent,

    //Coming Soon Component
    ComingSoonComponent,
    ComingSoon2Component,
    ComingSoon3Component,
    ComingSoon4Component,
    ComingSoon5Component,
    ComingSoon6Component,
    ComingSoon7Component,
    ComingSoon8Component,
    ComingSoon9Component,
    ComingSoon10Component,
    ComingSoon11Component,
    ComingSoon12Component,
    ComingSoon13Component,
    ComingSoon14Component,
    ComingSoon15Component,
    ComingSoon16Component,
    ComingSoon17Component,
    ComingSoon18Component,
    ComingSoon19Component,
    ComingSoon20Component,
    ComingSoon21Component,
    OvertimeConfigurationComponent,
    EditOvertimeConfigurationComponent,
    CostCenterComponent,
    AddCostCenterComponent,
    EditCostCenterComponent,


    OvertimeComponent,


    AddOvertimeComponent,
    EditOvertimeComponent,
    ChildEditOvertimeComponent,
    SalaryWizardComponent,


    ProductionRateNewComponent,
    AddProductionRateNewComponent,
    EditProductionRateNewComponent,


    SalaryListComponent,


     OvertimeComponent,


     AddOvertimeComponent,
     EditOvertimeComponent,
     ChildEditOvertimeComponent,
     SalaryWizardComponent,
     ItemWiseProductionComponent,
     EditItemWiseProductionComponent,

    //  //Company
    //  CompanyComponent,
    //  AddCompanyComponent,
    //  EditCompanyComponent,


     //Company
     ProductionRateNewComponent,
     AddProductionRateNewComponent,
     EditProductionRateNewComponent,

     //Overtime New
     OvertimeNewComponent,
     /////
     //////////////////////////
     SearchItemTypeComponent,
     SearchCategoryComponent,
     SearchSubCategoryComponent,
     SearchItemComponent,
     SearchEmployeeComponent,
     SearchItemGroupComponent,

     //repair rate
     EditRepairRateComponent,
     //Gratuity

     GratuityComponent,
     
     //Gratuity Config
     GratuityConfigComponent,
     
     ConfigDropdownComponent,

     //overtimr-new
     OvertimeNewComponent,

     //ag-grid
     ProductionRateNewComponent,
     AddProductionRateNewComponent,
     EditProductionRateNewComponent,
     EditHrConfigurationComponent,
     HrConfigurationComponent,
     SearchEditEmployeeComponent 
    
  ]
})

export class Hr { }
