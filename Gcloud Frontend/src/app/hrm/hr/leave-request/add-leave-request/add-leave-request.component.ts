import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';

// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { LeaveRequestComponent } from '../leave-request.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { LeaveRequestDto, LeaveRequestServiceProxy, EmployeeServiceProxy, LeaveTypeServiceProxy, EmployeeDto, LeaveTypeDto, HRConfigurationServiceProxy, HRConfigurationDto } from 'app/shared/service-proxies/service-proxies';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'add-leave-request',
  templateUrl: './add-leave-request.component.html',
  styleUrls: ['./add-leave-request.component.less'],
  providers:[HRConfigurationServiceProxy , LeaveRequestServiceProxy , EmployeeServiceProxy ]
})
export class AddLeaveRequestComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addLeaveRequestModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    leaveRequest: LeaveRequestDto = new LeaveRequestDto();
    employeeDDList: EmployeeDto[] = [];
    leaveTypeDDList: LeaveTypeDto[] = [];

    disabled : boolean = abp.auth.isGranted("Pages.Hrm.LeaveRequest.Approval_LeaveRequest");

    _leaveRequest : LeaveRequestDto[];
    _docNo : abc[] = [];
    _leaveRequestValidation : FormGroup
    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName: string;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();
    isAutoCode : boolean;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
        private _leaveRequestService: LeaveRequestServiceProxy,
        private _employeeService: EmployeeServiceProxy,
        private _leaveTypeService: LeaveTypeServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder
       
    ) {
        //super(injector);
    }

    //this function will save the record in datatabse.
    save(): void {
        //TODO: Refactor this, don't use jQuery style code
      this.saving = true;
        this._leaveRequestService.create(this.leaveRequest)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                // this.notify.info(this.l('Saved Successfully'));
                this.close();   
                this.modalSave.emit(null);
                
            });
      }

      initValidation() {

        this._leaveRequestValidation = this.formBuilder.group({
            employeeId: [null, Validators.required],
            leaveTypeId: [null, Validators.required],
            leaveToDate: [null, Validators.required],
            leaveFromDate: [null, Validators.required]
            
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    /*onType() {
    
      if (this._leaveRequestValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._leaveRequestValidation);
      }
    }*/
    onType() {
        debugger;
        if (this._leaveRequestValidation.valid) {
            debugger;
            let a, p = 0;
            for (a = 0; a < this.employeeDDList.length; a++) {
                if (this.leaveRequest.employeeId == this.employeeDDList[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._leaveRequestValidation);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._leaveRequestValidation);

        }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
    /////////////////////End of Validation code///////////////////////
    ///////////////////////////////search Method//////////////////////////////////
    public employeeDropdownSearch() {
        debugger;
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith<string | EmployeeDto>(''),
                map(value => typeof value === 'string' ? value : value.employee_Name),
                map(name => name ? this._filter(name) : this.employeeDDList.slice())
            );

    }

    private _filter(value: string): EmployeeDto[] {

        debugger;
        const filterValue = value.toLowerCase();

        return this.employeeDDList.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
    }


    displayEmployee = (emp: EmployeeDto): any => {
        debugger;

        if (emp instanceof EmployeeDto) {
            this.leaveRequest.employeeId = emp.id;
            return emp.employee_Name;
        }
        this.leaveRequest.employeeId = null;
        return emp;

    }

    ////////////////////////////////End of Search Method/////////////////////////////////////

    //this function will show the model when we click on Add new Button.
    show(): void{

        this.active = true;
        this.modal.show();
        this.leaveRequest = new LeaveRequestDto();
        this.leaveRequest.init({isActive:true});

        this._leaveRequestService.getAllLeaveRequests().subscribe((result)=>{

            this._leaveRequest = result ;
            //this.getAutoDocNumber();
            if(this.hrConfig.autoCode == true){

                this.getAutoDocNumber();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }

        })
        this.empName = null;
        this.employeeDropdownSearch();
        this._leaveRequestValidation.markAsUntouched({onlySelf:true});  
    }


    /*getAutoDocNumber():void{

        let i =0;
       
        for(i=0 ;i< this._leaveRequest.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._leaveRequest[i].docNo ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0 ){
            debugger;
            code = "1";
            this.leaveRequest.docNo = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    code = this._docNo[this._docNo.length-1].code ;
                    code ++;
                    if(code <=9){
    
                        this.leaveRequest.docNo = "00" + code ;
                    }
                    else if(code <=99){
    
                        this.leaveRequest.docNo = "0" + code ;
                    }
                    else if(code <=999){
    
                        this.leaveRequest.docNo =  code ;
                    }
            }                     
        }
        this._docNo = [];
    }*/

    getAutoDocNumber():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._leaveRequest.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._leaveRequest[i].docNo ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0  ){
          
            debugger;
            code = "1";
            this.leaveRequest.docNo = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    let x;
                    code = this._docNo[this._docNo.length-1].code ;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.leaveRequest.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.leaveRequest.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.leaveRequest.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.leaveRequest.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.leaveRequest.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.leaveRequest.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.leaveRequest.docNo = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.leaveRequest.docNo =  "0" + code;
                      }
                      else if(code){
      
                          this.leaveRequest.docNo =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.leaveRequest.docNo = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.leaveRequest.docNo =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.leaveRequest.docNo =  temp[0] ;
                      }
    
                    }
                    
            }                     
        }
        this._docNo = [];
    }


    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];
        });

    }



    ngOnInit(): void {
        //this function will gell all the employee record and then will set all the employee names to employeeDDList object.
        this._employeeService.getAllEmployees()
            .subscribe((result)=> {
                this.employeeDDList = result;
            });
        //this function will gell all the leave types record and then will set all the leave types to leaveTypeDDList object.
        this._leaveTypeService.getAllLeaveTypes()
            .subscribe((result)=> {
                this.leaveTypeDDList = result;
            }) 
       this.initValidation() 
       this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

// class for keeping all docNo number
class abc{
    public code: string;
  }


