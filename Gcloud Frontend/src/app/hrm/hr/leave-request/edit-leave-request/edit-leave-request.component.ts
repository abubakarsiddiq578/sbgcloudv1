import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { LeaveRequestComponent } from '../leave-request.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { LeaveRequestDto, LeaveRequestServiceProxy, EmployeeServiceProxy, LeaveTypeServiceProxy, EmployeeDto, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import { subscribeTo } from 'rxjs/internal-compatibility';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-leave-request',
  templateUrl: './edit-leave-request.component.html',
  providers:[EmployeeServiceProxy , LeaveRequestServiceProxy , HRConfigurationServiceProxy]
})
export class EditLeaveRequestComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editLeaveRequestModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    leaveRequest: LeaveRequestDto = new LeaveRequestDto();
    employeeDDList=[];
    leaveTypeDDList=[];

    _leaveRequestValidation : FormGroup
    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName: string;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
        private _leaveRequestService: LeaveRequestServiceProxy,
        private _employeeService: EmployeeServiceProxy,
        private _leaveTypeService: LeaveTypeServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder
       
    ) {
        //super(injector);
    }
  

    show(id:number): void {
        this._leaveRequestService.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: LeaveRequestDto)=> {
                this.leaveRequest = result;
                this._employeeService.get(result.employeeId).subscribe((result)=>{
                    this.empName = result.employee_Name;
                })
            })
        this.employeeDropdownSearch();
        this._leaveRequestValidation.markAsUntouched({onlySelf:true});
    }
 ////////////////////start of validation code/////////////////////////////////////
    initValidation() {

        this._leaveRequestValidation = this.formBuilder.group({
            employeeId: [null, Validators.required],
            leaveTypeId: [null, Validators.required],
            leaveToDate: [null, Validators.required],
            leaveFromDate: [null, Validators.required]
            
        });
    
    }
    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];


            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }


        });

    }



    //check form validation if it is valid then save it else throw error message in form //
   /* onType() {
    
      if (this._leaveRequestValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._leaveRequestValidation);
      }
    }*/

    onType() {
        
        if (this._leaveRequestValidation.valid) {
            let a, p = 0;
            for (a = 0; a < this.employeeDDList.length; a++) {
                if (this.leaveRequest.employeeId == this.employeeDDList[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._leaveRequestValidation);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._leaveRequestValidation);

        }
    }
     

    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
//////////////////////End of validation code///////////////////////////////////////
    ///////////////////////////////search Method//////////////////////////////////
    public employeeDropdownSearch() {
        debugger;
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith<string | EmployeeDto>(''),
                map(value => typeof value === 'string' ? value : value.employee_Name),
                map(name => name ? this._filter(name) : this.employeeDDList.slice())
            );

    }

    private _filter(value: string): EmployeeDto[] {

        debugger;
        const filterValue = value.toLowerCase();

        return this.employeeDDList.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
    }


    displayEmployee = (emp: EmployeeDto): any => {
        debugger;

        if (emp instanceof EmployeeDto) {
            this.leaveRequest.employeeId = emp.id;
            return emp.employee_Name;
        }
        this.leaveRequest.employeeId = null;
        return emp;

    }

    ////////////////////////////////End of Search Method/////////////////////////////////////



    ngOnInit(): void {
        this._employeeService.getAllEmployees()
            .subscribe((result)=> {
                this.employeeDDList = result;
            });

            this._leaveTypeService.getAllLeaveTypes()
               .subscribe( (result) => {
                this.leaveTypeDDList = result;
                });

                this.initValidation();
                this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }
    save(): void {
        this.saving = true;
        this._leaveRequestService.update(this.leaveRequest)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            // this.notify.info(this.l('Record Updated Successfully'));
            this.close();
            this.modalSave.emit(null);
          });
      }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

