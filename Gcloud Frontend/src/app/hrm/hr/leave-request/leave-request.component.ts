// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddLeaveRequestComponent } from './add-leave-request/add-leave-request.component';
import { EditLeaveRequestComponent } from './edit-leave-request/edit-leave-request.component';
import { LeaveRequestServiceProxy, LeaveRequestDto, EmployeeServiceProxy, LeaveTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'leaverequest',
    templateUrl: 'leave-request.component.html',
    providers: [LeaveRequestServiceProxy, EmployeeServiceProxy, LeaveTypeServiceProxy]
})

export class LeaveRequestComponent implements OnInit, AfterViewInit {
    public dataTable: DataTable;

    result: LeaveRequestDto[]=[];
    data: string[] = [];

    constructor(injector: Injector,
      private _leaveRequestService: LeaveRequestServiceProxy
      ,private _router : Router
    ){}

     @ViewChild('addLeaveRequestModal') addLeaveRequestModal: AddLeaveRequestComponent;
     @ViewChild('editLeaveRequestModal') editLeaveRequestModal: EditLeaveRequestComponent;


     globalFunction: GlobalFunctions = new GlobalFunctions
     //this ngOnInit function will be called as we run this page.
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "LeaveRequest")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
      //this function will get all the records. After getting all record, it will also call paging funciton and 
      //getAllData function.
      this.getAllRecord();
    }
    getAllRecord(): void {
      this._leaveRequestService.getAllLeaveRequests()
      .finally(()=>{
          console.log('completed');
      })
      .subscribe((result:any)=>{
  this.result = result;
  this.paging();
  this.getAllData(); 
      });
    }
    //this paging function will set datatable's header and footer rows and datarows also that will show in front .html page table.
    paging(): void{
      this.dataTable = {
        headerRow: [ 'Doc No', 'Doc Date', 'App No', 'App Date', 'Employee', 'Leave Type', 'Duration','Alt Contact','From Date', 'To Date', 'Reason', 'Actions' ],
        footerRow: [ /*'Leave Type', 'Year', 'Entitled', 'Accrued', 'Additional', 'Carried Over', 'Taken','Pending','Balance', 'Actions'*/ ],

        dataRows: [
            // ['Additiona Leaves', '2018', '12', '6', '0','0','0','0','6', ''],
            // ['Casual Leaves', '2018', '10', '0', '0','0','0','0','10', ''],
            // ['Sick Leaves', '2018', '10', '6', '0','0','0','0','10', ''],
            // ['Special Leaves', '2018', '3', '0', '0','0','0','0','3', '']
        ]
     };
    }
    //this function is called in ngInit. It will push all the record into datarows.
    getAllData():void{
      let i;
      for(i=0; i<this.result.length ; i++)
      {
        this.data.push(this.result[i].docNo);
        this.data.push(this.result[i].docDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].applicationNo);
        this.data.push(this.result[i].applicationDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].employee.employee_Name);
        this.data.push(this.result[i].leaveType.leaveTypeName);
        this.data.push(this.result[i].duration);
        this.data.push(this.result[i].alternateContactNo);
        this.data.push(this.result[i].fromDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].toDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].reason);
        
        this.data.push(this.result[i].id.toString());
        //this below line will push all the record.
        this.dataTable.dataRows.push(this.data);
        this.data = [];
      }
  }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    //this function will show popup and then delete the record from datatabse.
    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "LeaveRequest")) {
        this.globalFunction.showNoRightsMessage("Delete");
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Leave Request!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
      if (result.value) {
        debugger;
        this._leaveRequestService.delete(parseInt(id))
          .finally(() => {
          this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your leave request record has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
          }).catch(swal.noop)
           });
      } else {
        swal({
            title: 'Cancelled',
            text: 'Your leave request record is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        }).catch(swal.noop)
      }
    })
    }
    AddLeaveRequest(): void {
      if (!this.globalFunction.hasPermission("Create", "LeaveRequest")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addLeaveRequestModal.show();
      }

      EditLeaveRequest(id: string): void {
        if (!this.globalFunction.hasPermission("Edit", "LeaveRequest")) {
        this.globalFunction.showNoRightsMessage("Edit");
        return
      }
        this.editLeaveRequestModal.show(parseInt(id));
        }

}


