import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeNewComponent } from './overtime-new.component';

describe('OvertimeNewComponent', () => {
  let component: OvertimeNewComponent;
  let fixture: ComponentFixture<OvertimeNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
