import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { OvertimeServiceProxy, DepartmentServiceProxy, EmployeeDesignationServiceProxy, OvertimeDto, EmployeeDesignationDto, DepartmentDto, EmployeeServiceProxy, EmployeeDto, SalaryMasterServiceProxy, OvertimeConfigurationServiceProxy, OvertimeConfigurationDto } from '@app/shared/service-proxies/service-proxies';
import { ColDef, GridApi, ColumnApi } from '../../../../../node_modules/ag-grid-community';
import * as moment from 'moment';
import { parseDate } from '../../../../../node_modules/ngx-bootstrap/chronos';

@Component({
  selector: 'app-overtime-new',
  templateUrl: './overtime-new.component.html',
  styleUrls: ['./overtime-new.component.scss'],
  providers: [OvertimeServiceProxy, DepartmentServiceProxy, EmployeeDesignationServiceProxy, EmployeeServiceProxy, SalaryMasterServiceProxy, OvertimeServiceProxy, OvertimeConfigurationServiceProxy]
})
export class OvertimeNewComponent implements OnInit {


  // row data and column definitions

  public employee: EmployeeDto[]
  public rowData: any[];
  public columnDefs: ColDef[];
  public defaultColDef;

  // gridApi and columnApi
  public api: GridApi;
  public columnApi: ColumnApi;

  //row selectoin 
  public rowSelection;


  department: DepartmentDto[]
  designation: EmployeeDesignationDto[]
  overtimeConfiguration: OvertimeConfigurationDto[]

  overtime: OvertimeDto = new OvertimeDto

  columns: any = []

  isDays31: boolean = false
  isDays30: boolean = false
  isDays29: boolean = false
  isDays28: boolean = false

  departmentId: number
  designationId: number

  regDaysHrs: any = []

  firstDayDate: any

  month: string
  monthDays: any
  Data: any[] = [];


  constructor(

    private departmentService: DepartmentServiceProxy,
    private designationService: EmployeeDesignationServiceProxy,
    private employeeService: EmployeeServiceProxy,
    private overtimeService: OvertimeServiceProxy,
    private overtimeConfigurationService: OvertimeConfigurationServiceProxy

  ) {
    this.initAgGrid()
  }

  initAgGrid() {
    this.columnDefs = this.createColumnDefs();
    this.defaultColDef = { editable: true };
    
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  // create some simple column definitions
  private createColumnDefs() {
    return this.columns

  }
  
  onMonthChange(value) {
    this.month = (value).toString()
    let dynamicColoumns: any = []
    let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

    if (value == "1" || value == "3" || value == "5"
      || value == "7" || value == "8" || value == "10"
      || value == "12") {
      for (let i = 0; i < 31; i++) {
        dynamicColoumns.push({ headerName: "Day " + (i + 1) + " (" + days[new Date(new Date().getFullYear(), (parseInt(value) - 1), i + 1).getDay()] + ")", field: "day" + (i + 1), width: 120 })
      }
      this.isDays31 = true
      this.monthDays = 31
    }
    else if (value == "4" || value == "6" || value == "9"
      || value == "11") {
      for (let i = 0; i < 30; i++) {
        dynamicColoumns.push({ headerName: "Day " + (i + 1) + " (" + days[new Date(new Date().getFullYear(), (parseInt(value) - 1), i + 1).getDay()] + ")", field: "day" + (i + 1), width: 120 })

      }
      this.isDays30 = true
      this.monthDays = 30
    }
    else {
      let leapyear: boolean
      let year = new Date().getFullYear()
      if (year % 4 == 2) {
        leapyear = true
      }
      if (leapyear) {
        for (let i = 0; i < 28; i++) {
          dynamicColoumns.push({ headerName: "Day " + (i + 1) + " (" + days[new Date(new Date().getFullYear(), (parseInt(value) - 1), i + 1).getDay()] + ")", field: "day" + (i + 1), width: 120 })
        }
        this.isDays29 = true
        this.monthDays = 29
      } else {
        for (let i = 0; i < 29; i++) {
          dynamicColoumns.push({ headerName: "Day " + (i + 1) + " (" + days[new Date(new Date().getFullYear(), (parseInt(value) - 1), i + 1).getDay()] + ")", field: "day" + (i + 1), width: 120 })
        }
        this.isDays28 = true
        this.monthDays = 28
      }


    }

    let offDays = this.getOffDays(new Date().getFullYear(), value - 1)

    let sumOffDays: string = "eval(data.day" + offDays[0] + ")"
    for (let i = 0; i < (offDays.length - 1); i++) {
      sumOffDays += " + eval(data.day" + offDays[i + 1] + ")";

    }

    let sumRegDays: string = "eval(data.day1)"
    if (this.isDays31) {
      for (let i = 0; i < 30; i++) {
        sumRegDays += "+ eval(data.day" + (i + 2) + ")"
      }
    } else if (this.isDays30) {
      for (let i = 0; i < 29; i++) {
        sumRegDays += "+ eval(data.day" + (i + 2) + ")"
      }
    } else if (this.isDays29) {
      for (let i = 0; i < 28; i++) {
        sumRegDays += "+ eval(data.day" + (i + 2) + ")"
      }
    } else {
      for (let i = 0; i < 27; i++) {
        sumRegDays += "+ eval(data.day" + (i + 2) + ")"
      }
    }

    let staticColoumns = [
      { field: 'Employee Code', valueGetter: (params) => params.data.employee.employee_Code, width: 150 },
      { field: 'Employee Name ', valueGetter: (params) => params.data.employee.employee_Name, width: 150 },
      { field: 'Salary', valueGetter: (params) => params.data.annualGrossSalary, width: 150 },
      { field: 'regDaysHrs', colId: "regDaysHrs", width: 150, valueGetter: sumRegDays + "-" + "(" + sumOffDays + ")" },
      { field: 'offDaysHrs', colId: "offDaysHrs", valueGetter: sumOffDays, width: 150 },
      { field: 'regDaysOtRate', colId: "regDaysOtRate", width: 150 },
      { field: 'offDaysOtRate', colId: "offDaysOtRate", width: 150 },
      {
        field: 'regDaysOtAmount', colId: "regDaysOtAmount", width: 150,
        valueGetter: function chainValueGetter(params) {
          return params.getValue("regDaysHrs") * params.getValue("regDaysOtRate");
        }
      },
      {
        field: 'offDaysOtAmount', colId: "offDaysOtAmount", width: 150,
        valueGetter: function chainValueGetter(params) {
          return params.getValue("offDaysHrs") * params.getValue("offDaysOtRate");
        }
      },
      {
        field: 'totalAmount', colId: "totalAmount", valueGetter: function chainValueGetter(params) {

          return params.getValue("regDaysOtAmount") + params.getValue("offDaysOtAmount");
        }, width: 150
      },
      { field: 'otStartAt', width: 150 },
    ]



    this.columns = staticColoumns.concat(dynamicColoumns)
    this.initAgGrid()
    this.setValues(this.rowData)

  }

  onDepartmentChange(value) {
    this.departmentId = value
  }

  onDesignationChange(value) {
    this.designationId = value
  }

  getOffDays(year: any, month: any) {
    debugger
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    let tempWorkingDays: string[] = this.overtimeConfiguration[0].workingDays.split(',')
    let d = new Date();
    let getTot = daysInMonth(month, year);
    let offDays  = []
    let workingDays = []
    let totalDaysArr = []
    for (let i = 1; i <= getTot; i++) {
      let newDate = new Date(year, month, i)
      for (let j = 0; j < tempWorkingDays.length; j++) {
        if (days[newDate.getDay()] == tempWorkingDays[j]) {
          workingDays.push(i)
        }
      }      
    }
    for (let i = 1; i < getTot +1; i++) 
    {
      totalDaysArr.push(i)
    }
    
  //   workingDays.forEach((e1) => totalDaysArr.forEach((e2) => 
  //   {
  //     if (e1 == e2 ) {
  //       offDays.push(e2)
  //     }
  //   }
  // ));

  offDays = totalDaysArr.filter(item => workingDays.indexOf(item) < 0);

    return offDays
    function daysInMonth(month, year) {
      return new Date(year, month, 0).getDate();
    }
  }


  // one grid initialisation, grap the APIs and auto resize the columns to fit the available space
  onGridReady(params): void {
    this.api = params.api;
    this.columnApi = params.columnApi;

  }

  ngOnInit() {
    // get all record
    this.getAll()

  }

  getAll() {
     
    

      this.employeeService.getEmployeesWithSalary(0, 0)
      .subscribe((result) => {
        this.Data = result
       
      })

    //get all departments
    this.departmentService.getAllDepartments()
      .subscribe((result) => {
        this.department = result
      })
    //get all designation
    this.designationService.getAllEmployeeDesignations()
      .subscribe((result) => {
        this.designation = result
      })
    //get all employees
    this.employeeService.getAllEmployees()
      .subscribe((result) => {
        this.employee = result
      })

      this.overtimeConfigurationService.getAllOverTimeConfiguration()
      .subscribe((result) => {
        this.overtimeConfiguration = result
        this.employeeService.getEmployeesWithSalary(0, 0)
      .subscribe((result) => {
        this.mapResult(result)
        this.rowData = result
       
      })
      })
  }

  mapResult(params) {
    let i = 0;
    debugger
    params.map((obj: any) => {  
      obj.regDaysHrs = 0
      obj.offDaysHrs = 0
      obj.regDaysOtRate = this.overtimeConfiguration[0].regularDayHourRate
      obj.offDaysOtRate = this.overtimeConfiguration[0].offDayHourRate
      obj.regDaysOtAmount = 0
      obj.offDaysOtAmount = 0
      obj.totalAmount = 0
      obj.otStartAt = '6:30'
      obj.date = '12/12/12'
      obj.day1 = 0
      obj.day2 = 0
      obj.day3 = 0
      obj.day4 = 0
      obj.day5 = 0
      obj.day6 = 0
      obj.day7 = 0
      obj.day8 = 0
      obj.day9 = 0
      obj.day10 = 0
      obj.day11 = 0
      obj.day12 = 0
      obj.day13 = 0
      obj.day14 = 0
      obj.day15 = 0
      obj.day16 = 0
      obj.day17 = 0
      obj.day18 = 0
      obj.day19 = 0
      obj.day20 = 0
      obj.day21 = 0
      obj.day22 = 0
      obj.day23 = 0
      obj.day24 = 0
      obj.day25 = 0
      obj.day26 = 0
      obj.day27 = 0
      obj.day28 = 0
      obj.day29 = 0
      obj.day30 = 0
      obj.day31 = 0
      
    obj.id = i.toString()
      i++;
      // for (let i = 0; i < 31; i++) {
      //    
      //  d[i] = "Day" + (i + 1).toString()
      //   obj.window[d[i]]  = 0      
      // }
    })
  }
  onCellValueChanged(params) {
     
    let NewData = [];
    this.api.forEachNode(node => NewData.push(node.data));
    console.log(NewData)

    params.data.newValue
  }

  show() {
    this.employeeService.getEmployeesWithSalary(this.departmentId, this.designationId)
      .subscribe((result) => {
        this.mapResult(result)
        this.rowData = result
      })
  }

  save() {

     
    //   let newRegDaysHrs
    //  this.api.forEachNode((node) => {
    //   newRegDaysHrs = this.api.getValue("regDaysHrs", node)
    //  })
    //   console.log(newRegDaysHrs);

    let rowData = [];
    this.api.forEachNode((node) => {
      rowData.push(node.data)
    });

    for (let i = 0; i < rowData.length; i++) {
      for (let j = 0; j < this.monthDays; j++) {
       
        let tempDayHrs = this.api.getValue("day" + (j + 1), this.api.getRowNode(i.toString()))
        if (tempDayHrs > 0) {
          this.rowData[j].date = moment(new Date().getFullYear().toString() + "-" + this.month + "-" + (j + 1))
          this.overtime.employeeId = rowData[i].employeeId
          this.overtime.departmentId = rowData[i].employee.dept_ID
          this.overtime.overTimeDate = moment(new Date().getFullYear().toString() + "-" + this.month + "-" + (j + 2)).add(5,'hour')
          this.overtime.overTimerHour = tempDayHrs
          this.overtime.costCenterId = 1
          for (let k = 0; k < this.Data[j].employee.overtimes.length; k++) {
            if (moment(rowData[j].date).format("YYYY-MMM-DD") === moment(this.Data[j].employee.overtimes[k].overTimeDate).format("YYYY-MMM-DD")){
              this.overtime.id = this.Data[j].employee.overtimes[k].id
            }
          }
          
          this.overtimeService.create(this.overtime)
            .subscribe(() => {
              console.log("success")
            })
        }
      }
    }

  }

  setValues(result) {
    let dayHrs : any[] = []
    let tempObj;

    for (let i = 0; i < result.length; i++) {
      result[i].employee.overtimes.forEach(overtime => {
        tempObj = {
          otDate: new Date(overtime.overTimeDate).getDate(),
          otHrs: overtime.overTimerHour,
          otMonth:  (new Date(overtime.overTimeDate).getMonth() + 1),
          empId: overtime.employeeId
        }
        dayHrs.push(tempObj)
      });
    }

    this.api.forEachNode((node) => {
      console.log(node)
      for (let i = 0; i < dayHrs.length; i++) {
         
        if (parseInt(this.month) == dayHrs[i].otMonth && node.data.employeeId == dayHrs[i].empId){
          node.setDataValue('day' + dayHrs[i].otDate, dayHrs[i].otHrs.toString())
        }
        
      }
    })
    console.log(this.api.getDisplayedRowCount())
  }

}
