import { Routes } from '@angular/router';

import { DesignationComponent } from './Designation/designation.component';
import { DepartmentComponent } from './department/department.component';
import { LeaveRequestComponent } from './leave-request/leave-request.component';
import { HolidayComponent } from './holiday/holiday.component';
import { DeductionComponent } from './deduction/deduction.component';
import { TransferComponent } from './transfer/transfer.component';
import { LeaveQuotaComponent } from './leave-quota/leave-quota.component';
import { PromotionComponent } from './promotion/promotion.component';
import { TaxSlabComponent } from './tax-slab/tax-slab.component';
import { AllowanceComponent } from './allowance/allowance.component';
import { BonusComponent } from './bonus/bonus.component';
import { FineComponent } from './fine/fine.component';
import { WarningComponent } from './warning/warning.component';
import { TerminationComponent } from './termination/termination.component';
import { EmployeeExitComponent } from './employee-exit/employee-exit.component';
import { EmployeeWizardComponent } from './employee-wizard/employee-wizard.component';

import { JobTypeComponent } from './job-type/job-type.component';
import { DeductionTypeComponent } from './deduction-type/deduction-type.component';
import { LeaveTypeComponent } from './leave-type/leave-type.component';
import { AdditionalLeaveComponent } from './additional-leave/additional-leave.component';
import { AllowanceTypeComponent } from './allowance-type/allowance-type.component';
import { BonusTypeComponent } from './bonus-type/bonus-type.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { AttendanceTypeComponent } from './attendanceType/attendanceType.component';
import { AttendanceSetupComponent } from './attendance-setup/attendance-setup.component';
import { ItemComponent } from './item/item.component';
import { ItemTypeComponent } from './item-type/item-type.component';
import { CategoryComponent } from './category/category.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { FineTypeComponent } from './fine-type/fine-type.component';
import { SalaryTypeComponent } from './Salary-Type/salary-type.component';
import { LoanRequestComponent } from './Loan-Request/loan-request.component';
import { ResignationComponent } from './resignation/resignation.component';
import { AchievementComponent } from './achievement/achievement.component';

import { AdvanceRequestComponent } from './advance-request/advance-request.component';
import { SalaryGroupComponent } from "./salary-group/salary-group.component";



import { ComingSoonComponent } from '../coming-soon/coming-soon.component';
import { ComingSoon2Component } from '../coming-soon2/coming-soon2.component';
import { ComingSoon3Component } from '../coming-soon3/coming-soon3.component';
import { ComingSoon4Component } from '../coming-soon4/coming-soon4.component';
import { ComingSoon5Component } from '../coming-soon5/coming-soon5.component';
import { ComingSoon6Component } from '../coming-soon6/coming-soon6.component';
import { ComingSoon7Component } from '../coming-soon7/coming-soon7.component';
import { ComingSoon8Component } from '../coming-soon8/coming-soon8.component';
import { ComingSoon9Component } from '../coming-soon9/coming-soon9.component';
import { ComingSoon10Component } from '../coming-soon10/coming-soon10.component';
import { ComingSoon11Component } from '../coming-soon11/coming-soon11.component';
import { ComingSoon12Component } from '../coming-soon12/coming-soon12.component';
import { ComingSoon13Component } from '../coming-soon13/coming-soon13.component';
import { ComingSoon14Component } from '../coming-soon14/coming-soon14.component';
import { ComingSoon15Component } from '../coming-soon15/coming-soon15.component';
import { ComingSoon16Component } from '../coming-soon16/coming-soon16.component';
import { ComingSoon17Component } from '../coming-soon17/coming-soon17.component';
import { ComingSoon18Component } from '../coming-soon18/coming-soon18.component';
import { ComingSoon19Component } from '../coming-soon19/coming-soon19.component';
import { ComingSoon20Component } from '../coming-soon20/coming-soon20.component';
import { ComingSoon21Component } from '../coming-soon21/coming-soon21.component';
import { AppRouteGuard } from '@app/shared/auth/auth-route-guard';
import { EmployeeListComponent } from '@app/hrm/hr/employee-list/employee-list.component';
import { OvertimeConfigurationComponent } from '@app/hrm/hr/configuration/overtime-configuration/overtime-configuration.component';
import { CostCenterComponent } from '@app/hrm/hr/cost-center/cost-center.component';
import { GroupComponent } from '@app/hrm/hr/group/group.component';
import { ProductionRateNewComponent } from '@app/hrm/hr/production-rate-new/production-rate-new.component';

import { SalaryWizardComponent } from '@app/hrm/hr/salary-wizard/salary-wizard.component';
import { SalaryListComponent } from './salary-list/salaryList.component';


// import { CompanyComponent } from './company/company.component';
//import { ProductionRateComponent } from '@app/hrm/hr/production-rate/production-rate.component';
import { GratuityComponent } from './gratuity/graduity.component';
import { GratuityConfigComponent } from './gratuity-config/gratuity-config.component';

import { OvertimeNewComponent } from '@app/hrm/hr/overtime-new/overtime-new.component';

import { OvertimeComponent } from '@app/hrm/hr/overtime/overtime.component';
import { ItemWiseProductionComponent } from './item-wise-production/item-wise-production.component';
import { HrConfigurationComponent } from './configuration/hr-configuration/hr-configuration.component';
import { SalaryComponent } from './salary/salary.component';

export const HrRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'Designation',
        component: DesignationComponent,
        canActivate: [AppRouteGuard]
    }]},{
      path: '',
      children: [{
        path: 'employeeWizard',
        component: EmployeeWizardComponent,
        canActivate: [AppRouteGuard]
      }]},{
        path: '',
        children: [{
          path: 'employeeWizard/:id',
          component: EmployeeWizardComponent,
          canActivate: [AppRouteGuard]
        }]},
      {
        path: '',
        children: [ {
          path: 'employee-list',
          component: EmployeeListComponent,
          canActivate: [AppRouteGuard]
      }]},
    {
        path: '',
        children: [ {
          path: 'department',
          component: DepartmentComponent,
          data: { permission: 'Pages.Administration.Department' }, 
          canActivate: [AppRouteGuard]
      }]},
      {
          path: '',
          children: [ {
            path: 'leaverequest',
            component: LeaveRequestComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'holiday',
            component: HolidayComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'deduction',
            component: DeductionComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'transfer',
            component: TransferComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'leavequota',
            component: LeaveQuotaComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'promotion',
            component: PromotionComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'TaxSlab',
            component: TaxSlabComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'allowance',
            component: AllowanceComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'bonus',
            component: BonusComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'fine',
            component: FineComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'warning',
            component: WarningComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'termination',
            component: TerminationComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'employee-exit',
            component: EmployeeExitComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'employee-registration',
            component: EmployeeWizardComponent,
            canActivate: [AppRouteGuard]
        }]},
        {
          path: '',
          children: [ {
            path: 'JobType',
            component: JobTypeComponent,
            canActivate: [AppRouteGuard]
        }]},
          {
            path: '',
            children: [ {
              path: 'DeductionType',
              component: DeductionTypeComponent,
              canActivate: [AppRouteGuard]
          }]},
    
          {
            path: '',
            children: [ {
              path: 'LeaveType',
              component: LeaveTypeComponent,
              canActivate: [AppRouteGuard]
          }]},
    
          {
            path: '',
            children: [ {
              path: 'AdditionalLeave',
              component: AdditionalLeaveComponent,
              canActivate: [AppRouteGuard]
          }]},
    
          {
            path: '',
            children: [ {
              path: 'AllowanceType',
              component: AllowanceTypeComponent,
              canActivate: [AppRouteGuard]
          }]},
    
          {
            path: '',
            children: [ {
              path: 'BonusType',
              component: BonusTypeComponent,
              canActivate: [AppRouteGuard]
          }]},
    
          {
            path: '',
            children: [ {
              path: 'Attendance',
              component: AttendanceComponent,
              canActivate: [AppRouteGuard]
          }]},
          {
            path: '',
            children: [ {
              path: 'AttendanceType',
              component: AttendanceTypeComponent,
              canActivate: [AppRouteGuard]
          }]},
          {
            path: '',
            children: [ {
              path: 'AttendanceSetup',
              component: AttendanceSetupComponent,
              canActivate: [AppRouteGuard]
          }]},
    
          {
            path: '',
            children: [ {
              path: 'Item',
              component: ItemComponent,
              canActivate: [AppRouteGuard]
          }]},


          {
            path: '',
            children: [ {
              path: 'ProductionRate',
              component: ProductionRateNewComponent,
              canActivate: [AppRouteGuard]
          }]},

    
          {
            path: '',
            children: [ {
              path: 'ItemType',
              component: ItemTypeComponent,
              canActivate: [AppRouteGuard]
          }]},
    
          {
            path: '',
            children: [ {
              path: 'Category',
              component: CategoryComponent,
              canActivate: [AppRouteGuard]
           }]},
    
           {
             path: '',
             children: [ {
               path: 'SubCategory',
               component: SubCategoryComponent,
               canActivate: [AppRouteGuard]
            }]},
            {
              path: '',
              children: [ {
                path: 'configuration/overtime-configuration',
                component: OvertimeConfigurationComponent,
                canActivate: [AppRouteGuard]
             }]},

             {
              path: 'configuration',
              children: [ {
                path: 'configuration/hr-configuration/hr-configuration',
                component: HrConfigurationComponent,
                canActivate: [AppRouteGuard]
             }]},

            {
              path: '',
              children: [ {
                path: 'FineType',
                component: FineTypeComponent,
                canActivate: [AppRouteGuard]
             }]},
             {
                 path: 'SalaryType',
                 component: SalaryTypeComponent,
                 canActivate: [AppRouteGuard]
             }
             ,
             {
                 path: 'LoanRequest',
                 component: LoanRequestComponent,
                 canActivate: [AppRouteGuard]
             },
            {
              path: 'Salary',
              component: SalaryComponent,
              canActivate: [AppRouteGuard]
             },
             {
              path: 'SalaryGroup',
              component: SalaryGroupComponent,
              canActivate: [AppRouteGuard]
             },
            {
              path: 'SalaryWizard',
              component: SalaryWizardComponent,
              canActivate: [AppRouteGuard]
             },
             {

              path: 'Resignation',
              component: ResignationComponent,
              canActivate: [AppRouteGuard]
            },

            

            {

              path: 'Achievement',
              component: AchievementComponent,
              canActivate: [AppRouteGuard]

            },

            

            
            {
              path: 'CostCenter',
              component: CostCenterComponent,
              canActivate: [AppRouteGuard]
            },

            




            {

              path: 'AdvanceRequest',
              component: AdvanceRequestComponent,
              canActivate: [AppRouteGuard]
            },


            {

              path: 'WorkGroup',
              component: GroupComponent,
              canActivate: [AppRouteGuard]
            },

            {

             path: 'ProductionRate',
              component: ProductionRateNewComponent,
              canActivate: [AppRouteGuard]
           },
            {

              path: 'Overtime',
              component: OvertimeComponent,
              canActivate: [AppRouteGuard] 
            
            },
            {
               path: 'Overtime',
               component: OvertimeComponent,
               canActivate: [AppRouteGuard] 
            },

            // {

            //   path: 'Company',
            //   component: CompanyComponent
            // },
            

            {

              path: 'OvertimeNew',
              component: OvertimeNewComponent,
              canActivate: [AppRouteGuard] 
            },

            {

              path: 'Overtime',
              component: OvertimeComponent,
              canActivate: [AppRouteGuard] 
            },
           {
              path: 'ItemWiseProduction',
              component: ItemWiseProductionComponent,
              canActivate: [AppRouteGuard] 
           },
           {
            path: 'tax-slab',
            component: TaxSlabComponent,
            canActivate: [AppRouteGuard] 
         },

            {
               path: '',
               children: [ {
                 path: 'coming-soon',
                 component: ComingSoonComponent,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon2',
                 component: ComingSoon2Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon3',
                 component: ComingSoon3Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon4',
                 component: ComingSoon4Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon5',
                 component: ComingSoon5Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon6',
                 component: ComingSoon6Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon7',
                 component: ComingSoon7Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon8',
                 component: ComingSoon8Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon9',
                 component: ComingSoon9Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon10',
                 component: ComingSoon10Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon11',
                 component: ComingSoon11Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon12',
                 component: ComingSoon12Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon13',
                 component: ComingSoon13Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon14',
                 component: ComingSoon14Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon15',
                 component: ComingSoon15Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon16',
                 component: ComingSoon16Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon17',
                 component: ComingSoon17Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon18',
                 component: ComingSoon18Component,
                 canActivate: [AppRouteGuard]
             }]},{
               path: '',
               children: [ {
                 path: 'coming-soon19',
                 component: ComingSoon19Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon20',
                 component: ComingSoon20Component,
                 canActivate: [AppRouteGuard]
             }]},
             {
               path: '',
               children: [ {
                 path: 'coming-soon21',
                 component: ComingSoon21Component,
                 canActivate: [AppRouteGuard]
             }]},
             
            {
              path: '',
              children: [ {
                path: 'salaryList',
                component: SalaryListComponent,
                canActivate: [AppRouteGuard]
            }]},



             {
              path: '',
              children: [ {
                path: 'gratuity',
                component: GratuityComponent,
                canActivate: [AppRouteGuard]
            }]},
             
            {
              path: '',
              children: [ {
                path: 'gratuity-config',
                component: GratuityConfigComponent,
                canActivate: [AppRouteGuard]
            }]},
            
];  
