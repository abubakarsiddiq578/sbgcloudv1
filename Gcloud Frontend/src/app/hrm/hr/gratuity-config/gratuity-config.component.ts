import { Component, OnInit, AfterViewInit, Injector, ViewChild, Input, Output, EventEmitter, PipeTransform } from '@angular/core';
import { FormBuilder, AbstractControl, Validators, FormGroup } from '@angular/forms';
import { CostCenterServiceProxy, DepartmentServiceProxy, CostCenterDto, DepartmentDto, EmployeeDto, EmployeeServiceProxy, GratuityConfigurationServiceProxy, GratuityConfigurationDto, SalaryExpenseTypeDto, SalaryExpenseTypeServiceProxy, ListResultDtoOfAllowanceTypeDropdownDto, Department } from '@app/shared/service-proxies/service-proxies';
import { ConfigDropdownComponent } from './config-dropdown/config-dropdown.component';
import swal from 'sweetalert2';



@Component({
    selector: 'gratuity-config',
    templateUrl: 'gratuity-config.component.html',
    providers: [CostCenterServiceProxy,
        DepartmentServiceProxy,
        EmployeeServiceProxy,
        GratuityConfigurationServiceProxy,
        SalaryExpenseTypeServiceProxy
    ]
})

export class GratuityConfigComponent implements OnInit {




    @ViewChild('searchGratuityConfigDropdownModal') searchGratuityConfigDropdownModal: ConfigDropdownComponent
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    data: string[] = [];
    dataDept: string[] = [];
    dataCost: string[] = [];
    dataEmp: string[] = [];

    GradtuityApproved: Boolean

    tempArr: any[] = [];
    tempArrCost: any[] = [];
    tempArray: any[] = [];
    tempArrayEmp: any[] = [];

    EmployeesArrayId: string;
    CostCenterArrayId: string;

    DepartmentArrayId: string;
    salary: SalaryExpenseTypeDto = new SalaryExpenseTypeDto();




    _form: FormGroup

    costCenter: CostCenterDto = new CostCenterDto()
    department: DepartmentDto = new DepartmentDto()
    employee: EmployeeDto = new EmployeeDto()
    salaryExpType: SalaryExpenseTypeDto[] = []
    gratuityApplicable: GratuityConfigurationDto[] = []
    abc: string[] = [];
    gratuityDueOn: GratuityConfigurationDto[] = []
    CostCenters: CostCenterDto[] = [];
    //CostCenterName: string;

    CostCenterName: CostCenterDto[] = []
    DepartmentName: DepartmentDto[] = []

    checkBitId: boolean = true;
    SalaryId: number

    empSelected: EmployeeDto[];

    gratuityConfig: GratuityConfigurationDto = new GratuityConfigurationDto()



    constructor(injector: Injector,
        private _costCenterService: CostCenterServiceProxy,
        private _departmentService: DepartmentServiceProxy,
        private _employeeService: EmployeeServiceProxy,
        private _gratuityConfigService: GratuityConfigurationServiceProxy,
        private _salaryType: SalaryExpenseTypeServiceProxy,
        private formBuilder: FormBuilder
    ) { }




    resultDept: DepartmentDto[] = [];
    resultCost: CostCenterDto[] = [];
    resultEmp: EmployeeDto[] = [];
    resultSalaryType: SalaryExpenseTypeDto[] = [];

    ngOnInit() {

        this.getAllCostCenterRecord()
        this.getAllEmployeeRecord()
        this.getAllDepartmentRecord()
        this.getAllSalaryTypes()
        this.getAllApplicableDate()
        this.getAllDueOn()
        this.initValidation()
    }

    initValidation() {

        this._form = this.formBuilder.group({
            salaryTypeId: [null, Validators.required],
            gratuityApplicable: [null, Validators.required],
            gratuityDueOn: [null, Validators.required],


        });

    }

    getAllCostCenterRecord(): void {
        debugger;
        this._costCenterService.getAllCostCenters()
            .finally(() => {
                console.log('completed');
            })
            .subscribe((result: any) => {

                this.resultCost = result;
                this.CostCenterName = result;


            });
    }


    getAllDepartmentRecord(): void {
        debugger;
        this._departmentService.getAllDepartments()
            .finally(() => {
                console.log('completed');
            })
            .subscribe((result: any) => {

                this.resultDept = result;
                this.DepartmentName = result;


            });
    }


    getAllEmployeeRecord(): void {
        debugger;
        this._employeeService.getAllEmployees()
            .finally(() => {
                console.log('completed');
            })
            .subscribe((result: any) => {

                this.resultEmp = result;
                this.empSelected = result;


            });
    }



    getAllSalaryTypes() {
        this._salaryType.getAllSalaryExpenseType().subscribe((result) => {

            this.salaryExpType = result;
           
        })
    }
    getAllApplicableDate() {
        this._gratuityConfigService.getAllGratuityConfiguration().subscribe((result) => {

            this.gratuityApplicable = result;
    
        })
    }

    getAllDueOn() {
        this._gratuityConfigService.getAllGratuityConfiguration().subscribe((result) => {

            this.gratuityDueOn = result;
        })
    }



    CustomPopUp(): void {
        this.searchGratuityConfigDropdownModal.show()

    }

    @Input()
    Ivalue: any


    //CostCenterfilters on basis of Id

    CostCenterHead(event, id: number): void {
        debugger

        let h;
        h = event.currentTarget.checked;

        for (let i = 0; i < this.resultCost.length; i++) {
            if (this.resultCost[i].id == id) {


                if (h == true) {
                    this.tempArr[i] = id;

                }

                else {

                    this.tempArr[i] = null;
                }

            }

        }


        this.CostCenter();


    }



    //Filtered CostCenter
    CostCenter(): void {
        debugger
        this.CostCenterName = [];
        let p = 0;
        for (let i = 0; this.tempArr.length; i++) {
            if (this.resultCost[i].id == this.tempArr[i]) {
                this.CostCenterName[p] = this.resultCost[i];
                p++;
            }
        }
    }

    CostCenterId(event, id: number): void {
        debugger

        let h;
        h = event.currentTarget.checked;

        for (let i = 0; i < this.resultCost.length; i++) {
            if (this.resultCost[i].id == id) {

                if (h = true) {
                    this.tempArrCost[i] = id;
                }
                else {
                    //id of cost center array
                    this.tempArrCost[i] = null;
                }
            }
        }

        this.CostCenterArrayId = this.tempArr.toString();
    }


    DepartmentId(event, id: number): void {
        debugger

        let h;
        h = event.currentTarget.checked;

        for (let i = 0; i < this.resultDept.length; i++) {
            if (this.resultDept[i].id == id) {

                if (h = true) {
                    this.tempArray[i] = id;
                }
                else {

                    //id of dept array
                    this.tempArray[i] = null;
                }

            }

        }
        this.DepartmentArrayId = this.tempArray.toString();
        //    this.Employee();
        this.employeeList();
    }


    //employees on the basis of depart and cost center

    employeeList() {

        this.empSelected = []
        let t = 0;
        for (let j = 0; j < this.CostCenterName.length; j++) {

            for (let n = 0; n < this.tempArray.length; n++) {

                if (this.tempArray[n] == this.resultEmp[n].dept_ID && this.CostCenterName[n].id == this.resultEmp[n].costCenterId) {

                    this.empSelected[t] = this.resultEmp[n];
                    t++;
                }


            }


        }



    }




    EmployeesId(event, id: number): void {
        debugger

        let h;
        h = event.currentTarget.checked;

        for (let i = 0; i < this.resultEmp.length; i++) {
            if (this.resultEmp[i].id == id) {


                if (h == true) {
                    this.tempArrayEmp[i] = id;

                }

                else {

                    this.tempArrayEmp[i] = null;
                }


            }

        }
        //selected employee array id's

        this.EmployeesArrayId = this.tempArrayEmp.toString();
    }


    save(): void {
        debugger
        this.saving = true;
        this.gratuityConfig.employeeId = this.EmployeesArrayId;
        this.gratuityConfig.costCenterId = this.CostCenterArrayId;
        this.gratuityConfig.departmentId = this.DepartmentArrayId;
       
       
        
     


        this._gratuityConfigService.create(this.gratuityConfig)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                swal({
                    title: 'Added',
                    text: 'Your Configuration has been added.',
                    type: 'success',
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                  }).catch(swal.noop)

            });
     



        }
}
