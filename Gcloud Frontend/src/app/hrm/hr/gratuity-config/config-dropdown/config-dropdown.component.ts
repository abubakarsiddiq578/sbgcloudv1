import { Component, OnInit, ElementRef, ViewChild, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { SalaryExpenseTypeServiceProxy, AllowanceServiceProxy, SalaryExpenseTypeDto, AllowanceDto, AllowanceTypeDto, AllowanceTypeServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-config-dropdown',
  templateUrl: './config-dropdown.component.html',
  styleUrls: ['./config-dropdown.component.scss'],
  providers: [
     SalaryExpenseTypeServiceProxy,
     AllowanceTypeServiceProxy
]
})
export class ConfigDropdownComponent implements OnInit {

  @ViewChild('searchGratuityConfigDropdownModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  constructor(injector: Injector,
    private _allownaceService: AllowanceTypeServiceProxy,



    private _salaryTypeService: SalaryExpenseTypeServiceProxy,
    private formBuilder: FormBuilder
    ) { }

    resultSalaryType: SalaryExpenseTypeDto[] = [];
    resultAllowance: AllowanceTypeDto[] = [];

    active: boolean = false;

  ngOnInit() {
    this.getAllAllowancesRecord();
    this.getAllSalaryTypeRecord();
  }

  getAllAllowancesRecord(): void {
    debugger;
    this._salaryTypeService.getAllSalaryExpenseType()
        .finally(() => {
            console.log('completed');
        })
        .subscribe((result: any) => {

            this.resultSalaryType = result;


        });
}

show(){
  this.active = true;
  this.modal.show();

}

close(){
  this.active = false
  this.modal.hide();
}

getAllSalaryTypeRecord(): void {
    debugger;
    this._allownaceService.getAllAllowanceTypes()
        .finally(() => {
            console.log('completed');
        })
        .subscribe((result: any) => {

            this.resultAllowance = result;


        });
}

}
