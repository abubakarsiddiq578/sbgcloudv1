import { Component, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddDeductionTypeComponent } from './add-deduction/add-deduction.component';
import { EditDeductionTypeComponent } from './edit-deduction/edit-deduction.component';
import { DeductionTypeServiceProxy, DeductionTypeDto } from '../../../shared/service-proxies/service-proxies';
import { Http } from '@angular/http';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;
@Component({
  selector: 'app-deduction-type',
  templateUrl: './deduction-type.component.html',
  providers: [DeductionTypeServiceProxy]
})


export class DeductionTypeComponent implements OnInit, AfterViewInit {

  @ViewChild('addDeductionTypeModal') addDeductionTypeModal: AddDeductionTypeComponent;
  @ViewChild('editDeductionTypeModal') editDeductionTypeModal: EditDeductionTypeComponent;

    public dataTable: DataTable;
    result: DeductionTypeDto[]=[];
    data: string[] = [];
    constructor(injector: Injector , 
      /*private _authService: AppAuthService,*/
      private http: Http,
      private _deductionTypeService: DeductionTypeServiceProxy,
      private _router : Router
      
  ) 
  {
      //super(injector);
  }

  globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "DeductionType")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate(['']);
      }
      this.getAllRecord();
      
    }
    getAllRecord(): void {
    this._deductionTypeService.getAllDeductionTypes()
    .finally(()=>{
        console.log('completed');
    })
    .subscribe((result:any)=>{
this.result = result;
this.paging();
this.getAllData(); 

    });
  }

  paging(): void{
    this.dataTable = {
      headerRow: [ 'Deduction Title', 'Deduction Amount', 'Detail', 'Actions' ],
      footerRow: [ 'Deduction Title', 'Deduction Amount', 'Detail', 'Actions' ],

      dataRows: [
          // ['1234', 'Manager', 'Develop', ''],
          // ['1234', 'Manager', 'Develop', 'btn-round'],
          // ['1234', 'Manager', 'Develop', 'btn-simple'],
      ]
   };
  }
  getAllData():void{
    let i;
    for(i=0; i<this.result.length ; i++)
    {
      this.data.push(this.result[i].deductionTitle);
      this.data.push(this.result[i].deductionAmmount.toString());
      this.data.push(this.result[i].deductionDetail);
      this.data.push(this.result[i].id.toString());
      
      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
}



    ngAfterViewInit() {
      this.showDataTble();
    }

    showDataTble():void {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "DeductionType")) {
        this.globalFunction.showNoRightsMessage("Delete");
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Deduction Type!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
      if (result.value) {
        
        this._deductionTypeService.delete(parseInt(id))
          .finally(() => {
   
          this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Deductoin Type has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
          }).catch(swal.noop)
           });
      } else {
        swal({
            title: 'Cancelled',
            text: 'Your Deduction Type is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        }).catch(swal.noop)
      }
    })
    }

    AddDeductionType(): void {
      if (!this.globalFunction.hasPermission("Create", "DeductionType")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addDeductionTypeModal.show();
  }

  EditDeductionType(id:string): void{
    if (!this.globalFunction.hasPermission("Edit", "DeductionType")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
      this.editDeductionTypeModal.show(parseInt(id));
  }
}
