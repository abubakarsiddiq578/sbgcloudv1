import { Component, OnInit, Injector, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DeductionTypeServiceProxy, DeductionTypeDto } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'edit-deduction-type',
  templateUrl: './edit-deduction.component.html',
  providers: [DeductionTypeServiceProxy]
})
export class EditDeductionTypeComponent implements OnInit {

  @ViewChild('editDeductionTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  deduction: DeductionTypeDto = new DeductionTypeDto();

  _deductionTypeValidation :FormGroup

  constructor(
      injector: Injector,
      // private _userService: UserServiceProxy
      private _deductionTypeService: DeductionTypeServiceProxy,
      private formBuilder : FormBuilder
     
  ) {
      // super(injector);
  }


  show(id:number): void {
    
    this._deductionTypeService.get(id)
        .finally(() => {
            this.active = true;
            this.modal.show();
        })
        .subscribe((result: DeductionTypeDto)=> {
            this.deduction = result;
        })
    
}


     /////////////////////////Validations//////////////////////////////////////
     initValidation() {

        this._deductionTypeValidation = this.formBuilder.group({
           
            deductionTitle : [null , Validators.required],
            deductionAmount : [null , Validators.required],
           
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    onType() {
    
      if (this._deductionTypeValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._deductionTypeValidation);
      }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }






save(): void {
    debugger
    this.saving = true;
    this._deductionTypeService.update(this.deduction)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
        // this.notify.info(this.l('Record Updated Successfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }


  ngOnInit(): void {
    
    this.initValidation();

}




  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  
  close(): void {
      this.active = false;
      this.modal.hide();
  }

}
