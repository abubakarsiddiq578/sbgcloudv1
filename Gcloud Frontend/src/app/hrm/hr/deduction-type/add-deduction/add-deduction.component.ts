import { Component, OnInit, Output, EventEmitter, Injector, ElementRef, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DeductionTypeDto, DeductionTypeServiceProxy } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'add-deduction-type',
  templateUrl: './add-deduction.component.html',
})
export class AddDeductionTypeComponent implements OnInit {

  @ViewChild('addDeductionTypeModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    deduction: DeductionTypeDto = new DeductionTypeDto();
    
    _deductionTypeValidation :FormGroup


    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       private _deductionTypeService : DeductionTypeServiceProxy,
       private formBuilder : FormBuilder
    ) {
        // super(injector);
    }


     /////////////////////////Validations//////////////////////////////////////
     initValidation() {

        this._deductionTypeValidation = this.formBuilder.group({
           
            deductionTitle : [null , Validators.required],
            deductionAmount : [null , Validators.required],
           
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    onType() {
    
      if (this._deductionTypeValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._deductionTypeValidation);
      }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }



    ///////////////////////////////////////////////////////////////
  

    show(): void{

        this.active = true;
        this.modal.show();
        this.deduction = new DeductionTypeDto();
        this.deduction.init({isActive:true});

        this._deductionTypeValidation.markAsUntouched({onlySelf:true});
    }

    save(): void {
        //TODO: Refactor this, don't use jQuery style code
      
      this.saving = true;
        this._deductionTypeService.create(this.deduction)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                // this.notify.info(this.l('Saved Successfully'));
                this.close();   
                this.modalSave.emit(null);
                
            });
      }

    ngOnInit(): void {

        this.initValidation();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
