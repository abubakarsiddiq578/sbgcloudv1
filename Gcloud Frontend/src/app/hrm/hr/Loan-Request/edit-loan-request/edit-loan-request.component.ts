import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { LoanRequestDto, LoanRequestServiceProxy, EmployeeDropdownDto, EmployeeDropDownServiceProxy, HRConfigurationServiceProxy, HRConfigurationDto, EmployeeServiceProxy, EmployeeDto } from '../../../../shared/service-proxies/service-proxies';
//import { AppComponentBase } from '@shared/app-component-base';
//import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as moment from 'moment';
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';

@Component({
  selector: 'edit-loan-request',
  templateUrl: './edit-loan-request.component.html',
  //animations: [appModuleAnimation()],
  providers: [LoanRequestServiceProxy , EmployeeDropDownServiceProxy , HRConfigurationServiceProxy, EmployeeServiceProxy]
})

export class EditLoanRequestComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('EditLoanRequestModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    loanRequest: LoanRequestDto = new LoanRequestDto();
    employeeDropDown: EmployeeDropdownDto[] = null;  
    public EmpName: string;
    employee : EmployeeDto[] = [];
    public docDate = new Date();
    public repayementStartDate = new Date();
    _loanRequestValidation : FormGroup

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDropdownDto[]>;
  
    public empName : string ;
      
  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;


    constructor(
        private loanRequestServiceProxy: LoanRequestServiceProxy,
        private employeeDropDownServiceProxy: EmployeeDropDownServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder,
        private employeeService : EmployeeServiceProxy
     ) {
    }



    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];


            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }

        });

    }


    EmployeeId(id): void {
        debugger
 
        for(let i = 0; i< this.employee.length; i++)
        {
          if(this.employee[i].id == id)
          {
            this.EmpName = this.employee[i].employee_Name;
 
            this.loanRequest.employeeId = this.employee[i].id;
          }
        }
 
 
      }
 
      addSearchDD(){
        this.searchEditEmployeeDropdownModel.show()
      }

    ngOnInit(): void {
        this.initValidation();
        this.fillEmployeeDropDown();
        this.getAllHRConfiguration();
        this.employeeService.getAllEmployees()
        .subscribe((result) => {
            this.employee = result;
 
        })
    }




      /////////////////////////////////////////////////////////////////////////
    initValidation() {

        this._loanRequestValidation = this.formBuilder.group({
            
            employeeId: [null, Validators.required],
            loanAmount: [null, Validators.required],
            repaymentAmount :  [null, Validators.required]
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
   /* onType() {
    
      if (this._loanRequestValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._loanRequestValidation);
      }
    }*/

    onType() {
 
        if (this._loanRequestValidation.valid) {
        
          let a,p=0 ;
          for(a=0;a<this.employeeDropDown.length;a++){
            if(this.loanRequest.employeeId == this.employeeDropDown[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
          
            this.validateAllFormFields(this._loanRequestValidation);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._loanRequestValidation);
           
        }
      }
    
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }

    ///////////////////////////////////////////////////////////////////////////
///////////////////////////////Search Code//////////////////////////////////
public employeeDropdownSearch(){
  
    this.filteredOptions = this.myControl.valueChanges
     .pipe(
       startWith<string | EmployeeDropdownDto >(''),
       map(value => typeof value === 'string' ? value : value.employee_Name),
       map(name => name ? this._filter(name) : this.employeeDropDown.slice())
     ); 
    
   }
   
   private _filter(value: string): EmployeeDropdownDto[]{
   
    
     const filterValue = value.toLowerCase();
   
     return this.employeeDropDown.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
   }
   
   
   displayEmployee = (emp: EmployeeDropdownDto):any => {
     debugger;
     
     if(emp instanceof EmployeeDropdownDto)
     {
         this.loanRequest.employeeId = emp.id ;
         return emp.employee_Name ;
     }
     this.loanRequest.employeeId = null;
     return emp;
   
   }
 ///////////////////////////////////////////End of Search Code////////////////////////////////////////////



    show(id:number): void {
        this.loanRequestServiceProxy.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: LoanRequestDto) => {
                this.loanRequest = result;

                this.docDate = this.loanRequest.docDate.toDate();
                this.repayementStartDate = this.loanRequest.repayementStartDate.toDate();

                this.employeeDropDownServiceProxy.get(result.employeeId).subscribe((result)=>{
                    this.empName = result.employee_Name ;
                });
            });
            this.employeeDropdownSearch();
            this._loanRequestValidation.markAsUntouched({onlySelf:true});
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        this.saving = true;
        debugger;
        this.loanRequest.docDate = moment(this.docDate).add(5,'hour');
        this.loanRequest.repayementStartDate = moment(this.repayementStartDate).add(5,'hour');

        this.loanRequestServiceProxy.update(this.loanRequest)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
    }
   

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    doTextareaValueChange(ev){
        try {
          this.loanRequest.detail = ev.target.value;
        } catch(e) {
          console.info('could not set textarea-value');
        }
    }

    fillEmployeeDropDown(){
        this.employeeDropDownServiceProxy.getEmployeeDropdown()
        .subscribe((result) => {
            this.employeeDropDown = result.items;
        });
    }

    notify(){
        swal({
            title: "Success!",
            text: "Record Updated Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}
