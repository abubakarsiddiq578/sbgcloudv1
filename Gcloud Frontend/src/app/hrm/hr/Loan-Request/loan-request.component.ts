// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddLoanRequestComponent } from "./add-loan-request/add-loan-request.component";
import { EditLoanRequestComponent } from "./edit-loan-request/edit-loan-request.component";
import { LoanRequestDto, LoanRequestServiceProxy, Employee, EmployeeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  //footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
    selector: 'loan-request',
    templateUrl: 'loan-request.component.html',
    providers: [LoanRequestServiceProxy, EmployeeServiceProxy] 
})

export class LoanRequestComponent implements OnInit, AfterViewInit {
    
    public dataTable: DataTable;
    @ViewChild('AddLoanRequestModal') public AddLoanRequestModal: AddLoanRequestComponent;
    @ViewChild('EditLoanRequestModal') EditLoanRequestModal: EditLoanRequestComponent;

    data: string[] = [];

    loanRequest: LoanRequestDto[];

    constructor(private loanRequestServiceProxy : LoanRequestServiceProxy,private _router : Router) 
    {
    }
    globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "LoanRequest")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
      this.getAllLoanRequest();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        //alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    AddLoanRequest(): void {
      if (!this.globalFunction.hasPermission("Create", "LoanRequest")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.AddLoanRequestModal.show();
    }

    EditLoanRequest(id: string): void {
      if (!this.globalFunction.hasPermission("Edit", "LoanRequest")) {
        this.globalFunction.showNoRightsMessage("Edit");
        return
      }
      this.EditLoanRequestModal.show(parseInt(id));  
  }

  initDataTable(): void{
    this.dataTable = {
      headerRow: [ 'Document No', 'Document Date', 'Employee Name', 'Loan Amount', 'Monthly Repayement Amount', 'Repayment Start Date' , 'Detail' , 'Actions' ],
      // footerRow: [ 'Document No', 'Document Date', 'Employee Name', 'Loan Amount', 'Monthly Repayement Amount', 'Repayment Start Date' , 'Detail' , 'Actions' ],

      dataRows: []
   };
  }

  fillDataTable(){

    let i;
    for (i= 0; i < this.loanRequest.length; i++) {

      this.data.push(this.loanRequest[i].docNo)
      this.data.push( this.loanRequest[i].docDate.toDate().toDateString())
      this.data.push(this.loanRequest[i].employee.employee_Name)
      this.data.push(this.loanRequest[i].loadAmount.toString())
      this.data.push(this.loanRequest[i].monthlyRepayementAmount.toString())
      this.data.push(this.loanRequest[i].repayementStartDate.toDate().toDateString())
      this.data.push(this.loanRequest[i].detail.toString())
      this.data.push(this.loanRequest[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];
    }
  }

  getAllLoanRequest(): void{
    this.loanRequestServiceProxy.getLoanRequest()
    .subscribe((result) => {
      this.initDataTable()
      this.loanRequest = result
      this.fillDataTable();   
    });
  }

  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "LoanRequest")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Loan Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
    if (result.value) {
      debugger;
      this.loanRequestServiceProxy.delete(parseInt(id))
        .finally(() => {
             
          this.getAllLoanRequest();
        })
        .subscribe(() => {
          swal({
            title: 'Deleted!',
            text: 'Your Loan Request has been deleted.',
            type: 'success',
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
        }).catch(swal.noop)
         });
    } else {
      swal({
          title: 'Cancelled',
          text: 'Your Loan Request is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
      }).catch(swal.noop)
    }
  })
  }
}
