import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { LoanRequestDto, LoanRequestServiceProxy, EmployeeDropdownDto, EmployeeDropDownServiceProxy, ListResultDtoOfEmployeeDropdownDto, HRConfigurationServiceProxy, HRConfigurationDto, EmployeeDto, EmployeeServiceProxy } from '../../../../shared/service-proxies/service-proxies';
//import { AppComponentBase } from '@shared/app-component-base';
//import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { UnitOfTime } from 'ngx-bootstrap/chronos/types';
import { unitOfTime } from 'moment-timezone';
import { parseDate } from 'ngx-bootstrap/chronos';
import * as moment from 'moment';
//import { OwlDateTimeModule , OwlNativeDateTimeModule} from 'ng-pick-datetime';
import swal from 'sweetalert2';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';

@Component({
  selector: 'add-loan-request',
  templateUrl: './add-loan-request.component.html',
  styleUrls : ['./add-loan-request.scss'],
  providers: [LoanRequestServiceProxy , EmployeeDropDownServiceProxy, HRConfigurationServiceProxy, EmployeeServiceProxy] 
})
export class AddLoanRequestComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('AddLoanRequestModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    
    public EmpName: string;

    loanRequest: LoanRequestDto = new LoanRequestDto();
    employeeDropDown: EmployeeDropdownDto[] = null;  

    isSelect: boolean = true;

    public docDate = new Date();
    public repayementStartDate = new Date();

    public myDate = new Date(); // Current Date

    employee : EmployeeDto[] = [];

    _loanRequest : LoanRequestDto[];
    _docNo : abc[] = [];

    _loanRequestValidation : FormGroup

    date: string;

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDropdownDto[]>;
  
    public empName : string ;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    /*Employees = [
        {value: '1', viewValue: 'Saad Afzaal'},
        {value: '2', viewValue: 'Waqar Raza'},
        {value: '3', viewValue: 'Ayesha Rehman'},
    ];*/

    constructor(
        private loanRequestServiceProxy: LoanRequestServiceProxy,
        private employeeDropDownServiceProxy: EmployeeDropDownServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder,
        private employeeService: EmployeeServiceProxy
    ) {
        //super(injector);

        this.date = moment().format('DD/MM/YYYY');
    }

    ngOnInit(): void {
        this.initValidation();
        this.myDate = new Date();
        this.fillEmployeeDropDown();
        this.getAllHRConfiguration();
        this.employeeService.getAllEmployees()
       .subscribe((result) => {
           this.employee = result;

       })
     
    }


    EmployeeId(id): void {
        debugger
 
        for(let i = 0; i< this.employee.length; i++)
        {
          if(this.employee[i].id == id)
          {
            this.EmpName = this.employee[i].employee_Name;
 
            this.loanRequest.employeeId = this.employee[i].id;
          }
        }
 
 
      }
 
      addSearchDD(){
        this.searchEmployeeDropdownModel.show()
      }

      public employeeDropdownSearch(){

        this.filteredOptions = this.myControl.valueChanges
         .pipe(
           startWith<string | EmployeeDto >(''),
           map(value => typeof value === 'string' ? value : value.employee_Name),
           map(name => name ? this._filter(name) : this.employee.slice())
         );
     
       }
     
       private _filter(value: string): EmployeeDto[]{
     
     
         const filterValue = value.toLowerCase();
     
         return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
       }
     
     
       displayEmployee = (emp: EmployeeDto):any => {
         debugger;
     
         if(emp instanceof EmployeeDto)
         {
             this.loanRequest.employeeId = emp.id ;
             return emp.employee_Name ;
         }
         this.loanRequest.employeeId = null;
         return emp;
     
       }



      //Item
 
      displayItem = (emp: EmployeeDto): any => {
        debugger;
 
        if (emp instanceof EmployeeDto) {
            this.loanRequest.employeeId = emp.id;
          return emp.employee_Name;
        }
        return emp;
 
      }




    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];
        });

    }

    /////////////////////////////////////////////////////////////////////////
    initValidation() {

        this._loanRequestValidation = this.formBuilder.group({
            
            
            loanAmount: [null, Validators.required],
            repaymentAmount :  [null, Validators.required] ,
            employeeId :  [null, Validators.required]
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    /*onType() {
    
      if (this._loanRequestValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._loanRequestValidation);
      }
    }*/

    onType() {
 
        if (this._loanRequestValidation.valid) {
        
          let a,p=0 ;
          for(a=0;a<this.employeeDropDown.length;a++){
            if(this.loanRequest.employeeId == this.employeeDropDown[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
          
            this.validateAllFormFields(this._loanRequestValidation);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._loanRequestValidation);
           
        }
      }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }

    ///////////////////////////////////////////////////////////////////////////
    




    show(): void {
        this.active = true;
        this.modal.show();
        this.loanRequest = new LoanRequestDto();
        this.loanRequest.init({ isActive: true  , employeeId: 0 });

        this.loanRequestServiceProxy.getLoanRequest().subscribe((result)=>{

            this._loanRequest = result;
            if(this.hrConfig.autoCode == true){

                this.getAutoDocNumber();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }

            //this.getAutoDocNumber();

        })
        
        this.empName = null;
        this.employeeDropdownSearch();
        //let date = Date.now();
        
        //this.loanRequest.docDate =  moment(new Date);  
        this._loanRequestValidation.markAsUntouched({onlySelf:true});
    }


    getAutoDocNumber():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._loanRequest.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._loanRequest[i].docNo ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0  ){
          
            debugger;
            code = "1";
            this.loanRequest.docNo = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    let x;
                    code = this._docNo[this._docNo.length-1].code ;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.loanRequest.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.loanRequest.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.loanRequest.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.loanRequest.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.loanRequest.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.loanRequest.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.loanRequest.docNo = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.loanRequest.docNo =  "0" + code;
                      }
                      else if(code){
      
                          this.loanRequest.docNo =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.loanRequest.docNo = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.loanRequest.docNo =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.loanRequest.docNo =  temp[0] ;
                      }
    
                    }
                    
            }                     
        }
        this._docNo = [];
    }




    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    save(): void {
        this.loanRequest.docDate = moment(this.docDate).add(5,'hour');
        this.loanRequest.repayementStartDate = moment(this.repayementStartDate).add(5,'hour');

      this.saving = true;
        this.loanRequestServiceProxy.create(this.loanRequest)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify();
                this.close();   
                this.modalSave.emit(null);
            });
      }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    fillEmployeeDropDown(){
        this.employeeDropDownServiceProxy.getEmployeeDropdown()
        .subscribe((result) => {
            this.employeeDropDown = result.items;
        });
    }

    doTextareaValueChange(ev){
        try {
          this.loanRequest.detail = ev.target.value;
        } catch(e) {
          console.info('could not set textarea-value');
        }
    }

    notify(){
        swal({
            title: "Success!",
            text: "Saved Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}

// class for keeping all docNo number
class abc{
    public code: string;
  }
