// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Inject } from '@angular/core';
// import { EditTaxSlabComponent } from '../tax-slab/edit-tax-slab/edit-tax-slab.component';
import { AttendanceTypeServiceProxy, AttendanceTypeDto, AttendanceServiceProxy, AttendanceDto, EmployeeDto, EmployeeServiceProxy, DepartmentServiceProxy, DepartmentDto, EmployeeDesignationServiceProxy, EmployeeDesignationDto } from 'app/shared/service-proxies/service-proxies';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { from } from 'rxjs';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'attendance-setup',
  templateUrl: 'attendance-setup.component.html',
  styleUrls: ['./attendance-setup.component.less'],
  providers: [AttendanceTypeServiceProxy, EmployeeServiceProxy, DepartmentServiceProxy, EmployeeDesignationServiceProxy, AttendanceServiceProxy]
})

// export class SelectMultipleExample {
//     toppings = new FormControl();
//     toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
//   }

export class AttendanceSetupComponent implements OnInit, AfterViewInit {
  public dataTable1: DataTable;



  //   @ViewChild('editTaxSlabModal') editTaxSlabModal: EditTaxSlabComponent;
  @ViewChild('dept') departments: any;
  @ViewChild('desig') designations: any;
  @ViewChild('selectedEmp') selectedEmp: any;
  toppings = new FormControl();

  toppingList: string[] = ['Production', 'Quality Assurance', 'Marketing', 'HR', 'Administration', 'Managment'];

  public fromDate = new Date();
  public toDate = new Date();
  data: string[] = [];

  active: boolean = false;
  saving: boolean = false;


  attendaceTypes: AttendanceTypeDto[];
  Attendance: AttendanceDto = new AttendanceDto();
  allAttendances: AttendanceDto[];
  checkDiff: any;
  totalDays: any;

  IsEmployee: any = true;
  IsDepartment: any = false;
  IsDesignation: any = false;
  IsSelectedEmployees : any = false;

  allEmployees: EmployeeDto[];
  allDepartment: DepartmentDto[];
  allDesignation: EmployeeDesignationDto[];
  selectedDepartments: any = [];
  selectedDesignation: any = [];
  selectedEmployees: EmployeeDto[];
  // allSelectedEmployees: EmployeeDto = new EmployeeDto();
  allSelectedEmployees:EmployeeDto[];


  constructor(
    @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private _attendanceTypeService: AttendanceTypeServiceProxy,
    private _employeeService: EmployeeServiceProxy,
    private _departmentService: DepartmentServiceProxy,
    private _designationService: EmployeeDesignationServiceProxy,
    private _attendanceService: AttendanceServiceProxy
  ) {

  }


  OnSelectedEmployeeToggleChange() {
    debugger;
    this.IsEmployee = false;
    this.IsDepartment = false;
    this.IsDesignation = false;
    this.IsSelectedEmployees = true;
    if(this.IsSelectedEmployees == true)
    {
      this._employeeService.getAllEmployees()
      .subscribe((result)=> {
        this.selectedEmployees = result;
      });
    }

  }


  OnDesignationToggleChange() {
    this.IsEmployee = false;
    this.IsDepartment = false;
    this.IsDesignation = true;
    this.IsSelectedEmployees = false;
    
    if (this.IsDesignation == true) {
      this._designationService.getAllEmployeeDesignations()
        .subscribe((result) => {
          this.allDesignation = result;
        });
    }
  }


  OnDepartmentToggleChange() {
    this.IsEmployee = false;
    this.IsDepartment = true;
    this.IsDesignation = false;
    this.IsSelectedEmployees = false;
    if (this.IsDepartment == true) {
      this._departmentService.getAllDepartments()
        .subscribe((result) => {
          this.allDepartment = result;
        });
    }
  }

  OnEmployeeToggleChange() {
    this.IsEmployee = true;
    this.IsDepartment = false;
    this.IsDesignation = false;
    this.IsSelectedEmployees = false;
    if (this.IsEmployee == true) {
      this._employeeService.getAllEmployees()
        .subscribe((result) => {
          this.allEmployees = result;
        });
    }

  }


  getDays(event) {
    debugger;
    var from = moment(this.fromDate);
    var to = moment(this.toDate);

    this.checkDiff = from.diff(to, 'days');
    this.totalDays = this.checkDiff + 1;
  }


  ngOnInit() {

    if(this.IsEmployee = true )
    {
      this._employeeService.getAllEmployees()
        .subscribe((result) => {
          this.allEmployees = result;
        });
    }


    this._attendanceService.getAllAttendances()
    .subscribe((result)=> {
      debugger;
      this.allAttendances = result;
      this.paging();
      this.fillDataTable();
    });

    // reset and initialize detailDataTable Grid
    
    this.getAllAttendanceTypes();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('#datatables1').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table1 = $('#datatables1').DataTable();

    // Edit record
    table1.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table1.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table1.on('click', '.like', function (e) {
      //alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  paging() {
    this.dataTable1 = {
      headerRow: ['Employee', 'Department', 'Designation', 'Date', 'Status', 'Actions'],
      footerRow: ['Employee', 'Department', 'Designation', 'Date', 'Status', 'Actions'],

      dataRows: [
        //['150,000', '200,000', '150', '10%', '150', '10', ''],
        //['150,000', '200,000', '150', '10%', '150', '10', ''],
        //['150,000', '200,000', '150', '10%', '150', '10', ''],
      ]
    };
  }

  // get all salary group records
  getAllAttendanceTypes(): void {
    this._attendanceTypeService.getAllAttendanceTypes()
      .subscribe((result) => {
        this.attendaceTypes = result
      });
  }



  // fill datatable to show master record in datatable grid
  fillDataTable() {
    let i = 0;

    for (i = 0; i < this.allAttendances.length; i++) {
      this.data.push(this.allAttendances[i].employee.employee_Name);
      this.data.push(this.allAttendances[i].department.title);
      this.data.push(this.allAttendances[i].employeeDesignation.employeeDesignationName);
      this.data.push(this.allAttendances[i].attendanceDate.format());
      this.data.push(this.allAttendances[i].status.toString());
      this.data.push(this.allAttendances[i].id.toString());

      this.dataTable1.dataRows.push(this.data);

      this.data = [];
    }
  }

  // perform save opertaion to save data
  save(): void {
    if (this.IsEmployee == true) {
      this.allEmployees = [];
      this._employeeService.getAllEmployees()
        .subscribe((result) => {
          debugger;
          this.allEmployees = result;
          for (let i = 0; i < this.allEmployees.length; i++) {
            for (let j = 0; j < this.totalDays; j++) {
              this.Attendance.attendanceDate = moment(this.toDate).add('days', j);
              this.Attendance.employeeId = this.allEmployees[i].id;
              this.Attendance.departmentId = this.allEmployees[i].dept_ID;
              this.Attendance.designationId = this.allEmployees[i].desig_ID;
              this._attendanceService.create(this.Attendance)
                .finally(() => {
                })
                .subscribe(() => {
                  this.reset();
                  // this.notify();
                  // this.close();
                  //this.modalSave.emit(null);
                });
            }
          }
        });
    }

    if (this.IsDepartment == true) {
      debugger
      this.allEmployees = [];
      for (let i = 0; i < this.departments.value.length; i++) {
        this._employeeService.getAllEmployeesByDepartment((this.departments.value[i]))
          .subscribe((result) => {
            this.allEmployees = result;
            if (this.allEmployees.length > 0) {
              for (let k = 0; k < this.allEmployees.length; k++) {
                for (let j = 0; j < this.totalDays; j++) {
                  this.Attendance.attendanceDate = moment(this.toDate).add('days', j);
                  this.Attendance.employeeId = this.allEmployees[k].id;
                  this.Attendance.departmentId = this.allEmployees[k].dept_ID;
                  this.Attendance.designationId = this.allEmployees[k].desig_ID;
                  this._attendanceService.create(this.Attendance)
                    .finally(() => {

                    })
                    .subscribe(() => {
                      this.reset();
                      // this.notify();
                      // this.close();
                      //this.modalSave.emit(null);
                    });
                }
              }
            }
            else {
              alert("There is no employee in this department");
            }
          })
      }
    }

    if(this.IsDesignation == true )
    {
      debugger;
      this.allEmployees = [];
      for (let i = 0; i < this.designations.value.length; i++) {
        this._employeeService.getAllEmployeesByDesignation((this.designations.value[i]))
          .subscribe((result) => {
            debugger;
            this.allEmployees = result;
            if (this.allEmployees.length > 0) {
              for (let k = 0; k < this.allEmployees.length; k++) {
                for (let j = 0; j < this.totalDays; j++) {
                  this.Attendance.attendanceDate = moment(this.toDate).add('days', j);
                  this.Attendance.employeeId = this.allEmployees[k].id;
                  this.Attendance.departmentId = this.allEmployees[k].dept_ID;
                  this.Attendance.designationId = this.allEmployees[k].desig_ID;
                  this._attendanceService.create(this.Attendance)
                    .finally(() => {
                    })
                    .subscribe(() => {
                      this.reset();
                    });
                }
              }
            }
            else {
              alert("There is no employee in this department");
            }
          })
      }
    }

    if(this.IsSelectedEmployees == true){
      debugger;
      for(let i = 0; i< this.selectedEmp.value.length; i++)
      {
        this._employeeService.getAllEmployeesById(this.selectedEmp.value[i])
        .subscribe((result)=> {
          debugger;
          this.allSelectedEmployees = result;
          if (this.allSelectedEmployees.length !=  null) {

            for (let k = 0; k < this.allSelectedEmployees.length; k++) {
              for (let j = 0; j < this.totalDays; j++) {
                this.Attendance.attendanceDate = moment(this.toDate).add('days', j);
                this.Attendance.employeeId = this.allSelectedEmployees[k].id;
                this.Attendance.departmentId = this.allSelectedEmployees[k].dept_ID;
                this.Attendance.designationId = this.allSelectedEmployees[k].desig_ID;
                this._attendanceService.create(this.Attendance)
                  .finally(() => {
                  })
                  .subscribe(() => {
                    this.reset();
                  });
              }
            }
          }
          else {
            alert("There is no employee in this department");
          }
        })
      }
    }


  }

  reset(){
    debugger;
    this.Attendance.comments  = "";
    this.Attendance.departmentId = null;
    this.Attendance.designationId = null;
    this.Attendance.designationId = null;
    this.Attendance.employeeId = null;
    this.totalDays = 0;
    this.IsEmployee = true;
    this.IsDepartment = false;
    this.IsDesignation = false;
    this.IsSelectedEmployees = false;
    this.selectedEmp = [];
    this.departments = [];
    this.designations = [];


  }


  notifyMsg(title, msg) {
    swal({
      title: title,
      text: msg,
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  // delete record of salary group master 
  // deleteTaxSlabMasterDetail(id: string) {
  //   swal({
  //     title: 'Are you sure?',
  //     text: 'You will not be able to recover this Tax Slab!',
  //     type: 'warning',
  //     showCancelButton: true,
  //     confirmButtonText: 'Yes, delete it!',
  //     cancelButtonText: 'No, keep it',
  //     confirmButtonClass: "btn btn-success",
  //     cancelButtonClass: "btn btn-danger",
  //     buttonsStyling: false
  //   }).then((result) => {
  //     if (result.value) {
  //       this.taxSlabsMasterServiceProxy.delete(parseInt(id))
  //         .finally(() => {

  //           this.getAllTaxSlabs();
  //         })
  //         .subscribe(() => {
  //           swal({
  //             title: 'Deleted!',
  //             text: 'Your Tax Slab has been deleted.',
  //             type: 'success',
  //             confirmButtonClass: "btn btn-success",
  //             buttonsStyling: false
  //           }).catch(swal.noop)
  //         });
  //     } else {
  //       swal({
  //         title: 'Cancelled',
  //         text: 'Your Tax Slab is safe :)',
  //         type: 'error',
  //         confirmButtonClass: "btn btn-info",
  //         buttonsStyling: false
  //       }).catch(swal.noop)
  //     }
  //   })
  // }



}


