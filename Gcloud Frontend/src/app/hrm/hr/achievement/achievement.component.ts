import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddAchievementComponent } from './add-achievement/add-achievement.component';
import { EditAchievementComponent } from './edit-achievement/edit-achievement.component';
import { AchievementDto, AchievementServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';


declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-achievement',
  templateUrl: './achievement.component.html',
  styleUrls: ['./achievement.component.scss'],
  providers: [AchievementServiceProxy]
})
export class AchievementComponent implements OnInit, AfterViewInit {


  @ViewChild('addAchievementModal') addAchievementModal: AddAchievementComponent;

  @ViewChild('editAchievementModal') editAchievementModal: EditAchievementComponent;




  constructor(private AchievementServiceProxy: AchievementServiceProxy, private _router: Router) {
  }


  public dataTable: DataTable;
  globalFunction: GlobalFunctions = new GlobalFunctions

  data: string[] = [];

  Achievement: AchievementDto[];

  ngOnInit() {

    if (!this.globalFunction.hasPermission("View", "Achievement")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);
      return
    }

    this.getAllAchievement();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');

      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  initDataTable() {

    this.dataTable = {
      headerRow: ['Doc No', 'Doc Date', 'Employee', 'Achievement Date', 'Achievement Type', 'Achievement Detail', 'Actions'],
      footerRow: [/*'Doc No', 'Doc Date', 'Employee', 'Achievement Date', 'Achievement Type', 'Achievement Detail', 'Actions'*/],

      dataRows: []
    };

  }



  fillDataTable() {

    let i;
    for (i = 0; i < this.Achievement.length; i++) {

      this.data.push(this.Achievement[i].documentNo)
      this.data.push(this.Achievement[i].documentDate.toString())

      this.data.push(this.Achievement[i].employee.employee_Name)
      this.data.push(this.Achievement[i].achievementDate.toString())
      this.data.push(this.Achievement[i].achievementType)
      this.data.push(this.Achievement[i].achievementDetail)
      this.data.push(this.Achievement[i].id.toString())




      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }


  getAllAchievement(): void {
    this.AchievementServiceProxy.getAllAchievements()
      .subscribe((result) => {
        debugger;
        this.initDataTable()
        this.Achievement = result
        this.fillDataTable();
      });
  }

  AddAchievement(): void {
    if (!this.globalFunction.hasPermission("Create", "Achievement")) {
      this.globalFunction.showNoRightsMessage("Create")
      return
    }
    this.addAchievementModal.show();

  }

  EditAchievement(id: string): void {

    if (!this.globalFunction.hasPermission("Edit", "Achievement")) {
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    this.editAchievementModal.show(parseInt(id));

  }



  protected delete(id: string): void {

    if (!this.globalFunction.hasPermission("Delete", "Achievement")) {
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }

    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.AchievementServiceProxy.delete(parseInt(id))
          .finally(() => {
            this.getAllAchievement();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Achievement has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Achievement is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }




}
