import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AchievementDto, AchievementServiceProxy, EmployeeServiceProxy, EmployeeDto , HRConfigurationDto , HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';

@Component({
  selector: 'add-achievement',
  templateUrl: './add-achievement.component.html',
  styleUrls: ['./add-achievement.component.scss'],
  providers: [AchievementServiceProxy, EmployeeServiceProxy , HRConfigurationServiceProxy]
})
export class AddAchievementComponent implements OnInit {


  
  @ViewChild('addAchievementModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;
  @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
  active: boolean = false;
  saving: boolean = false;


  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  employee: EmployeeDto[] = [];
  EmpName : string;
  documentDate: Date = new Date(); // for getting datetime


  AchievementDate : Date = new Date(); //for getting datetime for achievementdate


  Achievement: AchievementDto = new AchievementDto();

  _achievement  : AchievementDto[]; //for getting disable docNo Value from  resignation table
  _docNo : abc[] =[];
  
  _achve : FormGroup // for validation

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  myControl = new FormControl();

  filteredOptions: Observable<EmployeeDto[]>;

  isAutoCode : boolean;

  public empName : string ;


  constructor(injector: Injector,
    private AchievementProxyService: AchievementServiceProxy,
    private employeeService: EmployeeServiceProxy, private _hrConfigService : HRConfigurationServiceProxy,
    private formBuilder : FormBuilder
    ) { }

  ngOnInit():void {

    this.employeeService.getAllEmployees()
    .subscribe((result) => {
      this.employee = result;
    })
    this.initValidation();
    this.getAllHRConfiguration();

  }



  initValidation() {

    this._achve = this.formBuilder.group({
        employeeId: [null, Validators.required],
        achievementDate : [null , Validators.required]
    });

}

/*onType() {

  if (this._achve.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._achve);
  }
}*/
onType() {
  debugger;
  if (this._achve.valid) {
    debugger;
    let a,p=0 ;
    for(a=0;a<this.employee.length;a++){
      if(this.Achievement.employeeId == this.employee[a].id) ///
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.empName = null;
      
      this.validateAllFormFields(this._achve);
      //this.employeeDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._achve);  
  }
}


validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}
/////////////////////////////////////////////////////////////////
// public employeeDropdownSearch(){
//   debugger;
//  this.filteredOptions = this.myControl.valueChanges
//   .pipe(
//     startWith<string | EmployeeDto >(''),
//     map(value => typeof value === 'string' ? value : value.employee_Name),
//     map(name => name ? this._filter(name) : this.employee.slice())
//   ); 
 
// }

// private _filter(value: string): EmployeeDto[]{

//   debugger;
//   const filterValue = value.toLowerCase();

//   return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
// }


// displayEmployee = (emp: EmployeeDto):any => {
 
//   if(emp instanceof EmployeeDto)
//   {
//       this.Achievement.employeeId = emp.id ;
//       return emp.employee_Name ;
//   }
//   this.Achievement.employeeId = null;
//   return emp;

// }

/////////////////////////////////////////////////////////////////////

EmployeeId(id): void {
  debugger

  for(let i = 0; i< this.employee.length; i++)
  {
    if(this.employee[i].id == id)
    {
      this.EmpName = this.employee[i].employee_Name;

      this.Achievement.employeeId = this.employee[i].id;
    }
  }


}
addSearchDD(){
  this.searchEmployeeDropdownModel.show()
}

getAllHRConfiguration(){

this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{

  this.hrConfig = result[0] ;
});

}



/////////////////////////////////////////////////////////////////////

  show(): void {

    this.active = true;
    this.modal.show();
    this.Achievement = new AchievementDto();
    this.AchievementProxyService.getAllAchievements().subscribe((result)=>{

      this._achievement = result;
      
      if(this.hrConfig.autoCode == true){
        debugger;
        this.getAutoDocNumber();
        this.isAutoCode = true ;
      }
      else if(this.hrConfig.autoCode == false){
        this.isAutoCode = false;
      }
      
    })
      this.empName = null;
      //this.employeeDropdownSearch();
      this._achve.markAsUntouched({onlySelf:true});
  }


    //this function generate auto doc number
    getAutoDocNumber():void{

      debugger;
      let i =0;
      
      let temp : any[];
     
      for(i=0 ;i< this._achievement.length ; i++){
  
          let abc_ ; 
  
          abc_ = new abc();
  
          abc_.code = this._achievement[i].documentNo ; 
  
          this._docNo.push(abc_);
  
      }
  
  
      let code : any ;
  
      if(this._docNo.length == 0  ){
        
          debugger;
          code = "1";
          this.Achievement.documentNo = "00" + code ;
  
      }
  
      else{
  
          if(this._docNo[this._docNo.length - 1] != null ){
  
                  let x;
                  code = this._docNo[this._docNo.length-1].code ;
                  temp = code;
                  if(code!=null){
                    temp = code.split("-");
                     x = parseInt(temp[0]);
                     x = x.toString();
                  }
                  debugger;
                  //let j = parseInt(code);
                  //
                  if(temp.length == 1 && x=="NaN"  ){

                    debugger;
                    temp[1] = 0;
                    temp[1] ++;

                   
                    if(temp[1] <=9){
    
                        this.Achievement.documentNo = temp[0] + "-00" + temp[1] ;
                    }
                    else if(temp[1] <=99){
    
                        this.Achievement.documentNo =  temp[0] + "-0" + temp[1] ;
                    }
                    else if(temp[1]){
    
                        this.Achievement.documentNo =  temp[0] + "-" +temp[1] ;
                    }

                  
                  }

                  else if (temp.length == 2) {

                    temp[1] ++;
                    if(temp[1] <=9){
    
                        this.Achievement.documentNo = temp[0] + "-00" + temp[1] ;
                    }
                    else if(temp[1] <=99){
    
                        this.Achievement.documentNo =  temp[0] + "-0" + temp[1] ;
                    }
                    else if(temp[1]){
    
                        this.Achievement.documentNo =  temp[0] + "-" +temp[1] ;
                    }

                  }

                  else if(temp.length == 0){

                    code ++;
                    if(code <=9){
    
                        this.Achievement.documentNo = "00" + code ;
                    }
                    else if(code<=99){
    
                        this.Achievement.documentNo =  "0" + code;
                    }
                    else if(code){
    
                        this.Achievement.documentNo =  code ;
                    }


                  } 
                  else if(temp.length==1 && x!="NaN" ){

                    temp[0]++;
                    if(temp[0] <=9){
    
                        this.Achievement.documentNo = "00" + temp[0] ;
                    }
                    else if(code<=99){
    
                        this.Achievement.documentNo =  "0" + temp[0];
                    }
                    else if(code){
    
                        this.Achievement.documentNo =  temp[0] ;
                    }

                  }



                  
          }                     
      }
      this._docNo = [];
  }

  save(): void {


    this.Achievement.documentDate = moment(this.documentDate).add(5,'hour');
    this.Achievement.achievementDate = moment(this.AchievementDate).add(5,'hour');
    this.saving = true;
    this.AchievementProxyService.create(this.Achievement)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }


  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }
  onShown():void{
    
  }

}

// class for keeping all docNo number
class abc{
  public code: string;
}
