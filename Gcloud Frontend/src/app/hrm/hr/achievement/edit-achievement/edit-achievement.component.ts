import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AchievementDto, AchievementServiceProxy, EmployeeServiceProxy, EmployeeDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import * as moment from 'moment';

import {  HRConfigurationDto , HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';


import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';


@Component({
  selector: 'edit-achievement',
  templateUrl: './edit-achievement.component.html',
  styleUrls: ['./edit-achievement.component.scss'],
  providers : [AchievementServiceProxy,EmployeeServiceProxy ,  HRConfigurationServiceProxy ] 
})
export class EditAchievementComponent implements OnInit {

    active: boolean = false;
    saving: boolean = false;

  @ViewChild('editAchievementModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;

  employee: EmployeeDto[] = [];

  documentDate: Date = new Date(); // for getting datetime
  EmpName: string;

  AchievementDate : Date = new Date(); //for getting datetime for achievementdate


  Achievement: AchievementDto = new AchievementDto();

  _achve : FormGroup

  myControl = new FormControl();

  filteredOptions: Observable<EmployeeDto[]>;

  public empName : string ;

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(injector: Injector,
    private AchievementServiceProxy: AchievementServiceProxy,
    private employeeService: EmployeeServiceProxy , private _hrConfigService : HRConfigurationServiceProxy,

    private formBuilder : FormBuilder
    
    ) { }

    show(id: number): void{


      if(this.hrConfig.autoCode == true){

        this.isAutoCode = true ;
      }
      else if(this.hrConfig.autoCode == false){
        this.isAutoCode = false;
      }

       this.AchievementServiceProxy.get(id)
        .finally(() => {
           this.active = true;
           this.modal.show();
           
       }).subscribe((result: AchievementDto) => {
            
        this.Achievement = result;

            this.documentDate = this.Achievement.documentDate.toDate();
            this.AchievementDate = this.Achievement.achievementDate.toDate();

            this.employeeService.get(result.employeeId).subscribe((result)=>{
                this.empName = result.employee_Name
            });
        
    });






    this.employeeDropdownSearch();
    this._achve.markAsUntouched({onlySelf:true});
    
  }

  EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.Achievement.employeeId = this.employee[i].id;
      }
    }


  }
  addSearchDD(){
    this.searchEditEmployeeDropdownModel.show()
  }

  initValidation() {

    this._achve = this.formBuilder.group({
        employeeId: [null, Validators.required],
        achievementDate : [null , Validators.required]
    });

    //this.getAllHRConfiguration();
}

/*onType() {

  if (this._achve.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._achve);
  }
}*/
//////////////////////////////////////////////

getAllHRConfiguration(){

  this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
  
    this.hrConfig = result[0] ;
  });
  
  }
  



/////////////////////////////////////////////////



onType() {
    debugger;
    if (this._achve.valid) {
      debugger;
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.Achievement.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
       
        this.validateAllFormFields(this._achve);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._achve);
       
    }
  }
  
validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}

  ngOnInit(): void {

        
    this.employeeService.getAllEmployees()
    .subscribe((result)=> {
        this.employee = result;
    })
    this.initValidation();
    this.getAllHRConfiguration();
}

/////////////////////////////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }
  
  private _filter(value: string): EmployeeDto[]{
  
    debugger;
    const filterValue = value.toLowerCase();
  
    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Achievement.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.Achievement.employeeId = null;
    return emp;
  
  }
  
  /////////////////////////////////////////////////////////////////////
  

onShown(): void {
  
}


close(): void {
  this.active = false;
  this.modal.hide();
}

notify(){
  swal({
      title: "Success!",
      text: "Record Updated Successfully.",
      timer: 2000,
      showConfirmButton: false
  }).catch(swal.noop)
}
save(): void {


  this.Achievement.documentDate = moment(this.documentDate).add(5,'hour');
  this.Achievement.achievementDate = moment(this.AchievementDate).add(5,'hour');
  this.saving = true;

  this.AchievementServiceProxy.update(this.Achievement)
    .finally(() => {this.saving = false;})
    .subscribe(() => {
      this.notify();
      this.close();
      this.modalSave.emit(null);
    });
}

}
