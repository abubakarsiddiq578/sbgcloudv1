import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { HolidayComponent } from '../holiday.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import * as moment from 'moment';
import swal from 'sweetalert2';
import { HolidayDto, HolidayServiceProxy , CostCenterDto ,CostCenterServiceProxy , DepartmentDto,DepartmentServiceProxy ,EmployeeDesignationDto , EmployeeDesignationServiceProxy , LeaveTypeDto , LeaveTypeServiceProxy} from 'app/shared/service-proxies/service-proxies';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-holiday',
  templateUrl: './edit-holiday.component.html',
  styleUrls : ['./edit-holiday.component.less'],
  providers: [ HolidayServiceProxy, CostCenterServiceProxy, DepartmentServiceProxy, EmployeeDesignationServiceProxy, LeaveTypeServiceProxy]
})
export class EditHolidayComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editHolidayModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    isActive: boolean;
    holidayEndDate: Date = new Date(); // for  holidayEnddatetime

    holidayStartDate: Date = new Date(); // for holidaystartdate


    costCenter: CostCenterDto[] = [];

    leaveType: LeaveTypeDto[] = [];
    employeeDesignation: EmployeeDesignationDto [] = [];

    department : DepartmentDto[] = [];

    _holiday : FormGroup;
    Holiday: HolidayDto = new HolidayDto();
    myControl = new FormControl();

    filteredOptions: Observable<CostCenterDto[]>;
  
    public costCenterName : string ;


    constructor(
        injector: Injector,
        private HolidayServiceProxy : HolidayServiceProxy , 
        private CostCenterServiceProxy: CostCenterServiceProxy,
        private DepartmentServiceProxy: DepartmentServiceProxy,
        private EmployeeDesignationServiceProxy : EmployeeDesignationServiceProxy,
        private LeaveTypeServiceServiceProxy: LeaveTypeServiceProxy,
        private formBuilder : FormBuilder
        // private _userService: UserServiceProxy
       
    ) {
        //super(injector);
    }

    initValidation() {


        this._holiday = this.formBuilder.group({
            costCenterId: [null, Validators.required],
            employeeDesignationId : [null , Validators.required],
            departmentId: [null, Validators.required],
            leaveTypeId : [null , Validators.required],
            holidayStartDate : [null , Validators.required],
            holidayEndDate : [null , Validators.required],
            
        });
    
    }

  

    show(id:number): void{

        debugger
        this.HolidayServiceProxy.get(id)
          .finally(() => {
            this.active = true;
            this.modal.show();
    
          })
          .subscribe((result: HolidayDto) => {
    
            this.Holiday = result;
            this.holidayStartDate = this.Holiday.holidayStartDate.toDate();
            this.holidayEndDate = this.Holiday.holidayEndDate.toDate();
            this.CostCenterServiceProxy.get(result.costCenterId).subscribe((result)=>{
                this.costCenterName = result.name ;
            });

            this.costCenterDropdownSearch();
            
          });    
    }

   /* onType() {

        if (this._holiday.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._holiday);
        }
      }*/
      
      validateAllFormFields(formGroup: FormGroup) {
      
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
      }
      onType() {
        debugger;
        if (this._holiday.valid) {
          debugger;
          let a,p=0 ;
          for(a=0;a<this.costCenter.length;a++){
            if(this.Holiday.costCenterId == this.costCenter[a].id) ///
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.costCenterName = null;
            
            this.validateAllFormFields(this._holiday);
            this.costCenterDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._holiday);
           
        }
      }
////////////////////////////////////////////////////////////////////

 //////////////////////////////Search Method Start///////////////////////////////////
 public costCenterDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | CostCenterDto >(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filter(name) : this.costCenter.slice())
    ); 
   
  }
  
  private _filter(value: string): CostCenterDto[]{
  
    debugger;
    const filterValue = value.toLowerCase();
  
    return this.costCenter.filter(option => option.name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayCostCenter = (costCentr: CostCenterDto):any => {
    debugger;
    
    if(costCentr instanceof CostCenterDto)
    {
        this.Holiday.costCenterId = costCentr.id ;
        return costCentr.name ;
    }
    this.Holiday.costCenterId =  null; 
    return costCentr;
  
  }

//////////////////////////////////////////////////////
    ngOnInit(): void {
    
        debugger;
        this.CostCenterServiceProxy.getAllCostCenters().subscribe((result)=>{

            
            this.costCenter = result ;
            


        })


        //to get all dpartments

        this.DepartmentServiceProxy.getAllDepartments().subscribe((result)=>{

            this.department = result ;
            


        })

        // to get all employee'sdesignation

        this.EmployeeDesignationServiceProxy.getAllEmployeeDesignations().subscribe((result)=>{

            this.employeeDesignation = result ;
            


        })


        //to get all leave types
        this.LeaveTypeServiceServiceProxy.getAllLeaveTypes().subscribe((result)=>{

            this.leaveType = result ;
            
        })
        this.initValidation();
    
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }


    save(): void {



        this.Holiday.holidayStartDate = moment(this.holidayStartDate).add(5,'hour');
        
        this.Holiday.holidayEndDate = moment(this.holidayEndDate).add(5,'hour');

        //this.Holiday.isActive = this.isActive;

        
    
        this.saving = true;
    
       
    
        this.HolidayServiceProxy.update(this.Holiday)
          .finally(() => { this.saving = false; })
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
      }



    notify() {
        swal({
          title: "Success!",
          text: "Record Updated Successfully.",
          timer: 2000,
          showConfirmButton: false
        }).catch(swal.noop)
      }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }






}

