// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddHolidayComponent } from './add-holiday/add-holiday.component';
import { EditHolidayComponent } from './edit-holiday/edit-holiday.component';
import { HolidayDto, HolidayServiceProxy} from 'app/shared/service-proxies/service-proxies';

import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'holiday',
  templateUrl: 'holiday.component.html',
  providers: [HolidayServiceProxy]
})

export class HolidayComponent implements OnInit, AfterViewInit {


  Holiday: HolidayDto[];
  public dataTable: DataTable;
  data: string[] = [];

  @ViewChild('addHolidayModal') addHolidayModal: AddHolidayComponent;
  @ViewChild('editHolidayModal') editHolidayModal: EditHolidayComponent;

 

  constructor(private HolidayServiceProxy: HolidayServiceProxy,private _router : Router ) {



  }

  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Holiday")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate([''])
    }

    this.getAllHoliday();

  }

  
  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  AddHoliday(): void {
    if (!this.globalFunction.hasPermission("Create", "Holiday")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
    this.addHolidayModal.show();
  }

  EditHoliday(id:string): void {
    if (!this.globalFunction.hasPermission("Edit", "Holiday")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    this.editHolidayModal.show(parseInt(id));
  }

  initDataTable() {

    this.dataTable = {
      headerRow: ['Cost Center', 'Department', 'Designation', 'Leave Type', 'Leave Start Date', 'Leave End Date ','Holiday Detail', 'Active', 'Actions'],
      footerRow: [ /*'Cost Center', 'Department', 'Designation', 'Leave Type', 'Leave Start Date', 'Leave End Date ', 'Active', 'Actions'*/],

      dataRows: []

    };

  }

  fillDataTable() {

    let i;
    for (i = 0; i < this.Holiday.length; i++) {

      this.data.push(this.Holiday[i].costCenter.name)
      this.data.push(this.Holiday[i].department.title)
      this.data.push(this.Holiday[i].employeeDesignation.employeeDesignationName)
      this.data.push(this.Holiday[i].leaveType.leaveTypeName)

      this.data.push(this.Holiday[i].holidayStartDate.toString())

      this.data.push(this.Holiday[i].holidayEndDate.toString())

      this.data.push(this.Holiday[i].holidayDetail)

      this.data.push(this.Holiday[i].isActive.toString())


      this.data.push(this.Holiday[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }





  getAllHoliday(): void {
    this.HolidayServiceProxy.getAllHolidays().subscribe((result) => {
      debugger;
      this.initDataTable()
      this.Holiday = result;
      this.fillDataTable();


    })




  }


  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Holiday")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.HolidayServiceProxy.delete(parseInt(id))
          .finally(() => {
            this.getAllHoliday();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Holiday has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Holiday is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }



}


