// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector, EventEmitter } from '@angular/core';
import { AddBonusComponent } from '../bonus/add-bonus/add-bonus.component';
import { EditBonusComponent } from '../bonus/edit-bonus/edit-bonus.component';
import { EmployeeServiceProxy, EmployeeDto } from '@app/shared/service-proxies/service-proxies';
import {Router} from '@angular/router'
import { parse } from 'url';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
// import { ROUTER_PROVIDERS } from '/app/node_modules/@angular/router/src/router_module';


declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'employee-list',
    templateUrl: 'employee-list.component.html',
    providers: [EmployeeServiceProxy]
})
export class EmployeeListComponent implements OnInit, AfterViewInit {
    public dataTable: DataTable;

    result: EmployeeDto[]=[];
    data: string[] = [];

    @Output() employeeEditEvent = new EventEmitter<number>();

    constructor(injector: Injector, 
      private _employeeService: EmployeeServiceProxy,
      private _router : Router
    ){}

    globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "EmployeeList")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate(['']);
      }
      this.getAllRecord();

      
        

    }


    getAllRecord(): void {
      debugger;
      this._employeeService.getAllEmployees()
      .finally(()=>{
          console.log('completed');
      })
      .subscribe((result:any)=>{
        debugger;
  this.result = result;
  this.paging();
  this.getAllData(); 
      });
    }

    paging():void{
      this.dataTable = {
        headerRow: [ 
        'Code',
        'name',
        'Father Name',
        'Gender',
        'Relegion',
        'DoB',
        'Maritial Status',
       ' CNIC',
       ' CNIC Expiry',
        'CNIC Place',
        'Qualification',
       ' Job Type',
        'Cost Center',
       ' landline number',
        'mailing address',
        'cellphone',
        'permanent address',
        'state',
        'region',
        'zone',
        'belt',
        'city',
        'emergency contact',
        ' passport number',
        'insurance number',
       ' email id',
        'bank name',
        'Iban',
        'Account Name',
        'Account Type',
        'NTN Number',
        'Account Number',
        'Graduity Date',
       ' EOBI number',
        'PESSI number',
        'SS Number',
        'Joining Date',
       ' Confirmation Date',
       ' Attendance Date',
       ' Contract Date',
        'Contract End Date',
        'Leaving Date',
        'Department',
        'Designation',
        'Shift Group',
        'Payrol Division',
        ' Details',
        'Sales Person',
        'SO Person',
        'Daily Wages',
        'Active',
        'Alternate employee code',
        'employee type',
        'relation',
        'ref employee',
        'family code',
        'ID mark' ,
        'domicile',
        'blood group',
        'referencee',
        'Actions'
      ],
        footerRow: [ /*'Doc No', 'Doc Date', 'Bonus Date', 'Employee', 'Bonus Type', 'Bonus Percentage', 'Actions'*/ ],

        dataRows: [
            // ['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Eid','10%', ''],
            // ['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Eid','10%', ''],
        ]
     };
    }

    getAllData():void{
      let i;
      for(i=0; i<this.result.length ; i++)
      {
        this.data.push(this.result[i].employee_Code);
        this.data.push(this.result[i].employee_Name);
        this.data.push(this.result[i].father_Name);
        this.data.push(this.result[i].gender);
        this.data.push(this.result[i].religion);
        this.data.push(this.result[i].dob.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].martial_Status);
        this.data.push(this.result[i].nic);
        this.data.push(this.result[i].cnicExpiryDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].niC_Place);
        this.data.push(this.result[i].qualification);
        this.data.push(this.result[i].jobType.jobTypeTitle);
        this.data.push(this.result[i].costCenter.name);
        this.data.push(this.result[i].phone);
        this.data.push(this.result[i].mailingAddress);
        this.data.push(this.result[i].mobile);
        this.data.push(this.result[i].address);
        this.data.push(this.result[i].state.state_Name);
        this.data.push(this.result[i].region.regionName);
        this.data.push(this.result[i].zone.zoneName);
        this.data.push(this.result[i].belt.belt_Name);
        this.data.push(this.result[i].city.cityName);
        this.data.push(this.result[i].emergency_No);
        this.data.push(this.result[i].passport_No);
        this.data.push(this.result[i].insurance_No);
        this.data.push(this.result[i].email);
        this.data.push(this.result[i].bank_Ac_Name);
        this.data.push(this.result[i].iban);
        this.data.push(this.result[i].accountName);
        this.data.push(this.result[i].accountType);
        this.data.push(this.result[i].ntn);
        this.data.push(this.result[i].bankAccount_No);
        this.data.push(this.result[i].graduityDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].eobiNo);
        this.data.push(this.result[i].pessiNo);
        this.data.push(this.result[i].social_Security_No);
        this.data.push(this.result[i].joiningDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].confirmationDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].attendanceDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].contractDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].contractEndingDate.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].leaving_date.format("YYYY-MMM-DD"));
        this.data.push(this.result[i].department.title);
        this.data.push(this.result[i].employeeDesignation.employeeDesignationName);
        this.data.push(this.result[i].shiftgroupId);
        this.data.push(this.result[i].payRoll_Division);
        this.data.push(this.result[i].anyDetail);
        this.data.push(this.result[i].salesPerson.toString() );
        this.data.push(this.result[i].sale_Order_Person.toString());
        this.data.push(this.result[i].isDailyWages.toString());
        this.data.push(this.result[i].isActive.toString());
        this.data.push(this.result[i].alternameEmpNo.toString());
        this.data.push(this.result[i].employeeTypeId.toString());
        this.data.push(this.result[i].relation);
        this.data.push(this.result[i].refEmployeeId.toString());
        this.data.push(this.result[i].family_Code);
        this.data.push(this.result[i].iD_Remark);
        this.data.push(this.result[i].domicile);
        this.data.push(this.result[i].blood_Group);
        this.data.push(this.result[i].reference);
        this.data.push(this.result[i].id.toString());
        //this below line will push all the record.
        this.dataTable.dataRows.push(this.data);
        this.data = [];
      }
  }

  addEmployee():void{
    if (!this.globalFunction.hasPermission("Create", "EmployeeList")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }

    this._router.navigate(['../hr/employeeWizard']);
  }

  editEmployee(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "EmployeeList")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }

    this._router.navigate([`../hr/employeeWizard/${parseInt(id)}`]);
  }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        //const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        // table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        //alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "EmployeeList")) {
        this.globalFunction.showNoRightsMessage("Delete");
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Fine!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
      if (result.value) {
        this._employeeService.delete(parseInt(id))
          .finally(() => {
             this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your employee has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
          }).catch(swal.noop)
           });
      } else {
        swal({
            title: 'Cancelled',
            text: 'Your employee is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

}


