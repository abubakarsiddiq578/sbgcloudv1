import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEditEmployeeComponent } from './search-edit-employee.component';

describe('SearchEditEmployeeComponent', () => {
  let component: SearchEditEmployeeComponent;
  let fixture: ComponentFixture<SearchEditEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchEditEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEditEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
