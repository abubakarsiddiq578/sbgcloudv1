import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { EmployeeDto, EmployeeServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
declare const $: any;

declare interface DataTable {
 headerRow: string[];
 footerRow: string[];
 dataRows: string[][];
}
@Component({
 selector: 'app-search-edit-employee',
 templateUrl: './search-edit-employee.component.html',
 styleUrls: ['./search-edit-employee.component.scss']
})
export class SearchEditEmployeeComponent implements OnInit {

 @ViewChild('searchEditEmployeeDropdownModal') modal: ModalDirective;
 @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

 active: boolean = false;
 saving: boolean = false;


 public dataTable: DataTable;
   data: string[] = [];

   result: EmployeeDto[] = [];

 constructor(private employeeService:EmployeeServiceProxy ,
   private _router : Router) { }

 ngOnInit() {
   this.getAllRecord()
 }
 getAllRecord(): void {
   debugger;
   this.employeeService.getAllEmployees()
     .finally(() => {
       console.log('completed');
     })
     .subscribe((result: any) => {
       this.paging();
       this.result = result;
       this.getAllData();

     });
 }
 getAllData(): void {
   let i;
   for (i = 0; i < this.result.length; i++) {
     this.data.push(this.result[i].employee_Code);
     this.data.push(this.result[i].refEmployeeId.toString());
     this.data.push(this.result[i].employee_Name);


     this.data.push(this.result[i].department.code);




     this.data.push(this.result[i].employeeDesignation.employeeDesignationName);

     this.data.push(this.result[i].state.state_Name);





     this.data.push(this.result[i].id.toString());




     this.dataTable.dataRows.push(this.data);
     this.data = [];
   }
 }
 paging(): void {
   this.dataTable = {
     headerRow: [ 'Code', 'Reference Id','Employee Name',  'Department Code', 'Designation', 'State'  ],
       footerRow: [ 'Code','Reference Id', 'Employee Name',  'Department Code', 'Designation', 'State' ],
       dataRows: [

     ]

 };
}
ngAfterViewInit() {
 $('#datatablesss').DataTable({
   "pagingType": "full_numbers",
   "lengthMenu": [
     [10, 25, 50, -1],
     [10, 25, 50, "All"]
   ],
   responsive: true,
   language: {
     search: "_INPUT_",
     searchPlaceholder: "Search records",
   }

 });

 const table = $('#datatablesss').DataTable();

 $('.card .material-datatables label').addClass('form-group');
}

 show(){
   this.active = true;
   this.modal.show();

 }
 close(): void {
   this.active = false;
   this.modal.hide();
}
//smart Search
@Input()
Ivalue: any

//smart search
Data(EmployeeId : any) : void{
 debugger
this.Ivalue = EmployeeId
this.active =  true
this.close();
this.modalSave.emit(this.Ivalue);

}

}