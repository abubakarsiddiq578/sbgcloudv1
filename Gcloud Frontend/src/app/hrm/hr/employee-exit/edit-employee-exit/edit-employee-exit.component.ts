import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { EmployeeExitComponent } from '../employee-exit.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { EmployeeExitDto , EmployeeExitServiceProxy , EmployeeDto , EmployeeServiceProxy, HRConfigurationServiceProxy, HRConfigurationDto} from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-employee-exit',
  templateUrl: './edit-employee-exit.component.html',
  styleUrls : ['./edit-employee-exit.component.less'],
  providers: [EmployeeExitServiceProxy , EmployeeServiceProxy , HRConfigurationServiceProxy]
})
export class EditEmployeeExitComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editEmployeeExitModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;


    employeeExit : EmployeeExitDto = new EmployeeExitDto();

    employee : EmployeeDto[];

    EmployeeExitDate : Date  = new Date();
    documentDate : Date = new Date();
    _empExt : FormGroup; // for Validations

    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName: string;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       
        private employeeExitService : EmployeeExitServiceProxy,
        private  employeeService : EmployeeServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder :FormBuilder
    ) {
        //super(injector);
    }
  

    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }
    

        });

    }

    show(id: number): void{

        this.employeeExitService.get(id)
          .finally(() => {
            this.active = true;
            this.modal.show();
    
          })
          .subscribe((result : EmployeeExitDto )=>{

            this.employeeExit = result;
        
            this.documentDate = this.employeeExit.documentDate.toDate();
            this.EmployeeExitDate = this.employeeExit.exit_Date.toDate();
            
            this.employeeService.get(result.employeeId).subscribe((result)=>{

                this.empName = result.employee_Name;
            });
        
        });

        this.employeeDropdownSearch();
        this._empExt.markAsUntouched({onlySelf:true});
        
    }
   ///////////////////Start of Validation Code////////////////////////////
    initValidation() {

        this._empExt = this.formBuilder.group({
            employeeId: [null, Validators.required],
            employeeExitDate: [null , Validators.required]
        });
    
    }
    
  /*  onType() {
    
      if (this._empExt.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._empExt);
      }
    }*/

    onType() {
        debugger;
        if (this._empExt.valid) {
            debugger;
            let a, p = 0;
            for (a = 0; a < this.employee.length; a++) {
                if (this.employeeExit.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._empExt);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._empExt);

        }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
  /////////////////////////////End of Validation Code//////////////////////////////
    ///////////////////////////////search Method//////////////////////////////////
    public employeeDropdownSearch() {
        debugger;
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith<string | EmployeeDto>(''),
                map(value => typeof value === 'string' ? value : value.employee_Name),
                map(name => name ? this._filter(name) : this.employee.slice())
            );

    }

    private _filter(value: string): EmployeeDto[] {

        debugger;
        const filterValue = value.toLowerCase();

        return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
    }


    displayEmployee = (emp: EmployeeDto): any => {
        debugger;

        if (emp instanceof EmployeeDto) {
            this.employeeExit.employeeId = emp.id;
            return emp.employee_Name;
        }
        this.employeeExit.employeeId = null;
        return emp;

    }

    ////////////////////////////////End of Search Method/////////////////////////////////////


    ngOnInit(): void {

        this.employeeService.getAllEmployees()
        .subscribe((result) => {
          this.employee = result;
         // console.log("this is employee: ", this.employee);
         
        })
        this.getAllHRConfiguration();
  

    }

    save(): void {

        this.employeeExit.documentDate = moment(this.documentDate).add(5,'hour') ;
        
        this.employeeExit.exit_Date = moment(this.EmployeeExitDate).add(5,'hour');
        this.saving = true;
      
        this.employeeExitService.update(this.employeeExit)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
           
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
      }





    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    notify() {
        swal({
          title: "Success!",
          text: "Saved Successfully.",
          timer: 2000,
          showConfirmButton: false
        }).catch(swal.noop)
      }
    


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

