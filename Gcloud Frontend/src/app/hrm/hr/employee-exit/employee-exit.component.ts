// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddEmployeeExitComponent } from './add-employee-exit/add-employee-exit.component';
import { EditEmployeeExitComponent } from './edit-employee-exit/edit-employee-exit.component';
import { EmployeeExitDto , EmployeeExitServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';


declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'employee-exit',
    templateUrl: 'employee-exit.component.html',
    providers:[EmployeeExitServiceProxy]
})

export class EmployeeExitComponent implements OnInit, AfterViewInit {
    public dataTable: DataTable;
    data : string[] = [];

    employeeExit : EmployeeExitDto[];

    @ViewChild('addEmployeeExitModal') addEmployeeExitModal: AddEmployeeExitComponent;
    @ViewChild('editEmployeeExitModal') editEmployeeExitModal: EditEmployeeExitComponent;


    constructor( private employeeExitService : EmployeeExitServiceProxy,
      private _router : Router){


    }

    globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "EmployeeExit")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate(['']);
      }

      this.getAllEmployeeExit();
      
    }


    getAllEmployeeExit(){



      this.employeeExitService.getAllEmployeeExits().subscribe((result)=>{

          this.initializeDatatable();
          this.employeeExit = result;
          this.fillDatatable();
      });

    }



    initializeDatatable():void{

      this.dataTable = {
        headerRow: [ 'Doc No', 'Doc Date', 'Employee', 'Exit Type', 'Exit Date', 'Details', 'Actions' ],
        footerRow: [ /*'Doc No', 'Doc Date', 'Employee', 'Exit Type', 'Exit Date', 'Actions'*/ ],

        dataRows: []
     };


    }

    fillDatatable():void{

     
      let i;
      for (i = 0; i < this.employeeExit.length; i++) {
  
       this.data.push(this.employeeExit[i].documentNo)
        
        this.data.push(this.employeeExit[i].documentDate.toString())
        
          
        this.data.push(this.employeeExit[i].employee.employee_Name)

        
        this.data.push(this.employeeExit[i].exit_Type)

        
        this.data.push(this.employeeExit[i].exit_Date.toString())
        
        
        
        this.data.push(this.employeeExit[i].details)
        
        
        this.data.push(this.employeeExit[i].id.toString())
        
  
        
  
        this.dataTable.dataRows.push(this.data)
  
        this.data = [];
  
      }
  

    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        //const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        ;//table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    AddEmployeeExit(): void {
      if (!this.globalFunction.hasPermission("Create", "EmployeeExit")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addEmployeeExitModal.show();
      }

      EditEmployeeExit(id: string): void {
        if (!this.globalFunction.hasPermission("Edit", "EmployeeExit")) {
          this.globalFunction.showNoRightsMessage("Edit");
          return
        }
        this.editEmployeeExitModal.show(parseInt(id));
        }

        protected delete(id: string): void {
          if (!this.globalFunction.hasPermission("Delete", "EmployeeExit")) {
            this.globalFunction.showNoRightsMessage("Delete");
            return
          }
          swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this Request!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it',
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: false
          }).then((result) => {
            if (result.value) {
              this.employeeExitService.delete(parseInt(id))
                .finally(() => {
                  this.getAllEmployeeExit();
                })
                .subscribe(() => {
                  debugger
                  swal({
                    title: 'Deleted!',
                    text: 'Employee Exit has been deleted.',
                    type: 'success',
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                  }).catch(swal.noop)
                });
            } else {
              swal({
                title: 'Cancelled',
                text: 'Employee Exit Deletion is safe :)',
                type: 'error',
                confirmButtonClass: "btn btn-info",
                buttonsStyling: false
              }).catch(swal.noop)
            }
          })
        }

}


