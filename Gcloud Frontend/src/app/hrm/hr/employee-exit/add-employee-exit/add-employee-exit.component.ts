import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { EmployeeExitComponent } from '../employee-exit.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { EmployeeExitDto , EmployeeExitServiceProxy , EmployeeDto , EmployeeServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy} from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import * as moment from 'moment';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'add-employee-exit',
  templateUrl: './add-employee-exit.component.html',
  styleUrls : ['./add-employee-exit.component.less'],
  providers: [EmployeeExitServiceProxy , EmployeeServiceProxy , HRConfigurationServiceProxy]
})
export class AddEmployeeExitComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addEmployeeExitModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    
     EmployeeExitDate : Date  = new Date();

     documentDate : Date = new Date();
          
    employeeExit : EmployeeExitDto = new EmployeeExitDto();

    employee : EmployeeDto[];

    _employExit : EmployeeExitDto[]; 

    _abc: abc[] = [];

    _empExt : FormGroup; // for Validations

    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName: string;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,

        private employeeExitService : EmployeeExitServiceProxy,
        private  employeeService : EmployeeServiceProxy,
        private formBuilder : FormBuilder,
        private _hrConfigService : HRConfigurationServiceProxy
        // private _userService: UserServiceProxy
       
    ) {
        //super(injector);
    }
  /////////////////////Start of Validation Code/////////////////////////
    initValidation() {

        this._empExt = this.formBuilder.group({
            employeeId: [null, Validators.required],
            employeeExitDate: [null , Validators.required]
        });
    
    }
    
  /*  onType() {
    
      if (this._empExt.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._empExt);
      }
    }*/

    onType() {
        debugger;
        if (this._empExt.valid) {
            debugger;
            let a, p = 0;
            for (a = 0; a < this.employee.length; a++) {
                if (this.employeeExit.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._empExt);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._empExt);

        }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
  /////////////////////////////End of Validation Code//////////////////////////////
    ///////////////////////////////search Method//////////////////////////////////
    public employeeDropdownSearch() {
        debugger;
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith<string | EmployeeDto>(''),
                map(value => typeof value === 'string' ? value : value.employee_Name),
                map(name => name ? this._filter(name) : this.employee.slice())
            );

    }

    private _filter(value: string): EmployeeDto[] {

        debugger;
        const filterValue = value.toLowerCase();

        return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
    }


    displayEmployee = (emp: EmployeeDto): any => {
        debugger;

        if (emp instanceof EmployeeDto) {
            this.employeeExit.employeeId = emp.id;
            return emp.employee_Name;
        }
        this.employeeExit.employeeId = null;
        return emp;

    }

    ////////////////////////////////End of Search Method/////////////////////////////////////

    show(): void{

        this.active = true;
        this.modal.show();
        this.employeeExit = new EmployeeExitDto();
        //this._employExit = new _employExit();
        this.employeeExitService.getAllEmployeeExits().subscribe((result )=>{

            this._employExit = result;
            //this.getAutoDocNumber();
            if(this.hrConfig.autoCode == true){
                debugger;
                this.getAutoDocNumber();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }

        })
        this.empName = null;
        this.employeeDropdownSearch();
        this._empExt.markAsUntouched({onlySelf:true});
    }



    
    ngOnInit(): void {

        this.employeeService.getAllEmployees().subscribe(( result)=>{

            this.employee = result;

        })
        this.initValidation();
       /* this.employeeExitService.getAllEmployeeExits().subscribe((result )=>{


            this._employExit = result;
            this.getAutoDocNumber();
            debugger;
            this.docNo = this.employeeExit.documentNo;
        })

      */  
        this.getAllHRConfiguration();        
    }



    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

        });

    }

    // adding auto document No functionality  
    /*getAutoDocNumber():void{

        debugger;
        let i =0;
       
        for(i=0 ;i< this._employExit.length ; i++){

            let abc_ ; 

            abc_ = new abc();

            abc_.code = this._employExit[i].documentNo ; 

            this._abc.push(abc_);

        }


        let code : any ;

        if(this._abc.length == 0 ){
            debugger;
            code = "1";
            this.employeeExit.documentNo = "00" + code ;

        }

        else{

            if(this._abc[this._abc.length - 1] != null ){

                    code = this._abc[this._abc.length-1].code ;
                    code ++;
                    if(code <=9){

                        this.employeeExit.documentNo = "00" + code ;
                    }
                    else if(code <=99){

                        this.employeeExit.documentNo = "0" + code ;
                    }
                    else if(code <=999){

                        this.employeeExit.documentNo =  code ;
                    }
            }                     
        }
        this._abc = [];
    }*/


         //this function generate auto doc number
         getAutoDocNumber():void{

            debugger;
            let i =0;
            
            let temp : any[];
           
            for(i=0 ;i< this._employExit.length ; i++){
        
                let abc_ ; 
        
                abc_ = new abc();
        
                abc_.code = this._employExit[i].documentNo ; 
        
                this._abc.push(abc_);
        
            }
        
        
            let code : any ;
        
            if(this._abc.length == 0  ){
              
                debugger;
                code = "1";
                this.employeeExit.documentNo = "00" + code ;
        
            }
        
            else{
        
                if(this._abc[this._abc.length - 1] != null ){
        
                        let x;
                        code = this._abc[this._abc.length-1].code ;
                        if(code!=null){
                          temp = code.split("-");
                           x = parseInt(temp[0]);
                           x = x.toString();
                        }
                        debugger;
                        //let j = parseInt(code);
                        //
                        if(temp.length == 1 && x=="NaN"  ){
        
                          debugger;
                          temp[1] = 0;
                          temp[1] ++;
        
                         
                          if(temp[1] <=9){
          
                              this.employeeExit.documentNo = temp[0] + "-00" + temp[1] ;
                          }
                          else if(temp[1] <=99){
          
                              this.employeeExit.documentNo =  temp[0] + "-0" + temp[1] ;
                          }
                          else if(temp[1]){
          
                              this.employeeExit.documentNo =  temp[0] + "-" +temp[1] ;
                          }
        
                        
                        }
        
                        else if (temp.length == 2) {
        
                          temp[1] ++;
                          if(temp[1] <=9){
          
                              this.employeeExit.documentNo = temp[0] + "-00" + temp[1] ;
                          }
                          else if(temp[1] <=99){
          
                              this.employeeExit.documentNo =  temp[0] + "-0" + temp[1] ;
                          }
                          else if(temp[1]){
          
                              this.employeeExit.documentNo =  temp[0] + "-" +temp[1] ;
                          }
        
                        }
        
                        else if(temp.length == 0){
        
                          code ++;
                          if(code <=9){
          
                              this.employeeExit.documentNo = "00" + code ;
                          }
                          else if(code<=99){
          
                              this.employeeExit.documentNo =  "0" + code;
                          }
                          else if(code){
          
                              this.employeeExit.documentNo =  code ;
                          }
        
        
                        } 
                        else if(temp.length==1 && x!="NaN" ){
        
                          temp[0]++;
                          if(temp[0] <=9){
          
                              this.employeeExit.documentNo = "00" + temp[0] ;
                          }
                          else if(code<=99){
          
                              this.employeeExit.documentNo =  "0" + temp[0];
                          }
                          else if(code){
          
                              this.employeeExit.documentNo =  temp[0] ;
                          }
        
                        }
        
        
        
                        
                }                     
            }
            this._abc = [];
        }
        




    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {


        debugger;


    
        this.employeeExit.documentDate = moment(this.documentDate).add(5,'hour');

        this.employeeExit.exit_Date = moment(this.EmployeeExitDate).add(5,'hour');

        this.saving = true;
    
       // this.employeeExit.documentNo = this.docNo;
        

        this.employeeExitService.create(this.employeeExit)
          .finally(() => { this.saving = false; })
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
        
          });
      }
    
      notify() {
        swal({
          title: "Success!",
          text: "Saved Successfully.",
          timer: 2000,
          showConfirmButton: false
        }).catch(swal.noop)
      }
    
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }


}

// class for keeping all docNo number
 class abc{
    public code: string;
  }

