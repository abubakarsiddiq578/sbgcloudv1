// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Inject, ElementRef } from '@angular/core';
import { EditSalaryItemComponent } from '../salary/edit-salary-item/edit-salary-item.component';
import { SalaryGroupMasterServiceProxy, SalaryGroupMasterDto, EmployeeDropdownDto, EmployeeDropDownServiceProxy, SalaryMasterServiceProxy, SalaryMasterDto, SalaryDetailServiceProxy, SalaryDetailDto, EmployeeDto, EmployeeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import swal from 'sweetalert2';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { SearchEmployeeComponent } from '../promotion/search-employee/search-employee.component';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'salary',
  templateUrl: 'salary.component.html',
  providers: [SalaryGroupMasterServiceProxy, SalaryMasterServiceProxy, SalaryDetailServiceProxy, EmployeeDropDownServiceProxy, EmployeeServiceProxy,]
})

export class SalaryComponent implements OnInit, AfterViewInit {
  public dataTable: DataTable;
  public dataTable1: DataTable;

  @ViewChild('editSalaryItemModal') editSalaryItemModal: EditSalaryItemComponent;
  @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
  employeeDropDown: EmployeeDropdownDto[] = null;
  salaryGroupDropDown: SalaryGroupMasterDto[] = null;

  salaryMaster: SalaryMasterDto = new SalaryMasterDto();

  salaryDetail: SalaryDetailDto = new SalaryDetailDto();
  salaryDetailList = [];
  salaryGroupDetailList = [];

  employee: EmployeeDto[] = [];
  EmpName: string;
  data: string[] = [];

  salaryMasterDto: SalaryMasterDto[] = [];

  isDetail: boolean = true;
  isHistory: boolean = false;

  isSaveBtn: boolean = true;
  isEditBtn: boolean = false;

  detailId: number = 0;

  detailRecordId: number = 0;

  detailRecordLength: number = 0;

  isEditMode: boolean = false;

  myControl = new FormControl();
  filteredEmployees: Observable<EmployeeDropdownDto[]>;
  filteredOptions: Observable<EmployeeDropdownDto[]>;

  constructor(
    private employeeDropDownServiceProxy: EmployeeDropDownServiceProxy,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private salaryMasterServiceProxy: SalaryMasterServiceProxy,
    private salaryGroupMasterServiceProxy: SalaryGroupMasterServiceProxy,
    private _router : Router,
    private employeeService: EmployeeServiceProxy
  ) { }

  globalFunction: GlobalFunctions = new GlobalFunctions

  // EmployeeId(id): void {
  //   debugger
 
  //   for(let i = 0; i< this.employee.length; i++)
  //   {
  //     if(this.employee[i].id == id)
  //     {
  //       this.EmpName = this.employee[i].employee_Name;
  //       this.salaryMaster.employeeId = this.employee[i].id;
  //     }
  //   } 
  // }
  //Item
 
  // displayItem = (emp: EmployeeDto): any => {
  //   debugger;
 
  //   if (emp instanceof EmployeeDto) {
  //       this.salaryMaster.employeeId = emp.id;
  //     return emp.employee_Name;
  //   }
  //   return emp;
 
  // }



  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Salary")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate([''])
    }

    debugger;
    
    this.employeeService.getAllEmployees()
   .subscribe((result) => {
       this.employee = result;

   })
    
    //this.fillEmployeeDropDown();
    this.fillSalaryGroupDropDown();



    // reset and initialize detailDataTable Grid
    this.initDetailDataTable();

    // get all salary record in masterDataTable Grid
    this.getAllSalaries();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('#datatables1').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table1 = $('#datatables1').DataTable();

    // Edit record
    table1.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table1.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table1.on('click', '.like', function (e) {
      //alert('You clicked on Like button');
      e.preventDefault();
    });


    $('.card .material-datatables label').addClass('form-group');
  }


  EmployeeId(id): void {
    debugger
 
    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;
        this.salaryMaster.employeeId = this.employee[i].id;
 
      }
    }
 
 
 
  }






//   fillEmployeeDropDown() {
//     debugger;
//     this.employeeService.getAllEmployees()
//       .subscribe((result) => {
//         this.employee = result;

//         this.filteredEmployees = this.myControl.valueChanges
//         .pipe(
//         startWith<string | EmployeeDto>(''),
//         map(value => typeof value === 'string' ? value : value.employee_Name),
//         map(name => name ? this._filter(name) : this.employee.slice())
//       );

//       });
//   }

  
//   displayEmployee(employee): string | undefined {
//     debugger
//     return employee ? employee.employee_Name : undefined;
//   }

//   private _filter(name: string): EmployeeDto[] {
//     const filterValue = name.toLowerCase();

//     return this.employee.filter(option => option.employee_Name.toLowerCase().indexOf(filterValue) === 0);
// }

///

addSearchDD(){
  this.searchEmployeeDropdownModel.show()
}

// public employeeDropdownSearch() {

//   this.filteredOptions = this.myControl.valueChanges
//   .pipe(
//     startWith<string | EmployeeDto >(''),
//     map(value => typeof value === 'string' ? value : value.employee_Name),
//     map(name => name ? this._filter(name) : this.employee.slice())
//   );
// }

// private _filter(value: string): EmployeeDto[]{

//   debugger;
//   const filterValue = value.toLowerCase();

//   return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
// }


// displayEmployee = (emp: EmployeeDto):any => {
//   debugger;

//   if(emp instanceof EmployeeDto)
//   {
//       this.salaryMaster.employeeId = emp.id ;
//       return emp.employee_Name ;
//   }
//   this.salaryMaster.employeeId = null;
//   return emp;

// }
////





  fillSalaryGroupDropDown() {
    this.salaryGroupMasterServiceProxy.getAllSalaryGroup()
      .subscribe((result) => {
        this.salaryGroupDropDown = result;
      });
  }

  onChange(salaryGroupId) {
    this.getSalaryGroupDetail(salaryGroupId);
  }

  // get detail record of particular master of salary group selection mode
  getSalaryGroupDetail(id: number) {
    this.salaryGroupMasterServiceProxy.getAllDetailById(id)
      .subscribe((result) => {
        this.salaryDetailList = [];
        this.salaryGroupDetailList = [];
        this.salaryGroupDetailList = result;
        this.detailRecordLength = this.salaryGroupDetailList.length;
        this.initDetailDataTable();
        this.fillDetailDataTable();
      });
  }

  // reset and initialize detailDataTable Grid
  initDetailDataTable() {
    this.dataTable = {
      headerRow: ['Salary Item', 'Amount', 'Action'],
      footerRow: ['Salary Item', 'Amount', 'Action'],

      dataRows: [
        //['Basic Salary', '30000', ''],
        //['Allowance', '3000', '']
      ]
    };
  }

  // fill datatable to show detail record in datatable grid in eidt mode
  fillDetailDataTable() {
    let i;

    for (i = 0; i < this.salaryGroupDetailList.length; i++) {

      // create new object of salaryDetail to add another value in the grid
      this.salaryDetail = new SalaryDetailDto();

      if (this.storage.get('TenantId') > 0) {
        this.salaryDetail.tenantId = this.storage.get('TenantId');
      } 

      this.salaryDetail.salaryItem = this.salaryGroupDetailList[i].salaryType;
      this.salaryDetail.amount = 0;
      this.salaryDetail.id = this.salaryGroupDetailList[i].id;

      // push salaryDetail in salaryDetailList
      this.salaryDetailList.push(this.salaryDetail);

    }

    // to fill slary detail list
    this.fillSalaryDetailList();
  }

  // to fill slary detail list
  fillSalaryDetailList() {
    for (let i = 0; i < this.salaryDetailList.length; i++) {
      this.data.push(this.salaryDetailList[i].salaryItem);
      this.data.push(this.salaryDetailList[i].amount);
      this.data.push(this.salaryDetailList[i].id.toString());

      this.dataTable.dataRows.push(this.data)

      this.data = [];
    }
  }

  // get all salaries records
  getAllSalaries(): void {
    this.salaryMasterServiceProxy.getAllSalaries()
      .subscribe((result) => {
        this.initMasterDataTable();
        this.salaryMasterDto = result
        this.fillMasterDataTable();
      });
  }

  // reset and initialize MasterDataTable Grid
  initMasterDataTable() {
    this.dataTable1 = {
      headerRow: ['Employee Name', 'Annual Gross Salary', 'Salary Group', 'Action'],
      footerRow: ['Employee Name', 'Annual Gross Salary', 'Salary Group', 'Action'],

      dataRows: [
        //['Saad Afzaal', '30000', 'Development Department', ''],
        //['Nabeel Rana', '3000', 'Support Departmrnt', '']
      ]
    };
  }

  // fill datatable to show master record in datatable grid
  fillMasterDataTable() {
    let i = 0;

    for (i = 0; i < this.salaryMasterDto.length; i++) {
      this.data.push(this.salaryMasterDto[i].employee.employee_Name);
      this.data.push(this.salaryMasterDto[i].annualGrossSalary.toString());
      this.data.push(this.salaryMasterDto[i].salaryGroupMaster.salaryGroupTitle);
      this.data.push(this.salaryMasterDto[i].id.toString());


      this.dataTable1.dataRows.push(this.data);

      this.data = [];
    }
  }

  // perform save opertaion to save data
  save(): void {
    // set tenant id that get from session  
    if (this.storage.get('TenantId') > 0) {
      this.salaryMaster.tenantId = this.storage.get('TenantId');
    }

    // set master id undefined to save record
    this.salaryMaster.id = undefined;

    // set detail id undefined to save record
    for (let i = 0; i < this.salaryDetailList.length; i++) {
      this.salaryDetailList[i].id = undefined;
    }

    this.salaryMaster.salaryDetails = this.salaryDetailList;

    this.salaryMasterServiceProxy.add(this.salaryMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Saved Successfully');

        // perform reset operation after saving record

        this.initDetailDataTable();
        this.getAllSalaries();

        this.salaryGroupDetailList = [];
        this.salaryDetailList = [];

        this.salaryMaster.employeeId = null;
        this.salaryMaster.annualGrossSalary = null;
        this.salaryMaster.salaryGroupMasterId = null;

        this.detailRecordId = 0;

        this.detailRecordLength = 0;
      });
  }

  notifyMsg(title, msg) {
    swal({
      title: title,
      text: msg,
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  // delete record of salary master 
  deleteSalaryMasterDetail(id: string) {
    if (!this.globalFunction.hasPermission("Delete", "Salary")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Salary!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.salaryMasterServiceProxy.delete(parseInt(id))
          .finally(() => {

            this.getAllSalaries();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Salary has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Salary is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

  // get detail record of particular master in edit mode
  EditSalaryMasterDetail(id: string) {
    if (!this.globalFunction.hasPermission("Edit", "Salary")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    debugger;
    this.detailId = parseInt(id);

    this.salaryMasterServiceProxy.getAllDetailById(parseInt(id))
      .subscribe((result) => {

        this.salaryDetailList = [];
        this.salaryGroupDetailList = [];
        this.salaryDetailList = result;

        this.salaryMasterServiceProxy.get(parseInt(id))
          .finally(() => {
          })
          .subscribe((result) => {

            this.salaryMaster.employeeId = result.employeeId;
            this.salaryMaster.annualGrossSalary = result.annualGrossSalary;
            this.salaryMaster.salaryGroupMasterId = result.salaryGroupMasterId;

            this.isEditMode = true;

            this.detailRecordLength = this.salaryDetailList.length;
            this.initDetailDataTable();
            this.fillSalaryDetailList();
            this.ActiveUpdateBtn();
          });
      });

    this.ActiveDetail();
  }

  // delete data of salary detail record from detail datatable 
  DeleteSalaryItem(id: string) {
    if (!this.globalFunction.hasPermission("Delete", "Salary")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    let salaryDetail;

    // get record of matching id
    salaryDetail = this.salaryDetailList.find(function (obj) { return obj.id === parseInt(id); });

    // perform delete operation to delete object from detail list 
    const index: number = this.salaryDetailList.indexOf(salaryDetail);
    if (index !== -1) {
      this.salaryDetailList.splice(index, 1);

      this.initDetailDataTable();
      this.fillSalaryDetailList();
    }
  }

  // perform edit operation to edit data 
  edit(): void {
    if (!this.globalFunction.hasPermission("Edit", "Salary")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    if (this.storage.get('TenantId') > 0) {
      this.salaryMaster.tenantId = this.storage.get('TenantId');
    }

    this.salaryMaster.id = this.detailId;

    if (this.isEditMode == true) {
      // set detail id undefined to save record
      for (let i = 0; i < this.salaryDetailList.length; i++) {
        this.salaryDetailList[i].id = undefined;
      }
    } else {
      for (let i = this.detailRecordLength; i < this.salaryDetailList.length; i++) {
        this.salaryDetailList[i].id = undefined;
      }
    }

    this.salaryMaster.salaryDetails = this.salaryDetailList;

    this.salaryMasterServiceProxy.edit(this.salaryMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Updated Successfully');

        this.initDetailDataTable();
        this.getAllSalaries();

        this.salaryGroupDetailList = [];
        this.salaryDetailList = [];

        this.salaryMaster.employeeId = null;
        this.salaryMaster.annualGrossSalary = null;
        this.salaryMaster.salaryGroupMasterId = null;

        this.detailRecordId = 0;

        this.detailRecordLength = 0;

        this.isEditMode = false;
      });
  }


  EditSalaryItem(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Salary")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    let salaryDetail;
    let totalAmount = 0;
    let annualGrossSalary = 0;

    salaryDetail = this.salaryDetailList.find(function (obj) { return obj.id === parseInt(id); });

    for(let i=0; i<this.salaryDetailList.length; i++){
      totalAmount += this.salaryDetailList[i].amount;
    }

    if(this.salaryMaster.annualGrossSalary > 0){
      annualGrossSalary = this.salaryMaster.annualGrossSalary
    }

    this.editSalaryItemModal.show(salaryDetail , totalAmount , annualGrossSalary);
  }

  // incase when edit operation is apply to edit data of detail datatable record
  updateSalaryDetail(salaryDetail) {
    this.initDetailDataTable();
    this.fillSalaryDetailList();
  }

  // show detail tab 
  ActiveDetail() {
    this.isDetail = true;
    this.isHistory = false;
  }

  // show history tab 
  ActiveHistory() {
    this.isDetail = false;
    this.isHistory = true;
  }

  // show save btn
  ActiveSaveBtn() {
    this.isSaveBtn = true;
    this.isEditBtn = false;
  }

  // show update btn
  ActiveUpdateBtn() {
    this.isSaveBtn = false;
    this.isEditBtn = true;
  }

  // reset value of master detail input and detail datatable grid record
  reset() {
    this.salaryMaster.employeeId = null;
    this.salaryMaster.annualGrossSalary = null;
    this.salaryMaster.salaryGroupMasterId = null;
    this.detailId = 0;

    this.salaryGroupDetailList = [];
    this.salaryDetailList = [];

    this.detailRecordId = 0;

    this.detailRecordLength = 0;

    this.isEditMode = false;

    this.initDetailDataTable();
    this.ActiveSaveBtn();
  }

}


