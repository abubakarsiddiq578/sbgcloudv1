import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
//import { SalaryComponent } from '../salary.component';

import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import swal from 'sweetalert2';


@Component({
    selector: 'edit-salary-item',
    templateUrl: './edit-salary-item.component.html'
})
export class EditSalaryItemComponent implements OnInit {

    @ViewChild('editSalaryItemModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    isSuccess: boolean = false;

    editData: any;
    totalAmount: number;
    amount: number;
    annualGrossSalary: number;

    constructor(
        injector: Injector,

    ) {
    }


    show(editData, totalAmount , annualGrossSalary): void {

        this.active = true;
        this.modal.show();
        this.editData = editData;
        this.totalAmount = totalAmount;
        this.amount = this.editData.amount;
        this.annualGrossSalary = annualGrossSalary
    }

    doCalculation() {
        
    }

    notifyMsg(title, msg) {
        swal({
            title: title,
            text: msg,
            buttonsStyling: false,
            confirmButtonClass: "btn btn-info"
        }).catch(swal.noop)
    }

    ngOnInit(): void {
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save() {

        let changeTotalAmount;
        let editAmount;

        changeTotalAmount = (this.totalAmount - this.amount) + this.editData.amount

        if (changeTotalAmount > this.annualGrossSalary) {
            editAmount = this.editData.amount - (changeTotalAmount - this.annualGrossSalary)
            this.notifyMsg("Warning!", "Annual Gross Salary must be equal and less then sum of all salary items amount. Maximun amount you can enter : " + editAmount);
        }
        else{
            this.isSuccess = true;
            this.close();
            this.modalSave.emit(this.editData);
        }
    }

    close(): void {

        if(this.isSuccess == true){
            this.isSuccess = false;
        }
        else{
            this.editData.amount = this.amount;
        }

        this.active = false;
        this.modal.hide();
    }
}

