// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, OnChanges, AfterViewInit, SimpleChanges, Injector, ViewChild } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormBuilder } from '@angular/forms';
import { JobTypeServiceProxy, EmployeeDesignationServiceProxy, DepartmentServiceProxy, EmployeeServiceProxy, EmployeeDto, JobTypeDto, DepartmentDto, EmployeeDesignationDto, CostCenterDto, CostCenterServiceProxy, StateDto, CountryDto, ZoneDto, RegionDto, BeltDto, StateServiceProxy, ZoneServiceProxy, CountryServiceProxy, BeltServiceProxy, RegionServiceProxy, CityServiceProxy, CityDto, HRConfigurationDto, HRConfigurationServiceProxy, ProvinceServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { subscribeTo } from '../../../../../node_modules/rxjs/internal-compatibility';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { debug } from 'util';
import { Http } from '@angular/http';
//import { moment } from '../../../../../node_modules/ngx-bootstrap/chronos/test/chain';
import * as moment from 'moment';
import { forkJoin, Observable } from 'rxjs';
import 'rxjs/add/observable/forkJoin';





declare const $: any;
interface FileReaderEventTarget extends EventTarget {
    result: string;
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'employee-wizard',
    templateUrl: 'employee-wizard.component.html',
    styleUrls: ['employee-wizard.component.css'],
    providers: [EmployeeServiceProxy,
        JobTypeServiceProxy,
        DepartmentServiceProxy,
        EmployeeDesignationServiceProxy,
        CostCenterServiceProxy,
        StateServiceProxy,
        CountryServiceProxy,
        ZoneServiceProxy,
        BeltServiceProxy,
        RegionServiceProxy,
        CityServiceProxy,
        HRConfigurationServiceProxy,
        ProvinceServiceProxy
    ]
})

export class EmployeeWizardComponent implements OnInit, OnChanges, AfterViewInit {

    active: boolean = false;
    saving: boolean = false;

    employeeRecord: EmployeeDto = new EmployeeDto();
    refEmployeeDDlist: EmployeeDto[] = [];
    jobTypeDDList: JobTypeDto[] = [];
    departmentDDList: DepartmentDto[] = [];
    designationDDList: EmployeeDesignationDto[] = [];
    costCenterDDList: CostCenterDto[] = [];
    stateDDList: StateDto[] = [];
    cityDDList: CityDto[] = [];
    zoneDDList: ZoneDto[]=[];
    regionDDList: RegionDto[] = [];
    beltDDList: BeltDto[] = [];

    pid : number;
    //////////

    _employee: EmployeeDto[] = [];

    _empCode : abc[] = []; 


    ////////////

    _employeeWizardValidation : FormGroup


    empId: number;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;



    //   cities = [
    //     {value: 'paris-0', viewValue: 'Paris'},
    //     {value: 'miami-1', viewValue: 'Miami'},
    //     {value: 'bucharest-2', viewValue: 'Bucharest'},
    //     {value: 'new-york-3', viewValue: 'New York'},
    //     {value: 'london-4', viewValue: 'London'},
    //     {value: 'barcelona-5', viewValue: 'Barcelona'},
    //     {value: 'moscow-6', viewValue: 'Moscow'},
    //   ];
    //   emailFormControl = new FormControl('', [
    //     Validators.required,
    //     Validators.email,
    //   ]);

    matcher = new MyErrorStateMatcher();
    type: FormGroup;

    @ViewChild("userPic") fileInput;

    ProfilePicUrl: string = "";

    constructor(private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        injector: Injector,
        private _employeeService: EmployeeServiceProxy,
        private _jobTypeService: JobTypeServiceProxy,
        private _departmentService: DepartmentServiceProxy,
        private _costCenterService: CostCenterServiceProxy,
        private _employeeDesignationService: EmployeeDesignationServiceProxy,
        private http: Http,
        private _stateService: StateServiceProxy,
        private _regionService: RegionServiceProxy,
        private _zoneService: ZoneServiceProxy,
        private _cityService: CityServiceProxy,
        private _beltService: BeltServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private _province : ProvinceServiceProxy

    ) {
        //this route param will get route parameter from url and the plus sign is for to convert it into number : By Abubakar
        route.params.subscribe(p => {
            // this.employeeRecord.id = +p['id'];
            this.empId = +p['id'];
        })
    }

    //this function will call controller action for adding file and return the path of the picture.
    //this function will also create a new folder if it is not available in the directory : By Abubakar
    upload(fileToUpload: any) {
        debugger;
        let input = new FormData();
        input.append("file", fileToUpload);

        return this.http.post("http://localhost:21021/api/services/app/Account/CreateProfilePic", input);
    }


    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {
 
            this.hrConfig = result[0];
            let a = this.empId.toString();
            if(this.hrConfig.autoCode == true && a == "NaN" ){
 
                this.getAutoCode();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == true && this.empId!=null){
                this.isAutoCode = true;
              }
              else if(this.hrConfig.autoCode == false){
 
                    this.isAutoCode = false ;
              }
 
 
 
        });
 
    }



    /////////////////////////////////////////////////////////////////////////
    initValidation() {

        this._employeeWizardValidation = this.formBuilder.group({
            /*employeeName: [null, Validators.required],
            CNIC: [null, Validators.required],
            costCenterId: [null, Validators.required],
            stateId: [null, Validators.required],
            regionId: [null, Validators.required],
            zoneId: [null, Validators.required],
            beltId: [null, Validators.required], 
            cityId: [null, Validators.required],
            departmentId: [null, Validators.required],
            designationId: [null, Validators.required], 
            jobTypeId: [null, Validators.required],
            referredEmployeeId: [null, Validators.required],*/    
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
  /*  onType() {
    
        debugger;
      if (this._employeeWizardValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._employeeWizardValidation);
      }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
*/





    ///////////////////////////////////////////////////////////////////////////








    //this uploadUserPic function will upload the file and then call the upload method. Then it will call the save method after subscribing this function :By Abubakar
    uploadUserPic(): void {
        let fi = this.fileInput.nativeElement;
        if (fi.files && fi.files[0]) {
            let fileToUpload = fi.files[0];
            this.upload(fileToUpload)
                .subscribe(result => {
                    this.ProfilePicUrl = result.text();   //data.result
                    this.saveUpdateEmployeeRecord();
                });
        }
    }
//////////////////////////////////////////////

/*getAutoDocNumber():void{

    debugger;
    let i =0;
   
    for(i=0 ;i< this._employee.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._employee[i].employee_Code ; 

        this._empCode.push(abc_);

    }


    let code : any ;

    if(this._empCode.length == 0 ){
        debugger;
        code = "1";
        this.employeeRecord.employee_Code = "00" + code ;

    }

    else{

        if(this._empCode[this._empCode.length - 1] != null ){

                code = this._empCode[this._empCode.length-1].code ;
                code ++;
                if(code <=9){

                    this.employeeRecord.employee_Code = "00" + code ;
                }
                else if(code <=99){

                    this.employeeRecord.employee_Code = "0" + code ;
                }
                else if(code){

                    this.employeeRecord.employee_Code =  code ;
                }
        }                     
    }
    this._empCode = [];
}*/


getAutoCode():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._employee.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._employee[i].employee_Code ; 

        this._empCode.push(abc_);

    }


    let code : any ;

    if(this._empCode.length == 0  ){
      
        debugger;
        code = "1";
        this.employeeRecord.employee_Code = "00" + code ;

    }

    else{

        if(this._empCode[this._empCode.length - 1] != null ){

                let x;
                code = this._empCode[this._empCode.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.employeeRecord.employee_Code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.employeeRecord.employee_Code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.employeeRecord.employee_Code =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.employeeRecord.employee_Code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.employeeRecord.employee_Code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.employeeRecord.employee_Code =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.employeeRecord.employee_Code = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.employeeRecord.employee_Code =  "0" + code;
                  }
                  else if(code){
  
                      this.employeeRecord.employee_Code =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.employeeRecord.employee_Code = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.employeeRecord.employee_Code =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.employeeRecord.employee_Code =  temp[0] ;
                  }

                }
          
        }                     
    }
    this._empCode = [];
}






    
/////////////////////////////////////////////////
    //this function will save the record in datatabse.
    save(): void {
        //TODO: Refactor this, don't use jQuery style code
        debugger;
        this.saving = true;
        let fi = this.fileInput.nativeElement;
        if(fi.files && fi.files[0])
        {
            this.uploadUserPic();
        }
        else
        {
            this.saveUpdateEmployeeRecord();
        }
        
    }

    //this function has two block. One is for saving the new recored and the other one for updating the record.
    //if the url of this page has parameter, then it will call update block and vice versa
    saveUpdateEmployeeRecord(): void {
        debugger;
        if (this.empId) {
            //will update the record
            this.employeeRecord.id = this.empId;
            let fi = this.fileInput.nativeElement;
            if(fi.files && fi.files[0])
            {
                this.employeeRecord.employeePicture = this.ProfilePicUrl;
            }
            debugger;
            this._employeeService.update(this.employeeRecord)
                .finally(() => { this.saving = false; })
                .subscribe(() => {
                    this.router.navigate(['../hr/employee-list']);
                });
        }
        else {
            //will save the new record.
            let fi = this.fileInput.nativeElement;
            if(fi.files && fi.files[0])
            {
                this.employeeRecord.employeePicture = this.ProfilePicUrl;
            }
            this._employeeService.create(this.employeeRecord)
                .finally(() => { this.saving = false; })
                .subscribe(() => {
                    this.router.navigate(['../hr/employee-list']);
                });
        }
    }


    ////////////////////////
    //   isFieldValid(form: FormGroup, field: string) {
    //     return !form.get(field).valid && form.get(field).touched;
    //   }
    //   displayFieldCss(form: FormGroup, field: string) {
    //     return {
    //       'has-error': this.isFieldValid(form, field),
    //       'has-feedback': this.isFieldValid(form, field)
    //     };
    //   }
    //////////////////////////////////
    

    ngOnInit() {


       // this.initValidation();
       //this.getAllHRConfiguration();
        this.employeeRecord.isActive = true;
        this._employeeService.getAllEmployees().subscribe((result)=>{

          this._employee = result ;
          this.getAllHRConfiguration();  

        

        });

        
        this.employeeRecord.employeePicture = "placeholder.jpg";
        // this.employeeRecord.costCenterId = 0;
        // this.employeeRecord.refEmployeeId = 0;
        
        //get all needed record for dropdown will be called using this getAllDropDown function
        this.getAllDropDown();  
        if (this.empId) {
            this._employeeService.get(this.empId)
                .finally(() => {
                    this.active = true;
                })
                .subscribe((result: EmployeeDto) => {
                    this.employeeRecord = result;
                    this.employeeRecord.jobTypeId = this.employeeRecord.jobTypeId;
                }, err => {
                    if (err.status == 404) {
                        this.router.navigate(['../hr/employee-list']);
                    }
                }
                )
        }
        else{
            this.getCurrentDate();
        }
        // this.type = this.formBuilder.group({
        //     // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
        //     jobtype: [this.employeeRecord.jobTypeId, Validators.required]
        //     // lastName: [null, Validators.required],
        //     // email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
        // });
        // Code for the Validator
        // const $validator = $('.card-wizard form').validate({
        //     rules: {
        //         jobtype: {
        //             required: true
        //         }
        //     },

        //     highlight: function(element) {
        //       $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
        //     },
        //     success: function(element) {
        //       $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
        //     },
        //     errorPlacement : function(error, element) {
        //       $(element).append(error);
        //     }
        // });

        // Wizard Initialization
        $('.card-wizard').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-previous',

            onNext: function (tab, navigation, index) {
                var $valid = $('.card-wizard form').valid();
                if (!$valid) {
                    // $validator.focusInvalid();
                    // return false;
                }
            },

            onInit: function (tab: any, navigation: any, index: any) {

                // check number of tabs and fill the entire row
                let $total = navigation.find('li').length;
                let $wizard = navigation.closest('.card-wizard');

                let $first_li = navigation.find('li:first-child a').html();
                let $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
                $('.card-wizard .wizard-navigation').append($moving_div);

                $total = $wizard.find('.nav li').length;
                let $li_width = 100 / $total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                let $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
                $('.moving-tab').css('transition', 'transform 0s');
            },

            onTabClick: function (tab: any, navigation: any, index: any) {

                // const $valid = $('.card-wizard form').valid();

                // if (!$valid) {
                //     return false;
                // } else {
                //     return true;
                // }
            },

            onTabShow: function (tab: any, navigation: any, index: any) {
                let $total = navigation.find('li').length;
                let $current = index + 1;

                const $wizard = navigation.closest('.card-wizard');

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $($wizard).find('.btn-next').hide();
                    $($wizard).find('.btn-finish').show();
                } else {
                    $($wizard).find('.btn-next').show();
                    $($wizard).find('.btn-finish').hide();
                }

                const button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                setTimeout(function () {
                    $('.moving-tab').text(button_text);
                }, 150);

                const checkbox = $('.footer-checkbox');

                if (index !== 0) {
                    $(checkbox).css({
                        'opacity': '0',
                        'visibility': 'hidden',
                        'position': 'absolute'
                    });
                } else {
                    $(checkbox).css({
                        'opacity': '1',
                        'visibility': 'visible'
                    });
                }
                $total = $wizard.find('.nav li').length;
                let $li_width = 100 / $total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        });


        // Prepare the preview for profile picture
        // $('#wizard-picture').change(function(){
        //     const input = $(this);

        //     if (input[0].files && input[0].files[0]) {
        //         const reader = new FileReader();

        //         // reader.onload = function (e: FileReaderEvent) {
        //         //     $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        //         };
        //         reader.readAsDataURL(input[0].files[0]);
        //     }
        // });

        $('[data-toggle="wizard-radio"]').click(function () {
            const wizard = $(this).closest('.card-wizard');
            wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
            $(this).addClass('active');
            $(wizard).find('[type="radio"]').removeAttr('checked');
            $(this).find('[type="radio"]').attr('checked', 'true');
        });

        $('[data-toggle="wizard-checkbox"]').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).find('[type="checkbox"]').removeAttr('checked');
            } else {
                $(this).addClass('active');
                $(this).find('[type="checkbox"]').attr('checked', 'true');
            }
        });

        $('.set-full-height').css('height', 'auto');

    }







    //get all the current dates for this form's fields.
    getCurrentDate(){
        this.employeeRecord.attendanceDate = moment().add(5,'hour');
        this.employeeRecord.cnicExpiryDate = moment().add(5,'hour');
        this.employeeRecord.confirmationDate = moment().add(5,'hour');
        this.employeeRecord.contractDate = moment().add(5,'hour');
        this.employeeRecord.contractEndingDate = moment().add(5,'hour');
        this.employeeRecord.joiningDate = moment().add(5,'hour');
        this.employeeRecord.leaving_date = moment().add(5,'hour');
        this.employeeRecord.dob = moment().add(5,'hour');
        this.employeeRecord.graduityDate = moment().add(5,'hour');
    }


    //this getAllDropDown function will get all the needed drop down record like jobtype, department, designation, costcenter
    getAllDropDown(): void {

        //start to get all the state record for dropdown
        this._stateService.getAllStates()
            .subscribe((result)=> {
                this.stateDDList = result;
            });
        //end to get all the state record for dropdown
        //start to get all the region record for dropdown
        this._regionService.getAllRegions()
            .subscribe((result)=> {
                this.regionDDList = result;
            });
        //end to get all the region record for dropdown
        //start to get all the zone record for dropdown
        this._zoneService.getAllZones()
            .subscribe((result)=> {
                this.zoneDDList = result;
            });
        //end to get all the zone record for dropdown
        //start to get all the belt record for dropdown
        this._beltService.getAllBelts()
            .subscribe((result)=> {
                this.beltDDList = result;
            });
        //end to get all the belt record for dropdown
        //start to get all the city record for dropdown
        this._cityService.getAllCities()
            .subscribe((result)=> {
                this.cityDDList = result;
            });
        //end to get all the city record for dropdown


        //start to get all the Employees record for dropdown list
        this._employeeService.getAllEmployees()
            .subscribe((result) => {
                this.refEmployeeDDlist = result;
            })
        //end to get all the Employees record for dropdown list

        //start to get all the job type record for dropdown list
        this._jobTypeService.getAllJobTypes()
            .subscribe((result) => {
                this.jobTypeDDList = result;
            })
        //end to get all the job type record for dropdown list
        //start to get all the department record for dropdown list    
        this._departmentService.getAllDepartments()
            .subscribe((result) => {
                this.departmentDDList = result;
            })
        //end to get all the department record for dropdown list    
        //start to get all the costcenter record for dropdown list
        this._costCenterService.getAllCostCenters()
            .subscribe((result) => {
                this.costCenterDDList = result;
            })
        //end to get all the costcenter record for dropdown list
        //start to get all the designation record for dropdown list    
        this._employeeDesignationService.getAllEmployeeDesignations()
            .subscribe((result) => {
                this.designationDDList = result;
            });
        //end to get all the designation record for dropdown list
    }

    ngOnChanges(changes: SimpleChanges) {
        const input = $(this);

        if (input[0].files && input[0].files[0]) {
            const reader: any = new FileReader();

            reader.onload = function (e: FileReaderEvent) {
                $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
            };
            reader.readAsDataURL(input[0].files[0]);
        }
    }
    ngAfterViewInit() {

        $(window).resize(() => {
            $('.card-wizard').each(function () {

                const $wizard = $(this);
                const index = $wizard.bootstrapWizard('currentIndex');
                let $total = $wizard.find('.nav li').length;
                let $li_width = 100 / $total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                let $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
                });

                $('.moving-tab').css({
                    'transition': 'transform 0s'
                });
            });
        });
    }



    RegionWithState(id: number){
        let  cid;
        console.log("dddd::::", this.regionDDList);
        this._stateService.get(id).subscribe((result)=>{
 
            cid = result.countryId;
 
            this._province.getAllProvinceByCountryId(cid).subscribe((pvnc)=>{
 
 
                let m =0 ;
                let regionArr = new Array();
                for(m = 0 ; m<pvnc.length; m++){
 
                    regionArr.push(this._regionService.getAllRegionsByProvinceId(pvnc[m].id));
                }
                debugger
                Observable.forkJoin(regionArr)
                .subscribe(regionData => {
 
                    let u=0;
                    this.regionDDList = []
                    console.log("dd : ",regionData )
                    //this.regionDDList = regionData;
 
                    //console.log("regionDDList-", this.regionDDList);
                    /*for(u=0;u<regionData.length; u++){
 
                        this.regionDDList.push( regionData[u]);
 
                    }*/
 
                     regionData.forEach(element => {
                        this.regionDDList.push(element[0]);
                    });
                    console.log("regionDDList-", this.regionDDList);
                })
 
            })
 
        })
 
    }
 
 
    ZoneWithRegion(id: number){
        let ftrRegion;
        ftrRegion = this.regionDDList.find(function (obj) { return obj.id === id; });
 
        this.pid = ftrRegion.provinceId; // for getting cities//
        this.zoneDDList = [];
        this._zoneService.getZoneByRegionId(id).subscribe((zoneData)=>{
 
            this.zoneDDList = zoneData
 
        })
        //for getting cities //
 
        this._cityService.getCityWithProvinceId(this.pid).subscribe((cityData)=>{
 
            this.cityDDList = [];
            this.cityDDList = cityData;
        })
    }
    BeltWithZone(id: number){
        this.beltDDList = [];
        this._beltService.getBeltByZoneId(id).subscribe((beltData)=>{
            this.beltDDList  = beltData;
        })
    }
}
// class for keeping all empCode number
class abc{
    public code: string;
  }
