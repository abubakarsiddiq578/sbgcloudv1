// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Inject } from '@angular/core';
import { EditTaxSlabComponent } from '../tax-slab/edit-tax-slab/edit-tax-slab.component';
import { TaxSlabsMasterServiceProxy, TaxSlabMasterDto, TaxSlabsDetailServiceProxy, TaxSlabDetailDto,  HRConfigurationDto, HRConfigurationServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { ColDef, GridApi, ColumnApi } from '../../../../../node_modules/ag-grid-community';
import { Observable } from 'rxjs';
import { Moment } from 'moment';


declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'tax-slab',
  templateUrl: 'tax-slab.component.html',
  styleUrls: ['./tax-slab.component.less'],
  providers: [TaxSlabsMasterServiceProxy, TaxSlabsDetailServiceProxy , HRConfigurationServiceProxy]
})

export class TaxSlabComponent implements OnInit, AfterViewInit {
  public dataTable: DataTable;
  public dataTable1: DataTable;

  @ViewChild('editTaxSlabModal') editTaxSlabModal: EditTaxSlabComponent;

  public fromDate = new Date();
  public toDate = new Date();

  taxSlabMaster: TaxSlabMasterDto = new TaxSlabMasterDto();

  taxSlabDetail: TaxSlabDetailDto = new TaxSlabDetailDto();
  taxSlabDetailList = [];

  data: string[] = [];

  taxSlabMasterDto: TaxSlabMasterDto[];

  isDetail: boolean = true;
  isHistory: boolean = false;

  isSaveBtn: boolean = true;
  isEditBtn: boolean = false;

  detailId: number = 0;

  detailRecordId: number = 0;

  detailRecordLength: number = 0;
  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;


  _taxSlab : TaxSlabMasterDto[];

  _docNo : abc[] = []; 

////////////////////////////////////////////
  //public rowData: TaxSlabDetailDto[] = [];
 
  tempArr=[] ; // for storing data for save//
  public rowData : TaxSlabDetailDto[] = [];
  public columnDefs: ColDef[];
  public defaultColDef;


  // gridApi and columnApi
  private api: GridApi;
  private columnApi: ColumnApi;

   //row selectoin 
   public rowSelection;

   @ViewChild('PageSize') PageSize: any;
   gf = new GlobalFunctions

  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private taxSlabsMasterServiceProxy: TaxSlabsMasterServiceProxy,
    private _router : Router,
    private _hrConfigService : HRConfigurationServiceProxy ) {

      this.initAgGrid();
  }
globalFunction : GlobalFunctions = new GlobalFunctions;


  initAgGrid(){

    this.columnDefs = this.createColumnDefs();
    this.defaultColDef = { editable: true };
    this.rowSelection = "multiple";

   }



  ngOnInit() {
    if(!this.globalFunction.hasPermission("View","TaxSlab")){
      this.globalFunction.showNoRightsMessage("View")
      this._router.navigate(['']);
    }
    // reset and initialize detailDataTable Grid
   // this.initDetailDataTable();

    // get all tax slabs record in masterDataTable Grid
    this.getAllTaxSlabs();
    //this.getAllHRConfiguration();
  }


  public createColumnDefs() {
    //console.log("ffff: ", this.rowData);
    return [
        { field: 'valueFrom'},
        { field: 'valueTo'},
        { field: 'applicableAmount' , valueGetter: function chainValueGetter(params) { return params.getValue("valueTo") - params.getValue("valueFrom") }, },
        { field: 'fixed'} ,
        { field: 'taxPercentage'} ,
        { field: 'valuePerMonth' , valueGetter: function chainValueGetter(params) { return parseFloat((params.getValue("applicableAmount")/12).toFixed(2)) },} //,
       // { field: 'Action', valueGetter: (params) => params.data.id, cellRenderer: this.CellRendererDelete }
       /* { field: 'leaveTypeName'},
        { field: 'leaveAllowed'},
        { field: 'leaveReset' },
        { field: 'leaveDetail'} */
      //  { field: 'Action', valueGetter: (params) => params.data.id, cellRenderer: this.CellRendererDelete }
    ]
  }


  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];

        if(this.hrConfig.autoCode == true){

          this.getAutoTaxSlabNumber();
          this.isAutoCode = true ;
      }
        else if(this.hrConfig.autoCode == false){
          this.isAutoCode = false;
      }



    });

}




  getAutoTaxSlabNumber():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._taxSlab.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._taxSlab[i].code ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0  ){
      
        debugger;
        code = "1";
        this.taxSlabMaster.code = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                let x;
                code = this._docNo[this._docNo.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.taxSlabMaster.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.taxSlabMaster.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.taxSlabMaster.code =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.taxSlabMaster.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.taxSlabMaster.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.taxSlabMaster.code =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.taxSlabMaster.code = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.taxSlabMaster.code =  "0" + code;
                  }
                  else if(code){
  
                      this.taxSlabMaster.code =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.taxSlabMaster.code = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.taxSlabMaster.code =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.taxSlabMaster.code =  temp[0] ;
                  }

                }
          
        }                     
    }
    this._docNo = [];
}







  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('#datatables1').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table1 = $('#datatables1').DataTable();

    // Edit record
    table1.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table1.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table1.on('click', '.like', function (e) {
      //alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }



      // this set the page size
      onPageSizeChanged(value) {
        debugger
        //let value : any = this.PageSize.value;
        this.api.paginationSetPageSize(Number(value));
    }
    
    onCellValueChanged(params) {
      debugger
      let NewData = [];
      this.api.forEachNode(node => NewData.push(node.data));
      console.log(NewData)
    
      params.data.newValue
    }
    
    
     // one grid initialisation, grap the APIs and auto resize the columns to fit the available space
     onGridReady(params): void {
      this.api = params.api;
      this.columnApi = params.columnApi;
      //this.api.sizeColumnsToFit();
    }
    
    
     //Render Delete button // not using //
     CellRendererDelete(params) {
      var button = document.createElement('button');
      button.innerHTML = 'Del';
      button.addEventListener('click', function () {
          window.alert("Delete button clicked with id: " + params.value);
      });
    
      return button;
    }
    
    
    //Remove Selected Rows
    onRemoveSelected() {
      var selectedData = this.api.getSelectedRows();
      var res = this.api.updateRowData({ remove: selectedData });
    }
    
  // get all salary group records
  getAllTaxSlabs(): void {
    this.taxSlabsMasterServiceProxy.getAllTaxSlabs()
      .subscribe((result) => {
        this.initMasterDataTable();
        this.taxSlabMasterDto = result
        this._taxSlab = result;
        this.getAllHRConfiguration();
        this.fillMasterDataTable();
      });
  }




  // reset and initialize MasterDataTable Grid
  initMasterDataTable() {
    this.dataTable1 = {
      headerRow: ['Code', 'Tax Type', 'From Date', 'To Date', 'Actions'],
      footerRow: ['Code', 'Tax Type', 'From Date', 'To Date', 'Actions'],

      dataRows: [
        //['TaxSlabs-00-1', 'Sales Tax', '15-10-2018', '10-02-2019', ''],
        //['TaxSlabs-00-1', 'Sales Tax', '15-10-2018', '10-02-2019', ''],
        //['TaxSlabs-00-1', 'Sales Tax', '15-10-2018', '10-02-2019', ''],
      ]
    };
  }


  // fill datatable to show master record in datatable grid
  fillMasterDataTable() {
    let i = 0;

    for (i = 0; i < this.taxSlabMasterDto.length; i++) {
      this.data.push(this.taxSlabMasterDto[i].code);
      this.data.push(this.taxSlabMasterDto[i].taxType);
      this.data.push(this.taxSlabMasterDto[i].fromDate.toDate().toDateString());
      this.data.push(this.taxSlabMasterDto[i].toDate.toDate().toDateString());
      this.data.push(this.taxSlabMasterDto[i].id.toString());

      this.dataTable1.dataRows.push(this.data);

      this.data = [];
    }
  }

  // add the values in grid against master tax slab code
  addToGrid(): void {
    if (this.storage.get('TenantId') > 0) {
      this.taxSlabDetail.tenantId = this.storage.get('TenantId');
    }

    // icrement detailRecordId to identify each entry
    this.detailRecordId++;

    // set value of salaryGroupDetail.id to detailRecordId
    this.taxSlabDetail.id = this.detailRecordId;

    // push salaryGroupDetail in salaryGroupDetailList
    this.taxSlabDetailList.push(this.taxSlabDetail);

    // fill datatable to show detail record in datatable grid
   // this.fillDetailDataTable();
    /////////Editable grid code//////////////////
    this.rowData = this.taxSlabDetailList;
    this.initAgGrid();

    this.api.setRowData(this.rowData);

    ////////////////////////////////////////
  
    // create new object of salaryGroupDetail to add another value in the grid
    this.taxSlabDetail = new TaxSlabDetailDto();
  }

  saveData(){

    debugger;
    // set tenant id that get from session  
    if (this.storage.get('TenantId') > 0) {
      this.taxSlabMaster.tenantId = this.storage.get('TenantId');
    }


    let rowData = [];
    this.api.forEachNode((node) => {
      rowData.push(node.data)
    });

    this.tempArr =[];

    // set master id undefined to save record
    this.taxSlabMaster.id = undefined;

    let m=0;
    for( m=0;m<rowData.length; m++){

      this.rowData[m].id = undefined;
      this.rowData[m].taxSlabsMaster = undefined ;
      this.tempArr.push(this.rowData[m] );

    }

 
    //this.taxSlabMaster.fromDate = moment(this.fromDate);
    //this.taxSlabMaster.toDate = moment(this.toDate);

    debugger;
    this.taxSlabMaster.taxSlabsDetails = this.tempArr;
 
    this.taxSlabsMasterServiceProxy.add(this.taxSlabMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Saved Successfully');

        // perform reset operation after saving record

        //this.initDetailDataTable();
        this.getAllTaxSlabs();

        this.taxSlabDetailList = [];

        this.taxSlabMaster.code = "";
        this.taxSlabMaster.taxType = "";
        this.fromDate = new Date();
        this.toDate = new Date();

        this.detailRecordId = 0;

        this.detailRecordLength = 0;
      });
  }

  notifyMsg(title, msg) {
    swal({
      title: title,
      text: msg,
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  // delete record of salary group master 
  deleteTaxSlabMasterDetail(id: string) {

if(!this.globalFunction.hasPermission("Delete","TaxSlab")){
  this.globalFunction.showNoRightsMessage("Delete")
  return
}


    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Tax Slab!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.taxSlabsMasterServiceProxy.delete(parseInt(id))
          .finally(() => {

            this.getAllTaxSlabs();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Tax Slab has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Tax Slab is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }


  EditTaxSlabMasterDetail(id: string) {
    debugger;
    this.detailId = parseInt(id);

    this.taxSlabMaster.id = parseInt(id);
    this.taxSlabsMasterServiceProxy.getAllDetailById(parseInt(id))
      .subscribe((result) => {
        debugger;
        this.taxSlabDetailList = [];
        this.rowData = [];

        this.taxSlabDetailList = result;
        
        this.taxSlabMaster.code = result[0].taxSlabsMaster.code;
        this.taxSlabMaster.fromDate = result[0].taxSlabsMaster.fromDate ;
        this.taxSlabMaster.toDate = result[0].taxSlabsMaster.toDate;
        this.taxSlabMaster.taxType = result[0].taxSlabsMaster.taxType;
      
        this.rowData = this.taxSlabDetailList;

        this.initAgGrid();
  
        this.api.setRowData(this.rowData);


        this.ActiveUpdateBtn();

      });

      this.ActiveDetail();
  }


  edit(): void {
    if(!this.globalFunction.hasPermission("Edit","TaxSlab")){
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    if (this.storage.get('TenantId') > 0) {
      this.taxSlabMaster.tenantId = this.storage.get('TenantId');
    }

   

    let rowData = [];
    this.api.forEachNode((node) => {
      rowData.push(node.data)
    });

    this.tempArr =[];

    
    let m=0;
    for( m=0;m<rowData.length; m++){

      this.rowData[m].id = undefined;
      this.rowData[m].taxSlabsMaster = undefined ;
      this.tempArr.push(this.rowData[m] );

    }




    this.taxSlabMaster.taxSlabsDetails = this.tempArr;

    this.taxSlabsMasterServiceProxy.edit(this.taxSlabMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Updated Successfully');

        //this.initDetailDataTable();
        this.getAllTaxSlabs();

        this.taxSlabDetailList = [];

        this.ResetControls();
        /*this.taxSlabMaster.code = "";
        this.taxSlabMaster.taxType = "";
        this.fromDate = new Date();
        this.toDate = new Date();*/

        this.detailRecordId = 0;

        this.detailRecordLength = 0;
      });
  }



  doCalculation() {
    this.taxSlabDetail.applicableAmount = this.taxSlabDetail.valueTo - this.taxSlabDetail.valueFrom
    this.taxSlabDetail.valuePerMonth = (this.taxSlabDetail.valueTo - this.taxSlabDetail.valueFrom) / 12;
    this.taxSlabDetail.valuePerMonth = parseFloat(this.taxSlabDetail.valuePerMonth.toFixed(2));
  }

  // show detail tab 
  ActiveDetail() {
    this.isDetail = true;
    this.isHistory = false;
  }

  // show history tab 
  ActiveHistory() {
    this.isDetail = false;
    this.isHistory = true;
  }

  // show save btn
  ActiveSaveBtn() {
    this.isSaveBtn = true;
    this.isEditBtn = false;
  }

  // show update btn
  ActiveUpdateBtn() {
    this.isSaveBtn = false;
    this.isEditBtn = true;
  }


  ResetControls() {


    this.taxSlabMaster = new TaxSlabMasterDto();

    this.taxSlabDetailList = [];

    this.detailRecordId = 0;

    this.detailRecordLength = 0;

    this.rowData = [];
    this.initAgGrid();

    this.api.setRowData(this.rowData);


    this.ActiveSaveBtn();

  }

   /* initDetailDataTable() {
    this.dataTable = {
      headerRow: ['Value From', 'Value To', 'Applicable Amount', 'Tax %', 'Fixed', 'Value Per Month', 'Actions'],
      footerRow: ['Value From', 'Value To', 'Applicable Amount', 'Tax %', 'Fixed', 'Value Per Month', 'Actions'],

      dataRows: [
        //['150,000', '200,000', '150', '10%', '150', '10', ''],
        //['150,000', '200,000', '150', '10%', '150', '10', ''],
        //['150,000', '200,000', '150', '10%', '150', '10', ''],
      ]
    };
  }*/

    // fill datatable to show detail record in datatable grid
 /* fillDetailDataTable() {
    try {
      this.data.push(this.taxSlabDetail.valueFrom.toString());
      this.data.push(this.taxSlabDetail.valueTo.toString());
      this.data.push(this.taxSlabDetail.applicableAmount.toString());
      this.data.push(this.taxSlabDetail.taxPercentage.toString());
      this.data.push(this.taxSlabDetail.fixed.toString());
      this.data.push(this.taxSlabDetail.valuePerMonth.toString());
      this.data.push(this.taxSlabDetail.id.toString());

      this.dataTable.dataRows.push(this.data)

      this.data = [];
    }
    catch{
      this.notifyMsg("Error" , "Please Provide all Required Fields");
    }
  }*/

  // fill datatable to show detail record in datatable grid in eidt mode
 /* fillEditDetailDataTable() {
    let i;

    for (i = 0; i < this.taxSlabDetailList.length; i++) {
      this.data.push(this.taxSlabDetailList[i].valueFrom.toString());
      this.data.push(this.taxSlabDetailList[i].valueTo.toString());
      this.data.push(this.taxSlabDetailList[i].applicableAmount.toString());
      this.data.push(this.taxSlabDetailList[i].taxPercentage.toString());
      this.data.push(this.taxSlabDetailList[i].fixed.toString());
      this.data.push(this.taxSlabDetailList[i].valuePerMonth.toString());
      this.data.push(this.taxSlabDetailList[i].id.toString());

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }*/


    // perform save opertaion to save data
  /*save(): void {

    debugger;
    // set tenant id that get from session  
    if (this.storage.get('TenantId') > 0) {
      this.taxSlabMaster.tenantId = this.storage.get('TenantId');
    }

    // set master id undefined to save record
    this.taxSlabMaster.id = undefined;

    this.taxSlabMaster.fromDate = moment(this.fromDate);
    this.taxSlabMaster.toDate = moment(this.toDate);

    // set detail id undefined to save record
    for (let i = 0; i < this.taxSlabDetailList.length; i++) {
      this.taxSlabDetailList[i].id = undefined;

    }

   

    //this.taxSlabMaster.taxSlabsDetails = this.taxSlabDetailList;

    //this.taxSlabMaster.taxSlabsDetails = this.rowData
    this.taxSlabsMasterServiceProxy.add(this.taxSlabMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Saved Successfully');

        // perform reset operation after saving record

        //this.initDetailDataTable();
        this.getAllTaxSlabs();

        this.taxSlabDetailList = [];

        this.taxSlabMaster.code = "";
        this.taxSlabMaster.taxType = "";
        this.fromDate = new Date();
        this.toDate = new Date();

        this.detailRecordId = 0;

        this.detailRecordLength = 0;
      });
  }*/

    // get detail record of particular master in edit mode
  /*EditTaxSlabMasterDetail(id: string) {
    this.detailId = parseInt(id);

    this.taxSlabsMasterServiceProxy.getAllDetailById(parseInt(id))
      .subscribe((result) => {
        this.taxSlabDetailList = [];
        this.taxSlabDetailList = result;

        this.taxSlabsMasterServiceProxy.get(parseInt(id))
          .finally(() => {
          })
          .subscribe((result) => {
            this.taxSlabMaster.code = result.code;
            this.taxSlabMaster.taxType = result.taxType;
            this.fromDate = result.fromDate.toDate();
            this.toDate = result.toDate.toDate();

            this.detailRecordLength = this.taxSlabDetailList.length;
            this.initDetailDataTable();
            this.fillEditDetailDataTable();
            this.ActiveUpdateBtn();
          });
      });

    this.ActiveDetail();
  }*/

  // perform edit operation to edit data 

  

    // perform edit operation to edit data 
 /* edit(): void {
    if (this.storage.get('TenantId') > 0) {
      this.taxSlabMaster.tenantId = this.storage.get('TenantId');
    }

    this.taxSlabMaster.id = this.detailId;

    this.taxSlabMaster.fromDate = moment(this.fromDate).add(5,'hour');
    this.taxSlabMaster.toDate = moment(this.toDate).add(5,'hour');

    for (let i = this.detailRecordLength; i < this.taxSlabDetailList.length; i++) {
      this.taxSlabDetailList[i].id = undefined;
    }

    this.taxSlabMaster.taxSlabsDetails = this.taxSlabDetailList;

    this.taxSlabsMasterServiceProxy.edit(this.taxSlabMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Updated Successfully');

        this.initDetailDataTable();
        this.getAllTaxSlabs();

        this.taxSlabDetailList = [];

        this.taxSlabMaster.code = "";
        this.taxSlabMaster.taxType = "";
        this.fromDate = new Date();
        this.toDate = new Date();

        this.detailRecordId = 0;

        this.detailRecordLength = 0;
      });
  }*/


   //this.taxSlabMaster.id = this.detailId;

    //this.taxSlabMaster.fromDate = moment(this.fromDate);
    //this.taxSlabMaster.toDate = moment(this.toDate);

    /*for (let i = this.detailRecordLength; i < this.taxSlabDetailList.length; i++) {
      this.taxSlabDetailList[i].id = undefined;
    }*/


  // delete data of Tax Slab detail record from detail datatable 
  DeleteTaxSlabDetail(id: string) {
    if(!this.globalFunction.hasPermission("Delete","TaxSlab")){
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }
  }
  /*DeleteTaxSlabDetail(id: string) {
    let salaryGroupDetail;

    // get record of matching id
    salaryGroupDetail = this.taxSlabDetailList.find(function (obj) { return obj.id === parseInt(id); });

    // perform delete operation to delete object from detail list 
    const index: number = this.taxSlabDetailList.indexOf(salaryGroupDetail);
    if (index !== -1) {
      this.taxSlabDetailList.splice(index, 1);

      this.initDetailDataTable();
      this.fillEditDetailDataTable();
    }
  }*/

  // open edit pop up from to edit of detail datatable record
  EditTaxSlabDetail(id: string) {
    if(!this.globalFunction.hasPermission("Edit","TaxSlab")){
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
 /* EditTaxSlabDetail(id: string) {
    let salaryGroupDetail;

    salaryGroupDetail = this.taxSlabDetailList.find(function (obj) { return obj.id === parseInt(id); });

    this.editTaxSlabModal.show(salaryGroupDetail);
  }*/

  // incase when edit operation is apply to edit data of detail datatable record
  /*updateSalaryGroupDetail(taxSlabDetail) {
    this.initDetailDataTable();
    this.fillEditDetailDataTable();
  }*/


  // reset value of master detail input and detail datatable grid record
  /*reset() {
    this.taxSlabMaster.code = "";
    this.taxSlabMaster.taxType = "";
    this.fromDate = new Date();
    this.toDate = new Date();
    this.detailId = 0;

    this.taxSlabDetail.valueFrom = null;
    this.taxSlabDetail.valueTo = null;
    this.taxSlabDetail.applicableAmount = null;
    this.taxSlabDetail.taxPercentage = null;
    this.taxSlabDetail.fixed = null;
    this.taxSlabDetail.valuePerMonth = null;

    this.taxSlabDetailList = [];

    this.detailRecordId = 0;

    this.detailRecordLength = 0;

    this.initDetailDataTable();
    this.ActiveSaveBtn();
  }*/

}
}

// class for keeping all docNo number
class abc{
  public code: string;
}



