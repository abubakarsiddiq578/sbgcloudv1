import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';
import { TaxSlabComponent } from '../tax-slab.component';

import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';


@Component({
  selector: 'edit-tax-slab',
  templateUrl: './edit-tax-slab.component.html'
})
export class EditTaxSlabComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editTaxSlabModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    editData: any;


    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       
    ) {
        //super(injector);
    }
  

    show(editData): void{
        this.active = true;
        this.modal.show();

        this.editData = editData;
    }

    save(){
        this.close();
        this.modalSave.emit(this.editData);
    }


    ngOnInit(): void {
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    doCalculation(){
        this.editData.applicableAmount = this.editData.valueTo - this.editData.valueFrom
        this.editData.valuePerMonth = (this.editData.valueTo - this.editData.valueFrom) / 12;
        this.editData.valuePerMonth = parseFloat(this.editData.valuePerMonth.toFixed(2));
      }
}

