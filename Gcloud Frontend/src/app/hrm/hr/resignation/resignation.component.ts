// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import{ AddResignationComponent }  from './add-resignation/add-resignation.component'; 
import { EditResignationComponent } from './edit-resignation/edit-resignation.component';
import { ResignationDto, ResignationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';


declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'resignation',
  templateUrl: 'resignation.component.html',
  providers: [ResignationServiceProxy]
})

export class ResignationComponent implements OnInit, AfterViewInit {

  @ViewChild('addResignationModal') addResignationModal: AddResignationComponent;

  @ViewChild('editResignationModal') editResignationModal: EditResignationComponent;

  public dataTable: DataTable;


  data: string[] = [];

  Resignation: ResignationDto[];

  constructor(private ResignationServiceProxy: ResignationServiceProxy ,private _router : Router) {
  }

  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Resignation")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate([''])
    }

    this.getAllResignation();

  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  

  AddResignation(): void {
    if (!this.globalFunction.hasPermission("Create", "Resignation")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
   
    this.addResignationModal.show();
  }


  EditResignation(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Resignation")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    this.editResignationModal.show(parseInt(id));
  }



  initDataTable() {

    this.dataTable = {
     headerRow: ['Doc No', 'Doc Date', 'Employee', 'Notice Date', 'Resignation Date', 'Resignation Reason', 'Actions'],
      footerRow: [/*'Doc No', 'Doc Date', 'Employee', 'Notice Date', 'Resignation Date', 'Resignation Reason', 'Actions'*/],

      dataRows: []
    };

  }


  fillDataTable() {
    
    let i;
    for (i = 0; i < this.Resignation.length; i++) {

      this.data.push(this.Resignation[i].documentNo)
      this.data.push(this.Resignation[i].documentDate.toString())
    
      this.data.push(this.Resignation[i].employee.employee_Name) 
      this.data.push(this.Resignation[i].noticeDate.toString())
       this.data.push(this.Resignation[i].resignationDate.toString())
      this.data.push(this.Resignation[i].resignationReason)
      this.data.push(this.Resignation[i].id.toString())

     


      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }






  getAllResignation(): void {
    this.ResignationServiceProxy.getAllResignations()
      .subscribe((result) => {
        debugger;
        this.initDataTable()
        this.Resignation = result
        this.fillDataTable();
      });
  }





  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Resignation")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.ResignationServiceProxy.delete(parseInt(id))
          .finally(() => {
            this.getAllResignation();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Resignation has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Resignation is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
}

}


