import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ResignationDto, ResignationServiceProxy, EmployeeServiceProxy, EmployeeDto, HRConfigurationServiceProxy, HRConfigurationDto } from 'app/shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';
@Component({
  selector: 'edit-resignation',

  templateUrl: './edit-resignation.component.html',
  styleUrls : ['./edit-resignation.component.less'],
  providers: [ResignationServiceProxy, EmployeeServiceProxy, HRConfigurationServiceProxy]

})
export class EditResignationComponent implements OnInit {


  @ViewChild('editResignationModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;
  active: boolean = false;
  saving: boolean = false;

  employee: EmployeeDto[] = [];

  Resignation: ResignationDto = new ResignationDto();

  documentDate: Date = new Date(); // for getting datetime

  noticeDate: Date = new Date(); //for getting datetime for notice date

  resignationDate : Date = new Date(); //for getting datetime for resigndate

  _resign : FormGroup

  myControl = new FormControl();

  filteredOptions: Observable<EmployeeDto[]>;

  public empName: string ;

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  EmpName: string;


  constructor(injector: Injector,
    private ResignationServiceProxy: ResignationServiceProxy,
    private employeeService: EmployeeServiceProxy,
    private _hrConfigService : HRConfigurationServiceProxy,
    private formBuilder : FormBuilder

  ) { }


  EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.Resignation.employeeId = this.employee[i].id;
      }
    }


  }
  addSearchDD(){
    this.searchEditEmployeeDropdownModel.show()
  }
  ngOnInit(): void {

    this.employeeService.getAllEmployees()
      .subscribe((result) => {
        this.employee = result;
        console.log("this is employee: ", this.employee);

      })
      this.initValidation();
      this.getAllHRConfiguration();
  }


  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];


        if (this.hrConfig.autoCode == true) {

            this.isAutoCode = true;
        }
        else if (this.hrConfig.autoCode == false) {
            this.isAutoCode = false;
        }

    });

}





  ////////////////////////////////////////////////////////////////////////////////////////

  public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }

  
 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Resignation.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  
/////////////////////////////////////////////////////////////////////


  initValidation() {


    this._resign = this.formBuilder.group({
        employeeId: [null, Validators.required],
        resignationDate : [null , Validators.required]
    });

}

/*
onType() {

  if (this._resign.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._resign);

  }
}*/

onType() {
 
  if (this._resign.valid) {
  
    let a,p=0 ;
    for(a=0;a<this.employee.length;a++){
      if(this.Resignation.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.empName = null; // null empName 
    
      this.validateAllFormFields(this._resign);
      this.employeeDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._resign);
     
  }
}


validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}


  show(id: number): void {

    
    debugger
    this.ResignationServiceProxy.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();

      })
      .subscribe((result: ResignationDto) => {

        this.Resignation = result;
        this.employeeService.get(result.employeeId).subscribe((result)=>{
          this.empName = result.employee_Name ;
      })
      });
      this.employeeDropdownSearch();
      this._resign.markAsUntouched({onlySelf:true});
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }


  notify() {
    swal({
      title: "Success!",
      text: "Record Updated Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  save(): void {


    this.Resignation.documentDate = moment(this.documentDate).add(5,'hour');
    this.Resignation.noticeDate = moment(this.noticeDate).add(5,'hour');
    this.Resignation.resignationDate = moment(this.resignationDate).add(5,'hour');

    this.saving = true;

   

    this.ResignationServiceProxy.update(this.Resignation)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  onShown():void{
    
  }


}
