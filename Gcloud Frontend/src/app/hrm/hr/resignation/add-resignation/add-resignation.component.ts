import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ResignationDto, ResignationServiceProxy, EmployeeServiceProxy, EmployeeDto, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';

@Component({
  selector: 'add-resignation',
  templateUrl: './add-resignation.component.html',
  styleUrls: ['./add-resignation.component.less'],
  providers: [ResignationServiceProxy, EmployeeServiceProxy , HRConfigurationServiceProxy]
})
export class AddResignationComponent implements OnInit {


  @ViewChild('addResignationModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;
  @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
  active: boolean = false;
  saving: boolean = false;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  employee: EmployeeDto[] = [];

  documentDate: Date = new Date(); // for getting datetime

  noticeDate: Date = new Date(); //for getting datetime for notice date

  resignationDate: Date = new Date(); //for getting datetime for resigndate


  _resignation: ResignationDto[]; //for getting disable docNo from transfer table
  _docNo: abc[] = [];
  EmpName: string
  myControl = new FormControl();

  filteredOptions: Observable<EmployeeDto[]>;

  public empName: string ;

  _resign: FormGroup

  Resignation: ResignationDto = new ResignationDto();

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(injector: Injector,
    private ResignationProxyService: ResignationServiceProxy,
    private employeeService: EmployeeServiceProxy,
    private _hrConfigService : HRConfigurationServiceProxy,
    private formBuilder: FormBuilder

  ) {


  }

  EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.Resignation.employeeId = this.employee[i].id;
      }
    }
  }
  addSearchDD(){
    this.searchEmployeeDropdownModel.show()
  }
  ngOnInit(): void {

    this.employeeService.getAllEmployees()
      .subscribe((result) => {
        this.employee = result;
        console.log("this is employee: ", this.employee);

      })
    this.initValidation();
    this.getAllHRConfiguration();

  }


  initValidation() {


    this._resign = this.formBuilder.group({
      employeeId: [null, Validators.required],
      resignationDate: [null, Validators.required]
    });

  }


  
  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];
    });

}

////////////////////////////////////////////////////////////////////////////////////////

  public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }

  
 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Resignation.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  
/////////////////////////////////////////////////////////////////////

  show(): void {

    this.active = true;
    this.modal.show();
    this.Resignation = new ResignationDto();
    this.ResignationProxyService.getAllResignations().subscribe((result) => {

      this._resignation = result;
      //this.getAutoDocNumber();
      if(this.hrConfig.autoCode == true){

        this.getAutoDocNumber();
        this.isAutoCode = true ;
      }
      else if(this.hrConfig.autoCode == false){
        this.isAutoCode = false;
      }


    })

    this.myControl = new FormControl();
    this.filteredOptions = new Observable<EmployeeDto[]>();
    this.employeeDropdownSearch();
    this._resign.markAsUntouched({ onlySelf: true });

  }


  /*getAutoDocNumber(): void {

    debugger;
    let i = 0;

    for (i = 0; i < this._resignation.length; i++) {

      let abc_;

      abc_ = new abc();

      abc_.code = this._resignation[i].documentNo;

      this._docNo.push(abc_);

    }


    let code: any;

    if (this._docNo.length == 0) {
      debugger;
      code = "1";
      this.Resignation.documentNo = "00" + code;

    }

    else {

      if (this._docNo[this._docNo.length - 1] != null) {

        code = this._docNo[this._docNo.length - 1].code;
        code++;
        if (code <= 9) {

          this.Resignation.documentNo = "00" + code;
        }
        else if (code <= 99) {

          this.Resignation.documentNo = "0" + code;
        }
        else if (code <= 999) {

          this.Resignation.documentNo = code;
        }
      }
    }
    this._docNo = [];
  }*/

  getAutoDocNumber():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._resignation.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._resignation[i].documentNo ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0  ){
      
        debugger;
        code = "1";
        this.Resignation.documentNo = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                let x;
                code = this._docNo[this._docNo.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.Resignation.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Resignation.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Resignation.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.Resignation.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Resignation.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Resignation.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.Resignation.documentNo = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.Resignation.documentNo =  "0" + code;
                  }
                  else if(code){
  
                      this.Resignation.documentNo =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.Resignation.documentNo = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.Resignation.documentNo =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.Resignation.documentNo =  temp[0] ;
                  }

                }
        
        }                     
    }
    this._docNo = [];
}




  /*onType() {

    if (this._resign.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._resign);

    }
  }*/

  onType() {
 
    if (this._resign.valid) {
    
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.Resignation.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
      
        this.validateAllFormFields(this._resign);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._resign);
       
    }
  }



  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  onShown(): void { }


  save(): void {

    this.Resignation.documentDate = moment(this.documentDate).add(5,'hour');
    this.Resignation.noticeDate = moment(this.noticeDate).add(5,'hour');
    this.Resignation.resignationDate = moment(this.resignationDate).add(5,'hour');
    this.saving = true;
    this.ResignationProxyService.create(this.Resignation)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }



}
class abc{
  public code: string;
}
