// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddFineComponent } from './add-fine/add-fine.component';
import { EditFineComponent } from './edit-fine/edit-fine.component';
import { FineServiceProxy , FineDto } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  //footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'fine',
    templateUrl: 'fine.component.html',
    providers: [FineServiceProxy] 
})

export class FineComponent implements OnInit, AfterViewInit {
    public dataTable: DataTable;

    @ViewChild('addFineModal') addFineModal: AddFineComponent;
    @ViewChild('editFineModal') editFineModal: EditFineComponent;

    fine: FineDto[];

    data: string[] = [];

    constructor(
      private fineServiceProxy: FineServiceProxy,
      private _router : Router
  ) {

  }
  globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "Fine")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
      this.getAllFines();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        //alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    AddFine(): void {
      if (!this.globalFunction.hasPermission("Create", "Fine")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addFineModal.show();
    }

    EditFine(id: string): void {
      if (!this.globalFunction.hasPermission("Edit", "Fine")) {
        this.globalFunction.showNoRightsMessage("Edit");
        return
      }
        this.editFineModal.show(parseInt(id));
    }

    getAllFines(): void{
      this.fineServiceProxy.getAllFines()
      .subscribe((result) => {
        this.initDataTable()
        this.fine = result
        this.fillDataTable();   
      });
    }

    initDataTable(): void{
      this.dataTable = {
        headerRow: [ 'Doc No', 'Doc Date', 'Fine Date', 'Employee', 'Fine Type', 'Fine Amount', 'Detail' , 'Actions' ],
        //footerRow: [ 'Doc No', 'Doc Date', 'Fine Date', 'Employee', 'Fine Type', 'Fine Amount', 'Actions' ],

        dataRows: [
            //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Late','100', ''],
            //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Late','100', ''],
        ]
     };
    }

    fillDataTable(){

      let i;
      for (i= 0; i < this.fine.length; i++) {
  
        this.data.push(this.fine[i].docNo)
        this.data.push( this.fine[i].docDate.toDate().toDateString())
        this.data.push(this.fine[i].fineDate.toDate().toDateString())
        this.data.push(this.fine[i].employee.employee_Name)
        this.data.push(this.fine[i].fineType.fintTitle)
        this.data.push(this.fine[i].amount.toString())
        this.data.push(this.fine[i].detail.toString())
        this.data.push(this.fine[i].id.toString())
  
        this.dataTable.dataRows.push(this.data)
  
        this.data = [];
      }
    }

    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "Fine")) {
        this.globalFunction.showNoRightsMessage("Delete");
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Fine!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
      if (result.value) {
        this.fineServiceProxy.delete(parseInt(id))
          .finally(() => {
            this.getAllFines();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Fine has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
          }).catch(swal.noop)
           });
      } else {
        swal({
            title: 'Cancelled',
            text: 'Your Fine is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }
}


