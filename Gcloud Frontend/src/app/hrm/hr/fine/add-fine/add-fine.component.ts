import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { FineComponent } from '../fine.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { FineServiceProxy , FineDto , FineTypeDropdownServiceProxy , FineTypeDropdownDto , EmployeeDropdownDto, EmployeeDropDownServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy, EmployeeDto, EmployeeServiceProxy} from '../../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import * as moment from 'moment';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';

@Component({
  selector: 'add-fine',
  templateUrl: './add-fine.component.html',
  styleUrls : ['./add-fine.component.less'],
  providers: [FineTypeDropdownServiceProxy , EmployeeDropDownServiceProxy ,EmployeeServiceProxy, FineServiceProxy , HRConfigurationServiceProxy] 
})
export class AddFineComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addFineModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    public docDate = new Date();
    public fineDate = new Date();

    public EmpName: string;

   employee: EmployeeDto[] = [];

    employeeDropDown: EmployeeDropdownDto[] = null;  
    fineTypeDropDown: FineTypeDropdownDto[] = null; 
    
    fine: FineDto = new FineDto();

    _fine : FineDto[] ;
    _docNo : abc[] = [];

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDropdownDto[]>;

    public empName : string ;

    _fineValidation : FormGroup
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        private fineTypeDropdownServiceProxy: FineTypeDropdownServiceProxy,
        private employeeDropDownServiceProxy: EmployeeDropDownServiceProxy,
        private fineServiceProxy: FineServiceProxy,
        private formBuilder : FormBuilder,
        private employeeService : EmployeeServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy
       
    ) {
        //super(injector);
    }
  

    show(): void{

        this.active = true;
        this.modal.show();
        
        this.fine = new FineDto();

        this.fineServiceProxy.getAllFines().subscribe((result)=>{

            this._fine = result ;
           //this.getAutoDocNumber();
           if(this.hrConfig.autoCode == true){
            debugger;
            this.getAutoDocNumber();
            this.isAutoCode = true ;
          }
          else if(this.hrConfig.autoCode == false){
            this.isAutoCode = false;
          }
        })

        this.myControl = new FormControl();
        this.filteredOptions = new Observable<EmployeeDropdownDto[]>();
        this._fineValidation.markAsUntouched({onlySelf:true});
    }
    ////////////////////////////////////////////////////////////////////////////////

    initValidation() {

        this._fineValidation = this.formBuilder.group({
            fineDate: [null, Validators.required],
            employeeId : [null , Validators.required],
            fineTypeId : [null , Validators.required],
            
        });
    
    }

    
    //check form validation if it is valid then save it else throw error message in form //
   /* onType() {
    
      if (this._fineValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._fineValidation);
      }
    }*/


      
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

        });

    }

    onType() {
        
        if (this._fineValidation.valid) {
            let a, p = 0;
            for (a = 0; a < this.employeeDropDown.length; a++) {
                if (this.fine.employeeId == this.employeeDropDown[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._fineValidation);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._fineValidation);

        }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }

   ///////////////////////////////////////////////////////////////////////////////////

    
         //this function generate auto doc number
         getAutoDocNumber():void{

            debugger;
            let i =0;
            
            let temp : any[];
           
            for(i=0 ;i< this._fine.length ; i++){
        
                let abc_ ; 
        
                abc_ = new abc();
        
                abc_.code = this._fine[i].docNo ; 
        
                this._docNo.push(abc_);
        
            }
        
        
            let code : any ;
        
            if(this._docNo.length == 0  ){
              
                debugger;
                code = "1";
                this.fine.docNo = "00" + code ;
        
            }
        
            else{
        
                if(this._docNo[this._docNo.length - 1] != null ){
        
                        let x;
                        code = this._docNo[this._docNo.length-1].code ;
                        if(code!=null){
                          temp = code.split("-");
                           x = parseInt(temp[0]);
                           x = x.toString();
                        }
                        debugger;
                        //let j = parseInt(code);
                        //
                        if(temp.length == 1 && x=="NaN"  ){
        
                          debugger;
                          temp[1] = 0;
                          temp[1] ++;
        
                         
                          if(temp[1] <=9){
          
                              this.fine.docNo = temp[0] + "-00" + temp[1] ;
                          }
                          else if(temp[1] <=99){
          
                              this.fine.docNo =  temp[0] + "-0" + temp[1] ;
                          }
                          else if(temp[1]){
          
                              this.fine.docNo =  temp[0] + "-" +temp[1] ;
                          }
        
                        
                        }
        
                        else if (temp.length == 2) {
        
                          temp[1] ++;
                          if(temp[1] <=9){
          
                              this.fine.docNo = temp[0] + "-00" + temp[1] ;
                          }
                          else if(temp[1] <=99){
          
                              this.fine.docNo =  temp[0] + "-0" + temp[1] ;
                          }
                          else if(temp[1]){
          
                              this.fine.docNo =  temp[0] + "-" +temp[1] ;
                          }
        
                        }
        
                        else if(temp.length == 0){
        
                          code ++;
                          if(code <=9){
          
                              this.fine.docNo = "00" + code ;
                          }
                          else if(code<=99){
          
                              this.fine.docNo =  "0" + code;
                          }
                          else if(code){
          
                              this.fine.docNo =  code ;
                          }
        
        
                        } 
                        else if(temp.length==1 && x!="NaN" ){
        
                          temp[0]++;
                          if(temp[0] <=9){
          
                              this.fine.docNo = "00" + temp[0] ;
                          }
                          else if(code<=99){
          
                              this.fine.docNo =  "0" + temp[0];
                          }
                          else if(code){
          
                              this.fine.docNo =  temp[0] ;
                          }
        
                        }
        
        
        
                        
                }                     
            }
            this._docNo = [];
        }
        



    ngOnInit(): void {
        this.employeeService.getAllEmployees()
       .subscribe((result) => {
           this.employee = result;

       })
        this.fillEmployeeDropDown();
        this.fillFineTypeDropDown();
        this.initValidation();
        this.getAllHRConfiguration();
    }
    addSearchDD(){
        this.searchEmployeeDropdownModel.show()
      }

    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    fillEmployeeDropDown(){
        this.employeeDropDownServiceProxy.getEmployeeDropdown()
        .subscribe((result) => {
            this.employeeDropDown = result.items;
        });
    }

    fillFineTypeDropDown(){
        this.fineTypeDropdownServiceProxy.getFineTypeDropdown()
        .subscribe((result) => {
            this.fineTypeDropDown = result.items;
        });
    }

    doTextareaValueChange(ev){
        try {
          this.fine.detail = ev.target.value;
        } catch(e) {
          console.info('could not set textarea-value');
        }
    }

    save(): void {
        this.fine.docDate = moment(this.docDate).add(5,'hour');
        this.fine.fineDate = moment(this.fineDate).add(5,'hour');

        this.saving = true;
        this.fineServiceProxy.create(this.fine)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify();
                this.close();   
                this.modalSave.emit(null);
        });
    }

    notify(){
        swal({
            title: "Success!",
            text: "Saved Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }


      ////////////////////////////////Filter Method////////////////////////////////////////////////////////

public employeeDropdownSearch(){
       debugger;
      this.filteredOptions = this.myControl.valueChanges
       .pipe(
         startWith<string | EmployeeDto >(''),
         map(value => typeof value === 'string' ? value : value.employee_Name),
         map(name => name ? this._filter(name) : this.employee.slice())
       );

     }

     private _filter(value: string): EmployeeDto[]{

       debugger;
       const filterValue = value.toLowerCase();

       return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
     }


     displayEmployee = (emp: EmployeeDto):any => {
       debugger;

       if(emp instanceof EmployeeDto)
       {
           this.fine.employeeId = emp.id ;
           return emp.employee_Name ;
       }
       this.fine.employeeId = null;
       return emp;

     }
  
/////////////////////////////////////////////////////////////////////

EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.fine.employeeId = this.employee[i].id;
      }
    }


  }
  //Item

  displayItem = (emp: EmployeeDto): any => {
    debugger;

    if (emp instanceof EmployeeDto) {
        this.fine.employeeId = emp.id;
      return emp.employee_Name;
    }
    return emp;

  }



}


// class for keeping all docNo number
class abc{
    public code: string;
  }
