import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { FineComponent } from '../fine.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { FineServiceProxy , FineDto , FineTypeDropdownServiceProxy , FineTypeDropdownDto , EmployeeDropdownDto, EmployeeDropDownServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy, EmployeeDto, EmployeeServiceProxy} from '../../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import * as moment from 'moment';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';

@Component({
  selector: 'edit-fine',
  templateUrl: './edit-fine.component.html',
  styleUrls : ['./edit-fine.component.less'],
  providers: [FineTypeDropdownServiceProxy , EmployeeDropDownServiceProxy , FineServiceProxy , HRConfigurationServiceProxy, EmployeeServiceProxy] 
})
export class EditFineComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editFineModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    public docDate = new Date();
    public fineDate = new Date();

    employee : EmployeeDto[] = [];
    EmpName : string;


    employeeDropDown: EmployeeDropdownDto[] = null;  
    fineTypeDropDown: FineTypeDropdownDto[] = null; 
    
    fine: FineDto = new FineDto();
    _fineValidation : FormGroup

    
    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDropdownDto[]>;

    public empName : string ;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        private fineTypeDropdownServiceProxy: FineTypeDropdownServiceProxy,
        private employeeDropDownServiceProxy: EmployeeDropDownServiceProxy,
        private fineServiceProxy: FineServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder,
        private employeeService: EmployeeServiceProxy
       
    ) {
        //super(injector);

}

//////////////////////////////////////////////////////////////////////////
initValidation() {

    this._fineValidation = this.formBuilder.group({
        fineDate: [null, Validators.required],
        employeeId : [null , Validators.required],
        fineTypeId : [null , Validators.required],
        
    });

}


//check form validation if it is valid then save it else throw error message in form //

/*onType() {

  if (this._fineValidation.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._fineValidation);
  }
}*/
onType() {
        
    if (this._fineValidation.valid) {
        let a, p = 0;
        for (a = 0; a < this.employeeDropDown.length; a++) {
            if (this.fine.employeeId == this.employeeDropDown[a].id) ///check if employee selected matched in list or not
            {
                p = 1;
                this.save();
                break;
            }
        }
        if (p == 0) {

            this.empName = null; // null empName 
            this.validateAllFormFields(this._fineValidation);
            this.employeeDropdownSearch();
        }

    } else {
        this.validateAllFormFields(this._fineValidation);

    }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}



//////////////////////////////////////////////////////////////////////////////

    show(id: number): void{

        this.fineServiceProxy.get(id)
        .finally(() => {
            this.active = true;
            this.modal.show();
        })
        .subscribe((result: FineDto) => {
            this.fine = result;

            this.docDate = this.fine.docDate.toDate();
            this.fineDate = this.fine.fineDate.toDate();

            this.employeeDropDownServiceProxy.get(result.employeeId).subscribe((result)=>{
                this.empName = result.employee_Name ;
            })
        });   

        this.employeeDropdownSearch();
        this._fineValidation.markAsUntouched({onlySelf:true});
    }


      
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }
    

        });

    }

    ngOnInit(): void {
        this.fillEmployeeDropDown();
        this.fillFineTypeDropDown();
        this.initValidation();
        this.getAllHRConfiguration();
        this.employeeService.getAllEmployees()
       .subscribe((result) => {
           this.employee = result;

       })
    }

    fillEmployeeDropDown(){
        this.employeeDropDownServiceProxy.getEmployeeDropdown()
        .subscribe((result) => {
            this.employeeDropDown = result.items;
        });
    }

    fillFineTypeDropDown(){
        this.fineTypeDropdownServiceProxy.getFineTypeDropdown()
        .subscribe((result) => {
            this.fineTypeDropDown = result.items;
        });
    }

    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    doTextareaValueChange(ev){
        try {
          this.fine.detail = ev.target.value;
        } catch(e) {
          console.info('could not set textarea-value');
        }
    }

    notify(){
        swal({
            title: "Success!",
            text: "Record Updated Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }

    save(): void {
        this.saving = true;
        this.fine.docDate = moment(this.docDate).add(5,'hour');
        this.fine.fineDate = moment(this.fineDate).add(5,'hour');

        this.fineServiceProxy.update(this.fine)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
    }

 ////////////////////////////////Filter Method////////////////////////////////////////////////////////

 public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    );

  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
  }


  displayEmployee = (emp: EmployeeDto):any => {
    debugger;

    if(emp instanceof EmployeeDto)
    {
        this.fine.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.fine.employeeId = null;
    return emp;

  }


  EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.fine.employeeId = this.employee[i].id;
      }
    }
  }
  //Item

  displayItem = (emp: EmployeeDto): any => {
    debugger;

    if (emp instanceof EmployeeDto) {
        this.fine.employeeId = emp.id;
      return emp.employee_Name;
    }
    return emp;

  }

  addSearchDD(){
    this.searchEditEmployeeDropdownModel.show()
  }
  
/////////////////////////////////////////////////////////////////////



}

