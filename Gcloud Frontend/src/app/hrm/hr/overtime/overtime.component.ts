import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
import PerfectScrollbar from 'perfect-scrollbar';
import { AddOvertimeComponent } from './add-overtime/add-overtime.component';

import { OvertimeDto, OvertimeServiceProxy, CostCenterDto, CostCenterServiceProxy, DepartmentDto, DepartmentServiceProxy, EmployeeDto, EmployeeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { Moment } from 'moment';
import { EditOvertimeComponent } from '@app/hrm/hr/overtime/edit-overtime/edit-overtime.component';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


declare const $: any;

@Component({
    selector: 'overtime',
    templateUrl: 'overtime.component.html',
    providers: [OvertimeServiceProxy, CostCenterServiceProxy, DepartmentServiceProxy, EmployeeServiceProxy]
})

export class OvertimeComponent implements OnInit {

    overTime: OvertimeDto = new OvertimeDto();
    costCenter: CostCenterDto[];
    department: DepartmentDto[];
    employee: EmployeeDto[];

    empId: number;
    costCenterId: number;
    departmentId: number;
    ovtMonth: Moment;

    _overtime: OvertimeDto[];

    _overtimeEdit: OvertimeDto[];

    cDisplayData: fCalendarData[] = [];

    _overtimeEditValidation : FormGroup


    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;
  
    public empName : string ;
    public costCenterName : string ;
    _overtimeCheckValidation : FormGroup;

    @ViewChild('addOvertimeModal') addOvertimeModal: AddOvertimeComponent;
    @ViewChild('editOvertimeModal') editOvertimeModal: EditOvertimeComponent;
    

    constructor(private overTimeService: OvertimeServiceProxy, private costCenterService: CostCenterServiceProxy,
        private departmentService: DepartmentServiceProxy, private employeeService: EmployeeServiceProxy ,
        private formBuilder : FormBuilder,
        private _router: Router
    ) { }

    //get all cost center
    getAllCostCenter() {
        this.costCenterService.getAllCostCenters().subscribe((result) => {

            this.costCenter = result;
            //this.costCenterDropdownSearch();
        })
    }

    //get all department
    getAllDepartment() {
        this.departmentService.getAllDepartments().subscribe((result) => {
            this.department = result;
        })
    }

    //get all employee
    getAllEmployee() {
        this.employeeService.getAllEmployees().subscribe((result) => {
            debugger;
            this.employee = result;
            this.employeeDropdownSearch();
        })
    }


    resetInput(){

        this.overTime = new OvertimeDto();
        this.empName = null;
        this.employeeDropdownSearch();
        
        //this.costCenterDropdownSearch();
        //this._overtimeCheckValidation.markAsUntouched({onlySelf:true});

        this._overtimeEditValidation.markAsUntouched({onlySelf:true});
       
    }



    renderdSearch(_overtime : OvertimeDto[]){

        let p;
        const $calendar = $('#fullCalendar');
        for (p = 0; p < this._overtime.length; p++) {

            let xyz;
            let abc;
            xyz = new fCalendarData();
            xyz.title = this._overtime[p].overTimerHour +"hrs " + "-" + this._overtime[p].employee.employee_Name;
            //abc = this._overtime[p].overTimeMonth;
            xyz.start = this._overtime[p].overTimeDate;
            //xyz.name = this._overtime[p].employee.employee_Name;
           // this.cDisplayData.push(xyz);
            $calendar.fullCalendar('renderEvent', xyz, true);
           
        }

    }



    checkOverTime() {

        debugger;
        this._overtimeEditValidation.markAsUntouched({onlySelf:true});
        const $calendar = $('#fullCalendar');
        $calendar.fullCalendar('removeEvents');
        this.empId = this.overTime.employeeId;
        this.costCenterId = this.overTime.costCenterId;
        this.departmentId = this.overTime.departmentId;

        this.ovtMonth = this.overTime.overTimeDate;

        this.overTimeService.getOvertime(this.empId, this.costCenterId, this.departmentId, this.ovtMonth)
            .subscribe((result) => {
                debugger;

                this._overtime = result;
                this.renderdSearch(this._overtime);
            });
           // this.employeeDropdownSearch();
        /*let p;

        for (p = 0; p < this._overtime.length; p++) {

            let xyz;
            let abc;
            xyz = new fCalendarData();
            abc = new dte();
            xyz.title = this._overtime[p].overTimerHour;
            abc = this._overtime[p].overTimeMonth;//.toDate().getDate();
            xyz.start = abc;
            //this.cDisplayData.push(xyz);
            $calendar.fullCalendar('renderEvent', xyz, true);

        }*/

       /* $('#calendar').fullCalendar({
            events: [
              {
                title: 'My Event',
                start: new Date(2018,12,12),
                description: 'This is a cool event'
              }
              // more events here
            ],
            eventRender: function(event, element) {
              element.qtip({
                content: event.description
              });
            }
          });
        */

        //this._overtime = [];
        
    }

///////////////////////////////////////

/*
initValidationCheck() {

    this._overtimeEditValidation = this.formBuilder.group({
        costCenterId :[null, Validators.required]
    });

}*/



initValidation() {

    this._overtimeEditValidation = this.formBuilder.group({
        employeeId: [null, Validators.required],
       // departmentId: [null, Validators.required],
        overTimeDate :[null, Validators.required],
        //costCenterId :[null, Validators.required]
    });

}

/*onType() {
  
    if (this._overtimeEditValidation.valid) {
        this.EditOvertime();
    } else {
        this.validateAllFormFields(this._overtimeEditValidation);
    }
  }*/

  onType() {
 
    if (this._overtimeEditValidation.valid) {
    
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.empName == this.employee[a].employee_Name) ///check if employee selected matched in list or not
        {
          p=1;
          this.EditOvertime();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
      
        this.validateAllFormFields(this._overtimeEditValidation);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._overtimeEditValidation);
       
    }
  }

  /*onType1() {
 
    if(this.costCenterName!=null){
        debugger;
        let j=0;
        for(j=0;j<this.costCenter.length;j++){

            if(this.overTime.costCenterId == this.costCenter[j].id){
                this.checkOverTime();
                break;
            }
            else{

                this.costCenterName = null; // 
                this.validateAllFormFields(this._overtimeCheckValidation);
                this.costCenterDropdownSearch();
            }
        }
    }

    else{
        this.checkOverTime();
    }
      
  }*/


  
  validateAllFormFields(formGroup: FormGroup) {
  
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
  }
  globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
        if (!this.globalFunction.hasPermission("View", "Overtime")) {
            this.globalFunction.showNoRightsMessage("View");
            this._router.navigate([''])
          }

        this.initValidation();

        this.getAllCostCenter();
        this.getAllDepartment();
        this.getAllEmployee();

        const today = new Date();
        const y = today.getFullYear();
        const m = today.getMonth();
        const d = today.getDate();


        const $calendar = $('#fullCalendar');
        $calendar.fullCalendar({
            
            viewRender: function (view: any, element: any) {
                debugger
                // We make sure that we activate the perfect scrollbar when the view isn't on Month
                if (view.name != 'month') {
                    var elem = $(element).find('.fc-scroller')[0];
                    let ps = new PerfectScrollbar(elem);
                }
            },
              /* to update data on clickk
            eventClick: function(calEvent, jsEvent) {
                
                debugger;
                var title = prompt('Event Title:', calEvent.title);
        
                if (title) {
                    calEvent.title = title;
                    $('#calendar').fullCalendar('updateEvent', calEvent);
                }
            },*/
                //////////////////////////////////////
            header: {
                left: 'title',
                center: 'month, agendaWeek, agendaDay',
                right: 'prev, next, today'
            },

            buttonText: {
                month: 'Monthly', 
                agendaDay: 'Daily', 
                agendaWeek: 'Weekly', 
                today: 'Today'
             },



            defaultDate: today,
            selectable: true,
            selectHelper: true,
            views: {
                month: { // name of view
                    titleFormat: 'MMMM YYYY'
                    // other view-specific options here
                },
                week: {
                    titleFormat: ' MMMM D YYYY'
                },
                day: {
                    titleFormat: 'D MMM, YYYY'
                }
            },

            

            /*select: function (start: any, end: any) {

                // on select we show the Sweet Alert modal with an input
                swal({
                    title: 'Add Overtime',
                    html: '<div class="row">' +
                        '<div class="col-md-6"><div class="form-group">' +
                        '<input class="form-control" placeholder="Overtime Hours" id="input-field">' +
                        '</div></div>'+
                        '</row>',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function (result: any) {
                    debugger
                    let eventData;
                    const event_title = $('#input-field').val();
                    //const event_title1 = $('#input-field').val();

                    if (event_title) {
                        eventData = {
                            title: event_title,
                            start: start,
                            end: end
                        };
                        
                        $calendar.fullCalendar('renderEvent', eventData, true); // stick? = true
                      
                 }
                    $calendar.fullCalendar('unselect');

                });
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events


            // color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
            /* events: [
                 {
                     title: 'All Day Event',
                     
                     start: new Date(y, m, 1),
     
                     //start: new Date(dteY , dteM , dteY),
     
                     className: 'event-default'
                 }
                 
             ]*/
        });
        
    }



    ///////////////////////////////Search Code//////////////////////////////////
  public employeeDropdownSearch(){
  
    this.filteredOptions = this.myControl.valueChanges
     .pipe(
       startWith<string | EmployeeDto >(''),
       map(value => typeof value === 'string' ? value : value.employee_Name),
       map(name => name ? this._filter(name) : this.employee.slice())
     );
   }
   
   private _filter(value: string): EmployeeDto[]{
   
    
     const filterValue = value.toLowerCase();
   
     return this.employee.filter(op=>op.employee_Name.toLowerCase().includes(filterValue));
   }
   
   displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.overTime.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.overTime.employeeId = undefined;
    return emp;
  
  }
   
    ///////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////S//////////////////////////////////
/*   
    myControl1 = new FormControl();

    filteredOption1: Observable<CostCenterDto[]>;
   
    public costCenterDropdownSearch(){
  
        this.filteredOption1= this.myControl1.valueChanges
         .pipe(
           startWith<string | CostCenterDto >(''),
           map(value => typeof value === 'string' ? value : value.name),
           map(name => name ? this._filter1(name) : this.costCenter.slice())
         );
       }
       
       private _filter1(value: string): CostCenterDto[]{
       
        
         const filterValue = value.toLowerCase();
       
         return this.costCenter.filter(op=>op.name.toLowerCase().includes(filterValue));
       }
       
       displayCostCenter = (costCntr: CostCenterDto):any => {
        debugger;
        
        if(costCntr instanceof CostCenterDto)
        {
            this.overTime.costCenterId = costCntr.id ;
            return costCntr.name;
        }
        this.overTime.costCenterId = undefined;
        return costCntr;
      
      }
       
     ///////////////////////////////////////////////////////////////////////////////////////
  */  


    AddOvertime(): void {
        if (!this.globalFunction.hasPermission("Create", "Overtime")) {
            this.globalFunction.showNoRightsMessage("Create");
            return
          }
        debugger;
        this.addOvertimeModal.show();
    }


    EditOvertime():void{
        if (!this.globalFunction.hasPermission("Edit", "Overtime")) {
            this.globalFunction.showNoRightsMessage("Edit");
            return
          }

        debugger;
        this.overTimeService.getOvertime(this.overTime.employeeId, this.overTime.costCenterId,
            this.overTime.departmentId, this.overTime.overTimeDate).subscribe((result) => {

                this._overtimeEdit = result;
                this.editOvertimeModal.show(this._overtimeEdit); //passing filtered data to show method  to display in popup 
            })
    }

}
class fCalendarData {

    public title: string;
    public start: Moment;
}

