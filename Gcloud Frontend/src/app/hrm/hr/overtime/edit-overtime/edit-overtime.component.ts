import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';

import { OvertimeComponent } from '../overtime.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
//import { OvertimeDto } from '@app/shared/service-proxies/service-proxies';
import { OvertimeDto, OvertimeServiceProxy, CostCenterDto, CostCenterServiceProxy, DepartmentDto, DepartmentServiceProxy, EmployeeDto, EmployeeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ChildEditOvertimeComponent } from '@app/hrm/hr/overtime/edit-overtime/child-edit-overtime/child-edit-overtime.component';


declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: string[][];
  }


@Component({
  selector: 'edit-overtime',
  templateUrl: './edit-overtime.component.html',
  providers:[OvertimeServiceProxy]

})
export class EditOvertimeComponent implements OnInit {

    @ViewChild('editOvertimeModal') modal: ModalDirective;
   
    @ViewChild('childEditOvertimeModal') childEditOvertimeModal : ChildEditOvertimeComponent;

    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    public dataTable : DataTable ;

    active: boolean = false;
    saving: boolean = false;

    data : string[] =[];

    constructor(
        injector: Injector, private overTimeService : OvertimeServiceProxy
        // private _userService: UserServiceProxy
       
    ) {
        //super(injector);
    }
  

    show(overTime : OvertimeDto[]): void{

        debugger;
        this.active = true;
        this.initDataTable();
        this.fillDataTable(overTime);
        this.modal.show();
        
    }


    initDataTable() {

        this.dataTable = {
          headerRow: ['Employee Name', 'Cost Center', 'Overtime Hour','Actions'],
          footerRow: [/*'Doc No', 'Doc Date', 'Transfer Date', 'Employee', 'Transfer Type', 'Transfer From', 'Transfer To', 'Actions'*/],
    
          dataRows: []
        };
    
      }
    
    
      fillDataTable(overTime : OvertimeDto[]) {
    
        let i;
        for (i = 0; i < overTime.length; i++) {
    
          this.data.push(overTime[i].employee.employee_Name)
          
          this.data.push(overTime[i].costCenter.name)
          
          this.data.push(overTime[i].overTimerHour.toString())
          
          this.data.push(overTime[i].id.toString())
    
          this.dataTable.dataRows.push(this.data)
    
          this.data = [];
    
        }
      }
    
    ngOnInit(): void {

      this.initDataTable();
    }


    EditOvertime(id: string): void {
    
        this.childEditOvertimeModal.show(parseInt(id));
        this.close();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    protected delete(id: string): void {
        swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this Request!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it',
          confirmButtonClass: "btn btn-success",
          cancelButtonClass: "btn btn-danger",
          buttonsStyling: false
        }).then((result) => {
          if (result.value) {
            this.overTimeService.delete(parseInt(id))
              .finally(() => {
               
                this.modal.hide();
              })
              .subscribe(() => {
                debugger
                swal({
                  title: 'Deleted!',
                  text: 'Overtime has been deleted.',
                  type: 'success',
                  confirmButtonClass: "btn btn-info",
                  buttonsStyling: false
                }).catch(swal.noop)
              });
          } else {
            swal({
              title: 'Cancelled',
              text: 'Overtime Deletion is safe :)',
              type: 'error',
              confirmButtonClass: "btn btn-info",
              buttonsStyling: false
            }).catch(swal.noop)
          }
        })
      }

}
