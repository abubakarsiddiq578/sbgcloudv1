import { Component, OnInit , Injector, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { OvertimeDto, OvertimeServiceProxy, CostCenterDto, CostCenterServiceProxy, DepartmentDto, DepartmentServiceProxy, EmployeeDto, EmployeeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'child-edit-overtime',
  templateUrl: './child-edit-overtime.component.html',
  styleUrls: ['./child-edit-overtime.component.scss'],
  providers: [OvertimeServiceProxy, CostCenterServiceProxy, DepartmentServiceProxy, EmployeeServiceProxy]
})
export class ChildEditOvertimeComponent implements OnInit {

  active: boolean = false;
  saving: boolean = false;

  overTime: OvertimeDto = new OvertimeDto();

  costCenter: CostCenterDto[];
  department: DepartmentDto[];
  employee: EmployeeDto[];

  _overTimeValidation: FormGroup; // for form validation 


  @ViewChild('childEditOvertimeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();


  constructor(injector: Injector, private overTimeService: OvertimeServiceProxy, private costCenterService: CostCenterServiceProxy,
    private departmentService: DepartmentServiceProxy, private employeeService: EmployeeServiceProxy, private formBuilder: FormBuilder
  ) { }


  //get all cost center
  getAllCostCenter() {
    this.costCenterService.getAllCostCenters().subscribe((result) => {

      this.costCenter = result;
    })
  }

  //get all department
  getAllDepartment() {
    this.departmentService.getAllDepartments().subscribe((result) => {
      this.department = result;
    })
  }

  //get all employee
  getAllEmployee() {
    this.employeeService.getAllEmployees().subscribe((result) => {
      debugger;
      this.employee = result;
    })
  }


  ngOnInit() {

    this.getAllEmployee();
    this.getAllCostCenter();
    this.getAllDepartment();
    this.initValidation();
  }


  initValidation() {

    this._overTimeValidation = this.formBuilder.group({
      employeeId: [null, Validators.required],
      costCenterId: [null, Validators.required],
      departmentId: [null, Validators.required],
      overTimeMonth: [null, Validators.required]
    });

  }

  //check form validation if it is valid then save it else throw error message in form //
  onType() {

    if (this._overTimeValidation.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._overTimeValidation);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  
  show(id: number): void {

    
    this.overTimeService.get(id).finally(() => {

      this.active = true;
      this.modal.show();

    }).subscribe((result: OvertimeDto) => {
      this.overTime = result;
    });

  }


  onShown(): void {

  }


  save(): void {

    this.saving = true;

    this.overTimeService.update(this.overTime).finally(() => {

      this.saving = false;
     

    }).subscribe(() => {

      this.notify();
      this.close();
      this.modalSave.emit(null);

    })
  }

  
  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

}
