import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AllowanceTypeDto, AllowanceTypeServiceProxy } from '../../../../shared/service-proxies/service-proxies';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
@Component({
  selector: 'add-allowance-type',
  templateUrl: './add-allowance-type.component.html',
})
export class AddAllowanceTypeComponent implements OnInit {

  @ViewChild('addAllowanceTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  AllowanceType: AllowanceTypeDto = new AllowanceTypeDto();

  _allowanceType : FormGroup

  constructor(
      injector: Injector,
      // private _userService: UserServiceProxy
      private _AllowanceTypeService: AllowanceTypeServiceProxy,
      private formBuilder : FormBuilder
     
  ) {
      // super(injector);
  }


  show(): void{

    this.active = true; 
    this.modal.show();
    this.AllowanceType = new AllowanceTypeDto();
    this.AllowanceType.init({ isActive: true });
    
    this._allowanceType.markAsUntouched({onlySelf:true});
      
  }

   //to initialize fields on which validations are being applied
   initValidation() {

    this._allowanceType = this.formBuilder.group({
        allowanceTitle: [null, Validators.required],
        allowanceAmount: [null , Validators.required]
    });

}

//check form validation if it is valid then save it else throw error message in form //
onType() {

  if (this._allowanceType.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._allowanceType);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}



  ngOnInit(): void {
      this.initValidation();
  }


  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  
  close(): void {
      this.active = false;
      this.modal.hide();
  }

  save(): void {
    
  this.saving = true;
  this._AllowanceTypeService.create(this.AllowanceType)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
          //this.notify.info(this.l('Saved Successfully'));
          this.close();   
          this.modalSave.emit(null);
      });
    }
}
