import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AllowanceTypeDto, AllowanceTypeServiceProxy } from '../../../../shared/service-proxies/service-proxies';
import { AllowanceTypeComponent } from '../allowance-type.component';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'edit-allowance-type',
  templateUrl: './edit-allowance-type.component.html',
})
export class EditAllowanceTypeComponent implements OnInit {

  @ViewChild('editAllowanceTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  AllowanceType: AllowanceTypeDto = new AllowanceTypeDto();

  _allowanceType : FormGroup

  constructor(
      injector: Injector,
      private _AllowanceTypeService: AllowanceTypeServiceProxy,
      private formBuilder : FormBuilder
     
      // private _userService: UserServiceProxy
     
  ) {
      // super(injector);
  }

  //to initialize fields on which validations are being applied
  initValidation() {

    this._allowanceType = this.formBuilder.group({
        allowanceTitle: [null, Validators.required],
        allowanceAmount: [null , Validators.required]
    });

}

//check form validation if it is valid then save it else throw error message in form //
onType() {

  if (this._allowanceType.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._allowanceType);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}


  show(id:number): void{
      
    this._AllowanceTypeService.get(id)
    .finally(() => {
        this.active = true;
        this.modal.show(); 
    })
    .subscribe((result: AllowanceTypeDto) => {
        this.AllowanceType = result;
    });      
    this.active = true;
    this.modal.show();
  }

  ngOnInit(): void {
      this.initValidation();
  }


  onShown(): void {
     //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  
  close(): void {
      this.active = false;
      this.modal.hide();
  }

  save(): void {
    this.saving = true;
    this._AllowanceTypeService.update(this.AllowanceType)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
        //this.notify.info(this.l('Record Updated Successfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }


}
