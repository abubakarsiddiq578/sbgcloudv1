import { Component, OnInit, ViewChild, AfterViewInit, Injector } from '@angular/core';
import { EditAllowanceTypeComponent } from './edit-allowance-type/edit-allowance-type.component';
import { AddAllowanceTypeComponent } from './add-allowance-type/add-allowance-type.component';
import swal from 'sweetalert2';
import { AllowanceTypeServiceProxy, AllowanceTypeDto } from '../../../shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';


declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;


@Component({
  selector: 'allowance-type',
  templateUrl: './allowance-type.component.html',
  providers: [AllowanceTypeServiceProxy]
})
export class AllowanceTypeComponent implements OnInit, AfterViewInit {

  @ViewChild('addAllowanceTypeModal') addAllowanceTypeModal: AddAllowanceTypeComponent;
  @ViewChild('editAllowanceTypeModal') editAllowanceTypeModal: EditAllowanceTypeComponent;

  public dataTable: DataTable;
  result: AllowanceTypeDto[] = [];
  data: string[] = [];

  constructor(injector: Injector,
    //  private _authService: AppAuthService,
    private _AllowanceTypeService: AllowanceTypeServiceProxy
    , private _router: Router) { }


  globalFunction: GlobalFunctions = new GlobalFunctions
  
  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "AllowanceType")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);

    }
    this.getAllRecord();
  }

  getAllRecord(): void {
    this._AllowanceTypeService.getAllAllowanceTypes()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        this.result = result;
        this.paging();
        this.getAllData();

      });
  }

  paging(): void {
    this.dataTable = {
      headerRow: ['Allowance Title', 'Allowance Amount', 'Detail', 'Sort Order', 'Actions'],
      footerRow: ['Allowance Title', 'Allowance Amount', 'Detail', 'Sort Order', 'Actions'],

      dataRows: [
        // ['1234', 'Manager', 'Develop', ''],
        // ['1234', 'Manager', 'Develop', 'btn-round'],
        // ['1234', 'Manager', 'Develop', 'btn-simple']
      ]

    };
  }
  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].allowanceTitle);
      this.data.push(this.result[i].allowanceAmmount.toString());
      this.data.push(this.result[i].allowanceDetail);
      this.data.push(this.result[i].sortOrder.toString());
      this.data.push(this.result[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }


  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      e.preventDefault();
    });

    // //Like record
    // table.on('click', '.like', function(e) {
    //   alert('You clicked on Like button');
    //   e.preventDefault();
    // });

    $('.card .material-datatables label').addClass('form-group');
  }
  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "AllowanceType")) {
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Allowance Type!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this._AllowanceTypeService.delete(parseInt(id))
          .finally(() => {

            this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Allowance Type has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Allowance Type is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })

  }


  AddAllowanceType(): void {
    if (!this.globalFunction.hasPermission("Create", "AllowanceType")) {
      this.globalFunction.showNoRightsMessage("Create")
      return
    }
    this.addAllowanceTypeModal.show();
  }

  EditAllowanceType(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "AllowanceType")) {
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    this.editAllowanceTypeModal.show(parseInt(id));
  }

}
