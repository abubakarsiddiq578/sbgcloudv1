import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { BonusDto, BonusServiceProxy, BonusTypeDto, EmployeeDto, EmployeeServiceProxy, BonusTypeServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';


@Component({
  selector: 'edit-bonus',
  templateUrl: './edit-bonus.component.html',
  providers:[BonusServiceProxy , HRConfigurationServiceProxy , HRConfigurationServiceProxy , EmployeeServiceProxy , BonusTypeServiceProxy]
})
export class EditBonusComponent implements OnInit {

    @ViewChild('editBonusModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    Bonus: BonusDto = new BonusDto();
    BonusType:BonusTypeDto
    employee: EmployeeDto[];

    
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;
    

  constructor(
    injector: Injector,
    private _BonusService: BonusServiceProxy,
    private _BonusTypeService: BonusTypeServiceProxy,
    private _employeeService: EmployeeServiceProxy,
    private _hrConfigService: HRConfigurationServiceProxy
     
  ) {
      // super(injector);
  }
  show(id:number): void{
      

    this._BonusService.get(id)
    .finally(() => {
        this.active = true;
        this.modal.show(); 
    })
    .subscribe((result: BonusDto) => {
        this.Bonus = result;
    });      
  }

  ngOnInit(): void {
    this._BonusTypeService.getAllBonusTypes()
    .finally(() => { console.log('completed') })
    .subscribe((result: any) => {
        this.BonusType = result;

    })

this._employeeService.getAllEmployees()
    .finally(() => { console.log('completed') })
    .subscribe((result: any) => {
        this.employee = result;
        
    })

    this.getAllHRConfiguration();
    

  }


  
getAllHRConfiguration(){

    this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
    
      this.hrConfig = result[0] ;

      
      if(this.hrConfig.autoCode == true){

        this.isAutoCode = true ;
      }
      else if(this.hrConfig.autoCode == false){
        this.isAutoCode = false;
      }


    });
    
    }




  onShown(): void {
     //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  
  close(): void {
      this.active = false;
      this.modal.hide();
  }

  save(): void {
    this.saving = true;
    this._BonusService.update(this.Bonus)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
        //this.notify.info(this.l('Record Updated Successfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }
}

