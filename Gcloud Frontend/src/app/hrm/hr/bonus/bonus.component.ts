// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddBonusComponent } from './add-bonus/add-bonus.component';
import { EditBonusComponent } from './edit-bonus/edit-bonus.component';
import { BonusServiceProxy, BonusDto, EmployeeServiceProxy, BonusTypeServiceProxy, BonusTypeDto, EmployeeDto } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'bonus',
    templateUrl: 'bonus.component.html',
    providers: [BonusServiceProxy, EmployeeServiceProxy, BonusTypeServiceProxy]
})

export class BonusComponent implements OnInit, AfterViewInit {

    @ViewChild('addBonusModal') addBonusModal: AddBonusComponent;
    @ViewChild('editBonusModal') editBonusModal: EditBonusComponent;

    public dataTable: DataTable;
    data: string[] = [];

    result: BonusDto[] = [];
    bonusType: BonusTypeDto[] = [];
    employee: EmployeeDto[] = [];

    constructor(injector: Injector,
      private _BonusService: BonusServiceProxy,
      private _router: Router
      
    ) { }

    globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "Bonus")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate(['']);
      }
      this.getAllRecord();
    }
  
    getAllRecord(): void {
      this._BonusService.getAllBonus()
        .finally(() => {
          console.log('completed');
        })
        .subscribe((result: any) => {
          this.result = result;
          this.paging();
          this.getAllData();
  
        });
    }
  
    paging(): void {
        this.dataTable = {
          headerRow: [ 'Doc No', 'Doc Date', 'Bonus Date', 'Employee', 'Bonus Type', 'Bonus Percentage', 'Actions' ],
            footerRow: [ 'Doc No', 'Doc Date', 'Bonus Date', 'Employee', 'Bonus Type', 'Bonus Percentage', 'Actions' ],

          dataRows: [
              // ['1234', 'Manager', 'Develop', ''],
              // ['1234', 'Manager', 'Develop', 'btn-round'],
              // ['1234', 'Manager', 'Develop', 'btn-simple']
          ]
  
      };
    }
    getAllData(): void {
      let i;
      for (i = 0; i < this.result.length; i++) {
        this.data.push(this.result[i].documentNo);
        this.data.push(this.result[i].documentDate.toString());
        this.data.push(this.result[i].bonusDate.toString());
        this.data.push(this.result[i].employeeId.toString());
        this.data.push(this.result[i].bonusTypeId.toString());
        this.data.push(this.result[i].percentage.toString());
        this.data.push(this.result[i].id.toString());
  
        this.dataTable.dataRows.push(this.data);
        this.data = [];
      }
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      $('.card .material-datatables label').addClass('form-group');
    }

    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "Bonus")) {
        this.globalFunction.showNoRightsMessage("Delete")
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Bonus!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this._BonusService.delete(parseInt(id))
            .finally(() => {
  
              this.getAllRecord();
            })
            .subscribe(() => {
              swal({
                title: 'Deleted!',
                text: 'Your Bonus has been deleted.',
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
              }).catch(swal.noop)
            });
        } else {
          swal({
            title: 'Cancelled',
            text: 'Your Bonus is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
          }).catch(swal.noop)
        }
      })
      
    }

    AddBonus(): void {
      if (!this.globalFunction.hasPermission("Create", "Bonus")) {
        this.globalFunction.showNoRightsMessage("Create")
        return
      }
      this.addBonusModal.show();
      }

      EditBonus(id: string): void {
        if (!this.globalFunction.hasPermission("Edit", "Bonus")) {
          this.globalFunction.showNoRightsMessage("Edit")
          return
        }
        debugger
        this.editBonusModal.show(parseInt(id));
        }

}


