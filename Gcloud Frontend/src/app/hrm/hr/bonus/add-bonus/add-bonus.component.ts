import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';
import { BonusDto, BonusTypeDto, BonusServiceProxy, EmployeeDto, EmployeeServiceProxy, BonusTypeServiceProxy , HRConfigurationDto , HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'add-bonus',
  templateUrl: './add-bonus.component.html',
  providers:[BonusServiceProxy , HRConfigurationServiceProxy , EmployeeServiceProxy , BonusTypeServiceProxy ]
})
export class AddBonusComponent /*extends AppComponentBase*/ implements OnInit {

    
    @ViewChild('addBonusModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    Bonus: BonusDto = new BonusDto();
    BonusType:BonusTypeDto
    BonusTypeDDList = []
    employee: EmployeeDto[];

    _bonus  : BonusDto[]; //for getting disable docNo Value from  resignation table
    _docNo : abc[] =[]; // for getting auto document number

    _bonusValidation : FormGroup;
    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;
  
    public empName : string ;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;
    

  constructor(
      injector: Injector,
      private _BonusService: BonusServiceProxy,
      private _BonusTypeService: BonusTypeServiceProxy,
      private _BonusTypeListService: BonusTypeServiceProxy,
      private _employeeService: EmployeeServiceProxy,
      private formBuilder : FormBuilder ,
      private _hrConfigService: HRConfigurationServiceProxy
  ) {
      // super(injector);
  }
  show(): void{
      
    this.active = true; 
    this.modal.show();
    this.Bonus = new BonusDto();
    this._BonusService.getAllBonus().subscribe((result)=>{

        this._bonus = result;

        if(this.hrConfig.autoCode == true){

            this.getAutoDocNumber();
            this.isAutoCode = true ;
          }
          else if(this.hrConfig.autoCode == false){
            this.isAutoCode = false;
          }

        
    })

    this.empName = null;
    this.employeeDropdownSearch();
    this.Bonus.init({ isActive: true });
  }

  ngOnInit(): void {

    this.initValidation();

    this._BonusTypeListService.getAll(0, 1000)
    .subscribe((result) => {
      this.BonusTypeDDList = result.items;
  });
//end getting dropdown list for Deduction Type List

this._BonusTypeService.getAllBonusTypes()
    .finally(() => { console.log('completed') })
    .subscribe((result: any) => {
        this.BonusType = result;

    })

this._employeeService.getAllEmployees()
    .finally(() => { console.log('completed') })
    .subscribe((result: any) => {
        this.employee = result;
        
    })

    this.getAllHRConfiguration();

  }


  initValidation() {

    this._bonusValidation = this.formBuilder.group({
        employeeId: [null, Validators.required],
        bonusTypeId: [null , Validators.required],
        bonusDate: [null , Validators.required]
    });

}

/*onType() {

  if (this._advnceReq.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._advnceReq);
  }
}*/
onType() {
 
  if (this._bonusValidation.valid) {
  
    let a,p=0 ;
    for(a=0;a<this.employee.length;a++){
      if(this.Bonus.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.empName = null; // null empName 
    
      this.validateAllFormFields(this._bonusValidation);
      this.employeeDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._bonusValidation);
     
  }
}


validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}
////////////////////////////////////////////////////////////////////


  ///////////////////////////////Search Code//////////////////////////////////
  public employeeDropdownSearch(){
  
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }
  
  private _filter(value: string): EmployeeDto[]{
  
   
    const filterValue = value.toLowerCase();
  
    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Bonus.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.Bonus.employeeId = null;
    return emp;
  
  }

  getAllHRConfiguration(){

    this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
    
      this.hrConfig = result[0] ;
    });
    
    }



///////////////////////////////////////////////////////////////////////////////////////
///////////////Auto Code///////////////


  //this function generate auto doc number

   //this function generate auto doc number
   getAutoDocNumber():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._bonus.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._bonus[i].documentNo ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0  ){
      
        debugger;
        code = "1";
        this.Bonus.documentNo = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                let x;
                code = this._docNo[this._docNo.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.Bonus.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Bonus.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Bonus.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.Bonus.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Bonus.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Bonus.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.Bonus.documentNo = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.Bonus.documentNo =  "0" + code;
                  }
                  else if(code){
  
                      this.Bonus.documentNo =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.Bonus.documentNo = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.Bonus.documentNo =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.Bonus.documentNo =  temp[0] ;
                  }

                }



                
        }                     
    }
    this._docNo = [];
}




/////////////////End of auto code/////////////////
  onShown(): void {
     //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  
  close(): void {
      this.active = false;
      this.modal.hide();
  }

  save(): void {
    this.saving = true;
    this._BonusService.create(this.Bonus)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
        //this.notify.info(this.l('Record Updated Successfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }
}


// class for keeping all docNo number
class abc{
    public code: string;
  }
  
  