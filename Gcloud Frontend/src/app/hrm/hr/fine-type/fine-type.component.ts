import { Component, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { EditFineTypeComponent } from './edit-fine-type/edit-fine-type.component';
import { AddFineTypeComponent } from './add-fine-type/add-fine-type.component';
import { FineTypeServiceProxy, FineTypeDto, PagedResultDtoOfCostCenterDto, PagedResultDtoOfFineTypeDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'fine-type',
  templateUrl: './fine-type.component.html',
  providers: [FineTypeServiceProxy]
})
// export class FineTypeComponent extends PagedListingComponentBase<CostCenterDto> {
export class FineTypeComponent implements OnInit {

  @ViewChild('addFineTypeModal') public addFineTypeModal: AddFineTypeComponent;
  @ViewChild('editFineTypeModal') editFineTypeModal: EditFineTypeComponent;

  public dataTable: DataTable;
  result: FineTypeDto[] = [];
  data: string[] = [];

  constructor(injector: Injector,
    //  private _authService: AppAuthService,
    private _fineTypeServiceProxy: FineTypeServiceProxy,private _router : Router
  ) { }


  //   protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
  //     this._fineTypeServiceProxy.getAll(request.skipCount, request.maxResultCount)
  //         .finally(()=>{
  //             finishedCallback();
  //         })
  //         .subscribe((result:PagedResultDtoOfFineTypeDto)=>{
  //     this.result = result.items;
  //     this.dtTrigger.next();
  //     this.showPaging(result, pageNumber);
  //         });
  // }


  // protected delete(entity: FineTypeDto): void {
  //   abp.message.confirm(
  // "Delete Sample '"+ entity.fintTitle +"'?",
  // (result:boolean) => {
  //   if(result) {
  //     this._fineTypeServiceProxy.delete(entity.id)
  //       .finally(() => {
  //             abp.notify.info("Deleted Sample: " + entity.fintTitle );
  //         this.refresh();
  //       })
  //       .subscribe(() => { });
  //   }
  // });
  // }

  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "FineType")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate([''])
    }
    this.getAllRecord();



  }

  getAllRecord(): void {
    this._fineTypeServiceProxy.getAllFineTypes()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        this.result = result;
        this.paging();
        this.getAllData();

      });
  }

  paging(): void {
    this.dataTable = {
      headerRow: ['Fine Title', 'Fine Amount', 'Detail', 'Sort Order', 'Actions'],
      footerRow: ['Fine Title', 'Fine Amount', 'Detail', 'Sort Order', 'Actions'],

      dataRows: [
        //     ['1234', 'Manager', 'Develop', ''],
        //     ['1234', 'Manager', 'Develop', 'btn-round'],
        //     ['1234', 'Manager', 'Develop', 'btn-simple'],
      ]

    };
  }

  getAllData(): void {
    debugger
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].fintTitle);
      this.data.push(this.result[i].fineAmmount.toString());
      this.data.push(this.result[i].fineDetail);
      this.data.push(this.result[i].sortOrder.toString());
      this.data.push(this.result[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }
  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    // table.on('click', '.remove', function(e) {
    //   const $tr = $(this).closest('tr');
    //   table.row($tr).remove().draw();
    //   e.preventDefault();
    // });

    // //Like record
    // table.on('click', '.like', function(e) {
    //   alert('You clicked on Like button');
    //   e.preventDefault();
    // });

    $('.card .material-datatables label').addClass('form-group');


  }

  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "FineType")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Bonus Type!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        debugger
        this._fineTypeServiceProxy.delete(parseInt(id))
          .finally(() => {

            this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Bonus Type has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Bonus Type is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }
  AddFineType(): void {
    if (!this.globalFunction.hasPermission("Create", "FineType")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
    this.addFineTypeModal.show();
  }

  EditFineType(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "FineType")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    this.editFineTypeModal.show(parseInt(id));
  }

}
