import { Component, OnInit, EventEmitter, ElementRef, ViewChild, Output, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FineTypeDto, FineTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
//import { AppComponentBase } from '../../../../shared/ap';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'add-fine-type',
  templateUrl: './add-fine-type.component.html'

})
export class AddFineTypeComponent implements OnInit {

  @ViewChild('addFineTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;
  

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  fineType: FineTypeDto = new FineTypeDto();

  _fineTypeValidation : FormGroup

  constructor(
    injector: Injector,
    private _fineTypeService: FineTypeServiceProxy,
    private formBuilder : FormBuilder
     
  ) {
      //super(injector);
  }


  show(): void{

    this.active = true; 
    this.modal.show();
    this.fineType = new FineTypeDto();
    this.fineType.init({ isActive: true });
      
    this._fineTypeValidation.markAsUntouched({onlySelf:true});
  }

  initValidation() {

    this._fineTypeValidation = this.formBuilder.group({
        fineTitle: [null, Validators.required],
        fineAmount: [null, Validators.required]
      
    });

}

//check form validation if it is valid then save it else throw error message in form //
onType() {

  if (this._fineTypeValidation.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._fineTypeValidation);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}





  ngOnInit(): void {
    this.initValidation();
  }


  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  save(): void {
      

  this.saving = true;
    this._fineTypeService.create(this.fineType)
        .finally(() => { this.saving = false; })
        .subscribe(() => {
            //this.notify.info(this.l('Saved Successfully'));
            this.close();   
            this.modalSave.emit(null);
        });
  }
  
  close(): void {
      this.active = false;
      this.modal.hide();
  }

}
