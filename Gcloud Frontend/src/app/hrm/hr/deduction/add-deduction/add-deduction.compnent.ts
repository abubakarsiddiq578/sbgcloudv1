import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { DeductionComponent } from '../deduction.component';

//import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { DeductionDto, DeductionTypeDto, DeductionServiceProxy, DeductionTypeServiceProxy, EmployeeServiceProxy, EmployeeDto, HRConfigurationDto , HRConfigurationServiceProxy } from '../../../../shared/service-proxies/service-proxies';
import { DeductionTypeComponent } from '../../deduction-type/deduction-type.component';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';
@Component({
    selector: 'add-deduction',
    templateUrl: './add-deduction.component.html',
    styleUrls: ['./add-deduction.component.less'],
     providers:[DeductionServiceProxy , HRConfigurationServiceProxy]


})
export class AddDeductionComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addDeductionModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
    public docDate = new Date();
    public EmpName: string;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    deduction: DeductionDto = new DeductionDto();
    deductionType: DeductionTypeDto = new DeductionTypeDto();
    deductionTypeDDList = [];

    //deductionType: DeductionTypeDto[] = [];
    _deduction: DeductionDto[]; // for getting docNo data from deduction table
    _docNo: abc[] = [];

    employee: EmployeeDto[] = [];

    _deductionValidation: FormGroup

    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName: string;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
        private _deductionService: DeductionServiceProxy,
        private _deductionTypeListService: DeductionTypeServiceProxy,
        private _deductionTypeService: DeductionTypeServiceProxy,
        private _employeeService: EmployeeServiceProxy,
        private formBuilder: FormBuilder,
        private _hrConfigService : HRConfigurationServiceProxy

    ) {
        //super(injector);
    }


    show(): void {
        this.active = true;
        this.modal.show();
        this.deduction = new DeductionDto();
        this.deduction.init({ isActive: true });
        ///////////////////////////////////////////////////////
        this._deductionService.getAllDeductions().subscribe((result) => {

            this._deduction = result;
            if(this.hrConfig.autoCode == true){

                this.getAutoDocNumber();
                this.isAutoCode = true ;
            }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
            }
           // this.getAutoDocNumber();
        })

        this.empName = null;
        this.employeeDropdownSearch();

        this._deductionValidation.markAsUntouched({ onlySelf: true });
    }



    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];
        });

    }



    /////////////////////////Validations//////////////////////////////////////
    initValidation() {

        this._deductionValidation = this.formBuilder.group({
            employeeId: [null, Validators.required],
            deductionTypeId: [null, Validators.required],
            deductionAmount: [null, Validators.required],
            deductionDate: [null, Validators.required]
        });

    }

    //check form validation if it is valid then save it else throw error message in form //
    /*  onType() {
      
        if (this._deductionValidation.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._deductionValidation);
        }
      }*/

    onType() {
        debugger;
        if (this._deductionValidation.valid) {
            debugger;
            let a, p = 0;
            for (a = 0; a < this.employee.length; a++) {
                if (this.deduction.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._deductionValidation);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._deductionValidation);

        }
    }


    validateAllFormFields(formGroup: FormGroup) {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
    /////////////////////////////End of Validation//////////////////////////////////

    ///////////////////////////////search Method//////////////////////////////////
    public employeeDropdownSearch() {
        debugger;
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith<string | EmployeeDto>(''),
                map(value => typeof value === 'string' ? value : value.employee_Name),
                map(name => name ? this._filter(name) : this.employee.slice())
            );

    }

    private _filter(value: string): EmployeeDto[] {

        debugger;
        const filterValue = value.toLowerCase();

        return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
    }


    displayEmployee = (emp: EmployeeDto): any => {
        debugger;

        if (emp instanceof EmployeeDto) {
            this.deduction.employeeId = emp.id;
            return emp.employee_Name;
        }
        this.deduction.employeeId = null;
        return emp;

    }

    ////////////////////////////////End of Search Method/////////////////////////////////////

    getAutoDocNumber():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._deduction.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._deduction[i].docNo ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0  ){
          
            debugger;
            code = "1";
            this.deduction.docNo = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    let x;
                    code = this._docNo[this._docNo.length-1].code ;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.deduction.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.deduction.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.deduction.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.deduction.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.deduction.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.deduction.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.deduction.docNo = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.deduction.docNo =  "0" + code;
                      }
                      else if(code){
      
                          this.deduction.docNo =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.deduction.docNo = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.deduction.docNo =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.deduction.docNo =  temp[0] ;
                      }
    
                    }
                    
            }                     
        }
        this._docNo = [];
    }


    save(): void {
        //TODO: Refactor this, don't use jQuery style code

        debugger
        this.saving = true;
        this._deductionService.create(this.deduction)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                // this.notify.info(this.l('Saved Successfully'));
                this.close();
                this.modalSave.emit(null);

            });
    }

    ngOnInit(): void {
        //start getting dropdown list for Deduction Type List 
        this._deductionTypeListService.getAll(0, 1000)
            .subscribe((result) => {
                this.deductionTypeDDList = result.items;
            });
        //end getting dropdown list for Deduction Type List

        this._deductionTypeService.getAllDeductionTypes()
            .finally(() => { console.log('completed') })
            .subscribe((result: any) => {
                this.deductionType = result;

            })

        this._employeeService.getAllEmployees()
            .finally(() => { console.log('completed') })
            .subscribe((result: any) => {
                this.employee = result;

            })

        this.initValidation();
        this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }




    EmployeeId(id): void {
        debugger
 
        for(let i = 0; i< this.employee.length; i++)
        {
          if(this.employee[i].id == id)
          {
            this.EmpName = this.employee[i].employee_Name;
 
            this.deduction.employeeId = this.employee[i].id;
          }
        }
 
 
      }
 
      addSearchDD(){
        this.searchEmployeeDropdownModel.show()
      }
      //Item
 
      displayItem = (emp: EmployeeDto): any => {
        debugger;
 
        if (emp instanceof EmployeeDto) {
            this.deduction.employeeId = emp.id;
          return emp.employee_Name;
        }
        return emp;
 
      }




}

// class for keeping all docNo number
class abc {
    public code: string;
}
