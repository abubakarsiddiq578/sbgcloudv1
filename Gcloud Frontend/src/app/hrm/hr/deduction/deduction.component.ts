// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddDeductionComponent } from './add-deduction/add-deduction.compnent';
import { EditDeductionComponent } from './edit-deduction/edit-deduction.component';
import { DeductionTypeServiceProxy, DeductionServiceProxy, EmployeeServiceProxy, DeductionDto, DeductionTypeDto, EmployeeDto } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'deduction',
  templateUrl: 'deduction.component.html',
  providers: [
    DeductionTypeServiceProxy,
    DeductionServiceProxy,
    EmployeeServiceProxy
  ]
})

export class DeductionComponent implements OnInit, AfterViewInit {

  @ViewChild('addDeductionModal') addDeductionModal: AddDeductionComponent;
  @ViewChild('editDeductionModal') editDeductionModal: EditDeductionComponent;

  result: DeductionDto[] = [];
  deductionType: DeductionTypeDto[] = [];
  employee: EmployeeDto[] = [];

  public dataTable: DataTable;
  data: string[] = [];
  constructor(injector: Injector, private _deductionService: DeductionServiceProxy,
    private _router : Router
  ) {
    //super(injector);
  }

  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Deduction")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);
    }
    this.getAllRecord();

  }
  getAllRecord(): void {
    
    this._deductionService.getAllDeductions()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        
        this.result = result;
        this.paging();
        this.getAllData();

      });
  }

  paging(): void {
    this.dataTable = {
      headerRow: ['Doc No', 'Doc Date', 'Deduction Date', 'Deduction Detail', 'Amount', 'Deduction Type', 'Employee', 'Actions'],
      footerRow: ['Doc No', 'Doc Date', 'Deduction Date', 'Deduction Detail', 'Amount', 'Deduction Type', 'Employee', 'Actions'],

      dataRows: [
        // ['DE01', '10/08/2018', '10/08/2018', 'Nusrat', 'Late Coming','500', 'fdsf', ''],
        // ['DE01', '10/08/2018', '10/08/2018', 'Nusrat', 'Late Coming','500', ''],
        // ['DE01', '10/08/2018', '10/08/2018', 'Nusrat', 'Late Coming','500', ''],
      ]
    };
  }

  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].docNo);
      this.data.push(this.result[i].docDate.toString());
      this.data.push(this.result[i].deductionDate.toString());
      this.data.push(this.result[i].deductionDetail);
      this.data.push(this.result[i].amount.toString());
      this.data.push(this.result[i].deductionTypeId.toString());
      this.data.push(this.result[i].employeeId.toString());
      this.data.push(this.result[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
     // const data = table.row($tr).data();
     // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');

      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }



  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Deduction")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Deduction!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
    if (result.value) {
      
      this._deductionService.delete(parseInt(id))
        .finally(() => {
 
        this.getAllRecord();
        })
        .subscribe(() => {
          swal({
            title: 'Deleted!',
            text: 'Your Deductoin has been deleted.',
            type: 'success',
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
        }).catch(swal.noop)
         });
    } else {
      swal({
          title: 'Cancelled',
          text: 'Your Deduction is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
      }).catch(swal.noop)
    }
  })
  }

  AddDeduction(): void {
    if (!this.globalFunction.hasPermission("Create", "Deduction")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
    this.addDeductionModal.show();
  }

  EditDeduction(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Deduction")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    this.editDeductionModal.show(parseInt(id));
  }

}


