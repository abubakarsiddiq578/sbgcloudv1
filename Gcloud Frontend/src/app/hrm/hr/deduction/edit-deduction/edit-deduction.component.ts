import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { DeductionComponent } from '../deduction.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { DeductionServiceProxy, DeductionDto, DeductionTypeDto, EmployeeDto, DeductionTypeServiceProxy, EmployeeServiceProxy, HRConfigurationServiceProxy, HRConfigurationDto } from '../../../../shared/service-proxies/service-proxies';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';


@Component({
  selector: 'edit-deduction',
  templateUrl: './edit-deduction.component.html',
  providers: [DeductionServiceProxy, DeductionTypeServiceProxy, EmployeeServiceProxy , HRConfigurationServiceProxy]
})
export class EditDeductionComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editDeductionModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    public EmpName: string;
    active: boolean = false;
    saving: boolean = false;
    deduction: DeductionDto = new DeductionDto();
    deductionType: DeductionTypeDto[] = [];
    employee: EmployeeDto[] = [];
    _deductionValidation :FormGroup
    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName : string ;
    
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;
  
    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       private _deductionService: DeductionServiceProxy,
       private _deductionTypeService: DeductionTypeServiceProxy,
       private _employeeService: EmployeeServiceProxy,
       private _hrConfigService : HRConfigurationServiceProxy,
       private formBuilder : FormBuilder
    ) {
        //super(injector);
    }
  

    show(id:number): void{

        this._deductionService.get(id)
      .finally(() => {
          this.active = true;
          this.modal.show();
      })
      .subscribe((result: DeductionDto) => {
          this.deduction = result;
          this._employeeService.get(result.employeeId).subscribe((result)=>{
              this.empName = result.employee_Name;
          })
      });

      this.employeeDropdownSearch();
      this._deductionValidation.markAsUntouched({onlySelf:true});
        
    }
    save(): void {
        
        this.saving = true;
        this._deductionService.update(this.deduction)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            // this.notify.info(this.l('Record Updated Successfully'));
            this.close();
            this.modalSave.emit(null);
          });
      }


      getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }
    

        });

    }



    EmployeeId(id): void {
        debugger
    
        for(let i = 0; i< this.employee.length; i++)
        {
          if(this.employee[i].id == id)
          {
            this.EmpName = this.employee[i].employee_Name;
    
            this.deduction.employeeId = this.employee[i].id;
          }
        }
    
      }
    
      addSearchDD(){
        this.searchEditEmployeeDropdownModel.show()
      }
      //Item
      displayItem = (emp: EmployeeDto): any => {
        debugger;
    
        if (emp instanceof EmployeeDto) {
            this.deduction.employeeId = emp.id;
          return emp.employee_Name;
        }
        return emp;
    
      }



    ngOnInit(): void {
        this._deductionTypeService.getAllDeductionTypes()
            .finally(() => { console.log('completed') })
            .subscribe((result: any) => {
                this.deductionType = result;

            })

        this._employeeService.getAllEmployees()
            .finally(() => { console.log('completed') })
            .subscribe((result: any) => {
                this.employee = result;
                
            })
            this.initValidation();
            this.getAllHRConfiguration();
    }

    /////////////////////////Start of Validations///////////////////////
    initValidation() {

        this._deductionValidation = this.formBuilder.group({
            employeeId: [null, Validators.required],
            deductionTypeId : [null , Validators.required],
            deductionAmount : [null , Validators.required],
            deductionDate :[null , Validators.required]
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
  /*  onType() {
    
      if (this._deductionValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._deductionValidation);
      }
    }*/

    onType() {
        debugger;
        if (this._deductionValidation.valid) {
          debugger;
          let a,p=0 ;
          for(a=0;a<this.employee.length;a++){
            if(this.deduction.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
            this.validateAllFormFields(this._deductionValidation);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._deductionValidation);
           
        }
      }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
////////////////////////End of Validation //////////////////////////////////

///////////////////////////////search Method//////////////////////////////////
public employeeDropdownSearch() {
    debugger;
    this.filteredOptions = this.myControl.valueChanges
        .pipe(
            startWith<string | EmployeeDto>(''),
            map(value => typeof value === 'string' ? value : value.employee_Name),
            map(name => name ? this._filter(name) : this.employee.slice())
        );

}

private _filter(value: string): EmployeeDto[] {

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
}


displayEmployee = (emp: EmployeeDto): any => {
    debugger;

    if (emp instanceof EmployeeDto) {
        this.deduction.employeeId = emp.id;
        return emp.employee_Name;
    }
    this.deduction.employeeId = null;
    return emp;

}





//////////////////////////////////////////////////////////////////////



    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}


