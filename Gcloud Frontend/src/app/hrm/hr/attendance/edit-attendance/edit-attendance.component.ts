import { Component, OnInit, ViewChild, EventEmitter, Output, Injector, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import swal from 'sweetalert2';
import { AttendanceDto, AttendanceServiceProxy, EmployeeServiceProxy, EmployeeDto } from 'app/shared/service-proxies/service-proxies';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-attendance',
  templateUrl: './edit-attendance.component.html',
  styleUrls: ['./edit-attendance.component.less'],
  providers: [AttendanceServiceProxy, EmployeeServiceProxy]
})
export class EditAttendanceComponent implements OnInit {

  @ViewChild('editAttendanceModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  _attendance: FormGroup
  employee: EmployeeDto[];
  Attendance: AttendanceDto = new AttendanceDto();
  
  myControl = new FormControl();
  filteredOptions: Observable<EmployeeDto[]>;
  public empName : string ;

  constructor(
    injector: Injector,

    private AttendanceServiceProxy: AttendanceServiceProxy,
    private EmployeeServiceProxy: EmployeeServiceProxy,
    private formBuilder: FormBuilder


  ) {
    //super(injector);
  }


  show(id: number): void {


    this.AttendanceServiceProxy.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();

      })
      .subscribe((result: AttendanceDto) => {

        this.Attendance = result;
        this.EmployeeServiceProxy.get(result.employeeId).subscribe((result)=>{

            this.empName = result.employee_Name ;
        }) ;
      });

      this.employeeDropdownSearch()
      this._attendance.markAsUntouched({onlySelf:true});

  }
  ////////////////////validations////////////////
  initValidation() {

    this._attendance = this.formBuilder.group({
      employeeId: [null, Validators.required],
      attendanceDate: [null, Validators.required]
    });

  }
/*
  onType() {

    if (this._attendance.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._attendance);
    }
  }*/

  onType() {
    debugger;
    if (this._attendance.valid) {
      debugger;
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.Attendance.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
        
        this.validateAllFormFields(this._attendance);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._attendance);
       
    }
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
/////////////////////////End of Validation/////////////////////////
 ///////////////////////////////search Method//////////////////////////////////
 public employeeDropdownSearch(){
  debugger;
 this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith<string | EmployeeDto >(''),
    map(value => typeof value === 'string' ? value : value.employee_Name),
    map(name => name ? this._filter(name) : this.employee.slice())
  ); 
 
}

private _filter(value: string): EmployeeDto[]{

  debugger;
  const filterValue = value.toLowerCase();

  return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
}


displayEmployee = (emp: EmployeeDto):any => {
  debugger;
  
  if(emp instanceof EmployeeDto)
  {
      this.Attendance.employeeId = emp.id ;
      return emp.employee_Name ;
  }
  this.Attendance.employeeId = null;
  return emp;

}

/////////////////////////////////////////////////////////////////////

  ngOnInit(): void {

    this.EmployeeServiceProxy.getAllEmployees().subscribe((result) => {

      this.employee = result;

    })

    this.initValidation();
  }


  onShown(): void {
    // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }


  notify() {
    swal({
      title: "Success!",
      text: "Attendance Record Updated Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }


  close(): void {
    console.log("Yes Done");
    this.active = false;
    this.modal.hide();
  }


  save(): void {

    debugger;
    this.saving = true;

    this.AttendanceServiceProxy.update(this.Attendance)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        debugger;
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

}
