import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import swal from 'sweetalert2';
import{AttendanceDto,AttendanceServiceProxy, EmployeeServiceProxy , EmployeeDto} from 'app/shared/service-proxies/service-proxies';
import { Time } from '@angular/common';
import * as moment from 'moment';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
///////////////////////////////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
@Component({
  selector: 'add-attendance',
  templateUrl: './add-attendance.component.html',
  styleUrls: ['./add-attendance.component.less'],
  providers : [AttendanceServiceProxy, EmployeeServiceProxy]
})
export class AddAttendanceComponent implements OnInit {

    @ViewChild('addAttendanceModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;


    employee   : EmployeeDto[];
    Attendance : AttendanceDto = new AttendanceDto();

    attendanceDate: Date = new Date();

    _attendance : FormGroup
    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;
  
    public empName : string ;

    constructor(
        injector: Injector,
        private AttendanceServiceProxy : AttendanceServiceProxy,
        private EmployeeServiceProxy : EmployeeServiceProxy,
        private formBuilder : FormBuilder
       
    ) {
        //super(injector);
    }
  

    show(): void{

        this.active = true;
        this.modal.show();
        this.Attendance = new AttendanceDto();
        this.empName = null ;
        this.employeeDropdownSearch();
        this._attendance.markAsUntouched({onlySelf:true});
        
    }
 /////////////////////////////////Validation/////////////////////////////////////
    initValidation() {

        this._attendance = this.formBuilder.group({
            employeeId: [null, Validators.required],
            attendanceDate : [null , Validators.required]
        });
    
    }
    
   /* onType() {
    
      if (this._attendance.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._attendance);
      }
    }*/

  onType() {
    debugger;
    if (this._attendance.valid) {
      debugger;
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.Attendance.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
        //this._attendance.value.employeeId = null; // setting value to null 
        this.validateAllFormFields(this._attendance);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._attendance);
       
    }
  }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
    
////////////////////////////End of Validation/////////////////////////////

 ///////////////////////////////search Method//////////////////////////////////
 public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }
  
  private _filter(value: string): EmployeeDto[]{
  
    debugger;
    const filterValue = value.toLowerCase();
  
    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Attendance.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.Attendance.employeeId = null;
    return emp;
  
  }
  
  /////////////////////////////////////////////////////////////////////
    ngOnInit(): void {


        this.EmployeeServiceProxy.getAllEmployees().subscribe((result)=> {

            debugger;
            this.employee = result;
        })
        this.initValidation();

    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    notify() {
        swal({
          title: "Success!",
          text: "Attendance Record Updated Successfully.",
          timer: 2000,
          showConfirmButton: false
        }).catch(swal.noop)
      }



      save(): void {

        debugger    
       this.Attendance.attendanceDate = moment(this.attendanceDate).add(5,'hour')
        this.saving = true;
        debugger;
        this.AttendanceServiceProxy.create(this.Attendance)
        .finally(() => { this.saving = false; })
        .subscribe(() => {
          this.notify();
          this.close();
          this.modalSave.emit(null);
        });
    
       
      }



    close(): void {
        console.log("Yes Done");    
        this.active = false;
        this.modal.hide();
    }

}
