import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { AddAttendanceComponent } from './add-attendance/add-attendance.component';
import { EditAttendanceComponent } from './edit-attendance/edit-attendance.component';
import swal from 'sweetalert2';
import{AttendanceDto,AttendanceServiceProxy} from 'app/shared/service-proxies/service-proxies';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';
import { Input } from '../../../../../node_modules/@angular/compiler/src/core';
import * as moment from 'moment';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'attendance',
  templateUrl: './attendance.component.html',
  providers:[AttendanceServiceProxy]
})
export class AttendanceComponent implements OnInit, AfterViewInit {

  

  Attendance :AttendanceDto[];
  data: string[] = [];

  public dataTable: DataTable;


  @ViewChild('addAttendanceModal') addAttendanceModal: AddAttendanceComponent;
  @ViewChild('editAttendanceModal') editAttendanceModal: EditAttendanceComponent;
  @ViewChild('fromDate') fromDate: ElementRef;
  @ViewChild('toDate') toDate: ElementRef;
  @ViewChild('employee') employee: ElementRef;


  gf: GlobalFunctions = new GlobalFunctions
 
  constructor(

    private AttendanceServiceProxy : AttendanceServiceProxy
    ,private _router: Router){

  }


  globalFunction: GlobalFunctions = new GlobalFunctions
  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Attendance")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);

    }

    this.getAllAttendance();
  }





    ngAfterViewInit() {
  
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      //edit a record

      table.on('click', '.edit', function (e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });
  



      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }


    AddAttendance(): void {
      if (!this.globalFunction.hasPermission("Create", "Attendance")) {
        this.globalFunction.showNoRightsMessage("Create")
        return
      }
      this.addAttendanceModal.show();
  }

  EditAttendance(id:string): void{
    if (!this.globalFunction.hasPermission("Edit", "Attendance")) {
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
      this.editAttendanceModal.show(parseInt(id));
  }

  initializeDatatable(){


    
    this.dataTable = {
      headerRow: [ 'Employee', 'Attendance Date', 'Attendance Type', 'Comments', 'Actions' ],
      footerRow: [ 'Employee', 'Attendance Date', 'Attendance Type', 'Comments', 'Actions' ],

      dataRows: []
   };


  }

  fillDatatable(){

    let i;
    for (i = 0; i < this.Attendance.length; i++) {

      this.data.push(this.Attendance[i].employee.employee_Name)
      
      this.data.push(this.Attendance[i].attendanceDate.format("DD-MMM-YYYY"))

      this.data.push(this.Attendance[i].attendanceType)
      
      //this.data.push(this.Attendance[i].attendanceTime.toString())
      
      this.data.push(this.Attendance[i].comments)
      

      this.data.push(this.Attendance[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];


    }

  }


  getAllAttendance():void{


    this.AttendanceServiceProxy.getAllAttendances().subscribe((result)=>{


      this.initializeDatatable();
      this.Attendance = result ;
      this.fillDatatable();


    })
  




  }


  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Attendance")) {
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.AttendanceServiceProxy.delete(parseInt(id))
          .finally(() => {
            this.getAllAttendance();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Attendance has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Attendance is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

  // showFilteredData(){
  //   debugger
  //   // this.AttendanceServiceProxy.getFilterdAttendances(moment(this.fromDate.nativeElement.value.toString()), moment(this.toDate.nativeElement.value.toString()))
  //   // .subscribe((result) => {
  //   //   this.Attendance = result
  //   //   this.initializeDatatable()
  //   //   this.fillDatatable()
  //   // })
  // }


  showFilteredData() {


    if(this.employee.nativeElement.value != null && this.employee.nativeElement.value != "" && (this.fromDate.nativeElement.value != "" && this.toDate.nativeElement.value != "")) {
      this.Attendance = [];

      this.AttendanceServiceProxy.getFilterdAttendances(moment(this.fromDate.nativeElement.value.toString()), moment(this.toDate.nativeElement.value.toString()), this.employee.nativeElement.value)
        .subscribe((result) => {
          this.Attendance = result
          this.initializeDatatable()
          this.fillDatatable()
        })
        return
    }


    debugger
    if (this.employee.nativeElement.value != null && this.employee.nativeElement.value != "") {
      this.AttendanceServiceProxy.getFilterdAttendances(null, null, this.employee.nativeElement.value)
        .subscribe((result) => {
          this.Attendance = result
          this.initializeDatatable()
          this.fillDatatable()
        })
    } else if (this.fromDate.nativeElement.value == "" || this.toDate.nativeElement.value == "") {
      this.gf.showErrorMessage("please select To Date or From Date", 'top', 'right')
      return
    } else {
      this.AttendanceServiceProxy.getFilterdAttendances(moment(this.fromDate.nativeElement.value.toString()), moment(this.toDate.nativeElement.value.toString()), this.employee.nativeElement.value)
        .subscribe((result) => {
          this.Attendance = result
          this.initializeDatatable()
          this.fillDatatable()
        })
    }
 
    
  }

}
