import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Injector, Output } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {CategoryDto, CategoryServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy} from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
//import { FormGroup } from '@angular/forms';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'edit-category',
  templateUrl: './edit-category.component.html',
  providers:[CategoryServiceProxy , HRConfigurationServiceProxy]
})
export class EditCategoryComponent implements OnInit {

  @ViewChild('editCategoryModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  Category : CategoryDto = new CategoryDto();

  _cat : FormGroup
  
  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(
      injector: Injector,

      private CategoryService : CategoryServiceProxy,
      private formBuilder : FormBuilder,
      private _hrConfigService : HRConfigurationServiceProxy
      // private _userService: UserServiceProxy
     
  ) {
      // super(injector);
  }


  show(id : number): void{

    this.CategoryService.get(id).finally(()=>
    {

        this.active = true;
        this.modal.show();

    }).subscribe((result : CategoryDto)=>{


        this.Category = result;
    })    
    

  }


  initValidation() {

    this._cat = this.formBuilder.group({
        categoryName: [null, Validators.required]
    });

}


  ngOnInit(): void {
    this.initValidation();
    this.getAllHRConfiguration();
  }

  onType() {
    debugger;
    if (this._cat.valid) {
        this.save();
    } else {
        this.validateAllFormFields(this._cat);

    }
}

validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
}

    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];


            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }


        });

    }




  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }



  save(): void {

    
    this.saving = true;
  
    this.CategoryService.update(this.Category)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
       
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }


  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  
  close(): void {
      this.active = false;
      this.modal.hide();
  }



}
