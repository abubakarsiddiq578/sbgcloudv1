import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddCategoryComponent } from './add-category/add-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { CategoryDto, CategoryServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  providers: [CategoryServiceProxy]
})
export class CategoryComponent implements OnInit, AfterViewInit {

  @ViewChild('addCategoryModal') addCategoryModal: AddCategoryComponent;
  @ViewChild('editCategoryModal') editCategoryModal: EditCategoryComponent;

  public dataTable: DataTable;

  data: string[] = [];

  Category: CategoryDto[];

  constructor(private CategoryService: CategoryServiceProxy,
    private _router: Router
    ) {



  }
  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Category")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);
    }
    this.getAllCategories();
  }


  getAllCategories(): void {

    this.CategoryService.getAllCategories().subscribe((result) => {

      this.intializeDatatable()
      this.Category = result;
      this.fillDatatable()


    })
  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Code', 'Name', 'Sort Order', 'Remarks', 'Actions'],
      footerRow: [/* 'Code', 'Name', 'Sort Order', 'Remarks', 'Actions'*/],

      dataRows: []

    };


  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.Category.length; i++) {

      this.data.push(this.Category[i].code)
      this.data.push(this.Category[i].name)

      this.data.push(this.Category[i].sortOrder.toString())

      this.data.push(this.Category[i].remarks)

      this.data.push(this.Category[i].id.toString())



      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  AddCategory(): void {
    if (!this.globalFunction.hasPermission("Create", "Category")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
    this.addCategoryModal.show();
  }

  EditCategory(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Category")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    this.editCategoryModal.show(parseInt(id));
  }

  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Category")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.CategoryService.delete(parseInt(id))
          .finally(() => {
            this.getAllCategories();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Category has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Category Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }


}
