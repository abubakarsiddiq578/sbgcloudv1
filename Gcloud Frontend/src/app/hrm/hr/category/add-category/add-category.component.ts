import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {CategoryDto, CategoryServiceProxy, HRConfigurationServiceProxy, HRConfigurationDto} from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
//import { FormGroup } from '@angular/forms';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'add-category',
  templateUrl: './add-category.component.html',
  providers: [CategoryServiceProxy , HRConfigurationServiceProxy]
})
export class AddCategoryComponent implements OnInit {

  @ViewChild('addCategoryModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  Category : CategoryDto = new CategoryDto();

  _category  : CategoryDto[]; //for getting all categoryCode Value from  Category table
  _docNo : abc[] =[];
  _cat : FormGroup ;

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;


  constructor(
      injector: Injector,

      private CategoryService : CategoryServiceProxy,

      private formBuilder : FormBuilder , 
      private  _hrConfigService : HRConfigurationServiceProxy
      // private _userService: UserServiceProxy
     
  ) {
      // super(injector);
  }

  initValidation() {


    this._cat = this.formBuilder.group({
        //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
        categoryName: [null, Validators.required]
    });

}


  show(): void{

      this.active = true;
      this.modal.show();
      this.Category = new CategoryDto();
      this.CategoryService.getAllCategories().subscribe((result)=>{

        this._category = result;
        //this.getAutoCategoryNumber();
        if(this.hrConfig.autoCode == true){

            this.getAutoCategoryNumber();
            this.isAutoCode = true ;
          }
          else if(this.hrConfig.autoCode == false){
            this.isAutoCode = false;
          }
        
      });

      this._cat.markAsUntouched({onlySelf:true});
  }


  
  getAllHRConfiguration(){

    this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
    
      this.hrConfig = result[0] ;
    });
    
    }




  onType() {
    debugger;
    if (this._cat.valid) {
        this.save();
    } else {
        this.validateAllFormFields(this._cat);

    }
}

validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
}


  getAutoCategoryNumber():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._category.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._category[i].code ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0  ){
      
        debugger;
        code = "1";
        this.Category.code = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                let x;
                code = this._docNo[this._docNo.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.Category.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Category.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Category.code =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.Category.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Category.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Category.code =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.Category.code = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.Category.code =  "0" + code;
                  }
                  else if(code){
  
                      this.Category.code =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.Category.code = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.Category.code =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.Category.code =  temp[0] ;
                  }

                }



                
        }                     
    }
    this._docNo = [];
}




  ngOnInit(): void {
      this.initValidation();
      this.getAllHRConfiguration();
  }


  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }



  save(): void {

      debugger;
      this.saving = true;
      
      this.CategoryService.create(this.Category)
        .finally(() => { this.saving = false; })
        .subscribe(() => {
          this.notify();
          this.close();
          this.modalSave.emit(null);
        });
    }
  
    notify() {
      swal({
        title: "Success!",
        text: "Saved Successfully.",
        timer: 2000,
        showConfirmButton: false
      }).catch(swal.noop)
    }
  
  close(): void {
      this.active = false;
      this.modal.hide();
      
  }

}
// class for keeping all docNo number
class abc{
  public code: string;
}
