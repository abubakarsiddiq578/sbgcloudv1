import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { DepartmentComponent } from '../department.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { DepartmentServiceProxy, DepartmentDto , HRConfigurationServiceProxy, HRConfigurationDto } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'edit-department',
  templateUrl: './edit-department.component.html',
  providers: [DepartmentServiceProxy , HRConfigurationServiceProxy]
})
export class EditDepartmentComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editDepartmentModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    department: DepartmentDto = new DepartmentDto();
    _departmentValidation : FormGroup

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy,
        private departmentService: DepartmentServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder
       
    ) {
        //super(injector);
    }

    initValidation() {

        this._departmentValidation = this.formBuilder.group({
            departmentTitle: [null, Validators.required]
            
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    onType() {
    
      if (this._departmentValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._departmentValidation);
      }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }


    show(id:number): void {
        debugger
        this.departmentService.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: DepartmentDto)=> {
                this.department = result;
            })
        
    }

    save(): void {
        debugger
        this.saving = true;
        this.departmentService.update(this.department)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            // this.notify.info(this.l('Record Updated Successfully'));
            this.close();
            this.modalSave.emit(null);
          });
      }



      getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }
    

        });

    }




    ngOnInit(): void {
        this.initValidation();
        this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

