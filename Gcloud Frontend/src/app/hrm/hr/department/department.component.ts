// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddDepartmentComponent } from './add-department/add-department.component';
import { EditDepartmentComponent } from './edit-department/edit-department.component';
import swal from 'sweetalert2';
import { DepartmentServiceProxy, DepartmentDto } from '../../../shared/service-proxies/service-proxies';
import { Http } from '@angular/http';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'department',
  templateUrl: 'department.component.html',
  providers: [DepartmentServiceProxy]
})

export class DepartmentComponent implements OnInit, AfterViewInit {
    public dataTable: DataTable;

    @ViewChild('addDepartmentModal') addDepartmentModal: AddDepartmentComponent;
    @ViewChild('editDepartmentModal') editDepartmentModal: EditDepartmentComponent;

    
    // IsEdit: boolean = abp.auth.isGranted("Pages.Hrm.Department.Edit_Departments");;
    // IsDelete: boolean = abp.auth.isGranted("Pages.Hrm.Department.Delete_Departments");
    // IsCreate: boolean = abp.auth.isGranted("Pages.Hrm.Department.Create_Departments");
    // IsView: boolean = abp.auth.isGranted("Pages.Hrm.Department.View_Departments");
    // IsPrint: boolean = abp.auth.isGranted("Pages.Hrm.Department.Print_Departments");

    result: DepartmentDto[]=[];
    data: string[] = [];

    globalFunction : GlobalFunctions = new GlobalFunctions
    constructor(injector: Injector , 
      /*private _authService: AppAuthService,*/
      private http: Http,
      private _departmentService: DepartmentServiceProxy,
      private _router: Router
      
  ) 
  {
      //super(injector);
  }


    ngOnInit() {
      debugger
      if (!this.globalFunction.hasPermission("View", "Department")) {
        this.showNotification('top','left','You do not have rights to view Department')
        this._router.navigate([''])

      }
      this.getAllRecord();
    }

    showNotification(from: any, align: any, message: any) {
      $.notify({
          icon: 'notifications',
          message: message
      }, {
          type: 'danger',
          timer: 1500,
          placement: {
              from: from,
              align: align
          },
      });
    }

    getAllRecord(): void {
      
    this._departmentService.getAllDepartments()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        
        this.result = result;
        this.paging();
        this.getAllData();
      });
  }


  paging(): void {
    this.dataTable = {
      headerRow: ['Code', 'Department Title', 'Salary Group', 'Sort Order', 'Comments', 'Active', 'Actions'],
      footerRow: ['Code', 'Department Title', 'Salary Group', 'Sort Order', 'Comments', 'Active', 'Actions'],

      dataRows: [
        // ['D01', 'Development', 'Engineering', '1', 'Engneering Salary Group','Yes', ''],
        // ['D02', 'Support', 'Engineering', '2', 'Support Salary Group','Yes', ''],
        // ['D03', 'Sales', 'Sale and Marketing', '3', 'Sales and Marketing','Yes', ''],
      ]
    };
  }


  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].code);
      this.data.push(this.result[i].title);
      this.data.push(this.result[i].salaryGroup);
      this.data.push(this.result[i].sortOrder.toString());
      this.data.push(this.result[i].comments);
      this.data.push(this.result[i].isActive.toString());
      this.data.push(this.result[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      // table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

    protected delete(id: string): void {
debugger
      if (!this.globalFunction.hasPermission("Delete", "Department")) {
        this.showNotification('top','left','You do not have rights to Delete Department')
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Location!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
      if (result.value) {
        debugger;
        this._departmentService.delete(parseInt(id))
          .finally(() => {

            this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Salary Expense Type has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        debugger;
        swal({
          title: 'Cancelled',
          text: 'Your Salary Expense Type is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }


    AddDepartment(): void {
      if (!this.globalFunction.hasPermission("Create", "Department")) {
        this.showNotification('top','left','You do not have rights to Create Department')
        return
      }
      this.addDepartmentModal.show();
      }

      EditDepartment(id:string): void {
        if (!this.globalFunction.hasPermission("Edit", "Department")) {
          this.showNotification('top','left','You do not have rights to Edit Department')
          return
        }
        this.editDepartmentModal.show(parseInt(id));
        }

}


