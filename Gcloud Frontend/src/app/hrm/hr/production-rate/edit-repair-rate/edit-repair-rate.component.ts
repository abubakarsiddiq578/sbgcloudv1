import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';

@Component({
  selector: 'edit-repair-rate',
  templateUrl: './edit-repair-rate.component.html'
})
export class EditRepairRateComponent implements OnInit {

    @ViewChild('editRepairRateModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    editData; any;

    constructor(
        injector: Injector,     
    ) {}

    show(data: any): void{

        this.editData = data;
        this.active = true;
        this.modal.show();
        
    }

    save(){
        debugger
        this.close();
        this.modalSave.emit(this.editData);
    }

    ngOnInit(): void {
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

