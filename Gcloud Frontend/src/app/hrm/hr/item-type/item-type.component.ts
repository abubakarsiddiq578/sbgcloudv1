import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddItemTypeComponent } from './add-item-type/add-item-type.component';
import { EditItemTypeComponent } from './edit-item-type/edit-item-type.component';
import { ItemTypeDto, ItemTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-item-type',
  templateUrl: './item-type.component.html',
  providers:[ItemTypeServiceProxy]
})
export class ItemTypeComponent implements OnInit, AfterViewInit {

  @ViewChild('addItemTypeModal') addItemTypeModal: AddItemTypeComponent;
  @ViewChild('editItemTypeModal') editItemTypeModal: EditItemTypeComponent;

    public dataTable: DataTable;
    data : string[] = [];
    itemType:ItemTypeDto[];

    constructor(private ItemtypeServices : ItemTypeServiceProxy,private _router : Router){

    }
    
    globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "ItemType")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
       this.getAllItemType();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }


    AddItemType(): void {
      if (!this.globalFunction.hasPermission("Create", "ItemType")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addItemTypeModal.show();
  }

  EditItemType(id:string): void{
    if (!this.globalFunction.hasPermission("Edit", "ItemType")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
      this.editItemTypeModal.show(parseInt(id));
  }


  initializeDatatable(){

    this.dataTable = {
      headerRow: [ 'Code', 'Name', 'SortOrder', 'Remarks', 'Actions' ],
      footerRow: [/* 'Code', 'Name', 'SortOrder', 'Remarks', 'Actions'*/ ],

      dataRows: []
        
   };

  }

  getAllItemType():void{

    this.ItemtypeServices.getAllItemTypes().subscribe((result)=>{

      this.initializeDatatable()
      this.itemType = result;
      this.fillDatatable()
    })


  }


  fillDatatable() {
    let i;
    for (i = 0; i < this.itemType.length; i++) {

      this.data.push(this.itemType[i].code)
      
      this.data.push(this.itemType[i].name)

      this.data.push(this.itemType[i].sortOrder.toString())

      this.data.push(this.itemType[i].remarks)

      this.data.push(this.itemType[i].id.toString())
      
      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }

  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "ItemType")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.ItemtypeServices.delete(parseInt(id))
          .finally(() => {
            this.getAllItemType();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Item Type has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Item Type Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

}
