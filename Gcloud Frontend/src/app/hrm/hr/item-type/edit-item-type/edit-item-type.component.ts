import { Component, OnInit, Injector, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ItemTypeDto, ItemTypeServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'edit-item-type',
  templateUrl: './edit-item-type.component.html',
  providers: [HRConfigurationServiceProxy, ItemTypeServiceProxy]
})
export class EditItemTypeComponent implements OnInit {

  @ViewChild('editItemTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  itemType : ItemTypeDto = new ItemTypeDto();
  _item : FormGroup;
  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;
  constructor(
      injector: Injector,
      private itemTypeService : ItemTypeServiceProxy,
      private formBuilder : FormBuilder,
      private _hrConfigService : HRConfigurationServiceProxy
      // private _userService: UserServiceProxy
     
  ) {
      // super(injector);
  }


  show(id:number): void{

      this.itemTypeService.get(id).finally(()=>{

          this.active = true;
          this.modal.show();
      
      }).subscribe((result : ItemTypeDto)=>{

            this.itemType = result;
      })
      
  }

  initValidation() {

    this._item = this.formBuilder.group({
        itemName: [null, Validators.required],
      
    });

}

onType() {

    if (this._item.valid) {
        this.save();
    } else {
        this.validateAllFormFields(this._item);
    }
  }
  
  validateAllFormFields(formGroup: FormGroup) {
  
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
  }



  ngOnInit(): void {
      this.initValidation();
      this.getAllHRConfiguration();
  }


  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];

        if (this.hrConfig.autoCode == true) {

            this.isAutoCode = true;
        }
        else if (this.hrConfig.autoCode == false) {
            this.isAutoCode = false;
        }


    });

}




  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }



  save(): void {

    
    this.saving = true;
  
    this.itemTypeService.update(this.itemType)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
       
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
      this.active = false;
      this.modal.hide();
  }

}
