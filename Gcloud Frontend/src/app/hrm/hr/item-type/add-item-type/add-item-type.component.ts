import { Component, OnInit, ViewChild, Output, ElementRef, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ItemTypeDto, ItemTypeServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'add-item-type',
  templateUrl: './add-item-type.component.html',
  providers:[ItemTypeServiceProxy , HRConfigurationServiceProxy]
})
export class AddItemTypeComponent implements OnInit {

  @ViewChild('addItemTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  itemType : ItemTypeDto = new ItemTypeDto();
  _docNo : abc[] = [];
  _item : FormGroup;
  _itemType : ItemTypeDto[];
  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(
      injector: Injector,
      private itemTypeService : ItemTypeServiceProxy,
      private formBuilder : FormBuilder,
      private _hrConfigService : HRConfigurationServiceProxy
      // private _userService: UserServiceProxy
     
  ) {
      // super(injector);
  }


  show(): void{

      this.active = true;
      this.modal.show();
      this.itemType = new ItemTypeDto();
      this.itemTypeService.getAllItemTypes().subscribe((result)=>{

        this._itemType = result;
       // this.getAutoCode();
       if(this.hrConfig.autoCode == true){
        debugger;
        this.getAutoCode();
        this.isAutoCode = true ;
      }
      else if(this.hrConfig.autoCode == false){
        this.isAutoCode = false;
      }

    })
      this._item.markAsUntouched({onlySelf:true});
      
  }

  initValidation() {


    this._item = this.formBuilder.group({
        itemName: [null, Validators.required],
      
    });

}


onType() {

  if (this._item.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._item);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}


getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];

    });

}


  ngOnInit(): void {
    this.initValidation();
    this.getAllHRConfiguration();
  }


  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

     //this function generate auto doc number
     getAutoCode():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._itemType.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._itemType[i].code ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0  ){
          
            debugger;
            code = "1";
            this.itemType.code = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    let x;
                    code = this._docNo[this._docNo.length-1].code ;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.itemType.code = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.itemType.code =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.itemType.code =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.itemType.code = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.itemType.code =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.itemType.code =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.itemType.code = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.itemType.code =  "0" + code;
                      }
                      else if(code){
      
                          this.itemType.code =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.itemType.code = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.itemType.code =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.itemType.code =  temp[0] ;
                      }
    
                    }
                    
            }                     
        }
        this._docNo = [];
    }
    




  save(): void {

    debugger;
    this.saving = true;
    
    this.itemTypeService.create(this.itemType)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }



  
  close(): void {
      this.active = false;
      this.modal.hide();
  }

}


// class for keeping all docNo number
class abc{
  public code: string;
}

