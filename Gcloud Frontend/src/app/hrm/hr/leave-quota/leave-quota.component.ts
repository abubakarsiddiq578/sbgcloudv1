// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddLeaveQuotaComponent } from './add-leave-quota/add-leave-quota.component';
import { EditLeaveQuotaComponent } from './edit-leave-quota/edit-leave-quota.component';
import { LeaveQuotaServiceProxy, LeaveTypeServiceProxy, LeaveTypeDto, LeaveQuotaMasterDto, LeaveQuotaDetailDto, LeaveQuotaDetailServiceProxy, LeaveQuotaDetail } from '../../../shared/service-proxies/service-proxies';
import { toDate } from '@angular/common/src/i18n/format_date';
import { parseDate } from 'ngx-bootstrap/chronos';
//import { moment } from 'ngx-bootstrap/chronos/test/chain';
import swal from 'sweetalert2';
import { ColDef, GridApi, ColumnApi } from '../../../../../node_modules/ag-grid-community';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { Moment } from 'moment';
declare const $: any;
declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'leave-quota',
  templateUrl: 'leave-quota.component.html',
  providers: [LeaveQuotaServiceProxy, LeaveTypeServiceProxy, LeaveQuotaDetailServiceProxy]
})

export class LeaveQuotaComponent implements OnInit, AfterViewInit {
  public dataTable: DataTable;
  public dataTable1: DataTable;

  isDetail: boolean = true;
  isHistory: boolean = false;
  isSaveBtn: boolean = true;
  isEditBtn: boolean = false;

  @ViewChild('addLeaveQuotaModal') addLeaveQuotaModal: AddLeaveQuotaComponent;
  @ViewChild('editLeaveQuotaModal') editLeaveQuotaModal: EditLeaveQuotaComponent;
  active: boolean = false;
  saving: boolean = false;
  editing: boolean = false;
  leaveQuotaData: LeaveQuotaData[] = [];
  data: string[] = [];
  data1: string[] = [];
  editData: any;
  leaveTypeDDList: LeaveTypeDto[] = [];
  dataArr2 : LeaveQuotaDetailDto = new LeaveQuotaDetailDto(); // for reset data//
  test2Arr =[];
  //leaveQuota: LeaveQuotaMasterDto = new LeaveQuotaMasterDto();
  leaveQuotaDetailList = [];
  LeaveQuotaList: LeaveQuotaMasterDto[] = [];
  leaveQuotaMasterDetail: any = [];

  dataArr : LeaveQuotaDetailDto = new LeaveQuotaDetailDto();
  tempDataArr: LeaveQuotaDetailDto[] = [];

  leaveQuota: LeaveQuotaMasterDto = new LeaveQuotaMasterDto();
  testARR = [];
  //private rowData: any[] = [];
  public rowData: LeaveQuotaDetailDto[] = [];
  public columnDefs: ColDef[];
  public defaultColDef;


  // gridApi and columnApi
  private api: GridApi;
  public columnApi: ColumnApi;

   //row selectoin 
   public  rowSelection;

   @ViewChild('PageSize') PageSize: any;
   gf = new GlobalFunctions

  constructor(injector: Injector, private _leaveTypeService: LeaveTypeServiceProxy,
    private _leaveQuotaService: LeaveQuotaServiceProxy,
    private _leaveQuotaDetailService: LeaveQuotaDetailServiceProxy,
    
  ) {

       this.initAgGrid();

   }


   initAgGrid(){

    this.columnDefs = this.createColumnDefs();
    this.defaultColDef = { editable: true };
    this.rowSelection = "multiple";

   }
  ngOnInit() {
    this._leaveTypeService.getAllLeaveTypes()
      .subscribe((result) => {
        this.leaveTypeDDList = result;
       
        this.initEditableGridData();
        
      })


    this._leaveQuotaService.getAllLeaveQuota()
      .subscribe((result) => {
        this.LeaveQuotaList = result;
        console.log("All Master Record:", this.LeaveQuotaList);
        this.initMasterRecord();
        this.getAllData();
      })
    
  }


  initEditableGridData(){
    let m=0;
    let temArr = new Array();
    for(m=0;m<this.leaveTypeDDList.length;m++){

      temArr.push(this._leaveQuotaService.getLeaveTypeById(this.leaveTypeDDList[m].id));
   
    }


    Observable.forkJoin(temArr).subscribe((data)=>{

      data.forEach(element => {
        
        this.dataArr.id = element[0].id;
        this.dataArr.leaveTypeId = element[0].id
        this.dataArr.leaveType = element[0] 
        this.dataArr.allowedLeaves = 0;
        this.dataArr.leaveQuotaMaster = undefined;
        this.dataArr.notes = "";
        this.dataArr.resetDate = moment();
        this.tempDataArr.push(this.dataArr);
        this.dataArr = new LeaveQuotaDetailDto();


        ///////////////////for reset ag grid////////////////////////////////

       this.dataArr2.id = element[0].id;
       this.dataArr2.leaveTypeId = element[0].id
       this.dataArr2.leaveType = element[0]
       this.dataArr2.allowedLeaves = 0;
       this.dataArr2.leaveQuotaMaster = undefined;
       this.dataArr2.notes = "empty";
       this.dataArr2.resetDate = moment();
       this.test2Arr.push(this.dataArr2);
       this.dataArr2 = new LeaveQuotaDetailDto();


       /////////////////////////////////////////////////////

      });


      console.log("0-- : " , this.tempDataArr);
      this.rowData = this.tempDataArr;
      console.log("rowdata :" , this.rowData );
    })
    

  }


  public createColumnDefs() {
    //console.log("ffff: ", this.rowData);
    return [
        { field: 'leaveTypeName', valueGetter: (params) => params.data.leaveType.leaveTypeName},
        { field: 'allowedLeaves'},
        { field: 'resetDate' },
        { field: 'notes'} //,
       // { field: 'Action', valueGetter: (params) => params.data.id, cellRenderer: this.CellRendererDelete }
       /* { field: 'leaveTypeName'},
        { field: 'leaveAllowed'},
        { field: 'leaveReset' },
        { field: 'leaveDetail'} */
      //  { field: 'Action', valueGetter: (params) => params.data.id, cellRenderer: this.CellRendererDelete }
    ]
  }

  // this set the page size
  onPageSizeChanged(value) {
    debugger
    //let value : any = this.PageSize.value;
    this.api.paginationSetPageSize(Number(value));
}

onCellValueChanged(params) {
  debugger
  let NewData = [];
  this.api.forEachNode(node => NewData.push(node.data));
  console.log(NewData)

  params.data.newValue
}


 // one grid initialisation, grap the APIs and auto resize the columns to fit the available space
 onGridReady(params): void {
  this.api = params.api;
  this.columnApi = params.columnApi;
  this.api.sizeColumnsToFit();
}


 //Render Delete button // not using //
 CellRendererDelete(params) {
  var button = document.createElement('button');
  button.innerHTML = 'Del';
  button.addEventListener('click', function () {
      window.alert("Delete button clicked with id: " + params.value);
  });

  return button;
}


//Remove Selected Rows
onRemoveSelected() {
  var selectedData = this.api.getSelectedRows();
  var res = this.api.updateRowData({ remove: selectedData });
}




  getAllLeaveTypes() {

    this._leaveTypeService.getAllLeaveTypes()
      .subscribe((result) => {
        this.leaveTypeDDList = result;
       
        this.getAllLeaveQuotas(); // initializing datatables too //
      })
  }




  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    //Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


//this is with editable grid//
EditLeaveQuotaMasterDetail(id: string): void {
  debugger;
  this.leaveQuota.id = parseInt(id);
  this._leaveQuotaService.getAllDetailById(parseInt(id)).subscribe((result)=>{

      debugger;

      this.leaveQuota.leaveQuotaYear = result[0].leaveQuotaMaster.leaveQuotaYear
      this.leaveQuota.title = result[0].leaveQuotaMaster.title
      this.rowData = result ; 
      this.editing = true;
      //this.initDataTable();
      this.createColumnDefs();
     // this.fillEditRecord();
      this.ActiveUpdateBtn();
      this.ActiveDetail();
    })
}


  getAllLeaveQuotas(): void {
    //this.initDataTable();
    //this.fillDataTable();

  }


  initMasterRecord(): void {
    this.dataTable1 = {
      headerRow: ['Title', 'Year', 'Actions'],
      footerRow: [ /*'Leave Type', 'Allowed Leaves', 'Reset Date', 'Note', 'Actions'*/],

      dataRows: [
        // ['Annual Leaves', '12', '30/09/2018', 'Annualy allowed leaves', ''],
        // ['Sick Leaves', '10', '30/09/2018', 'Annualy sick leaves allowed', ''],
        // ['Casual Leaves', '10', '30/09/2018', 'Annualy casual leaves allowed', ''],
        // ['Special Leaves', '3', '30/09/2018', 'Annualy special leaves for specal occasions', '']
      ],
    };
  }
  getAllLeaveQuotaRecord(): void {
    debugger;
    this._leaveQuotaService.getAllLeaveQuota()
      .subscribe((result) => {
        this.LeaveQuotaList = [];
        this.LeaveQuotaList = result;
        //this.initDataTable();
        this.initMasterRecord();
        this.getAllData();
      })
  }


  getAllData(): void {
    let i;
    for (i = 0; i < this.LeaveQuotaList.length; i++) {
      this.data1.push(this.LeaveQuotaList[i].title);
      this.data1.push(this.LeaveQuotaList[i].leaveQuotaYear.toString());


      this.data1.push(this.LeaveQuotaList[i].id.toString());
      //this below line will push all the record.
      this.dataTable1.dataRows.push(this.data1);
      this.data1 = [];
    }
  }


  //this function is implement for editable grid

updateData(): void {

  let rowData = [];
  this.api.forEachNode((node) => {
    debugger
    rowData.push(node.data)
  });

  debugger;
  this.testARR = [];
 
  let g = 0;
  for (g = 0; g < rowData.length; g++) {

    // rowData[g].id = undefined;
     rowData[g].leaveType = undefined ; 
     rowData[g].leaveQuotaMaster = undefined;
     this.testARR.push(rowData[g]);
  }


  this.leaveQuota.leaveQuotaDetails = this.testARR;

  debugger;
  this.saving = true;
  this._leaveQuotaService.update(this.leaveQuota)
    .finally(() => { this.saving = false; })
    .subscribe(() => {
      this.notifyMsg('Success!', 'Updated Successfully');
      this.ResetControls();
      this.ActiveHistory();
    });
  }




  notifyMsg(title, msg) {
    swal({
      title: title,
      text: msg,
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

 

  saveData() {

    debugger
 
    let rowData = [];
    this.api.forEachNode((node) => {
      rowData.push(node.data)
    });

    debugger;
    this.testARR = [];
    this.leaveQuota.id = undefined;
    let g = 0;
    for (g = 0; g < rowData.length; g++) {

      //this.leaveQuotaDetailList[g].leaveType = undefined;
      //this.leaveQuotaDetailList[g].leaveQuotaMaster = undefined;
       rowData[g].id = undefined;
       rowData[g].leaveType = undefined ; 
       rowData[g].leaveQuotaMaster = undefined;
       this.testARR.push(rowData[g]);
    }

    this.leaveQuota.leaveQuotaDetails = this.testARR;

    this._leaveQuotaService.create(this.leaveQuota)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        debugger;

        this.notifyMsg('Success!', 'Data Successfully Added');
        this.ResetControls();
        //this.getAllLeaveTypes();
        this.getAllLeaveQuotaRecord();
        this.ActiveHistory();
        //this.notify.info(this.l('Saved Successfully'));
        //this.close();   
        //this.modalSave.emit(null);
      });

      this.ActiveSaveBtn();


  }



  ResetControls() {

  
  this.leaveQuota = new LeaveQuotaMasterDto();

   //this.rowData = this.tempDataArr;
   this.rowData = this.test2Arr;
   this.initAgGrid();

   this.api.setRowData(this.rowData);
 

    this.ActiveSaveBtn();

  }


  ActiveDetail() {
    this.isDetail = true;
    this.isHistory = false;
  }

  // show history tab 
  ActiveHistory() {
    this.isDetail = false;
    this.isHistory = true;
  }

   // show save btn
   ActiveSaveBtn() {
    this.isSaveBtn = true;
    this.isEditBtn = false;
  }

  // show update btn
  ActiveUpdateBtn() {
    this.isSaveBtn = false;
    this.isEditBtn = true;
  }


  deleteLeaveQuotaMaster(id: string) {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Leave Quota!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this._leaveQuotaService.delete(parseInt(id))
          .finally(() => {
            debugger;
            //this.getAllLeaveQuotaRecord();
            this.getAllLeaveQuotaRecord();            

          })
          .subscribe(() => {

            swal({
              title: 'Deleted!',
              text: 'Leave Quota has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Leave Quota Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

///////

  onBtPrint() {
    let gridApii = this.api;
   // setPrinterFriendly(gridApii);
    setTimeout(function () {
      print();
      //setNormal(gridApii);
    }, 2000);
  }

   /*function setPrinterFriendly(api) {
        var eGridDiv = document.querySelector(".my-grid");
        //eGridDiv.style.width = "";
        //eGridDiv.style.height = "";
        api.setDomLayout("print");
    }

    function setNormal(api) {
        var eGridDiv = document.querySelector(".my-grid");
        //eGridDiv.style.width = "600px";
        //eGridDiv.style.height = "200px";
        api.setDomLayout(null);
     }*/





  //start: fill data table with filter data. Edit record.
  /*fillEditRecord() {
    debugger;
    this.leaveQuotaData = [];
    this.leaveQuotaDetailList = [];
    let i;
    //let n = 0;
    for (i = 0; i < this.leaveQuotaMasterDetail.length; i++) {
      this.data.push(this.leaveQuotaMasterDetail[i].leaveType.leaveTypeName);
      this.data.push(this.leaveQuotaMasterDetail[i].allowedLeaves);
      this.data.push(this.leaveQuotaMasterDetail[i].resetDate);
      this.data.push(this.leaveQuotaMasterDetail[i].notes);
      this.data.push(this.leaveQuotaMasterDetail[i].id);

      this.dataTable.dataRows.push(this.data)

      let _leaveQuotaData = new LeaveQuotaData();
      _leaveQuotaData.id = this.leaveQuotaMasterDetail[i].id;
      _leaveQuotaData.LeaveType = this.leaveQuotaMasterDetail[i].leaveType.leaveTypeName;
      _leaveQuotaData.AllowedLeaves = this.leaveQuotaMasterDetail[i].allowedLeaves;
      _leaveQuotaData.ResetDate = this.leaveQuotaMasterDetail[i].resetDate;
      _leaveQuotaData.Note = this.leaveQuotaMasterDetail[i].notes;
      _leaveQuotaData.LeaveTypeId = this.leaveQuotaMasterDetail[i].leaveTypeId;
      this.leaveQuotaData.push(_leaveQuotaData);

      let leaveQuotaDetail: LeaveQuotaDetailDto = new LeaveQuotaDetailDto();
      leaveQuotaDetail.allowedLeaves = this.leaveQuotaMasterDetail[i].allowedLeaves;
      leaveQuotaDetail.leaveTypeId = this.leaveQuotaMasterDetail[i].leaveTypeId;
      leaveQuotaDetail.resetDate = this.leaveQuotaMasterDetail[i].resetDate;
      leaveQuotaDetail.notes = this.leaveQuotaMasterDetail[i].notes;
      leaveQuotaDetail.id = this.leaveQuotaMasterDetail[i].id;

      this.leaveQuotaDetailList.push(leaveQuotaDetail);
      //this.leaveQuota.leaveQuotaDetails = this.leaveQuotaDetailList;

      this.leaveQuota.title = this.leaveQuotaMasterDetail[i].leaveQuotaMaster.title;
      this.leaveQuota.leaveQuotaYear = this.leaveQuotaMasterDetail[i].leaveQuotaMaster.leaveQuotaYear;
      this.leaveQuota.id = this.leaveQuotaMasterDetail[i].leaveQuotaMaster.id;

      this.data = [];
    }
  }*/
  //end: fill data table with filter data. Edit record.
  /*fillDataTable() {
    debugger;
    this.leaveQuotaData = [];
    let i;
    //let n = 0;
    for (i = 0; i < this.leaveTypeDDList.length; i++) {
      this.data.push(this.leaveTypeDDList[i].leaveTypeName);
      this.data.push(this.leaveTypeDDList[i].leaveAllowed.toString());
      this.data.push(this.leaveTypeDDList[i].leaveReset.toString());
      this.data.push(this.leaveTypeDDList[i].leaveDetail);
      this.data.push(this.leaveTypeDDList[i].id.toString());

      this.dataTable.dataRows.push(this.data)
////////////////////////pushing leaveTypeDDlist data to row///////////////////////////////////////////
     // this.rowData.push(this.leaveTypeDDList[i]);



////////////////////////////////////////////////////////////////////////////////

      let _leaveQuotaData = new LeaveQuotaData();
      _leaveQuotaData.id = this.leaveTypeDDList[i].id;
      _leaveQuotaData.LeaveType = this.leaveTypeDDList[i].leaveTypeName;
      _leaveQuotaData.AllowedLeaves = this.leaveTypeDDList[i].leaveAllowed.toString();
      _leaveQuotaData.ResetDate = this.leaveTypeDDList[i].leaveReset.toString();
      _leaveQuotaData.Note = this.leaveTypeDDList[i].leaveDetail;
      ////////
      _leaveQuotaData.LeaveTypeId = this.leaveTypeDDList[i].id;
      //////////////
      this.leaveQuotaData.push(_leaveQuotaData);

      let leaveQuotaDetail: LeaveQuotaDetailDto = new LeaveQuotaDetailDto();
      leaveQuotaDetail.allowedLeaves = this.leaveTypeDDList[i].leaveAllowed;
      leaveQuotaDetail.leaveTypeId = this.leaveTypeDDList[i].id;
      leaveQuotaDetail.resetDate = this.leaveTypeDDList[i].leaveReset;
      leaveQuotaDetail.notes = this.leaveTypeDDList[i].leaveDetail;

      this.leaveQuotaDetailList.push(leaveQuotaDetail);
      this.leaveQuota.leaveQuotaDetails = this.leaveQuotaDetailList;
      this.data = [];
    }
  }*/

 /* deleteLeaveQuotaDetail(id: string) {

    debugger
    let DelLeaveQuotaDetail;

    // get record of matching id
    DelLeaveQuotaDetail = this.leaveQuotaData.find(function (obj) { return obj.id === parseInt(id); });

    // perform delete operation to delete object from detail list 
    const index: number = this.leaveQuotaData.indexOf(DelLeaveQuotaDetail);
    if (index !== -1) {
      this.leaveQuotaData.splice(index, 1);

      //this.initDetailDataTable();
      //this.fillEditDetailDataTable();
      this.initDataTable();
      
      this.fillUpdateDataTable();
    }
  }*/





 /* setEditData(editLeaveQuota) {
    this.leaveQuotaData.find(function (obj) { return obj.id === parseInt(editLeaveQuota.id); }).AllowedLeaves = editLeaveQuota.AllowedLeaves;
    this.leaveQuotaData.find(function (obj) { return obj.id === parseInt(editLeaveQuota.id); }).LeaveType = editLeaveQuota.LeaveType;
    this.leaveQuotaData.find(function (obj) { return obj.id === parseInt(editLeaveQuota.id); }).ResetDate = editLeaveQuota.ResetDate;
    this.leaveQuotaData.find(function (obj) { return obj.id === parseInt(editLeaveQuota.id); }).Note = editLeaveQuota.Note;

    this.initDataTable();
    this.fillUpdateDataTable();
  }*/

  /*
  fillUpdateDataTable() {
    let i;
    this.leaveQuotaDetailList = [];
    debugger
    for (i = 0; i < this.leaveQuotaData.length; i++) {
      this.data.push(this.leaveQuotaData[i].LeaveType);
      this.data.push(this.leaveQuotaData[i].AllowedLeaves);
      this.data.push(this.leaveQuotaData[i].ResetDate);
      this.data.push(this.leaveQuotaData[i].Note);
      this.data.push(this.leaveQuotaData[i].id.toString())

      this.dataTable.dataRows.push(this.data)


      let leaveQuotaDetail: LeaveQuotaDetailDto = new LeaveQuotaDetailDto();
      leaveQuotaDetail.allowedLeaves = parseInt(this.leaveQuotaData[i].AllowedLeaves);
      leaveQuotaDetail.leaveTypeId = this.leaveQuotaData[i].LeaveTypeId;
      leaveQuotaDetail.resetDate = this.leaveQuotaData[i].ResetDate;
      leaveQuotaDetail.notes = this.leaveQuotaData[i].Note;

      if (this.editing == true) {
        leaveQuotaDetail.id = this.leaveQuotaData[i].id;
      }


      this.leaveQuotaDetailList.push(leaveQuotaDetail);
      //this.leaveQuota.leaveQuotaDetails = this.leaveQuotaDetailList;

      this.data = [];
    }
  }*/

  //Start: this Save() method is used for saving the record to database with dataTable.
  /*Save(): void {
    //TODO: Refactor this, don't use jQuery style code
    debugger;
    this.leaveQuota.id = undefined;
    let g = 0;
    for (g = 0; g < this.leaveQuotaDetailList.length; g++) {

      this.leaveQuotaDetailList[g].leaveType = undefined;
      this.leaveQuotaDetailList[g].leaveQuotaMaster = undefined;

    }

    this.leaveQuota.leaveQuotaDetails = this.leaveQuotaDetailList;
    this.saving = true;

    debugger;
    this._leaveQuotaService.create(this.leaveQuota)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        debugger;

        this.notifyMsg('Success!', 'Data Successfully Added');
        this.ResetControls();
        //this.getAllLeaveTypes();
        this.getAllLeaveQuotaRecord();
        this.ActiveHistory();
        //this.notify.info(this.l('Saved Successfully'));
        //this.close();   
        //this.modalSave.emit(null);
      });

      this.ActiveSaveBtn();

  }*/
  //End: this Save() method is used for saving the record to database. leaveQuota
  //this update function is implement for dataTable
  /*update(): void {

    let p =0;
    for(p=0;p<this.leaveQuotaDetailList.length;p++){

     // this.leaveQuotaDetailList[p].id = undefined;
       this.leaveQuotaDetailList[p].leaveQuotaMaster = undefined;
      this.leaveQuotaDetailList[p].leaveType = undefined;

    }
    this.leaveQuota.leaveQuotaDetails = this.leaveQuotaDetailList;

    debugger;
    this.saving = true;
    this._leaveQuotaService.update(this.leaveQuota)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Updated Successfully');
        this.ResetControls();
        this.ActiveHistory();
      });
  }*/


    /*AddLeaveQuota(): void {
    this.addLeaveQuotaModal.show();
  }

  EditLeaveQuota(id: string): void {

    this.editData = this.leaveQuotaData.find(function (obj) { return obj.id === parseInt(id); });

    this.editLeaveQuotaModal.show(this.editData);
  }*/


  ////this is with datatable
  /*EditLeaveQuotaMasterDetail(id: string): void {
    debugger;
   
      this._leaveQuotaService.getAllDetailById(parseInt(id)).subscribe((result)=>{

        debugger;
        this.leaveQuotaMasterDetail = result ; 
        this.editing = true;
        this.initDataTable();
        this.fillEditRecord();
        this.ActiveUpdateBtn();
        this.ActiveDetail();
      })
}*/

 /* initDataTable(): void {
    this.dataTable = {
      headerRow: ['Leave Type', 'Allowed Leaves', 'Reset Date', 'Note', 'Actions'],
      footerRow: [],

      dataRows: [ ],
    };
  }*/

   /*
    close(): void {
      this.active = false;
      this.modal.hide();
  }*/

}

//LeaveQuotaData class is used for updating the grid on client side.
class LeaveQuotaData {
  public id: number;
  public LeaveType: string;
  public LeaveTypeId: number;
  
  public AllowedLeaves: string;
  // public ResetDate: string;
  public ResetDate: any;
  public Note: string;
}

