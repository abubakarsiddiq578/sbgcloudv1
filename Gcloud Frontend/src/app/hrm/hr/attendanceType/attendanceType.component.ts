import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AttendanceTypeDto, AttendanceTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { AddAttendanceTypeComponent } from './add-attendanceType/add-attendanceType.component';
import { EditAttendanceTypeComponent } from './edit-attendanceType/edit-attendanceType.component';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'app-attendanceType',
  templateUrl: './attendanceType.component.html',
  styleUrls: ['./attendanceType.component.scss'],
  providers: [AttendanceTypeServiceProxy]
})
export class AttendanceTypeComponent implements OnInit, AfterViewInit {



  @ViewChild('addAttendanceTypeModal') addAttendanceTypeModal: AddAttendanceTypeComponent;
  @ViewChild('editAttendanceTypeModal') editAttendanceTypeModal: EditAttendanceTypeComponent;





  constructor(private AttendanceTypeService: AttendanceTypeServiceProxy) { }
  public dataTable: DataTable;
  AttendanceType: AttendanceTypeDto[];
  data: string[] = [];


  ngOnInit() {
    this.getAllAttendanceType();
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllAttendanceType() {

    this.AttendanceTypeService.getAllAttendanceTypes().subscribe((result) => {

      
      this.AttendanceType = result;
      this.intializeDatatable();
      this.fillDatatable();

    });

  }


  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Status', 'Comments', 'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.AttendanceType.length; i++) {

      this.data.push(this.AttendanceType[i].status)
      this.data.push(this.AttendanceType[i].comments)

      this.data.push(this.AttendanceType[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  AddAttendanceType(): void {

    this.addAttendanceTypeModal.show();
  }

  EditAttendanceType(id: string): void {
    this.editAttendanceTypeModal.show(parseInt(id));
  }



  protected delete(id: string): void {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.AttendanceTypeService.delete(parseInt(id))
          .finally(() => {
            this.getAllAttendanceType();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'AttendanceType has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'AttendanceType Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }



}
