import { Component, OnInit, Injector, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { AttendanceTypeDto, AttendanceTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'edit-attendanceType',
  templateUrl: './edit-AttendanceType.component.html',
  styleUrls: ['./edit-AttendanceType.component.scss'],
  providers: [AttendanceTypeServiceProxy]
})
export class EditAttendanceTypeComponent implements OnInit {

  @ViewChild('editAttendanceTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  AttendanceType: AttendanceTypeDto = new AttendanceTypeDto();


  _AttendanceType : FormGroup;

  constructor(injector: Injector,
    private AttendanceTypeService: AttendanceTypeServiceProxy,

    private formBuilder: FormBuilder

  ) { }

  ngOnInit() {
    this.initValidation();
  }
  
  initValidation() {

    this._AttendanceType = this.formBuilder.group({
        AttendanceTypeName : [null , Validators.required]
    });

}

onType() {

  if (this._AttendanceType.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._AttendanceType);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}





  show(id: number): void {

    this.AttendanceTypeService.get(id).finally(() => {

      this.active = true;
      this.modal.show();

    }).subscribe((result: AttendanceTypeDto) => {
      this.AttendanceType = result;
    });

  }


  onShown(): void {

  }


  save(): void {

    this.saving = true;

    this.AttendanceTypeService.update(this.AttendanceType).finally(() => {

      this.saving = false;

    }).subscribe(() => {

      this.notify();
      this.close();
      this.modalSave.emit(null);

    })
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }





}
