import { Component, OnInit, Injector, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { AttendanceTypeDto, AttendanceTypeServiceProxy, ZoneDto, ZoneServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
@Component({
  selector: 'add-attendanceType',
  templateUrl: './add-attendanceType.component.html',
  styleUrls: ['./add-attendanceType.component.scss'],
  providers: [AttendanceTypeServiceProxy]
})
export class AddAttendanceTypeComponent implements OnInit {


  @ViewChild('addAttendanceTypeModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  AttendanceType: AttendanceTypeDto = new AttendanceTypeDto();

  Zone: ZoneDto[];

  isActive: boolean;
  _AttendanceType : FormGroup; // for form validation 


  constructor(injector: Injector,
    private AttendanceTypeService: AttendanceTypeServiceProxy,
    private formBuilder : FormBuilder

  ) { }

  ngOnInit() {
    this.initValidation();
  }


  initValidation() {

    this._AttendanceType = this.formBuilder.group({
        AttendanceTypeName : [null , Validators.required]
    });

}

//check form validation if it is valid then save it else throw error message in form //
onType() {
 
  debugger;
  if (this._AttendanceType.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._AttendanceType);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}

  

  show(): void {

    this.active = true;
    this.modal.show();
    this.AttendanceType = new AttendanceTypeDto();
    this._AttendanceType.markAsUntouched({onlySelf:true}); // to untouched the required form field // 
  }



  save(): void {
    this.saving = true;
    debugger;
    this.AttendanceTypeService.create(this.AttendanceType)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

  onShown():void{
    
  }

}
