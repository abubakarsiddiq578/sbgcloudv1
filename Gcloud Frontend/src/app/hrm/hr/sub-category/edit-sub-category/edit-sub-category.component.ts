import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import{SubCategoryDto, SubCategoryServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy} from "app/shared/service-proxies/service-proxies";
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'edit-sub-category',
  templateUrl: './edit-sub-category.component.html',
  providers:[SubCategoryServiceProxy , HRConfigurationServiceProxy]
})
export class EditSubCategoryComponent implements OnInit {

  @ViewChild('editSubCategoryModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  SubCategory : SubCategoryDto = new SubCategoryDto();
  _subCat : FormGroup
  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(
      injector: Injector,

      private SubCategoryService: SubCategoryServiceProxy,
      private _hrConfigService : HRConfigurationServiceProxy,
      private formBuilder : FormBuilder
      // private _userService: UserServiceProxy
     
  ) {
      // super(injector);
  }


  show(id:number): void{

      
      this.SubCategoryService.get(id).finally(()=>{

      this.active = true;
      this.modal.show();

      }).subscribe((result : SubCategoryDto)=>{

            this.SubCategory = result;

      })
      
  }

  initValidation() {


    this._subCat = this.formBuilder.group({
        //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
        subCatName: [null, Validators.required]
    });

}

onType() {
  debugger;
  if (this._subCat.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._subCat);

  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}



  ngOnInit(): void {
    this.initValidation();
    this.getAllHRConfiguration();
  }


  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];


        if (this.hrConfig.autoCode == true) {

            this.isAutoCode = true;
        }
        else if (this.hrConfig.autoCode == false) {
            this.isAutoCode = false;
        }

    });

}



  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }



  save(): void {

    
    this.saving = true;
  
    this.SubCategoryService.update(this.SubCategory)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
       
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
      this.active = false;
      this.modal.hide();
  }

}
