import { Component, OnInit, ViewChild, Output, EventEmitter, Injector, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import{SubCategoryDto, SubCategoryServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy} from "app/shared/service-proxies/service-proxies";
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'add-sub-category',
  templateUrl: './add-sub-category.component.html',
  providers:[SubCategoryServiceProxy , HRConfigurationServiceProxy]
})
export class AddSubCategoryComponent implements OnInit {

  @ViewChild('addSubCategoryModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  SubCategory: SubCategoryDto = new SubCategoryDto();

  _subCategory : SubCategoryDto[];
  _docNo : abc[] = [];
  _subCat :FormGroup;

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(
      injector: Injector,
      private SubCategoryService : SubCategoryServiceProxy,
      private _hrConfigService : HRConfigurationServiceProxy,
      private formBuilder : FormBuilder
      // private _userService: UserServiceProxy
     
  ) {
      // super(injector);
  }


  show(): void{

      this.active = true;
      this.modal.show();
      this.SubCategory = new SubCategoryDto();

      this.SubCategoryService.getAllSubCategories().subscribe((result)=>{

          this._subCategory = result;
          //this.getAutoCode();

          if(this.hrConfig.autoCode == true){

            this.getAutoCode();
            this.isAutoCode = true ;
          }
          else if(this.hrConfig.autoCode == false){
            this.isAutoCode = false;
          }

      })
      this._subCat.markAsUntouched({onlySelf:true}); 

  }


  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];
    });

}



  initValidation() {


    this._subCat = this.formBuilder.group({
        //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
        subCatName: [null, Validators.required]
    });

}


 /* getAutoCode():void{

    let i =0;
   
    for(i=0 ;i< this._subCategory.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._subCategory[i].code ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0 ){
        debugger;
        code = "1";
        this.SubCategory.code = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                code = this._docNo[this._docNo.length-1].code ;
                code ++;
                if(code <=9){

                    this.SubCategory.code = "00" + code ;
                }
                else if(code <=99){

                    this.SubCategory.code = "0" + code ;
                }
                else if(code <=999){

                    this.SubCategory.code =  code ;
                }
        }                     
    }
    this._docNo = [];
  }*/

  getAutoCode():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._subCategory.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._subCategory[i].code ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0  ){
      
        debugger;
        code = "1";
        this.SubCategory.code = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                let x;
                code = this._docNo[this._docNo.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.SubCategory.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.SubCategory.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.SubCategory.code =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.SubCategory.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.SubCategory.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.SubCategory.code =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.SubCategory.code = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.SubCategory.code =  "0" + code;
                  }
                  else if(code){
  
                      this.SubCategory.code =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.SubCategory.code = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.SubCategory.code =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.SubCategory.code =  temp[0] ;
                  }

                }
        
        }                     
    }
    this._docNo = [];
}




  ngOnInit(): void {
      this.initValidation();
      this.getAllHRConfiguration();
  }




  onType() {
    debugger;
    if (this._subCat.valid) {
        this.save();
    } else {
        this.validateAllFormFields(this._subCat);

    }
   
}

validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
}

  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  save(): void {


    debugger;
    this.saving = true;
    this.SubCategoryService.create(this.SubCategory)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }


  close(): void {
    this.active = false;
    this.modal.hide();
}

}

// class for keeping all docNo number
class abc{
  public code: string;
}


