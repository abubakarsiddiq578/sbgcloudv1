import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';
import { TerminationDto, TerminationServiceProxy, EmployeeServiceProxy, EmployeeDto, HRConfigurationDto, HRConfigurationServiceProxy } from '../../../../shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { TerminationComponent } from '../termination.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
//import {ErrorStateMatcher} from '@angular/material/core';

///////////////////imports for validation////

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

////////////////////////////////////////////

import {Observable} from 'rxjs';
import {map, startWith, count} from 'rxjs/operators';
import { debug } from 'util';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';

/////////////////////////////////

@Component({
    selector: 'add-termination',
    templateUrl: './add-termination.component.html',
    providers: [TerminationServiceProxy, EmployeeServiceProxy, HRConfigurationServiceProxy]
})
export class AddTerminationComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addTerminationModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    employee: EmployeeDto[] = [];
    EmpName : string;

    Termination: TerminationDto = new TerminationDto();
    employeeName: string[] = [];

    _termination: TerminationDto[];

    _docNo: abc[] = [];

    public empName :string ;
    _term: FormGroup // for checking validations

    myControl = new FormControl();
  
    filteredOptions: Observable<EmployeeDto[]>;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;
    

    constructor(
        private TerminationServiceProxy: TerminationServiceProxy,
        private employeeService: EmployeeServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder: FormBuilder
    ) {

    }

    // constructor(
    //     injector: Injector , private TerminationServiceProxy : TerminationServiceProxy,
    //       private employeeService: EmployeeServiceProxy
    //     // private _userService: UserServiceProxy

    // ) {

    //     //super(injector);
    // }

    EmployeeId(id): void {
        debugger
    
        for(let i = 0; i< this.employee.length; i++)
        {
          if(this.employee[i].id == id)
          {
            this.EmpName = this.employee[i].employee_Name;
    
            this.Termination.employeeId = this.employee[i].id;
          }
        }
      }


      addSearchDD(){
        this.searchEmployeeDropdownModel.show()
      }

    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

        });

    }




    initValidation() {


        this._term = this.formBuilder.group({
            //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            employeeId: [null, Validators.required],
            terminationType: [null, Validators.required],
            terminationDate: [null, Validators.required]
            //email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],

        });


    }


    ///////////////////////Validations///////////

 /*    onType() {
        debugger;
        if (this._term.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._term);

        }
    }*/

    onType() {
 
        if (this._term.valid) {
        
          let a,p=0 ;
          for(a=0;a<this.employee.length;a++){
            if(this.Termination.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
          
            this.validateAllFormFields(this._term);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._term);
           
        }
      }

    validateAllFormFields(formGroup: FormGroup) {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

//////////////////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }

  

 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Termination.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  


/////////////////////////////////////////////////////////////////////
    show(): void {

        this.active = true;
        this.modal.show();
        this.Termination = new TerminationDto();
        this.TerminationServiceProxy.getAllTermination().subscribe((result) => {

            this._termination = result;
            //this.getAutoDocNumber();
            if(this.hrConfig.autoCode == true){

                this.getAutoDocNumber();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }

        })

        this.empName = null;
        this.employeeDropdownSearch();
        this._term.markAsUntouched({onlySelf:true});

    }


/*    getAutoDocNumber(): void {

        debugger;
        let i = 0;

        for (i = 0; i < this._termination.length; i++) {

            let abc_;

            abc_ = new abc();

            abc_.code = this._termination[i].documentNo;

            this._docNo.push(abc_);

        }


        let code: any;

        if (this._docNo.length == 0) {
            debugger;
            code = "1";
            this.Termination.documentNo = "00" + code;

        }

        else {

            if (this._docNo[this._docNo.length - 1] != null) {

                code = this._docNo[this._docNo.length - 1].code;
                code++;
                if (code <= 9) {

                    this.Termination.documentNo = "00" + code;
                }
                else if (code <= 99) {

                    this.Termination.documentNo = "0" + code;
                }
                else if (code) {

                    this.Termination.documentNo = code;
                }
            }
        }
        this._docNo = [];
    }*/

    
  getAutoDocNumber():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._termination.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._termination[i].documentNo ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0  ){
      
        debugger;
        code = "1";
        this.Termination.documentNo = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                let x;
                code = this._docNo[this._docNo.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.Termination.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Termination.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Termination.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.Termination.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.Termination.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.Termination.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.Termination.documentNo = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.Termination.documentNo =  "0" + code;
                  }
                  else if(code){
  
                      this.Termination.documentNo =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.Termination.documentNo = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.Termination.documentNo =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.Termination.documentNo =  temp[0] ;
                  }

                }
        
        }                     
    }
    this._docNo = [];
}





    save(): void {
        //this.leaveType.leaveReset = moment(this.resetDate);

        debugger;
        this.saving = true;
        this.TerminationServiceProxy.create(this.Termination)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify();
                this.close();
                this.modalSave.emit(null);
            });
    }


    ngOnInit(): void {
        this.employeeService.getAllEmployees()
            .subscribe((result) => {
                this.employee = result;
              //  console.log("this is employee: ", this.employee);
              //this.employeeDropdownSearch();
            })
        this.initValidation(); 
        this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }


    notify() {
        swal({
            title: "Success!",
            text: "Saved Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

// class for keeping all docNo number
class abc {
    public code: string;
}
