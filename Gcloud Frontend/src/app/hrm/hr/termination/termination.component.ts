// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddTerminationComponent } from './add-termination/add-termination.component';
import { EditTerminationComponent } from './edit-termination/edit-termination.component';
import { TerminationDto, TerminationServiceProxy } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';


declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'termination',
  templateUrl: 'termination.component.html',
  providers: [TerminationServiceProxy]
})

export class TerminationComponent implements OnInit, AfterViewInit {

  @ViewChild('addTerminationModal') addTerminationModal: AddTerminationComponent;

  @ViewChild('editTerminationModal') editTerminationModal: EditTerminationComponent;

  public dataTable: DataTable;


  data: string[] = [];

  Termination: TerminationDto[];

  constructor(private TerminationServiceProxy: TerminationServiceProxy, private _router: Router) {
  }


  globalFunction: GlobalFunctions = new GlobalFunctions
  ngOnInit() {
    if(!this.globalFunction.hasPermission("View","Termination")){
      this.globalFunction.showNoRightsMessage("View")
      this._router.navigate(['']);
    }
    this.getAllTermination();

  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  AddTermination(): void {
    if(!this.globalFunction.hasPermission("Create","Termination")){
      this.globalFunction.showNoRightsMessage("Create")
      return
    }
    this.addTerminationModal.show();
  }

  EditTermination(id: string): void {
    if(!this.globalFunction.hasPermission("Edit","Termination")){
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    this.editTerminationModal.show(parseInt(id));
  }



  initDataTable() {

    this.dataTable = {
      headerRow: ['Doc No', 'Doc Date', 'Termination Date', 'Employee', 'Notice Date', 'Termination Type', 'Detail', 'Actions'],
      footerRow: [/*'Doc No', 'Doc Date', 'Transfer Date', 'Employee', 'Transfer Type', 'Transfer From', 'Transfer To', 'Actions'*/],

      dataRows: []
    };

  }


  fillDataTable() {

    let i;
    for (i = 0; i < this.Termination.length; i++) {

      this.data.push(this.Termination[i].documentNo)
      this.data.push(this.Termination[i].documentDate.toString())
      this.data.push(this.Termination[i].terminationDate.toString())
      this.data.push(this.Termination[i].employee.employee_Name) 
      this.data.push(this.Termination[i].noticeDate.toString())
      this.data.push(this.Termination[i].terminationType)
      this.data.push(this.Termination[i].detail)
      this.data.push(this.Termination[i].id.toString())

      //this.data.push(this.Termination[i].detail.toString()) 


      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }






  getAllTermination(): void {
    this.TerminationServiceProxy.getAllTermination()
      .subscribe((result) => {
        debugger;
        this.initDataTable()
        this.Termination = result
        this.fillDataTable();
      });
  }





  protected delete(id: string): void {

    if(!this.globalFunction.hasPermission("Delete","Termination")){
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }

    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.TerminationServiceProxy.delete(parseInt(id))
          .finally(() => {
            this.getAllTermination();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Your Termination has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Termination is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

}


