import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';
//import { TerminationDto,, TerminationServiceProxy , EmployeeServiceProxy} from 'app/shared/service-proxies/service-proxies';
import { TerminationDto, TerminationServiceProxy, EmployeeServiceProxy, EmployeeDto, HRConfigurationDto, HRConfigurationServiceProxy } from '../../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';

// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { TerminationComponent } from '../termination.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
//import {ErrorStateMatcher} from '@angular/material/core';

///////////////////imports for validation////

import { FormBuilder, AbstractControl } from '@angular/forms';
import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';

////////////////////////////////////////////
import {Observable} from 'rxjs';
import {map, startWith, count} from 'rxjs/operators';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';

@Component({
  selector: 'edit-termination',
  templateUrl: './edit-termination.component.html',
  providers: [TerminationServiceProxy, EmployeeServiceProxy , HRConfigurationServiceProxy] 
})
export class EditTerminationComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editTerminationModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    employee: EmployeeDto[] = [];

    Termination: TerminationDto = new TerminationDto();

    _term : FormGroup // for checking validations

    public empName :string ;
  

    myControl = new FormControl();
  
    filteredOptions: Observable<EmployeeDto[]>;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;
    EmpName: string;
  

    constructor(
        injector: Injector , private TerminationServiceProxy : TerminationServiceProxy ,  private employeeService: EmployeeServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder
         //private _userService: UserServiceProxy
       
    ) {

        //super(injector);
    }
  
    EmployeeId(id): void {
        debugger
    
        for(let i = 0; i< this.employee.length; i++)
        {
          if(this.employee[i].id == id)
          {
            this.EmpName = this.employee[i].employee_Name;
    
            this.Termination.employeeId = this.employee[i].id;
          }
        }
      }
    
  addSearchDD(){
    this.searchEditEmployeeDropdownModel.show()
  }

    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];


            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }

        });

    }
    

    /*show(): void{

        this.active = true;
        this.modal.show();
        
    }*/

    show(id: number): void{
        debugger
        this.TerminationServiceProxy.get(id)
        .finally(() => {
            this.active = true;
            this.modal.show();
        })
        .subscribe((result: TerminationDto) => {
            debugger
            this.Termination = result;
            
            this.employeeService.get(result.id).subscribe((result)=>{
                this.empName = result.employee_Name;
            });
            
        });
        this.employeeDropdownSearch();
        
    }

///////////////////////////////////////////////////////////////////
    initValidation(){


        this._term = this.formBuilder.group({
            //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            employeeId: [null, Validators.required],
            terminationType :[null, Validators.required],
            terminationDate:[null, Validators.required]
            //email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            
           });    
       }

    /*onType() {
     if (this._term.valid) {
       this.save();
     } else {
        this.validateAllFormFields(this._term);
        
     }
   }*/

   onType() {
 
    if (this._term.valid) {
    
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.Termination.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
      
        this.validateAllFormFields(this._term);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._term);
       
    }
  }
   
   validateAllFormFields(formGroup: FormGroup) {
   
     Object.keys(formGroup.controls).forEach(field => {
       const control = formGroup.get(field);
       if (control instanceof FormControl) {
         control.markAsTouched({ onlySelf: true });
       } else if (control instanceof FormGroup) {
         // this.validateAllFormFields(control);
       }
     });
   }
 ////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }

  

 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Termination.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  


/////////////////////////////////////////////////////////////////////


    ngOnInit(): void {
        
        this.employeeService.getAllEmployees()
        .subscribe((result)=> {
            this.employee = result;
           console.log("this is employee: ", this.employee);

        })
        this.initValidation();
        this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    
    notify(){
        swal({
            title: "Success!",
            text: "Record Updated Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }

    save(): void {
        this.saving = true;
     
        this.TerminationServiceProxy.update(this.Termination)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
    }

}
