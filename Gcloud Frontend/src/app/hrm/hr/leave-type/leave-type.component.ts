import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddLeaveTypeComponent } from './add-leave-type/add-leave-type.component';
import { EditLeaveTypeComponent } from './edit-leave-type/edit-leave-type.component';
import { LeaveTypeDto, LeaveTypeServiceProxy } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  //footerRow: string[];
  dataRows: string[][];
}

declare const $: any;
@Component({
  selector: 'leave-type',
  templateUrl: './leave-type.component.html',
  providers: [LeaveTypeServiceProxy] 
})
export class LeaveTypeComponent implements OnInit, AfterViewInit {

  @ViewChild('addLeaveTypeModal') addLeaveTypeModal: AddLeaveTypeComponent;
  @ViewChild('editLeaveTypeModal') editLeaveTypeModal: EditLeaveTypeComponent;

    public dataTable: DataTable;

    data: string[] = [];

    leaveType: LeaveTypeDto[];

    constructor(private leaveTypeServiceProxy : LeaveTypeServiceProxy,private _router : Router) 
    {
    }
    globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      debugger;
      if (!this.globalFunction.hasPermission("View", "LeaveType")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
      this.getAllLeaveTypes();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        //alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }


  AddLeaveType(): void {
    if (!this.globalFunction.hasPermission("Create", "LeaveType")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }
      this.addLeaveTypeModal.show();
  }

  EditLeaveType(id: string): void{
    if (!this.globalFunction.hasPermission("Edit", "LeaveType")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
      this.editLeaveTypeModal.show(parseInt(id));
  }

  initDataTable(): void{
    this.dataTable = {
      headerRow: [ 'Leave Type', 'Leave Allowed', 'Leave Reset', 'Detail', 'Actions' ],
      //footerRow: [ 'Leave Type', 'Leave Allowed', 'Leave Reset', 'Detail', 'Actions' ],

      dataRows: []
   };
  }

  fillDataTable(){

    let i;
    for (i= 0; i < this.leaveType.length; i++) {

      this.data.push(this.leaveType[i].leaveTypeName)
      this.data.push(this.leaveType[i].leaveAllowed.toString())
      this.data.push( this.leaveType[i].leaveReset.toDate().toDateString())
      this.data.push(this.leaveType[i].leaveDetail)
      this.data.push(this.leaveType[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];
    }
  }

  getAllLeaveTypes(): void{
    this.leaveTypeServiceProxy.getAllLeaveTypes()
    .subscribe((result) => {
      this.initDataTable()
      this.leaveType = result
      this.fillDataTable();   
    });
  }

  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "LeaveType")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Loan Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
    if (result.value) {
      this.leaveTypeServiceProxy.delete(parseInt(id))
        .finally(() => {
          this.getAllLeaveTypes();
        })
        .subscribe(() => {
          swal({
            title: 'Deleted!',
            text: 'Your Leave Type has been deleted.',
            type: 'success',
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
        }).catch(swal.noop)
         });
    } else {
      swal({
          title: 'Cancelled',
          text: 'Your Leave Type is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
      }).catch(swal.noop)
    }
  })
  }
}
