import { Component, OnInit, Injector, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { LeaveTypeDto, LeaveTypeServiceProxy } from '../../../../shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'edit-leave-type',
  templateUrl: './edit-leave-type.component.html',
  providers: [LeaveTypeServiceProxy] 
})
export class EditLeaveTypeComponent implements OnInit {

  @ViewChild('editLeaveTypeModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    public resetDate = new Date();

    leaveType: LeaveTypeDto = new LeaveTypeDto();

    _leaveTypeValidation : FormGroup

    constructor(private leaveTypeServiceProxy : LeaveTypeServiceProxy , private formBuilder : FormBuilder) {
    }
  

    show(id: number): void{
        this.leaveTypeServiceProxy.get(id)
        .finally(() => {
            this.active = true;
            this.modal.show();
        })
        .subscribe((result: LeaveTypeDto) => {
            this.leaveType = result;

            this.resetDate = this.leaveType.leaveReset.toDate();
        });
    }

     /////////////////////////////////////////////////////////////////////////
     initValidation() {

        this._leaveTypeValidation = this.formBuilder.group({
            leaveAllowed: [null, Validators.required],
            
            
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    onType() {
    
      if (this._leaveTypeValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._leaveTypeValidation);
      }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }






    ///////////////////////////////////////////////////////////////////////////
  


    ngOnInit(): void {
        this.initValidation();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    doTextareaValueChange(ev){
        try {
          this.leaveType.leaveDetail = ev.target.value;
        } catch(e) {
          console.info('could not set textarea-value');
        }
    }

    notify(){
        swal({
            title: "Success!",
            text: "Record Updated Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }

    save(): void {
        this.saving = true;
        this.leaveType.leaveReset = moment(this.resetDate).add(5,'hour');

        this.leaveTypeServiceProxy.update(this.leaveType)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
    }
}
