import { Component, OnInit ,ViewChild, Injector, Output, EventEmitter, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';

import { ItemWiseProductionComponent } from '../item-wise-production.component';
import { ItemWiseProductionDetailDto , ItemWiseProductionDetailServiceProxy , ItemWiseProductionMasterDto , ItemWiseProductionMasterServiceProxy ,CategoryDto , CategoryServiceProxy , SubCategoryDto , SubCategoryServiceProxy , ItemTypeServiceProxy , ItemTypeDto , WorkGroupMasterDto , WorkGroupMasterServiceProxy, WorkGroupEmployeeDetail, EmployeeDto , EmployeeServiceProxy } from 'app/shared/service-proxies/service-proxies';

import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
@Component({
  selector: 'edit-quantity',
  templateUrl: './edit-item-wise-production.component.html',
  styleUrls: ['./edit-item-wise-production.component.scss'],
  providers:[ItemWiseProductionMasterServiceProxy , ItemWiseProductionDetailServiceProxy]

})
export class EditItemWiseProductionComponent implements OnInit {

  
  @ViewChild('editQuantityModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  empId:number;
  mId : number;

  editData : any; // for taking input for edit input//


  indx : number;
  colIndx : number;

  itemWiseProductionDetail : ItemWiseProductionDetailDto = new ItemWiseProductionDetailDto();

  itemWiserProductionMaster : ItemWiseProductionMasterDto = new ItemWiseProductionMasterDto();



  constructor(
      injector: Injector,
      private _itemWiseProductionDetailService : ItemWiseProductionDetailServiceProxy ,
      private _itemWiseProductionMasterService : ItemWiseProductionMasterServiceProxy ,  private _itemTypeService : ItemTypeServiceProxy, 
  
       private _categoryService : CategoryServiceProxy , private _subCategoryService : SubCategoryServiceProxy
     
  ) {
      
  }


  show(data, i, c  ): void{

      //this.mId = mId ;
      
      //this.editData.employeeId = empId;

      this.indx = i;
      this.colIndx = c;
      this.editData = data ;

      this.active = true;
      this.modal.show();

      this.editData = data ;

      
  }



  ngOnInit(): void {
  }


  onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }


  save():void{

    this.close();
    debugger;

    //this.editData.itemWiseProductListLatest.total = this.editData.itemWiseProductListLatest.quantity + this.editData.itemWiseProductListLatest.extraQuantity + this.editData.itemWiseProductListLatest.repairQuantity;
    this.editData.total = this.editData.quantity + this.editData.extraQuantity + this.editData.repairQuantity;
    this.modalSave.emit(this.editData);
    
    
  }

  close(): void
  {
      this.active = false;
      this.modal.hide();
   }

}
