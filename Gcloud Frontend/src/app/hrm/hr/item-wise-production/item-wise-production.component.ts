import { Component, OnInit  ,  Output, AfterViewInit, ViewChild, Inject, ElementRef} from '@angular/core';
import swal from 'sweetalert2';

import { ItemWiseProductionDetailDto , ItemWiseProductionDetailServiceProxy , ItemWiseProductionMasterDto , ItemWiseProductionMasterServiceProxy, WorkGroupItemDetailDto , WorkGroupItemDetailServiceProxy ,CategoryDto , CategoryServiceProxy , SubCategoryDto , SubCategoryServiceProxy , ItemTypeServiceProxy , ItemTypeDto , WorkGroupMasterDto , WorkGroupMasterServiceProxy, WorkGroupEmployeeDetail, WorkGroupEmployeeDetailDto, WorkGroupEmployeeDetailServiceProxy , ItemDto , ItemServiceProxy, WorkGroupMaster, Item, ItemWiseProductionMaster } from 'app/shared/service-proxies/service-proxies';
import { EditItemWiseProductionComponent } from './edit-item-wise-production/edit-item-wise-production.component';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
//import { Observable } from 'rxjs/Observable';
import { forkJoin, Observable } from 'rxjs';
import 'rxjs/add/observable/forkJoin';
declare const $: any;
import * as moment from 'moment';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-item-wise-production',
  templateUrl: './item-wise-production.component.html',
  styleUrls: ['./item-wise-production.component.scss'],
  providers:[ WorkGroupMasterServiceProxy ,WorkGroupItemDetailServiceProxy, ItemWiseProductionDetailServiceProxy , WorkGroupEmployeeDetailServiceProxy, ItemWiseProductionMasterServiceProxy , CategoryServiceProxy , SubCategoryServiceProxy , ItemTypeServiceProxy , WorkGroupMasterServiceProxy]
})

export class ItemWiseProductionComponent implements OnInit, AfterViewInit {

  public dataTable: DataTable;
  public dataTableHistory: DataTable;

  documentDate : Date = new Date();
  isDetail: boolean = true;
  isHistory: boolean = false;
  isSaveBtn: boolean = true;
  isEditBtn: boolean = false;

  _itemWiseProductionMaster : ItemWiseProductionMasterDto[];

  itemWiseProductionDetail : ItemWiseProductionDetailDto = new ItemWiseProductionDetailDto();

  itemWiserProductionMaster : ItemWiseProductionMasterDto = new ItemWiseProductionMasterDto();

  _workGroupItem : WorkGroupItemDetailDto[];

  _itemType : ItemTypeDto[];
  
  _category : CategoryDto[];

  _subCategory : SubCategoryDto[];


  _item : ItemDto[];

  itemWiseProductionDetailListt  = []; //for storing list from DataList

  public quantity : number =0;
  public extraQuantity : number=0;
  public repairQuantity:number =0;
  public total : number = 0 ;


  itemTypeId :number ;

  categoryId : number ;

  subCategoryId: number;

  workGroupEmployee : WorkGroupEmployeeDetailDto[] = []; //EmployeeDto[] = [];

  itemWiseProductionDetailList = [];

  abc  :  WorkGroupMasterDto[];

  data:string[] = [];
  dataMaster:string[] = [];

  ab = new Observable();

  //itemObj : Item;

  public itemsData : Item[] = [];

  uniqueItem : Item[] = [];

  DataList : dataStore[] = [];

  typeId : number;
  catId :number;
  subCatId : number; // temp variable for mapping itemtype,cat , subCatId to item
  prodMasterItem :itemStoreMaster[] = []; 

  tempArr:WorkGroupItemDetailDto[];

  tmpItem : any[]= [];
  gf: GlobalFunctions = new GlobalFunctions

 
  constructor( @Inject(LOCAL_STORAGE) private storage: WebStorageService, private _itemWiseProductionDetailService : ItemWiseProductionDetailServiceProxy ,
    private _itemWiseProductionMasterService : ItemWiseProductionMasterServiceProxy ,  private _itemTypeService : ItemTypeServiceProxy, 

     private _categoryService : CategoryServiceProxy , private _subCategoryService : SubCategoryServiceProxy , private _workGroupItemService : WorkGroupItemDetailServiceProxy,

     private _workGroupMasterService : WorkGroupMasterServiceProxy

 ) { }




  @ViewChild('editQuantityModal') editQuantityModal: EditItemWiseProductionComponent;
  @ViewChild('fromDate') fromDate: ElementRef;
  @ViewChild('toDate') toDate: ElementRef;
  ngOnInit() {
    this.itemWiserProductionMaster.docDate = moment().add(5, 'hour') ;
      this.initDataTable();
      this.getAllCategories();
      this.getAllItemType();
      this.getAllSubCategories();
      this.initMasterDataTable();
      this.getAllItemWiseProductionMaster();
  }


  filterData(){
    if(this.fromDate.nativeElement.value == "" || this.toDate.nativeElement.value == ""){
      this.gf.showErrorMessage("please select To Date or From Date", 'top', 'right')
      return
  }
  this._itemWiseProductionMasterService.getFilterdItemWiseProductions(moment(this.fromDate.nativeElement.value.toString()), moment(this.toDate.nativeElement.value.toString()))
  .subscribe((result) => {
    this._itemWiseProductionMaster = result
    this.initMasterDataTable()
    this.fillMasterDataTable()
  })
  }


  initDataTable(){

    this.dataTable = {
      headerRow: [ 'ItemName ', 'Quantity', 'Extra Quantity', 'Repair Quantity', 'Total', 'Actions' ],
      footerRow: [ 'ItemName', 'Quantity', 'Extra Quantity', 'Repair Quantity', 'Total', 'Actions' ],

      dataRows: []
   };

  }


  getAllItemType(){

  this._itemTypeService.getAllItemTypes().subscribe((result)=>{
    
    this._itemType = result;

  });
  }

  
  getAllCategories(){

    this._categoryService.getAllCategories().subscribe((result)=>{
        this._category = result;
    });
  }

getAllSubCategories(){

  this._subCategoryService.getAllSubCategories().subscribe((result)=>{

      this._subCategory = result;
  })
}


  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    $('#datatables1').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });
    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function(e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
     // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function(e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function(e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  fillDataTable()
  {
      let a =0;
      let b=0;
      //for(b=0;b<this.itemWiseProductionDetailList.length;b++){
        debugger;
      for(b=0;b<this.DataList.length;b++){


        //Change Requirements//


        let d=0;
        for(d=0;d<this.DataList[b].itemWiseProductListLatest.length;d++)
        {


          this.data.push(this.DataList[b].itemWiseProductListLatest[d].workGroupItemDetail.workGroupMaster.groupName);

          this.data.push(this.DataList[b].itemWiseProductListLatest[d].quantity.toString());

          this.data.push(this.DataList[b].itemWiseProductListLatest[d].extraQuantity.toString());

          this.data.push(this.DataList[b].itemWiseProductListLatest[d].repairQuantity.toString());

          this.data.push(this.DataList[b].itemWiseProductListLatest[d].total.toString());

          this.data.push(this.DataList[b].itemWiseProductListLatest[d].workGroupItemDetail.id.toString());

          this.dataTable.dataRows.push(this.data);

          this.data = [];

        }
      
      }


  }
  //i is the item index whereas c is the column index
  EditQuantity(id : string  , itemId :string, i: any , c: any ): void {
    
    debugger;
      let itemWiseProductionDetail;

      //itemWiseProductionDetail = this.itemWiseProductionDetailList.find(function (obj) { return obj.id === parseInt(id); });

      //itemWiseProductionDetail = this.DataList.find(function (obj) { return obj.itemWiseProductListLatest[0].id == parseInt(id); });

      itemWiseProductionDetail = this.DataList[i].itemWiseProductListLatest.find(function (obj) { return obj.id == parseInt(id)  });

      this.editQuantityModal.show(itemWiseProductionDetail, i, c);
  }

  initMasterDataTable(){
    
    this.dataTableHistory = {
      headerRow: [ 'DocNo','DocDate', 'Remarks','Actions' ],
      footerRow: [ 'DocNo', 'DocDate','Remarks','Actions' ],

      dataRows: []
   };

  }

  fillMasterDataTable(){

    let p=0;
    for(p=0;p<this._itemWiseProductionMaster.length;p++){

        this.dataMaster.push(this._itemWiseProductionMaster[p].docNo.toString());
        this.dataMaster.push(this._itemWiseProductionMaster[p].docDate.toString());
        this.dataMaster.push(this._itemWiseProductionMaster[p].remarks);
        this.dataMaster.push(this._itemWiseProductionMaster[p].id.toString());
        this.dataTableHistory.dataRows.push(this.dataMaster);
        this.dataMaster = [];
        
    }
  }

  getAllItemWiseProductionMaster(){

    this._itemWiseProductionMasterService.getAllItemWiseProductions().subscribe((result)=>{

      debugger;
      this.initMasterDataTable();
      this._itemWiseProductionMaster = result;
      this.fillMasterDataTable();
      
    })
  }
   l: Number;
  addToGrid(){

    this.itemsData = [];
    this.uniqueItem = [];
    this.DataList = [];
   // itemType:number = this.itemWiserProductionMaster.itemTypeId
    this.initDataTable();
   

    ///Change Requirement//
    this._itemWiseProductionMasterService.getWorkGroupItems(this.itemWiserProductionMaster.itemTypeId , this.itemWiserProductionMaster.subCategoryId , this.itemWiserProductionMaster.categoryId)
    .subscribe((result)=>{
      debugger;
      this._workGroupItem = result;
      
      let p =0;
      let observables = new Array();
      for(p=0;p<this._workGroupItem.length;p++)
      {
       
        this.itemsData.push(this._workGroupItem[p].item);

        //observables.push(this._workGroupItemService.get(this._workGroupItem[p].id));
        observables.push(this._itemWiseProductionMasterService.getWorkGroupItemById(this._workGroupItem[p].id));

      }
      
      //filtering  unique item and placing it another array
      let m = 0;
      for(m = 0 ;m< this.itemsData.length;m++)
      {
        
        let found =0;
        let j=0;
        for(j=m+1;j<this.itemsData.length;j++){
          
          if(this.itemsData[j].id == this.itemsData[m].id)
          {
              found = 1;
          }   
        }

        if(found  ==0 )
        {
          this.uniqueItem.push(this.itemsData[m]);
        }
      }

      
      console.log("",this.uniqueItem);

      Observable.forkJoin(observables)
      .subscribe(itmData => {
        // All observables in `observables` array have resolved and `dataArray` is an array of result of each observable
          let e;
          let t = 0;
          let u =0;
          this.tmpItem = itmData;
         // l: Number;
          
          for(u=0;u<this.uniqueItem.length;u++)
          {
            
            let dataStoreObj;
            dataStoreObj  = new dataStore();
            dataStoreObj.itemData = this.uniqueItem[u];
            for(t=0;t<this._workGroupItem.length;t++)
             {
                  if(this.uniqueItem[u].id == this._workGroupItem[t].itemId )
                  {            
                    this.itemWiseProductionDetail.quantity = 0;
                    this.itemWiseProductionDetail.extraQuantity = 0;
                    this.itemWiseProductionDetail.repairQuantity = 0;
                    this.itemWiseProductionDetail.total = 0;
                    
                    this.itemWiseProductionDetail.workGroupItemDetailId = this._workGroupItem[t].id;
                    //this.itemWiseProductionDetail.workGroupItemDetail = this._
                    //this.itemWiseProductionDetail.workGroupItemDetail.workGroupMaster.groupName  = this._workGroupItem[t].workGroupMaster.groupName;
                   // this.itemWiseProductionDetail.workGroupItemDetail = this._workGroupItem[t];
                  
                  debugger;
                   itmData.forEach(element => {
                    let p = element;
                    let a = p[0].id ;
                    if(this.itemWiseProductionDetail.workGroupItemDetailId == a)
                    { 
                          this.itemWiseProductionDetail.workGroupItemDetail = p[0]
                    }

                  });

                    

                    this.itemWiseProductionDetail.id = this._workGroupItem[t].id; // to store temp id for edit quantity//
                    //this.itemWiseProductionDetailList.push(this.itemWiseProductionDetail);
                    
                    dataStoreObj.itemWiseProductListLatest.push(this.itemWiseProductionDetail);


                    this.itemWiseProductionDetail = new ItemWiseProductionDetailDto();

                  }              
              
              }

              this.DataList.push(dataStoreObj);

            }
   
        this.initDataTable();
        this.fillDataTable();
      
      });


    });
    
  }


  updateItemWiseProductionDetail(itemWiseProductionEditData) {
    this.initDataTable();
    //this.fillEditProductionDetailDataTable();
  }


  fillEditProductionDetailDataTable(){

    let b=0;
    for(b=0;b<this.itemWiseProductionDetailList.length;b++){

      this.data.push(this.itemWiseProductionDetailList[b].employee.employee_Name.toString());
      
      this.data.push(this.itemWiseProductionDetailList[b].quantity.toString());
      
      this.data.push(this.itemWiseProductionDetailList[b].extraQuantity.toString());
      
      this.data.push(this.itemWiseProductionDetailList[b].repairQuantity.toString());
      
      this.data.push(this.itemWiseProductionDetailList[b].total.toString());
      
     
      this.data.push(this.itemWiseProductionDetailList[b].employeeId.toString()); 
      
      this.dataTable.dataRows.push(this.data);

      this.data = [];
  
    }

  }

//////////////////////////////////////////////////////////////

  // delete data of production detail record from detail datatable 
  DeleteItemWiseProductionDetail(id: string , i: any){

    debugger
    let ProdDetail;

    // get record of matching id
    //ProdDetail = this.itemWiseProductionDetailList.find(function (obj) { return obj.employeeId === parseInt(id); });
    
    ProdDetail = this.DataList[i].itemWiseProductListLatest.find(function (obj) { return obj.id == parseInt(id)  });

    // perform delete operation to delete object from detail list 
    //const index: number = this.itemWiseProductionDetailList.indexOf(ProdDetail);
    const index: number = this.DataList[i].itemWiseProductListLatest.indexOf(ProdDetail);
    if (index !== -1) {
        //this.itemWiseProductionDetailList.splice(index, 1);

        this.DataList[i].itemWiseProductListLatest.splice(index, 1);


        //this.initDataTable();
        //this.fillEditProductionDetailDataTable();
    }
  }
  ////////////////////////

  // saving 

  save(): void {


    if( this.DataList.length >0 ){    
    debugger
    // set tenant id that get from session  
     if (this.storage.get('TenantId') > 0) {
         this.itemWiserProductionMaster.tenantId = this.storage.get('TenantId');
       }

    // set master id undefined to save record
    this.itemWiserProductionMaster.id = undefined;
    
    

    let p=0;
    let n=0;
    for(p=0;p<this.DataList.length;p++){

      for(n=0;n< this.DataList[p].itemWiseProductListLatest.length; n++){

          this.DataList[p].itemWiseProductListLatest[n].workGroupItemDetail = undefined; // workgroupitem object to undefined
          this.DataList[p].itemWiseProductListLatest[n].id = undefined; // setting id to undefined//

          this.itemWiseProductionDetailListt.push(this.DataList[p].itemWiseProductListLatest[n]);

      }

    }

    //storing collection of item list in detail 
    this.itemWiserProductionMaster.itemWiseProductionDetails = this.itemWiseProductionDetailListt ;

    //adding itemwiseproduction Master
    this._itemWiseProductionMasterService.add(this.itemWiserProductionMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Saved Successfully');

        // perform reset operation after saving record
        this.RefreshControls();
        this.initDataTable();
    

        this.getAllItemWiseProductionMaster();
        this.ActiveHistory();
        this.itemWiseProductionDetailList = [];

        
      });
    }
    else{

      this.notifyMsg('Warning!', 'Cannot Saved Empty Record');

    }



  }
//////////////////////////



  EditMasterDetail(id: string) {

    debugger

    this.itemWiserProductionMaster.id = parseInt(id); // assigning master id for editing master record

   
    //Change Requirements//


    this._itemWiseProductionMasterService.getAllProductionDetail(parseInt(id))
      .subscribe((result) => {

        this.DataList = [];

        this.itemWiseProductionDetailListt = result;


        let w = result[0].itemWiseProductionMasterId;

        this._itemWiseProductionMasterService.get(w).subscribe((dd) => { // gettin data on basis of particular master id

        this.itemWiserProductionMaster = dd ;

          //this.typeId = dd.itemTypeId;
          //this.catId = dd.categoryId;
          //this.subCatId = dd.subCategoryId;


          this.LoadDataToTable(this.itemWiserProductionMaster ,  this.itemWiseProductionDetailListt)


          //to load data in grid for editing
          //this.LoadToGrid(this.itemWiseProductionDetailListt, this.typeId, this.subCatId, this.catId);

          this.initDataTable();
          ///this.fillDataTable();
          this.ActiveUpdateBtn();
          this.ActiveDetail();

        });


      });
  }

  LoadDataToTable(itemWisePM: ItemWiseProductionMasterDto, itemWiseProductionDetailListtt: any[]) 
  {

    this.itemsData = [];
    this.uniqueItem = [];
    this.DataList = [];
    this.itemWiseProductionDetail = new ItemWiseProductionDetailDto();
    this.initDataTable();
    let p = 0;
    for (p = 0; p < itemWiseProductionDetailListtt.length; p++) {

      this.itemsData.push(itemWiseProductionDetailListtt[p].workGroupItemDetail.item);

      // observables.push(this._workGroupMasterService.get(this._workGroupItem[p].workGroupMasterId)); //Getting all master records
    }

    // making itemData non repitive and assigning in new array//
    let m = 0;
    for (m = 0; m < this.itemsData.length; m++) {
      let found = 0;
      let j = 0;
      for (j = m + 1; j < this.itemsData.length; j++) {

        if (this.itemsData[j].id == this.itemsData[m].id) {
          found = 1;
        }
      }

      if (found == 0) {
        this.uniqueItem.push(this.itemsData[m]);
      }
    }

    debugger

    let a = itemWiseProductionDetailListtt[0].workGroupItemDetail.item.id;
    ///itemWiseProductionDetailListtt is editable list
    let c = 0;
    let q = 0;
    for (c = 0; c < this.uniqueItem.length; c++) {

      let dataStoreObj;
      dataStoreObj = new dataStore();
      dataStoreObj.itemData = this.uniqueItem[c];
      for (q = 0; q < itemWiseProductionDetailListtt.length; q++) {

        if (this.uniqueItem[c].id == itemWiseProductionDetailListtt[q].workGroupItemDetail.item.id ) {

          this.itemWiseProductionDetail.quantity = itemWiseProductionDetailListtt[q].quantity;
          this.itemWiseProductionDetail.extraQuantity = itemWiseProductionDetailListtt[q].extraQuantity;
          this.itemWiseProductionDetail.repairQuantity = itemWiseProductionDetailListtt[q].repairQuantity;
          this.itemWiseProductionDetail.total = itemWiseProductionDetailListtt[q].total;

          this.itemWiseProductionDetail.workGroupItemDetail = itemWiseProductionDetailListtt[q].workGroupItemDetail;

          this.itemWiseProductionDetail.id = itemWiseProductionDetailListtt[q].id; // to store temp id for edit quantity//

          this.itemWiseProductionDetail.workGroupItemDetailId = itemWiseProductionDetailListtt[q].workGroupItemDetail.id

          dataStoreObj.itemWiseProductListLatest.push(this.itemWiseProductionDetail);


          this.itemWiseProductionDetail = new ItemWiseProductionDetailDto();

        }
      }
      this.DataList.push(dataStoreObj);
    }

    this.initDataTable()

  }



//Refresh Controls
RefreshControls() {

    this.itemTypeId = undefined;
    this.categoryId = undefined;
    this.subCategoryId = undefined;
    //this.itemWiserProductionMaster = new ItemWiseProductionMaster();
    this.itemWiserProductionMaster = new ItemWiseProductionMasterDto();
    this.itemWiseProductionDetailList =  [];
    this.DataList = [];
    this.itemWiseProductionDetailListt = [];
    this.initDataTable();
    this.ActiveSaveBtn();

}


 // perform edit operation to edit data 
 edit(): void {


  debugger
  if (this.storage.get('TenantId') > 0) {
    this.itemWiserProductionMaster.tenantId = this.storage.get('TenantId');
  }

    this.itemWiseProductionDetailListt = [];
    let p=0;
    let n=0;
    for(p=0;p<this.DataList.length;p++){

      for(n=0;n< this.DataList[p].itemWiseProductListLatest.length; n++){

         this.DataList[p].itemWiseProductListLatest[n].workGroupItemDetail = undefined; // master object to undefined
          //this.DataList[p].itemWiseProductListLatest[n].id = undefined; // setting id to undefined//
          this.itemWiseProductionDetailListt.push(this.DataList[p].itemWiseProductListLatest[n]);

      }

    }

  this.itemWiserProductionMaster.itemWiseProductionDetails = null; // making the old list empty 
  this.itemWiserProductionMaster.itemWiseProductionDetails = this.itemWiseProductionDetailListt; // setting the new updated list 
  
  this._itemWiseProductionMasterService.edit(this.itemWiserProductionMaster)
    .finally(() => {
    })
    .subscribe(() => {
      this.notifyMsg('Success!', 'Updated Successfully');

      this.initDataTable();
      this.getAllItemWiseProductionMaster();

      this.itemWiseProductionDetailList = [];
     
      this.RefreshControls();


    });
}



  LoadToGrid(prodDetailDto: ItemWiseProductionDetailDto[] , typeId : number , subCatId : number , catId : number) {
  
////////////////////////////////////////////////////////////////////////////////////
  this.itemsData = [];
  this.uniqueItem = [];
  this.DataList = [];
  this.itemWiseProductionDetail = new ItemWiseProductionDetailDto();
  this.initDataTable();
  
  //get data on basis of typeId , subCatId , catId//
  this._itemWiseProductionMasterService.getWorkGroupItems(this.typeId, this.subCatId , this.catId)
  .subscribe((result)=>{
    debugger;
    this._workGroupItem = result;
    
    let p =0;
    let observables = new Array(); // creating array so that push multiple call on basis work group master id and then pass in forkjoin // 
    for(p=0;p<this._workGroupItem.length;p++)
    {
     
      this.itemsData.push(this._workGroupItem[p].item);

      observables.push(this._workGroupMasterService.get(this._workGroupItem[p].workGroupMasterId)); //Getting all master records
    }

    // making itemData non repitive and assigning in new array//
    let m = 0;
    for(m = 0 ;m< this.itemsData.length;m++)
    {
      let found =0;
      let j=0;
      for(j=m+1;j<this.itemsData.length;j++){
        
        if(this.itemsData[j].id == this.itemsData[m].id)
        {
            found = 1;
        }   
      }

      if(found  ==0 )
      {
        this.uniqueItem.push(this.itemsData[m]);
      }
    }

    
    debugger;
    console.log("",this.uniqueItem);
  
    
    Observable.forkJoin(observables)
    .subscribe(dataArray => {
      // All observables in `observables` array have resolved and `dataArray` is an array of result of each observable
        let e;
        let t = 0;// counter for workgroupItems
        let u =0;// counter for unique item 
        let z= 0 ;//counter for prodItemDetails //
        let  repeat : boolean = false;
        //loop until length of unique item // 
        for(u=0;u<this.uniqueItem.length;u++)
        {
          //z=0 ; 
          repeat = false;
          let dataStoreObj;
          dataStoreObj  = new dataStore();
          dataStoreObj.itemData = this.uniqueItem[u];
          for(t=0;t<this._workGroupItem.length;t++)
           {
                //let repeat =  false;
                if( this.uniqueItem[u].id == this._workGroupItem[t].itemId )
                {            
                  if (t < prodDetailDto.length  ) {
                    this.itemWiseProductionDetail.quantity = prodDetailDto[t].quantity;
                    this.itemWiseProductionDetail.extraQuantity = prodDetailDto[t].extraQuantity;
                    this.itemWiseProductionDetail.repairQuantity = prodDetailDto[t].repairQuantity;
                    this.itemWiseProductionDetail.total = prodDetailDto[t].total;

                  //  this.itemWiseProductionDetail.workGroupMasterId = this._workGroupItem[t].workGroupMasterId;
           
                    for (e = 0; e < dataArray.length; e++) {
                      let n = 0;
                     /* if (this.itemWiseProductionDetail.workGroupMasterId == dataArray[e].id) {
                        this.itemWiseProductionDetail.workGroupMaster = dataArray[e]
                      }*/

                    }

                    // this.itemWiseProductionDetail.id = this._workGroupItem[t].workGroupMasterId; // to store temp id for edit quantity//
                    //this.itemWiseProductionDetailList.push(this.itemWiseProductionDetail);

                    this.itemWiseProductionDetail.id = prodDetailDto[t].id; // to store temp id for edit quantity//

                   /*if(z>=2){

                        repeat = true ; //  set true ;
                    }else{
                      
                     repeat = false ; 
                    }*/


                    dataStoreObj.itemWiseProductListLatest.push(this.itemWiseProductionDetail);


                    this.itemWiseProductionDetail = new ItemWiseProductionDetailDto();
                  
                  }
               }              
            
           }

  

            this.DataList.push(dataStoreObj);

          }


      
      this.initDataTable();
      //this.fillDataTable();
    
    });


  });
  
}

sortItem( ddata : itemStoreMaster  )
  {

    let d = ddata;
    let m;
    for(m = 0 ;m< ddata.workGrpItem.length;m++)
    {
      debugger;
      let found =0;
      let j=0;
      for(j=m+1;j<ddata.workGrpItem.length;j++){
        
        if(ddata.workGrpItem[j].id == ddata.workGrpItem[m].id)
        {
            found = 1;
        }   
      }

      if(found  ==0 )
      {
        this.uniqueItem.push(ddata.workGrpItem[m]);
      }
    }

    ddata.workGrpItem = this.uniqueItem;
    this.prodMasterItem.push(ddata); 

  }



  ActiveDetail() {
    this.isDetail = true;
    this.isHistory = false;
  }

  // show history tab 
  ActiveHistory() {
    this.isDetail = false;
    this.isHistory = true;
  }

   // show save btn
   ActiveSaveBtn() {
    this.isSaveBtn = true;
    this.isEditBtn = false;
  }

  // show update btn
  ActiveUpdateBtn() {
    this.isSaveBtn = false;
    this.isEditBtn = true;
  }

  notifyMsg(title, msg) {
    swal({
      title: title,
      text: msg,
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }



  deleteProductionMasterDetail(id: string) {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Item Wise Production!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this._itemWiseProductionMasterService.delete(parseInt(id))
          .finally(() => {

            this.getAllItemWiseProductionMaster();
            //this.();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Item Wise Production has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Item Wise Production is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

}
 
class dataStore
{
  public itemData : Item;
  public itemWiseProductListLatest : ItemWiseProductionDetailDto[] = [];
}

class itemStoreMaster{

  public workGrpItem : Item[] =  [];
  
}
