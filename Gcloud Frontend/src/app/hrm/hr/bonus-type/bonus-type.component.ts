import { Component, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddBonusTypeComponent } from './add-bonus-type/add-bonus-type.component';
import { EditBonusTypeComponent } from './edit-bonus-type/edit-bonus-type.component';
import { BonusTypeServiceProxy, BonusTypeDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';


declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'bonus-type',
  templateUrl: './bonus-type.component.html',
  providers: [BonusTypeServiceProxy]
})
export class BonusTypeComponent implements OnInit{

  @ViewChild('addBonusTypeModal') public addBonusTypeModal: AddBonusTypeComponent;
  @ViewChild('editBonusTypeModal') editBonusTypeModal: EditBonusTypeComponent;

    public dataTable: DataTable;
    result: BonusTypeDto[] = [];
    data: string[] = [];

    constructor(injector: Injector,
      //  private _authService: AppAuthService,
      private _BonusTypeService: BonusTypeServiceProxy,
      private _router: Router
    ) { }
    globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "BonusType")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate(['']);
      }
      this.getAllRecord();
    }
  
    getAllRecord(): void {
      this._BonusTypeService.getAllBonusTypes()
        .finally(() => {
          console.log('completed');
        })
        .subscribe((result: any) => {
          this.result = result;
          this.paging();
          this.getAllData();
  
        });
    }
  
    paging(): void {
        this.dataTable = {
          headerRow: [ 'Bonus Title', 'Bonus Percentage', 'Detail', 'SortOrder', 'Actions' ],
          footerRow: [ 'Bonus Title', 'Bonus Percentage', 'Detail', 'SortOrder', 'Actions' ],

          dataRows: [
              // ['1234', 'Manager', 'Develop', ''],
              // ['1234', 'Manager', 'Develop', 'btn-round'],
              // ['1234', 'Manager', 'Develop', 'btn-simple']
          ]
  
      };
    }
    getAllData(): void {
      debugger
      let i;
      for (i = 0; i < this.result.length; i++) {
        this.data.push(this.result[i].bonusTitle);
        this.data.push(this.result[i].bonusPercentage.toString());
        this.data.push(this.result[i].bonusDetail);
        this.data.push(this.result[i].sortOrder.toString());
        this.data.push(this.result[i].id.toString());
  
        this.dataTable.dataRows.push(this.data);
        this.data = [];
      }
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      // // Delete a record
      // table.on('click', '.remove', function(e) {
      //   const $tr = $(this).closest('tr');
      //   table.row($tr).remove().draw();
      //   e.preventDefault();
      // });

      // //Like record
      // table.on('click', '.like', function(e) {
      //   alert('You clicked on Like button');
      //   e.preventDefault();
      // });

      $('.card .material-datatables label').addClass('form-group');
    }

    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "BonusType")) {
        this.globalFunction.showNoRightsMessage("Delete");
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Bonus Type!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this._BonusTypeService.delete(parseInt(id))
            .finally(() => {
  
              this.getAllRecord();
            })
            .subscribe(() => {
              swal({
                title: 'Deleted!',
                text: 'Your Bonus Type has been deleted.',
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
              }).catch(swal.noop)
            });
        } else {
          swal({
            title: 'Cancelled',
            text: 'Your Bonus Type is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
          }).catch(swal.noop)
        }
      })
      
    }


    AddBonusType(): void {
      if (!this.globalFunction.hasPermission("Create", "BonusType")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addBonusTypeModal.show();
  }

  EditBonusType(id: string): void{
    if (!this.globalFunction.hasPermission("Edit", "BonusType")) {
      this.globalFunction.showNoRightsMessage("Edit");
      this._router.navigate(['']);
    }
      this.editBonusTypeModal.show(parseInt(id));
  }

}
