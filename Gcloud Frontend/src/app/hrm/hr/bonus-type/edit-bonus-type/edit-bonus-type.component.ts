import { Component, OnInit, Injector, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { BonusTypeDto, BonusTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
    selector: 'edit-bonus-type',
    templateUrl: './edit-bonus-type.component.html',
})
export class EditBonusTypeComponent implements OnInit {

    @ViewChild('editBonusTypeModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    BonusType: BonusTypeDto = new BonusTypeDto();

    _bonusType: FormGroup

    constructor(
        injector: Injector,
        private _BonusTypeService: BonusTypeServiceProxy,
        private formBuilder: FormBuilder
    ) {
        // super(injector);
    }
    show(id: number): void {

        this._BonusTypeService.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: BonusTypeDto) => {
                this.BonusType = result;
            });
        this.active = true;
        this.modal.show();
    }

    ngOnInit(): void {
        this.initValidation();
    }


    initValidation() {

        this._bonusType = this.formBuilder.group({
            bonusTitle: [null, Validators.required]
        });

    }

    //check form validation if it is valid then save it else throw error message in form //
    onType() {

        if (this._bonusType.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._bonusType);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }




    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }

    save(): void {
        this.saving = true;
        this._BonusTypeService.update(this.BonusType)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                //this.notify.info(this.l('Record Updated Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

}
