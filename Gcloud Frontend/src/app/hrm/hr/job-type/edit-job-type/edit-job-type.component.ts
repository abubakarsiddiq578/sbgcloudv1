import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { JobTypeComponent } from '../job-type.component'
import { JobTypeServiceProxy, JobTypeDto, HRConfigurationDto, HRConfigurationServiceProxy } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'edit-job-type',
  templateUrl: './edit-job-type.component.html',
  providers: [JobTypeServiceProxy , HRConfigurationServiceProxy]
})
export class EditJobTypeComponent implements OnInit {

    @ViewChild('editJobTypeModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    jobType: JobTypeDto = new JobTypeDto();
    _jobTypeValidation : FormGroup
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       private _jobTypeServie : JobTypeServiceProxy,
       private  formBuilder : FormBuilder,
       private _hrConfigService : HRConfigurationServiceProxy
    ) {
        // super(injector);
    }
  

    show(id:number): void {
        this._jobTypeServie.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: JobTypeDto)=> {
                this.jobType = result;
            })
        
    }

    initValidation() {

        this._jobTypeValidation = this.formBuilder.group({
            jobType: [null, Validators.required]
            
        });
    
    }


    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];


            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }

        });

    }


    
    //check form validation if it is valid then save it else throw error message in form //
    onType() {
    
      if (this._jobTypeValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._jobTypeValidation);
      }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }


    save(): void {
        this.saving = true;
        this._jobTypeServie.update(this.jobType)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            // this.notify.info(this.l('Record Updated Successfully'));
            this.close();
            this.modalSave.emit(null);
          });
      }


    ngOnInit(): void {
        this.initValidation();
        this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
