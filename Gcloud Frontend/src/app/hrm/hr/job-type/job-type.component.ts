import { Component, Injector, AfterViewInit , ViewChild, OnInit} from '@angular/core';
import { AddJobTypeComponent } from './add-job-type/add-job-type.component';
import { EditJobTypeComponent } from './edit-job-type/edit-job-type.component';
import { JobTypeServiceProxy, JobTypeDto } from '../../../shared/service-proxies/service-proxies';
import { Http } from '@angular/http';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';
// import { AppAuthService } from '@shared/auth/app-auth.service';
// import { AddJobTypeComponent } from '@app/hrm/job-type/add-job-type/add-job-type.component';
// import { EditJobTypeComponent } from '@app/hrm/job-type/edit-job-type/edit-job-type.component';

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: string[][];
  }

  declare const $: any;

@Component({
    templateUrl: './job-type.component.html',
    selector: 'job-type',
    //animations: [appModuleAnimation()],
    providers: [JobTypeServiceProxy]
    
})
export class JobTypeComponent implements OnInit, AfterViewInit {
    @ViewChild('addJobTypeModal') addJobTypeModal: AddJobTypeComponent;
  @ViewChild('editJobTypeModal') editJobTypeModal: EditJobTypeComponent;

    public dataTable: DataTable;

    result: JobTypeDto[]=[];
    data: string[] = [];
    constructor(injector: Injector , 
      /*private _authService: AppAuthService,*/
      private http: Http,
      private _jobTypeService: JobTypeServiceProxy
      ,private _router : Router
      
  ) 
  {
      //super(injector);
  }
  globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "JobType")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
      this.getAllRecord();
    }
    getAllRecord(): void {
    this._jobTypeService.getAllJobTypes()
    .finally(()=>{
        console.log('completed');
    })
    .subscribe((result:any)=>{
this.result = result;
this.paging();
this.getAllData(); 
    });
  }
  paging(): void{
    this.dataTable = {
      headerRow: [ 'Code', 'Job Type Title', 'Comments', 'Sort Order', 'Actions' ],
      footerRow: [ 'Code', 'Job Type Title', 'Comments', 'Sort Order', 'Actions' ],

      dataRows: [
          // ['1234', 'Manager', 'Develop', '1', ''],
          // ['1234', 'Manager', 'Develop', '1', 'btn-round'],
          // ['1234', 'Manager', 'Develop', '1', 'btn-simple'],
      ]
   };
  }
  getAllData():void{
    let i;
    for(i=0; i<this.result.length ; i++)
    {
      this.data.push(this.result[i].code);
      this.data.push(this.result[i].jobTypeTitle);
      this.data.push(this.result[i].comments);
      this.data.push(this.result[i].sortOrder.toString());
      // this.data.push(this.result[i].isActive.toString());
      this.data.push(this.result[i].id.toString());
      
      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
}
    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }



    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "JobType")) {
        this.globalFunction.showNoRightsMessage("Delete");
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Job Type!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
      if (result.value) {
        this._jobTypeService.delete(parseInt(id))
          .finally(() => {
   
          this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Job Type has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
          }).catch(swal.noop)
           });
      } else {
        swal({
            title: 'Cancelled',
            text: 'Your Job Type is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        }).catch(swal.noop)
      }
    })
    }

    AddJobType(): void {
      if (!this.globalFunction.hasPermission("Create", "JobType")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addJobTypeModal.show();
  }

  EditJobType(id:string): void{
    if (!this.globalFunction.hasPermission("Edit", "JobType")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
      this.editJobTypeModal.show(parseInt(id));
  }




}