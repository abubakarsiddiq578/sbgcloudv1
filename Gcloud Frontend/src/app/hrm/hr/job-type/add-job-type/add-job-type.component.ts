import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
// import { JobTypeComponent } from '../job-type.component'
import { JobTypeDto, JobTypeServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'add-job-type',
  templateUrl: './add-job-type.component.html',
  providers:[JobTypeServiceProxy , HRConfigurationServiceProxy]
})
export class AddJobTypeComponent implements OnInit {

    @ViewChild('addJobTypeModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    jobType: JobTypeDto = new JobTypeDto();
    _jobType : JobTypeDto[];
    _docNo : abc[] = [];
    _jobTypeValidation : FormGroup
    
  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       private _jobTypeService : JobTypeServiceProxy,
       private _hrConfigService : HRConfigurationServiceProxy,
       private formBuilder : FormBuilder
    ) {
        // super(injector);
    }
  
    save(): void {
        //TODO: Refactor this, don't use jQuery style code
      this.saving = true;
        this._jobTypeService.create(this.jobType)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                // this.notify.info(this.l('Saved Successfully'));
                this.close();   
                this.modalSave.emit(null);
                
            });
      }
    
      initValidation() {

        this._jobTypeValidation = this.formBuilder.group({
            jobType: [null, Validators.required]
            
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    onType() {
    
      if (this._jobTypeValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._jobTypeValidation);
      }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }


    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];
        });

    }


    show(): void{
        this.jobType.isActive = true
        this.active = true;
        this.modal.show();
        this.jobType = new JobTypeDto();
        this.jobType.init({isActive:true});

        this._jobTypeService.getAllJobTypes().subscribe((result)=>{
            this._jobType = result;
            //this.getAutoCode();
            if(this.hrConfig.autoCode == true){

                this.getAutoCode();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }
        })
        this._jobTypeValidation.markAsUntouched({onlySelf:true});
    }


     //this function generate auto doc number
     getAutoCode():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._jobType.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._jobType[i].code ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0  ){
          
            debugger;
            code = "1";
            this.jobType.code = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    let x;
                    code = this._docNo[this._docNo.length-1].code ;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.jobType.code = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.jobType.code =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.jobType.code =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.jobType.code = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.jobType.code =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.jobType.code =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.jobType.code = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.jobType.code =  "0" + code;
                      }
                      else if(code){
      
                          this.jobType.code =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.jobType.code = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.jobType.code =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.jobType.code =  temp[0] ;
                      }
    
                    }
                    
            }                     
        }
        this._docNo = [];
    }
    

    ngOnInit(): void {
        this.initValidation();
        this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
// class for keeping all docNo number
class abc{
    public code: string;
  }

