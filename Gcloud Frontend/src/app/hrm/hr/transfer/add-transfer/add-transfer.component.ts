import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { TransferComponent } from '../transfer.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {TransferDto , TransferServiceProxy , EmployeeDto , EmployeeServiceProxy , DepartmentDto , DeductionServiceProxy, DepartmentServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy } from  'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import * as moment from 'moment';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'add-transfer',
  templateUrl: './add-transfer.component.html',
  styleUrls : ['./add-transfer.component.less'],
  providers:[TransferServiceProxy , EmployeeServiceProxy, DepartmentServiceProxy , HRConfigurationServiceProxy]
})
export class AddTransferComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addTransferModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    employee : EmployeeDto[];

    department : DepartmentDto[];

    Transfer : TransferDto = new TransferDto();
    documentDate:Date = new Date();

    transferDate:Date = new Date();


    _transfer : TransferDto[]; //for getting disable docNo from transfer table
    _docNo : abc[] =[];

    _trans : FormGroup;
     
    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;

    public empName:string ;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;
    
    /////////////////////////////////////////////////////
    /*mailFormControl = new FormControl('', [
      Validators.required,
      Validators.email,
    ]);

    ////////////////////////
    documentNoFormControl = new FormControl('', [Validators.required]);
    


      transferForm: FormGroup = new FormGroup({
      documentNoFormControl : this.documentNoFormControl
      
    });*/

    /*
    getDocumentNoErrorMessage() {
      debugger;
      return this.documentNoFormControl.hasError('required') ? 'You must enter a value':'';
        
    }*/
  
//////////////////////////////////////////////////////
    
    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
        private TransferService : TransferServiceProxy ,
        private employeeService : EmployeeServiceProxy,
        private DepartmentService : DepartmentServiceProxy,
        private formBuilder : FormBuilder,
        private _hrConfigService  : HRConfigurationServiceProxy

    ) {
        //super(injector);
    }
  
        ///////////////////filter /////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }

  
 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Transfer.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  
/////////////////////////////////////////////////////////////////////
/*
public departmentDropdownSearch(){
    debugger;
   this.filteredOptionsDept = this.myControl.valueChanges
    .pipe(
      startWith<string | DepartmentDto >(''),
      map(value => typeof value === 'string' ? value : value.title),
      map(name => name ? this._filterDept(name) : this.employee.slice())
    ); 
   
  }

  private _filterDept(value: string): DepartmentDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.department.filter(option => option.title.toString().toLowerCase().includes(filterValue)); 
  }

  
 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Transfer.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  */
/////////////////////////////////////////////////////////////////////

getAllHRConfiguration() {

  this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

      this.hrConfig = result[0];

  });

}



  show(): void{

        this.active = true;
        this.modal.show();
        this.Transfer = new TransferDto();
        this.TransferService.getAllTransfers().subscribe((result )=>{

          this._transfer = result;
          //his.getAutoDocNumber();

          if(this.hrConfig.autoCode == true){

            this.getAutoDocNumber();
            this.isAutoCode = true ;
          }
          else if(this.hrConfig.autoCode == false){
            this.isAutoCode = false;
          }
       
      })

      this.myControl = new FormControl();

      this.filteredOptions = new Observable<EmployeeDto[]>();

      this.employeeDropdownSearch();
      this._trans.markAsUntouched({onlySelf:true});
        
    }


    initValidation() {


      this._trans = this.formBuilder.group({
          //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
          employeeId: [null, Validators.required],
          //transferType: [null, Validators.required],
          transferDate: [null, Validators.required],
          transferTo: [null, Validators.required],
          transferFrom: [null, Validators.required]
      });

  }

/*
  onType() {
    debugger;
    if (this._trans.valid) {
        this.save();
    } else {
        this.validateAllFormFields(this._trans);

    }
}*/

onType() {
 
    if (this._trans.valid) {
    
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.Transfer.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
      
        this.validateAllFormFields(this._trans);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._trans);
       
    }
  }




validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
        }
    });
}


    ngOnInit(): void {


      this.getAllEmployee();
      this.getAllDepartment();
      this.initValidation();
      this.getAllHRConfiguration();
     
    }


  /*  getAutoDocNumber():void{

      debugger;
      let i =0;
     
      for(i=0 ;i< this._transfer.length ; i++){

          let abc_ ; 

          abc_ = new abc();

          abc_.code = this._transfer[i].documentNo ; 

          this._docNo.push(abc_);

      }


      let code : any ;

      if(this._docNo.length == 0 ){
          debugger;
          code = "1";
          this.Transfer.documentNo = "00" + code ;

      }

      else{

          if(this._docNo[this._docNo.length - 1] != null ){

                  code = this._docNo[this._docNo.length-1].code ;
                  code ++;
                  if(code <=9){

                      this.Transfer.documentNo = "00" + code ;
                  }
                  else if(code <=99){

                      this.Transfer.documentNo = "0" + code ;
                  }
                  else if(code <=999){

                      this.Transfer.documentNo =  code ;
                  }
          }                     
      }
      this._docNo = [];
    }*/

    getAutoDocNumber():void{

      debugger;
      let i =0;
      
      let temp : any[];
     
      for(i=0 ;i< this._transfer.length ; i++){
  
          let abc_ ; 
  
          abc_ = new abc();
  
          abc_.code = this._transfer[i].documentNo ; 
  
          this._docNo.push(abc_);
  
      }
  
  
      let code : any ;
  
      if(this._docNo.length == 0  ){
        
          debugger;
          code = "1";
          this.Transfer.documentNo = "00" + code ;
  
      }
  
      else{
  
          if(this._docNo[this._docNo.length - 1] != null ){
  
                  let x;
                  code = this._docNo[this._docNo.length-1].code ;
                  temp = code;
                  if(code!=null){
                    temp = code.split("-");
                     x = parseInt(temp[0]);
                     x = x.toString();
                  }
                  debugger;
                  //let j = parseInt(code);
                  //
                  if(temp.length == 1 && x=="NaN"  ){
  
                    debugger;
                    temp[1] = 0;
                    temp[1] ++;
  
                   
                    if(temp[1] <=9){
    
                        this.Transfer.documentNo = temp[0] + "-00" + temp[1] ;
                    }
                    else if(temp[1] <=99){
    
                        this.Transfer.documentNo =  temp[0] + "-0" + temp[1] ;
                    }
                    else if(temp[1]){
    
                        this.Transfer.documentNo =  temp[0] + "-" +temp[1] ;
                    }
  
                  
                  }
  
                  else if (temp.length == 2) {
  
                    temp[1] ++;
                    if(temp[1] <=9){
    
                        this.Transfer.documentNo = temp[0] + "-00" + temp[1] ;
                    }
                    else if(temp[1] <=99){
    
                        this.Transfer.documentNo =  temp[0] + "-0" + temp[1] ;
                    }
                    else if(temp[1]){
    
                        this.Transfer.documentNo =  temp[0] + "-" +temp[1] ;
                    }
  
                  }
  
                  else if(temp.length == 0){
  
                    code ++;
                    if(code <=9){
    
                        this.Transfer.documentNo = "00" + code ;
                    }
                    else if(code<=99){
    
                        this.Transfer.documentNo =  "0" + code;
                    }
                    else if(code){
    
                        this.Transfer.documentNo =  code ;
                    }
  
  
                  } 
                  else if(temp.length==1 && x!="NaN" ){
  
                    temp[0]++;
                    if(temp[0] <=9){
    
                        this.Transfer.documentNo = "00" + temp[0] ;
                    }
                    else if(code<=99){
    
                        this.Transfer.documentNo =  "0" + temp[0];
                    }
                    else if(code){
    
                        this.Transfer.documentNo =  temp[0] ;
                    }
  
                  }
          
          }                     
      }
      this._docNo = [];
  }






    // to get all the employees and show it in the dropdown
    getAllEmployee(){
      
      this.employeeService.getAllEmployees()
      .subscribe((result) => {
        this.employee = result;
        console.log("this is employee: ", this.employee);

      })

    }





    // to get all the department and show it in dropdown 
    getAllDepartment(){

     
       this.DepartmentService.getAllDepartments()
       .subscribe((result) => {
         this.department = result;
         
       })

    }



    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }


    save(): void {


      //for saving tranfer date and document to transfer
        this.Transfer.documentDate = moment(this.documentDate);
        this.Transfer.transferDate = moment(this.transferDate);
        this.saving = true;
        debugger;
        this.TransferService.create(this.Transfer)
          .finally(() => { this.saving = false; })
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
      }
    
      notify() {
        swal({
          title: "Success!",
          text: "Saved Successfully.",
          timer: 2000,
          showConfirmButton: false
        }).catch(swal.noop)
      }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

class abc{
  public code: string;
}


