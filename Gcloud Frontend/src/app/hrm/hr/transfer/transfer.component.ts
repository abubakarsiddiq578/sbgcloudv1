// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddTransferComponent } from './add-transfer/add-transfer.component';
import { EditTransferComponent } from './edit-transfer/edit-transfer.component';
import {TransferDto , TransferServiceProxy } from  'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'transfer',
    templateUrl: 'transfer.component.html',
    providers: [TransferServiceProxy]
})

export class TransferComponent implements OnInit, AfterViewInit {
    public dataTable: DataTable;

    data:string[] = [];

    Transfer:TransferDto[];

    constructor(private TransferService:TransferServiceProxy ,private _router : Router){



    }

    @ViewChild('addTransferModal') addTransferModal: AddTransferComponent;
    @ViewChild('editTransferModal') editTransferModal: EditTransferComponent;


    globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if(!this.globalFunction.hasPermission("View","Transfer")){
        this.globalFunction.showNoRightsMessage("View")
        this._router.navigate(['']);
      }
      this.getAllTransfer();

    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }


    
    fillDataTable() {
    
      let i;
      for (i = 0; i < this.Transfer.length; i++) {
  
        this.data.push(this.Transfer[i].documentNo)
        
        this.data.push(this.Transfer[i].documentDate.toString())

        this.data.push(this.Transfer[i].transferDate.toString())

        this.data.push(this.Transfer[i].employee.employee_Name)

        this.data.push(this.Transfer[i].transferType)

        this.data.push(this.Transfer[i].transFrom.title)

        this.data.push(this.Transfer[i].transTo.title)

        this.data.push(this.Transfer[i].detail)

        this.data.push(this.Transfer[i].id.toString())

        this.dataTable.dataRows.push(this.data)
  
        this.data = [];
  
      }
    }


    initDatatable(){

      this.dataTable = {
        headerRow: [ 'Doc No', 'Doc Date', 'Transfer Date', 'Employee', 'Transfer Type', 'Transfer From', 'Transfer To','Transfer Detail' ,'Actions' ],
        footerRow: [ 'Doc No', 'Doc Date', 'Transfer Date', 'Employee', 'Transfer Type', 'Transfer From', 'Transfer To','Transfer Detail' ,'Actions' ],

        dataRows: []
    
        
     };

    }


    AddTransfer(): void {
      if(!this.globalFunction.hasPermission("Create","Transfer")){
        this.globalFunction.showNoRightsMessage("Create")
        return
      }
      this.addTransferModal.show();
      }

      EditTransfer(id:string): void {
        if(!this.globalFunction.hasPermission("Edit","Transfer")){
          this.globalFunction.showNoRightsMessage("Edit")
          return
        }
        this.editTransferModal.show(parseInt(id));
        }


        getAllTransfer():void{

            this.TransferService.getAllTransfers().subscribe((result)=>{

                this.initDatatable()
                this.Transfer = result;
                this.fillDataTable()

            })

        }



        protected delete(id: string): void {
          if(!this.globalFunction.hasPermission("Delete","Transfer")){
            this.globalFunction.showNoRightsMessage("Delete")
            return
          }
          swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this Request!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it',
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: false
          }).then((result) => {
            if (result.value) {
              this.TransferService.delete(parseInt(id))
                .finally(() => {
                  this.getAllTransfer();
                })
                .subscribe(() => {
                  debugger
                  swal({
                    title: 'Deleted!',
                    text: 'Transfer has been deleted.',
                    type: 'success',
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                  }).catch(swal.noop)
                });
            } else {
              swal({
                title: 'Cancelled',
                text: 'Transfer Deletion is safe :)',
                type: 'error',
                confirmButtonClass: "btn btn-info",
                buttonsStyling: false
              }).catch(swal.noop)
            }
          })
      }

}


