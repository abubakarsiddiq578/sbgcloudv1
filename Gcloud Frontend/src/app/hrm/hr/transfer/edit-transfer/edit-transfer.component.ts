import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';

import { TransferComponent } from '../transfer.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { TransferDto, TransferServiceProxy, EmployeeDto, EmployeeServiceProxy, DepartmentDto, DeductionServiceProxy, DepartmentServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import * as moment from 'moment';
///////////////////////////imports for validations////////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
///////////////////////////////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-transfer',
  templateUrl: './edit-transfer.component.html',
  styleUrls: ['./edit-transfer.component.less'],
  providers: [TransferServiceProxy, EmployeeServiceProxy, DepartmentServiceProxy , HRConfigurationServiceProxy]
})
export class EditTransferComponent /*extends AppComponentBase*/ implements OnInit {

  @ViewChild('editTransferModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;


  employee: EmployeeDto[];

  department: DepartmentDto[];

  Transfer: TransferDto = new TransferDto();

  transferDate: Date = new Date();
  documentDate: Date = new Date();

  _trans: FormGroup;

  myControl = new FormControl();

  filteredOptions: Observable<EmployeeDto[]>;

  public empName : string ;

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(
    injector: Injector,
    // private _userService: UserServiceProxy
    private TransferService: TransferServiceProxy,
    private employeeService: EmployeeServiceProxy,
    private DepartmentService: DepartmentServiceProxy,
    private formBuilder: FormBuilder ,
    private _hrConfigService : HRConfigurationServiceProxy

  ) {
    //super(injector);
  }


  show(id: number): void {

    this.TransferService.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();

      })
      .subscribe((result: TransferDto) => {

        this.Transfer = result;

        this.documentDate = this.Transfer.documentDate.toDate();
        this.transferDate = this.Transfer.transferDate.toDate();

        this.employeeService.get(result.employeeId).subscribe(()=>{

        });

      });

      this.empName = null;
      this.employeeDropdownSearch();
      this._trans.markAsUntouched({onlySelf:true});
  }




  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];


        if (this.hrConfig.autoCode == true) {

            this.isAutoCode = true;
        }
        else if (this.hrConfig.autoCode == false) {
            this.isAutoCode = false;
        }

    });

}


//////////////////////Validation///////////////////////////////////
  initValidation() {

    this._trans = this.formBuilder.group({
      //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      employeeId: [null, Validators.required],
      //transferType: [null, Validators.required],
      transferDate: [null, Validators.required],
      transferTo: [null, Validators.required],
      transferFrom: [null, Validators.required]
    });

  }

  /*onType() {
    debugger;
    if (this._trans.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._trans);

    }
  }*/

  onType() {
    debugger;
    if (this._trans.valid) {
      debugger;
      let a,p=0 ;
      for(a=0;a<this.employee.length;a++){
        if(this.Transfer.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
        this._trans.value.countryName = null; // setting value to null 
        this.validateAllFormFields(this._trans);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._trans);
       
    }
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ///////////////////////////////End of Validation//////////////////////////////////
public employeeDropdownSearch(){
  debugger;
 this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith<string | EmployeeDto >(''),
    map(value => typeof value === 'string' ? value : value.employee_Name),
    map(name => name ? this._filter(name) : this.employee.slice())
  ); 
 
}

private _filter(value: string): EmployeeDto[]{

  debugger;
  const filterValue = value.toLowerCase();

  return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
}


displayEmployee = (emp: EmployeeDto):any => {
  debugger;
  
  if(emp instanceof EmployeeDto)
  {
      this.Transfer.employeeId = emp.id ;
      return emp.employee_Name ;
  }
  this.Transfer.employeeId = null;
  return emp;

}

/////////////////////////////////////////////////////////////////////
  ngOnInit(): void {

    this.employeeService.getAllEmployees()
      .subscribe((result) => {
        this.employee = result;
        // console.log("this is employee: ", this.employee);

      })

    // to get all the departments
    this.DepartmentService.getAllDepartments()
      .subscribe((result) => {
        this.department = result;

      })
    this.initValidation();
    this.getAllHRConfiguration();
  }


  onShown(): void {
    // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

  save(): void {

    this.Transfer.documentDate = moment(this.documentDate).add(5,'hour');

    this.Transfer.transferDate = moment(this.transferDate).add(5,'hour');

    this.saving = true;

    this.TransferService.update(this.Transfer)
      .finally(() => { this.saving = false; })
      .subscribe(() => {

        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }


  close(): void {
    this.active = false;
    this.modal.hide();
  }
}

