import { Component, OnInit, Injector, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AdditionalLeaveServiceProxy, AdditionalLeaveDto, EmployeeServiceProxy, LeaveTypeDto, LeaveTypeServiceProxy, EmployeeDto } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-additional-leave',
  templateUrl: './edit-additional-leave.component.html',
  providers: [AdditionalLeaveServiceProxy, EmployeeServiceProxy, LeaveTypeServiceProxy]
})
export class EditAdditionalLeaveComponent implements OnInit {

    @ViewChild('editAdditionalLeaveModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    additionalLeave: AdditionalLeaveDto = new AdditionalLeaveDto();
    employeeDDList : EmployeeDto[]=[];
    leaveTypeDDList: LeaveTypeDto[]=[];

    _additionalLeave : FormGroup ;

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;
  
    public empName : string ;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       private _additionalLeaveService: AdditionalLeaveServiceProxy,
       private _employeeDDService : EmployeeServiceProxy,
       private _leaveTypeService: LeaveTypeServiceProxy,
       private formBuilder : FormBuilder
    ) {
        // super(injector);
    }

    //to initialize fields on which validations are being applied
    initValidation() {

        this._additionalLeave = this.formBuilder.group({
            employeeId: [null, Validators.required],
            leaveTypeId : [null , Validators.required]
        });
    
    }

    /*onType() {
    debugger;
    if (this._trans.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._trans);

    }
  }*/

  onType() {
    debugger;
    if (this._additionalLeave.valid) {
      debugger;
      let a,p=0 ;
      for(a=0;a<this.employeeDDList.length;a++){
        if(this.additionalLeave.employeeId == this.employeeDDList[a].id) ///check if employee selected matched in list or not
        {
          p=1;
          this.save();
          break;
        }
      }
      if(p==0)
      {
        
        this.empName = null; // null empName 
    
        this.validateAllFormFields(this._additionalLeave);
        this.employeeDropdownSearch();
      }
      
    } else {
       this.validateAllFormFields(this._additionalLeave);
       
    }
  }


     
    validateAllFormFields(formGroup: FormGroup) {
    
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
      }
  
  
   ////////////////////////////////////End of Validation /////////////////////////////////

    ////////////////////////////Filter Method/////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employeeDDList.slice())
    ); 
   
  }
  
  private _filter(value: string): EmployeeDto[]{
  
    debugger;
    const filterValue = value.toLowerCase();
  
    return this.employeeDDList.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.additionalLeave.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.additionalLeave.employeeId = null;
    return emp;
  
  }
  
  ////////////////////////////////end of filter Method/////////////////////////////////////
  
    show(id:number): void {
        this._additionalLeaveService.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: AdditionalLeaveDto)=> {
                this.additionalLeave = result;

                this._employeeDDService.get(result.employeeId).subscribe((result)=>{

                    this.empName = result.employee_Name ;
                });
            });

        this.employeeDropdownSearch();
        this._additionalLeave.markAsUntouched({onlySelf:true});
    }

    save(): void {
        debugger
        this.saving = true;
        this._additionalLeaveService.update(this.additionalLeave)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            // this.notify.info(this.l('Record Updated Successfully'));
            this.close();
            this.modalSave.emit(null);
          });
      }

    ngOnInit(): void {

        this._employeeDDService.getAllEmployees()
            .subscribe((result)=>{
                this.employeeDDList = result;
            })
        this._leaveTypeService.getAllLeaveTypes()
            .subscribe((result)=> {

                this.leaveTypeDDList = result;

            })

            this.initValidation();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
