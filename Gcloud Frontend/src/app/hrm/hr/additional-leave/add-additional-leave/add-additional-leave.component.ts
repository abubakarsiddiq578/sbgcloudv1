import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AdditionalLeaveDto, LeaveTypeDto, EmployeeDto, EmployeeServiceProxy, LeaveTypeServiceProxy, AdditionalLeaveServiceProxy } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'add-additional-leave',
  templateUrl: './add-additional-leave.component.html',
  providers: [AdditionalLeaveServiceProxy, EmployeeServiceProxy, LeaveTypeServiceProxy]
})
export class AddAdditionalLeaveComponent implements OnInit {

    @ViewChild('addAdditionalLeaveModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    additionalLeave: AdditionalLeaveDto = new AdditionalLeaveDto(); 
    public docDate = new Date();
    leaveTypeDDList: LeaveTypeDto[] = [];
    employeeDDList: EmployeeDto[] = [];
    additionalLeaveDDList= [];

    _additionalLeave : FormGroup

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;
  
    public empName : string ;

    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
        private _additionalLeaveService: AdditionalLeaveServiceProxy,
        private _employeeService: EmployeeServiceProxy,
        private _leaveTypeService: LeaveTypeServiceProxy,
        private formBuilder : FormBuilder
       
    ) {
        // super(injector);
    }
  

    //to initialize fields on which validations are being applied
    initValidation() {

        this._additionalLeave = this.formBuilder.group({
            employeeId: [null, Validators.required],
            leaveTypeId : [null , Validators.required]
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
    /*onType() {
    
      if (this._additionalLeave.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._additionalLeave);
      }
    }*/

    onType() {
        debugger;
        if (this._additionalLeave.valid) {
          debugger;
          let a,p=0 ;
          for(a=0;a<this.employeeDDList.length;a++){
            if(this.additionalLeave.employeeId == this.employeeDDList[a].id) ///
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
            
            this.validateAllFormFields(this._additionalLeave);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._additionalLeave);
           
        }
      }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }


 /////////////////////////////////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employeeDDList.slice())
    ); 
   
  }
  
  private _filter(value: string): EmployeeDto[]{
  
    debugger;
    const filterValue = value.toLowerCase();
  
    return this.employeeDDList.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.additionalLeave.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.additionalLeave.employeeId = null;
    return emp;
  
  }
  
  /////////////////////////////////////////////////////////////////////

    show(): void{

        this.active = true;
        this.modal.show();
        this.additionalLeave = new AdditionalLeaveDto();
        this.additionalLeave.init({ isActive: true });
        
        ///////////////////////////

        this.empName = null;
        this.employeeDropdownSearch();
        this._additionalLeave.markAsUntouched({onlySelf:true})
        
    }
    save(): void {
        //TODO: Refactor this, don't use jQuery style code

        this.saving = true;
        this._additionalLeaveService.create(this.additionalLeave)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                // this.notify.info(this.l('Saved Successfully'));
                this.close();
                this.modalSave.emit(null);

            });
    }


    ngOnInit(): void {
        this._employeeService.getAllEmployees()
            .finally(() => { console.log('completed') })
            .subscribe((result: any) => {
                this.employeeDDList = result;

            })

        this._leaveTypeService.getAllLeaveTypes()
            .finally(() => { console.log('completed') })
            .subscribe((result: any) => {
                this.leaveTypeDDList = result;
                
            })

            this.initValidation();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
