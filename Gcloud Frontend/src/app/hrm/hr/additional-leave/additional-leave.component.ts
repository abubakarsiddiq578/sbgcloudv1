import { Component, OnInit, ViewChild, AfterViewInit, Injector } from '@angular/core';
import { AddAdditionalLeaveComponent } from './add-additional-leave/add-additional-leave.component';
import { EditAdditionalLeaveComponent } from './edit-additional-leave/edit-additional-leave.component';
import { AdditionalLeaveServiceProxy, AdditionalLeaveDto } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'additional-leave',
  templateUrl: './additional-leave.component.html',
  providers: [AdditionalLeaveServiceProxy]
})
export class AdditionalLeaveComponent implements OnInit, AfterViewInit {
  @ViewChild('addAdditionalLeaveModal') addAdditionalLeaveModal: AddAdditionalLeaveComponent;
  @ViewChild('editAdditionalLeaveModal') editAdditionalLeaveModal: EditAdditionalLeaveComponent;

  public dataTable: DataTable;
  result: AdditionalLeaveDto[] = [];
  data: string[] = [];
  constructor(injector: Injector, private _additionalLeaveService: AdditionalLeaveServiceProxy, private _router: Router) { }

  globalFunction: GlobalFunctions = new GlobalFunctions
  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "AdditionalLeave")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);
    }
    this.getAllRecord();
  }
  getAllRecord(): void {
    this._additionalLeaveService.getAllAdditionalLeaves()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        
        this.result = result;
        this.paging();
        this.getAllData();
      });
  }
  paging(): void {
    this.dataTable = {
      headerRow: ['Employee', 'Leave Type', 'Additional Leave', 'Detail', 'Actions'],
      footerRow: [ /*'Leave Type', 'Leave Allowed', 'Leave Reset', 'Detail', 'Actions'*/],

      dataRows: [
        // ['1234', 'Manager', 'Develop', '1', 'abc', ''],
        // ['1234', 'Manager', 'Develop', '1','abc', 'btn-round'],
        // ['1234', 'Manager', 'Develop', '1', 'abc','btn-simple'],

      ]

    };
  }
  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].employee.employee_Name);
      this.data.push(this.result[i].leaveType.leaveTypeName);
      this.data.push(this.result[i].additionalLeaves.toString());
      this.data.push(this.result[i].detail);
      this.data.push(this.result[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      // table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "AdditionalLeave")) {
      this.globalFunction.showNoRightsMessage("Delete")
      return
    }

    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Additional leave Record!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        debugger;
        this._additionalLeaveService.delete(parseInt(id))
          .finally(() => {

            this.getAllRecord();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your additional Leave record has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        debugger;
        swal({
          title: 'Cancelled',
          text: 'Your addotional leave record is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

  AddAdditionalLeave(): void {
    if (!this.globalFunction.hasPermission("Create", "AdditionalLeave")) {
      this.globalFunction.showNoRightsMessage("Create")
      return
    }
    this.addAdditionalLeaveModal.show();
  }

  EditAdditionalLeave(id:string): void {
    if (!this.globalFunction.hasPermission("Edit", "AdditionalLeave")) {
      this.globalFunction.showNoRightsMessage("Edit")
      return
    }
    this.editAdditionalLeaveModal.show(parseInt(id));
  }


}
