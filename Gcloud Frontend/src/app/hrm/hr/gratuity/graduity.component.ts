import { Component, OnInit, AfterViewInit, Injector, Input, EventEmitter, Output } from '@angular/core';
import { CostCenterServiceProxy, DepartmentServiceProxy, EmployeeServiceProxy, GratuityConfigurationServiceProxy, SalaryExpenseTypeServiceProxy, GraduityMasterServiceProxy, GraduityDetailServiceProxy, GratuityConfigurationDto, EmployeeDto, GraduityDetailDto, GraduityMasterDto, DepartmentDto, CostCenterDto, SalaryMasterServiceProxy, SalaryMasterDto } from '@app/shared/service-proxies/service-proxies';
import { FormBuilder } from '@angular/forms';
import swal from 'sweetalert2';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}


@Component({
  selector: 'gratuity',
  templateUrl: 'gratuity.component.html',
  providers: [CostCenterServiceProxy,
    DepartmentServiceProxy,
    EmployeeServiceProxy,
    GratuityConfigurationServiceProxy,
    SalaryExpenseTypeServiceProxy,
    GraduityMasterServiceProxy,
    GraduityDetailServiceProxy,
    SalaryMasterServiceProxy
  ]
})

export class GratuityComponent implements OnInit, AfterViewInit {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  public dataTable: DataTable;
  resultGratuityConfig: GratuityConfigurationDto[] = [];
  resultGratuityConfg: GratuityConfigurationDto[] = [];
  resultGratuityDetail: GraduityDetailDto[] = [];
  resultGratuityMaster: GraduityMasterDto[] = [];
  resultEmployee: EmployeeDto[] = [];
  resultSalaryMaster: SalaryMasterDto[] = [];
  empIdArray: any[] = [];
  CostCenterHeadIdArray: any[] = [];
  deptIdArray: any[] = [];
  CostCenterIdArray: any[] = [];
  NoOfYears: number;
  gratuityFactor: number;

  active: boolean = false;
    saving: boolean = false;

  gratuityDetail: any[] = [];
  data: string[] = [];
  grossSalary: number;




  resultDept: DepartmentDto[] = [];
  resultCost: CostCenterDto[] = [];
  resultEmp: EmployeeDto[] = [];


  tempArr: any[] = [];
  tempArrCost: any[] = [];
  tempArray: any[] = [];
  tempArrayEmp: any[] = [];

  EmployeesArrayId: string;
  CostCenterArrayId: string;

  DepartmentArrayId: string;

  CostCenterName: CostCenterDto[] = []
  DepartmentName: DepartmentDto[] = []
  empSelected: EmployeeDto[];

  TotalGratuitySalary: number;
  gratuityMasterData: GraduityMasterDto = new  GraduityMasterDto();
  gratuityConfigData: GratuityConfigurationDto = new  GratuityConfigurationDto();
  // resultCost: CostCenterDto[] = [];
  // resultEmp: EmployeeDto[] = [];
  // resultSalaryType: SalaryExpenseTypeDto[] = [];


  constructor(injector: Injector,
    private _costCenterService: CostCenterServiceProxy,
    private _departmentService: DepartmentServiceProxy,
    private _employeeService: EmployeeServiceProxy,
    private _gratuityConfigService: GratuityConfigurationServiceProxy,
    private _salaryType: SalaryExpenseTypeServiceProxy,
    private _GratuityMasterService: GraduityMasterServiceProxy,
    private _GratuityDetailService: GraduityDetailServiceProxy,
    private _salaryMasterService: SalaryMasterServiceProxy,
    private formBuilder: FormBuilder
  ) { }










  ngOnInit() {
    


      
    
    //this.getAllGratuityConfiguration();
   // this.getAllEmployee();
  //  this.getAllGratuityMaster();
   // this.getAllSalary();
     this.getAllCostCenterRecord();
   
     this.getAllDepartmentRecord();
     this.dataTable = {
      headerRow: ['Employee Code', 'Name', 'Department', 'Designation', 'Joining Date', 'Leaving Date', 'Gratiuity Amount'],
      footerRow: [],

      dataRows: []
    }
   

   

  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  //GratuityConfig to split Id's

  getAllGratuityConfiguration(): void {
    //debugger;
    this._gratuityConfigService.getAllGratuityConfiguration()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {

        this.resultGratuityConfig = result;

        //calling split id string into array
        this.SplitId();


      });



  }
  //get Master data
  getAllGratuityMaster(): void {
    // debugger;
    this._GratuityMasterService.getAllGraduity()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {

        this.resultGratuityMaster = result;

        //loop to iterate each gratuity detail object in a master and store in a array
        for (let i = 0; i < this.resultGratuityMaster.length; i++) {

          this.gratuityDetail = this.resultGratuityMaster[i].graduityDetails;
          console.log('Abc ', this.gratuityDetail);
        }



      });


  }


  getAllEmployee(): void {
    // debugger;
    this._employeeService.getAllEmployees()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {

        this.resultEmployee = result;
        this.empSelected = result;
        // this.DepartmentName = result;
        console.log('Emp All Record ', this.resultEmployee);

      });

      this.getAllSalary();

  }

  //To get a anualsalary of configured employees
  getAllSalary(): void {
    // debugger;
    this._salaryMasterService.getAllSalaries()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {

        this.resultSalaryMaster = result;



      });


      this.getAllGratuityConfiguration();
  }
  //calculate graduity of employee on salary and gratuity factors
  GraduityCalculate(): void {
    debugger;
    let j = 0;
    for (let i = 0; i < this.empIdArray.length; i++) {

      if (this.resultSalaryMaster[j].employeeId == this.empIdArray[i]) {
        this.grossSalary = this.resultSalaryMaster[j].annualGrossSalary
      }
    }
    //formula to calculate gratuity on salary
    this.TotalGratuitySalary = this.grossSalary * this.gratuityFactor * this.NoOfYears / 26 //here 26 is total working days;
    //rounding off value without decimal
    this.TotalGratuitySalary = Math.round(this.TotalGratuitySalary);
  
    this.getAllEmpData();
  }



  //Split GratuityConfig MultiSelect Id's

  SplitId(): void {

    // debugger;
    for (let i = 0; i < this.resultGratuityConfig.length; i++) {
       //.split(","); //passing direct value because only one value is in id and store in a array
      this.empIdArray[i] = this.resultGratuityConfig[i].employeeId;
      this.deptIdArray = this.resultGratuityConfig[i].departmentId.split(",");
      this.CostCenterIdArray = this.resultGratuityConfig[i].costCenterId.split(",");
      this.NoOfYears = this.resultGratuityConfig[i].gratuityYearsOfService;
      this.gratuityFactor = this.resultGratuityConfig[i].gratuityFactor;

    }

    this.GraduityCalculate();
    
    console.log('Cost center ', this.CostCenterIdArray);
    console.log('Department ', this.deptIdArray);
    console.log('Emp ', this.empIdArray);



  }

  //Employee data eqaul to detail 
  getAllEmpData(): void {
    this.getAllGratuityMaster();
    let j = 0;
    // debugger;
    for (let i = 0; i < this.empIdArray.length; i++) {


      if (this.resultEmployee[j].id == this.empIdArray[i]) {
        debugger;
        //copy&assign value into gratuity detail
        this.gratuityDetail[j].employeeCode = this.resultEmployee[j].employee_Code;
        this.gratuityDetail[j].employeeName = this.resultEmployee[j].employee_Name;
        this.gratuityDetail[j].department = this.resultEmployee[j].department.title;
        this.gratuityDetail[j].designation = this.resultEmployee[j].employeeDesignation.employeeDesignationName;
        this.gratuityDetail[j].joiningDate = this.resultEmployee[j].joiningDate.toDate();
        this.gratuityDetail[j].leavingDate = this.resultEmployee[j].leaving_date.toDate();
        this.gratuityDetail[j].graduityAmount = this.TotalGratuitySalary;
        j++;


      }
      //try for only one recored not final when id is 12
      if (i == 12) {
        break;
      }

    }
    this.getAllData()
   
    console.log('Detail Copy in Gratuity', this.gratuityDetail);
  }

  initDatatable(){
  
    
  }



  getAllData(): void {
    
    debugger;
    let i;
    for (i = 0; i < this.gratuityDetail.length; i++) {
      this.data.push(this.gratuityDetail[i].employeeCode);
      this.data.push(this.gratuityDetail[i].employeeName);
      this.data.push(this.gratuityDetail[i].department);
      this.data.push(this.gratuityDetail[i].designation)
      this.data.push(this.gratuityDetail[i].joiningDate)
      this.data.push(this.gratuityDetail[i].leavingDate)
      this.data.push(this.gratuityDetail[i].graduityAmount)
      // this.data.push(this.gratuityDetail[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];


    }
    
    
  }

  ///////////////////////////////////////////////////////////////////////////////////
  /////////////////////////*****  Get Data to filter *****//////////////////////////////////

  
  getAllCostCenterRecord(): void {
    debugger;
    this._costCenterService.getAllCostCenters()
        .finally(() => {
            console.log('completed');
        })
        .subscribe((result: any) => {

            this.resultCost = result;
            this.CostCenterName = result;


        });
}


getAllDepartmentRecord(): void {
    debugger;
    this._departmentService.getAllDepartments()
        .finally(() => {
            console.log('completed');
        })
        .subscribe((result: any) => {

            this.resultDept = result;
            this.DepartmentName = result;


        });

        this.getAllEmployee();
}


// getAllEmployeeRecord(): void {
//     debugger;
//     this._employeeService.getAllEmployees()
//         .finally(() => {
//             console.log('completed');
//         })
//         .subscribe((result: any) => {

//             this.resultEmp = result;
           


//         });
// }

  ///////////////////////////////////////////////////////////////////////////////////
  /////////////////////////*****   Filtering Data *****//////////////////////////////////

@Input()
Ivalue: any


//CostCenterfilters on basis of Id

CostCenterHead(event, id: number): void {
    debugger

    let h;
    h = event.source.focused;

    for (let i = 0; i < this.resultCost.length; i++) {
        if (this.resultCost[i].id == event.value) {


            if (h == true) {
                this.tempArr[i] = event.value;

            }

            else {

                this.tempArr[i] = null;
            }

        }

    }


    this.CostCenter();


}



//Filtered CostCenter
CostCenter(): void {
    debugger
    this.CostCenterName = [];
    let p = 0;
    for (let i = 0; this.tempArr.length; i++) {
        if (this.resultCost[i].id == this.tempArr[i]) {
            this.CostCenterName[p] = this.resultCost[i];
            p++;
        }
    }
}

CostCenterId(event, id: number): void {
    debugger

    let h;
    h = event.source.focused;
    for (let i = 0; i < this.resultCost.length; i++) {
        if (this.resultCost[i].id == event.value) {

            if (h = true) {
                this.tempArrCost[i] = event.value;
            }
            else {
                //id of cost center array
                this.tempArrCost[i] = null;
            }
        }
    }

    this.CostCenterArrayId = this.tempArr.toString();
}


DepartmentId(event, id: number): void {
    debugger

    let h;
    h = event.source.focused;

    for (let i = 0; i < this.resultDept.length; i++) {
        if (this.resultDept[i].id == event.value) {

            if (h = true) {
                this.tempArray[i] = event.value;
            }
            else {

                //id of dept array
                this.tempArray[i] = null;
            }

        }

    }
    this.DepartmentArrayId = this.tempArray.toString();
    //    this.Employee();
    this.employeeList();
}


//employees on the basis of depart and cost center

employeeList() {

    this.empSelected = []
    let t = 0;
    for (let j = 0; j < this.CostCenterName.length; j++) {

        for (let n = 0; n < this.tempArray.length; n++) {

            if (this.tempArray[n] == this.resultEmployee[n].dept_ID && this.CostCenterName[n].id == this.resultEmployee[n].costCenterId) {

                this.empSelected[t] = this.resultEmployee[n];
                t++;
            }


        }


    }



}




EmployeesId(event, id: number): void {
    debugger

    let h,disabbleButton ;
    h = event.source.focused;


    for (let i = 0; i < this.resultEmployee.length; i++) {
        if (this.resultEmployee[i].id == event.value) {


            if (h == true) {
                this.tempArrayEmp[i] = event.value;

            }

            else {

                this.tempArrayEmp[i] = null;
            }

            


        }

    }//disable save button until no employee selected from dropdown

   
      if(this.tempArrayEmp.length < 0){
        disabbleButton = true;
      }

       //selected employee array id's
    this.EmployeesArrayId = this.tempArrayEmp.toString();
}

save(): void {
  debugger
  this.saving = true;
//save gratuity details in master gratuity which is calculated
  this.gratuityMasterData.graduityDetails = this.gratuityDetail;

//id's in string to change employees and store in gratuity configuration
  this.gratuityConfigData.employeeId = this.EmployeesArrayId;
  this.gratuityConfigData.departmentId = this.DepartmentArrayId;
  this.gratuityConfigData.costCenterId = this.CostCenterArrayId;
  
  
 
  



  this._GratuityMasterService.create(this.gratuityMasterData)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        swal({
          title: 'Added',
          text: 'Your Details has been added.',
          type: 'success',
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        }).catch(swal.noop)

      });
      // this._gratuityConfigService.create(this.gratuityConfigData)
      // .finally(() => { this.saving = false; })
      // .subscribe(() => {
        

      // });



  }

  




}


