import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHrConfigurationComponent } from './edit-hr-configuration.component';

describe('EditHrConfigurationComponent', () => {
  let component: EditHrConfigurationComponent;
  let fixture: ComponentFixture<EditHrConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditHrConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHrConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
