import { Component, OnInit, ViewChild, Injector, Inject, ElementRef, EventEmitter, Output } from '@angular/core';
import swal from 'sweetalert2';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-edit-hr-configuration',
  templateUrl: './edit-hr-configuration.component.html',
  styleUrls: ['./edit-hr-configuration.component.scss']
})
export class EditHrConfigurationComponent implements OnInit {

  @ViewChild('editHRConfigurationModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  active: boolean = false;
  saving: boolean = false;
  HrConfiguration: HRConfigurationDto = new HRConfigurationDto();


  constructor(injector: Injector,
    // private _userService: UserServiceProxy,
    private _HRConfigurationService: HRConfigurationServiceProxy) { }


  show(id: number): void {
    this._HRConfigurationService.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();
      })
      .subscribe((result: HRConfigurationDto) => {
        this.HrConfiguration = result;

      })
  }

  save(): void {
    debugger;



    this.saving = true;
    this._HRConfigurationService.update(this.HrConfiguration)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        // this.notify.info(this.l('Record Updated Successfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }


  ngOnInit() {
  }

  onShown(): void {

  }




}