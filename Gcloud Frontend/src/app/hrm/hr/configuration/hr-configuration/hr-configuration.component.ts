import { Component, OnInit, ViewChild, Injector, Inject } from '@angular/core';
import swal from 'sweetalert2';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import { EditHrConfigurationComponent } from './edit-hr-configuration/edit-hr-configuration.component';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;


@Component({
  selector: 'app-hr-configuration',
  templateUrl: './hr-configuration.component.html',
  styleUrls: ['./hr-configuration.component.scss'],
  providers:[HRConfigurationServiceProxy]
})
export class HrConfigurationComponent implements OnInit {


  @ViewChild('editHRConfigurationModal') editHRConfigurationModal: EditHrConfigurationComponent;

  public dataTable: DataTable;
  result: HRConfigurationDto[] = [];
  addNewHRConfiguration: HRConfigurationDto = new HRConfigurationDto();
  data: string[] = [];


  constructor(injector: Injector, private _HRConfigurationService: HRConfigurationServiceProxy,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService

  ) { }


  ngOnInit() {
    this.getAllRecord();
    this.storage.get('TenantId');
    //console.log("this is tenant id: ", this.storage.get('TenantId'));
  }

  getAllRecord(): void {
    this._HRConfigurationService.getAllHRConfiguration()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        debugger;
        this.result = result;
        //if result is empty then it will create default HR configuration.
        if (this.result.length == 0) {
         
          this.addNewHRConfiguration.autoCode= false;
          //this.addNewHRConfiguration.tenantId = 0;//this.storage.get('TenantId');
        
          
          this._HRConfigurationService.create(this.addNewHRConfiguration)
            .finally(() => {
              //this.saving = false;
            })
            .subscribe(() => {
              // this.notify.info(this.l('Saved Successfully'));
              // this.close();
              // this.modalSave.emit(null);
              //alert("Default Configuration Added. Please edit HR configuration according to your requirement.");
              
              swal({
                title: "Important Note!",
                text: "Default Configuration Added. Please edit HR configuration according to your requirement.",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "success"
            }).catch(swal.noop)



              this.getAllRecord();
            });
        }
        this.paging();
        this.getAllData();
      });
  }


  paging(): void {
    this.dataTable = {
      headerRow: ['Auto Code' , 'Actions'],
      footerRow: [ /*'Code', 'Name', 'Sort Order', 'Remarks', 'Actions'*/],

      dataRows: [
       
      ]
    };
  }

  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      //
      this.data.push(this.result[i].autoCode.toString());
      
      this.data.push(this.result[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }



  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }
    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  //   AddCategory(): void {
  //     this.addCategoryModal.show();
  // }

  EditHRConfiguration(id: string): void {
    this.editHRConfigurationModal.show(parseInt(id));
  }

}
