import { Component, OnInit, ViewChild, AfterViewInit, Injector, Inject } from '@angular/core';
import { OvertimeConfigurationServiceProxy, OvertimeConfigurationDto } from '@app/shared/service-proxies/service-proxies';
import { EditOvertimeConfigurationComponent } from '@app/hrm/hr/configuration/overtime-configuration/edit-overtime-configuration/edit-overtime-configuration.component';
import { WebStorageService, LOCAL_STORAGE } from '../../../../../../node_modules/angular-webstorage-service';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';
// import { AddCategoryComponent } from './add-category/add-category.component';
//import { EditCategoryComponent } from './edit-category/edit-category.component';


declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;



@Component({
  selector: 'app-overtime-configuration',
  templateUrl: './overtime-configuration.component.html',
  styleUrls: ['./overtime-configuration.component.scss'],
  providers: [OvertimeConfigurationServiceProxy]
})
export class OvertimeConfigurationComponent implements OnInit {


  // @ViewChild('addCategoryModal') addCategoryModal: AddCategoryComponent;
  @ViewChild('editOvertimeConfigurationModal') editOvertimeConfigurationModal: EditOvertimeConfigurationComponent;

  public dataTable: DataTable;
  result: OvertimeConfigurationDto[] = [];
  addNewOvertimeConfiguration: OvertimeConfigurationDto = new OvertimeConfigurationDto();
  data: string[] = [];


  constructor(injector: Injector, private _overtimeConfigurationService: OvertimeConfigurationServiceProxy,
    private _router: Router,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService

  ) { }

  globalFunction: GlobalFunctions = new GlobalFunctions
  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "Configuration")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate(['']);
    }
    this.getAllRecord();
    // this.storage.get('TenantId');
    console.log("this is tenant id: ", this.storage.get('TenantId'));
  }

  getAllRecord(): void {
    this._overtimeConfigurationService.getAllOverTimeConfiguration()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        this.result = result;
        //if result is empty then it will create default overtime configuration.
        if (this.result.length == 0) {
          this.addNewOvertimeConfiguration.offDayHourRate = 500;
          this.addNewOvertimeConfiguration.regularDayHourRate = 500;
          this.addNewOvertimeConfiguration.tenantId = this.storage.get('TenantId');
          //this.addNewOvertimeConfiguration.workingDays = 5;
          this.addNewOvertimeConfiguration.workinghours = 9;
          this.addNewOvertimeConfiguration.defaultWorkingDays = true;
          this._overtimeConfigurationService.create(this.addNewOvertimeConfiguration)
            .finally(() => {
              //this.saving = false;
            })
            .subscribe(() => {
              // this.notify.info(this.l('Saved Successfully'));
              // this.close();
              // this.modalSave.emit(null);
              //alert("Default Configuration Added. Please edit overtime configuration according to your requirement.");
              
              swal({
                title: "Important Note!",
                text: "Default Configuration Added. Please edit overtime configuration according to your requirement.",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "success"
            }).catch(swal.noop)



              this.getAllRecord();
            });
        }
        this.paging();
        this.getAllData();
      });
  }


  paging(): void {
    this.dataTable = {
      headerRow: ['Working Days', 'Working Hours', 'Working Days/hr', 'Off Days/hr', 'Default Working Day', 'Actions'],
      footerRow: [ /*'Code', 'Name', 'Sort Order', 'Remarks', 'Actions'*/],

      dataRows: [
        // ['123', 'ABC', '1', 'abc',''],
        // ['123', 'ABC', '1', 'abc',''],
        // ['123', 'ABC', '1', 'abc',''],
        // ['123', 'ABC', '1', 'abc',''],
      ]
    };
  }

  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].workingDays);
      this.data.push(this.result[i].workinghours.toString());
      this.data.push(this.result[i].regularDayHourRate.toString());
      this.data.push(this.result[i].offDayHourRate.toString());
      this.data.push(this.result[i].defaultWorkingDays.toString());
      this.data.push(this.result[i].id.toString());

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }



  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }
    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  //   AddCategory(): void {
  //     this.addCategoryModal.show();
  // }

  EditOvertimeConfiguration(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "Configuration")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
    this.editOvertimeConfigurationModal.show(parseInt(id));
  }

}
