import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { ItemTypeServiceProxy, ItemTypeDto } from '@app/shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}


@Component({
  selector: 'app-search-item-type',
  templateUrl: './search-item-type.component.html',
  styleUrls: ['./search-item-type.component.scss']
})
export class SearchItemTypeComponent implements OnInit {

  @ViewChild('searchDropdownModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  
  public dataTable: DataTable;
    data: string[] = [];

    result: ItemTypeDto[] = [];

    ItemTypeId: any;

  _itemType : FormGroup
  constructor(private itemTypeService:ItemTypeServiceProxy ,
    private _router : Router) { }

  ngOnInit() {
    this.getAllRecord()
  }
  getAllRecord(): void {
    debugger;
    this.itemTypeService.getAllItemTypes()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        this.paging();
        this.result = result;
        this.getAllData();

      });
  }
  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].id.toString());
      this.data.push(this.result[i].code);
      this.data.push(this.result[i].name);
     

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }
  paging(): void {
    this.dataTable = {
      headerRow: [ 'Id', 'Code', 'Type' ],
        footerRow: [ 'Id', 'Code', 'Type' ],
        dataRows: [
        
      ]

  };
}
ngAfterViewInit() {
  $('#datatabless').DataTable({
    "pagingType": "full_numbers",
    "lengthMenu": [
      [10, 25, 50, -1],
      [10, 25, 50, "All"]
    ],
    responsive: true,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search records",
    }

  });

  const table = $('#datatabless').DataTable();

  $('.card .material-datatables label').addClass('form-group');
}

  show(){
    this.active = true;
    this.modal.show();

  }
  close(): void {
    this.active = false;
    this.modal.hide();
}
//smart Search
@Input()
Ivalue: any 

//smart search 
Data(ItemTId : any) : void{
  debugger
this.Ivalue = ItemTId
this.active =  true
this.close();
this.modalSave.emit(this.Ivalue);

}

}
