import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ItemDto, ItemServiceProxy, CategoryDto, CategoryServiceProxy, SubCategoryDto, SubCategoryServiceProxy, ItemTypeDto, ItemTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'edit-item',
  templateUrl: './edit-item.component.html',
  providers: [ItemServiceProxy, CategoryServiceProxy, SubCategoryServiceProxy, ItemTypeServiceProxy]
})
export class EditItemComponent implements OnInit {

  @ViewChild('editItemModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;


  category: CategoryDto[];
  subCategory: SubCategoryDto[];
  itemType: ItemTypeDto[];
  item: ItemDto = new ItemDto();
  _item : FormGroup;

  constructor(
    injector: Injector,
    // private _userService: UserServiceProxy

    private itemService: ItemServiceProxy,
    private catService: CategoryServiceProxy,
    private subCatService: SubCategoryServiceProxy,
    private itemTypeService: ItemTypeServiceProxy,
    private formBuilder: FormBuilder


  ) {
    // super(injector);
  }


  show(id:number):void  {

    debugger;
    this.itemService.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();

      })
      .subscribe((result: ItemDto) => {

        this.item = result;


      });
  }

  initValidation() {

    this._item = this.formBuilder.group({
        itemCode: [null, Validators.required],
        itemTypeId: [null, Validators.required],
        categoryId: [null, Validators.required],
        subCategoryId: [null, Validators.required],
        itemName: [null, Validators.required],
        
        
      });

}

onType() {

  if (this._item.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._item);
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}




  ngOnInit(): void {
    this.getAllCategory();
    this.getAllSubCategory();
    this.getAllItemType();
    this.initValidation();

  }


  onShown(): void {
    // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }


  //to get all categories

  getAllCategory() {

    this.catService.getAllCategories().subscribe((result) => {

      this.category = result;
    })

  }

  // to get all sub category

  getAllSubCategory() {

    this.subCatService.getAllSubCategories().subscribe((result) => {

      this.subCategory = result;
    })


  }

  // get all ItemType

  getAllItemType() {
    this.itemTypeService.getAllItemTypes().subscribe((result) => {

      this.itemType = result;
    })
  }


  save(): void {


    this.saving = true;

    this.itemService.update(this.item)
      .finally(() => { this.saving = false; })
      .subscribe(() => {

        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }



  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

}
