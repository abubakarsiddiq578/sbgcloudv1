import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AddItemComponent } from './add-item/add-item.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { ItemDto, ItemServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  providers:[ItemServiceProxy]
})
export class ItemComponent implements OnInit, AfterViewInit {

  @ViewChild('addItemModal') addItemModal: AddItemComponent;
  @ViewChild('editItemModal') editItemModal: EditItemComponent;

    public dataTable: DataTable;
    data:string[]=[];

    Item: ItemDto[] ;


    constructor(private ItemService : ItemServiceProxy,private _router : Router){}

    globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "Item")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
        this.getAllItem();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }


    AddItem(): void {
      if (!this.globalFunction.hasPermission("Create", "Item")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addItemModal.show();
  }

  editItem(id:string): void{
    if (!this.globalFunction.hasPermission("Edit", "Item")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
      debugger;
      this.editItemModal.show(parseInt(id));
  }
/*
  check(id: string  ): void {
    debugger;
    this.editItemModal.show(parseInt(id));
  }*/

  initializeDataTable(){

    this.dataTable = {
      headerRow: [ 'Code', 'Type', 'Name', 'Category', 'Sub Category', 'Actions' ],
      footerRow: [/* 'Item Type', 'Name', 'Category', 'Sub Category', 'Actions' */],

      dataRows: []
        
   };

  }
  fillDataTable(){

    let i;
    for (i = 0; i < this.Item.length; i++) {

      this.data.push(this.Item[i].itemCode)
      this.data.push(this.Item[i].itemType.name)
      
      this.data.push(this.Item[i].name)

      this.data.push(this.Item[i].category.name)

      
      this.data.push(this.Item[i].subCategory.name)

      this.data.push(this.Item[i].id.toString())
      
      
      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }


  getAllItem():void{

      this.ItemService.getAllItems().subscribe((result)=>{

      this.initializeDataTable()  
      this.Item = result;
      this.fillDataTable();
    })

  }


  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "Item")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.ItemService.delete(parseInt(id))
          .finally(() => {
            this.getAllItem();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Item has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Item Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }


}
