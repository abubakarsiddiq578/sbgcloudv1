import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ItemDto, ItemServiceProxy, CategoryDto, CategoryServiceProxy, SubCategoryDto, SubCategoryServiceProxy, ItemTypeDto, ItemTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { ItemTypeComponent } from '../../item-type/item-type.component';
import { SearchItemTypeComponent } from '../search-item-type/search-item-type.component';
import { SearchCategoryComponent } from '../search-category/search-category.component';
import { SearchSubCategoryComponent } from '../search-sub-category/search-sub-category.component';
////////////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
    selector: 'add-item',
    templateUrl: './add-item.component.html',
    providers: [ItemServiceProxy, CategoryServiceProxy, SubCategoryServiceProxy, ItemTypeServiceProxy]
})
export class AddItemComponent implements OnInit {

    @ViewChild('addItemModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchDropdownModal') searchDropdownModel: SearchItemTypeComponent;
    @ViewChild('searchCategoryDropdownModal') searchCategoryDropdownModel: SearchCategoryComponent;
    @ViewChild('searchSubCategoryDropdownModal') searchSubCategoryDropdownModel: SearchSubCategoryComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    item: ItemDto = new ItemDto();

    ItemCode: string;
    ItemName: string;
    CategoryName: string;
    SubCategoryName: string;
    category: CategoryDto[];
    subCategory: SubCategoryDto[];
    itemType: ItemTypeDto[];

    _item: FormGroup;


    myControl : FormControl
        filteredOptions : Observable<CategoryDto[]>
        public  categoryName: string

    constructor(
        injector: Injector,
        private itemService: ItemServiceProxy,
        private catService: CategoryServiceProxy,
        private subCatService: SubCategoryServiceProxy,
        private itemTypeService: ItemTypeServiceProxy,

        private formBuilder: FormBuilder,
        


        // private _userService: UserServiceProxy

    ) {
        // super(injector);
    }

    show(): void {

        this.active = true;
        this.modal.show();
        this.item = new ItemDto();
        this._item.markAsUntouched({ onlySelf: true });



    }

    ngOnInit(): void {

        this.getAllCategory();
        this.getAllSubCategory();
        this.getAllItemType();
        this.initValidation();

    }

    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    
    this._item = this.formBuilder.group({
        itemCode: [null, Validators.required],
        itemTypeId: [null, Validators.required],
        categoryId: [null, Validators.required],
        subCategoryId: [null, Validators.required],
        itemName: [null, Validators.required],
            
      });
    }

    initValidation() {

        this._item = this.formBuilder.group({
            itemCode: [null, Validators.required],
            itemTypeId: [null, Validators.required],
            categoryId: [null, Validators.required],
            subCategoryId: [null, Validators.required],
            itemName: [null, Validators.required],


        });

    }

    //ItemType
    addSearchDD(): void {
        this.searchDropdownModel.show()
    }

    addCategoryDD(): void {
        this.searchCategoryDropdownModel.show()
    }
    addSubCategoryDD(): void {
        this.searchSubCategoryDropdownModel.show()
    }

    onType() {

        if (this._item.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._item);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
    //CategoryID
    CategoryId(id): void {
        debugger

        for (let i = 0; i < this.category.length; i++) {
            if (this.category[i].id == id) {
                this.CategoryName = this.category[i].name;
                this.item.categoryId = this.category[i].id;
            }
        }


    }
    //Category

    displayCategory = (category: CategoryDto): any => {
        debugger;

        if (category instanceof CategoryDto) {
            this.item.categoryId = category.id;
            return category.name;
        }
        return category;

    }
    //SubCategoryID
    SubCategoryId(id): void {
        debugger

        for (let i = 0; i < this.subCategory.length; i++) {
            if (this.subCategory[i].id == id) {
                this.SubCategoryName = this.subCategory[i].name;
                this.item.subCategoryId = this.subCategory[i].id;
            }
        }


    }
    //ItemType

    displaySubCategory = (subCategorys: SubCategoryDto): any => {
        debugger;

        if (subCategorys instanceof SubCategoryDto) {
            this.item.subCategoryId = subCategorys.id;
            return subCategorys.name;
        }
        return subCategorys;

    }



    //ItemTypeID
    ItemTypeId(id): void {
        debugger

        for (let i = 0; i < this.itemType.length; i++) {
            if (this.itemType[i].id == id) {
                this.ItemName = this.itemType[i].name;
                this.item.itemTypeId = this.itemType[i].id;
            }
        }


    }
    //ItemType

    displayItemType = (itemType: ItemTypeDto): any => {
        debugger;

        if (itemType instanceof ItemTypeDto) {
            this.item.itemTypeId = itemType.id;
            return itemType.name;
        }
        return itemType;

    }


    //to get category

    getAllCategory() {

        this.catService.getAllCategories().subscribe((result) => {

            this.category = result;
        })

    }


    // to get all sub category

    getAllSubCategory() {

        this.subCatService.getAllSubCategories().subscribe((result) => {

            this.subCategory = result;
        })


    }


    // get all ItemType

    getAllItemType() {
        this.itemTypeService.getAllItemTypes().subscribe((result) => {

            this.itemType = result;
        })
    }


    save(): void {

        debugger;
        this.saving = true;

        this.itemService.create(this.item)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify();
                this.close();
                this.modalSave.emit(null);
            });
    }

    notify() {
        swal({
            title: "Success!",
            text: "Saved Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
