import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { CostCenterDto, CostCenterServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';

import { ModalDirective } from 'ngx-bootstrap';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

///////////////////////////////
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'add-cost-center',
  templateUrl: './add-cost-center.component.html',
  styleUrls: ['./add-cost-center.component.scss'],
  providers: [CostCenterServiceProxy , HRConfigurationServiceProxy]
})
export class AddCostCenterComponent implements OnInit {

  @ViewChild('addCostCenterModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  costCenter: CostCenterDto = new CostCenterDto();


  isActive: boolean;
  _costCenterValidation: FormGroup; // for form validation 

  _costCenterCodeArray: abc[] = []; //array for shifting cost center codes

  _costCenter: CostCenterDto[]; //for getting all costCentercode  from  Cost Center table

  //public cGD : CostCenterDto[];

  myControl = new FormControl();
  
  //filteredOptions: Observable<CostCenterDto[]>;

  filteredOptions : Observable<string[]>;
  data : string[] =[];

  public cGD : string[] = [];

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  constructor(private costCenterService: CostCenterServiceProxy, private formBuilder: FormBuilder , private _hrConfigService : HRConfigurationServiceProxy) { }

  ngOnInit() {
 
    this.initValidation();
   // this.getAllCostCenterGroups();
    /*
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(costCenterGroup => this._filter(costCenterGroup))
      );
        */

       this.getAllCostCenterGroups();
       
       this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );



       /*
       this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | CostCenterDto>(''),
        map(value => typeof value === 'string' ? value : value.costCenterGroup),
        map(costCenterGroup => costCenterGroup  ? this._filter(costCenterGroup) : this.cGD.slice())
      );*/
       //this.filteredGroupData();
       this.getAllHRConfiguration();
  }

  

  initValidation() {

    this._costCenterValidation = this.formBuilder.group({
      costCenterName: [null, Validators.required],
      costCenterGp: [null, Validators.required]
    });
  }

  onType() {
    debugger;
    if (this._costCenterValidation.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._costCenterValidation);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  show(): void {

    this.active = true;
    this.modal.show();
    this.costCenter = new CostCenterDto();
    this.isActive = true;
    this.costCenterService.getAllCostCenters().subscribe((result) => {

      this._costCenter = result;

      if(this.hrConfig.autoCode == true){

        this.getAutoCostCenterNumber();
        this.isAutoCode = true ;
      }
      else if(this.hrConfig.autoCode == false){
        this.isAutoCode = false;
      }
    })
    this.getAllCostCenterGroups();
    this._costCenterValidation.markAsUntouched({onlySelf:true});
  }

  onShown(): void {
    
  }

  
  //////////////////////filter Group ////////////////////
  /*
private _filter(costCenterGroup: string): CostCenterDto[] {
  
    const filterValue = costCenterGroup.toLowerCase();
    return this.cGD.filter(option => option.costCenterGroup.toString().toLowerCase().indexOf(filterValue) === 0);
    
  }*/
  
  private _filter(costCenterGroup: string): any {
  
    const filterValue = costCenterGroup.toLowerCase();
    return this.cGD.filter(a=>a.toLowerCase().indexOf(filterValue) === 0);
  }



  // to get all cost center group
  getAllCostCenterGroups() {

    debugger;
    this.costCenterService.getAllCCG().subscribe((result)=>{

      this.cGD= result ;
    });


    /*
    let i;
    let j;
    let p ;
    let counter;
    for(i=0;i<=this.cGD.length;i++){
      counter = 0;
      for(j=0;j<=this.cGD.length;j++){

        if(this.cGD[i].costCenterGroup==this.cGD[j].costCenterGroup){
          if(i!=j){
            counter = 1;
          }
          else{}
          
        }
        else{

        }
      }
      if(counter!=1)
      {
        this.data[p] = this.cGD[i].costCenterGroup;
      }

    }
    */
    
  }
/////////////////////////////////////////////////////////////

  getAutoCostCenterNumber():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._costCenter.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._costCenter[i].code ; 

        this._costCenterCodeArray.push(abc_);

    }


    let code : any ;

    if(this._costCenterCodeArray.length == 0  ){
      
        debugger;
        code = "1";
        this.costCenter.code = "00" + code ;

    }

    else{

        if(this._costCenter[this._costCenterCodeArray.length - 1] != null ){

                let x;
                code = this._costCenterCodeArray[this._costCenterCodeArray.length-1].code ;
                temp = code;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.costCenter.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.costCenter.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.costCenter.code =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.costCenter.code = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.costCenter.code =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.costCenter.code =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.costCenter.code = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.costCenter.code =  "0" + code;
                  }
                  else if(code){
  
                      this.costCenter.code =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.costCenter.code = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.costCenter.code =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.costCenter.code =  temp[0] ;
                  }

                }



                
        }                     
    }
    this._costCenterCodeArray = [];
}


 
getAllHRConfiguration(){

  this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
  
    this.hrConfig = result[0] ;
  });
  
  }


  save(): void {

    debugger;
    this.costCenter.isActive = this.isActive;
    this.saving = true;

    this.costCenterService.create(this.costCenter)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }
}

// class for keeping all code number
class abc {
  public code: string;
}