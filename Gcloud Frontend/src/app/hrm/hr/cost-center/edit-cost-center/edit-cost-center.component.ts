import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { CostCenterDto, CostCenterServiceProxy, HRConfigurationServiceProxy, HRConfigurationDto } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';

import { ModalDirective } from 'ngx-bootstrap';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////////
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'edit-cost-center',
  templateUrl: './edit-cost-center.component.html',
  styleUrls: ['./edit-cost-center.component.scss'],
  providers: [CostCenterServiceProxy , HRConfigurationServiceProxy]
})
export class EditCostCenterComponent implements OnInit {

  @ViewChild('editCostCenterModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  costCenter: CostCenterDto = new CostCenterDto();


  isActive: boolean;
  _costCenterValidation: FormGroup; // for form validation 

  //public cGD : CostCenterDto[];

  public cGD: string[];
  
  myControl = new FormControl();

  //filteredOptions: Observable<CostCenterDto[]>;
  filteredOptions: Observable<string[]>;

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;


  constructor(private costCenterService: CostCenterServiceProxy, private formBuilder: FormBuilder , private _hrConfigService : HRConfigurationServiceProxy) { }

  ngOnInit() {

    this.initValidation();
    this.getAllCostCenterGroups();
    /* this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | CostCenterDto>(''),
      map(value => typeof value === 'string' ? value : value.costCenterGroup),
      map(costCenterGroup => costCenterGroup  ? this._filter(costCenterGroup) : this.cGD.slice())
    );*/

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

      this.getAllHRConfiguration();

  }


  getAllHRConfiguration() {

    this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

        this.hrConfig = result[0];


        if (this.hrConfig.autoCode == true) {

            this.isAutoCode = true;
        }
        else if (this.hrConfig.autoCode == false) {
            this.isAutoCode = false;
        }


    });

}

  /////////////////Code For Validation/////////////
  initValidation() {

    this._costCenterValidation = this.formBuilder.group({
      costCenterName: [null, Validators.required],
      costCenterGp: [null, Validators.required]

    });

  }


  




  onType() {
    debugger;
    if (this._costCenterValidation.valid) {
      this.save();
    } else {
      this.validateAllFormFields(this._costCenterValidation);

    }
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  /////////////////////////////////////////////End Code For Validation//////////////////////

  filteredGroupData() {
    /*
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(costCenterGroup => this._filter(costCenterGroup))
    );*/

  }

  private _filter(costCenterGroup: string): any {
    const filterValue = costCenterGroup.toLowerCase();

    //return this.cGD.filter(option => option.costCenterGroup.toLowerCase().indexOf(filterValue) === 0);
    return this.cGD.filter(a => a.toLowerCase().indexOf(filterValue) === 0);

  }


  // to get all cost center group
  getAllCostCenterGroups() {

    /* debugger;
     this.costCenterService.getAllCostCenters().subscribe((result)=>{
 
         this.cGD = result;
     })*/

    this.costCenterService.getAllCCG().subscribe((result) => {

      this.cGD = result;
    })


  }
  /////////////////////////////////////////////////////////

  show(id: number): void {

    this.costCenterService.get(id).finally(() => {

      this.active = true;
      this.modal.show();

    }).subscribe((result: CostCenterDto) => {
      this.costCenter = result;
    });

  }

  onShown(): void {

  }


  save(): void {

    this.saving = true;

    this.costCenterService.update(this.costCenter).finally(() => {

      this.saving = false;

    }).subscribe(() => {

      this.notify();
      this.close();
      this.modalSave.emit(null);

    })
  }


  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();

  }

}
