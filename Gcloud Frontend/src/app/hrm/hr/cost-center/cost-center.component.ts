import { Component, OnInit,ViewChild, AfterViewInit  } from '@angular/core';
import { AddCostCenterComponent } from './add-cost-center/add-cost-center.component';
import { EditCostCenterComponent } from './edit-cost-center/edit-cost-center.component';
import { CostCenterDto, CostCenterServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-cost-center',
  templateUrl: './cost-center.component.html',
  styleUrls: ['./cost-center.component.scss'],
  providers:[CostCenterServiceProxy]
})
export class CostCenterComponent implements OnInit,AfterViewInit {


  @ViewChild('addCostCenterModal') addCostCenterModal: AddCostCenterComponent;
  @ViewChild('editCostCenterModal') editCostCenterModal: EditCostCenterComponent;


  constructor(private costCenterService : CostCenterServiceProxy,
    private _router: Router
    ) { }

  public dataTable : DataTable;
  costCenter : CostCenterDto[]=[];
  data : string[] = [];

globalFunction : GlobalFunctions = new GlobalFunctions
  ngOnInit() {
    if(!this.globalFunction.hasPermission("View","CostCenter")){
          this.globalFunction.showNoRightsMessage("View");
          this._router.navigate(['']);
    }

    this.getAllCostCenter();
  }


  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();


    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }


  getAllCostCenter(){

    this.costCenterService.getAllCostCenters().subscribe((result)=>{

      this.intializeDatatable();
      this.costCenter = result;
      this.fillDatatable();

    })
  }

  intializeDatatable() {

    this.dataTable = {
      headerRow: ['Name', 'Code', 'Group', 'Sort Order', 'Active', 'Logical','Outward Gatepass','DayShift' ,'Actions'],
      footerRow: [/* */],

      dataRows: []

    };

  }

  fillDatatable() {
    let i;
    for (i = 0; i < this.costCenter.length; i++) {

      this.data.push(this.costCenter[i].name)
      this.data.push(this.costCenter[i].code)
      this.data.push(this.costCenter[i].costCenterGroup)
      this.data.push(this.costCenter[i].sortOrder.toString())
      this.data.push(this.costCenter[i].isActive.toString())
      this.data.push(this.costCenter[i].isLogical.toString())
      this.data.push(this.costCenter[i].outwardGatepass.toString())
      this.data.push(this.costCenter[i].dayShift.toString())
      
      this.data.push(this.costCenter[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }



  AddCostCenter(): void {
    if (!this.globalFunction.hasPermission("Create", "CostCenter")) {
      this.globalFunction.showNoRightsMessage("Create");
      return
    }

    this.addCostCenterModal.show();
  }

  EditCostCenter(id: string): void {
    if (!this.globalFunction.hasPermission("Edit", "CostCenter")) {
      this.globalFunction.showNoRightsMessage("Edit");
      return
    }
   this.editCostCenterModal.show(parseInt(id));
  }



  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "CostCenter")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.costCenterService.delete(parseInt(id))
          .finally(() => {
            this.getAllCostCenter();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'CostCenter has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'CostCenter Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }




}
