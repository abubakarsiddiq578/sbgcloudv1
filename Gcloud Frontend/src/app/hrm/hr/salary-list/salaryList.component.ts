import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ItemServiceProxy, ItemDto, ItemTypeDto, SalaryMasterServiceProxy, SalaryMasterDto, AutoSalary, AutoSalaryDto, SalaryWizardServiceProxy, EmployeeServiceProxy, CostCenterServiceProxy, DepartmentServiceProxy, SalaryExpenseTypeServiceProxy, AutoSalaryServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { ColDef, GridApi, ColumnApi } from '../../../../../node_modules/ag-grid-community';
import { iteratorToArray } from '../../../../../node_modules/@angular/animations/browser/src/util';
//import { GlobalFunctions } from '@app/GlobalFunctions';
import { GoogleLoginProvider } from '../../../../../node_modules/angularx-social-login';
import { debug } from 'util';
import { SalaryWizardComponent } from '../salary-wizard/salary-wizard.component';
import { audit } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import 'rxjs/add/observable/forkJoin';
import * as moment from 'moment';
import swal from 'sweetalert2';


@Component({
    selector: 'salaryList',
    templateUrl: './salaryList.component.html',
    styleUrls: ['./salaryList.component.scss'],
    providers: [ItemServiceProxy, SalaryMasterServiceProxy, 
        SalaryWizardComponent, EmployeeServiceProxy, CostCenterServiceProxy,
    DepartmentServiceProxy , SalaryExpenseTypeServiceProxy, AutoSalaryServiceProxy ]
})
export class SalaryListComponent implements OnInit {

    @ViewChild('PageSize') PageSize: any;
    //gf = new GlobalFunctions
    item: ItemDto = new ItemDto()
    salary: AutoSalaryDto = new AutoSalaryDto();


    salaryGenerate: AutoSalaryDto = new AutoSalaryDto();

    salaryG: any[] = [];

    //salaryArray: AutoSalaryDto[];



    salaryList: any[] = [];

    SalaryExpenseTypes : any[] = [];
    dynamicColumn : any[] = [];

    // row data and column definitions
    public rowData: AutoSalaryDto[];
    public columnDefs: ColDef[];
    public defaultColDef;

    // gridApi and columnApi
    public api: GridApi;
    public columnApi: ColumnApi;

    //row selectoin 
    public rowSelection;

    // inject the itemService
    constructor(
        private itemService: ItemServiceProxy,
        private _salaryService: SalaryMasterServiceProxy,
        private salaryWizard: SalaryWizardComponent,
        private _salaryExpenseTypeService: SalaryExpenseTypeServiceProxy,
        private _autoSalaryService: AutoSalaryServiceProxy
        ) {
        
        this.defaultColDef = { editable: true };
        this.rowSelection = "multiple"
        
        this.salaryWizard.shareDataSubject
            .subscribe((receiveData)=>{
                console.log("ReceiveData:", receiveData);
            });
        

    }

    // on init, subscribe to the item data
    ngOnInit() {

        

        this.salary.autoSalaryDetails =[];
        this.rowData = [];
        this.salaryList = JSON.parse(localStorage.getItem("salaryFilter"));
        debugger;
        if(this.salaryList != null)
        {
            for(let j = 0; j < this.salaryList.length; j++)
            {
                this.salary.id = this.salaryList[j].id;
                this.salary.absentDays = this.salaryList[j].absentDays;
                this.salary.absentDeduction = this.salaryList[j].absentDeduction;
                this.salary.allowedLeaves = this.salaryList[j].allowedLeaves;
                this.salary.code = this.salaryList[j].code;
                this.salary.costCenterName = this.salaryList[j].costCenterName;
                this.salary.currentLeaves = this.salaryList[j].currentLeaves;
                this.salary.daysInMonth = this.salaryList[j].daysInMonth;
                this.salary.deductionAdvAgainstSalary = this.salaryList[j].deductionAdvAgainstSalary;
                this.salary.designation = this.salaryList[j].designation;
                this.salary.empDepartment = this.salaryList[j].empDepartment;
                this.salary.empId = this.salaryList[j].empId;
                this.salary.graduityFund = this.salaryList[j].graduityFund;
                this.salary.grossSalary = this.salaryList[j].grossSalary;
                this.salary.incomeTax = this.salaryList[j].incomeTax;
                this.salary.leaveBalance = this.salaryList[j].leaveBalance;
                this.salary.name = this.salaryList[j].name;
                this.salary.overtime = this.salaryList[j].overtime;
                this.salary.overtimeHrs = this.salaryList[j].overtimeHrs;
                this.salary.presentDays = this.salaryList[j].presentDays;
                this.salary.totalSalary = this.salaryList[j].totalSalary;
                this.salary.visitAllowance = this.salaryList[j].visitAllowance;
                this.salary.workingDays = this.salaryList[j].workingDays;
                this.salary.salaryType = this.salaryList[j].salaryType;
                this.salary.salaryMonth =  moment(this.salaryList[j].salaryMonth);
                for(let k = 0; k < this.salaryList[j].employeeSalaryDetailList.length; k++)
                {
                    this.salary.autoSalaryDetails.push(this.salaryList[j].employeeSalaryDetailList[k])
                } 
                this.rowData.push(this.salary);
                this.salary = new AutoSalaryDto();
                this.salary.autoSalaryDetails = [];            
            }
        }
        
        localStorage.removeItem("salaryFilter")

        this._salaryService.getAllSalaryTypes()
            .subscribe((salaryTypes) => {
                
                this.SalaryExpenseTypes = salaryTypes;
                
                for (let i = 0; i < this.SalaryExpenseTypes.length; i++) {
                    this.dynamicColumn.push({ headerName: this.SalaryExpenseTypes[i], valueGetter: (params) => params.data.autoSalaryDetails[i].earning , editable : false, width: 120 })
                  }

                  this.columnDefs = this.createColumnDefs();
            });
        
    }

    onCellValueChanged(params) {
        debugger
        let dataObject = params.data;
        // this._autoSalaryService.editGeneratedAutoSalary(params.data)
        // .subscribe(()=> {
        //     alert("Record is updated.");
        // });

      }



    // one grid initialisation, grap the APIs and auto resize the columns to fit the available space
    onGridReady(params): void {
        this.api = params.api;
        this.columnApi = params.columnApi;
        //this.api.sizeColumnsToFit();
    }

    // create some simple column definitions
    private createColumnDefs() {
    

        let coloumn = [];
    

       let staticColumn = [
            { field: 'name', editable : false },
            { field: 'code', editable : false },
            { field: 'salaryType', editable : false },
            { field: 'salaryMonth', editable : false },

            { field: 'costCenterName', editable : false},
            { field: 'designation', editable : false},
            { field: 'empDepartment' , editable : false },
            { field: 'absentDays', editable : false },
            { field: 'absentDeduction', editable : false },
            { field: 'allowedLeaves', editable : false },
            { field: 'availedLeaves', editable : false },
            { field: 'deductionAdvAgainstSalary' , editable : false},
            { field: 'graduityFund' , editable : false },
            { field: 'grossSalary' , editable : false },
            { field: 'incomeTax', editable : false },
            { field: 'overtime', editable : false },
            { field: 'overtimeHrs' , editable : false},

        ]
        coloumn =  staticColumn.concat(this.dynamicColumn)

        coloumn.push({ headerName: "Total Salary", field: 'totalSalary', editable : false,   width: 120 });
        //valueGetter: function chainValueGetter(params) { return params.getValue("grossSalary") - params.getValue("absentDeduction") },
        return coloumn
    }

    //save record in the grid
    save(): void {
        debugger;
        this.getAllRows()


        // for(let j = 0; j < this.rowData.length; j++)
        // {
        //     this.salary.absentDays = this.rowData[j].absentDays;
        //     this.salary.absentDeduction = this.rowData[j].absentDeduction;
        //     this.salary.allowedLeaves = this.rowData[j].allowedLeaves;
        //     this.salary.code = this.rowData[j].code;
        //     this.salary.costCenterName = this.rowData[j].costCenterName;
        //     this.salary.currentLeaves = this.rowData[j].currentLeaves;
        //     this.salary.daysInMonth = this.rowData[j].daysInMonth;
        //     this.salary.deductionAdvAgainstSalary = this.rowData[j].deductionAdvAgainstSalary;
        //     this.salary.designation = this.rowData[j].designation;
        //     this.salary.empDepartment = this.rowData[j].empDepartment;
        //     this.salary.empId = this.rowData[j].empId;
        //     this.salary.graduityFund = this.rowData[j].graduityFund;
        //     this.salary.grossSalary = this.rowData[j].grossSalary;
        //     this.salary.incomeTax = this.rowData[j].incomeTax;
        //     this.salary.leaveBalance = this.rowData[j].leaveBalance;
        //     this.salary.name = this.rowData[j].name;
        //     this.salary.overtime = this.rowData[j].overtime;
        //     this.salary.overtimeHrs = this.rowData[j].overtimeHrs;
        //     this.salary.presentDays = this.rowData[j].presentDays;
        //     this.salary.totalSalary = this.rowData[j].totalSalary;
        //     this.salary.visitAllowance = this.rowData[j].visitAllowance;
        //     this.salary.workingDays = this.rowData[j].workingDays;
        //     debugger;
        //     for(let k = 0; k < this.rowData[j].autoSalaryDetails.length; k++)
        //     {
        //         this.salary.autoSalaryDetails.push(this.rowData[j].autoSalaryDetails[k])
        //     } 
            

        //     this._autoSalaryService.addAutoSalary(this.salary[j])
        //         .subscribe(() => {
        //             alert('Save');
        //             this.salary = new AutoSalaryDto();
        //     this.salary.autoSalaryDetails = [];  
        //         });       
        //  }
        let observables = new Array();   

        if(this.rowData.length != 0 && this.rowData.length != null)
        {
            debugger;
            for(let i = 0; i < this.rowData.length; i++ )
            {
                observables.push(this._autoSalaryService.addAutoSalary( this.rowData[i]));              
            }
            Observable.forkJoin(observables)
                .subscribe(() => {
                    alert("Save");
                });
        }
        else
        {
            this.notifyMsg('Failed!', 'Grid Does not have any record to save. ');
        }

        
        // }

        // this.itemService.update(this.rowData[0])
        //     .subscribe(() => {
        //         this.gf.showErrorMessage("Success", "top", "right")
        //     })
    }

    //to show messages 
    notifyMsg(title, msg) {
        swal({
          title: title,
          text: msg,
          timer: 2000,
          showConfirmButton: false
        }).catch(swal.noop)
      }
    // this will return all the rows in the grid
    getAllRows() {
        let rowData = [];
        this.api.forEachNode(node => rowData.push(node.data));
        return rowData;
    }

    // this set the page size
    onPageSizeChanged(value) {
        debugger
        //let value : any = this.PageSize.value;
        this.api.paginationSetPageSize(Number(value));
    }

    // this will add new row in grid
    getGeneratedSalaries() {
        debugger
        this._autoSalaryService.getAllGeneratedSalaries()
            .subscribe((result)=> {
                debugger;
                
                if(result.length != 0 && result.length != null)
                {
                    this.rowData = result;
                }
                else
                {
                    this.notifyMsg('Failed!', 'There is no generated salary.');
                }
            });
        //var newItem = this.createNewRowData();
        //var res = this.api.updateRowData({ add: [newItem] });
    }

    //this will create new item model
    // createNewRowData() {
    //     let newData = {
    //         name: "New Item",
    //         category: { name: "New category" },
    //         subCategory: { name: "New Sub Category" },
    //         itemType: { name: "New Item Type" },
    //         id: "New ID"
    //     }
    //     return newData
    // }

    //Render Delete button
    // CellRendererDelete(params) {
    //     var button = document.createElement('button');
    //     button.innerHTML = 'Del';
    //     button.addEventListener('click', function () {
    //         window.alert("Delete button clicked with id: " + params.value);
    //     });
    //     ''
    //     return button;
    // }

   //Remove Selected Rows
    // onRemoveSelected() {
    //     var selectedData = this.api.getSelectedRows();
    //     var res = this.api.updateRowData({ remove: selectedData });
    // }

}