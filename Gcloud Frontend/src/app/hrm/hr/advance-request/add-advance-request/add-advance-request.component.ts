import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef,OnInit } from '@angular/core';
import { AdvanceRequestDto, AdvanceRequestServiceProxy , EmployeeDto , EmployeeServiceProxy ,  HRConfigurationDto , HRConfigurationServiceProxy} from 'app/shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
///////////////////////////////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'add-AdvanceRequest',
  templateUrl: './add-advance-request.component.html',
  styleUrls: ['./add-advance-request.component.scss'],
  providers:[AdvanceRequestServiceProxy,EmployeeServiceProxy , HRConfigurationServiceProxy]
})
export class AddAdvanceRequestComponent implements OnInit {

  @ViewChild('addAdvanceRequestModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  employee : EmployeeDto[];

  documentDate: Date = new Date();


  deductionDate: Date = new Date();

  AdvanceRequest : AdvanceRequestDto  = new AdvanceRequestDto()

  _advanceRequest  : AdvanceRequestDto[]; //for getting disable docNo Value from  resignation table
  _docNo : abc[] =[];
  
  _advnceReq : FormGroup // for validations

  
  myControl = new FormControl();

  filteredOptions: Observable<EmployeeDto[]>;

  public empName : string ;

  hrConfig: HRConfigurationDto =  new HRConfigurationDto();
  isAutoCode : boolean;
  
  constructor(injector : Injector ,

    private EmployeeServiceProxy : EmployeeServiceProxy,
    private AdvanceRequestService : AdvanceRequestServiceProxy, private _hrConfigService : HRConfigurationServiceProxy,
    private formBuilder : FormBuilder



  ) { }

  ngOnInit() {

    this.EmployeeServiceProxy.getAllEmployees().subscribe((result)=>{

      this.employee = result;
    })
    this.initValidation();
    this.getAllHRConfiguration();
  }



  getAllHRConfiguration(){

    this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
    
      this.hrConfig = result[0] ;
    });
    
    }


  initValidation() {

    this._advnceReq = this.formBuilder.group({
        employeeId: [null, Validators.required],
        advanceAmount: [null , Validators.required]
    });

}

/*onType() {

  if (this._advnceReq.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._advnceReq);
  }
}*/
onType() {
 
  if (this._advnceReq.valid) {
  
    let a,p=0 ;
    for(a=0;a<this.employee.length;a++){
      if(this.AdvanceRequest.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.empName = null; // null empName 
    
      this.validateAllFormFields(this._advnceReq);
      this.employeeDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._advnceReq);
     
  }
}


validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}
////////////////////////////////////////////////////////////////////


  ///////////////////////////////Search//////////////////////////////////
  public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }
  
  private _filter(value: string): EmployeeDto[]{
  
    debugger;
    const filterValue = value.toLowerCase();
  
    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.AdvanceRequest.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.AdvanceRequest.employeeId = null;
    return emp;
  
  }
///////////////////////////////////////////////////////////////////////////////////////
  show(): void {

    
    this.active = true;
    this.modal.show();
    this.AdvanceRequest = new AdvanceRequestDto();
    this.AdvanceRequestService.getAllAdvanceRequests().subscribe((result)=>{

      this._advanceRequest = result ;
     // this.getAutoDocNumber();
     if(this.hrConfig.autoCode == true){

      this.getAutoDocNumber();
      this.isAutoCode = true ;
    }
    else if(this.hrConfig.autoCode == false){
      this.isAutoCode = false;
    }



    })
    this.empName = null;
    this.employeeDropdownSearch();
    this._advnceReq.markAsUntouched({onlySelf:true});
  }

  //this function generate auto doc number
  getAutoDocNumber():void{

    debugger;
    let i =0;
    
    let temp : any[];
   
    for(i=0 ;i< this._advanceRequest.length ; i++){

        let abc_ ; 

        abc_ = new abc();

        abc_.code = this._advanceRequest[i].documentNo ; 

        this._docNo.push(abc_);

    }


    let code : any ;

    if(this._docNo.length == 0  ){
      
        debugger;
        code = "1";
        this.AdvanceRequest.documentNo = "00" + code ;

    }

    else{

        if(this._docNo[this._docNo.length - 1] != null ){

                let x;
                code = this._docNo[this._docNo.length-1].code ;
                if(code!=null){
                  temp = code.split("-");
                   x = parseInt(temp[0]);
                   x = x.toString();
                }
                debugger;
                //let j = parseInt(code);
                //
                if(temp.length == 1 && x=="NaN"  ){

                  debugger;
                  temp[1] = 0;
                  temp[1] ++;

                 
                  if(temp[1] <=9){
  
                      this.AdvanceRequest.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.AdvanceRequest.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.AdvanceRequest.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                
                }

                else if (temp.length == 2) {

                  temp[1] ++;
                  if(temp[1] <=9){
  
                      this.AdvanceRequest.documentNo = temp[0] + "-00" + temp[1] ;
                  }
                  else if(temp[1] <=99){
  
                      this.AdvanceRequest.documentNo =  temp[0] + "-0" + temp[1] ;
                  }
                  else if(temp[1]){
  
                      this.AdvanceRequest.documentNo =  temp[0] + "-" +temp[1] ;
                  }

                }

                else if(temp.length == 0){

                  code ++;
                  if(code <=9){
  
                      this.AdvanceRequest.documentNo = "00" + code ;
                  }
                  else if(code<=99){
  
                      this.AdvanceRequest.documentNo =  "0" + code;
                  }
                  else if(code){
  
                      this.AdvanceRequest.documentNo =  code ;
                  }


                } 
                else if(temp.length==1 && x!="NaN" ){

                  temp[0]++;
                  if(temp[0] <=9){
  
                      this.AdvanceRequest.documentNo = "00" + temp[0] ;
                  }
                  else if(code<=99){
  
                      this.AdvanceRequest.documentNo =  "0" + temp[0];
                  }
                  else if(code){
  
                      this.AdvanceRequest.documentNo =  temp[0] ;
                  }

                }



                
        }                     
    }
    this._docNo = [];
}





  save(): void {


    debugger;

    this.AdvanceRequest.deductionDate = moment(this.deductionDate).add(5,'hour')

    this.AdvanceRequest.documentDate = moment(this.documentDate).add(5,'hour')

    this.saving = true;
    this.AdvanceRequestService.create(this.AdvanceRequest)
      .finally(() => { this.saving = false; })
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }

  onShown():void{
    
  }


  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  

  close(): void {
    this.active = false;
    this.modal.hide();
  }
}

// class for keeping all docNo number
class abc{
  public code: string;
}

