import { Component,ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { AdvanceRequestDto, AdvanceRequestServiceProxy , EmployeeDto , EmployeeServiceProxy ,HRConfigurationDto , HRConfigurationServiceProxy} from 'app/shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
@Component({
  selector: 'edit-AdvanceRequest',
  templateUrl: './edit-advance-request.component.html',
  styleUrls: ['./edit-advance-request.component.scss'],
  providers:[AdvanceRequestServiceProxy,EmployeeServiceProxy , HRConfigurationServiceProxy]
})
export class EditAdvanceRequestComponent implements OnInit {


  @ViewChild('editAdvanceRequestModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  employee : EmployeeDto[];

  //documentDate: Date = new Date();


  //deductionDate: Date = new Date();


  hrConfig: HRConfigurationDto =  new HRConfigurationDto();

  isAutoCode : boolean;

  AdvanceRequest : AdvanceRequestDto  = new AdvanceRequestDto()
  _advnceReq : FormGroup // for validations

  myControl = new FormControl();

  filteredOptions: Observable<EmployeeDto[]>;

  public empName : string ;
  
  constructor(injector : Injector ,

    private EmployeeServiceProxy : EmployeeServiceProxy,
    private AdvanceRequestService : AdvanceRequestServiceProxy,
    private _hrConfigService : HRConfigurationServiceProxy,
    private formBuilder : FormBuilder

  ) { }

  ngOnInit() {

    this.EmployeeServiceProxy.getAllEmployees().subscribe((result)=>{

      this.employee = result;
    })
    this.initValidation();
    this.getAllHRConfiguration();
    
  }

  initValidation() {

    this._advnceReq = this.formBuilder.group({
        employeeId: [null, Validators.required],
        advanceAmount: [null , Validators.required]
    });

}



getAllHRConfiguration() {

  this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

      this.hrConfig = result[0];

      if (this.hrConfig.autoCode == true) {

          this.isAutoCode = true;
      }
      else if (this.hrConfig.autoCode == false) {
          this.isAutoCode = false;
      }


  });

}




/*onType() {

  if (this._advnceReq.valid) {
      this.save();
  } else {
      this.validateAllFormFields(this._advnceReq);
  }
}*/
onType() {
 
  if (this._advnceReq.valid) {
  
    let a,p=0 ;
    for(a=0;a<this.employee.length;a++){
      if(this.AdvanceRequest.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
      {
        p=1;
        this.save();
        break;
      }
    }
    if(p==0)
    {
      
      this.empName = null; // null empName 
      
      this.validateAllFormFields(this._advnceReq);
      this.employeeDropdownSearch();
    }
    
  } else {
     this.validateAllFormFields(this._advnceReq);
     
  }
}

validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
      }
  });
}
////////////////////////////////////////////////////////////////////

///////////////////////////////Search//////////////////////////////////
public employeeDropdownSearch(){
  debugger;
 this.filteredOptions = this.myControl.valueChanges
  .pipe(
    startWith<string | EmployeeDto >(''),
    map(value => typeof value === 'string' ? value : value.employee_Name),
    map(name => name ? this._filter(name) : this.employee.slice())
  ); 
 
}

private _filter(value: string): EmployeeDto[]{

  debugger;
  const filterValue = value.toLowerCase();

  return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
}


displayEmployee = (emp: EmployeeDto):any => {
  debugger;
  
  if(emp instanceof EmployeeDto)
  {
      this.AdvanceRequest.employeeId = emp.id ;
      return emp.employee_Name ;
  }
  this.AdvanceRequest.employeeId = null;
  return emp;

}
/////////////////////////////////////End of Search//////////////////////////////////////////////////




  onShown(): void {
    //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
}

  show(id: number): void {


    if(this.hrConfig.autoCode == true){

      this.isAutoCode = true ;
    }
    else if(this.hrConfig.autoCode == false){
      this.isAutoCode = false;
    }

    


    this.AdvanceRequestService.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();

      })
      .subscribe((result: AdvanceRequestDto) => {

        this.AdvanceRequest = result;
        debugger
        this.EmployeeServiceProxy.get(result.employeeId).subscribe((result)=>{
            this.empName = result.employee_Name;
        });

      });

      this.employeeDropdownSearch();
      this._advnceReq.markAsUntouched({onlySelf:true});

  }

  notify() {
    swal({
      title: "Success!",
      text: "Saved Successfully.",
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }


  save(): void {

    this.saving = true;
  
    this.AdvanceRequestService.update(this.AdvanceRequest)
      .finally(() => {this.saving = false;})
      .subscribe(() => {
        this.notify();
        this.close();
        this.modalSave.emit(null);
      });
  }


}
