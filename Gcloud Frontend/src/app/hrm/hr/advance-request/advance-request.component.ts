import { Component,ViewChild ,OnInit } from '@angular/core';
import { AdvanceRequestDto, AdvanceRequestServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import {AddAdvanceRequestComponent} from './add-advance-request/add-advance-request.component'; 
import {EditAdvanceRequestComponent} from './edit-advance-request/edit-advance-request.component'; 

declare const $: any;
declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'advance-request',
  templateUrl: './advance-request.component.html',
  
  providers:[AdvanceRequestServiceProxy]
})
export class AdvanceRequestComponent implements OnInit {



 @ViewChild('addAdvanceRequestModal') addAdvanceRequestModal: AddAdvanceRequestComponent;

 @ViewChild('editAdvanceRequestModal') editAdvanceRequestModal: EditAdvanceRequestComponent;

  public dataTable: DataTable;


  data: string[] = [];


  AdvanceRequest:AdvanceRequestDto[];


  constructor(private AdvanceRequestService : AdvanceRequestServiceProxy ) { }

  ngOnInit() {

    this.getAllAdvanceRequest();

  }


  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }



  initDataTable() {

    this.dataTable = {
     headerRow: ['Doc No', 'Document Date', 'Deduction Date', 'Employee Name', 'Amount', 'Detail', 'Actions'],
      footerRow: [/*Doc No', 'Document Date', 'Deduction Date', 'Employee Name', 'Amount', 'Detail', 'Actions'*/],

      dataRows: []
    };

  }


  fillDataTable() {
    
    let i;
    for (i = 0; i < this.AdvanceRequest.length; i++) {

      this.data.push(this.AdvanceRequest[i].documentNo)
      
      this.data.push(this.AdvanceRequest[i].documentDate.toString())

      this.data.push(this.AdvanceRequest[i].deductionDate.toString())
      

      this.data.push(this.AdvanceRequest[i].employee.employee_Name)

      this.data.push(this.AdvanceRequest[i].amount.toString())

      this.data.push(this.AdvanceRequest[i].detail)


      this.data.push(this.AdvanceRequest[i].id.toString())
      
     


      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }


  AddAdvanceRequest(): void {
   
    this.addAdvanceRequestModal.show();
  }


  EditAdvanceRequest(id: string): void {
    this.editAdvanceRequestModal.show(parseInt(id));
  }


  getAllAdvanceRequest():void {

    this.AdvanceRequestService.getAllAdvanceRequests().subscribe((result)=>{


      this.AdvanceRequest = result ;

      this.initDataTable()
      this.AdvanceRequest = result
      this.fillDataTable()

    })

  }


  protected delete(id: string): void {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.AdvanceRequestService.delete(parseInt(id))
          .finally(() => {
            this.getAllAdvanceRequest();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Advance Request has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Advance Request is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
}



}
