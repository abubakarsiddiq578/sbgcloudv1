
import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { ItemTypeServiceProxy, ItemTypeDto, CategoryDto, CategoryServiceProxy, ItemDto, ItemServiceProxy, EmployeeServiceProxy, EmployeeDto } from '@app/shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';

declare const $: any;



declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-search-item-group',
  templateUrl: './search-item-group.component.html',
  styleUrls: ['./search-item-group.component.scss']
})
export class SearchItemGroupComponent implements OnInit {

  @ViewChild('searchItemGroupDropdownModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;

  
  public dataTable: DataTable;
    data: string[] = [];

    result:ItemDto[] = [];

    Arr: any[] = []

    itemId : any

    checked: boolean
   tempArr : any[] = []
  _category: FormGroup
  constructor(private itemService:ItemServiceProxy ,
    private _router : Router) { }

  ngOnInit() {
    this.getAllRecord()
  }
  getAllRecord(): void {
    debugger;
    this.itemService.getAllItems()
      .finally(() => {
        console.log('completed');
      })
      .subscribe((result: any) => {
        this.paging();
        this.result = result;
        this.getAllData();

      });
  }
  getAllData(): void {
    let i;
    for (i = 0; i < this.result.length; i++) {
      this.data.push(this.result[i].name);
      this.data.push(this.result[i].itemType.name);
      this.data.push(this.result[i].category.name);
      this.data.push(this.result[i].subCategory.name);
   
     
      this.data.push(this.result[i].id.toString());


     

      this.dataTable.dataRows.push(this.data);
      this.data = [];
    }
  }
  paging(): void {
    this.dataTable = {
      headerRow: [ 'Item', 'Item Type','Category',  'Sub Category' ],
        footerRow: [ 'Item', 'Item Type','Category',  'Sub Category' ],
        dataRows: [
        
      ]

  };
}
ngAfterViewInit() {
  $('#datatabless').DataTable({
    "pagingType": "full_numbers",
    "lengthMenu": [
      [10, 25, 50, -1],
      [10, 25, 50, "All"]
    ],
    responsive: true,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search records",
    }

  });

  const table = $('#datatabless').DataTable();

  $('.card .material-datatables label').addClass('form-group');
}

  show(){
    this.active = true;
    this.modal.show();

  }
  close(): void {
    this.active = false;
    this.modal.hide();
}
//smart Search
@Input()


debugger;
//smart search 
Data(ItemId : string) : void{
  debugger


this.tempArr.push(parseInt(ItemId))


}


Submit() : void{
debugger  
  this.active =  true
  this.modalSave.emit(this.tempArr);
  this.tempArr = this.Arr

  this.close()
}




}