import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemGroupComponent } from './search-item-group.component';

describe('SearchItemGroupComponent', () => {
  let component: SearchItemGroupComponent;
  let fixture: ComponentFixture<SearchItemGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
