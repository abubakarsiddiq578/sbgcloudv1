import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { GroupComponent } from '../group.component';

import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';


@Component({
  selector: 'edit-group',
  templateUrl: './edit-group.component.html'
})
export class EditGroupComponent implements OnInit {

    @ViewChild('editGroupModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    editData: any;


    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       
    ) {
        //super(injector);
    }
  

    show(data: any): void{
        this.editData = data;
        this.active = true;
        this.modal.show();

    }



    ngOnInit(): void {
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(){
        this.close();
        this.modalSave.emit(this.editData);
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}