// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector, Inject } from '@angular/core';

import { EditGroupComponent } from '../group/edit-group/edit-group.component';
import { FormControl } from '@angular/forms';
import { WorkGroupItemDetailServiceProxy, WorkGroupEmployeeDetailServiceProxy, WorkGroupMasterServiceProxy, EmployeeServiceProxy, WorkGroupMasterDto, WorkGroupEmployeeDetailDto, WorkGroupItemDetailDto, EmployeeDto, CategoryDto, SubCategoryDto, ItemDto, ItemServiceProxy, CategoryServiceProxy, SubCategoryServiceProxy, ItemTypeServiceProxy, ItemTypeDto } from '@app/shared/service-proxies/service-proxies';
import { WebStorageService, LOCAL_STORAGE } from '../../../../../node_modules/angular-webstorage-service';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { SearchEmployeeComponent } from '../promotion/search-employee/search-employee.component';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'group',
  templateUrl: 'group.component.html',
  providers: [WorkGroupMasterServiceProxy, WorkGroupItemDetailServiceProxy, WorkGroupEmployeeDetailServiceProxy, EmployeeServiceProxy, ItemServiceProxy, CategoryServiceProxy, SubCategoryServiceProxy, ItemTypeServiceProxy]
})

export class GroupComponent implements OnInit, AfterViewInit {

  @ViewChild('editGroupModal') editGroupModal: EditGroupComponent;
  @ViewChild('i') itemDD: any;
  @ViewChild('itemtypeId') itemtypeId: any;
 @ViewChild('categoryId') categoryId: any;
 @ViewChild('subCategoryId') subCategoryId: any;


 @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;

  public dataTable: DataTable;
  public dataTableHistory: DataTable;

  data: string[] = [];

  isDetail: boolean = true;
  isHistory: boolean = false;
  isSaveBtn: boolean = true;
  isEditBtn: boolean = false;


  itemTypeId : number
CategoryId : number
SubCategoryId : number


  employeeDetailId: number = 0
  employeeDetailRecordId: number = 0
  employeeDetailRecordLength: number = 0

  itemDetailRecordId: number = 0
  itemDetailRecordLength: number = 0

  WorkGroupMaster: WorkGroupMasterDto = new WorkGroupMasterDto
  WorkGroupEmployeeDetail: WorkGroupEmployeeDetailDto = new WorkGroupEmployeeDetailDto
  WorkGroupItemDetail: WorkGroupItemDetailDto = new WorkGroupItemDetailDto

  workGroupEmployeeDetailList = [];
  workGroupItemDetailList = [];

  WorkGroupMasterDto: WorkGroupMasterDto[]

  Employee: EmployeeDto[] = [];
  Category: CategoryDto
  SubCategory: SubCategoryDto
  Item: ItemDto[]= []
  ItemType: ItemTypeDto


  gf:GlobalFunctions = new GlobalFunctions

  EmpName : string;


  constructor(
    @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private _WorkGroupMasterService: WorkGroupMasterServiceProxy,
    private _EmployeeService: EmployeeServiceProxy,
    private _ItemService: ItemServiceProxy,
    private _CategoryService: CategoryServiceProxy,
    private _SubCategoryService: SubCategoryServiceProxy,
    private _ItemTypeService: ItemTypeServiceProxy,

  ) { }


  addSearchDD(){
    this.searchEmployeeDropdownModel.show()
  }

  EmployeeId(id): void {
    debugger
  
    for(let i = 0; i< this.Employee.length; i++)
    {
      if(this.Employee[i].id == id)
      {
        this.EmpName = this.Employee[i].employee_Name;
        this.WorkGroupEmployeeDetail.employeeId = this.Employee[i].id;
        this.employeeIndexChanged(this.Employee[i].id);
      }
    }
  
  
  }
  
  
  displayItem = (emp: EmployeeDto): any => {
    debugger;
  
    if (emp instanceof EmployeeDto) {
      this.WorkGroupEmployeeDetail.employeeId = emp.id;
      return emp.employee_Name;
    }
    return emp;
  
  }




  initDetailDataTable() {
    this.dataTable = {
      headerRow: ['Employee Name', 'Department', 'Designation', 'Ratio', 'Action'],
      footerRow: ['Employee Name', 'Department', 'Designation', 'Ratio', 'Action'],
      dataRows: []
    };
  }

  initMasterDataTable() {
    this.dataTableHistory = {
      headerRow: ['Group Name', 'Group Type', 'Category', 'Sub category', 'Action'],
      footerRow: ['Group Name', 'Group Type', 'Category', 'Sub category', 'Action'],
      dataRows: [],
    };
  }

  fillMasterDataTable() {
    let i = 0;
    for (i = 0; i < this.WorkGroupMasterDto.length; i++) {
      this.data.push(this.WorkGroupMasterDto[i].groupName);
      this.data.push(this.WorkGroupMasterDto[i].itemType.name.toString());
      this.data.push(this.WorkGroupMasterDto[i].category.name.toString());
      this.data.push(this.WorkGroupMasterDto[i].subCategory.name.toString());
      this.data.push(this.WorkGroupMasterDto[i].id.toString());

      this.dataTableHistory.dataRows.push(this.data);

      this.data = [];
    }
  }

  fillDetailDataTable() {
    try {
      this.data.push(this.WorkGroupEmployeeDetail.employee.employee_Name)
      this.data.push(this.WorkGroupEmployeeDetail.employee.employeeDesignation.employeeDesignationName.toString())
      this.data.push(this.WorkGroupEmployeeDetail.employee.department.title.toString())
      this.data.push(this.WorkGroupEmployeeDetail.ratio.toString())
      this.data.push(this.WorkGroupEmployeeDetail.id.toString())

      this.dataTable.dataRows.push(this.data)
      this.data = [];
    }
    catch{

    }

  }

  // delete data of salary group detail record from detail datatable 
  DeleteWorkGroupEmployeeDetail(id: string) {

    debugger
    let workGroupDetail;

    // get record of matching id
    workGroupDetail = this.workGroupEmployeeDetailList.find(function (obj) { return obj.id === parseInt(id); });

    // perform delete operation to delete object from detail list 
    const index: number = this.workGroupEmployeeDetailList.indexOf(workGroupDetail);
    if (index !== -1) {
      this.workGroupEmployeeDetailList.splice(index, 1);

      this.initDetailDataTable();
      this.fillEditDetailDataTable()
    }
  }

  deleteWorkGroupMasterDetail(id: string) {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Work Group!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this._WorkGroupMasterService.delete(parseInt(id))
          .finally(() => {

            this.getAllWorkGroup();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Salary Group has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Salary Group is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

  itemIndexChanged(Ids: number[]) {

    console.log(Ids)
    debugger
    Ids.forEach(element => {
      this.WorkGroupItemDetail.itemId = Ids[element]
      this.workGroupItemDetailList.push(this.WorkGroupItemDetail)
      this.WorkGroupItemDetail = new WorkGroupItemDetailDto();
    });
    // this.WorkGroupItemDetail.itemId = id
    // this.workGroupItemDetailList.push(this.WorkGroupItemDetail)
    //  //icrement detailRecordId to identify each entry
    //  this.itemDetailRecordId++;

    //  // set value of salaryGroupDetail.id to detailRecordId
    //  this.WorkGroupEmployeeDetail.id = this.itemDetailRecordId;
    // this.WorkGroupItemDetail = new WorkGroupItemDetailDto();
  }

  employeeIndexChanged(id: number) {

    debugger
    // if (this.storage.get('TenantId') > 0) {
    //   this.WorkGroupEmployeeDetail.tenantId = this.storage.get('TenantId');
    // }
    let workGroupEmployeeDetail;
    workGroupEmployeeDetail = this.workGroupEmployeeDetailList.find(function (obj) { return obj.employeeId === id; });
    
    if (workGroupEmployeeDetail != undefined) {
      this.gf.showErrorMessage("Employee Already Exist", "top", "right")
      return
    }
    this._WorkGroupMasterService.getEmployeeDetailsById(id)
      .subscribe((result) => {
        debugger
        //this.initDetailDataTable()
        this.WorkGroupEmployeeDetail.employee = result[0]

        this.WorkGroupEmployeeDetail.employeeId = id
        // fill datatable to show detail record in datatable grid
        this.WorkGroupEmployeeDetail.ratio = 0


        this.employeeDetailRecordId++;

        // set value of salaryGroupDetail.id to detailRecordId
        this.WorkGroupEmployeeDetail.id = this.employeeDetailRecordId;

        this.fillDetailDataTable();

        this.workGroupEmployeeDetailList.push(this.WorkGroupEmployeeDetail);

        // create new object of salaryGroupDetail to add another value in the grid
        this.WorkGroupEmployeeDetail = new WorkGroupEmployeeDetailDto();
      })

    //icrement detailRecordId to identify each entry


    // Set employee Id 

    // // push salaryGroupDetail in salaryGroupDetailList


  }


  getAllWorkGroup() {
    debugger
    this._WorkGroupMasterService.getAllWorkGroup()
      .subscribe((result) => {
        this.initMasterDataTable();
        this.WorkGroupMasterDto = result
        this.fillMasterDataTable();
      });
  }

  ngOnInit() {
    debugger

    this._EmployeeService.getAllEmployees()
      .finally(() => { console.log('completed') })
      .subscribe((result: any) => {
        this.Employee = result;

      })
    this._ItemService.getAllItems()
      .finally(() => { console.log('completed') })
      .subscribe((result: any) => {
        this.Item = result;

      })
    this._CategoryService.getAllCategories()
      .finally(() => { console.log('completed') })
      .subscribe((result: any) => {
        this.Category = result;

      })
    this._SubCategoryService.getAllSubCategories()
      .finally(() => { console.log('completed') })
      .subscribe((result: any) => {
        this.SubCategory = result;

      })
    this._ItemTypeService.getAllItemTypes()
      .finally(() => { console.log('completed') })
      .subscribe((result: any) => {
        this.ItemType = result;

      })

    this.initDetailDataTable()
    this.getAllWorkGroup()
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  EditEmployeeDetail(id: string): void {
    debugger
    let workGroupEmployeeDetail;

    workGroupEmployeeDetail = this.workGroupEmployeeDetailList.find(function (obj) { return obj.id === parseInt(id); });

    this.editGroupModal.show(workGroupEmployeeDetail);
  }

  EditMasterDetail(id: string) {
    debugger

    this.employeeDetailId = parseInt(id)

    this._WorkGroupMasterService.getAllWorkGroupById(parseInt(id))
      .subscribe((result) => {
        debugger;
        this.WorkGroupMaster.groupName = result[0].groupName
        this.WorkGroupMaster.categoryId = result[0].categoryId
        this.WorkGroupMaster.itemTypeId = result[0].itemTypeId
        this.WorkGroupMaster.subCategoryId = result[0].subCategoryId
        // this.WorkGroupMaster.workGroupEmployeeDetails = result[0].workGroupEmployeeDetails
        // this.WorkGroupMaster.workGroupItemDetails = result[0].workGroupItemDetails
        console.log("this is master: ", result[0].groupName);
      })

    this._WorkGroupMasterService.getAllEmployeeDetailById(parseInt(id))
      .subscribe((result) => {
        this.WorkGroupEmployeeDetail.employeeId = result[0].employeeId
        this.workGroupEmployeeDetailList = [];
        this.workGroupEmployeeDetailList = result;
        this.employeeDetailRecordLength = this.workGroupEmployeeDetailList.length;
        this.initDetailDataTable();
        this.fillEditDetailDataTable();
        this.ActiveUpdateBtn();
      });

    this._WorkGroupMasterService.getAllItemDetailById(parseInt(id))
      .subscribe((result) => {

        this.workGroupItemDetailList = []
        this.workGroupItemDetailList = result
        this.itemDetailRecordLength = this.workGroupItemDetailList.length;
        debugger
        let tempValue = []
        for (let i = 0; i < this.itemDetailRecordLength; i++) {

          tempValue.push(result[i].itemId)

        }
        this.itemDD.value = tempValue
      })

    this.ActiveDetail();
  }

  fillEditDetailDataTable() {
    let i;
    debugger
    for (i = 0; i < this.workGroupEmployeeDetailList.length; i++) {
      this.data.push(this.workGroupEmployeeDetailList[i].employee.employee_Name);
      this.data.push(this.workGroupEmployeeDetailList[i].employee.employeeDesignation.employeeDesignationName);
      this.data.push(this.workGroupEmployeeDetailList[i].employee.department.title);
      this.data.push(this.workGroupEmployeeDetailList[i].ratio.toString());
      this.data.push(this.workGroupEmployeeDetailList[i].id.toString());
      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }

  updateWorkGroupEmployeeDetail() {
    this.initDetailDataTable();
    this.fillEditDetailDataTable();
  }

  // perform edit operation to edit data 
  edit(): void {

    debugger
    if (this.storage.get('TenantId') > 0) {
      this.WorkGroupMaster.tenantId = this.storage.get('TenantId');
    }
    // this._WorkGroupMasterService.deleteItem(this.employeeDetailId)
    // .subscribe((result) => {
    //   console.log("Item Deleted successfully")
    // })

    this.workGroupItemDetailList = []
    for (let i = 0; i < this.itemDD.value.length; i++) {
      this.WorkGroupItemDetail.itemId = this.itemDD.value[i]
      this.workGroupItemDetailList.push(this.WorkGroupItemDetail)
      // this.itemDetailRecordId++;
      // this.WorkGroupItemDetail.id = this.itemDetailRecordId;
      this.WorkGroupItemDetail = new WorkGroupItemDetailDto();
    }


    this.WorkGroupMaster.id = this.employeeDetailId;

    for (let i = this.employeeDetailRecordLength; i < this.workGroupEmployeeDetailList.length; i++) {
      this.workGroupEmployeeDetailList[i].id = undefined;
    }

    for (let i = 0; i < this.workGroupEmployeeDetailList.length; i++) {
      this.workGroupEmployeeDetailList[i].employee = undefined
    }

    for (let i = this.itemDetailRecordLength; i < this.workGroupItemDetailList.length; i++) {
      this.workGroupItemDetailList[i].id = undefined;
    }

    this.WorkGroupMaster.workGroupEmployeeDetails = this.workGroupEmployeeDetailList;
    this.WorkGroupMaster.workGroupItemDetails = this.workGroupItemDetailList;

    this._WorkGroupMasterService.edit(this.WorkGroupMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Updated Successfully');

        this.initDetailDataTable();
        this.getAllWorkGroup();

        this.RefreshControls()
      });
  }


  save(ids: number[]): void {
    debugger
    // set tenant id that get from session  
    if (this.storage.get('TenantId') > 0) {
      this.WorkGroupMaster.tenantId = this.storage.get('TenantId');
    }

    for (let i = 0; i < ids.length; i++) {
      this.WorkGroupItemDetail.itemId = ids[i]
      this.workGroupItemDetailList.push(this.WorkGroupItemDetail)
      this.itemDetailRecordId++;
      this.WorkGroupItemDetail.id = this.itemDetailRecordId;
      this.WorkGroupItemDetail = new WorkGroupItemDetailDto();
    }
    // set master id undefined to save record
    this.WorkGroupMaster.id = undefined;

    // set detail id undefined to save record
    for (let i = 0; i < this.workGroupEmployeeDetailList.length; i++) {
      this.workGroupEmployeeDetailList[i].id = undefined;
      this.workGroupEmployeeDetailList[i].employee = undefined;
    }
    for (let i = 0; i < this.workGroupItemDetailList.length; i++) {
      this.workGroupItemDetailList[i].id = undefined;
    }

    this.WorkGroupMaster.workGroupEmployeeDetails = this.workGroupEmployeeDetailList;
    this.WorkGroupMaster.workGroupItemDetails = this.workGroupItemDetailList;

    this._WorkGroupMasterService.add(this.WorkGroupMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Saved Successfully');

        // perform reset operation after saving record

        this.initDetailDataTable();
        this.getAllWorkGroup();

        this.RefreshControls()
      });
  }
  ActiveDetail() {
    this.isDetail = true;
    this.isHistory = false;
  }

  // show history tab 
  ActiveHistory() {
    this.isDetail = false;
    this.isHistory = true;
  }
  //Refresh Controls
  RefreshControls() {
    this.WorkGroupMaster.groupName = undefined;
    this.WorkGroupMaster.itemTypeId = undefined;
    this.WorkGroupMaster.subCategoryId = undefined;
    this.WorkGroupMaster.categoryId = undefined;
    this.WorkGroupItemDetail.itemId = undefined;
    this.WorkGroupEmployeeDetail.employeeId = undefined;

    this.itemDD.value = []

    this.workGroupEmployeeDetailList = [];
        this.workGroupItemDetailList = [];


        this.employeeDetailRecordId = 0;
        this.itemDetailRecordId = 0;


        this.employeeDetailRecordLength = 0;
        this.itemDetailRecordLength = 0;

        this.initDetailDataTable()
        this.ActiveSaveBtn();

  }

  // show save btn
  ActiveSaveBtn() {
    this.isSaveBtn = true;
    this.isEditBtn = false;
  }

  // show update btn
  ActiveUpdateBtn() {
    this.isSaveBtn = false;
    this.isEditBtn = true;
  }

  notifyMsg(title, msg) {
    swal({
      title: title,
      text: msg,
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }


  FilterData() {
    debugger
    
    this._ItemService.filterData(this.itemTypeId , this.CategoryId , this.SubCategoryId)
      .subscribe((result) => {
        this.Item = result;
 
      });
  }
 
 
  onitemTypeChange(event, id: number) : void {
    debugger;
        this.itemTypeId = event.value;
        this.FilterData()
  }
 
  onCategoryChange(event, id: number): void{
    debugger;
    this.CategoryId = event.value;
    this.FilterData()
  }
 
 
  onSubCategoryChange(event, id: number): void{
    debugger;
    this.SubCategoryId = event.value;
    this.FilterData()
  }

}
