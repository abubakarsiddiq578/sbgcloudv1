import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';

@Component({
    selector: 'edit-salary-group',
    templateUrl: './edit-salary-group.component.html'
})

export class EditSalaryGroupComponent /*extends AppComponentBase*/ implements OnInit {
    @ViewChild('editSalaryGroupModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    // public dataTable : DataTable ;

    active: boolean = false;
    saving: boolean = false;

    editData; any;


    constructor(
        injector: Injector
        // private _userService: UserServiceProxy

    ) {
        //super(injector);
    }


    show(data: any): void {
        this.editData = data;
        this.active = true;

        this.modal.show();

    }



    ngOnInit(): void {

    }



    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }


}


