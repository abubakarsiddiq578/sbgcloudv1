// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Inject } from '@angular/core';
import { SalaryGroupMasterServiceProxy, SalaryGroupMasterDto, SalaryGroupDetailServiceProxy, SalaryGroupDetailDto, SalaryExpenseTypeDto, SalaryExpenseTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
//import { EditSalaryGroupComponent } from "./edit-salary-group/edit-salary-group.component";
import swal from 'sweetalert2';
import { empty } from 'rxjs';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
  selector: 'salary-group',
  templateUrl: 'salary-group.component.html',
  providers: [SalaryGroupMasterServiceProxy, SalaryGroupDetailServiceProxy, SalaryExpenseTypeServiceProxy]
})

export class SalaryGroupComponent implements OnInit, AfterViewInit {
  public dataTable: DataTable;
  public dataTable1: DataTable;

  //@ViewChild('EditSalaryGroupModal') editSalaryGroupModal: EditSalaryGroupComponent;

  salaryGroupMaster: SalaryGroupMasterDto = new SalaryGroupMasterDto();

  salaryGroupDetail: SalaryGroupDetailDto = new SalaryGroupDetailDto();
  salaryGroupDetailList = [];

  data: string[] = [];

  salaryGroupMasterDto: SalaryGroupMasterDto[];

  isDetail: boolean = true;
  isHistory: boolean = false;

  isSaveBtn: boolean = true;
  isEditBtn: boolean = false;

  detailId: number = 0;

  detailRecordId: number = 0;

  detailRecordLength: number = 0;
  salaryType: SalaryExpenseTypeDto[] = [];

  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private salaryGroupMasterServiceProxy: SalaryGroupMasterServiceProxy
    ,private _router : Router,
    private salaryExpenseType: SalaryExpenseTypeServiceProxy
    ) {

  }

  globalFunction: GlobalFunctions = new GlobalFunctions

  ngOnInit() {
    if (!this.globalFunction.hasPermission("View", "SalaryGroup")) {
      this.globalFunction.showNoRightsMessage("View");
      this._router.navigate([''])
    }

    // Initialize boolean value of salaryGroupDetail object properties
    this.salaryGroupDetail.init({ isActive: true, dedeuction: false, exempted: false });

    // reset and initialize detailDataTable Grid
    this.initDetailDataTable();

    // get all salary group record in masterDataTable Grid
    this.getAllSalaryGroup();

    this.salaryExpenseType.getAllSalaryExpenseType()
    .subscribe((result) => {
      debugger
      this.salaryType = result
    })

  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      //alert('You clicked on Like button');
      e.preventDefault();
    });



    $('#datatables1').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table1 = $('#datatables1').DataTable();

    // Edit record
    table1.on('click', '.edit', function (e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table1.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      //table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table1.on('click', '.like', function (e) {
      //alert('You clicked on Like button');
      e.preventDefault();
    });



    $('.card .material-datatables label').addClass('form-group');
  }

  // add the values in grid against master salary group title name
  addToGrid(): void {
    if (this.storage.get('TenantId') > 0) {
      this.salaryGroupDetail.tenantId = this.storage.get('TenantId');
    } 

    // icrement detailRecordId to identify each entry
    this.detailRecordId++;

    // set value of salaryGroupDetail.id to detailRecordId
    this.salaryGroupDetail.id = this.detailRecordId;

    debugger
    // push salaryGroupDetail in salaryGroupDetailList
    this.salaryGroupDetailList.push(this.salaryGroupDetail);

    // fill datatable to show detail record in datatable grid
    this.fillDetailDataTable();

    // create new object of salaryGroupDetail to add another value in the grid
    this.salaryGroupDetail = new SalaryGroupDetailDto();
    this.salaryGroupDetail.init({ isActive: true, dedeuction: false, exempted: false });
  }

  // fill datatable to show detail record in datatable grid
  fillDetailDataTable() {

    try{
      debugger
      this.data.push(this.salaryGroupDetail.salaryType);
      this.data.push(this.salaryGroupDetail.applyValue);
      this.data.push(this.salaryGroupDetail.value.toString());
      this.data.push(this.salaryGroupDetail.dedeuction.toString());
      this.data.push(this.salaryGroupDetail.exempted.toString());
      this.data.push(this.salaryGroupDetail.sortOrder.toString());
      this.data.push(this.salaryGroupDetail.isActive.toString());
      this.data.push(this.salaryGroupDetail.id.toString());
  
      this.dataTable.dataRows.push(this.data)
  
      this.data = [];
    }
    catch{
      this.notifyMsg("Error" , "Please Provide all Required Fields");
    }
  }

  // fill datatable to show detail record in datatable grid in eidt mode
  fillEditDetailDataTable() {
    let i;

    for (i = 0; i < this.salaryGroupDetailList.length; i++) {
      this.data.push(this.salaryGroupDetailList[i].salaryType);
      this.data.push(this.salaryGroupDetailList[i].applyValue);
      this.data.push(this.salaryGroupDetailList[i].value.toString());
      this.data.push(this.salaryGroupDetailList[i].dedeuction.toString());
      this.data.push(this.salaryGroupDetailList[i].exempted.toString());
      this.data.push(this.salaryGroupDetailList[i].sortOrder.toString());
      this.data.push(this.salaryGroupDetailList[i].isActive.toString());
      this.data.push(this.salaryGroupDetailList[i].id.toString());

      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }
  }

  // reset and initialize detailDataTable Grid
  initDetailDataTable() {
    this.dataTable = {
      headerRow: ['Salary Type', 'Value Type', 'Value', 'Deduction', 'Exempted', 'Sort Order', 'Active'],
      footerRow: ['Salary Type', 'Value Type', 'Value', 'Deduction', 'Exempted', 'Sort Order', 'Active'],

      dataRows: [
        //['Basic Salary', 'Percentage', '70', 'No', 'No','1','Yes',''],
        //['Allowance', 'Fixed', '70', 'No', 'No','2','Yes','']
      ]
    };
  }

  // reset and initialize MasterDataTable Grid
  initMasterDataTable() {
    this.dataTable1 = {
      headerRow: ['Salary Group', 'Actions'],
      footerRow: ['Salary Group', 'Actions'],

      dataRows: [
        //['Support Department', ''],
        //['Development Development' ,  ''],
        // ['Casual Leaves', '10', '30/09/2018', 'Annualy casual leaves allowed', ''],
        // ['Special Leaves', '3', '30/09/2018', 'Annualy special leaves for specal occasions', '']
      ],
    };
  }

  // fill datatable to show master record in datatable grid
  fillMasterDataTable() {
    let i = 0;

    for (i = 0; i < this.salaryGroupMasterDto.length; i++) {
      this.data.push(this.salaryGroupMasterDto[i].salaryGroupTitle);
      this.data.push(this.salaryGroupMasterDto[i].id.toString());

      this.dataTable1.dataRows.push(this.data);

      this.data = [];
    }
  }

  // perform save opertaion to save data
  save(): void {

    // set tenant id that get from session  
    if (this.storage.get('TenantId') > 0) {
      this.salaryGroupMaster.tenantId = this.storage.get('TenantId');
    } 

    // set master id undefined to save record
    this.salaryGroupMaster.id = undefined;

    // set detail id undefined to save record
    for (let i = 0; i < this.salaryGroupDetailList.length; i++) {
      this.salaryGroupDetailList[i].id = undefined;
    }

    this.salaryGroupMaster.salaryGroupDetails = this.salaryGroupDetailList;

    this.salaryGroupMasterServiceProxy.add(this.salaryGroupMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Saved Successfully');

        // perform reset operation after saving record

        this.initDetailDataTable();
        this.getAllSalaryGroup();

        this.salaryGroupDetailList = [];

        this.salaryGroupMaster.salaryGroupTitle = "";

        this.detailRecordId = 0;

        this.detailRecordLength = 0;
      });
  }

  // get all salary group records
  getAllSalaryGroup(): void {
    this.salaryGroupMasterServiceProxy.getAllSalaryGroup()
      .subscribe((result) => {
        this.initMasterDataTable();
        this.salaryGroupMasterDto = result
        this.fillMasterDataTable();
      });
  }

  notifyMsg(title, msg) {
    swal({
      title: title,
      text: msg,
      timer: 2000,
      showConfirmButton: false
    }).catch(swal.noop)
  }

  // delete record of salary group master 
  deleteSalaryGroupMasterDetail(id: string) {
    if (!this.globalFunction.hasPermission("Delete", "SalaryGroup")) {
      this.globalFunction.showNoRightsMessage("Delete");
     return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Salary Group!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.salaryGroupMasterServiceProxy.delete(parseInt(id))
          .finally(() => {

            this.getAllSalaryGroup();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Salary Group has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Your Salary Group is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

  // get detail record of particular master in edit mode
  EditSalaryGroupMasterDetail(title: string, id: string) {
    if (!this.globalFunction.hasPermission("Edit", "SalaryGroup")) {
      this.globalFunction.showNoRightsMessage("Edit");
     return
    }
    this.salaryGroupMaster.salaryGroupTitle = title;
    this.detailId = parseInt(id);

    this.salaryGroupMasterServiceProxy.getAllDetailById(parseInt(id))
      .subscribe((result) => {
        this.salaryGroupDetailList = [];
        this.salaryGroupDetailList = result;
        this.detailRecordLength = this.salaryGroupDetailList.length;
        this.initDetailDataTable();
        this.fillEditDetailDataTable();
        this.ActiveUpdateBtn();
      });

    this.ActiveDetail();
  }

  // perform edit operation to edit data 
  edit(): void {
    if (!this.globalFunction.hasPermission("Edit", "SalaryGroup")) {
      this.globalFunction.showNoRightsMessage("Edit");
     return
    }
    if (this.storage.get('TenantId') > 0) {
      this.salaryGroupMaster.tenantId = this.storage.get('TenantId');
    }

    this.salaryGroupMaster.id = this.detailId;

    for (let i = this.detailRecordLength; i < this.salaryGroupDetailList.length; i++) {
      this.salaryGroupDetailList[i].id = undefined;
    }

    this.salaryGroupMaster.salaryGroupDetails = this.salaryGroupDetailList;

    this.salaryGroupMasterServiceProxy.edit(this.salaryGroupMaster)
      .finally(() => {
      })
      .subscribe(() => {
        this.notifyMsg('Success!', 'Updated Successfully');

        this.initDetailDataTable();
        this.getAllSalaryGroup();

        this.salaryGroupDetailList = [];

        this.salaryGroupMaster.salaryGroupTitle = ""

        this.detailRecordId = 0;

        this.detailRecordLength = 0;
      });
  }

  // delete data of salary group detail record from detail datatable 
  DeleteSalaryGroupDetail(id: string){
    if (!this.globalFunction.hasPermission("Delete", "SalaryGroup")) {
      this.globalFunction.showNoRightsMessage("Delete");
     return
    }
    let salaryGroupDetail;

    // get record of matching id
    salaryGroupDetail = this.salaryGroupDetailList.find(function (obj) { return obj.id === parseInt(id); });
  
    // perform delete operation to delete object from detail list 
    const index: number = this.salaryGroupDetailList.indexOf(salaryGroupDetail);
    if (index !== -1) {
        this.salaryGroupDetailList.splice(index, 1);

        this.initDetailDataTable();
        this.fillEditDetailDataTable();
    }
  }

  // open edit pop up from to edit of detail datatable record
  EditSalaryGroupDetail(id: string){
    if (!this.globalFunction.hasPermission("Edit", "SalaryGroup")) {
      this.globalFunction.showNoRightsMessage("Edit");
     return
    }
    let salaryGroupDetail;

    salaryGroupDetail = this.salaryGroupDetailList.find(function (obj) { return obj.id === parseInt(id); });
        
    //this.editSalaryGroupModal.show(salaryGroupDetail);
  }

  // incase when edit operation is apply to edit data of detail datatable record
  updateSalaryGroupDetail(salaryGroupDetail){
    //let _salaryGroupDetail;

    //_salaryGroupDetail = this.salaryGroupDetailList.find(function (obj) { return obj.id === salaryGroupDetail.id; });

    // _salaryGroupDetail.salaryType = salaryGroupDetail.salaryType;
    // _salaryGroupDetail.applyValue = salaryGroupDetail.applyValue;
    // _salaryGroupDetail.value = salaryGroupDetail.value;
    // _salaryGroupDetail.dedeuction = salaryGroupDetail.dedeuction;
    // _salaryGroupDetail.exempted = salaryGroupDetail.exempted;
    // _salaryGroupDetail.sortOrder = salaryGroupDetail.sortOrder;
    // _salaryGroupDetail.isActive = salaryGroupDetail.isActive;
  
    this.initDetailDataTable();
    this.fillEditDetailDataTable();
  }

  // show detail tab 
  ActiveDetail() {
    this.isDetail = true;
    this.isHistory = false;
  }

  // show history tab 
  ActiveHistory() {
    this.isDetail = false;
    this.isHistory = true;
  }

  // show save btn
  ActiveSaveBtn(){
    this.isSaveBtn = true;
    this.isEditBtn = false;
  }

  // show update btn
  ActiveUpdateBtn(){
    this.isSaveBtn = false;
    this.isEditBtn = true;
  }

  // reset value of master detail input and detail datatable grid record
  reset(){
    this.salaryGroupMaster.salaryGroupTitle = "";
    this.detailId = 0;

    this.salaryGroupDetail.salaryType = null;
    this.salaryGroupDetail.applyValue = null;
    this.salaryGroupDetail.value = null;
    this.salaryGroupDetail.dedeuction = false;
    this.salaryGroupDetail.exempted = false;
    this.salaryGroupDetail.sortOrder = null;
    this.salaryGroupDetail.isActive = true;

    this.salaryGroupDetailList = [];

    this.detailRecordId = 0;

    this.detailRecordLength = 0;

    this.initDetailDataTable();
    this.ActiveSaveBtn();
  }

}


