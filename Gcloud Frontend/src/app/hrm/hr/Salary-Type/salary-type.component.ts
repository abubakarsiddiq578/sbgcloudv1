// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddSalaryTypeComponent } from "./add-salary-type/add-salary-type.component";
import { EditSalaryTypeComponent } from "./edit-salary-type/edit-salary-type.component";
import { SalaryExpenseTypeDto, SalaryExpenseTypeServiceProxy } from 'app/shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
    selector: 'salary-type',
    templateUrl: 'salary-type.component.html',

    providers: [SalaryExpenseTypeServiceProxy]
})

export class SalaryTypeComponent implements OnInit, AfterViewInit {

    data: string[] = [];

    salaryExpenseType: SalaryExpenseTypeDto[];

    showData: boolean = false;
  
    public dataTable: DataTable;
    @ViewChild('AddSalaryExpenseTypeModal') public AddSalaryTypeModal: AddSalaryTypeComponent;
    @ViewChild('EditSalaryExpenseTypeModal') EditSalaryTypeModal: EditSalaryTypeComponent;

    constructor(private salaryExpenseTypeServiceProxy : SalaryExpenseTypeServiceProxy,private _router : Router) 
    {
      
    }

    globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "SalaryExpenseType")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
        this.getAllSalaryExpenseType();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
  
      });
  
      const table = $('#datatables').DataTable();
  
      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });
  
      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });
  
      //Like record
      table.on('click', '.like', function(e) {
        //alert('You clicked on Like button');
        e.preventDefault();
      });
  
      $('.card .material-datatables label').addClass('form-group');
    }

    AddSalaryType(): void {
      if (!this.globalFunction.hasPermission("Create", "SalaryExpenseType")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
        this.AddSalaryTypeModal.show();
    }

    EditSalaryType(id: string): void {
      if (!this.globalFunction.hasPermission("Edit", "SalaryExpenseType")) {
        this.globalFunction.showNoRightsMessage("Edit");
        return
      }
      this.EditSalaryTypeModal.show(parseInt(id));    
  }

  fillDataTable(){

    let i;
    for (i= 0; i < this.salaryExpenseType.length; i++) {

      this.data.push(this.salaryExpenseType[i].salaryExpType)
      this.data.push(this.salaryExpenseType[i].applyValue)
      this.data.push(this.salaryExpenseType[i].value.toString())
      this.data.push(this.salaryExpenseType[i].salaryDeduction.toString())
      this.data.push(this.salaryExpenseType[i].incomeTaxExempted.toString())
      this.data.push(this.salaryExpenseType[i].sortOrder.toString())
      this.data.push(this.salaryExpenseType[i].isActive.toString())
      this.data.push(this.salaryExpenseType[i].id.toString())

      this.dataTable.dataRows.push(this.data)

      this.data = [];
    }
  }

  initDataTable(){
    this.dataTable = {
      headerRow: [ 'Salary Type', 'Value Type', 'Value', 'Deduction', 'Exempted', 'Sort Order' , 'Active' , 'Actions' ],
      footerRow: [/* 'Salary Type', 'Value Type', 'Value', 'Deduction', 'Exempted', 'Sort Order' , 'Active' , 'Actions'  */ ],

      dataRows: []
   };
  }

  getAllSalaryExpenseType(): void{
    this.salaryExpenseTypeServiceProxy.getAllSalaryExpenseType()
    .subscribe((result) => { 
      this.initDataTable();
      this.salaryExpenseType = result
      this.fillDataTable();
    });
   }

  protected delete(id: string): void {
    if (!this.globalFunction.hasPermission("Delete", "SalaryType")) {
      this.globalFunction.showNoRightsMessage("Delete");
      return
    }
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Salary Expense Type!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
    if (result.value) {

      this.salaryExpenseTypeServiceProxy.delete(parseInt(id))
        .finally(() => {
             
          this.getAllSalaryExpenseType();
        })
        .subscribe(() => {
          swal({
            title: 'Deleted!',
            text: 'Your Salary Expense Type has been deleted.',
            type: 'success',
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
        }).catch(swal.noop)
         });
    } else {
      swal({
          title: 'Cancelled',
          text: 'Your Salary Expense Type is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
      }).catch(swal.noop)
    }
  })
  }
}
