import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { SalaryExpenseTypeDto, SalaryExpenseTypeServiceProxy } from '../../../../shared/service-proxies/service-proxies';
//import { AppComponentBase } from '@shared/app-component-base';
import swal from 'sweetalert2';

@Component({
  selector: 'add-salary-type',
  templateUrl: './add-salary-type.component.html'
})
export class AddSalaryTypeComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('AddSalaryExpenseTypeModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    salaryExpenseType: SalaryExpenseTypeDto = new SalaryExpenseTypeDto();

    applyValues = [
        {value: 'Fixed', viewValue: 'Fixed'},
        {value: 'Percentage', viewValue: 'Percentage'},
        {value: 'Variable', viewValue: 'Variable'},
      ];

    constructor(
        private salaryExpenseTypeServiceProxy: SalaryExpenseTypeServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    show(): void {
        this.active = true; 
        this.modal.show();
        this.salaryExpenseType = new SalaryExpenseTypeDto();
        this.salaryExpenseType.init({ isActive: true });
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

     
    save(): void {
        //TODO: Refactor this, don't use jQuery style code
      this.saving = true;
        this.salaryExpenseTypeServiceProxy.create(this.salaryExpenseType)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify();
                this.close();   
                this.modalSave.emit(null);
            });
    }
      


    close(): void {
        this.active = false;
        this.modal.hide();
    }

    notify(){
        swal({
            title: "Success!",
            text: "Saved Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}
