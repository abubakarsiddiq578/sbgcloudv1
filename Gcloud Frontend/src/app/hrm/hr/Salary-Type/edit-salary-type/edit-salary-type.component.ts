import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { SalaryExpenseTypeDto, SalaryExpenseTypeServiceProxy } from '../../../../shared/service-proxies/service-proxies';
//import { AppComponentBase } from '@shared/app-component-base';
//import { appModuleAnimation } from '@shared/animations/routerTransition';
import swal from 'sweetalert2';

@Component({
  selector: 'edit-salary-type',
  templateUrl: './edit-salary-type.component.html',
  //animations: [appModuleAnimation()],
  providers: [SalaryExpenseTypeServiceProxy]
})

export class EditSalaryTypeComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('EditSalaryExpenseTypeModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    salaryExpenseType: SalaryExpenseTypeDto = new SalaryExpenseTypeDto();

    applyValues = [
        {value: 'Fixed', viewValue: 'Fixed'},
        {value: 'Percentage', viewValue: 'Percentage'},
        {value: 'Variable', viewValue: 'Variable'},
    ];

    constructor(
        injector: Injector,
        //private _costCenterService: CostCenterServiceProxy,
        private salaryExpenseTypeServiceProxy: SalaryExpenseTypeServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    show(id:number): void {
        this.salaryExpenseTypeServiceProxy.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: SalaryExpenseTypeDto) => {
                this.salaryExpenseType = result;
            });
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        this.saving = true;
        this.salaryExpenseTypeServiceProxy.update(this.salaryExpenseType)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
      }
   

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    notify(){
        swal({
            title: "Success!",
            text: "Record Updated Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}
