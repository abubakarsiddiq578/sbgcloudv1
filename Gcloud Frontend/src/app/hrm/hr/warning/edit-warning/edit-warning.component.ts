import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';
import * as moment from 'moment';

// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { WarningComponent } from '../warning.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

import { WarningDto, WarningServiceProxy, EmployeeServiceProxy, EmployeeDto,DepartmentServiceProxy , DepartmentDto, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';

import swal from 'sweetalert2';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';

@Component({
  selector: 'edit-warning',
  templateUrl: './edit-warning.component.html',
  styleUrls: ['./edit-warning.component.less'],
  providers:[WarningServiceProxy,EmployeeServiceProxy,DepartmentServiceProxy, HRConfigurationServiceProxy]
})
export class EditWarningComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editWarningModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    documentDate: Date = new Date(); 
    EmpName : string;
    employee: EmployeeDto[] = [];
    department:DepartmentDto[] = [];
    Warning : WarningDto = new WarningDto();

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;
    
    public empName: string ;
    _warn : FormGroup;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        private WarningServiceProxy:WarningServiceProxy,
        private employeeService:EmployeeServiceProxy,

        private _departmentService: DepartmentServiceProxy,
        private departmentService:DepartmentServiceProxy,

        private formBuilder : FormBuilder,

        private _hrConfigService : HRConfigurationServiceProxy
        // private _userService: UserServiceProxy

    ) {
        //super(injector);
    }
  
    ////////////////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }

  EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.Warning.employeeId = this.employee[i].id;
      }
    }
  }
  addSearchDD(){
    this.searchEditEmployeeDropdownModel.show()
  }
 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Warning.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  
/////////////////////////////////////////////////////////////////////


    show(id: number): void {


       this.active = true;
        this.modal.show();
        this.Warning = new WarningDto();
        this.WarningServiceProxy.get(id)
         .finally(() => {
            this.active = true;
            this.modal.show();
        })
         .subscribe((result: WarningDto) => {
            
            this.Warning = result;
            this.employeeService.get(result.employeeId).subscribe((result)=>{
                this.empName = result.employee_Name ;
            })
        });
       
        this.employeeDropdownSearch();
        this._warn.markAsUntouched({onlySelf:true});
    }


    
    initValidation() {

        this._warn = this.formBuilder.group({
            //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            employeeId: [null, Validators.required],
            warningDate: [null, Validators.required],
            warningType: [null, Validators.required],
            warningBy: [null, Validators.required]
            
            //email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],

        });

    }


   /* onType() {
        debugger;
        if (this._warn.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._warn);

        }
    }*/

    onType() {
 
        if (this._warn.valid) {
        
          let a,p=0 ;
          for(a=0;a<this.employee.length;a++){
            if(this.Warning.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
          
            this.validateAllFormFields(this._warn);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._warn);
           
        }
      }
      

    validateAllFormFields(formGroup: FormGroup) {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    





    ngOnInit(): void {
        this.employeeService.getAllEmployees()
        .subscribe((result)=> {
            this.employee = result;
           console.log("this is employee: ", this.employee);

        })

        this.departmentService.getAllDepartments()
        .subscribe((result)=>{
        this.department = result;

        })

        this._departmentService.getAllDepartments()
            .subscribe((result)=> {
                this.department = result;
            })

            this.getAllHRConfiguration();
            
    }


    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {
    
            this.hrConfig = result[0];
    
    
            if (this.hrConfig.autoCode == true) {
    
                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }
    
        });
    
    }







    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    notify(){
        swal({
            title: "Success!",
            text: "Record Updated Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
   
    save(): void {


        this.Warning.documentDate = moment(this.documentDate).add(5,'hour');
        this.saving = true;
     
        this.WarningServiceProxy.update(this.Warning)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
    }




}