// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddWarningComponent } from './add-warning/add-warning.component';
import { EditWarningComponent } from './edit-warning/edit-warning.component';

import { WarningDto, WarningServiceProxy, DepartmentServiceProxy, EmployeeServiceProxy } from 'app/shared/service-proxies/service-proxies';

import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'warning',
    templateUrl: 'warning.component.html',
    providers:[WarningServiceProxy, DepartmentServiceProxy, EmployeeServiceProxy]
})

export class WarningComponent implements OnInit, AfterViewInit {
   
  
  @ViewChild('addWarningModal') addWarningModal: AddWarningComponent;
  @ViewChild('editWarningModal') editWarningModal: EditWarningComponent;

  
  
  public dataTable: DataTable;

    data:string[] = [];


    Warning:WarningDto[];

    constructor(private WarningServiceProxy: WarningServiceProxy ,private _router : Router) {
    }

    globalFunction: GlobalFunctions = new GlobalFunctions

    ngOnInit() {
      if(!this.globalFunction.hasPermission("View","Warning")){
        this.globalFunction.showNoRightsMessage("View")
        this._router.navigate(['']);
      } 
      this.getAllWarnings();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    //to initialize datatable
    initDataTable() {
      this.dataTable = {
          headerRow: [ 'Doc No', 'Doc Date', 'Warning Date', 'Employee', 'Warning Type', 'Warning By','Detail', 'Actions' ],
          footerRow: [/* 'Doc No', 'Doc Date', 'Warning Date', 'Employee', 'Warning Type', 'Warning By', 'Actions'*/ ],

          dataRows: []
       };

  }




  //filling data in datatable/
    fillDataTable() {

      let i;
      for (i = 0; i < this.Warning.length; i++) {
  
        this.data.push(this.Warning[i].documentNo)
        this.data.push(this.Warning[i].documentDate.format())
        this.data.push(this.Warning[i].warningDate.format())
        this.data.push(this.Warning[i].employee.employee_Name) 
        this.data.push(this.Warning[i].warningType)
        this.data.push(this.Warning[i].department.title)
        this.data.push(this.Warning[i].detail)
        this.data.push(this.Warning[i].id.toString())

        this.dataTable.dataRows.push(this.data)
  
        this.data = [];
  
      }
    }

    getAllWarnings(): void {
      

      this.WarningServiceProxy.getAllWarnings()
        .subscribe((result) => {

          this.initDataTable()
          this.Warning = result
          this.fillDataTable();
        });
    }
  



    AddWarning(): void {
      if(!this.globalFunction.hasPermission("Create","Warning")){
        this.globalFunction.showNoRightsMessage("Create")
        return
      }
      this.addWarningModal.show();
      }

    EditWarning(id: string): void {
      if(!this.globalFunction.hasPermission("Edit","Warning")){
        this.globalFunction.showNoRightsMessage("Edit")
        return
      }
        this.editWarningModal.show(parseInt(id));
       
      }

      protected delete(id: string): void {
        if(!this.globalFunction.hasPermission("Delete","Warning")){
          this.globalFunction.showNoRightsMessage("Delete")
          return
        }
        swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this Request!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it',
          confirmButtonClass: "btn btn-success",
          cancelButtonClass: "btn btn-danger",
          buttonsStyling: false
        }).then((result) => {
          if (result.value) {
            this.WarningServiceProxy.delete(parseInt(id))
              .finally(() => {
                this.getAllWarnings();
              })
              .subscribe(() => {
                swal({
                  title: 'Deleted!',
                  text: 'Your Termination has been deleted.',
                  type: 'success',
                  confirmButtonClass: "btn btn-success",
                  buttonsStyling: false
                }).catch(swal.noop)
              });
          } else {
            swal({
              title: 'Cancelled',
              text: 'Your Termination is safe :)',
              type: 'error',
              confirmButtonClass: "btn btn-info",
              buttonsStyling: false
            }).catch(swal.noop)
          }
        })
      }
}


