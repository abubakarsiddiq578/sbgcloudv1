import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';
import swal from 'sweetalert2';

// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { WarningComponent } from '../warning.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { WarningDto, WarningServiceProxy, EmployeeServiceProxy, EmployeeDto , DepartmentServiceProxy, DepartmentDto, HRConfigurationDto, HRConfigurationServiceProxy } from 'app/shared/service-proxies/service-proxies';

import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';


@Component({
  selector: 'add-warning',
  templateUrl: './add-warning.component.html',
  styleUrls: ['./add-warning.component.less'],
  providers: [WarningServiceProxy , EmployeeServiceProxy ,DepartmentServiceProxy , HRConfigurationServiceProxy] 
})
export class AddWarningComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addWarningModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    employee : EmployeeDto[] = [];
    Warning: WarningDto = new WarningDto();

    public empName: string; 
    _warning: WarningDto[];

    department: DepartmentDto[] = [];



    _abc: abc[] = [];

    _warn : FormGroup;

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDto[]>;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    EmpName: string;

    constructor(
        injector: Injector,
        private WarningServiceProxy : WarningServiceProxy,
        private employeeService : EmployeeServiceProxy,
        private _departmentService: DepartmentServiceProxy,
        private departmentService: DepartmentServiceProxy,
        private formBuilder : FormBuilder ,
        private _hrConfigService : HRConfigurationServiceProxy       

 
        // private _userService: UserServiceProxy
       
    ) {
        //super(injector);
    }
  
    EmployeeId(id): void {
        debugger
    
        for(let i = 0; i< this.employee.length; i++)
        {
          if(this.employee[i].id == id)
          {
            this.EmpName = this.employee[i].employee_Name;
    
            this.Warning.employeeId = this.employee[i].id;
          }
        }
      }

    addSearchDD(){
        this.searchEmployeeDropdownModel.show()
      }


    show(): void{

        this.active = true;
        this.modal.show();
        this.Warning = new WarningDto();

        this.WarningServiceProxy.getAllWarnings().subscribe((result )=>{


            this._warning = result;
            //this.getAutoDocNumber();
            if(this.hrConfig.autoCode == true){

                this.getAutoDocNumber();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }
         
         //   this.Warning.documentNo = this.Warning.documentNo;
        })

        this.empName = null;
        this.employeeDropdownSearch();;
        
        this._warn.markAsUntouched({onlySelf:true});
        
    }


    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];
        });

    }


    initValidation() {

        this._warn = this.formBuilder.group({
            //To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            employeeId: [null, Validators.required],
            warningDate: [null, Validators.required],
            warningType: [null, Validators.required],
            warningBy: [null, Validators.required]
            
            //email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],

        });

    }



    getAutoDocNumber():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._warning.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._warning[i].documentNo ; 
    
            this._abc.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._abc.length == 0  ){
          
            debugger;
            code = "1";
            this.Warning.documentNo = "00" + code ;
    
        }
    
        else{
    
            if(this._abc[this._abc.length - 1] != null ){
    
                    let x;
                    code = this._abc[this._abc.length-1].code ;
                    temp = code;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.Warning.documentNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.Warning.documentNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.Warning.documentNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.Warning.documentNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.Warning.documentNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.Warning.documentNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.Warning.documentNo = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.Warning.documentNo =  "0" + code;
                      }
                      else if(code){
      
                          this.Warning.documentNo =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.Warning.documentNo = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.Warning.documentNo =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.Warning.documentNo =  temp[0] ;
                      }
    
                    }
            
            }                     
        }
        this._abc = [];
    }
    


    ////////////////////////////////////////////////////

public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    ); 
   
  }

  private _filter(value: string): EmployeeDto[]{

    debugger;
    const filterValue = value.toLowerCase();

    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }

  

 displayEmployee = (emp: EmployeeDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDto)
    {
        this.Warning.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    
    return emp;
  
  }
  
/////////////////////////////////////////////////////////////////////
     
    save():void {
 
        this.saving = true;
        this.WarningServiceProxy.create(this.Warning)
        .finally(() => { this.saving = false; })
              .subscribe(() => {
                  this.notify();
                  this.close();   
                  this.modalSave.emit(null);
              });

              this._warn.markAsUntouched({onlySelf:true});
    }



    ngOnInit(): void {
        
        
        this.employeeService.getAllEmployees()
        .subscribe((result)=> {
 
            this.employee = result;

        
        })

        this.departmentService.getAllDepartments()
        .subscribe((result)=>{
        this.department = result;

        })

        this._departmentService.getAllDepartments()
            .subscribe((result)=> {
                this.department = result;
            })

            this.initValidation();
            this.getAllHRConfiguration();
    }

   /* onType() {
        debugger;
        if (this._warn.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._warn);

        }
    }*/

    onType() {
 
        if (this._warn.valid) {
        
          let a,p=0 ;
          for(a=0;a<this.employee.length;a++){
            if(this.Warning.employeeId == this.employee[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
          
            this.validateAllFormFields(this._warn);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._warn);
           
        }
      }
      

    validateAllFormFields(formGroup: FormGroup) {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }


    notify(){
        swal({
            title: "Success!",
            text: "Saved Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    
}

// class for keeping all docNo number
class abc{
    public code: string;
  }
