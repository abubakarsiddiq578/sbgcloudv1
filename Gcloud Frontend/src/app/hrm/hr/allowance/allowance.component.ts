// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddAllowanceComponent } from './add-allowance/add-allowance.component';
import { EditAllowanceComponent } from './edit-allowance/edit-allowance.component';
import { AllowanceServiceProxy , AllowanceDto } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { AppComponentBase } from '@app/shared/app-component-base';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  //footerRow: string[];
  dataRows: string[][];
}

@Component({
    selector: 'allowance',
    templateUrl: 'allowance.component.html',
    providers: [AllowanceServiceProxy] 
})

export class AllowanceComponent extends AppComponentBase implements OnInit, AfterViewInit {
    public dataTable: DataTable;

    @ViewChild('addAllowanceModal') addAllowanceModal: AddAllowanceComponent;
    @ViewChild('editAllowanceModal') editAllowanceModal: EditAllowanceComponent;

    data: string[] = [];

    allowance: AllowanceDto[];

    constructor(private injector: Injector , 
      private allowanceServiceProxy: AllowanceServiceProxy , private _router: Router
  ) {
     super(injector)
  }
  globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "Allowance")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate(['']);
        
      }
         this.getAllAlowances();
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        //alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        //table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        //alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    AddAllowance(): void {
      if (!this.globalFunction.hasPermission("Create", "Allowance")) {
        this.globalFunction.showNoRightsMessage("Create")
        return
      }
      this.addAllowanceModal.show();
    
  }
    EditAllowance(id: string): void {
      if (!this.globalFunction.hasPermission("Edit", "Allowance")) {
        this.globalFunction.showNoRightsMessage("Edit")
        return
      }
        this.editAllowanceModal.show(parseInt(id));
    }

    getAllAlowances(): void{
      this.allowanceServiceProxy.getAllAllowances()
      .subscribe((result) => {
        this.initDataTable()
        this.allowance = result
        this.fillDataTable();   
      });
    }
    
    initDataTable(): void{

      this.dataTable = {
        headerRow: [ 'Doc No', 'Doc Date', 'Allowance Date', 'Employee', 'Allowance Type', 'Amount', 'Detail' , 'Actions' ],
        //footerRow: [ 'Doc No', 'Doc Date', 'Allowance Date', 'Employee', 'Allowance Type', 'Amount', 'Actions' ],

        dataRows: [
            //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Medical','8000', ''],
            //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Medical','8000', ''],
            //['A01', '11/08/2018', '11/08/2018', 'Nusrat', 'Medical','8000', ''],
        ]
     };
    }

    fillDataTable(){

      let i;
      for (i= 0; i < this.allowance.length; i++) {
  
        this.data.push(this.allowance[i].docNo)
        this.data.push( this.allowance[i].docDate.toDate().toDateString())
        this.data.push(this.allowance[i].allowanceDate.toDate().toDateString())
        this.data.push(this.allowance[i].employee.employee_Name)
        this.data.push(this.allowance[i].allowanceType.allowanceTitle)
        this.data.push(this.allowance[i].amount.toString())
        this.data.push(this.allowance[i].detail)
        this.data.push(this.allowance[i].id.toString())
  
        this.dataTable.dataRows.push(this.data)
  
        this.data = [];
      }
    }

    protected delete(id: string): void {

      if (!this.globalFunction.hasPermission("Delete", "Allowance")) {
        this.globalFunction.showNoRightsMessage("Delete")
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Allowance!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
      if (result.value) {
        this.allowanceServiceProxy.delete(parseInt(id))
          .finally(() => {
               
            this.getAllAlowances();
          })
          .subscribe(() => {
            swal({
              title: 'Deleted!',
              text: 'Your Allowance has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
          }).catch(swal.noop)
           });
      } else {
        swal({
            title: 'Cancelled',
            text: 'Your Allowance is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }
}


