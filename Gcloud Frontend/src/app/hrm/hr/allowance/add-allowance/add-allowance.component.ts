import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';
import { AllowanceServiceProxy , AllowanceDto , AllowanceTypeDropdownServiceProxy , AllowanceTypeDropdownDto , EmployeeDropdownDto, EmployeeDropDownServiceProxy , HRConfigurationDto , HRConfigurationServiceProxy, EmployeeServiceProxy, EmployeeDto} from '../../../../shared/service-proxies/service-proxies';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { AllowanceComponent } from '../allowance.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import swal from 'sweetalert2';
import * as moment from 'moment';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEmployeeComponent } from '../../promotion/search-employee/search-employee.component';


@Component({
  selector: 'add-allowance',
  templateUrl: './add-allowance.component.html',
  styleUrls : ['./add-allowance.component.less'],
  providers: [AllowanceTypeDropdownServiceProxy , EmployeeDropDownServiceProxy , AllowanceServiceProxy ,  HRConfigurationServiceProxy, EmployeeServiceProxy] 
})
export class AddAllowanceComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addAllowanceModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    public docDate = new Date();
    public allowanceDate = new Date();

    employee : EmployeeDto[] = [];
    public EmpName: string;
    employeeDropDown: EmployeeDropdownDto[] = null;  
    allowanceTypeDropDown: AllowanceTypeDropdownDto[] = null; 
    
    allowance: AllowanceDto = new AllowanceDto();

    _allowance: AllowanceDto[];

    _docNo : abc[] =[];

    _allowanceValidation: FormGroup

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDropdownDto[]>;

    public empName : string ;
    
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        private allowanceTypeDropdownServiceProxy: AllowanceTypeDropdownServiceProxy,
        private employeeDropDownServiceProxy: EmployeeDropDownServiceProxy,
        private allowanceServiceProxy: AllowanceServiceProxy,private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder,
        private employeeService : EmployeeServiceProxy
       
    ) {
        //super(injector);
    }


    
    ngOnInit(): void {
        this.fillEmployeeDropDown();
        this.fillAllowanceTypeDropDown();
        this.initValidation();
        this.getAllHRConfiguration();
        this.employeeService.getAllEmployees()
       .subscribe((result) => {
           this.employee = result;

       })
    }
    
  
///////////////////Start of Validation /////////////////////////
    
initValidation() {

    this._allowanceValidation = this.formBuilder.group({
        employeeId: [null, Validators.required],
        allowanceTypeId: [null , Validators.required]
    });

}

getAllHRConfiguration(){

    this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
    
      this.hrConfig = result[0] ;
    });
    
    }



/*onType() {

        if (this._allowanceValidation.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._allowanceValidation);
        }
      }*/
      
      onType() {
 
        if (this._allowanceValidation.valid) {
        
          let a,p=0 ;
          for(a=0;a<this.employeeDropDown.length;a++){
            if(this.allowance.employeeId == this.employeeDropDown[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
           
            this.validateAllFormFields(this._allowanceValidation);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._allowanceValidation);
           
        }
      }

      validateAllFormFields(formGroup: FormGroup) {
      
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
      }

///////////////////////////////////End of Validations///////////////////////////////////////////
  
///////////////////////////////////Search Employee///////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employee.slice())
    );
 
  }
 
  private _filter(value: string): EmployeeDto[]{
 
    debugger;
    const filterValue = value.toLowerCase();
 
    return this.employee.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
  }
 
 
  displayEmployee = (emp: EmployeeDto):any => {
    debugger;
 
    if(emp instanceof EmployeeDto)
    {
        this.allowance.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.allowance.employeeId = null;
    return emp;
 
  }
  

  EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.allowance.employeeId = this.employee[i].id;
      }
    }


  }
  //Item

  displayItem = (emp: EmployeeDto): any => {
    debugger;

    if (emp instanceof EmployeeDto) {
        this.allowance.employeeId = emp.id;
      return emp.employee_Name;
    }
    return emp;

  }
addSearchDD(){
    this.searchEmployeeDropdownModel.show()
  }

    show(): void{

        this.active = true;
        this.modal.show();

        this.allowance = new AllowanceDto();

        this.allowanceServiceProxy.getAllAllowances().subscribe((result)=>{

            this._allowance = result;
            //this.getAutoDocNumber();
            if(this.hrConfig.autoCode == true){

                this.getAutoDocNumber();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }
        })
        this.empName = null ; // setting empName to null before add popup 
        this.employeeDropdownSearch();
        this._allowanceValidation.markAsUntouched({onlySelf:true}); // mark as untouched
    }


    //this function generate auto doc number
    getAutoDocNumber():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._allowance.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._allowance[i].docNo ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0  ){
          
            debugger;
            code = "1";
            this.allowance.docNo = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    let x;
                    code = this._docNo[this._docNo.length-1].code ;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.allowance.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.allowance.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.allowance.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.allowance.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.allowance.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.allowance.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.allowance.docNo = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.allowance.docNo =  "0" + code;
                      }
                      else if(code){
      
                          this.allowance.docNo =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.allowance.docNo = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.allowance.docNo =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.allowance.docNo =  temp[0] ;
                      }
    
                    }
    
    
    
                    
            }                     
        }
        this._docNo = [];
    }
    
    

    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    fillEmployeeDropDown(){
        this.employeeDropDownServiceProxy.getEmployeeDropdown()
        .subscribe((result) => {
            this.employeeDropDown = result.items;
        });
    }

    fillAllowanceTypeDropDown(){
        this.allowanceTypeDropdownServiceProxy.getAllowanceTypeDropdown()
        .subscribe((result) => {
            this.allowanceTypeDropDown = result.items;
        });
    }

    doTextareaValueChange(ev){
        try {
          this.allowance.detail = ev.target.value;
        } catch(e) {
          console.info('could not set textarea-value');
        }
    }

    save(): void {
        this.allowance.docDate = moment(this.docDate).add(5,'hour');
        this.allowance.allowanceDate = moment(this.allowanceDate).add(5,'hour');

        this.saving = true;
        this.allowanceServiceProxy.create(this.allowance)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify();
                this.close();   
                this.modalSave.emit(null);
        });
    }

    notify(){
        swal({
            title: "Success!",
            text: "Saved Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }
}

// class for keeping all docNo number
class abc{
    public code: string;
  }
  