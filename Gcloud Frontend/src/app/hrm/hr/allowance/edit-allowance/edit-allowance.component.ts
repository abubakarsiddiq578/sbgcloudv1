import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { AllowanceComponent } from '../allowance.component';

import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { AllowanceServiceProxy , AllowanceDto , AllowanceTypeDropdownServiceProxy , AllowanceTypeDropdownDto , EmployeeDropdownDto, EmployeeDropDownServiceProxy, HRConfigurationDto , HRConfigurationServiceProxy, EmployeeDto, EmployeeServiceProxy} from '../../../../shared/service-proxies/service-proxies';
import * as moment from 'moment';
import swal from 'sweetalert2';
////////////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SearchEditEmployeeComponent } from '../../employee-list/search-edit-employee/search-edit-employee.component';

@Component({
  selector: 'edit-allowance',
  templateUrl: './edit-allowance.component.html',
  styleUrls : ['./edit-allowance.component.less'],
  providers: [AllowanceTypeDropdownServiceProxy ,EmployeeServiceProxy, EmployeeDropDownServiceProxy , AllowanceServiceProxy,  HRConfigurationServiceProxy] 
})
export class EditAllowanceComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editAllowanceModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('searchEditEmployeeDropdownModal') searchEditEmployeeDropdownModel: SearchEditEmployeeComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false; 

    public docDate = new Date();
    public allowanceDate = new Date();

    employeeDropDown: EmployeeDropdownDto[] = null;  
    allowanceTypeDropDown: AllowanceTypeDropdownDto[] = null; 
    
    allowance: AllowanceDto = new AllowanceDto();
    _allowanceValidation : FormGroup

    myControl = new FormControl();

    filteredOptions: Observable<EmployeeDropdownDto[]>;
    employee : EmployeeDto[] = [];
    EmpName : string;
    empName: string;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    constructor(
        injector: Injector,
        private allowanceTypeDropdownServiceProxy: AllowanceTypeDropdownServiceProxy,
        private employeeDropDownServiceProxy: EmployeeDropDownServiceProxy,
        private allowanceServiceProxy: AllowanceServiceProxy , private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder,
        private employeeService: EmployeeServiceProxy
       
    ) {
        //super(injector);
    }
  

    show(id: number): void{


        
      if(this.hrConfig.autoCode == true){

        this.isAutoCode = true ;
      }
      else if(this.hrConfig.autoCode == false){
        this.isAutoCode = false;
      }


        this.allowanceServiceProxy.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: AllowanceDto) => {
                this.allowance = result;

                this.docDate = this.allowance.docDate.toDate();
                this.allowanceDate = this.allowance.allowanceDate.toDate();

                this.employeeDropDownServiceProxy.get(result.employeeId).subscribe((result)=>{
                    this.empName = result.employee_Name;
                });
            });

            this.employeeDropdownSearch();
            this._allowanceValidation.markAsUntouched({onlySelf:true});
    }



    ngOnInit(): void {
        this.fillEmployeeDropDown();
        this.fillAllowanceTypeDropDown();
        this.initValidation();
        this.getAllHRConfiguration();
        this.employeeService.getAllEmployees()
        .subscribe((result) => {
            this.employee = result;
 
        })
    }

    
  initValidation() {

    this._allowanceValidation = this.formBuilder.group({
        employeeId: [null, Validators.required],
        allowanceTypeId: [null , Validators.required]
    });

}

EmployeeId(id): void {
    debugger

    for(let i = 0; i< this.employee.length; i++)
    {
      if(this.employee[i].id == id)
      {
        this.EmpName = this.employee[i].employee_Name;

        this.allowance.employeeId = this.employee[i].id;
      }
    }


  }
addSearchDD(){
    this.searchEditEmployeeDropdownModel.show()
  }
    /*onType() {

        if (this._allowanceValidation.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._allowanceValidation);
        }
      }*/
      
      onType() {
 
        if (this._allowanceValidation.valid) {
        
          let a,p=0 ;
          for(a=0;a<this.employeeDropDown.length;a++){
            if(this.allowance.employeeId == this.employeeDropDown[a].id) ///check if employee selected matched in list or not
            {
              p=1;
              this.save();
              break;
            }
          }
          if(p==0)
          {
            
            this.empName = null; // null empName 
           
            this.validateAllFormFields(this._allowanceValidation);
            this.employeeDropdownSearch();
          }
          
        } else {
           this.validateAllFormFields(this._allowanceValidation);
           
        }
      }

      validateAllFormFields(formGroup: FormGroup) {
      
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
      }

///////////////////////////////////End of Validations///////////////////////////////////////////

///////////////////////////////////Search Employee///////////////////////////////////////////
public employeeDropdownSearch(){
    debugger;
   this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | EmployeeDropdownDto >(''),
      map(value => typeof value === 'string' ? value : value.employee_Name),
      map(name => name ? this._filter(name) : this.employeeDropDown.slice())
    ); 
   
  }
  
  private _filter(value: string): EmployeeDropdownDto[]{
  
    debugger;
    const filterValue = value.toLowerCase();
  
    return this.employeeDropDown.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue)); 
  }
  
  
  displayEmployee = (emp: EmployeeDropdownDto):any => {
    debugger;
    
    if(emp instanceof EmployeeDropdownDto)
    {
        this.allowance.employeeId = emp.id ;
        return emp.employee_Name ;
    }
    this.allowance.employeeId = null;
    return emp;
  
  }
  /////////////////////////////////////End of Search//////////////////////////////////////////////////
  
  getAllHRConfiguration(){

    this._hrConfigService.getAllHRConfiguration().subscribe((result)=>{
    
      this.hrConfig = result[0] ;
    });
    
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    
    fillEmployeeDropDown(){
        this.employeeDropDownServiceProxy.getEmployeeDropdown()
        .subscribe((result) => {
            this.employeeDropDown = result.items;
        });
    }

    fillAllowanceTypeDropDown(){
        this.allowanceTypeDropdownServiceProxy.getAllowanceTypeDropdown()
        .subscribe((result) => {
            this.allowanceTypeDropDown = result.items;
        });
    }

    doTextareaValueChange(ev){
        try {
          this.allowance.detail = ev.target.value;
        } catch(e) {
          console.info('could not set textarea-value');
        }
    }

    notify(){
        swal({
            title: "Success!",
            text: "Record Updated Successfully.",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }

    save(): void {
        this.saving = true;
        this.allowance.docDate = moment(this.docDate).add(5,'hour');
        this.allowance.allowanceDate = moment(this.allowanceDate).add(5,'hour');

        this.allowanceServiceProxy.update(this.allowance)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            this.notify();
            this.close();
            this.modalSave.emit(null);
          });
    }
}

