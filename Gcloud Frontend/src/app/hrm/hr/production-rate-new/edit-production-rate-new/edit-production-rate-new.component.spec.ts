import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProductionRateNewComponent } from './edit-production-rate-new.component';

describe('EditProductionRateNewComponent', () => {
  let component: EditProductionRateNewComponent;
  let fixture: ComponentFixture<EditProductionRateNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProductionRateNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProductionRateNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
