import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Injector } from '@angular/core';
import { ModalDirective } from '../../../../../../node_modules/ngx-bootstrap';
import { ProductionRateDto, ItemDto, ProductionRateServiceProxy, ItemServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { FormGroup, FormBuilder, Validators, FormControl } from '../../../../../../node_modules/@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-edit-production-rate-new',
  templateUrl: './edit-production-rate-new.component.html',
  styleUrls: ['./edit-production-rate-new.component.scss'],
  providers: [ItemServiceProxy]
})
export class EditProductionRateNewComponent implements OnInit {
  @ViewChild('editProductionRateModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  productionRate: ProductionRateDto = new ProductionRateDto();

  item: ItemDto[]
  _productionRate : FormGroup;

  constructor(
    injector: Injector,
    private _productionRatService: ProductionRateServiceProxy,
    private _itemServiceProxy: ItemServiceProxy,
    private formBuilder: FormBuilder
  ) { }

  show(id:number): void{
    this._productionRatService.get(id)
      .finally(() => {
        this.active = true;
        this.modal.show();

      })
      .subscribe((result) => {
        this.productionRate = result;
      });
}

ngOnInit(): void {

  this.getAllItem();
  this.initValidation();

  }

onShown(): void {
    // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
}


initValidation() {

  this._productionRate = this.formBuilder.group({
      itemId: [null, Validators.required],
      rate: [null, Validators.required],
      repairRate: [null, Validators.required],
      
    });

}

onType() {

if (this._productionRate.valid) {
    this.save();
} else {
    this.validateAllFormFields(this._productionRate);
}
}

validateAllFormFields(formGroup: FormGroup) {

Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
    }
});
}


// get all ItemType

getAllItem()
{
  this._itemServiceProxy.getAllItems().subscribe((result)=>{

      this.item = result ;
  })
}


save(): void {

  this.saving = true;
  
  this._productionRatService.update(this.productionRate)
    .finally(() => { this.saving = false; })
    .subscribe(() => {
      this.notify();
      this.close();
      this.modalSave.emit(null);
    });
}

notify() {
  swal({
    title: "Success!",
    text: "Saved Successfully.",
    timer: 2000,
    showConfirmButton: false
  }).catch(swal.noop)
}

close(): void {
    this.active = false;
    this.modal.hide();
}


}
