import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProductionRateDto, ProductionRateServiceProxy, CategoryServiceProxy, SubCategoryServiceProxy, ItemTypeServiceProxy, ItemTypeDto, CategoryDto, SubCategoryDto, ItemDto, ItemServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { AddProductionRateNewComponent } from '@app/hrm/hr/production-rate-new/add-production-rate-new/add-production-rate-new.component';
import { EditProductionRateNewComponent } from '@app/hrm/hr/production-rate-new/edit-production-rate-new/edit-production-rate-new.component';
import swal from 'sweetalert2';
import * as moment from 'moment';
declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-production-rate-new',
  templateUrl: './production-rate-new.component.html',
  providers: [ProductionRateServiceProxy, ItemTypeServiceProxy, CategoryServiceProxy, SubCategoryServiceProxy, ItemServiceProxy]
})
export class ProductionRateNewComponent implements OnInit {

  @ViewChild('addProductionRateModal') addProductionRateModal: AddProductionRateNewComponent;
  @ViewChild('editProductionRateModal') editProductionRateModal: EditProductionRateNewComponent;
  @ViewChild('ItemTypeTemplate') ItemTypeTemplate: any;
  @ViewChild('CategoryTemplate') CategoryTemplate: any;
  @ViewChild('SubCategoryTemplate') SubCategoryTemplate: any;
  @ViewChild('ItemTemplate') ItemTemplate: any;
  @ViewChild('fromDate') fromDate: ElementRef;
  

  @ViewChild('toDate') toDate: ElementRef;
  public dataTable: DataTable;
  data: string[] = [];

  productionRate: ProductionRateDto[];
  itemType: ItemTypeDto[];
  item: ItemDto[];
  category: CategoryDto[];
  subCategory: SubCategoryDto[];

  constructor(
    private _productionRateService: ProductionRateServiceProxy,
    private _itemTypeService: ItemTypeServiceProxy,
    private _categoryService: CategoryServiceProxy,
    private _subCategoryService: SubCategoryServiceProxy,
    private _itemServiceProxy: ItemServiceProxy
  ) { }

  ngOnInit() {

    this._itemTypeService.getAllItemTypes()
      .subscribe((result) => {

        this.initializeDataTable()
        this.itemType = result;
        this.fillDataTable();
      })
    this._categoryService.getAllCategories()
      .subscribe((result) => {

        this.initializeDataTable()
        this.category = result;
        this.fillDataTable();
      })

    this._subCategoryService.getAllSubCategories()
      .subscribe((result) => {

        this.initializeDataTable()
        this.subCategory = result;
        this.fillDataTable();
      })

    this._itemServiceProxy.getAllItems()
      .subscribe((result) => {
        this.initializeDataTable()
        this.item = result;
        this.fillDataTable();
      })
    this.getAllProductionRate()
  }

  showFilteredProductionRate() {
    debugger
    let itemId: any = parseInt(this.ItemTypeTemplate.value)
    let categoryId: any = parseInt(this.CategoryTemplate.value)
    let subCategoryId: any = parseInt(this.SubCategoryTemplate.value)

    if (isNaN(parseInt(this.ItemTypeTemplate.value))) { itemId = 0 }
    if (isNaN(parseInt(this.CategoryTemplate.value))) { categoryId = 0 }
    if (isNaN(parseInt(this.SubCategoryTemplate.value))) { subCategoryId = 0 }


    this._productionRateService.getFilteredProductionRates(itemId, categoryId, subCategoryId)
      .subscribe((result) => {
        this.productionRate = result
        this.initializeDataTable()
        this.fillDataTable();
      })
  }

  AddProductionRate(): void {
    debugger
    this.addProductionRateModal.show();
  }

  editProductionRate(id: string): void {
    debugger;
    this.editProductionRateModal.show(parseInt(id));
  }

  initializeDataTable() {

    this.dataTable = {
      headerRow: ['Item', 'Rate', 'Repair Rate', 'Actions'],
      footerRow: ['Item', 'Rate', 'Repair Rate', 'Actions'],

      dataRows: []

    };

  }
  fillDataTable() {

    let i;
    for (i = 0; i < this.productionRate.length; i++) {


      this.data.push(this.productionRate[i].item.name)
      this.data.push(this.productionRate[i].rate.toString())
      this.data.push(this.productionRate[i].repairRate.toString())
      this.data.push(this.productionRate[i].id.toString())
      this.dataTable.dataRows.push(this.data)

      this.data = [];

    }

  }

  getAllProductionRate(): void {

    this._productionRateService.getAllProductionRates()
      .subscribe((result) => {

        this.initializeDataTable()
        this.productionRate = result;
        this.fillDataTable();
      })


  }
  protected delete(id: string): void {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Request!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this._productionRateService.delete(parseInt(id))
          .finally(() => {
            this.getAllProductionRate();
          })
          .subscribe(() => {
            debugger
            swal({
              title: 'Deleted!',
              text: 'Item has been deleted.',
              type: 'success',
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            }).catch(swal.noop)
          });
      } else {
        swal({
          title: 'Cancelled',
          text: 'Item Deletion is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
        }).catch(swal.noop)
      }
    })
  }

  IsAdvanceSearch: boolean = false

  advanceSearch() {

    if (this.IsAdvanceSearch) {
      this.IsAdvanceSearch = false
    } else {
      this.IsAdvanceSearch = true
    }

  }

  showAdvanceSearch(){
    debugger
    if (this.ItemTemplate.value = null) {
      this.ItemTemplate.value = 0
    }
    this._productionRateService.getAdvanceProductionRates(parseInt(this.ItemTemplate.value),moment(this.fromDate.nativeElement.value.toString()), moment(this.toDate.nativeElement.value.toString()) )
    .subscribe((result) => {
      this.productionRate = result
      this.initializeDataTable()
      this.fillDataTable()
    })
  }
}

