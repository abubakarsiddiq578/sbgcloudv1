import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Injector } from '@angular/core';
import { ProductionRateServiceProxy, ItemDto, ProductionRateDto, ItemServiceProxy } from '@app/shared/service-proxies/service-proxies';
import { ModalDirective } from '../../../../../../node_modules/ngx-bootstrap';
import { FormGroup, FormBuilder, Validators, FormControl } from '../../../../../../node_modules/@angular/forms';
import swal from 'sweetalert2';
import { SearchItemComponent } from '../search-item/search-item.component';
import * as moment from 'moment';

@Component({
  selector: 'app-add-production-rate-new',
  templateUrl: './add-production-rate-new.component.html',
  providers: [ProductionRateServiceProxy,ItemServiceProxy]
})
export class AddProductionRateNewComponent implements OnInit {

  @ViewChild('addProductionRateModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;
  @ViewChild('searchItemDropdownModal') searchItemDropdownModel: SearchItemComponent;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  productionRate: ProductionRateDto = new ProductionRateDto();

  prodRate : ProductionRateDto[]

  item: ItemDto[]
  ItemName : string;
  _productionRate : FormGroup;

  constructor(
    injector: Injector,
    private _productionRatService: ProductionRateServiceProxy,
    private _itemServiceProxy: ItemServiceProxy,
    private formBuilder: FormBuilder
  ) { }

  show(): void{
    this.active = true;
   
    this.modal.show();
    this.productionRate   = new ProductionRateDto();
    this.productionRate.productionRateDate = moment().add(5,'hour');
    this._productionRate.markAsUntouched({onlySelf:true});

}

ngOnInit(): void {

  this.getAllItem();
  //this.productionRate.productionRateDate = moment();
  this.initValidation();
 

  }

onShown(): void {
    // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
}

addSearchDD(){
  this.searchItemDropdownModel.show()
}

initValidation() {

  this._productionRate = this.formBuilder.group({
      itemId: [null, Validators.required],
      rate: [null, Validators.required],
      repairRate: [null, Validators.required],
      
    });

}

onType() {

if (this._productionRate.valid) {
    this.save();
} else {
    this.validateAllFormFields(this._productionRate);
}
}

validateAllFormFields(formGroup: FormGroup) {

Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
    }
});
}
//CategoryID
ItemId(id): void {
  debugger
  
  for(let i = 0; i< this.item.length; i++)
  {
    if(this.item[i].id == id)
    {
      this.ItemName = this.item[i].name;
      this.productionRate.itemId = this.item[i].id;
    }
  }


}
//Item

displayItem = (item: ItemDto): any => {
  debugger;

  if (item instanceof ItemDto) {
    this.productionRate.itemId = item.id;
    return item.name;
  }
  return item;

}


// get all ItemType

getAllItem()
{
  this._itemServiceProxy.getAllItems().subscribe((result)=>{

      this.item = result ;
  })
}


save(): void {

  debugger;
  this.saving = true;
  
  this._productionRatService.create(this.productionRate)
    .finally(() => { this.saving = false; })
    .subscribe(() => {
      this.notify();
      this.close();
      this.modalSave.emit(null);
    });
}

notify() {
  swal({
    title: "Success!",
    text: "Saved Successfully.",
    timer: 2000,
    showConfirmButton: false
  }).catch(swal.noop)
}

close(): void {
    this.active = false;
    this.modal.hide();
}

}
