import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProductionRateNewComponent } from './add-production-rate-new.component';

describe('AddProductionRateNewComponent', () => {
  let component: AddProductionRateNewComponent;
  let fixture: ComponentFixture<AddProductionRateNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProductionRateNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProductionRateNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
