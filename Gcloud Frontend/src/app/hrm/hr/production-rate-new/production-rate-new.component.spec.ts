import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionRateNewComponent } from './production-rate-new.component';

describe('ProductionRateNewComponent', () => {
  let component: ProductionRateNewComponent;
  let fixture: ComponentFixture<ProductionRateNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionRateNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionRateNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
