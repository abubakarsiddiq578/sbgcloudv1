import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { DesignationComponent } from '../designation.component';
import { EmployeeDesignationServiceProxy, EmployeeDesignationDto, HRConfigurationDto, HRConfigurationServiceProxy } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';


@Component({
    selector: 'edit-designation',
    templateUrl: './edit-designation.component.html',
    providers:[HRConfigurationServiceProxy , EmployeeDesignationServiceProxy]
})
export class EditDesignationComponent implements OnInit {

    @ViewChild('editDesignationModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    EmployeeDesignation: EmployeeDesignationDto = new EmployeeDesignationDto();

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;

    _empDesigValidation: FormGroup;

    constructor(
        injector: Injector,
        private _EmployeeDesignationService: EmployeeDesignationServiceProxy,
        private formBuilder: FormBuilder,
        private _hrConfigService : HRConfigurationServiceProxy

    ) {
        //super(injector);
    }

    show(id: number): void {

        this._EmployeeDesignationService.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: EmployeeDesignationDto) => {
                this.EmployeeDesignation = result;
            });
        this.active = true;
        this.modal.show();
    }

/////////////////////////////////////////////////////////////////////////////////////
    initValidation() {

        this._empDesigValidation = this.formBuilder.group({
            designationTitle: [null, Validators.required]
        });

    }

    //check form validation if it is valid then save it else throw error message in form //
    onType() {

        if (this._empDesigValidation.valid) {
            this.save();
        } else {
            this.validateAllFormFields(this._empDesigValidation);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    ngOnInit(): void {
        this.initValidation();
        this.getAllHRConfiguration();
    }


    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];

            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }
    

        });

    }



    close(): void {
        this.active = false;
        this.modal.hide();
    }

    save(): void {
        this.saving = true;
        this._EmployeeDesignationService.update(this.EmployeeDesignation)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                //this.notify.info(this.l('Record Updated Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }
}
