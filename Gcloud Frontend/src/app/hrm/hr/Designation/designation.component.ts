// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddDesignationComponent } from './add-designation/add-designation.component';
import { EditDesignationComponent } from './edit-designation/edit-designation.component';
import { EmployeeDesignationServiceProxy, EmployeeDesignationDto } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { GlobalFunctions } from '@app/GlobalFunctions';
import { Router } from '@angular/router';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
    selector: 'designation',
    templateUrl: 'designation.component.html',
    providers:[EmployeeDesignationServiceProxy]
})

export class DesignationComponent implements OnInit {

  @ViewChild('addDesignationModal') addDesignationModal: AddDesignationComponent;
  @ViewChild('editDesignationModal') editDesignationModal: EditDesignationComponent;

    public dataTable: DataTable;
    result: EmployeeDesignationDto[] = [];
    data: string[] = [];

    constructor(injector: Injector,
      //  private _authService: AppAuthService,
      private _EmployeeDesignationService: EmployeeDesignationServiceProxy,
      private _router : Router
    ) { }
    globalFunction: GlobalFunctions = new GlobalFunctions
    ngOnInit() {
      // if (!this.globalFunction.hasPermission("View", "Designation")) {
      //   this.globalFunction.showNoRightsMessage("View");
      //   this._router.navigate(['']);
      // }
      this.getAllRecord();
  
  
  
    }
  
    getAllRecord(): void {
      debugger
      this._EmployeeDesignationService.getAllEmployeeDesignations()
        .finally(() => {
          console.log('completed');
        })
        .subscribe((result: any) => {
          this.result = result;
          this.paging();
          this.getAllData();
  
        });
    }
  
    paging(): void {
        this.dataTable = {
          headerRow: [ 'Designation', 'Code', 'comments', 'Sort Order', 'Active', 'Actions' ],
          footerRow: [ 'Designation', 'Code', 'comments', 'Sort Order', 'Active', 'Actions' ],

          dataRows: [
              // ['1234', 'Manager', 'Develop', ''],
              // ['1234', 'Manager', 'Develop', 'btn-round'],
              // ['1234', 'Manager', 'Develop', 'btn-simple']
          ]
  
      };
    }
    getAllData(): void {
      debugger
      let i;
      for (i = 0; i < this.result.length; i++) {
        this.data.push(this.result[i].employeeDesignationName);
        this.data.push(this.result[i].code);
        this.data.push(this.result[i].comments);
        this.data.push(this.result[i].sortOrder.toString());
        this.data.push(this.result[i].isActive.toString());
        this.data.push(this.result[i].id.toString());
  
        this.dataTable.dataRows.push(this.data);
        this.data = [];
      }
    }

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();


      $('.card .material-datatables label').addClass('form-group');
    }

    protected delete(id: string): void {
      // if (!this.globalFunction.hasPermission("Delete", "EmployeeDesignation")) {
      //   this.globalFunction.showNoRightsMessage("Delete");
      //   return
      // }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Employee Designation!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this._EmployeeDesignationService.delete(parseInt(id))
            .finally(() => {
  
              this.getAllRecord();
            })
            .subscribe(() => {
              swal({
                title: 'Deleted!',
                text: 'Your Employee Designation has been deleted.',
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
              }).catch(swal.noop)
            });
        } else {
          swal({
            title: 'Cancelled',
            text: 'Your Employee Designation is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
          }).catch(swal.noop)
        }
      })
      
    }

    AddDesignation(): void {
      // if (!this.globalFunction.hasPermission("Create", "EmployeeDesignation")) {
      //   this.globalFunction.showNoRightsMessage("Create");
      //   return
      // }
      this.addDesignationModal.show();
  }

  EditDesignation(id: string): void{
    // if (!this.globalFunction.hasPermission("Edit", "EmployeeDesignation")) {
    //   this.globalFunction.showNoRightsMessage("Edit");
    //   return
    // }
      this.editDesignationModal.show(parseInt(id));
  }

}
