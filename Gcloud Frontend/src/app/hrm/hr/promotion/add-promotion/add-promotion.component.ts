import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { PromotionComponent } from '../promotion.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { EmployeePromotionServiceProxy, EmployeeServiceProxy, EmployeeDesignation, EmployeeDesignationServiceProxy, EmployeePromotionDto, EmployeeDto, EmployeeDesignationDto, HRConfigurationDto, HRConfigurationServiceProxy } from '../../../../shared/service-proxies/service-proxies';
///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { SearchEmployeeComponent } from '../search-employee/search-employee.component';

////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
@Component({
  selector: 'add-promotion',
  templateUrl: './add-promotion.component.html',
  styleUrls: ['./add-promotion.component.less'],
  providers: [EmployeePromotionServiceProxy, EmployeeServiceProxy, EmployeeDesignationServiceProxy , HRConfigurationServiceProxy]
})
export class AddPromotionComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('addPromotionModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
   
    @ViewChild('searchEmployeeDropdownModal') searchEmployeeDropdownModel: SearchEmployeeComponent;
 

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    emp: EmployeeDto[]
  EmpName : string;
    active: boolean = false;
    saving: boolean = false;
    employeePromotion: EmployeePromotionDto = new EmployeePromotionDto(); 
    public docDate = new Date();
    employeeDesignationDDlistTo: EmployeeDesignationDto[] = [];
    employeeDesignationDDlistFrom: EmployeeDesignationDto[] = [];
    employeeDDList: EmployeeDto[] = [];
    
    _employeePromotion: EmployeePromotionDto[];

    _docNo : abc[] = [];

    _employeePromotionValidation  : FormGroup

    _promotionValidation : FormGroup 
    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName: string;

    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;


    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       private _employeePromotionService: EmployeePromotionServiceProxy,
       private _employeeDesignationService: EmployeeDesignationServiceProxy,
       private _employeeService: EmployeeServiceProxy,
       private _hrConfigService: HRConfigurationServiceProxy,
       private formBuilder : FormBuilder
    ) {
        //super(injector);
    }
  

    show(): void{

        this.active = true;
        this.modal.show();
        this.employeePromotion = new EmployeePromotionDto();
        this.employeePromotion.init({ isActive: true });

        this._employeePromotionService.getAllEmployeePromotions().subscribe((result)=>{

            this._employeePromotion = result;
            //this.getAutoDocNumber();
            if(this.hrConfig.autoCode == true){

                this.getAutoDocNumber();
                this.isAutoCode = true ;
              }
              else if(this.hrConfig.autoCode == false){
                this.isAutoCode = false;
              }


        })
        this.empName = null;
        this.employeeDropdownSearch();
        this._promotionValidation.markAsUntouched({onlySelf:true});
        
    }

    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];
        });

    }


    initValidation() {

        this._promotionValidation = this.formBuilder.group({
            employeeId: [null, Validators.required],
            promotionTo: [null, Validators.required],
            promotionFrom: [null, Validators.required],
            promotionDate: [null, Validators.required]
                
        });
    
    }
    addSearchDD(){
        this.searchEmployeeDropdownModel.show()
      }
      
    
    //check form validation if it is valid then save it else throw error message in form //
    /*onType() {
    
      if (this._promotionValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._promotionValidation);
      }
    }*/

    onType() {
        
        if (this._promotionValidation.valid) {
            let a, p = 0;
            for (a = 0; a < this.employeeDDList.length; a++) {
                if (this.employeePromotion.employeeId == this.employeeDDList[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._promotionValidation);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._promotionValidation);

        }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
    ///////////////////////////end of validation code//////////////////////////////
      ///////////////////////////////search Method//////////////////////////////////
      public employeeDropdownSearch() {
        debugger;
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith<string | EmployeeDto>(''),
                map(value => typeof value === 'string' ? value : value.employee_Name),
                map(name => name ? this._filter(name) : this.employeeDDList.slice())
            );

    }

    private _filter(value: string): EmployeeDto[] {

        debugger;
        const filterValue = value.toLowerCase();

        return this.employeeDDList.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
    }


    displayEmployee = (emp: EmployeeDto): any => {
        debugger;

        if (emp instanceof EmployeeDto) {
            this.employeePromotion.employeeId = emp.id;
            return emp.employee_Name;
        }
        this.employeePromotion.employeeId = null;
        return emp;

    }

    ////////////////////////////////End of Search Method/////////////////////////////////////


    /*getAutoDocNumber():void{

        let i =0;
       
        for(i=0 ;i< this._employeePromotion.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._employeePromotion[i].docNo ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0 ){
            debugger;
            code = "1";
            this.employeePromotion.docNo = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    code = this._docNo[this._docNo.length-1].code ;
                    code ++;
                    if(code <=9){
    
                        this.employeePromotion.docNo = "00" + code ;
                    }
                    else if(code <=99){
    
                        this.employeePromotion.docNo = "0" + code ;
                    }
                    else if(code <=999){
    
                        this.employeePromotion.docNo =  code ;
                    }
            }                     
        }
        this._docNo = [];
    }*/
    


    getAutoDocNumber():void{

        debugger;
        let i =0;
        
        let temp : any[];
       
        for(i=0 ;i< this._employeePromotion.length ; i++){
    
            let abc_ ; 
    
            abc_ = new abc();
    
            abc_.code = this._employeePromotion[i].docNo ; 
    
            this._docNo.push(abc_);
    
        }
    
    
        let code : any ;
    
        if(this._docNo.length == 0  ){
          
            debugger;
            code = "1";
            this.employeePromotion.docNo = "00" + code ;
    
        }
    
        else{
    
            if(this._docNo[this._docNo.length - 1] != null ){
    
                    let x;
                    code = this._docNo[this._docNo.length-1].code ;
                    if(code!=null){
                      temp = code.split("-");
                       x = parseInt(temp[0]);
                       x = x.toString();
                    }
                    debugger;
                    //let j = parseInt(code);
                    //
                    if(temp.length == 1 && x=="NaN"  ){
    
                      debugger;
                      temp[1] = 0;
                      temp[1] ++;
    
                     
                      if(temp[1] <=9){
      
                          this.employeePromotion.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.employeePromotion.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.employeePromotion.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    
                    }
    
                    else if (temp.length == 2) {
    
                      temp[1] ++;
                      if(temp[1] <=9){
      
                          this.employeePromotion.docNo = temp[0] + "-00" + temp[1] ;
                      }
                      else if(temp[1] <=99){
      
                          this.employeePromotion.docNo =  temp[0] + "-0" + temp[1] ;
                      }
                      else if(temp[1]){
      
                          this.employeePromotion.docNo =  temp[0] + "-" +temp[1] ;
                      }
    
                    }
    
                    else if(temp.length == 0){
    
                      code ++;
                      if(code <=9){
      
                          this.employeePromotion.docNo = "00" + code ;
                      }
                      else if(code<=99){
      
                          this.employeePromotion.docNo =  "0" + code;
                      }
                      else if(code){
      
                          this.employeePromotion.docNo =  code ;
                      }
    
    
                    } 
                    else if(temp.length==1 && x!="NaN" ){
    
                      temp[0]++;
                      if(temp[0] <=9){
      
                          this.employeePromotion.docNo = "00" + temp[0] ;
                      }
                      else if(code<=99){
      
                          this.employeePromotion.docNo =  "0" + temp[0];
                      }
                      else if(code){
      
                          this.employeePromotion.docNo =  temp[0] ;
                      }
    
                    }
                    
            }                     
        }
        this._docNo = [];
    }



    save(): void {
        //TODO: Refactor this, don't use jQuery style code

        debugger
        this.saving = true;
        this._employeePromotionService.create(this.employeePromotion)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                // this.notify.info(this.l('Saved Successfully'));
                this.close();
                this.modalSave.emit(null);

            });
    }

    ngOnInit(): void {
        this._employeeService.getAllEmployees()
        .finally(() => { console.log('completed') })
        .subscribe((result: any) => {
            this.employeeDDList = result;

        })

    this._employeeDesignationService.getAllEmployeeDesignations()
        .finally(() => { console.log('completed') })
        .subscribe((result: any) => {
            this.employeeDesignationDDlistTo = result;
            this.employeeDesignationDDlistFrom = result;
        })
        
        this.initValidation();
        this.getAllHRConfiguration();
    }


    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }
    EmployeeId(id): void {
        debugger
        
        for(let i = 0; i< this.employeeDDList.length; i++)
        {
          if(this.employeeDDList[i].id == id)
          {
            this.EmpName = this.employeeDDList[i].employee_Name;
            this.employeePromotion.employeeId = this.employeeDDList[i].id;
          }
        }
      
      
      }
      //Item
      
      displayItem = (emp: EmployeeDto): any => {
        debugger;
      
        if (emp instanceof EmployeeDto) {
          this.employeePromotion.employeeId = emp.id;
          return emp.employee_Name;
        }
        return emp;
      
      }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}


// class for keeping all docNo number
class abc{
    public code: string;
  }

