// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, Output, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { AddPromotionComponent } from './add-promotion/add-promotion.component';
import { EditPromotionComponent } from './edit-promotion/edit-promotion.component';
import { EmployeePromotionServiceProxy, EmployeePromotionDto, EmployeeDesignationServiceProxy, EmployeeServiceProxy } from '../../../shared/service-proxies/service-proxies';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GlobalFunctions } from '@app/GlobalFunctions';

declare const $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}



@Component({
    selector: 'promotion',
    templateUrl: 'promotion.component.html',
    providers: [EmployeePromotionServiceProxy, EmployeeDesignationServiceProxy, EmployeeServiceProxy]
})

export class PromotionComponent implements OnInit, AfterViewInit {
    public dataTable: DataTable;
    result: EmployeePromotionDto[] = [];
    data: string[] = [];
    @ViewChild('addPromotionModal') addPromotionModal: AddPromotionComponent;
    @ViewChild('editPromotionModal') editPromotionModal: EditPromotionComponent;

  constructor(injector: Injector, private _employeePromotionService: EmployeePromotionServiceProxy
    ,private _router : Router ){}

    globalFunction: GlobalFunctions = new GlobalFunctions
    
    ngOnInit() {
      if (!this.globalFunction.hasPermission("View", "Promotion")) {
        this.globalFunction.showNoRightsMessage("View");
        this._router.navigate([''])
      }
      this.getAllRecord();
    }
    getAllRecord(): void {
      debugger
      this._employeePromotionService.getAllEmployeePromotions()
        .finally(() => {
          console.log('completed');
        })
        .subscribe((result: any) => {
          this.result = result;
          this.paging();
          this.getAllData();
        });
    }
    paging(): void {
      this.dataTable = {
        headerRow: [ 'Employee', 'Promotion Date', 'Promotion Type', 'Promotion From', 'Promoted To', 'Detail','Actions' ],
        footerRow: [ /*'Employee', 'Promotion Date', 'Promotion Type', 'Promotion From', 'Promoted To', 'Actions'*/ ],

        dataRows: [
            // ['AbuBakar', '10/08/2018', 'Internal', 'Engineer', 'Project Manager', ''],
            // ['Aashir', '10/08/2018', 'Internal', 'Developer', 'Web Engineer', ''],
            // ['Saad', '10/08/2018', 'Internal', 'Software Engineer', 'Sr. Software Engineer', ''],
        ]
     };
    }
    getAllData(): void {
      let i;
      for (i = 0; i < this.result.length; i++) {
        this.data.push(this.result[i].employee.employee_Name);
        this.data.push(this.result[i].promotionDate.format());
        this.data.push(this.result[i].promotionType);
        this.data.push(this.result[i].proFrom.employeeDesignationName);
        this.data.push(this.result[i].proTo.employeeDesignationName);
        this.data.push(this.result[i].detail);
        this.data.push(this.result[i].id.toString());
  
        this.dataTable.dataRows.push(this.data);
        this.data = [];
      }
    }
  

    ngAfterViewInit() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });

      const table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        // alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        // table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
    }

    protected delete(id: string): void {
      if (!this.globalFunction.hasPermission("Delete", "Promotion")) {
        this.globalFunction.showNoRightsMessage("Delete");
        return
      }
      swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this promotion Record!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          debugger;
          this._employeePromotionService.delete(parseInt(id))
            .finally(() => {
  
              this.getAllRecord();
            })
            .subscribe(() => {
              swal({
                title: 'Deleted!',
                text: 'Your promotion record has been deleted.',
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
              }).catch(swal.noop)
            });
        } else {
          debugger;
          swal({
            title: 'Cancelled',
            text: 'Your promotion record is safe :)',
            type: 'error',
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
          }).catch(swal.noop)
        }
      })
    }

    AddPromotion(): void {
      if (!this.globalFunction.hasPermission("Create", "Promotion")) {
        this.globalFunction.showNoRightsMessage("Create");
        return
      }
      this.addPromotionModal.show();
      }

      EditPromotion(id:string): void {
        if (!this.globalFunction.hasPermission("Edit", "Promotion")) {
          this.globalFunction.showNoRightsMessage("Edit");
          return
        }
        this.editPromotionModal.show(parseInt(id));
        }

}


