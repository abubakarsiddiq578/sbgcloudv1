import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { DateAdapter } from '@angular/material';


// import { UserServiceProxy, CreateUserDto, RoleDto, CostCenterDropdownDto, CostCenterDropdownServiceProxy, CompanyInfoDropDownDto, EmployeeDropdownDto, LocationInfoDropDownDto, CompanyInfoDropDownServiceProxy, EmployeeDropDownServiceProxy, LocationInfoDropdownServiceProxy, POSConfigurationDto, POSServiceProxy,  POSConfigurationDetailDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { EDEADLK } from 'constants';
import { PromotionComponent } from '../promotion.component';

//import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { EmployeePromotionDto, EmployeeDesignationDto, EmployeeDto, EmployeePromotionServiceProxy, EmployeeDesignationServiceProxy, EmployeeServiceProxy, HRConfigurationDto, HRConfigurationServiceProxy, PromotionServiceProxy } from '../../../../shared/service-proxies/service-proxies';

///imports for validations///////////////
import { FormBuilder, AbstractControl } from '@angular/forms';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
////////////////////////imports for search///////////////////////////////////////////
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'edit-promotion',
  templateUrl: './edit-promotion.component.html',
  styleUrls: ['./edit-promotion.component.less'],
  providers:[PromotionServiceProxy , HRConfigurationServiceProxy , EmployeeDesignationServiceProxy ]
})
export class EditPromotionComponent /*extends AppComponentBase*/ implements OnInit {

    @ViewChild('editPromotionModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    employeePromotion: EmployeePromotionDto = new EmployeePromotionDto(); 
    public docDate = new Date();
    employeeDesignationDDlistTo: EmployeeDesignationDto[] = [];
    employeeDesignationDDlistFrom: EmployeeDesignationDto[] = [];
    employeeDDList: EmployeeDto[] = [];

    _promotionValidation : FormGroup 
    myControl = new FormControl();
    filteredOptions: Observable<EmployeeDto[]>;
    public empName: string;
    hrConfig: HRConfigurationDto =  new HRConfigurationDto();

    isAutoCode : boolean;


    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy
       private _employeePromotionService: EmployeePromotionServiceProxy,
        private _employeeDesignationService: EmployeeDesignationServiceProxy,
        private _employeeService: EmployeeServiceProxy,
        private _hrConfigService : HRConfigurationServiceProxy,
        private formBuilder : FormBuilder
    ) {
        //super(injector);
    }
  

    show(id:number): void {
        debugger
        this._employeePromotionService.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: EmployeePromotionDto)=> {
                this.employeePromotion = result;
                this._employeeService.get(result.employeeId).subscribe((result)=>{

                    this.empName = result.employee_Name;
                });
            });
        this.employeeDropdownSearch();
        this._promotionValidation.markAsUntouched({onlySelf:true});
    }
    save(): void {
        debugger
        this.saving = true;
        this._employeePromotionService.update(this.employeePromotion)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            // this.notify.info(this.l('Record Updated Successfully'));
            this.close();
            this.modalSave.emit(null);
          });
      }

      initValidation() {

        this._promotionValidation = this.formBuilder.group({
            employeeId: [null, Validators.required],
            promotionTo: [null, Validators.required],
            promotionFrom: [null, Validators.required],
            promotionDate: [null, Validators.required]
                
        });
    
    }
    
    //check form validation if it is valid then save it else throw error message in form //
   /* onType() {
    
      if (this._promotionValidation.valid) {
          this.save();
      } else {
          this.validateAllFormFields(this._promotionValidation);
      }
    }*/
    onType() {
        
        if (this._promotionValidation.valid) {
            let a, p = 0;
            for (a = 0; a < this.employeeDDList.length; a++) {
                if (this.employeePromotion.employeeId == this.employeeDDList[a].id) ///check if employee selected matched in list or not
                {
                    p = 1;
                    this.save();
                    break;
                }
            }
            if (p == 0) {

                this.empName = null; // null empName 
                this.validateAllFormFields(this._promotionValidation);
                this.employeeDropdownSearch();
            }

        } else {
            this.validateAllFormFields(this._promotionValidation);

        }
    }
    
    validateAllFormFields(formGroup: FormGroup) {
    
      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });
    }
/////////////////////////////////////////////////////////////////
  ///////////////////////////////search Method//////////////////////////////////
  public employeeDropdownSearch() {
    debugger;
    this.filteredOptions = this.myControl.valueChanges
        .pipe(
            startWith<string | EmployeeDto>(''),
            map(value => typeof value === 'string' ? value : value.employee_Name),
            map(name => name ? this._filter(name) : this.employeeDDList.slice())
        );

}

private _filter(value: string): EmployeeDto[] {

    debugger;
    const filterValue = value.toLowerCase();

    return this.employeeDDList.filter(option => option.employee_Name.toString().toLowerCase().includes(filterValue));
}


displayEmployee = (emp: EmployeeDto): any => {
    debugger;

    if (emp instanceof EmployeeDto) {
        this.employeePromotion.employeeId = emp.id;
        return emp.employee_Name;
    }
    this.employeePromotion.employeeId = null;
    return emp;

}

////////////////////////////////End of Search Method/////////////////////////////////////



    ngOnInit(): void {
         this._employeeService.getAllEmployees()
        .finally(() => { console.log('completed') })
        .subscribe((result: any) => {
            this.employeeDDList = result;

        })


    this._employeeDesignationService.getAllEmployeeDesignations()
        .finally(() => { console.log('completed') })
        .subscribe((result: any) => {
            this.employeeDesignationDDlistTo = result;
            this.employeeDesignationDDlistFrom = result;
        })

        this.initValidation();
        this.getAllHRConfiguration();
    }


    
    getAllHRConfiguration() {

        this._hrConfigService.getAllHRConfiguration().subscribe((result) => {

            this.hrConfig = result[0];


            if (this.hrConfig.autoCode == true) {

                this.isAutoCode = true;
            }
            else if (this.hrConfig.autoCode == false) {
                this.isAutoCode = false;
            }

        });

    }




    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

