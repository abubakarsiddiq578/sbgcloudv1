import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { LocationInfoRoutes } from './locationinfo.routing';
import { ModalModule } from 'ngx-bootstrap';
import { HttpClientModule, HttpResponse } from '@angular/common/http';
import { JsonpModule, HttpModule } from '@angular/http';
import { ServiceProxyModule } from '../shared/service-proxies/service-proxies.module';


import { LocationInfoComponent } from './locationinfo.component';
import {AddLocationInfoComponent} from './addlocationinfo/addlocationinfo.component';
import {EditLocationInfoComponent} from './editlocationinfo/editlocationinfo.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LocationInfoRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule.forRoot(),
    HttpClientModule,
    JsonpModule, 
    HttpModule,
    ServiceProxyModule
  ],
  declarations: [
    LocationInfoComponent,
    AddLocationInfoComponent,
    EditLocationInfoComponent
  ]
})

export class LocationInfo {}
