import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserServiceProxy, CreateUserDto, RoleDto, LocationInfoServiceProxy, LocationInfoDto } from '../../shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'editlocation',
  templateUrl: './editlocationinfo.component.html',
animations: [/*appModuleAnimation()*/],
  providers: [LocationInfoServiceProxy]
})
// export class EditLocationInfoComponent extends AppComponentBase implements OnInit {
export class EditLocationInfoComponent implements OnInit {

    @ViewChild('EditLocationInfoModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    locationInfo: LocationInfoDto = new LocationInfoDto();

    constructor(
        injector: Injector,
        private _locationInfoService: LocationInfoServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    show(id:number): void {
        debugger
        this._locationInfoService.get(id)
            .finally(() => {
                this.active = true;
                this.modal.show();
            })
            .subscribe((result: LocationInfoDto)=> {
                this.locationInfo = result;
            })
        
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        this.saving = true;
        this._locationInfoService.update(this.locationInfo)
          .finally(() => {this.saving = false;})
          .subscribe(() => {
            // this.notify.info(this.l('Record Updated Successfully'));
            this.close();
            this.modalSave.emit(null);
          });
      }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
