import { Component, Injector, AfterViewInit , OnInit, ViewChild} from '@angular/core';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';
// import { AppAuthService } from '@shared/auth/app-auth.service';
import { AddLocationInfoComponent } from "./addlocationinfo/addlocationinfo.component";
import { EditLocationInfoComponent } from "./editlocationinfo/editlocationinfo.component";
// import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
// import { LocationInfoServiceProxy, LocationInfoDto, PagedResultDtoOfLocationInfoDto } from '@shared/service-proxies/service-proxies';
// import { PagedRequestDto } from '@shared/paged-listing-component-base';
import { Subject } from 'rxjs/Subject';
import { LocationInfoServiceProxy, LocationInfoDto } from '../shared/service-proxies/service-proxies';
import { Http } from '@angular/http';
import { convertInjectableProviderToFactory } from '@angular/core/src/di/injectable';
import swal from 'sweetalert2';


declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: string[][];
  }
  
  declare const $: any;

  

@Component({
    templateUrl: './locationinfo.component.html',
    styleUrls: [
        './locationinfo.component.less'
    ],
    animations: [/*appModuleAnimation()*/],
    providers: [LocationInfoServiceProxy]
})
// export class LocationInfoComponent extends PagedListingComponentBase<LocationInfoDto> {
export class LocationInfoComponent implements OnInit {
     @ViewChild('AddLocationInfoModal') AddLocaitonInfoModal: AddLocationInfoComponent;
    @ViewChild('EditLocationInfoModal') EditLocationInfoModal: EditLocationInfoComponent;
    public dataTable: DataTable;

    data: string[] = [];

     result: LocationInfoDto[]=[];
     errorMessage:string;
    // IsEdit: boolean = abp.auth.isGranted("Pages.Administration.Edit_Samples");;
    // IsDelete: boolean = abp.auth.isGranted("Pages.Administration.Delete_Samples");
    // IsCreate: boolean = abp.auth.isGranted("Pages.Administration.Create_Samples");
  
    // variables for datatables
    // dtOptions: DataTables.Settings = {};  
    // dtTrigger: Subject<any> = new Subject();

    constructor(injector: Injector , 
        /*private _authService: AppAuthService,*/
        private http: Http,
        private _locationInfoService: LocationInfoServiceProxy
        
    ) 
    {
        //super(injector);
    }
    // public list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
    //     this._locationInfoService.getAll(request.skipCount, request.maxResultCount)
    //         .finally(()=>{
    //             finishedCallback();
    //         })
    //         .subscribe((result:PagedResultDtoOfLocationInfoDto)=>{
    //     this.result = result.items;

    //     //Below line is for datatable
    //     this.dtTrigger.next();
        
    //     this.showPaging(result, pageNumber);
    //         });
    // }


    protected delete(id: string): void {
        swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this Location!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it',
          confirmButtonClass: "btn btn-success",
          cancelButtonClass: "btn btn-danger",
          buttonsStyling: false
        }).then((result) => {
        if (result.value) {
     
          this._locationInfoService.delete(parseInt(id))
            .finally(() => {
     
            //   this.getAllRecord();
            })
            .subscribe(() => {
              swal({
                title: 'Deleted!',
                text: 'Your Salary Expense Type has been deleted.',
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
            }).catch(swal.noop)
             });
        } else {
          swal({
              title: 'Cancelled',
              text: 'Your Salary Expense Type is safe :)',
              type: 'error',
              confirmButtonClass: "btn btn-info",
              buttonsStyling: false
          }).catch(swal.noop)
        }
      })
      }



    // protected delete(entity: LocationInfoDto): void {
    //     abp.message.confirm(
    //   "Delete Sample '"+ entity.location_Name +"'?",
    //   (result:boolean) => {
    //     if(result) {
    //       this._locationInfoService.delete(entity.id)
    //         .finally(() => {
    //               abp.notify.info("Deleted Sample: " + entity.location_Name );
    //           this.refresh();
    //         })
    //         .subscribe(() => { });
    //     }
    //   });
    //   }

      ngAfterViewInit(){
        //this.refresh();
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
              [10, 25, 50, -1],
              [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Search records",
            }
      
          });
      
          const table = $('#datatables').DataTable();
      
          // Edit record
          table.on('click', '.edit', function(e) {
            const $tr = $(this).closest('tr');
            const data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            e.preventDefault();
          });
      
          // Delete a record
          table.on('click', '.remove', function(e) {
            const $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
          });
      
          //Like record
          table.on('click', '.like', function(e) {
            alert('You clicked on Like button');
            e.preventDefault();
          });
      
          $('.card .material-datatables label').addClass('form-group');

          
        
      }
    //   upload() {
    //     return this.http.get("http://localhost:21021/api/services/app/LocationInfo/GetAllLocation");
    // }


    // getConfig() {
    //     return this.http.get("http://localhost:21021/api/services/app/LocationInfo/GetAllLocation");
    //   }
    
      ngOnInit(){



         this.getAllRecord();
        // this.dtOptions = {
        //     pagingType: 'full_numbers',  
        //     pageLength: 2
        //   };
           
      }

      getAllRecord(): void {
          debugger
        this._locationInfoService.getAllLocation()
        .finally(()=>{
            console.log('completed');
        })
        .subscribe((result:any)=>{
    this.result = result;
    this.paging();
    this.getAllData(); 
        });
      }

    paging(): void{
        this.dataTable = {
            headerRow: [ 'Code', 'Name',  'Phone', 'Fax',  'Webpage', 'Address', 'Type', 'Mobile No', 'Comments', 'Active', 'Restricted', 'Allow Minus', 'Sort Order',  'Actions'],
            footerRow: [/* 'Code', 'Name',  'Phone', 'Fax',  'Webpage', 'Address', 'Type', 'Mobile No', 'Comments', 'Active', 'Restricted', 'Allow Minus', 'Sort Order',  'btn-round' */ ],
            dataRows: 
            [  
                //['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','fhdskjfh    ', 'btn-round'],
                // ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
                //  ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'], 
            ]
         };
    }

    open(): void{
        //abp.message.error("Open");
    }
    AddLocationInfo(): void {
        this.AddLocaitonInfoModal.show();
    }

    EditLocationInfo( id: string /*tenant: LocationInfoDto*/):void {
        debugger
        this.EditLocationInfoModal.show(parseInt(id)  /*tenant.id*/);
    }
    getAllData():void{
          let i;
          for(i=0; i<this.result.length ; i++)
          {
            this.data.push(this.result[i].location_Code);
            this.data.push(this.result[i].location_Name);
            this.data.push(this.result[i].location_Phone);
            this.data.push(this.result[i].location_Fax);
            this.data.push(this.result[i].location_Url);
            this.data.push(this.result[i].location_Address);
            this.data.push(this.result[i].location_Type);
            this.data.push(this.result[i].mobile_No);
            this.data.push(this.result[i].comments);
            this.data.push(this.result[i].isActive.toString());
            this.data.push(this.result[i].restrictedItems.toString());
            this.data.push(this.result[i].allowMinusStock.toString());
            this.data.push(this.result[i].sort_Order.toString());
            this.data.push(this.result[i].id.toString());
            this.dataTable.dataRows.push(this.data);
            this.data = [];
          }
    }

}