import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { LocationInfoServiceProxy, LocationInfoDto, UserServiceProxy, CreateUserDto, RoleDto } from '../../shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';

// import { LocationInfoComponent } from '@app/locationinfo/locationinfo.component';
import {LocationInfoComponent} from '../locationinfo.component';
import { viewParentEl } from '@angular/core/src/view/util';



@Component({
  selector: 'addlocationinfo',
  templateUrl: './addlocationinfo.component.html'
})
// export class AddLocationInfoComponent extends AppComponentBase implements OnInit {
export class AddLocationInfoComponent implements OnInit {

    @ViewChild('AddLocationInfoModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    
    

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    locationInfo: LocationInfoDto = new LocationInfoDto();


    constructor(
        injector: Injector,
        // private _userService: UserServiceProxy,
        private _locationInfoService: LocationInfoServiceProxy
    ) {
        //super(injector);
    }

    ngOnInit(): void {
    }

    show(): void {
        this.active = true;
        this.modal.show();
        this.locationInfo = new LocationInfoDto();
        this.locationInfo.init({isActive:true});
    }

    onShown(): void {
        //$.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        //TODO: Refactor this, don't use jQuery style code
      
      this.saving = true;
        this._locationInfoService.create(this.locationInfo)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                // this.notify.info(this.l('Saved Successfully'));
                this.close();   
                this.modalSave.emit(null);
                
            });
      }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
