import { Routes } from '@angular/router';
import { LocationInfoComponent } from './locationinfo.component';

export const LocationInfoRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'LocationInfo',
        component: LocationInfoComponent
    }]}
];  
