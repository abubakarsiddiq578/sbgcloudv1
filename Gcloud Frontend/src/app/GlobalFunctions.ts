declare const $: any;
// In this class 
export class GlobalFunctions {

    /**
 * Return premission(true or false) of the given permission type i.e; Edit, View etc ´.
 * @param permissionType  Type of permission i.e, View, Edit etc.
 * @param formName  Name of the form.
 * @returns ´IsPermission´ return IsPermission.
 */
    hasPermission(permissionType: any, formName: any) {
        let IsPermission: boolean = abp.auth.isGranted("Pages.Hrm." + formName + "." + permissionType + "_" + formName);
        return IsPermission
    }

    /**
 * Shows error message,  red notification´.
 * @param message  this is the message that will be displayed on that notification.
 * @param form  where to show message i.e, Top and Bottom.
 * @param align  message alighnment i.e, Left and Right.
 * @param duration duration of the message.
 */
    showErrorMessage(message: any, from: any, align: any, duration?: any) {

        $.notify({
            icon: 'notifications',
            message: message
        }, {
                type: 'danger',
                timer: duration ? duration : 1000,
                placement: {
                    from: from,
                    align: align
                },
            });
    }


    /**
 * show message For No Permission.
 * @param permissionType type of the permission i.e, View .
 */
    showNoRightsMessage(rightType: any) {
        let message: any = "You do not have rights to " + rightType + " this form"
        $.notify({
            icon: 'notifications',
            message: message
        }, {
                type: 'danger',
                timer: 1000,
                placement: {
                    from: 'top',
                    align: 'right'
                },
            });
    }
}



