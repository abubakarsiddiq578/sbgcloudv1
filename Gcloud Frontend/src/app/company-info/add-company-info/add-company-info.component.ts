import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { CompanyInfoServiceProxy, CompanyInfoDto } from '@shared/service-proxies/service-proxies';
// import { CostCenterDropDownServiceProxy, CostCenterDropDownDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';



@Component({
  selector: 'add-company-info',
  templateUrl: './add-company-info.component.html',
  styleUrls: ['./add-company-info.component.css']
})
export class AddCompanyInfoComponent implements OnInit {

  @ViewChild('addCompanyInfoModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active: boolean = false;
  saving: boolean = false;
  // costCenterList:CostCenterDropDownDto[] = null;
  // CompanyInfo:CompanyInfoDto = new CompanyInfoDto();

  constructor(
    injector: Injector, 
    // private _companyInfoService: CompanyInfoServiceProxy,
    // private _costCenterDropDownService: CostCenterDropDownServiceProxy
  ) {
    // super(injector);
   }

  ngOnInit(): void {
    
    // this._costCenterDropDownService.getCostCenterDropDown()
    //   .subscribe((result) => {
    //     this.costCenterList = result.items;
    //   });
  }

  show(): void {
    this.active = true;
    this.modal.show();
    // this.CompanyInfo = new CompanyInfoDto();
    // this.CompanyInfo.init({ isActive: true });
    
  }

  onShown(): void {
    // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
}


// save(): void {
//   //TODO: Refactor this, don't use jQuery style code
// this.saving = true;
//   this._companyInfoService.create(this.CompanyInfo)
//       .finally(() => { this.saving = false; })
//       .subscribe(() => {
//           this.notify.info(this.l('Saved Successfully'));
//           this.close();
//           this.modalSave.emit(null);
//       });
// }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

}
