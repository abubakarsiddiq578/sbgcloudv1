import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AddCompanyInfoComponent } from './add-company-info/add-company-info.component';
import { EditCompanyInfoComponent } from './edit-company-info/edit-company-info.component';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.scss']
})
export class CompanyInfoComponent implements OnInit, AfterViewInit {

  @ViewChild('addCompanyInfoModal') _addCompanyInfoModal: AddCompanyInfoComponent;
  @ViewChild('editCompanyInfoModal') _editCompanyInfoModal: EditCompanyInfoComponent;
    public dataTable: DataTable;

  constructor() { }

  ngOnInit() {
    this.dataTable = {
      headerRow: [ 'Code', 'Name', 'Legal Name', 'Phone', 'Fax', 'Email', 'Webpage', 'Address', 'Cost Center', 'Sales Tax Account', 'Prefix', 'Commercial Invoice', 'Actions'],
      footerRow: [ 'Code', 'Name', 'Legal Name', 'Phone', 'Fax', 'Email', 'Webpage', 'Address', 'Cost Center', 'Sales Tax Account', 'Prefix', 'Commercial Invoice', 'Actions'],

      dataRows: [
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple'],
          ['Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', '', 'btn-round'],
          ['Angelica Ramos', 'John Doe', 'Design', '2012', '89,241', 'btn-round','Airi Satou', 'Andrew Mike', 'Develop', '2013', '99,225', 'btn-simple']
          
      ]
   };
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function(e) {
      const $tr = $(this).closest('tr');
      const data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function(e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function(e) {
      alert('You clicked on Like button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  EditCompany(){
    this._editCompanyInfoModal.show();
  }
  AddCompany(){
    this._addCompanyInfoModal.show();
  }

}
