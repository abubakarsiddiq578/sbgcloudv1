import { Component, ViewChild, Injector, EventEmitter, Output, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
// import { CompanyInfoServiceProxy, CompanyInfoDto, CostCenterDropDownServiceProxy, CostCenterDropDownDto } from '@shared/service-proxies/service-proxies';
// import { AppComponentBase } from '@shared/app-component-base';
// import { appModuleAnimation } from '@shared/animations/routerTransition';


@Component({
  selector: 'app-edit-company-info',
  templateUrl: './edit-company-info.component.html',
  styleUrls: ['./edit-company-info.component.css'],
  // animations: [appModuleAnimation()],
  providers: [
    // CompanyInfoServiceProxy,
    // CostCenterDropDownServiceProxy
  ]
})
export class EditCompanyInfoComponent implements OnInit {

  @ViewChild('editCompanyInfoModal') modal: ModalDirective;
  @ViewChild('modalContent') modalContent: ElementRef;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  // CompanyInfo:CompanyInfoDto= new CompanyInfoDto();
  active: boolean = false;
  saving: boolean = false;
  // costCenterList: CostCenterDropDownDto[] = null;


  constructor(
    injector: Injector, 
    // private _companyInfoService: CompanyInfoServiceProxy,
    // private _costCenterDropDownService: CostCenterDropDownServiceProxy
  ) {
    // super(injector);
   }

  ngOnInit() {
    // this._costCenterDropDownService.getCostCenterDropDown()
    //   .subscribe((result) => {
    //     this.costCenterList = result.items;
    //   });
  }


  show(): void {
    this.active = true;
    this.modal.show();
		// this._companyInfoService.get(id)
		// 	.finally(()=>{
		// 		this.active = true;
		// 		this.modal.show();
		// 	})
		// 	.subscribe((result: CompanyInfoDto)=>{
		// 		this.CompanyInfo = result;
		// 	});
    }

    onShown(): void {
      // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }

    // save(): void {
    //   this.saving = true;
    //   this._companyInfoService.update(this.CompanyInfo)
    //     .finally(() => {this.saving = false;})
    //     .subscribe(() => {
    //       this.notify.info(this.l('Record Updated Successfully'));
    //       this.close();
    //       this.modalSave.emit(null);
    //     });
    // }


  close(): void {
    this.active = false;
    this.modal.hide();
  }
}
