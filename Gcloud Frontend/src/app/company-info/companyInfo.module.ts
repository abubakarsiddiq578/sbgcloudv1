import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { CompanyInfoRoutes } from './companyInfo.routing';
import { ModalModule } from 'ngx-bootstrap';

import { CompanyInfoComponent } from './company-info.component';
import { AddCompanyInfoComponent } from './add-company-info/add-company-info.component';
import { EditCompanyInfoComponent } from './edit-company-info/edit-company-info.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CompanyInfoRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    ModalModule.forRoot()
  ],
  declarations: [
    CompanyInfoComponent,
    AddCompanyInfoComponent,
    EditCompanyInfoComponent
  ]
})

export class CompanyInfo {}
