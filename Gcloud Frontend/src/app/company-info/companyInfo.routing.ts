import { Routes } from '@angular/router';
import { CompanyInfoComponent } from './company-info.component';

export const CompanyInfoRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'CompanyInfo',
        component: CompanyInfoComponent
    }]}
];  
