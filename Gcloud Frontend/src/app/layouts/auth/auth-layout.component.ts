import { Component, OnInit, ElementRef , ViewChild, inject, Injector, Inject} from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import swal from 'sweetalert2';
import { IsTenantAvailableInput } from 'app/shared/service-proxies/service-proxies';
import { AccountServiceProxy } from 'app/shared/service-proxies/service-proxies';
import { AppTenantAvailabilityState } from 'app/shared/AppEnums';
import { AppConsts } from 'app/shared/AppConsts';
import { AppComponentBase } from '../../../app/shared/app-component-base';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
declare var $: any;

@Component({
  selector: 'app-layout',
  templateUrl: './auth-layout.component.html'
})
export class AuthLayoutComponent extends AppComponentBase implements OnInit {
  private toggleButton: any;
  private sidebarVisible: boolean;
  mobile_menu_visible: any = 0;
  private _router: Subscription;

  saving: boolean = false;

  tenancyName: string;
  name: string;
  

  constructor( injector: Injector,  private router: Router, private element: ElementRef , 
    private _accountService: AccountServiceProxy , @Inject(LOCAL_STORAGE) private storage: WebStorageService) {

        super(injector);

      this.sidebarVisible = false;
  }

  ngOnInit(){
    const navbar: HTMLElement = this.element.nativeElement;

    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
      this.sidebarClose();
    });
    
     if (this.appSession.tenant) {
         this.saveTenantInLocal('TenantId' , this.appSession.tenantId);
         this.tenancyName = this.appSession.tenant.tenancyName;
         this.name = this.appSession.tenant.name;
    } 
  }

  saveTenantInLocal(key, val): void {
    //console.log('recieved= key:' + key + 'value:' + val);
    this.storage.set(key, val);
   }

  sidebarOpen() {
      const toggleButton = this.toggleButton;
      const body = document.getElementsByTagName('body')[0];
      setTimeout(function(){
          toggleButton.classList.add('toggled');
      }, 500);
      body.classList.add('nav-open');

      this.sidebarVisible = true;
  };
  sidebarClose() {
      const body = document.getElementsByTagName('body')[0];
      this.toggleButton.classList.remove('toggled');
      this.sidebarVisible = false;
      body.classList.remove('nav-open');
  };
  sidebarToggle() {
    const body = document.getElementsByTagName('body')[0];
      if (this.sidebarVisible === false) {
          this.sidebarOpen();
          var $layer = document.createElement('div');
          $layer.setAttribute('class', 'close-layer');
          if (body.querySelectorAll('.wrapper-full-page')) {
              document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
          }else if (body.classList.contains('off-canvas-sidebar')) {
              document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
          }
          setTimeout(function() {
              $layer.classList.add('visible');
          }, 100);
          $layer.onclick = function() { //asign a function
            body.classList.remove('nav-open');
            this.mobile_menu_visible = 0;
            $layer.classList.remove('visible');
            this.sidebarClose();
          }.bind(this);

          body.classList.add('nav-open');
      } else {
        document.getElementsByClassName("close-layer")[0].remove();
          this.sidebarClose();
      }
  }

  get isMultiTenancyEnabled(): boolean {
           
    return abp.multiTenancy.isEnabled;
  }

  saveTenant(tenancyName: string): void {
    if (!tenancyName) {
        abp.multiTenancy.setTenantIdCookie(undefined);
        this.saveTenantInLocal('TenantId' , 0);
        //this.close();
        location.reload();
        return;
    }

    var input = new IsTenantAvailableInput();
    input.tenancyName = tenancyName;

    this.saving = true;
    this._accountService.isTenantAvailable(input)
        .finally(() => { this.saving = false; })
        .subscribe((result) => {
            switch (result.state) {
                case AppTenantAvailabilityState.Available:
                    abp.multiTenancy.setTenantIdCookie(result.tenantId);
                    //this.close();
                    location.reload();
                    return;
                case AppTenantAvailabilityState.InActive:
                    this.TenantIsNotActive(tenancyName);
                    break;
                case AppTenantAvailabilityState.NotFound: //NotFound
                    this.ThereIsNoTenantDefinedWithName(tenancyName);
                    break;
            }
        });
    }

    TenantIsNotActive(tenancyName: string){
        swal({
            title: "Warning",
            text: tenancyName + "Tenant Is Not Active",
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }

    ThereIsNoTenantDefinedWithName(tenancyName: string){
        swal({
            title: "Warning",
            text: "There Is No Tenant Defined With Name " + tenancyName,
            timer: 2000,
            showConfirmButton: false
        }).catch(swal.noop)
    }

  changeTenant(): void{
    swal({
        title: 'Change Tenant',
        html: '<div class="form-group">' +
            '<input id="input-field" type="text" class="form-control" />' +
            '</div>',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then((result) => {
        if (result.value) {
            this.saveTenant($('#input-field').val());
            swal({
                type: 'success',
                html: 'Your Selected Tenant: <strong>' +
                    $('#input-field').val() +
                    '</strong>',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false
            }).catch(swal.noop)
          } else {
            swal({
                title: 'Cancelled',
                //text: 'You cancel tenant change :)',
                type: 'error',
                confirmButtonClass: "btn btn-info",
                buttonsStyling: false
            }).catch(swal.noop)
          }
    }).catch(swal.noop)
  }
}
