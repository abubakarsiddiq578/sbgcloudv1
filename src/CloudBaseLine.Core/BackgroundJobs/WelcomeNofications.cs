﻿using Abp;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Notifications;
using Abp.Threading;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.BackgroundJobs
{
    public class WelcomeNofications : BackgroundJob<UserIdentifier>, ITransientDependency
    {
        private readonly IRealTimeNotifier _realTimeNotifier;
        private readonly IUserNotificationManager _userNotificationManager;

        public WelcomeNofications(IRealTimeNotifier realTimeNotifier, IUserNotificationManager userNotificationManager)
        {
            _realTimeNotifier = realTimeNotifier;
            _userNotificationManager = userNotificationManager;
        }
        public override void Execute(UserIdentifier args)
        {
            var notifications = _userNotificationManager.GetUserNotifications(args);
            AsyncHelper.RunSync(() => _realTimeNotifier.SendNotificationsAsync(notifications.ToArray()));
        }
    }
}
