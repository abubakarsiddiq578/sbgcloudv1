﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class AttendanceType : BaselineBaseEntity
    {
        public string Status { get; set; }

        public string Comments { get; set; }
    }
}
