﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Bonuss : BaselineBaseEntity
    {
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }

        public DateTime BonusDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("BonusType")]
        public long BonusTypeId { get; set; }
        public virtual BonusType BonusType { get; set; }

        public int Percentage { get; set; }

        public string Detail { get; set; }
    }
}
