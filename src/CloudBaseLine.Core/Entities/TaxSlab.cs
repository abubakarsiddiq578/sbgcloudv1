﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class TaxSlab : BaselineBaseEntity
    {
        public string Code { get; set; }


        public string TaxType { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public long ValueFrom { get; set; }

        public long valueTo { get; set; }

        public decimal TaxPercentage { get; set; }

        public long Fixed { get; set; }

        public long ApplicableValue { get; set; }

        public decimal PerMonthvalue { get; set; }
    }
}
