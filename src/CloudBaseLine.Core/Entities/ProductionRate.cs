﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class ProductionRate : BaselineBaseEntity
    {
        [ForeignKey("Item")]
        public long ItemId { get; set; }
        public virtual Item Item { get; set; }

        public double Rate { get; set; }

        public DateTime ProductionRateDate { get; set; }

        public double RepairRate { get; set; }

        public Boolean? IsUpdated { get; set; }
    }
}
