﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Termination : BaselineBaseEntity
    {
        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }

        public DateTime TerminationDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public DateTime NoticeDate { get; set; }

        public string TerminationType { get; set; }

        public string Detail { get; set; }

        

    }
}
