﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Configuration
{
    public class OvertimeConfiguration : BaselineBaseEntity
    {
        public string WorkingDays { get; set; }

        public int Workinghours { get; set; }

        public int RegularDayHourRate { get; set; }

        public int OffDayHourRate { get; set; }

        public bool DefaultWorkingDays { get; set; }
    }
}
