﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Territory:BaselineBaseEntity
    {
        [ForeignKey("City")]
        public long CityId { get; set; }
        public virtual City City { get; set; }

        public string TerritoryName { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }


    }
}
