﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Department : BaselineBaseEntity
    {
        public string Code { get; set; }

        public string Title { get; set; }

        public int SortOrder { get; set; }
        
        public string SalaryGroup { get; set; }

        public string Comments { get; set; }
        
    }
}
