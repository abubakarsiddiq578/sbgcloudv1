﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CloudBaseLine.BaseEntity;
using Newtonsoft.Json;

namespace CloudBaseLine.Entities.WorkGroup
{
    public class WorkGroupMaster : BaselineBaseEntity
    {
        [ForeignKey("ItemType")]
        public long ItemTypeId { get; set; }
        public virtual ItemType ItemType { get; set; }

        [ForeignKey("Category")]
        public long CategoryId { get; set; }
        public virtual Category Category { get; set; }

        [ForeignKey("SubCategory")]
        public long SubCategoryId { get; set; }
        public virtual SubCategory SubCategory { get; set; }

        public string GroupName { get; set; }

        [JsonIgnore]
        public virtual ICollection<WorkGroupEmployeeDetail> WorkGroupEmployeeDetails { get; set; }

        [JsonIgnore]
        public virtual ICollection<WorkGroupItemDetail> WorkGroupItemDetails { get; set; }

        public WorkGroupMaster()
        {
            WorkGroupEmployeeDetails = new Collection<WorkGroupEmployeeDetail>();
            WorkGroupItemDetails = new Collection<WorkGroupItemDetail>();
        }

    }
}
