﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CloudBaseLine.BaseEntity;

namespace CloudBaseLine.Entities.WorkGroup
{
    public class WorkGroupEmployeeDetail : BaselineBaseEntity
    {
        [ForeignKey("WorkGroupMaster")]
        public long WorkGroupMasterId { get; set; }
        public virtual WorkGroupMaster WorkGroupMaster { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public double Ratio { get; set; }

    }
}
