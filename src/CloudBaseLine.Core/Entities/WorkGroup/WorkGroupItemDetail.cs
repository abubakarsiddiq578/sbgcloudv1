﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CloudBaseLine.BaseEntity;

namespace CloudBaseLine.Entities.WorkGroup
{
    public class WorkGroupItemDetail : BaselineBaseEntity
    {
        [ForeignKey("WorkGroupMaster")]
        public long WorkGroupMasterId { get; set; }
        public virtual WorkGroupMaster WorkGroupMaster { get; set; }


        [ForeignKey("Item")]
        public long ItemId { get; set; }
        public virtual Item Item { get; set; }
    }
}
