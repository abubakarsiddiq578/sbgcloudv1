﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class LoanRequest : BaselineBaseEntity
    {
        public string DocNo { get; set; }
        public DateTime DocDate { get; set; }
        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public double LoadAmount { get; set; }
        public double MonthlyRepayementAmount { get; set; }
        public DateTime RepayementStartDate { get; set; }
        public string Detail { get; set; }
    }
}
