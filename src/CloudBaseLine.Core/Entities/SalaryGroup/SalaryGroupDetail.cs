﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace CloudBaseLine.Entities.SalaryGroup
{
    public class SalaryGroupDetail : BaselineBaseEntity
    {
        [ForeignKey("SalaryGroupMaster")]
        public long SalaryGroupMasterId { get; set; }
        public virtual SalaryGroupMaster SalaryGroupMaster { get; set; }

        public string SalaryType { get; set; }

        public string ApplyValue { get; set; }

        public double Value { get; set; }

        public int SortOrder { get; set; }

        public bool Dedeuction { get; set; }

        public bool Exempted { get; set; }
    }
}
