﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SalaryGroup
{
    public class SalaryGroupMaster : BaselineBaseEntity
    {
        public string SalaryGroupTitle { get; set; }

        public virtual IEnumerable<SalaryGroupDetail> SalaryGroupDetails { get; set; }
    }
}
