﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Overtime : BaselineBaseEntity
    {
        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public DateTime OverTimeDate { get; set; }
        
        public int OverTimerHour { get; set; }

        [ForeignKey("CostCenter")]
        public long CostCenterId { get; set; }
        public virtual CostCenter CostCenter{ get; set; }

        [ForeignKey("Department")]
        public long DepartmentId { get; set; }
        public virtual Department Department { get; set; }


    }
}
