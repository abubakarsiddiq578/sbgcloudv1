﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class CostCenter: BaselineBaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int SortOrder { get; set; }
        public String CostCenterGroup { get; set; }
        public Boolean OutwardGatepass { get; set; }
        public Boolean DayShift { get; set; }
        public int LCDocId { get; set; }
        public Boolean IsLogical { get; set; }


        //public POSConfiguration POSConfigurations { get; set; }

        //public CostCenter()
        //{
        //    POSConfigurations = new POSConfiguration();

        //}

        //[ForeignKey("POSConfigurations")]
        //public long POSConfigurationId { get; set; }

        //public POSConfiguration POSConfigurations { get; set; }
    }
}
