﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class UserCompanyRight: BaselineBaseEntity
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public virtual User User{ get; set; }

        [ForeignKey("CompanyInfo")]
        public long CompanyId { get; set; }
        public virtual CompanyInfo CompanyInfo { get; set; }

        public bool Rights { get; set; }
    }
}
