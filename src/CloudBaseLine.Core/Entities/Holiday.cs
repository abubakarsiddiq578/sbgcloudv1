﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Holiday:BaselineBaseEntity
    {

        [ForeignKey("CostCenter")]
        public long CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        [ForeignKey("Department")]
        public long DepartmentId { get; set; }
        public virtual Department Department { get; set; }


        [ForeignKey("EmployeeDesignation")]
        public long EmployeeDesignationId { get; set; }
        public virtual EmployeeDesignation EmployeeDesignation { get; set; }

        [ForeignKey("LeaveType")]
        public long LeaveTypeId { get; set; }
        public virtual LeaveType LeaveType { get; set; }


        public DateTime holidayStartDate { get; set; }

        public DateTime holidayEndDate { get; set; }

        public string HolidayDetail { get; set; }

    }
}
