﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Zone:BaselineBaseEntity
    {

        [ForeignKey("Region")]
        public long RegionId { get; set; }
        public virtual Region Region { get; set; }

        public string ZoneName { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }


    }
}
