﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class LeaveRequest : BaselineBaseEntity
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        public string ApplicationNo { get; set; }

        public DateTime ApplicationDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public string AlternateContactNo { get; set; }

        [ForeignKey("LeaveType")]
        public long LeaveTypeId { get; set; }
        public LeaveType LeaveType { get; set; }

        public string Duration { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string Reason { get; set; }

        public Boolean Approved { get; set; }


    }
}
