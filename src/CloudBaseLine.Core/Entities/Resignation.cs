﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Resignation : BaselineBaseEntity
    {

        public string DocumentNo { get; set; }


        public DateTime DocumentDate { get; set; }


        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        
        public DateTime NoticeDate { get; set; }

        public DateTime ResignationDate { get; set; }


        public string ResignationReason { get; set; }




    }
}
