﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class JobType : BaselineBaseEntity
    {
        public string JobTypeTitle { get; set; }

        public string Code { get; set; }
        public string Comments { get; set; }
        public int SortOrder { get; set; }
    }
}
