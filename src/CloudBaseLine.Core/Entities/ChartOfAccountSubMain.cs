﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class ChartOfAccountSubMain : BaselineBaseEntity
    {
        [ForeignKey("ChartOfAccountMain")]
        public long ChartOfAccMainID { get; set; }

        public virtual ChartOfAccountMain ChartOfAccountMain { get; set; }

        public string Sub_code { get; set; }

        public string Sub_title { get; set; }

        

    }
}
