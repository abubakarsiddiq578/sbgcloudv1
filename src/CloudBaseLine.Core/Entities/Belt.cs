﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Belt : BaselineBaseEntity
    {
        [ForeignKey("Zone")]
        public long ZoneId { get; set; }

        public virtual Zone Zone { get; set; }

        public string Belt_Name { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }


    }
}
