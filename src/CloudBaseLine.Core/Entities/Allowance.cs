﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Allowance : BaselineBaseEntity
    {
        public string DocNo { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime AllowanceDate { get; set; }
        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        [ForeignKey("AllowanceType")]
        public long AllowanceTypeId { get; set; }
        public virtual AllowanceType AllowanceType { get; set; }
        public double Amount { get; set; }
        public string Detail { get; set; }
    }
}
