﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace CloudBaseLine.Entities
{
    public class UserCostCenterRight: BaselineBaseEntity
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("CostCenter")]
        public long CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public bool Rights { get; set; }
    }
}
