﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class UserVoucherTypeRight : BaselineBaseEntity
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("VoucherType")]
        public long VoucherTypeId { get; set; }

        public virtual VoucherType VoucherType { get; set; }
        public bool Rights { get; set; }
    }
}
