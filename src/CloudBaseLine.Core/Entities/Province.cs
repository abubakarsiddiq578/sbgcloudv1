﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Province:BaselineBaseEntity
    {
        public string Name { get; set; }

        [ForeignKey("Country")]
        public long CountryId { get; set; }

        public virtual Country Country { get; set; }

    }
}
