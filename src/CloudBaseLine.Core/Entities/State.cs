﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class State:BaselineBaseEntity
    {
        [ForeignKey("Country")]
        public long CountryId { get; set; }

        public virtual Country Country { get; set; }

        public string State_Name { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }

    }
}
