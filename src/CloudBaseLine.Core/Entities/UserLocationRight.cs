﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class UserLocationRight : BaselineBaseEntity
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("LocationInfo")]
        public long LocationId { get; set; }
        public virtual LocationInfo LocationInfo { get; set; }

        public bool Rights { get; set; }
    }
}
