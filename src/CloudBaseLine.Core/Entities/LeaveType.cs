﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class LeaveType : BaselineBaseEntity
    {
        public string LeaveTypeName { get; set; }

        public int LeaveAllowed { get; set; }

        public DateTime? LeaveReset { get; set; }

        public string LeaveDetail { get; set; }
    }
}
