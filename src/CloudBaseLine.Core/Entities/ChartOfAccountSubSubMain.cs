﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class ChartOfAccountSubSubMain : BaselineBaseEntity
    {
        [ForeignKey("ChartOfAccountSubMain")]
        public long SubMainId { get; set; }
        public virtual ChartOfAccountSubMain ChartOfAccountSubMain { get; set; }

        public string Sub_sub_code { get; set; }

        public string Sub_sub_title { get; set; }

        public string Sub_sub_type { get; set; }

        [ForeignKey("GLNote")]
        public long BsNoteId { get; set; }
        public virtual GLNotes GLNote { get; set; }

        [ForeignKey("PLNote")]
        public long PlNoteId { get; set; }

        public virtual GLNotes PLNote { get; set; }

        [ForeignKey("CompanyInfo")]
        public long CompanyId { get; set; }
        public virtual CompanyInfo CompanyInfo { get; set; }

    }
}
