﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class UserMediaContent : BaselineBaseEntity
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public string ImageUrl { get; set; }
    }
}
