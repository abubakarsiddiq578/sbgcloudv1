﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class EmpPromotion : BaselineBaseEntity
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public DateTime PromotionDate { get; set; }

        public string PromotionType { get; set; }

        [ForeignKey("proFrom")]
        public long PromotionFrom { get; set; }

        public virtual EmployeeDesignation proFrom { get; set; }

        [ForeignKey("proTo")]
        public long PromotionTo { get; set; }

        public virtual EmployeeDesignation proTo { get; set; }

        public string Detail { get; set; }
    }
}
