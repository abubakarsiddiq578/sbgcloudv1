﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class EmployeeExit : BaselineBaseEntity
    {

        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public string  Exit_Type { get; set; }

        public DateTime Exit_Date { get; set; }

        public string details { get; set; }


    }
}
