﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.TaxSlabs
{
    public class TaxSlabsDetail : BaselineBaseEntity
    {
        [ForeignKey("TaxSlabsMaster")]
        public long TaxSlabsMasterId { get; set; }
        public virtual TaxSlabsMaster TaxSlabsMaster { get; set; }
        public double ValueFrom { get; set; }
        public double ValueTo { get; set; }
        public double TaxPercentage { get; set; }
        public double Fixed { get; set; }
        public double ApplicableAmount { get; set; }
        public double ValuePerMonth { get; set; }
    }
}
