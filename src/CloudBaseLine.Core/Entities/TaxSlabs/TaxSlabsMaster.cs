﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.TaxSlabs
{
    public class TaxSlabsMaster : BaselineBaseEntity
    {
        public string Code { get; set; }
        public string TaxType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public virtual IEnumerable<TaxSlabsDetail> TaxSlabsDetails { get; set; }
    }
}
