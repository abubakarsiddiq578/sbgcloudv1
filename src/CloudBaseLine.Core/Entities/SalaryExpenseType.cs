﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace CloudBaseLine.Entities
{
    public class SalaryExpenseType: BaselineBaseEntity
    {
        public string SalaryExpType { get; set; }
        public string ApplyValue { get; set; }
        public int Value { get; set; }
        public Boolean SalaryDeduction { get; set; }
        public Boolean IncomeTaxExempted { get; set; }
        public int SortOrder { get; set; }
    }
}
