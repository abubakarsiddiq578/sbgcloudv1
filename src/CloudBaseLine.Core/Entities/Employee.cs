﻿using CloudBaseLine.BaseEntity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Employee : BaselineBaseEntity
    {
        public string Employee_Code { get; set; }

        public string Employee_Name { get; set; }

        public string Father_Name { get; set; }

        public string Gender { get; set; }

        public string Religion { get; set; }

        public DateTime DOB { get; set; }

        public string Martial_Status { get; set; }
        
        public string NIC { get; set; }

        public DateTime CNICExpiryDate { get; set; }

        public string NIC_Place { get; set; }

        public string Qualification { get; set; }

        [ForeignKey("JobType")]
        public long JobTypeId { get; set; }

        public virtual JobType JobType { get; set; }


        [ForeignKey("CostCenter")]
        public long CostCenterId { get; set; }

        public virtual CostCenter CostCenter { get; set; }

        public string Phone { get; set; }

        public string MailingAddress { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }

        [ForeignKey("State")]
        public long StateId { get; set; }
        public virtual State State { get; set; }

        [ForeignKey("RegionId")]
        public long RegionId { get; set; }
        public virtual Region Region { get; set; }

        [ForeignKey("Zone")]
        public long ZoneId { get; set; }
        public virtual Zone Zone { get; set; }

        [ForeignKey("Belt")]
        public long BeltId { get; set; }
        public virtual Belt Belt { get; set; }

        [ForeignKey("City")]
        public long CityId { get; set; }
        public virtual City City { get; set; }

        public string Emergency_No { get; set; }

        public string Passport_No { get; set; }

        public string Insurance_No { get; set; }

        public string Email { get; set; }

        public string Bank_Ac_Name { get; set; }

        public string Iban { get; set; }
        
        public string AccountName { get; set; }

        public string AccountType { get; set; }
        
        public string NTN { get; set; }

        public string BankAccount_No { get; set; }

        public string EobiNo { get; set; }

        public string PessiNo { get; set; }

        public string Social_Security_No { get; set; }

        public DateTime JoiningDate { get; set; }

        public DateTime ConfirmationDate { get; set; }

        public DateTime AttendanceDate { get; set; }

        public DateTime ContractDate { get; set; }

        public DateTime GraduityDate { get; set; } 

        public DateTime ContractEndingDate { get; set; }

        public DateTime Leaving_date { get; set; }

        [ForeignKey("Department")]
        public long Dept_ID { get; set; }

        public virtual Department Department { get; set; }

        [ForeignKey("EmployeeDesignation")]
        public long Desig_ID { get; set; }
        public virtual EmployeeDesignation EmployeeDesignation { get; set; }


        public string ShiftgroupId { get; set; }

        public string PayRoll_Division { get; set; }

        public string AnyDetail { get; set; }

        public bool SalesPerson { get; set; }

        public bool Sale_Order_Person { get; set; }

        public bool IsDailyWages { get; set; }

        public int AlternameEmpNo { get; set; }

        public long EmployeeTypeId { get; set; }

        public string Relation { get; set; }

        public long RefEmployeeId { get; set; }

        public string Family_Code { get; set; }

        public string ID_Remark { get; set; }

        public string Domicile { get; set; }

        public string Blood_Group { get; set; }

        public string Reference { get; set; }

        public string EmployeePicture { get; set; }

        public IEnumerable<Overtime> Overtimes { get; set; }


     


        //public string Salary { get; set; }
        //public string Comments { get; set; }
        //public string EmployeeCommission { get; set; }
        //public long EmpSalaryAccountId { get; set; }
        //public string EmpPicture { get; set; }
        //public long ReceiveableAccontId { get; set; }
        //public string Language { get; set; }
        //public string InReplacementNewCode { get; set; }
        //public string PreviousCode { get; set; }
        //public DateTime Last_Update { get; set; }
        //public int Dept_Division { get; set; }
        //public string Prefix { get; set; }
        //public string Factor_rate { get; set; }
    }
}
