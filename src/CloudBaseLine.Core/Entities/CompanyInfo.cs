﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class CompanyInfo: BaselineBaseEntity
    {

        public string CompanyCode { get; set; }

        public string Prefix { get; set; }

        public string CompanyName { get; set; }

        public string LegalName { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string EMail { get; set; }

        public string webpage { get; set; }

        public string address { get; set; }

        [ForeignKey("CostCenter")]
        public long CostCenterId { get; set; }

        public virtual CostCenter CostCenter { get; set; }

        public long SalesTaxAccountId { get; set; }

        public bool CommercialInvoice { get; set; }

        


    }
}
