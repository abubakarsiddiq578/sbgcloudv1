﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Sample: BaselineBaseEntity
    {
        public string Name { get; set; }

        public int Salary { get; set; }

        public string Designation { get; set; }

        public string CompanyName { get; set; }
    }
}
