﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeGraduity
{
    public class GraduityDetail : BaselineBaseEntity
    {
        [ForeignKey("GraduityMaster")]
        public long GraduityMasterId { get; set; }
        public virtual GraduityMaster GraduityMaster { get; set; }

        [ForeignKey("GraduityConfiguration")]
        public long GraduityConfigurationId { get; set; }
        public virtual GratuityConfiguration GratuityConfiguration { get; set; }

        public string EmployeeCode { get; set; }

        public string EmployeeName { get; set; }

        public string Department { get; set; }
        public string Designation { get; set; }

        public string JoiningDate { get; set; }

        public string LeavingDate { get; set; }

        public string GraduityAmount { get; set; }


    }
}
