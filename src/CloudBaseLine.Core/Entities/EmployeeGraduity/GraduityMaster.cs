﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CloudBaseLine.BaseEntity;
using Newtonsoft.Json;

namespace CloudBaseLine.Entities.EmployeeGraduity
{
    public class GraduityMaster : BaselineBaseEntity
    {
        

        public long Doc_No { get; set; }
        public DateTime Date { get; set; }
        
        
        public virtual IEnumerable<GraduityDetail> GraduityDetails { get; set; }

    }
}
