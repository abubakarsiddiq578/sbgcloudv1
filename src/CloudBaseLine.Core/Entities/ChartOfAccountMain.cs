﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class ChartOfAccountMain : BaselineBaseEntity
    {
        public string Main_code { get; set; }
        public string Main_Title { get; set; }
        public string Main_Type { get; set; }
    }
}
