﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class POSConfigurationDetail : BaselineBaseEntity
    {    
        public string MachineTitle { get; set; }

        public int BankAccountId { get; set; }
        

        [ForeignKey("POSConfiguration")]
        public long POSConfigurationId { get; set; }

        public POSConfiguration POSConfiguration { get; set; }
    }
}
