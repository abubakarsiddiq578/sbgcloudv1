﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Transfer:BaselineBaseEntity
    {
        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }


        public DateTime TransferDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }


        [ForeignKey("TransFrom")]
        public long TransferFromId { get; set; }
        
        public virtual Department TransFrom { get; set; }


        [ForeignKey("TransTo")]
        public long TransferToId { get; set; }

        public virtual Department TransTo{ get; set; }


        public string TransferType{ get; set; }

        public string Detail { get; set; }

    }
}
