﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class AutoSalaryDetail : BaselineBaseEntity
    {
        [ForeignKey("AutoSalary")]
        public long AutoSalaryId { get; set; }

        public virtual AutoSalary AutoSalary { get; set; }

        //[ForeignKey("SalaryExpenseType")]
        public long SalaryItemTypeId { get; set; }

        //public virtual SalaryExpenseType SalaryExpenseType { get; set; }

        public string salaryItemType { get; set; }

        public int Earning { get; set; }

        public int Deduction { get; set; }



    }
}
