﻿using CloudBaseLine.BaseEntity;
using CloudBaseLine.Entities.SalaryGroup;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class SalaryMaster : BaselineBaseEntity
    {
        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("SalaryGroupMaster")]
        public long SalaryGroupMasterId { get; set; }
        public virtual SalaryGroupMaster SalaryGroupMaster { get; set; }
        public double AnnualGrossSalary { get; set; }
        public virtual IEnumerable<SalaryDetail> SalaryDetails { get; set; }
    }
}
