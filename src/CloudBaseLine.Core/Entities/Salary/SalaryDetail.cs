﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class SalaryDetail : BaselineBaseEntity
    {
        [ForeignKey("SalaryMaster")]
        public long SalaryMasterId { get; set; }
        public virtual SalaryMaster SalaryMaster { get; set; }

        public string SalaryItem { get; set; }

        public double Amount { get; set; }
    }
}
