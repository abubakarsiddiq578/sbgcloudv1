﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class DeductionType : BaselineBaseEntity
    {
        public string DeductionTitle { get; set; }

        public long DeductionAmmount { get; set; }

        public string DeductionDetail { get; set; }

        public int SortOrder { get; set; }
    }
}
