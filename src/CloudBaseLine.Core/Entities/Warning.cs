﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Warning : BaselineBaseEntity
    {

        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }

        public DateTime WarningDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public string WarningType { get; set; }


        [ForeignKey("Department")]
        public long WarningById { get; set; }
        public virtual Department Department { get; set; }

        public string Detail { get; set; }

    }
}
