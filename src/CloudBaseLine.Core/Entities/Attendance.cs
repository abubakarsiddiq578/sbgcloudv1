﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Attendance:BaselineBaseEntity
    {

        [ForeignKey("Employee")]
        public long? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("Department")]
        public long? DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        [ForeignKey("EmployeeDesignation")]
        public long? DesignationId { get; set; }
        public virtual EmployeeDesignation EmployeeDesignation { get; set; }

        [ForeignKey("AttendanceTypes")]
        public long? Status { get; set; }

        public virtual AttendanceType AttendanceTypes { get; set; }


        public DateTime AttendanceDate { get; set; }


        public string AttendanceType { get; set; }


        public string Comments { get; set; }
        
    }
}
