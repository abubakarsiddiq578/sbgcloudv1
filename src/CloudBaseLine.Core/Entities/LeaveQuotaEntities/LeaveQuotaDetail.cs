﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.LeaveQuotaEntities
{
    public class LeaveQuotaDetail : BaselineBaseEntity
    {
        [ForeignKey("LeaveQuotaMaster")]
        public long LeaveQuotaMasterId { get; set; }

        public LeaveQuotaMaster LeaveQuotaMaster { get; set; }

        [ForeignKey("LeaveType")]
        public long LeaveTypeId { get; set; }

        public LeaveType LeaveType { get; set; }

        public int AllowedLeaves { get; set; }

        public DateTime ResetDate { get; set; }

        public string Notes { get; set; }
    }
}
