﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CloudBaseLine.Entities.LeaveQuotaEntities
{
    public class LeaveQuotaMaster : BaselineBaseEntity
    {
        public string Title { get; set; }

        public DateTime LeaveQuotaYear { get; set; }

        public ICollection<LeaveQuotaDetail> LeaveQuotaDetails { get; set; }

        public LeaveQuotaMaster()
        {
            LeaveQuotaDetails = new Collection<LeaveQuotaDetail>();
        }
    }
}
