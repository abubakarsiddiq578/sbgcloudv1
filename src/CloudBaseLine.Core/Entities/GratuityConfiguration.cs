﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using CloudBaseLine.BaseEntity;

namespace CloudBaseLine.Entities
{
    public class GratuityConfiguration: BaselineBaseEntity
    {
   
       
        public string EmployeeId { get; set; }
     
        public string SalaryExpenseTypeId { get; set; }
     
       
        public string DepartmentId { get; set; }
        
       
        public string CostCenterId { get; set; }
      

        public long GratuityYearsOfService { get; set; }
        public string GratuityApplicable { get; set; }

        public long GratuityFactor { get; set; }

        public bool RoundingOffGratuity { get; set; }

        public string GratuityDueOn { get; set; }

    }
}
