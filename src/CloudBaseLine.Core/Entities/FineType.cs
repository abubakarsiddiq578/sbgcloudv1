﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class FineType : BaselineBaseEntity
    {
        public string FintTitle { get; set; }

        public long FineAmmount { get; set; }

        public string FineDetail { get; set; }

        public int SortOrder { get; set; }
    }
}
