﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class GLNotes: BaselineBaseEntity
    {
        public string Note_Title { get; set; }

        public string Note_Type { get; set; }

        public int Note_No { get; set; }

        public int SortOrder { get; set; }
    }
}
