﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace CloudBaseLine.Entities
{
    public class VoucherType : BaselineBaseEntity
    {
        public string Voucher_Type_Name { get; set; }
        public string voucher_type { get; set; }
        public string  comments { get; set; }
        public string GL_type { get; set; }
        public int sort_order { get; set; }
        public bool read_only { get; set; }
    }
}
