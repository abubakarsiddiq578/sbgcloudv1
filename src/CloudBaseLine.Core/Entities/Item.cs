﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Item : BaselineBaseEntity
    {

        public string ItemCode { get; set; }

        [ForeignKey("ItemType")]
        public long ItemTypeId { get; set; }
        public virtual ItemType ItemType { get; set; }

        public string Name { get; set; }

        [ForeignKey("Category")]
        public long CategoryId { get; set; }
        public virtual Category Category { get; set; }

        [ForeignKey("SubCategory")]
        public long SubCategoryId { get; set; }
        public virtual SubCategory SubCategory { get; set; }


    }
}
