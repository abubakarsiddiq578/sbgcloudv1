﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class City:BaselineBaseEntity
    {
        [ForeignKey("Province")]
        public long ProvinceId { get; set; }

        public virtual Province Province { get; set;}

        public string CityName { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }
    }
}
