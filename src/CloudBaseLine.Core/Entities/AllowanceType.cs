﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class AllowanceType : BaselineBaseEntity
    {
        public string AllowanceTitle { get; set; }

        public long AllowanceAmmount { get; set; }

        public string AllowanceDetail { get; set; }

        public int SortOrder { get; set; }
    }
}
