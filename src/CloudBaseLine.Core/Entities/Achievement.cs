﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Achievement:BaselineBaseEntity
    {
        public string DocumentNo { get; set; }


        public DateTime DocumentDate { get; set; }


        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }


        

        public DateTime AchievementDate { get; set; }

        public string AchievementType { get; set; }


        public string AchievementDetail { get; set; }

    }
}
