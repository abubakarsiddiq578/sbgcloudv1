﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class EmployeePromotion : BaselineBaseEntity
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public DateTime PromotionDate { get; set; }

        public string PromotionType { get; set; }

        [ForeignKey("ProFrom")]
        public long PromotionFrom { get; set; }

        public virtual EmployeeDesignation ProFrom { get; set; }

        [ForeignKey("ProTo")]
        public long PromotionTo { get; set; }

        public virtual EmployeeDesignation ProTo { get; set; }

        public string Detail { get; set; }
    }
}
