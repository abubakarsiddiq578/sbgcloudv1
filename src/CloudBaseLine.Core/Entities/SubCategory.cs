﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class SubCategory: BaselineBaseEntity
    {

        public string code { get; set; }

        public string Name { get; set; }


        public int SortOrder { get; set; }

        public string Remarks { get; set; }

    }
}
