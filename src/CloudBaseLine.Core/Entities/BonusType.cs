﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class BonusType : BaselineBaseEntity
    {
        public string BonusTitle { get; set; }

        public int BonusPercentage { get; set; }

        public string BonusDetail { get; set; }

        public int SortOrder { get; set; }
    }
}
