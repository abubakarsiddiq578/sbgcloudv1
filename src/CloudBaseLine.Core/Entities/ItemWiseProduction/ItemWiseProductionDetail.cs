﻿using CloudBaseLine.BaseEntity;
using CloudBaseLine.Entities.WorkGroup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text;

namespace CloudBaseLine.Entities.ItemWiseProduction
{
   
    public class ItemWiseProductionDetail : BaselineBaseEntity
    {
        [ForeignKey("ItemWiseProductionMaster")]
        public long ItemWiseProductionMasterId { get; set; }
        public virtual ItemWiseProductionMaster ItemWiseProductionMaster { get; set; }

      
        [ForeignKey("WorkGroupItemDetail")]
        public long WorkGroupItemDetailId { get; set; }
        public virtual WorkGroupItemDetail WorkGroupItemDetail { get; set; }


        public double Quantity { get; set; }
   
        public double ExtraQuantity { get; set; }

        public double RepairQuantity { get; set; }

        public double total { get; set; }


    }
}
