﻿using CloudBaseLine.BaseEntity;
using CloudBaseLine.Entities.WorkGroup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

using System.Text;

namespace CloudBaseLine.Entities.ItemWiseProduction
{
    [JsonObject(IsReference = true)]
    public class ItemWiseProductionMaster : BaselineBaseEntity
    {

        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        public string Remarks { get; set; }

        public long itemTypeId { get; set; }

        public long categoryId { get; set; }

        public long subCategoryId { get; set; }

        [JsonIgnore]
        public virtual ICollection<ItemWiseProductionDetail> ItemWiseProductionDetails { get; set; }

        public ItemWiseProductionMaster()
        {
            ItemWiseProductionDetails = new Collection<ItemWiseProductionDetail>();
        }
    }
}
