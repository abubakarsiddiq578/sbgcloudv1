﻿using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class UserAccountRight : BaselineBaseEntity
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("ChartOfAccountSubSubDetail")]
        public long AccountId { get; set; }
        public virtual ChartOfAccountSubSubDetail ChartOfAccountSubSubDetail { get; set; }

        public bool Rights { get; set; }
    }
}
