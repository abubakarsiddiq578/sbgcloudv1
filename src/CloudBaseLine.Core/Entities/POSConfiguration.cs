﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class POSConfiguration : BaselineBaseEntity
    { 
        public string POSTitle { get; set; }

        public string MachineName { get; set; }

        [ForeignKey("CompanyInfo")]
        public long CompanyInfoId { get; set; }
        public virtual CompanyInfo CompanyInfo { get; set; }


        [ForeignKey("LocationInfo")]
        public long LocationInfoId { get; set; }
        public virtual LocationInfo LocationInfo { get; set; }

        [ForeignKey("CostCenter")]
        public long CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }
        

        //[ForeignKey("CashAccount")]
        public int CashAccountId { get; set; }
        //public virtual CashAccount CashAccount { get; set; }

        public bool DeliveryOption { get; set; }

        public int BankAccountId { get; set; }
        
        

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public IEnumerable<POSConfigurationDetail> POSConfigurationDetails { get; set; }

        


    }
}
