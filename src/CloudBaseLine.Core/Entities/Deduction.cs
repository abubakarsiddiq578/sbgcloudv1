﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class Deduction  : BaselineBaseEntity
    {
        public string DocNo { get; set; }

        [DisplayName("Document Date")]
        public DateTime DocDate { get; set; }

        [DisplayName("Deduction Date")]
        public DateTime DeductionDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("DeductionType")]
        public long DeductionTypeId { get; set; }
        public virtual DeductionType DeductionType { get; set; }

        [DataType(DataType.Text)]
        public long Amount { get; set; }

        [DataType(DataType.MultilineText)]
        public string DeductionDetail { get; set; }
    }
}
