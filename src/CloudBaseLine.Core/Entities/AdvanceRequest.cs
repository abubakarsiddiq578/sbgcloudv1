﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class AdvanceRequest:BaselineBaseEntity
    {
        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }


        public DateTime DeductionDate { get; set; }

        [ForeignKey("Employee")]
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }


        public double Amount { get; set; }


        public string Detail { get; set; }
    }
}
