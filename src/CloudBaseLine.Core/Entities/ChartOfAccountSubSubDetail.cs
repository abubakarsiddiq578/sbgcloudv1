﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class ChartOfAccountSubSubDetail : BaselineBaseEntity
    {
        [ForeignKey("ChartOfAccountSubSubMain")]
        public long MainSubSubId { get; set; }
        public virtual ChartOfAccountSubSubMain ChartOfAccountSubSubMain { get; set; }

        public string DetailCode { get; set; }

        public string DetailTitle { get; set; }

        public int SortOrder { get; set; }

        public float OpeningBalance { get; set; }

        public string AccessLevel { get; set; }

        public DateTime EndDate { get; set; }

        public int Old_Main_Sub_Sub_Id { get; set; }

        public string Old_Detail_Code { get; set; }

        public int? ParentId { get; set; }


    }
}
