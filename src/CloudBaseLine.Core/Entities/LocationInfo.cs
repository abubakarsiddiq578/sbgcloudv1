﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities
{
    public class LocationInfo :BaselineBaseEntity
    {
        public string Location_Code { get; set; }
        public string Location_Name { get; set; }
        public string Comments { get; set; }
        public int Sort_Order { get; set; }
        public string Location_Address { get; set; }
        public string Location_Phone { get; set; }
        public string Location_Fax { get; set; }
        public string Location_Url { get; set; }
        public string Location_Type { get; set; }
        public Boolean RestrictedItems { get; set; }
        public string Mobile_No { get; set; }
        public Boolean AllowMinusStock { get; set; }

        
    }
}
