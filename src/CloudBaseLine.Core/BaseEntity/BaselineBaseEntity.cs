﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.BaseEntity
{
    public class BaselineBaseEntity: FullAuditedEntity<long>
    {
        public bool IsActive { get; set; }
        public int TenantId { get; set; }


    }
}
