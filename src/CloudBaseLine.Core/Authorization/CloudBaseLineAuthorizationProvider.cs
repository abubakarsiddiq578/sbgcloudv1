﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace CloudBaseLine.Authorization
{
    public class CloudBaseLineAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages, L("Pages"));
            context.CreatePermission(PermissionNames.Pages_Administration, L("Adminstration"));

            ////UserRights
          //  context.CreatePermission(PermissionNames.Pages_Hrm_UserRights, L("UserRights"));
          //  context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_View, L("ViewUserRights"));

            //Tenant Permissions
            context.CreatePermission(PermissionNames.Pages_Administration_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Administration_Create_Tenants, L("CreateTenants"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Administration_Edit_Tenants, L("EditTenants"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Administration_Delete_Tenants, L("DeleteTenants"), multiTenancySides: MultiTenancySides.Host);

            //Users Permissions
            context.CreatePermission(PermissionNames.Pages_Administration_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Administration_View_Users, L("ViewUsers"));
            context.CreatePermission(PermissionNames.Pages_Administration_Create_Users, L("CreateUsers"));
            context.CreatePermission(PermissionNames.Pages_Administration_Edit_Users, L("EditUsers"));
            context.CreatePermission(PermissionNames.Pages_Administration_Delete_Users, L("DeleteUsers"));


            //Roles Permissions
            context.CreatePermission(PermissionNames.Pages_Administration_Roles, L("Roles"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Administration_Create_Roles, L("CreateRoles"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Administration_Edit_Roles, L("EditRoles"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Administration_Delete_Roles, L("DeleteRoles"), multiTenancySides: MultiTenancySides.Host);

    

            //Sample Crud 
            context.CreatePermission(PermissionNames.Pages_Administration_Samples, L("Samples"));
            context.CreatePermission(PermissionNames.Pages_Administration_Create_Samples, L("CreateSamples"));
            context.CreatePermission(PermissionNames.Pages_Administration_Edit_Samples, L("EditSamples"));
            context.CreatePermission(PermissionNames.Pages_Administration_Delete_Samples, L("DeleteSamples"));


            context.CreatePermission(PermissionNames.Pages_Administration_Department, L("Department"));
            context.CreatePermission(PermissionNames.Pages_Administration_Create_Department, L("CreateDepartment"));
            context.CreatePermission(PermissionNames.Pages_Administration_Edit_Department, L("EditDepartment"));
            context.CreatePermission(PermissionNames.Pages_Administration_Delete_Department, L("DeleteDepartment"));

            //Department 
            context.CreatePermission(PermissionNames.Pages_Hrm_Departments, L("Departments"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Departments_View, L("ViewDepartments"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Departments_Print, L("PrintDepartments"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Departments_Create, L("CreateDepartments"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Departments_Edit, L("EditDepartments"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Departments_Delete, L("DeleteDepartments"));

            //Achievement 
            context.CreatePermission(PermissionNames.Pages_Hrm_Achievements, L("Achievements"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Achievements_View, L("ViewAchievements"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Achievements_Print, L("PrintAchievements"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Achievements_Create, L("CreateAchievements"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Achievements_Edit, L("EditAchievements"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Achievements_Delete, L("DeleteAchievements"));

            //AdditionLeave
            context.CreatePermission(PermissionNames.Pages_Hrm_AdditionalLeaves, L("AdditionalLeaves"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdditionalLeaves_View, L("ViewAdditionalLeaves"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdditionalLeaves_Print, L("PrintAdditionalLeaves"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdditionalLeaves_Create, L("CreateAdditionalLeaves"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdditionalLeaves_Edit, L("EditAdditionalLeaves"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdditionalLeaves_Delete, L("DeleteAdditionalLeaves"));

            //AdvanceRequest
            context.CreatePermission(PermissionNames.Pages_Hrm_AdvanceRequests, L("AdvanceRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdvanceRequests_View, L("ViewAdvanceRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdvanceRequests_Print, L("PrintAdvanceRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdvanceRequests_Create, L("CreateAdvanceRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdvanceRequests_Edit, L("EditAdvanceRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AdvanceRequests_Delete, L("DeleteAdvanceRequests"));

            //Allowance
            context.CreatePermission(PermissionNames.Pages_Hrm_Allowances, L("Allowances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Allowances_View, L("ViewAllowances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Allowances_Print, L("PrintAllowances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Allowances_Create, L("CreateAllowances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Allowances_Edit, L("EditAllowances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Allowances_Delete, L("DeleteAllowances"));

            //AllowanceType
            context.CreatePermission(PermissionNames.Pages_Hrm_AllowanceTypes, L("AllowanceTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AllowanceTypes_View, L("ViewAllowanceTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AllowanceTypes_Print, L("PrintAllowanceTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AllowanceTypes_Create, L("CreateAllowanceTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AllowanceTypes_Edit, L("EditAllowanceTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_AllowanceTypes_Delete, L("DeleteAllowanceTypes"));

            //Attendance
            context.CreatePermission(PermissionNames.Pages_Hrm_Attendances, L("Attendances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Attendances_View, L("ViewAttendances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Attendances_Print, L("PrintAttendances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Attendances_Create, L("CreateAttendances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Attendances_Edit, L("EditAttendances"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Attendances_Delete, L("DeleteAttendances"));

            //Belt
            context.CreatePermission(PermissionNames.Pages_Hrm_Belts, L("Belts"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Belts_View, L("ViewBelts"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Belts_Print, L("PrintBelts"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Belts_Create, L("CreateBelts"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Belts_Edit, L("EditBelts"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Belts_Delete, L("DeleteBelts"));

            //Bonus
            context.CreatePermission(PermissionNames.Pages_Hrm_Bonuses, L("Bonuses"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Bonuses_View, L("ViewBonuses"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Bonuses_Print, L("PrintBonuses"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Bonuses_Create, L("CreateBonuses"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Bonuses_Edit, L("EditBonuses"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Bonuses_Delete, L("DeleteBonuses"));

            //BonusType
            context.CreatePermission(PermissionNames.Pages_Hrm_BonusTypes, L("BonusTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_BonusTypes_View, L("ViewBonusTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_BonusTypes_Print, L("PrintBonusTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_BonusTypes_Create, L("CreateBonusTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_BonusTypes_Edit, L("EditBonusTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_BonusTypes_Delete, L("DeleteBonusTypes"));

            //Category
            context.CreatePermission(PermissionNames.Pages_Hrm_Categories, L("Categories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Categories_View, L("ViewCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Categories_Print, L("PrintCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Categories_Create, L("CreateCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Categories_Edit, L("EditCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Categories_Delete, L("DeleteCategories"));

            //ChartOfAccountMain
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountMains, L("ChartOfAccountMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountMains_View, L("ViewChartOfAccountMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountMains_Print, L("PrintChartOfAccountMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountMains_Create, L("CreateChartOfAccountMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountMains_Edit, L("EditChartOfAccountMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountMains_Delete, L("DeleteChartOfAccountMains"));

                //ChartOfAccountSubMain
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains, L("ChartOfAccountSubMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_View, L("ViewChartOfAccountSubMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Print, L("PrintChartOfAccountSubMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Create, L("CreateChartOfAccountSubMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Edit, L("EditChartOfAccountSubMains"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Delete, L("DeleteChartOfAccountSubMains"));

            //ChartOfAccountSubSubDetail
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails, L("ChartOfAccountSubSubDetails"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_View, L("ViewChartOfAccountSubSubDetails"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_Print, L("PrintChartOfAccountSubSubDetails"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_Create, L("CreateChartOfAccountSubSubDetails"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_Edit, L("EditChartOfAccountSubSubDetails"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_Delete, L("DeleteChartOfAccountSubSubDetails"));

            //ChartOfAccountSubMain
          //  context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains, L("ChartOfAccountSubMains"));
           // context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_View, L("ViewChartOfAccountSubMains"));
           // context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Print, L("PrintChartOfAccountSubMains"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Create, L("CreateChartOfAccountSubMains"));
           // context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Edit, L("EditChartOfAccountSubMains"));
           // context.CreatePermission(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Delete, L("DeleteChartOfAccountSubMains"));

            //City
            context.CreatePermission(PermissionNames.Pages_Hrm_Cities, L("Cities"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Cities_View, L("ViewCities"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Cities_Print, L("PrintCities"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Cities_Create, L("CreateCities"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Cities_Edit, L("EditCities"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Cities_Delete, L("DeleteCities"));

            //Company
            context.CreatePermission(PermissionNames.Pages_Hrm_Companies, L("Companies"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Companies_View, L("ViewCompanies"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Companies_Print, L("PrintCompanies"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Companies_Create, L("CreateCompanies"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Companies_Edit, L("EditCompanies"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Companies_Delete, L("DeleteCompanies"));

            //CompanyInformation
            context.CreatePermission(PermissionNames.Pages_Hrm_CompanyInformations, L("CompanyInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CompanyInformations_View, L("ViewCompanyInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CompanyInformations_Print, L("PrintCompanyInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CompanyInformations_Create, L("CreateCompanyInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CompanyInformations_Edit, L("EditCompanyInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CompanyInformations_Delete, L("DeleteCompanyInformations"));

            //Configuration
            context.CreatePermission(PermissionNames.Pages_Hrm_Configurations, L("Configurations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Configurations_View, L("ViewConfigurations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Configurations_Print, L("PrintConfigurations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Configurations_Create, L("CreateConfigurations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Configurations_Edit, L("EditConfigurations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Configurations_Delete, L("DeleteConfigurations"));


            //CostCenter
            context.CreatePermission(PermissionNames.Pages_Hrm_CostCenters, L("CostCenters"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CostCenters_View, L("ViewCostCenters"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CostCenters_Print, L("PrintCostCenters"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CostCenters_Create, L("CreateCostCenters"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CostCenters_Edit, L("EditCostCenters"));
            context.CreatePermission(PermissionNames.Pages_Hrm_CostCenters_Delete, L("DeleteCostCenters"));

            //Country
            context.CreatePermission(PermissionNames.Pages_Hrm_Countries, L("Countries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Countries_View, L("ViewCountries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Countries_Print, L("PrintCountries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Countries_Create, L("CreateCountries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Countries_Edit, L("EditCountries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Countries_Delete, L("DeleteCountries"));

            //Deduction
            context.CreatePermission(PermissionNames.Pages_Hrm_Deductions, L("Deductions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Deductions_View, L("ViewDeductions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Deductions_Print, L("PrintDeductions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Deductions_Create, L("CreateDeductions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Deductions_Edit, L("EditDeductions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Deductions_Delete, L("DeleteDeductions"));

            //DeductionType
            context.CreatePermission(PermissionNames.Pages_Hrm_DeductionTypes, L("DeductionTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_DeductionTypes_View, L("ViewDeductionTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_DeductionTypes_Print, L("PrintDeductionTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_DeductionTypes_Create, L("CreateDeductionTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_DeductionTypes_Edit, L("EditDeductionTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_DeductionTypes_Delete, L("DeleteDeductionTypes"));

            //EmployeeDesignation
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeDesignations, L("EmployeeDesignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeDesignations_View, L("ViewEmployeeDesignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeDesignations_Print, L("PrintEmployeeDesignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeDesignations_Create, L("CreateEmployeeDesignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeDesignations_Edit, L("EditEmployeeDesignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeDesignations_Delete, L("DeleteEmployeeDesignations"));

            //EmployeeInfo
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeInfos, L("EmployeeInfos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeInfos_View, L("ViewEmployeeInfos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeInfos_Print, L("PrintEmployeeInfos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeInfos_Create, L("CreateEmployeeInfos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeInfos_Edit, L("EditEmployeeInfos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeInfos_Delete, L("DeleteEmployeeInfos"));


            //EmployeeList
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeLists, L("EmployeeLists"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeLists_View, L("ViewEmployeeLists"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeLists_Print, L("PrintEmployeeLists"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeLists_Create, L("CreateEmployeeLists"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeLists_Edit, L("EditEmployeeLists"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeLists_Delete, L("DeleteEmployeeLists"));

            //EmployeePromo
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeePromos, L("EmployeePromos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeePromos_View, L("ViewEmployeePromos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeePromos_Print, L("PrintEmployeePromos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeePromos_Create, L("CreateEmployeePromos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeePromos_Edit, L("EditEmployeePromos"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeePromos_Delete, L("DeleteEmployeePromos"));

            //EmployeeExit
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeExits, L("EmployeeExits"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeExits_View, L("ViewEmployeeExits"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeExits_Print, L("PrintEmployeeExits"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeExits_Create, L("CreateEmployeeExits"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeExits_Edit, L("EditEmployeeExits"));
            context.CreatePermission(PermissionNames.Pages_Hrm_EmployeeExits_Delete, L("DeleteEmployeeExits"));

            //Fine
            context.CreatePermission(PermissionNames.Pages_Hrm_Fines, L("Fines"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Fines_View, L("ViewFines"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Fines_Print, L("PrintFines"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Fines_Create, L("CreateFines"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Fines_Edit, L("EditFines"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Fines_Delete, L("DeleteFines"));

            //FineType
            context.CreatePermission(PermissionNames.Pages_Hrm_FineTypes, L("FineTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_FineTypes_View, L("ViewFineTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_FineTypes_Print, L("PrintFineTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_FineTypes_Create, L("CreateFineTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_FineTypes_Edit, L("EditFineTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_FineTypes_Delete, L("DeleteFineTypes"));

            //Holiday
            context.CreatePermission(PermissionNames.Pages_Hrm_Holidays, L("Holidays"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Holidays_View, L("ViewHolidays"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Holidays_Print, L("PrintHolidays"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Holidays_Create, L("CreateHolidays"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Holidays_Edit, L("EditHolidays"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Holidays_Delete, L("DeleteHolidays"));

            //Item
            context.CreatePermission(PermissionNames.Pages_Hrm_Items, L("Items"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Items_View, L("ViewItems"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Items_Print, L("PrintItems"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Items_Create, L("CreateItems"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Items_Edit, L("EditItems"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Items_Delete, L("DeleteItems"));

            //ItemType
            context.CreatePermission(PermissionNames.Pages_Hrm_ItemTypes, L("ItemTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ItemTypes_View, L("ViewItemTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ItemTypes_Print, L("PrintItemTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ItemTypes_Create, L("CreateItemTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ItemTypes_Edit, L("EditItemTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ItemTypes_Delete, L("DeleteItemTypes"));

            //JobType
            context.CreatePermission(PermissionNames.Pages_Hrm_JobTypes, L("JobTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_JobTypes_View, L("ViewJobTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_JobTypes_Print, L("PrintJobTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_JobTypes_Create, L("CreateJobTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_JobTypes_Edit, L("EditJobTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_JobTypes_Delete, L("DeleteJobTypes"));

            //LeaveQuota
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveQuotas, L("LeaveQuotas"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveQuotas_View, L("ViewLeaveQuotas"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveQuotas_Print, L("PrintLeaveQuotas"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveQuotas_Create, L("CreateLeaveQuotas"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveQuotas_Edit, L("EditLeaveQuotas"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveQuotas_Delete, L("DeleteLeaveQuotas"));

            //LeaveRequest
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveRequests, L("LeaveRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveRequests_View, L("ViewLeaveRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveRequests_Print, L("PrintLeaveRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveRequests_Create, L("CreateLeaveRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveRequests_Edit, L("EditLeaveRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveRequests_Delete, L("DeleteLeaveRequests"));

            //LeaveType
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveTypes, L("LeaveTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveTypes_View, L("ViewLeaveTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveTypes_Print, L("PrintLeaveTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveTypes_Create, L("CreateLeaveTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveTypes_Edit, L("EditLeaveTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LeaveTypes_Delete, L("DeleteLeaveTypes"));

            //LoanRequest
            context.CreatePermission(PermissionNames.Pages_Hrm_LoanRequests, L("LoanRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LoanRequests_View, L("ViewLoanRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LoanRequests_Print, L("PrintLoanRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LoanRequests_Create, L("CreateLoanRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LoanRequests_Edit, L("EditLoanRequests"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LoanRequests_Delete, L("DeleteLoanRequests"));

            //LocationInformation
            context.CreatePermission(PermissionNames.Pages_Hrm_LocationInformations, L("LocationInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LocationInformations_View, L("ViewLocationInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LocationInformations_Print, L("PrintLocationInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LocationInformations_Create, L("CreateLocationInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LocationInformations_Edit, L("EditLocationInformations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_LocationInformations_Delete, L("DeleteLocationInformations"));

            //Overtime
            context.CreatePermission(PermissionNames.Pages_Hrm_Overtimes, L("Overtimes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Overtimes_View, L("ViewOvertimes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Overtimes_Print, L("PrintOvertimes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Overtimes_Create, L("CreateOvertimes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Overtimes_Edit, L("EditOvertimes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Overtimes_Delete, L("DeleteOvertimes"));

            //POSs
            context.CreatePermission(PermissionNames.Pages_Hrm_POSs, L("POSss"));
            context.CreatePermission(PermissionNames.Pages_Hrm_POSs_View, L("ViewPOSss"));
            context.CreatePermission(PermissionNames.Pages_Hrm_POSs_Print, L("PrintPOSss"));
            context.CreatePermission(PermissionNames.Pages_Hrm_POSs_Create, L("CreatePOSss"));
            context.CreatePermission(PermissionNames.Pages_Hrm_POSs_Edit, L("EditPOSss"));
            context.CreatePermission(PermissionNames.Pages_Hrm_POSs_Delete, L("DeletePOSss"));

            //ProductionRate
            context.CreatePermission(PermissionNames.Pages_Hrm_ProductionRates, L("ProductionRates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ProductionRates_View, L("ViewProductionRates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ProductionRates_Print, L("PrintProductionRates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ProductionRates_Create, L("CreateProductionRates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ProductionRates_Edit, L("EditProductionRates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_ProductionRates_Delete, L("DeleteProductionRates"));

            //Promotion
            context.CreatePermission(PermissionNames.Pages_Hrm_Promotions, L("Promotions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Promotions_View, L("ViewPromotions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Promotions_Print, L("PrintPromotions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Promotions_Create, L("CreatePromotions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Promotions_Edit, L("EditPromotions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Promotions_Delete, L("DeletePromotions"));

            //Province
            context.CreatePermission(PermissionNames.Pages_Hrm_Provinces, L("Provinces"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Provinces_View, L("ViewProvinces"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Provinces_Print, L("PrintProvinces"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Provinces_Create, L("CreateProvinces"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Provinces_Edit, L("EditProvinces"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Provinces_Delete, L("DeleteProvinces"));

            //Region
            context.CreatePermission(PermissionNames.Pages_Hrm_Regions, L("Regions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Regions_View, L("ViewRegions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Regions_Print, L("PrintRegions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Regions_Create, L("CreateRegions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Regions_Edit, L("EditRegions"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Regions_Delete, L("DeleteRegions"));

            //Resignation
            context.CreatePermission(PermissionNames.Pages_Hrm_Resignations, L("Resignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Resignations_View, L("ViewResignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Resignations_Print, L("PrintResignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Resignations_Create, L("CreateResignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Resignations_Edit, L("EditResignations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Resignations_Delete, L("DeleteResignations"));

            //Salary
            context.CreatePermission(PermissionNames.Pages_Hrm_Salaries, L("Salaries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Salaries_View, L("ViewSalaries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Salaries_Print, L("PrintSalaries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Salaries_Create, L("CreateSalaries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Salaries_Edit, L("EditSalaries"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Salaries_Delete, L("DeleteSalaries"));

            //SalaryGroup
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryGroups, L("SalaryGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryGroups_View, L("ViewSalaryGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryGroups_Print, L("PrintSalaryGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryGroups_Create, L("CreateSalaryGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryGroups_Edit, L("EditSalaryGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryGroups_Delete, L("DeleteSalaryGroups"));

            //SalaryTypes
            //SalaryExpenseTypes
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryExpenseTypes, L("SalaryExpenseTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryExpenseTypes_View, L("ViewSalaryExpenseTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryExpenseTypes_Print, L("PrintSalaryExpenseTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryExpenseTypes_Create, L("CreateSalaryExpenseTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryExpenseTypes_Edit, L("EditSalaryExpenseTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryExpenseTypes_Delete, L("DeleteSalaryExpenseTypes"));

            //SalaryWizard
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryWizards, L("SalaryWizards"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryWizards_View, L("ViewSalaryWizards"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryWizards_Print, L("PrintSalaryWizards"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryWizards_Create, L("CreateSalaryWizards"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryWizards_Edit, L("EditSalaryWizards"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SalaryWizards_Delete, L("DeleteSalaryWizards"));

            //Sample
            context.CreatePermission(PermissionNames.Pages_Hrm_Samples, L("Samples"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Samples_View, L("ViewSamples"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Samples_Print, L("PrintSamples"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Samples_Create, L("CreateSamples"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Samples_Edit, L("EditSamples"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Samples_Delete, L("DeleteSamples"));

            //State
            context.CreatePermission(PermissionNames.Pages_Hrm_States, L("States"));
            context.CreatePermission(PermissionNames.Pages_Hrm_States_View, L("ViewStates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_States_Print, L("PrintStates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_States_Create, L("CreateStates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_States_Edit, L("EditStates"));
            context.CreatePermission(PermissionNames.Pages_Hrm_States_Delete, L("DeleteStates"));

            //SubCategory
            context.CreatePermission(PermissionNames.Pages_Hrm_SubCategories, L("SubCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SubCategories_View, L("ViewSubCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SubCategories_Print, L("PrintSubCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SubCategories_Create, L("CreateSubCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SubCategories_Edit, L("EditSubCategories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_SubCategories_Delete, L("DeleteSubCategories"));

            //TaxSlab
            context.CreatePermission(PermissionNames.Pages_Hrm_TaxSlabs, L("TaxSlabs"));
            context.CreatePermission(PermissionNames.Pages_Hrm_TaxSlabs_View, L("ViewTaxSlabs"));
            context.CreatePermission(PermissionNames.Pages_Hrm_TaxSlabs_Print, L("PrintTaxSlabs"));
            context.CreatePermission(PermissionNames.Pages_Hrm_TaxSlabs_Create, L("CreateTaxSlabs"));
            context.CreatePermission(PermissionNames.Pages_Hrm_TaxSlabs_Edit, L("EditTaxSlabs"));
            context.CreatePermission(PermissionNames.Pages_Hrm_TaxSlabs_Delete, L("DeleteTaxSlabs"));

            //Termination
            context.CreatePermission(PermissionNames.Pages_Hrm_Terminations, L("Terminations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Terminations_View, L("ViewTerminations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Terminations_Print, L("PrintTerminations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Terminations_Create, L("CreateTerminations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Terminations_Edit, L("EditTerminations"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Terminations_Delete, L("DeleteTerminations"));

            //Territory
            context.CreatePermission(PermissionNames.Pages_Hrm_Territories, L("Territories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Territories_View, L("ViewTerritories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Territories_Print, L("PrintTerritories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Territories_Create, L("CreateTerritories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Territories_Edit, L("EditTerritories"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Territories_Delete, L("DeleteTerritories"));

            //Transfer
            context.CreatePermission(PermissionNames.Pages_Hrm_Transfers, L("Transfers"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Transfers_View, L("ViewTransfers"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Transfers_Print, L("PrintTransfers"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Transfers_Create, L("CreateTransfers"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Transfers_Edit, L("EditTransfers"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Transfers_Delete, L("DeleteTransfers"));

            //UserAccountRight
            context.CreatePermission(PermissionNames.Pages_Hrm_UserAccountRights, L("UserAccountRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserAccountRights_View, L("ViewUserAccountRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserAccountRights_Print, L("PrintUserAccountRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserAccountRights_Create, L("CreateUserAccountRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserAccountRights_Edit, L("EditUserAccountRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserAccountRights_Delete, L("DeleteUserAccountRights"));

            //UserCompanyRight
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCompanyRights, L("UserCompanyRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCompanyRights_View, L("ViewUserCompanyRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCompanyRights_Print, L("PrintUserCompanyRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCompanyRights_Create, L("CreateUserCompanyRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCompanyRights_Edit, L("EditUserCompanyRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCompanyRights_Delete, L("DeleteUserCompanyRights"));

            //UserCostCenterRight
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCostCenterRights, L("UserCostCenterRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCostCenterRights_View, L("ViewUserCostCenterRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCostCenterRights_Print, L("PrintUserCostCenterRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCostCenterRights_Create, L("CreateUserCostCenterRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCostCenterRights_Edit, L("EditUserCostCenterRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserCostCenterRights_Delete, L("DeleteUserCostCenterRights"));

            //UserLocationRight
            context.CreatePermission(PermissionNames.Pages_Hrm_UserLocationRights, L("UserLocationRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserLocationRights_View, L("ViewUserLocationRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserLocationRights_Print, L("PrintUserLocationRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserLocationRights_Create, L("CreateUserLocationRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserLocationRights_Edit, L("EditUserLocationRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserLocationRights_Delete, L("DeleteUserLocationRights"));

            //UserMediaContent
            context.CreatePermission(PermissionNames.Pages_Hrm_UserMediaContents, L("UserMediaContents"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserMediaContents_View, L("ViewUserMediaContents"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserMediaContents_Print, L("PrintUserMediaContents"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserMediaContents_Create, L("CreateUserMediaContents"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserMediaContents_Edit, L("EditUserMediaContents"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserMediaContents_Delete, L("DeleteUserMediaContents"));

            //UserVoucherTypeRight
            context.CreatePermission(PermissionNames.Pages_Hrm_UserVoucherTypeRights, L("UserVoucherTypeRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserVoucherTypeRights_View, L("ViewUserVoucherTypeRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserVoucherTypeRights_Print, L("PrintUserVoucherTypeRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserVoucherTypeRights_Create, L("CreateUserVoucherTypeRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserVoucherTypeRights_Edit, L("EditUserVoucherTypeRights"));
            context.CreatePermission(PermissionNames.Pages_Hrm_UserVoucherTypeRights_Delete, L("DeleteUserVoucherTypeRights"));

            //VoucherType
            context.CreatePermission(PermissionNames.Pages_Hrm_VoucherTypes, L("VoucherTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_VoucherTypes_View, L("ViewVoucherTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_VoucherTypes_Print, L("PrintVoucherTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_VoucherTypes_Create, L("CreateVoucherTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_VoucherTypes_Edit, L("EditVoucherTypes"));
            context.CreatePermission(PermissionNames.Pages_Hrm_VoucherTypes_Delete, L("DeleteVoucherTypes"));

            //Warning
            context.CreatePermission(PermissionNames.Pages_Hrm_Warnings, L("Warnings"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Warnings_View, L("ViewWarnings"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Warnings_Print, L("PrintWarnings"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Warnings_Create, L("CreateWarnings"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Warnings_Edit, L("EditWarnings"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Warnings_Delete, L("DeleteWarnings"));

            //WorkGroup
            context.CreatePermission(PermissionNames.Pages_Hrm_WorkGroups, L("WorkGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_WorkGroups_View, L("ViewWorkGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_WorkGroups_Print, L("PrintWorkGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_WorkGroups_Create, L("CreateWorkGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_WorkGroups_Edit, L("EditWorkGroups"));
            context.CreatePermission(PermissionNames.Pages_Hrm_WorkGroups_Delete, L("DeleteWorkGroups"));

            //Zone
            context.CreatePermission(PermissionNames.Pages_Hrm_Zones, L("Zones"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Zones_View, L("ViewZones"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Zones_Print, L("PrintZones"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Zones_Create, L("CreateZones"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Zones_Edit, L("EditZones"));
            context.CreatePermission(PermissionNames.Pages_Hrm_Zones_Delete, L("DeleteZones"));

            //Security/Company
            context.CreatePermission(PermissionNames.Pages_Security_Companies, L("Companies"));
            context.CreatePermission(PermissionNames.Pages_Security_Companies_View, L("ViewCompanies"));
            context.CreatePermission(PermissionNames.Pages_Security_Companies_Create, L("CreateCompanies"));

            //Security/CostCenter
            context.CreatePermission(PermissionNames.Pages_Security_CostCenters, L("CostCenters"));
            context.CreatePermission(PermissionNames.Pages_Security_CostCenters_View, L("ViewCostCenters"));
            context.CreatePermission(PermissionNames.Pages_Security_CostCenters_Create, L("CreateCostCenters"));

            //Security/Account
            context.CreatePermission(PermissionNames.Pages_Security_Accounts, L("Accounts"));
            context.CreatePermission(PermissionNames.Pages_Security_Accounts_View, L("ViewAccounts"));
            context.CreatePermission(PermissionNames.Pages_Security_Accounts_Create, L("CreateAccounts"));

            //Security/VoucherType
            context.CreatePermission(PermissionNames.Pages_Security_VoucherTypes, L("VoucherTypes"));
            context.CreatePermission(PermissionNames.Pages_Security_VoucherTypes_View, L("ViewVoucherTypes"));
            context.CreatePermission(PermissionNames.Pages_Security_VoucherTypes_Create, L("CreateVoucherTypes"));

            //Security/Locations
            context.CreatePermission(PermissionNames.Pages_Security_Locations, L("Locations"));
            context.CreatePermission(PermissionNames.Pages_Security_Locations_View, L("ViewLocations"));
            context.CreatePermission(PermissionNames.Pages_Security_Locations_Create, L("CreateLocations"));



            ////Users

            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Users, L("Users"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Users_View, L("ViewUsers"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Users_Create, L("CreateUsers"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Users_Edit, L("EditUsers"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Users_Delete, L("DeleteUsers"));


            ////Roles
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Roles, L("Roles"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Roles_View, L("ViewRoles"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Roles_Create, L("CreateRoles"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Roles_Edit, L("EditRoles"));
            //context.CreatePermission(PermissionNames.Pages_Hrm_UserRights_Roles_Delete, L("DeleteRoles"));







            
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, CloudBaseLineConsts.LocalizationSourceName);
        }
    }
}
