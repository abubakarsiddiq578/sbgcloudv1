﻿namespace CloudBaseLine.Authorization
{
    public static class PermissionNames
    {
        public const string Pages = "Pages";
        public const string Pages_Administration = "Pages.Administration";

        //Permission for the Tenant
        public const string Pages_Administration_Tenants = "Pages.Administration.Tenants";
        public const string Pages_Administration_Create_Tenants = "Pages.Administration.Create_Tenants";
        public const string Pages_Administration_Edit_Tenants = "Pages.Administration.Edit_Tenants";
        public const string Pages_Administration_Delete_Tenants = "Pages.Administration.Delete_Tenants";

        //Permissions for the Users
        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_View_Users = "Pages.Administration.View_Users";
        public const string Pages_Administration_Create_Users = "Pages.Administration.Create_Users";
        public const string Pages_Administration_Edit_Users = "Pages.Administration.Edit_Users";
        public const string Pages_Administration_Delete_Users = "Pages.Administration.Delete_Users";

        //Permissions for the Roles
        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Create_Roles = "Pages.Administration.Create_Roles";
        public const string Pages_Administration_Edit_Roles = "Pages.Administration.Edit_Roles";
        public const string Pages_Administration_Delete_Roles = "Pages.Administration.Delete_Roles";

    

        //Sample Crud
        public const string Pages_Administration_Samples = "Pages.Administration.Samples";
        public const string Pages_Administration_Create_Samples = "Pages.Administration.Create_Samples";
        public const string Pages_Administration_Edit_Samples = "Pages.Administration.Edit_Samples";
        public const string Pages_Administration_Delete_Samples = "Pages.Administration.Delete_Samples";
        
        public const string Pages_Administration_Department = "Pages.Administration.Department";
        public const string Pages_Administration_Create_Department = "Pages.Administration.Create_Department";
        public const string Pages_Administration_Edit_Department = "Pages.Administration.Edit_Department";
        public const string Pages_Administration_Delete_Department = "Pages.Administration.Delete_Department";
        //Department
        public const string Pages_Hrm_Departments = "Pages.Hrm.Department.Department";
        public const string Pages_Hrm_Departments_View = "Pages.Hrm.Department.View_Department";
        public const string Pages_Hrm_Departments_Print = "Pages.Hrm.Department.Print_Department";
        public const string Pages_Hrm_Departments_Create = "Pages.Hrm.Department.Create_Department";
        public const string Pages_Hrm_Departments_Edit = "Pages.Hrm.Department.Edit_Department";
        public const string Pages_Hrm_Departments_Delete = "Pages.Hrm.Department.Delete_Department";



        //Achievement
        public const string Pages_Hrm_Achievements = "Pages.Hrm.Achievement.Achievement";
        public const string Pages_Hrm_Achievements_View = "Pages.Hrm.Achievement.View_Achievement";
        public const string Pages_Hrm_Achievements_Print = "Pages.Hrm.Achievement.Print_Achievement";
        public const string Pages_Hrm_Achievements_Create = "Pages.Hrm.Achievement.Create_Achievement";
        public const string Pages_Hrm_Achievements_Edit = "Pages.Hrm.Achievement.Edit_Achievement";
        public const string Pages_Hrm_Achievements_Delete = "Pages.Hrm.Achievement.Delete_Achievement";

        //AdditionalLeave
        public const string Pages_Hrm_AdditionalLeaves = "Pages.Hrm.AdditionalLeave.AdditionalLeave";
        public const string Pages_Hrm_AdditionalLeaves_View = "Pages.Hrm.AdditionalLeave.View_AdditionalLeave";
        public const string Pages_Hrm_AdditionalLeaves_Print = "Pages.Hrm.AdditionalLeave.Print_AdditionalLeave";
        public const string Pages_Hrm_AdditionalLeaves_Create = "Pages.Hrm.AdditionalLeave.Create_AdditionalLeave";
        public const string Pages_Hrm_AdditionalLeaves_Edit = "Pages.Hrm.AdditionalLeave.Edit_AdditionalLeave";
        public const string Pages_Hrm_AdditionalLeaves_Delete = "Pages.Hrm.AdditionalLeave.Delete_AdditionalLeave";

        //AdvanceRequest
        public const string Pages_Hrm_AdvanceRequests = "Pages.Hrm.AdvanceRequest.AdvanceRequest";
        public const string Pages_Hrm_AdvanceRequests_View = "Pages.Hrm.AdvanceRequest.View_AdvanceRequest";
        public const string Pages_Hrm_AdvanceRequests_Print = "Pages.Hrm.AdvanceRequest.Print_AdvanceRequest";
        public const string Pages_Hrm_AdvanceRequests_Create = "Pages.Hrm.AdvanceRequest.Create_AdvanceRequest";
        public const string Pages_Hrm_AdvanceRequests_Edit = "Pages.Hrm.AdvanceRequest.Edit_AdvanceRequest";
        public const string Pages_Hrm_AdvanceRequests_Delete = "Pages.Hrm.AdvanceRequest.Delete_AdvanceRequest";

        //Allowance
        public const string Pages_Hrm_Allowances = "Pages.Hrm.Allowance.Allowance";
        public const string Pages_Hrm_Allowances_View = "Pages.Hrm.Allowance.View_Allowance";
        public const string Pages_Hrm_Allowances_Print = "Pages.Hrm.Allowance.Print_Allowance";
        public const string Pages_Hrm_Allowances_Create = "Pages.Hrm.Allowance.Create_Allowance";
        public const string Pages_Hrm_Allowances_Edit = "Pages.Hrm.Allowance.Edit_Allowance";
        public const string Pages_Hrm_Allowances_Delete = "Pages.Hrm.Allowance.Delete_Allowance";

        //AllowanceType
        public const string Pages_Hrm_AllowanceTypes = "Pages.Hrm.AllowanceType.AllowanceType";
        public const string Pages_Hrm_AllowanceTypes_View = "Pages.Hrm.AllowanceType.View_AllowanceType";
        public const string Pages_Hrm_AllowanceTypes_Print = "Pages.Hrm.AllowanceType.Print_AllowanceType";
        public const string Pages_Hrm_AllowanceTypes_Create = "Pages.Hrm.AllowanceType.Create_AllowanceType";
        public const string Pages_Hrm_AllowanceTypes_Edit = "Pages.Hrm.AllowanceType.Edit_AllowanceType";
        public const string Pages_Hrm_AllowanceTypes_Delete = "Pages.Hrm.AllowanceType.Delete_AllowanceType";

        //Attendance
        public const string Pages_Hrm_Attendances = "Pages.Hrm.Attendance.Attendance";
        public const string Pages_Hrm_Attendances_View = "Pages.Hrm.Attendance.View_Attendance";
        public const string Pages_Hrm_Attendances_Print = "Pages.Hrm.Attendance.Print_Attendance";
        public const string Pages_Hrm_Attendances_Create = "Pages.Hrm.Attendance.Create_Attendance";
        public const string Pages_Hrm_Attendances_Edit = "Pages.Hrm.Attendance.Edit_Attendance";
        public const string Pages_Hrm_Attendances_Delete = "Pages.Hrm.Attendance.Delete_Attendance";

        //Belt
        public const string Pages_Hrm_Belts = "Pages.Hrm.Belt.Belt";
        public const string Pages_Hrm_Belts_View = "Pages.Hrm.Belt.View_Belt";
        public const string Pages_Hrm_Belts_Print = "Pages.Hrm.Belt.Print_Belt";
        public const string Pages_Hrm_Belts_Create = "Pages.Hrm.Belt.Create_Belt";
        public const string Pages_Hrm_Belts_Edit = "Pages.Hrm.Belt.Edit_Belt";
        public const string Pages_Hrm_Belts_Delete = "Pages.Hrm.Belt.Delete_Belt";

        //Bonus
        public const string Pages_Hrm_Bonuses = "Pages.Hrm.Bonus.Bonus";
        public const string Pages_Hrm_Bonuses_View = "Pages.Hrm.Bonus.View_Bonus";
        public const string Pages_Hrm_Bonuses_Print = "Pages.Hrm.Bonus.Print_Bonus";
        public const string Pages_Hrm_Bonuses_Create = "Pages.Hrm.Bonus.Create_Bonus";
        public const string Pages_Hrm_Bonuses_Edit = "Pages.Hrm.Bonus.Edit_Bonus";
        public const string Pages_Hrm_Bonuses_Delete = "Pages.Hrm.Bonus.Delete_Bonus";

        //BonusType
        public const string Pages_Hrm_BonusTypes = "Pages.Hrm.BonusType.BonusType";
        public const string Pages_Hrm_BonusTypes_View = "Pages.Hrm.BonusType.View_BonusType";
        public const string Pages_Hrm_BonusTypes_Print = "Pages.Hrm.BonusType.Print_BonusType";
        public const string Pages_Hrm_BonusTypes_Create = "Pages.Hrm.BonusType.Create_BonusType";
        public const string Pages_Hrm_BonusTypes_Edit = "Pages.Hrm.BonusType.Edit_BonusType";
        public const string Pages_Hrm_BonusTypes_Delete = "Pages.Hrm.BonusType.Delete_BonusType";

        //Category
        public const string Pages_Hrm_Categories = "Pages.Hrm.Category.Category";
        public const string Pages_Hrm_Categories_View = "Pages.Hrm.Category.View_Category";
        public const string Pages_Hrm_Categories_Print = "Pages.Hrm.Category.Print_Category";
        public const string Pages_Hrm_Categories_Create = "Pages.Hrm.Category.Create_Category";
        public const string Pages_Hrm_Categories_Edit = "Pages.Hrm.Category.Edit_Category";
        public const string Pages_Hrm_Categories_Delete = "Pages.Hrm.Category.Delete_Category";

        //ChartOfAccountMain
        public const string Pages_Hrm_ChartOfAccountMains = "Pages.Hrm.ChartOfAccountMain.ChartOfAccountMain";
        public const string Pages_Hrm_ChartOfAccountMains_View = "Pages.Hrm.ChartOfAccountMain.View_ChartOfAccountMain";
        public const string Pages_Hrm_ChartOfAccountMains_Print = "Pages.Hrm.ChartOfAccountMain.Print_ChartOfAccountMain";
        public const string Pages_Hrm_ChartOfAccountMains_Create = "Pages.Hrm.ChartOfAccountMain.Create_ChartOfAccountMain";
        public const string Pages_Hrm_ChartOfAccountMains_Edit = "Pages.Hrm.ChartOfAccountMain.Edit_ChartOfAccountMain";
        public const string Pages_Hrm_ChartOfAccountMains_Delete = "Pages.Hrm.ChartOfAccountMain.Delete_ChartOfAccountMain";

        //ChartOfAccountSubMain
        public const string Pages_Hrm_ChartOfAccountSubMains = "Pages.Hrm.ChartOfAccountSubMain.ChartOfAccountSubMain";
        public const string Pages_Hrm_ChartOfAccountSubMains_View = "Pages.Hrm.ChartOfAccountSubMain.View_ChartOfAccountSubMain";
        public const string Pages_Hrm_ChartOfAccountSubMains_Print = "Pages.Hrm.ChartOfAccountSubMain.Print_ChartOfAccountSubMain";
        public const string Pages_Hrm_ChartOfAccountSubMains_Create = "Pages.Hrm.ChartOfAccountSubMain.Create_ChartOfAccountSubMain";
        public const string Pages_Hrm_ChartOfAccountSubMains_Edit = "Pages.Hrm.ChartOfAccountSubMain.Edit_ChartOfAccountSubMain";
        public const string Pages_Hrm_ChartOfAccountSubMains_Delete = "Pages.Hrm.ChartOfAccountSubMain.Delete_ChartOfAccountSubMain";

        //ChartOfAccountSubSubDetail
        public const string Pages_Hrm_ChartOfAccountSubSubDetails = "Pages.Hrm.ChartOfAccountSubSubDetail.Achievement";
        public const string Pages_Hrm_ChartOfAccountSubSubDetails_View = "Pages.Hrm.ChartOfAccountSubSubDetail.View_ChartOfAccountSubSubDetail";
        public const string Pages_Hrm_ChartOfAccountSubSubDetails_Print = "Pages.Hrm.ChartOfAccountSubSubDetail.Print_ChartOfAccountSubSubDetail";
        public const string Pages_Hrm_ChartOfAccountSubSubDetails_Create = "Pages.Hrm.ChartOfAccountSubSubDetail.Create_ChartOfAccountSubSubDetail";
        public const string Pages_Hrm_ChartOfAccountSubSubDetails_Edit = "Pages.Hrm.ChartOfAccountSubSubDetail.Edit_ChartOfAccountSubSubDetail";
        public const string Pages_Hrm_ChartOfAccountSubSubDetails_Delete = "Pages.Hrm.ChartOfAccountSubSubDetail.Delete_ChartOfAccountSubSubDetail";

        //ChartOfAccountSubSubMain
        public const string Pages_Hrm_ChartOfAccountSubSubMains = "Pages.Hrm.ChartOfAccountSubSubMain.ChartOfAccountSubSubMain";
        public const string Pages_Hrm_ChartOfAccountSubSubMains_View = "Pages.Hrm.ChartOfAccountSubSubMain.View_ChartOfAccountSubSubMain";
        public const string Pages_Hrm_ChartOfAccountSubSubMains_Print = "Pages.Hrm.ChartOfAccountSubSubMain.Print_ChartOfAccountSubSubMain";
        public const string Pages_Hrm_ChartOfAccountSubSubMains_Create = "Pages.Hrm.ChartOfAccountSubSubMain.Create_ChartOfAccountSubSubMain";
        public const string Pages_Hrm_ChartOfAccountSubSubMains_Edit = "Pages.Hrm.ChartOfAccountSubSubMain.Edit_ChartOfAccountSubSubMain";
        public const string Pages_Hrm_ChartOfAccountSubSubMains_Delete = "Pages.Hrm.ChartOfAccountSubSubMain.Delete_ChartOfAccountSubSubMain";

        //City
        public const string Pages_Hrm_Cities = "Pages.Hrm.City.City";
        public const string Pages_Hrm_Cities_View = "Pages.Hrm.City.View_City";
        public const string Pages_Hrm_Cities_Print = "Pages.Hrm.City.Print_City";
        public const string Pages_Hrm_Cities_Create = "Pages.Hrm.City.Create_City";
        public const string Pages_Hrm_Cities_Edit = "Pages.Hrm.City.Edit_City";
        public const string Pages_Hrm_Cities_Delete = "Pages.Hrm.City.Delete_City";

        //Company
        public const string Pages_Hrm_Companies = "Pages.Hrm.Company.Company";
        public const string Pages_Hrm_Companies_View = "Pages.Hrm.Company.View_Company";
        public const string Pages_Hrm_Companies_Print = "Pages.Hrm.Company.Print_Company";
        public const string Pages_Hrm_Companies_Create = "Pages.Hrm.Company.Create_Company";
        public const string Pages_Hrm_Companies_Edit = "Pages.Hrm.Company.Edit_Company";
        public const string Pages_Hrm_Companies_Delete = "Pages.Hrm.Company.Delete_Company";

        //CompanyInformation
        public const string Pages_Hrm_CompanyInformations = "Pages.Hrm.CompanyInformation.CompanyInformation";
        public const string Pages_Hrm_CompanyInformations_View = "Pages.Hrm.CompanyInformation.View_CompanyInformation";
        public const string Pages_Hrm_CompanyInformations_Print = "Pages.Hrm.CompanyInformation.Print_CompanyInformation";
        public const string Pages_Hrm_CompanyInformations_Create = "Pages.Hrm.CompanyInformation.Create_CompanyInformation";
        public const string Pages_Hrm_CompanyInformations_Edit = "Pages.Hrm.CompanyInformation.Edit_CompanyInformation";
        public const string Pages_Hrm_CompanyInformations_Delete = "Pages.Hrm.CompanyInformation.Delete_CompanyInformation";

        //Configuration
        public const string Pages_Hrm_Configurations = "Pages.Hrm.Configuration.Configuration";
        public const string Pages_Hrm_Configurations_View = "Pages.Hrm.Configuration.View_Configuration";
        public const string Pages_Hrm_Configurations_Print = "Pages.Hrm.Configuration.Print_Configuration";
        public const string Pages_Hrm_Configurations_Create = "Pages.Hrm.Configuration.Create_Configuration";
        public const string Pages_Hrm_Configurations_Edit = "Pages.Hrm.Configuration.Edit_Configuration";
        public const string Pages_Hrm_Configurations_Delete = "Pages.Hrm.Configuration.Delete_Configuration";

        //CostCenter
        public const string Pages_Hrm_CostCenters = "Pages.Hrm.CostCenter.Configuration";
        public const string Pages_Hrm_CostCenters_View = "Pages.Hrm.CostCenter.View_CostCenter";
        public const string Pages_Hrm_CostCenters_Print = "Pages.Hrm.CostCenter.Print_CostCenter";
        public const string Pages_Hrm_CostCenters_Create = "Pages.Hrm.CostCenter.Create_CostCenter";
        public const string Pages_Hrm_CostCenters_Edit = "Pages.Hrm.CostCenter.Edit_CostCenter";
        public const string Pages_Hrm_CostCenters_Delete = "Pages.Hrm.CostCenter.Delete_CostCenter";


        //Country
       public const string Pages_Hrm_Countries = "Pages.Hrm.Country.Country";
        public const string Pages_Hrm_Countries_View = "Pages.Hrm.Country.View_Country";
        public const string Pages_Hrm_Countries_Print = "Pages.Hrm.Country.Print_Country";
        public const string Pages_Hrm_Countries_Create = "Pages.Hrm.Country.Create_Country";
        public const string Pages_Hrm_Countries_Edit = "Pages.Hrm.Country.Edit_Country";
        public const string Pages_Hrm_Countries_Delete = "Pages.Hrm.Country.Delete_Country";

        //Deduction
        public const string Pages_Hrm_Deductions = "Pages.Hrm.Deduction.Deduction";
        public const string Pages_Hrm_Deductions_View = "Pages.Hrm.Deduction.View_Deduction";
        public const string Pages_Hrm_Deductions_Print = "Pages.Hrm.Deduction.Print_Deduction";
        public const string Pages_Hrm_Deductions_Create = "Pages.Hrm.Deduction.Create_Deduction";
        public const string Pages_Hrm_Deductions_Edit = "Pages.Hrm.Deduction.Edit_Deduction";
        public const string Pages_Hrm_Deductions_Delete = "Pages.Hrm.Deduction.Delete_Deduction";


          //DeductionType
        public const string Pages_Hrm_DeductionTypes = "Pages.Hrm.DeductionType.DeductionType";
        public const string Pages_Hrm_DeductionTypes_View = "Pages.Hrm.DeductionType.View_DeductionType";
        public const string Pages_Hrm_DeductionTypes_Print = "Pages.Hrm.DeductionType.Print_DeductionType";
        public const string Pages_Hrm_DeductionTypes_Create = "Pages.Hrm.DeductionType.Create_DeductionType";
        public const string Pages_Hrm_DeductionTypes_Edit = "Pages.Hrm.DeductionType.Edit_DeductionType";
        public const string Pages_Hrm_DeductionTypes_Delete = "Pages.Hrm.DeductionType.Delete_DeductionType";

        //EmployeeDesignation
        public const string Pages_Hrm_EmployeeDesignations = "Pages.Hrm.EmployeeDesignation.EmployeeDesignation";
        public const string Pages_Hrm_EmployeeDesignations_View = "Pages.Hrm.EmployeeDesignation.View_EmployeeDesignation";
        public const string Pages_Hrm_EmployeeDesignations_Print = "Pages.Hrm.EmployeeDesignation.Print_EmployeeDesignation";
        public const string Pages_Hrm_EmployeeDesignations_Create = "Pages.Hrm.EmployeeDesignation.Create_EmployeeDesignation";
        public const string Pages_Hrm_EmployeeDesignations_Edit = "Pages.Hrm.EmployeeDesignation.Edit_EmployeeDesignation";
        public const string Pages_Hrm_EmployeeDesignations_Delete = "Pages.Hrm.EmployeeDesignation.Delete_EmployeeDesignation";


        //EmployeeInfo
        public const string Pages_Hrm_EmployeeInfos = "Pages.Hrm.EmployeeInfo.EmployeeInfo";
        public const string Pages_Hrm_EmployeeInfos_View = "Pages.Hrm.EmployeeInfo.View_EmployeeInfo";
        public const string Pages_Hrm_EmployeeInfos_Print = "Pages.Hrm.EmployeeInfo.Print_EmployeeInfo";
        public const string Pages_Hrm_EmployeeInfos_Create = "Pages.Hrm.EmployeeInfo.Create_EmployeeInfo";
        public const string Pages_Hrm_EmployeeInfos_Edit = "Pages.Hrm.EmployeeInfo.Edit_EmployeeInfo";
        public const string Pages_Hrm_EmployeeInfos_Delete = "Pages.Hrm.EmployeeInfo.Delete_EmployeeInfo";

        //Employee List
        public const string Pages_Hrm_EmployeeLists = "Pages.Hrm.EmployeeList.EmployeeList";
        public const string Pages_Hrm_EmployeeLists_View = "Pages.Hrm.EmployeeList.View_EmployeeList";
        public const string Pages_Hrm_EmployeeLists_Print = "Pages.Hrm.EmployeeList.Print_EmployeeList";
        public const string Pages_Hrm_EmployeeLists_Create = "Pages.Hrm.EmployeeList.Create_EmployeeList";
        public const string Pages_Hrm_EmployeeLists_Edit = "Pages.Hrm.EmployeeList.Edit_EmployeeList";
        public const string Pages_Hrm_EmployeeLists_Delete = "Pages.Hrm.EmployeeList.Delete_EmployeeList";


        //EmployeePromo
        public const string Pages_Hrm_EmployeePromos = "Pages.Hrm.EmployeePromo.EmployeePromo";
        public const string Pages_Hrm_EmployeePromos_View = "Pages.Hrm.EmployeePromo.View_EmployeePromo";
        public const string Pages_Hrm_EmployeePromos_Print = "Pages.Hrm.EmployeePromo.Print_EmployeePromo";
        public const string Pages_Hrm_EmployeePromos_Create = "Pages.Hrm.EmployeePromo.Create_EmployeePromo";
        public const string Pages_Hrm_EmployeePromos_Edit = "Pages.Hrm.EmployeePromo.Edit_EmployeePromo";
        public const string Pages_Hrm_EmployeePromos_Delete = "Pages.Hrm.EmployeePromo.Delete_EmployeePromo";

        //EmployeeExit
        public const string Pages_Hrm_EmployeeExits = "Pages.Hrm.EmployeeExit.EmployeeExit";
        public const string Pages_Hrm_EmployeeExits_View = "Pages.Hrm.EmployeeExit.View_EmployeeExit";
        public const string Pages_Hrm_EmployeeExits_Print = "Pages.Hrm.EmployeeExit.Print_EmployeeExit";
        public const string Pages_Hrm_EmployeeExits_Create = "Pages.Hrm.EmployeeExit.Create_EmployeeExit";
        public const string Pages_Hrm_EmployeeExits_Edit = "Pages.Hrm.EmployeeExit.Edit_EmployeeExit";
        public const string Pages_Hrm_EmployeeExits_Delete = "Pages.Hrm.EmployeeExit.Delete_EmployeeExit";


        //Fine
        public const string Pages_Hrm_Fines = "Pages.Hrm.Fine.Fine";
        public const string Pages_Hrm_Fines_View = "Pages.Hrm.Fine.View_Fine";
        public const string Pages_Hrm_Fines_Print = "Pages.Hrm.Fine.Print_Fine";
        public const string Pages_Hrm_Fines_Create = "Pages.Hrm.Fine.Create_Fine";
        public const string Pages_Hrm_Fines_Edit = "Pages.Hrm.Fine.Edit_Fine";
        public const string Pages_Hrm_Fines_Delete = "Pages.Hrm.Fine.Delete_Fine";

        //FineType
        public const string Pages_Hrm_FineTypes = "Pages.Hrm.FineType.FineType";
        public const string Pages_Hrm_FineTypes_View = "Pages.Hrm.FineType.View_FineType";
        public const string Pages_Hrm_FineTypes_Print = "Pages.Hrm.FineType.Print_FineType";
        public const string Pages_Hrm_FineTypes_Create = "Pages.Hrm.FineType.Create_FineType";
        public const string Pages_Hrm_FineTypes_Edit = "Pages.Hrm.FineType.Edit_FineType";
        public const string Pages_Hrm_FineTypes_Delete = "Pages.Hrm.FineType.Delete_FineType";

        //Holiday
        public const string Pages_Hrm_Holidays = "Pages.Hrm.Holiday.Holiday";
        public const string Pages_Hrm_Holidays_View = "Pages.Hrm.Holiday.View_Holiday";
        public const string Pages_Hrm_Holidays_Print = "Pages.Hrm.Holiday.Print_Holiday";
        public const string Pages_Hrm_Holidays_Create = "Pages.Hrm.Holiday.Create_Holiday";
        public const string Pages_Hrm_Holidays_Edit = "Pages.Hrm.Holiday.Edit_Holiday";
        public const string Pages_Hrm_Holidays_Delete = "Pages.Hrm.Holiday.Delete_Holiday";

              //Item
        public const string Pages_Hrm_Items = "Pages.Hrm.Item.Item";
        public const string Pages_Hrm_Items_View = "Pages.Hrm.Item.View_Item";
        public const string Pages_Hrm_Items_Print = "Pages.Hrm.Item.Print_Item";
        public const string Pages_Hrm_Items_Create = "Pages.Hrm.Item.Create_Item";
        public const string Pages_Hrm_Items_Edit = "Pages.Hrm.Item.Edit_Item";
        public const string Pages_Hrm_Items_Delete = "Pages.Hrm.Item.Delete_Item";

        //ItemType
        public const string Pages_Hrm_ItemTypes = "Pages.Hrm.ItemType.ItemType";
        public const string Pages_Hrm_ItemTypes_View = "Pages.Hrm.ItemType.View_ItemType";
        public const string Pages_Hrm_ItemTypes_Print = "Pages.Hrm.ItemType.Print_ItemType";
        public const string Pages_Hrm_ItemTypes_Create = "Pages.Hrm.ItemType.Create_ItemType";
        public const string Pages_Hrm_ItemTypes_Edit = "Pages.Hrm.ItemType.Edit_ItemType";
        public const string Pages_Hrm_ItemTypes_Delete = "Pages.Hrm.ItemType.Delete_ItemType";

        //JobType
        public const string Pages_Hrm_JobTypes = "Pages.Hrm.JobType.JobType";
        public const string Pages_Hrm_JobTypes_View = "Pages.Hrm.JobType.View_JobType";
        public const string Pages_Hrm_JobTypes_Print = "Pages.Hrm.JobType.Print_JobType";
        public const string Pages_Hrm_JobTypes_Create = "Pages.Hrm.JobType.Create_JobType";
        public const string Pages_Hrm_JobTypes_Edit = "Pages.Hrm.JobType.Edit_JobType";
        public const string Pages_Hrm_JobTypes_Delete = "Pages.Hrm.JobType.Delete_JobType";

        //LeaveQuota
        public const string Pages_Hrm_LeaveQuotas = "Pages.Hrm.LeaveQuota.LeaveQuota";
        public const string Pages_Hrm_LeaveQuotas_View = "Pages.Hrm.LeaveQuota.View_LeaveQuota";
        public const string Pages_Hrm_LeaveQuotas_Print = "Pages.Hrm.LeaveQuota.Print_LeaveQuota";
        public const string Pages_Hrm_LeaveQuotas_Create = "Pages.Hrm.LeaveQuota.Create_LeaveQuota";
        public const string Pages_Hrm_LeaveQuotas_Edit = "Pages.Hrm.LeaveQuota.Edit_LeaveQuota";
        public const string Pages_Hrm_LeaveQuotas_Delete = "Pages.Hrm.LeaveQuota.Delete_LeaveQuota";

        //LeaveRequest
        public const string Pages_Hrm_LeaveRequests = "Pages.Hrm.LeaveRequest.LeaveRequest";
        public const string Pages_Hrm_LeaveRequests_View = "Pages.Hrm.LeaveRequest.View_LeaveRequest";
        public const string Pages_Hrm_LeaveRequests_Print = "Pages.Hrm.LeaveRequest.Print_LeaveRequest";
        public const string Pages_Hrm_LeaveRequests_Create = "Pages.Hrm.LeaveRequest.Create_LeaveRequest";
        public const string Pages_Hrm_LeaveRequests_Edit = "Pages.Hrm.LeaveRequest.Edit_LeaveRequest";
        public const string Pages_Hrm_LeaveRequests_Delete = "Pages.Hrm.LeaveRequest.Delete_LeaveRequest";

        //LeaveType
        public const string Pages_Hrm_LeaveTypes = "Pages.Hrm.LeaveType.LeaveType";
        public const string Pages_Hrm_LeaveTypes_View = "Pages.Hrm.LeaveType.View_LeaveType";
        public const string Pages_Hrm_LeaveTypes_Print = "Pages.Hrm.LeaveType.Print_LeaveType";
        public const string Pages_Hrm_LeaveTypes_Create = "Pages.Hrm.LeaveType.Create_LeaveType";
        public const string Pages_Hrm_LeaveTypes_Edit = "Pages.Hrm.LeaveType.Edit_LeaveType";
        public const string Pages_Hrm_LeaveTypes_Delete = "Pages.Hrm.LeaveType.Delete_LeaveType";

        //LoanRequest
        public const string Pages_Hrm_LoanRequests = "Pages.Hrm.LoanRequest.LoanRequest";
        public const string Pages_Hrm_LoanRequests_View = "Pages.Hrm.LoanRequest.View_LoanRequest";
        public const string Pages_Hrm_LoanRequests_Print = "Pages.Hrm.LoanRequest.Print_LoanRequest";
        public const string Pages_Hrm_LoanRequests_Create = "Pages.Hrm.LoanRequest.Create_LoanRequest";
        public const string Pages_Hrm_LoanRequests_Edit = "Pages.Hrm.LoanRequest.Edit_LoanRequest";
        public const string Pages_Hrm_LoanRequests_Delete = "Pages.Hrm.LoanRequest.Delete_LoanRequest";

        //LocationInformation
        public const string Pages_Hrm_LocationInformations = "Pages.Hrm.LocationInformation.LocationInformation";
        public const string Pages_Hrm_LocationInformations_View = "Pages.Hrm.LocationInformation.View_LocationInformation";
        public const string Pages_Hrm_LocationInformations_Print = "Pages.Hrm.LocationInformation.Print_LocationInformation";
        public const string Pages_Hrm_LocationInformations_Create = "Pages.Hrm.LocationInformation.Create_LocationInformation";
        public const string Pages_Hrm_LocationInformations_Edit = "Pages.Hrm.LocationInformation.Edit_LocationInformation";
        public const string Pages_Hrm_LocationInformations_Delete = "Pages.Hrm.LocationInformation.Delete_LocationInformation";

        //Overtime
        public const string Pages_Hrm_Overtimes = "Pages.Hrm.Overtime.Overtime";
        public const string Pages_Hrm_Overtimes_View = "Pages.Hrm.Overtime.View_Overtime";
        public const string Pages_Hrm_Overtimes_Print = "Pages.Hrm.Overtime.Print_Overtime";
        public const string Pages_Hrm_Overtimes_Create = "Pages.Hrm.Overtime.Create_Overtime";
        public const string Pages_Hrm_Overtimes_Edit = "Pages.Hrm.Overtime.Edit_Overtime";
        public const string Pages_Hrm_Overtimes_Delete = "Pages.Hrm.Overtime.Delete_Overtime";

        //POS
        public const string Pages_Hrm_POSs = "Pages.Hrm.POS.POS";
        public const string Pages_Hrm_POSs_View = "Pages.Hrm.POS.View_POS";
        public const string Pages_Hrm_POSs_Print = "Pages.Hrm.POS.Print_POS";
        public const string Pages_Hrm_POSs_Create = "Pages.Hrm.POS.Create_POS";
        public const string Pages_Hrm_POSs_Edit = "Pages.Hrm.POS.Edit_POS";
        public const string Pages_Hrm_POSs_Delete = "Pages.Hrm.POS.Delete_POS";

        //ProductionRate
        public const string Pages_Hrm_ProductionRates = "Pages.Hrm.ProductionRate.ProductionRate";
        public const string Pages_Hrm_ProductionRates_View = "Pages.Hrm.ProductionRate.View_ProductionRate";
        public const string Pages_Hrm_ProductionRates_Print = "Pages.Hrm.ProductionRate.Print_ProductionRate";
        public const string Pages_Hrm_ProductionRates_Create = "Pages.Hrm.ProductionRate.Create_ProductionRate";
        public const string Pages_Hrm_ProductionRates_Edit = "Pages.Hrm.ProductionRate.Edit_ProductionRate";
        public const string Pages_Hrm_ProductionRates_Delete = "Pages.Hrm.ProductionRate.Delete_ProductionRate";

        //Promotion
        public const string Pages_Hrm_Promotions = "Pages.Hrm.Promotion.Promotion";
        public const string Pages_Hrm_Promotions_View = "Pages.Hrm.Promotion.View_Promotion";
        public const string Pages_Hrm_Promotions_Print = "Pages.Hrm.Promotion.Print_Promotion";
        public const string Pages_Hrm_Promotions_Create = "Pages.Hrm.Promotion.Create_Promotion";
        public const string Pages_Hrm_Promotions_Edit = "Pages.Hrm.Promotion.Edit_Promotion";
        public const string Pages_Hrm_Promotions_Delete = "Pages.Hrm.Promotion.Delete_Promotion";

        //Province
        public const string Pages_Hrm_Provinces = "Pages.Hrm.Province.Province";
        public const string Pages_Hrm_Provinces_View = "Pages.Hrm.Province.View_Province";
        public const string Pages_Hrm_Provinces_Print = "Pages.Hrm.Province.Print_Province";
        public const string Pages_Hrm_Provinces_Create = "Pages.Hrm.Province.Create_Province";
        public const string Pages_Hrm_Provinces_Edit = "Pages.Hrm.Province.Edit_Province";
        public const string Pages_Hrm_Provinces_Delete = "Pages.Hrm.Province.Delete_Province";

        //Region
        public const string Pages_Hrm_Regions = "Pages.Hrm.Region.Region";
        public const string Pages_Hrm_Regions_View = "Pages.Hrm.Region.View_Region";
        public const string Pages_Hrm_Regions_Print = "Pages.Hrm.Region.Print_Region";
        public const string Pages_Hrm_Regions_Create = "Pages.Hrm.Region.Create_Region";
        public const string Pages_Hrm_Regions_Edit = "Pages.Hrm.Region.Edit_Region";
        public const string Pages_Hrm_Regions_Delete = "Pages.Hrm.Region.Delete_Region";

        //Resignation
        public const string Pages_Hrm_Resignations = "Pages.Hrm.Resignation.Resignation";
        public const string Pages_Hrm_Resignations_View = "Pages.Hrm.Resignation.View_Resignation";
        public const string Pages_Hrm_Resignations_Print = "Pages.Hrm.Resignation.Print_Resignation";
        public const string Pages_Hrm_Resignations_Create = "Pages.Hrm.Resignation.Create_Resignation";
        public const string Pages_Hrm_Resignations_Edit = "Pages.Hrm.Resignation.Edit_Resignation";
        public const string Pages_Hrm_Resignations_Delete = "Pages.Hrm.Resignation.Delete_Resignation";

        //Salary
        public const string Pages_Hrm_Salaries = "Pages.Hrm.Salary.Salary";
        public const string Pages_Hrm_Salaries_View = "Pages.Hrm.Salary.View_Salary";
        public const string Pages_Hrm_Salaries_Print = "Pages.Hrm.Salary.Print_Salary";
        public const string Pages_Hrm_Salaries_Create = "Pages.Hrm.Salary.Create_Salary";
        public const string Pages_Hrm_Salaries_Edit = "Pages.Hrm.Salary.Edit_Salary";
        public const string Pages_Hrm_Salaries_Delete = "Pages.Hrm.Salary.Delete_Salary";

        //SalaryExpenseType
        public const string Pages_Hrm_SalaryExpenseTypes = "Pages.Hrm.SalaryExpenseType.SalaryExpenseType";
        public const string Pages_Hrm_SalaryExpenseTypes_View = "Pages.Hrm.SalaryExpenseType.View_SalaryExpenseType";
        public const string Pages_Hrm_SalaryExpenseTypes_Print = "Pages.Hrm.SalaryExpenseType.Print_SalaryExpenseType";
        public const string Pages_Hrm_SalaryExpenseTypes_Create = "Pages.Hrm.SalaryExpenseType.Create_SalaryExpenseType";
        public const string Pages_Hrm_SalaryExpenseTypes_Edit = "Pages.Hrm.SalaryExpenseType.Edit_SalaryExpenseType";
        public const string Pages_Hrm_SalaryExpenseTypes_Delete = "Pages.Hrm.SalaryExpenseType.Delete_SalaryExpenseType";

        //SalaryGroup
        public const string Pages_Hrm_SalaryGroups = "Pages.Hrm.SalaryGroup.SalaryGroup";
        public const string Pages_Hrm_SalaryGroups_View = "Pages.Hrm.SalaryGroup.View_SalaryGroup";
        public const string Pages_Hrm_SalaryGroups_Print = "Pages.Hrm.SalaryGroup.Print_SalaryGroup";
        public const string Pages_Hrm_SalaryGroups_Create = "Pages.Hrm.SalaryGroup.Create_SalaryGroup";
        public const string Pages_Hrm_SalaryGroups_Edit = "Pages.Hrm.SalaryGroup.Edit_SalaryGroup";
        public const string Pages_Hrm_SalaryGroups_Delete = "Pages.Hrm.SalaryGroup.Delete_SalaryGroup";


        //SalaryWizard
        public const string Pages_Hrm_SalaryWizards = "Pages.Hrm.SalaryWizard.SalaryWizard";
        public const string Pages_Hrm_SalaryWizards_View = "Pages.Hrm.SalaryWizard.View_SalaryWizard";
        public const string Pages_Hrm_SalaryWizards_Print = "Pages.Hrm.SalaryWizard.Print_SalaryWizard";
        public const string Pages_Hrm_SalaryWizards_Create = "Pages.Hrm.SalaryWizard.Create_SalaryWizard";
        public const string Pages_Hrm_SalaryWizards_Edit = "Pages.Hrm.SalaryWizard.Edit_SalaryWizard";
        public const string Pages_Hrm_SalaryWizards_Delete = "Pages.Hrm.SalaryWizard.Delete_SalaryWizard";

        //Sample
        public const string Pages_Hrm_Samples = "Pages.Hrm.Sample.Sample";
        public const string Pages_Hrm_Samples_View = "Pages.Hrm.Sample.View_Sample";
        public const string Pages_Hrm_Samples_Print = "Pages.Hrm.Sample.Print_Sample";
        public const string Pages_Hrm_Samples_Create = "Pages.Hrm.Sample.Create_Sample";
        public const string Pages_Hrm_Samples_Edit = "Pages.Hrm.Sample.Edit_Sample";
        public const string Pages_Hrm_Samples_Delete = "Pages.Hrm.Sample.Delete_Sample";

        //State
        public const string Pages_Hrm_States = "Pages.Hrm.State.State";
        public const string Pages_Hrm_States_View = "Pages.Hrm.State.View_State";
        public const string Pages_Hrm_States_Print = "Pages.Hrm.State.Print_State";
        public const string Pages_Hrm_States_Create = "Pages.Hrm.State.Create_State";
        public const string Pages_Hrm_States_Edit = "Pages.Hrm.State.Edit_State";
        public const string Pages_Hrm_States_Delete = "Pages.Hrm.State.Delete_State";

        //SubCategory
        public const string Pages_Hrm_SubCategories = "Pages.Hrm.SubCategory.SubCategory";
        public const string Pages_Hrm_SubCategories_View = "Pages.Hrm.SubCategory.View_SubCategory";
        public const string Pages_Hrm_SubCategories_Print = "Pages.Hrm.SubCategory.Print_SubCategory";
        public const string Pages_Hrm_SubCategories_Create = "Pages.Hrm.SubCategory.Create_SubCategory";
        public const string Pages_Hrm_SubCategories_Edit = "Pages.Hrm.SubCategory.Edit_SubCategory";
        public const string Pages_Hrm_SubCategories_Delete = "Pages.Hrm.SubCategory.Delete_SubCategory";

        //TaxSlab
        public const string Pages_Hrm_TaxSlabs = "Pages.Hrm.TaxSlab.TaxSlab";
        public const string Pages_Hrm_TaxSlabs_View = "Pages.Hrm.TaxSlab.View_TaxSlab";
        public const string Pages_Hrm_TaxSlabs_Print = "Pages.Hrm.TaxSlab.Print_TaxSlab";
        public const string Pages_Hrm_TaxSlabs_Create = "Pages.Hrm.TaxSlab.Create_TaxSlab";
        public const string Pages_Hrm_TaxSlabs_Edit = "Pages.Hrm.TaxSlab.Edit_TaxSlab";
        public const string Pages_Hrm_TaxSlabs_Delete = "Pages.Hrm.TaxSlab.Delete_TaxSlab";

        //Termination
        public const string Pages_Hrm_Terminations = "Pages.Hrm.Termination.Termination";
        public const string Pages_Hrm_Terminations_View = "Pages.Hrm.Termination.View_Termination";
        public const string Pages_Hrm_Terminations_Print = "Pages.Hrm.Termination.Print_Termination";
        public const string Pages_Hrm_Terminations_Create = "Pages.Hrm.Termination.Create_Termination";
        public const string Pages_Hrm_Terminations_Edit = "Pages.Hrm.Termination.Edit_Termination";
        public const string Pages_Hrm_Terminations_Delete = "Pages.Hrm.Termination.Delete_Termination";

        //Territory
        public const string Pages_Hrm_Territories = "Pages.Hrm.Territory.Territory";
        public const string Pages_Hrm_Territories_View = "Pages.Hrm.Territory.View_Territory";
        public const string Pages_Hrm_Territories_Print = "Pages.Hrm.Territory.Print_Territory";
        public const string Pages_Hrm_Territories_Create = "Pages.Hrm.Territory.Create_Territory";
        public const string Pages_Hrm_Territories_Edit = "Pages.Hrm.Territory.Edit_Territory";
        public const string Pages_Hrm_Territories_Delete = "Pages.Hrm.Territory.Delete_Territory";

        //Transfer
        public const string Pages_Hrm_Transfers = "Pages.Hrm.Transfer.Transfer";
        public const string Pages_Hrm_Transfers_View = "Pages.Hrm.Transfer.View_Transfer";
        public const string Pages_Hrm_Transfers_Print = "Pages.Hrm.Transfer.Print_Transfer";
        public const string Pages_Hrm_Transfers_Create = "Pages.Hrm.Transfer.Create_Transfer";
        public const string Pages_Hrm_Transfers_Edit = "Pages.Hrm.Transfer.Edit_Transfer";
        public const string Pages_Hrm_Transfers_Delete = "Pages.Hrm.Transfer.Delete_Transfer";

        //UserAccountRight
        public const string Pages_Hrm_UserAccountRights = "Pages.Hrm.UserAccountRight.UserAccountRight";
        public const string Pages_Hrm_UserAccountRights_View = "Pages.Hrm.UserAccountRight.View_UserAccountRight";
        public const string Pages_Hrm_UserAccountRights_Print = "Pages.Hrm.UserAccountRight.Print_UserAccountRight";
        public const string Pages_Hrm_UserAccountRights_Create = "Pages.Hrm.UserAccountRight.Create_UserAccountRight";
        public const string Pages_Hrm_UserAccountRights_Edit = "Pages.Hrm.UserAccountRight.Edit_UserAccountRight";
        public const string Pages_Hrm_UserAccountRights_Delete = "Pages.Hrm.UserAccountRight.Delete_UserAccountRight";

        //UserCompanyRight
        public const string Pages_Hrm_UserCompanyRights = "Pages.Hrm.UserCompanyRight.UserCompanyRight";
        public const string Pages_Hrm_UserCompanyRights_View = "Pages.Hrm.UserCompanyRight.View_UserCompanyRight";
        public const string Pages_Hrm_UserCompanyRights_Print = "Pages.Hrm.UserCompanyRight.Print_UserCompanyRight";
        public const string Pages_Hrm_UserCompanyRights_Create = "Pages.Hrm.UserCompanyRight.Create_UserCompanyRight";
        public const string Pages_Hrm_UserCompanyRights_Edit = "Pages.Hrm.UserCompanyRight.Edit_UserCompanyRight";
        public const string Pages_Hrm_UserCompanyRights_Delete = "Pages.Hrm.UserCompanyRight.Delete_UserCompanyRight";

        //UserCostCenterRight
        public const string Pages_Hrm_UserCostCenterRights = "Pages.Hrm.UserCostCenterRight.UserCostCenterRight";
        public const string Pages_Hrm_UserCostCenterRights_View = "Pages.Hrm.UserCostCenterRight.View_UserCostCenterRight";
        public const string Pages_Hrm_UserCostCenterRights_Print = "Pages.Hrm.UserCostCenterRight.Print_UserCostCenterRight";
        public const string Pages_Hrm_UserCostCenterRights_Create = "Pages.Hrm.UserCostCenterRight.Create_UserCostCenterRight";
        public const string Pages_Hrm_UserCostCenterRights_Edit = "Pages.Hrm.UserCostCenterRight.Edit_UserCostCenterRight";
        public const string Pages_Hrm_UserCostCenterRights_Delete = "Pages.Hrm.UserCostCenterRight.Delete_UserCostCenterRight";

        //UserLocationRight
        public const string Pages_Hrm_UserLocationRights = "Pages.Hrm.UserLocationRight.UserLocationRight";
        public const string Pages_Hrm_UserLocationRights_View = "Pages.Hrm.UserLocationRight.View_UserLocationRight";
        public const string Pages_Hrm_UserLocationRights_Print = "Pages.Hrm.UserLocationRight.Print_UserLocationRight";
        public const string Pages_Hrm_UserLocationRights_Create = "Pages.Hrm.UserLocationRight.Create_UserLocationRight";
        public const string Pages_Hrm_UserLocationRights_Edit = "Pages.Hrm.UserLocationRight.Edit_UserLocationRight";
        public const string Pages_Hrm_UserLocationRights_Delete = "Pages.Hrm.UserLocationRight.Delete_UserLocationRight";


          //UserMediaContent
       public const string Pages_Hrm_UserMediaContents = "Pages.Hrm.UserMediaContent.UserMediaContent";
        public const string Pages_Hrm_UserMediaContents_View = "Pages.Hrm.UserMediaContent.View_UserMediaContent";
        public const string Pages_Hrm_UserMediaContents_Print = "Pages.Hrm.UserMediaContent.Print_UserMediaContent";
        public const string Pages_Hrm_UserMediaContents_Create = "Pages.Hrm.UserMediaContent.Create_UserMediaContent";
        public const string Pages_Hrm_UserMediaContents_Edit = "Pages.Hrm.UserMediaContent.Edit_UserMediaContent";
        public const string Pages_Hrm_UserMediaContents_Delete = "Pages.Hrm.UserMediaContent.Delete_UserMediaContent";

        //UserVoucherTypeRight
        public const string Pages_Hrm_UserVoucherTypeRights = "Pages.Hrm.UserVoucherTypeRight.UserVoucherTypeRight";
        public const string Pages_Hrm_UserVoucherTypeRights_View = "Pages.Hrm.UserVoucherTypeRight.View_UserVoucherTypeRight";
        public const string Pages_Hrm_UserVoucherTypeRights_Print = "Pages.Hrm.UserVoucherTypeRight.Print_UserVoucherTypeRight";
        public const string Pages_Hrm_UserVoucherTypeRights_Create = "Pages.Hrm.UserVoucherTypeRight.Create_UserVoucherTypeRight";
        public const string Pages_Hrm_UserVoucherTypeRights_Edit = "Pages.Hrm.UserVoucherTypeRight.Edit_UserVoucherTypeRight";
        public const string Pages_Hrm_UserVoucherTypeRights_Delete = "Pages.Hrm.UserVoucherTypeRight.Delete_UserVoucherTypeRight";

        //VoucherType
        public const string Pages_Hrm_VoucherTypes = "Pages.Hrm.VoucherType.VoucherType";
        public const string Pages_Hrm_VoucherTypes_View = "Pages.Hrm.VoucherType.View_VoucherType";
        public const string Pages_Hrm_VoucherTypes_Print = "Pages.Hrm.VoucherType.Print_VoucherType";
        public const string Pages_Hrm_VoucherTypes_Create = "Pages.Hrm.VoucherType.Create_VoucherType";
        public const string Pages_Hrm_VoucherTypes_Edit = "Pages.Hrm.VoucherType.Edit_VoucherType";
        public const string Pages_Hrm_VoucherTypes_Delete = "Pages.Hrm.VoucherType.Delete_VoucherType";

        //Warning
        public const string Pages_Hrm_Warnings = "Pages.Hrm.Warning.Warning";
        public const string Pages_Hrm_Warnings_View = "Pages.Hrm.Warning.View_Warning";
        public const string Pages_Hrm_Warnings_Print = "Pages.Hrm.Warning.Print_Warning";
        public const string Pages_Hrm_Warnings_Create = "Pages.Hrm.Warning.Create_Warning";
        public const string Pages_Hrm_Warnings_Edit = "Pages.Hrm.Warning.Edit_Warning";
        public const string Pages_Hrm_Warnings_Delete = "Pages.Hrm.Warning.Delete_Warning";

        //WorkGroup
        public const string Pages_Hrm_WorkGroups = "Pages.Hrm.WorkGroup.WorkGroup";
        public const string Pages_Hrm_WorkGroups_View = "Pages.Hrm.WorkGroup.View_WorkGroup";
        public const string Pages_Hrm_WorkGroups_Print = "Pages.Hrm.WorkGroup.Print_WorkGroup";
        public const string Pages_Hrm_WorkGroups_Create = "Pages.Hrm.WorkGroup.Create_WorkGroup";
        public const string Pages_Hrm_WorkGroups_Edit = "Pages.Hrm.WorkGroup.Edit_WorkGroup";
        public const string Pages_Hrm_WorkGroups_Delete = "Pages.Hrm.WorkGroup.Delete_WorkGroup";

        //Zone
        public const string Pages_Hrm_Zones = "Pages.Hrm.Zone.Zone";
        public const string Pages_Hrm_Zones_View = "Pages.Hrm.Zone.View_Zone";
        public const string Pages_Hrm_Zones_Print = "Pages.Hrm.Zone.Print_Zone";
        public const string Pages_Hrm_Zones_Create = "Pages.Hrm.Zone.Create_Zone";
        public const string Pages_Hrm_Zones_Edit = "Pages.Hrm.Zone.Edit_Zone";
        public const string Pages_Hrm_Zones_Delete = "Pages.Hrm.Zone.Delete_Zone";


        //UserRights
        //public const string Pages_Hrm_UserRights = "Pages.Security.UserRight.UserRight";
        //public const string Pages_Hrm_UserRights_View = "Pages.Security.UserRight.View_UserRight";

        //UserRights-User
        public const string Pages_Hrm_UserRights_Users = "Pages.Hrm.UserRight.User";
        public const string Pages_Hrm_UserRights_Users_View = "Pages.Hrm.UserRight.User.View_User";
        public const string Pages_Hrm_UserRights_Users_Create = "Pages.Hrm.UserRight.User.Create_User";
        public const string Pages_Hrm_UserRights_Users_Edit = "Pages.Hrm.UserRight.User.Edit_User";
        public const string Pages_Hrm_UserRights_Users_Delete = "Pages.Hrm.UserRight.User.Delete_User";

        //UserRights-Roles
        public const string Pages_Hrm_UserRights_Roles = "Pages.Hrm.UserRight.Role";
        public const string Pages_Hrm_UserRights_Roles_View = "Pages.Hrm.UserRight.Role.View_Role";
        public const string Pages_Hrm_UserRights_Roles_Create = "Pages.Hrm.UserRight.Role.Create_Role";
        public const string Pages_Hrm_UserRights_Roles_Edit = "Pages.Hrm.UserRight.Role.Edit_Role";
        public const string Pages_Hrm_UserRights_Roles_Delete = "Pages.Hrm.UserRight.Role.Delete_Role";

        //Security
        public const string Pages_Security_Companies = "Pages.Security.Company.Company";
        public const string Pages_Security_Companies_View = "Pages.Security.Company.View_Company";
        public const string Pages_Security_Companies_Create = "Pages.Security.Company.Create_Company";

        //CostCenter
        public const string Pages_Security_CostCenters = "Pages.Security.CostCenter.CostCenter";
        public const string Pages_Security_CostCenters_View = "Pages.Security.CostCenter.View_CostCenter";
        public const string Pages_Security_CostCenters_Create = "Pages.Security.CostCenter.Create_CostCenter";

        //Account
        public const string Pages_Security_Accounts = "Pages.Security.Account.Account";
        public const string Pages_Security_Accounts_View = "Pages.Security.Account.View_Account";
        public const string Pages_Security_Accounts_Create = "Pages.Security.Account.Create_Account";

        //Location
        public const string Pages_Security_Locations = "Pages.Security.Location.Location";
        public const string Pages_Security_Locations_View = "Pages.Security.Location.View_Location";
        public const string Pages_Security_Locations_Create = "Pages.Security.Location.Create_Location";

        //VoucherType
        public const string Pages_Security_VoucherTypes = "Pages.Security.VoucherType.VoucherType";
        public const string Pages_Security_VoucherTypes_View = "Pages.Security.VoucherType.View_VoucherType";
        public const string Pages_Security_VoucherTypes_Create = "Pages.Security.VoucherType.Create_VoucherType";


        

    }

}
