﻿using Abp.Notifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Notification
{
    public class SendNotificationData:NotificationData
    {
        public string UserName { get; set; }
        public string WelcomeMessage { get; set; }

        public SendNotificationData(string _name,string _message)
        {
            UserName = _name;
            WelcomeMessage = _message;

        }
    }
}
