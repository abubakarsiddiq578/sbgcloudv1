﻿using Abp;
using Abp.Notifications;
using CloudBaseLine.Authorization.Users;
using CloudBaseLine.MultiTenancy;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Notification
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

    }
}
