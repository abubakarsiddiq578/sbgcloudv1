﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Authorization.Accounts.Dto
{
    public class UserImgDto
    {
        public IFormFile avatar { get; set; }
    }
}
