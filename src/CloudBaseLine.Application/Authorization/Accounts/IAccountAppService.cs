﻿using System.Threading.Tasks;
using Abp.Application.Services;
using CloudBaseLine.Authorization.Accounts.Dto;
using CloudBaseLine.Authorization.Users;
using Microsoft.AspNetCore.Http;

namespace CloudBaseLine.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);

        Task<User> VerifyEmail(string email);

        Task<User> VerifyUsernName(string username);

        Task<User> VerifyUsernNameOrEmail(string usernameOremail);

        Task<string> CreateProfilePic(IFormFile file);
    }
}
