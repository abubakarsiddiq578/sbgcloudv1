using System;
using System.Threading.Tasks;
using Abp;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Notifications;
using Abp.Zero.Configuration;
using CloudBaseLine.Authorization.Accounts.Dto;
using CloudBaseLine.Authorization.Users;
using CloudBaseLine.BackgroundJobs;
using CloudBaseLine.Notification;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IO;
using CloudBaseLine.Entities.UserMediaContents.Dto;

namespace CloudBaseLine.Authorization.Accounts
{
    public class AccountAppService : CloudBaseLineAppServiceBase, IAccountAppService
    {
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IUserNotificationManager _userNotificationManager;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly UserManager _userManager;

        public AccountAppService(
            UserRegistrationManager userRegistrationManager,
              INotificationSubscriptionManager notificationSubscriptionManager,
                    IAppNotifier appNotifier,
                       IUserNotificationManager userNotificationManager,
            IBackgroundJobManager backgroundJob,
            UserManager userManager)
        {
            _userRegistrationManager = userRegistrationManager;
            _appNotifier = appNotifier;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _userNotificationManager = userNotificationManager;
            _backgroundJobManager = backgroundJob;
            _userManager = userManager;
        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }

        public async Task<RegisterOutput> Register(RegisterInput input)
        {
            var user = await _userRegistrationManager.RegisterAsync(
                input.Name,
                input.Surname,
                input.EmailAddress,
                input.UserName,
                input.Password,
                true // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
            );

            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            await _backgroundJobManager.EnqueueAsync<WelcomeNofications, UserIdentifier>(
                user.ToUserIdentifier(),
                delay: TimeSpan.FromSeconds(5)
                );

            var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)
            };
        }

        public async Task<User> VerifyEmail(string email)
        {
            try
            {
                var result = await _userManager.FindByEmailAsync(email);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<User> VerifyUsernName(string username)
        {
            try
            {
                var result = await _userManager.FindByNameAsync(username);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<User> VerifyUsernNameOrEmail(string usernameOremail)
        {
            try
            {
                var result = await _userManager.FindByNameOrEmailAsync(usernameOremail);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> CreateProfilePic(IFormFile file)   // IFormFile file , string profile
        {
            //CreateUserProfileDto input = new CreateUserProfileDto();
            /*try
            {
                input = JsonConvert.DeserializeObject<CreateUserProfileDto>(profile);
            }
            catch (Exception e)
            {

            }*/

            string ProfilePictureName = "";




            if (file.Length > 0)
            {
                int UserId = Convert.ToInt32(AbpSession.UserId);
                int i = 0;
                var currentDirectory = System.IO.Directory.GetCurrentDirectory();

                //currentDirectory = currentDirectory + "\\src\\assets";

                string directoryPath = Path.Combine(currentDirectory, "profilepicture");
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                ProfilePictureName = Guid.NewGuid().ToString() + Path.GetFileName(file.FileName);


                var filePath = Path.Combine(directoryPath, ProfilePictureName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }


               

                //var newDirectory = "D:\\Softbeat Project\\GCloud BitBukcet\\Gcloud Frontend";
                //newDirectory = newDirectory + "\\src\\assets";
                //var newdirectoryPath = Path.Combine(newDirectory, "profilepicture");

                //if (!Directory.Exists(newdirectoryPath))
                //{
                //    Directory.CreateDirectory(newdirectoryPath);
                //}

                //var newfilePath = Path.Combine(newdirectoryPath, ProfilePictureName);

              


                //var newDirectory = "D:\\GCloud Bitbucket\\Source code\\Gcloud Frontend";
                //newDirectory = newDirectory + "\\src\\assets";
                //var newdirectoryPath = Path.Combine(newDirectory, "profilepicture");
                //var newfilePath = Path.Combine(newdirectoryPath, ProfilePictureName);

                //using (var fileStr = new FileStream(newfilePath, FileMode.Create))
                //{
                //    file.CopyTo(fileStr);
                //}
            }

            return ProfilePictureName;
        }
    }
}
