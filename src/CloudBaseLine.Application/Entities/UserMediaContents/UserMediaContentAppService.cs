﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.UserMediaContents.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.UserMediaContents
{
    public class UserMediaContentAppService : AsyncCrudAppService<UserMediaContent, UserMediaContentDto, long, PagedResultRequestDto, UserMediaContentDto, UserMediaContentDto>, IUserMediaContentAppService
    {
        private readonly IRepository<UserMediaContent, long> UserMediaContentrepository;
        private readonly IPermissionManager _permissionManager;

        public UserMediaContentAppService(IRepository<UserMediaContent, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            UserMediaContentrepository = _repository;
            _permissionManager = _Manager;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserMediaContents_Create)]
        public override async Task<UserMediaContentDto> Create(UserMediaContentDto input)
        {
            var result = ObjectMapper.Map<UserMediaContent>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await UserMediaContentrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserMediaContentDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserMediaContents_Edit)]
        public override async Task<UserMediaContentDto> Update(UserMediaContentDto input)
        {
            var data = ObjectMapper.Map<UserMediaContent>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await UserMediaContentrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserMediaContentDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserMediaContents_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = UserMediaContentrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await UserMediaContentrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<UserMediaContentDto>> GetAllMediaContent()
        {
            try
            {
                var result = UserMediaContentrepository.GetAll().Where(a => a.IsDeleted == false).ToList();
                return result.MapTo<List<UserMediaContentDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
