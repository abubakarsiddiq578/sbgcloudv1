﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.UserMediaContents.Dto
{
    public class UserImgFormDto 
    {
        public IFormFile avatar { get; set; }
    }
}
