﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Authorization.Users;
namespace CloudBaseLine.Entities.UserMediaContents.Dto
{
    [AutoMapTo(typeof(UserMediaContent)), AutoMapFrom(typeof(UserMediaContentDto))]
    public class UserMediaContentDto : EntityDto<long>
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public string ImageUrl { get; set; }
        public int TenantId { get; set; }
    }
}
