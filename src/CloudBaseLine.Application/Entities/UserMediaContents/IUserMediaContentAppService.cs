﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.UserMediaContents.Dto;

namespace CloudBaseLine.Entities.UserMediaContents
{
    public interface IUserMediaContentAppService : IAsyncCrudAppService<UserMediaContentDto, long, PagedResultRequestDto, UserMediaContentDto, UserMediaContentDto>
    {

    }
}
