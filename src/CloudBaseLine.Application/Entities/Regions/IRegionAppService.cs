﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Regions.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Regions
{
    public interface IRegionAppService : IAsyncCrudAppService<RegionDto,long,PagedResultRequestDto, RegionDto,RegionDto>
    {
    }
}
