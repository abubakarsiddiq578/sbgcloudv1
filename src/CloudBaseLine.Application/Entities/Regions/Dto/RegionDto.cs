﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Regions.Dto
{
    [AutoMapTo(typeof(Region)) , AutoMapFrom(typeof(RegionDto))]
    public class RegionDto:EntityDto<long>
    {
        public long ProvinceId { get; set; }
        public Province Province { get; set; }

        public string RegionName { get; set; }

        public int SortOrder { get; set; }

        public bool isActive { get; set; }

        public string Comments { get; set; }

        public int TenantId { get; set; }
    }
}
