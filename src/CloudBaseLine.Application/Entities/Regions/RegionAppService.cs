﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Regions.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Regions
{
    public class RegionAppService : AsyncCrudAppService<Region,RegionDto,long,PagedResultRequestDto,RegionDto,RegionDto>,IRegionAppService
    {
        private readonly IRepository<Region, long> RegionRepository;
        private readonly IPermissionManager _permissionManager;


        public RegionAppService(IRepository<Region, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            RegionRepository = _repository;

            _permissionManager = _Manager;

        }



        public async Task<List<RegionDto>> GetAllRegions()
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = RegionRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId).Include(t=>t.Province).ToList();

                return a.MapTo<List<RegionDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Regions_Create)]
        public override async Task<RegionDto> Create(RegionDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<Region>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await RegionRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<RegionDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Regions_Edit)]
        public override async Task<RegionDto> Update(RegionDto input)
        {
            var data = ObjectMapper.Map<Region>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await RegionRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<RegionDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Regions_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = RegionRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await RegionRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }
        public async Task<List<RegionDto>> GetAllRegionsByProvinceId(long id)
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = RegionRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId && b.ProvinceId == id).Include(t => t.Province).ToList();

                return a.MapTo<List<RegionDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


    }
}
