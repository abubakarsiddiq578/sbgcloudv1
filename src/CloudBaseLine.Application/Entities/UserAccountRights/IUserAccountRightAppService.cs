﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.UserAccountRights.Dto;

namespace CloudBaseLine.Entities.UserAccountRights
{
    public interface IUserAccountRightAppService : IAsyncCrudAppService<UserAccountRightDto, long, PagedResultRequestDto, UserAccountRightDto, UserAccountRightDto>
    {
        Task<List<UserAccountRightDto>> GetAccountRights();
    }
}
