﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.UserAccountRights.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.UserAccountRights
{
    public class UserAccountRightAppService : AsyncCrudAppService<UserAccountRight, UserAccountRightDto, long, PagedResultRequestDto, UserAccountRightDto, UserAccountRightDto>, IUserAccountRightAppService
    {
        private readonly IRepository<UserAccountRight, long> UserAccountRightrepository;
        private readonly IPermissionManager _permissionManager;

        public UserAccountRightAppService(IRepository<UserAccountRight, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            UserAccountRightrepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserAccountRights_Create)]
        public override async Task<UserAccountRightDto> Create(UserAccountRightDto input)
        {
            var result = ObjectMapper.Map<UserAccountRight>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await UserAccountRightrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserAccountRightDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserAccountRights_Edit)]
        public override async Task<UserAccountRightDto> Update(UserAccountRightDto input)
        {
            var data = ObjectMapper.Map<UserAccountRight>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await UserAccountRightrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserAccountRightDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserAccountRights_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = UserAccountRightrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await UserAccountRightrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<UserAccountRightDto>> GetAccountRights()
        {

            var result = UserAccountRightrepository.GetAll().Include(b => b.User).Include(a => a.ChartOfAccountSubSubDetail)
                .Where(x=> x.ChartOfAccountSubSubDetail.ChartOfAccountSubSubMain.Sub_sub_type == "Cash" || 
                x.ChartOfAccountSubSubDetail.ChartOfAccountSubSubMain.Sub_sub_type == "Bank")
                .ToList();
            return result.MapTo<List<UserAccountRightDto>>();
        }
    }
}
