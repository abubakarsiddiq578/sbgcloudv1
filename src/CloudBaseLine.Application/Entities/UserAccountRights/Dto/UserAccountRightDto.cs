﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Authorization.Users;

namespace CloudBaseLine.Entities.UserAccountRights.Dto
{
    [AutoMapTo(typeof(UserAccountRight)), AutoMapFrom(typeof(UserAccountRightDto))]
    public class UserAccountRightDto : EntityDto<long>
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public long AccountId { get; set; }
        public ChartOfAccountSubSubDetail ChartOfAccountSubSubDetail { get; set; }
        public bool Rights { get; set; }
        public int TenantId { get; set; }
    }
}
