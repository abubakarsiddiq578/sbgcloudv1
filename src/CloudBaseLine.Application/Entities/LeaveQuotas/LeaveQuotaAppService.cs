﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.LeaveQuotaEntities;
using CloudBaseLine.Entities.LeaveQuotas.Dto;
using CloudBaseLine.Entities.LeaveRequests.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.LeaveQuotas
{
    public class LeaveQuotaAppService : AsyncCrudAppService<LeaveQuotaMaster, LeaveQuotaMasterDto, long, PagedResultRequestDto, LeaveQuotaMasterDto, LeaveQuotaMasterDto>, ILeaveQuotaAppService
    {
        private readonly IRepository<LeaveQuotaMaster, long> _LeaveQuotaMasterRepository;
        private readonly IRepository<LeaveQuotaDetail, long> _LeaveQuotaDetailRepositoty;
        private readonly IPermissionManager _permissionManager;
        //private readonly IRepository<LeaveQuotaDetail, long> _LeaveQuotaDetailRepository;
        private readonly IRepository<LeaveType, long> _LeaveTypeRepository;

        public LeaveQuotaAppService(IRepository<LeaveQuotaMaster, long> _repository, IPermissionManager _Manager, IRepository<LeaveType, long> _LeaveType, IRepository<LeaveQuotaDetail, long> _detailRepository) : base(_repository)
        {
            _LeaveQuotaMasterRepository = _repository;
            _LeaveQuotaDetailRepositoty = _detailRepository;
            _permissionManager = _Manager;
            _LeaveTypeRepository = _LeaveType;

        }

        public async Task<List<LeaveQuotaMasterDto>> GetAllLeaveQuota()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _LeaveQuotaMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<LeaveQuotaMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<List<LeaveType>> GetLeaveTypeById(long id)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _LeaveTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.Id == id).ToList();
                return result.MapTo<List<LeaveType>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public async Task<List<LeaveQuotaDetailDto>> GetAllDetailById(long id)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _LeaveQuotaDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.LeaveQuotaMasterId == id)
                    .Include(t => t.LeaveType)
                    .Include(m => m.LeaveQuotaMaster)
                    .ToList();
                return result.MapTo<List<LeaveQuotaDetailDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveQuotas_Create)]
        public override async Task<LeaveQuotaMasterDto> Create(LeaveQuotaMasterDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<LeaveQuotaMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                var response = await _LeaveQuotaMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.LeaveQuotaDetails)
                {
                    //i.LeaveQuotaMasterId = response.Id;
                    ///////////
                    //await _LeaveQuotaDetailRepositoty.InsertAsync(i);
                    i.LeaveQuotaMasterId = response.Id;

                    await _LeaveQuotaDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                var data = result.MapTo<LeaveQuotaMasterDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveQuotas_Edit)]
        /* public override async Task<LeaveQuotaMasterDto> Update(LeaveQuotaMasterDto input)
         {
             //get logged in user's tenant id.
             var TenantId = AbpSession.TenantId;

             if (TenantId == null)
             {
                 TenantId = 0;
             }
             var data = ObjectMapper.Map<LeaveQuotaMaster>(input);
             data.LastModificationTime = DateTime.Now;
             data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);
             data.TenantId = Convert.ToInt32(TenantId);
             var response = await _LeaveQuotaMasterRepository.UpdateAsync(data);

             foreach (var i in input.LeaveQuotaDetails)
             {
                 i.LeaveQuotaMasterId = response.Id;
                 await _LeaveQuotaDetailRepositoty.UpdateAsync(i);
             }

             try
             {
                 CurrentUnitOfWork.SaveChanges();
             }

             catch (Exception ex )
             {
                 throw ex;

             }

             var result = data.MapTo<LeaveQuotaMasterDto>();
             return result;
         }
         */


        public override async Task<LeaveQuotaMasterDto> Update(LeaveQuotaMasterDto input)
        {
            var data = ObjectMapper.Map<LeaveQuotaMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await _LeaveQuotaMasterRepository.UpdateAsync(data);

            foreach (var i in input.LeaveQuotaDetails)
            {
                //i.LeaveQuotaMasterId = response.Id;
                //////
                //await _LeaveQuotaDetailRepositoty.UpdateAsync(i);
                i.LeaveQuotaMasterId = response.Id;

                if (i.Id < 0)
                {
                    await _LeaveQuotaDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _LeaveQuotaDetailRepositoty.UpdateAsync(i);
                }
            }

            var masterResult = _LeaveQuotaMasterRepository.GetAll().Include(x => x.LeaveQuotaDetails).Where(x => x.Id == input.Id).FirstOrDefault();

            bool checkedBit = true;

            foreach (var x in masterResult.LeaveQuotaDetails)
            {
                checkedBit = true;

                foreach (var y in input.LeaveQuotaDetails)
                {
                    if (x.Id == y.Id)
                    {
                        checkedBit = false;
                        break;
                    }
                }

                if (checkedBit == true)
                {
                    if (x != null)
                    {
                        await _LeaveQuotaDetailRepositoty.DeleteAsync(x);
                    }
                }
            }

            CurrentUnitOfWork.SaveChanges();
            var result = data.MapTo<LeaveQuotaMasterDto>();
            return result;
        }

        //Authorization with Permissions
       // [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveQuotas_Delete)]

        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var masterResult = _LeaveQuotaMasterRepository.GetAll().Include(x => x.LeaveQuotaDetails).Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (masterResult != null)
            {
                foreach (var i in masterResult.LeaveQuotaDetails)
                {
                    if (i != null)
                    {
                        await _LeaveQuotaDetailRepositoty.DeleteAsync(i);
                    }
                }

                await _LeaveQuotaMasterRepository.DeleteAsync(masterResult);
                CurrentUnitOfWork.SaveChanges();
            }
        }


        //public override async Task Delete(EntityDto<long> input)
        //{
        //    var result = _LocationInfoRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
        //    if (result != null)
        //    {
        //        await _LocationInfoRepository.DeleteAsync(result);
        //        CurrentUnitOfWork.SaveChanges();
        //    }

        //}

        //public override async Task Delete(EntityDto<long> input)
        //{
        //    var masterResult = _LeaveQuotaMasterRepository.GetAll().Where(x => x.Id == input.Id).Include(x => x.LeaveQuotaDetails).FirstOrDefault();
        //    if (masterResult != null)
        //    {
        //        foreach (var i in masterResult.LeaveQuotaDetails)
        //        {
        //            if (i != null)
        //            {
        //                await _LeaveQuotaDetailRepositoty.DeleteAsync(i);
        //            }
        //        }
        //        await _LeaveQuotaMasterRepository.DeleteAsync(masterResult);
        //        CurrentUnitOfWork.SaveChanges();
        //    }
        //}
    }
}
