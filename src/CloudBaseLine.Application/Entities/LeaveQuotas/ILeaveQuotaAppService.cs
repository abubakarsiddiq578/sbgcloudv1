﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.LeaveQuotas.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LeaveQuotas
{
    public interface ILeaveQuotaAppService : IAsyncCrudAppService<LeaveQuotaMasterDto, long, PagedResultRequestDto, LeaveQuotaMasterDto, LeaveQuotaMasterDto>
    {
    }
}
