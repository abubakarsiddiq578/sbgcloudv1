﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.LeaveQuotaEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LeaveQuotas.Dto
{
    [AutoMapTo(typeof(LeaveQuotaDetail)), AutoMapFrom(typeof(LeaveQuotaDetailDto))]
    public class LeaveQuotaDetailDto : EntityDto<long>
    {
        
        public long LeaveQuotaMasterId { get; set; }

        public LeaveQuotaMaster LeaveQuotaMaster { get; set; }

        
        public long LeaveTypeId { get; set; }

        public LeaveType LeaveType { get; set; }

        public int AllowedLeaves { get; set; }

        public DateTime ResetDate { get; set; }

        public string Notes { get; set; }

        public int TenantId { get; set; }
    }
}
