﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.LeaveQuotaEntities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CloudBaseLine.Entities.LeaveQuotas.Dto
{
    [AutoMapTo(typeof(LeaveQuotaMaster)), AutoMapFrom(typeof(LeaveQuotaMasterDto))]
    public class LeaveQuotaMasterDto : EntityDto<long>
    {
        public string Title { get; set; }

        public DateTime LeaveQuotaYear { get; set; }

        public ICollection<LeaveQuotaDetail> LeaveQuotaDetails { get; set; }

        public LeaveQuotaMasterDto()
        {
            LeaveQuotaDetails = new Collection<LeaveQuotaDetail>();
        }
    }
}
