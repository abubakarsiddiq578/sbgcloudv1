﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.LeaveQuotaEntities;
using CloudBaseLine.Entities.LeaveQuotas.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LeaveQuotas
{
    public class LeaveQuotaDetailAppService : AsyncCrudAppService<LeaveQuotaDetail, LeaveQuotaDetailDto, long, PagedResultRequestDto, LeaveQuotaDetailDto, LeaveQuotaDetailDto>, IleaveQuotaDetailAppService
    {
        private readonly IRepository<LeaveQuotaDetail, long> _LeaveQuotaDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public LeaveQuotaDetailAppService(IRepository<LeaveQuotaDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _LeaveQuotaDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }
}
