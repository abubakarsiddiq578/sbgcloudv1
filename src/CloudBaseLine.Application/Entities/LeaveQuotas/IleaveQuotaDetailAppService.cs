﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LeaveQuotas.Dto
{
    public interface IleaveQuotaDetailAppService : IAsyncCrudAppService<LeaveQuotaDetailDto, long, PagedResultRequestDto, LeaveQuotaDetailDto, LeaveQuotaDetailDto>
    {
    }
}
