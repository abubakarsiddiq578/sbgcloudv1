﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Territories.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Territories
{
    public interface ITerritoryAppService : IAsyncCrudAppService<TerritoryDto,long,PagedResultRequestDto,TerritoryDto,TerritoryDto>
    {
    }
}
