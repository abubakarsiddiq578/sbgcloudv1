﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Territories.Dto
{
    [AutoMapTo(typeof(Territory)),AutoMapFrom(typeof(TerritoryDto))]
    public class TerritoryDto : EntityDto<long>
    {
        public long CityId { get; set; }
        public City City { get; set; }

        public string TerritoryName { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }

        public bool isActive { get; set; }


    }
}
