﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Territories.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Territories
{
    public class TerritoryAppService : AsyncCrudAppService<Territory ,TerritoryDto, long, PagedResultRequestDto, TerritoryDto, TerritoryDto>,ITerritoryAppService
    {
        private readonly IRepository<Territory, long> TerritoryRepository;
        private readonly IPermissionManager _permissionManager;

        public TerritoryAppService(IRepository<Territory, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            TerritoryRepository = _repository;

            _permissionManager = _Manager;

        }



        public async Task<List<TerritoryDto>> GetAllTerritories()
        {

            try
            {
                var a = TerritoryRepository.GetAll().Where(b => b.IsDeleted == false).Include(t=>t.City).ToList();

                return a.MapTo<List<TerritoryDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Territories_Create)]
        public override async Task<TerritoryDto> Create(TerritoryDto input)
        {
            var result = ObjectMapper.Map<Territory>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await TerritoryRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<TerritoryDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Terminations_Edit)]
        public override async Task<TerritoryDto> Update(TerritoryDto input)
        {
            var data = ObjectMapper.Map<Territory>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await TerritoryRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<TerritoryDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Terminations_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = TerritoryRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await TerritoryRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


    }
}
