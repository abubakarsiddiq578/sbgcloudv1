﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.EmployeeDesignations.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.EmployeeDesignations
{
    public class EmployeeDesignationAppService : AsyncCrudAppService<EmployeeDesignation, EmployeeDesignationDto, long, PagedResultRequestDto, EmployeeDesignationDto, EmployeeDesignationDto>, IEmployeeDesignationAppService
    {
        private readonly IRepository<EmployeeDesignation, long> _EmployeeDesignationRepository;
        private readonly IPermissionManager _permissionManager;

        public EmployeeDesignationAppService(IRepository<EmployeeDesignation, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _EmployeeDesignationRepository = _repository;
            _permissionManager = _Manager;
        }

       
        public async Task<List<EmployeeDesignationDto>> GetAllEmployeeDesignations()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _EmployeeDesignationRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<EmployeeDesignationDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeDesignations_Create)]
        public override async Task<EmployeeDesignationDto> Create(EmployeeDesignationDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<EmployeeDesignation>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
            await _EmployeeDesignationRepository.InsertAsync(result);

            
                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<EmployeeDesignationDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeDesignations_Edit)]
        public override async Task<EmployeeDesignationDto> Update(EmployeeDesignationDto input)
        {
            var data = ObjectMapper.Map<EmployeeDesignation>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _EmployeeDesignationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<EmployeeDesignationDto>();
            return result;
        }
        
        
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeDesignations_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = _EmployeeDesignationRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId ).FirstOrDefault();
            if (result != null)
            {
                await _EmployeeDesignationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
