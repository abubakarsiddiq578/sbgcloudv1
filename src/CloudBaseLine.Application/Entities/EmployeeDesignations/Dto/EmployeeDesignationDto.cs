﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeDesignations.Dto
{
    [AutoMapTo(typeof(EmployeeDesignation)), AutoMapFrom(typeof(EmployeeDesignationDto))]
    public class EmployeeDesignationDto : EntityDto<long>
    {
        public string EmployeeDesignationName { get; set; }

        public string Code { get; set; }
        public string Comments { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public int TenantId { get; set; }
    }
}
