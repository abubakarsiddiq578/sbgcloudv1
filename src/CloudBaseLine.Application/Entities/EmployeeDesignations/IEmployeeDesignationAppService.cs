﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.EmployeeDesignations.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeDesignations
{
    public interface IEmployeeDesignationAppService : IAsyncCrudAppService<EmployeeDesignationDto, long, PagedResultRequestDto, EmployeeDesignationDto, EmployeeDesignationDto>
    {
    }
}
