﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.LocationInformaiton.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.LocationInformaiton
{
    
    public class LocationInfoAppService : AsyncCrudAppService<LocationInfo, LocationInfoDto, long, PagedResultRequestDto, LocationInfoDto, LocationInfoDto>, ILocationInfoAppService
    {
        private readonly IRepository<LocationInfo, long> _LocationInfoRepository;
        private readonly IPermissionManager _permissionManager;

        public LocationInfoAppService(IRepository<LocationInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _LocationInfoRepository = _repository;
            _permissionManager = _Manager;
        }



        public async Task<List<LocationInfoDto>> GetAllLocation()
        {
            try
            {
                var result = _LocationInfoRepository.GetAll().Where(a => a.IsDeleted == false).ToList();
                return result.MapTo<List<LocationInfoDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //var result = _UserCompanyRightrepository.GetAllIncluding(p=>p.User);
        }



        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LocationInformations_Create)]
        public override async Task<LocationInfoDto> Create(LocationInfoDto input)
        {
            var result = ObjectMapper.Map<LocationInfo>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await _LocationInfoRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<LocationInfoDto>();
            return data;

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LocationInformations_Edit)]
        public override async Task<LocationInfoDto> Update(LocationInfoDto input)
        {
            var data = ObjectMapper.Map<LocationInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _LocationInfoRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<LocationInfoDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LocationInformations_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _LocationInfoRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _LocationInfoRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
