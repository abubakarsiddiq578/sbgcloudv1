﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ChartsOfAccountSubSubDetail.Dto;
using CloudBaseLine.Entities.LocationInformaiton.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.LocationInformaiton
{
    public interface ILocationInfoAppService : IAsyncCrudAppService<LocationInfoDto, long, PagedResultRequestDto, LocationInfoDto, LocationInfoDto>
    {
        
    }
}
