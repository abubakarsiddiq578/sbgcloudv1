﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LocationInformaiton.Dto
{

    
    

    [AutoMapTo(typeof(LocationInfo)), AutoMapFrom(typeof(LocationInfoDto))]
    public class LocationInfoDto : EntityDto<long>
    {
        public string Location_Code { get; set; }
        public string Location_Name { get; set; }
        public string Comments { get; set; }
        public int Sort_Order { get; set; }
        public string Location_Address { get; set; }
        public string Location_Phone { get; set; }
        public string Location_Fax { get; set; }
        public string Location_Url { get; set; }
        public string Location_Type { get; set; }
        public Boolean RestrictedItems { get; set; }
        public string Mobile_No { get; set; }
        public Boolean AllowMinusStock { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }

    }
}
