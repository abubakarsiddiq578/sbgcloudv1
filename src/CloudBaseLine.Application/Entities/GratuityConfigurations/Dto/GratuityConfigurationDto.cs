﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.GratuityConfigurations.Dto
{
    [AutoMapTo(typeof(GratuityConfiguration)), AutoMapFrom(typeof(GratuityConfigurationDto))]
    

    public class GratuityConfigurationDto : EntityDto<long>
    {

        public string EmployeeId { get; set; }
        

        public long SalaryExpenseTypeId { get; set; }
       

        public string DepartmentId { get; set; }
       

        public string CostCenterId { get; set; }
    

        public long GratuityYearsOfService { get; set; }
        public string GratuityApplicable { get; set; }

        public string GratuityDueOn { get; set; }

        public long GratuityFactor { get; set; }
        public bool RoundingOffGratuity { get; set; }

        public int TenantId { get; set; }

    }
}
