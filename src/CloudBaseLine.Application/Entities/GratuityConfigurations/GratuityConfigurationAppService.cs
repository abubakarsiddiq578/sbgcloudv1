﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.GratuityConfigurations.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.GratuityConfigurations
{
    public class GratuityConfigurationAppService : AsyncCrudAppService<GratuityConfiguration, GratuityConfigurationDto, long, PagedResultRequestDto, GratuityConfigurationDto, GratuityConfigurationDto>, IGratuityConfigurationAppService
    {
        private readonly IRepository<GratuityConfiguration, long> _GratuityConfigurationRepository;
        private readonly IPermissionManager _permissionManager;

        public GratuityConfigurationAppService(IRepository<GratuityConfiguration, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _GratuityConfigurationRepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        //[AbpAuthorize(PermissionNames.Pages_Hrm_Allowances_View)]
        public async Task<List<GratuityConfigurationDto>> GetAllGratuityConfiguration()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _GratuityConfigurationRepository.GetAll()
                    .Where(a => a.IsDeleted == false && a.TenantId == TenantId)
                    //.Include(a => a.Employee)
                    //.Include(b => b.CostCenter)
                    //.Include(c => c.Department)
                    //.Include(d => d.SalaryExpenseType)
                    .ToList();
                return result.MapTo<List<GratuityConfigurationDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        
        public override async Task<GratuityConfigurationDto> Create(GratuityConfigurationDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<GratuityConfiguration>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _GratuityConfigurationRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<GratuityConfigurationDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        
        public override async Task<GratuityConfigurationDto> Update(GratuityConfigurationDto input)
        {// get logged in user's tenant id.

            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }
            var data = ObjectMapper.Map<GratuityConfiguration>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);
            data.TenantId = Convert.ToInt32(TenantId);

            await _GratuityConfigurationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<GratuityConfigurationDto>();
            return result;
        }


       
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _GratuityConfigurationRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await  _GratuityConfigurationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
