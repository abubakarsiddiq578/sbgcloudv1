﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.GratuityConfigurations.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.GratuityConfigurations
{
   public interface IGratuityConfigurationAppService : IAsyncCrudAppService<GratuityConfigurationDto, long, PagedResultRequestDto, GratuityConfigurationDto, GratuityConfigurationDto>
    {
    }
}
