﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ItemWiseProductions.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ItemWiseProductions
{
    public interface IItemWiseProductionMasterAppService : IAsyncCrudAppService<ItemWiseProductionMasterDto, long, PagedResultRequestDto, ItemWiseProductionMasterDto, ItemWiseProductionMasterDto>
    {
        Task<List<ItemWiseProductionMasterDto>> GetAllItemWiseProductions();
        //Task<List<ItemWiseProductionDetailDto>> GetAllDetailById(long id);

    }
}
