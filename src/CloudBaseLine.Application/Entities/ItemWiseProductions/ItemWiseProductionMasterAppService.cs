﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.EmployeeInfo.Dto;
using CloudBaseLine.Entities.ItemWiseProduction;
using CloudBaseLine.Entities.ItemWiseProductions.Dto;
using CloudBaseLine.Entities.WorkGroup;
using CloudBaseLine.Entities.WorkGroups.Dto;

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ItemWiseProductions
{
   

    public class ItemWiseProductionMasterAppService: AsyncCrudAppService <ItemWiseProductionMaster, ItemWiseProductionMasterDto, long, PagedResultRequestDto, ItemWiseProductionMasterDto, ItemWiseProductionMasterDto> ,IItemWiseProductionMasterAppService
    {
        private readonly IRepository<ItemWiseProductionDetail, long> _ItemWiseProductionDetailRepository;
        private readonly IPermissionManager _permissionManager;

        private readonly IRepository<ItemWiseProductionMaster, long> _ItemWiseProductionMasterRepository;

        private readonly IRepository<WorkGroupMaster, long> _WorkGroupMasterRepository;

        private readonly IRepository<WorkGroupEmployeeDetail, long> _WorkGroupEmployeeDetailRepository;

        private readonly IRepository<WorkGroupItemDetail, long> _WorkGroupItemDetailRepository;


        public ItemWiseProductionMasterAppService(IRepository<ItemWiseProductionMaster, long> _repository, IPermissionManager _manager, IRepository<ItemWiseProductionDetail, long> _detailrepository , IRepository<WorkGroupMaster, long> _wrkMasterRepo , IRepository<WorkGroupEmployeeDetail, long> _wrkEmpDetail , IRepository<WorkGroupItemDetail, long> _wrkItemDetail) : base(_repository)
        {

            _ItemWiseProductionMasterRepository = _repository;
            _ItemWiseProductionDetailRepository = _detailrepository;
            _WorkGroupMasterRepository = _wrkMasterRepo;
            _WorkGroupEmployeeDetailRepository = _wrkEmpDetail;
            _WorkGroupItemDetailRepository = _wrkItemDetail;
            _permissionManager = _manager;


        }


        public async Task<List<ItemWiseProductionMasterDto>> GetFilterdItemWiseProductions(DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _ItemWiseProductionMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(b => b.ItemWiseProductionDetails).ToList();

                if (fromDate != null && toDate != null)
                {
                    result = result.Where(a => a.DocDate.Date >= fromDate && a.DocDate.Date <= toDate).ToList();
                }

                return result.MapTo<List<ItemWiseProductionMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public async Task<List<ItemWiseProductionMasterDto>> GetAllItemWiseProductions()
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }


                var result = _ItemWiseProductionMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId ).Include(b=>b.ItemWiseProductionDetails).ToList();

                return result.MapTo<List<ItemWiseProductionMasterDto>>();

               
            }
            catch(Exception ex)
            {

                throw ex;
            }   

        }

        public async Task<List<WorkGroupItemDetailDto>> GetWorkGroupItems(long? itemTypeId , long? subCategoryId , long? categoryId)
        {


            /*try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }
                
                if (itemTypeId == null && subCategoryId == null && categoryId == null)
                {
                    return null;
                }

                
                var result = _WorkGroupEmployeeDetailRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.WorkGroupMaster)
                    .Include(e => e.Employee).ToList();


                if (itemTypeId!=null)
                {
                    result = result.Where(b => b.WorkGroupMaster.ItemTypeId == itemTypeId).ToList();
                }
                if(categoryId !=null)
                {
                    result = result.Where(a => a.WorkGroupMaster.CategoryId == categoryId).ToList();
                }
                if (subCategoryId != null)
                {

                    result = result.Where(a => a.WorkGroupMaster.SubCategoryId == subCategoryId).ToList();
                }
                
                return result.MapTo<List<WorkGroupEmployeeDetailDto>>();


            }
            catch (Exception ex)
            {
                throw ex;
            }*/

            ////////////////Change Requirements///////////////
            ///

            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                if (itemTypeId == null && subCategoryId == null && categoryId == null)
                {
                    return null;
                }


                var result = _WorkGroupItemDetailRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a=>a.WorkGroupMaster).Include(b => b.Item).ToList();

                if (itemTypeId != null && itemTypeId != 0 )
                {
                    result = result.Where(b => b.WorkGroupMaster.ItemTypeId == itemTypeId) .ToList();
                }
                if (categoryId != null && categoryId != 0)
                {
                    result = result.Where(a => a.WorkGroupMaster.CategoryId == categoryId).ToList();
                }
                if (subCategoryId != null && subCategoryId != 0)
                {

                    result = result.Where(a => a.WorkGroupMaster.SubCategoryId == subCategoryId   ).ToList();
                }

                return result.MapTo<List<WorkGroupItemDetailDto>>();


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public async Task<List<WorkGroupMasterDto>> GetMasterData(long? itemTypeId, long? subCategoryId, long? categoryId)
        {

            

            ////////////////Change Requirements///////////////
            ///

            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                if (itemTypeId == null && subCategoryId == null && categoryId == null)
                {
                    return null;
                }


                var result = _WorkGroupMasterRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.WorkGroupItemDetails).Include(a=>a.Category).Include(b=>b.ItemType).Include(c=>c.SubCategory).ToList();

                if (itemTypeId != null && itemTypeId != 0)
                {
                    result = result.Where(b => b.ItemTypeId == itemTypeId).ToList();
                }
                if (categoryId != null && categoryId != 0)
                {
                    result = result.Where(a => a.CategoryId == categoryId).ToList();
                }
                if (subCategoryId != null && subCategoryId != 0)
                {

                    result = result.Where(a => a.SubCategoryId == subCategoryId).ToList();
                }

                return result.MapTo<List<WorkGroupMasterDto>>();


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public async Task<List<ItemWiseProductionDetailDto>> GetAllProductionDetail(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _ItemWiseProductionDetailRepository.GetAll() //_ItemWiseProductionDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.ItemWiseProductionMasterId == id && a.TenantId == TenantId).Include(t => t.WorkGroupItemDetail).Include(t=>t.WorkGroupItemDetail.Item).Include(t=>t.WorkGroupItemDetail.WorkGroupMaster)
                    .ToList();
                if (result.Count == 0)
                {
                    ItemWiseProductionDetail temp = new ItemWiseProductionDetail();
                    temp.WorkGroupItemDetailId = id; // in case of empty data
                    result.Insert(0, temp);


                }
                return result.MapTo<List<ItemWiseProductionDetailDto>>();
                //return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///get workGroupItem Data on basis of id 
        ///
        public async Task<List<WorkGroupItemDetail>> GetWorkGroupItemById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _WorkGroupItemDetailRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.Id == id).Include(a => a.WorkGroupMaster).ToList();

                return result.MapTo<List<WorkGroupItemDetail>>();
            }


            catch (Exception ex)
            {

                throw ex;

            }



        }







                //recieving master id as a parameter//
                /* public async Task<List<ItemWiseProductionDetailDto>> GetAllProductionDetail(long id)
                 {
                     try
                     {
                         var TenantId = AbpSession.TenantId;

                         if (TenantId == null)
                         {
                             TenantId = 0;
                         }

                         var result = _ItemWiseProductionDetailRepository.GetAll() //_ItemWiseProductionDetailRepositoty.GetAll()
                             .Where(a => a.IsDeleted == false && a.ItemWiseProductionMasterId == id && a.TenantId == TenantId).Include(t=>t.WorkGroupMaster).Include(p=>p.WorkGroupMaster.WorkGroupItemDetails)
                             .ToList();
                         if (result.Count == 0 )
                         {
                             ItemWiseProductionDetail temp = new ItemWiseProductionDetail();
                             temp.WorkGroupMasterId = id;
                             result.Insert(0 , temp );


                         }
                         return result.MapTo<List<ItemWiseProductionDetailDto>>();
                         //return result;
                     }
                     catch (Exception ex)
                     {
                         throw ex;
                     }
                 }*/








                public async Task Add(ItemWiseProductionMasterDto input)
        {
            try
            {
                var result = ObjectMapper.Map<ItemWiseProductionMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _ItemWiseProductionMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.ItemWiseProductionDetails)
                {
                    i.ItemWiseProductionMasterId = response.Id;

                    await _ItemWiseProductionDetailRepository.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


               // var data = result.MapTo<ItemWiseProductionMasterDto>();
                //return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task Edit(ItemWiseProductionMasterDto input)
        {
            var data = ObjectMapper.Map<ItemWiseProductionMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);
           
            var response = await _ItemWiseProductionMasterRepository.UpdateAsync(data);
    
            foreach (var i in input.ItemWiseProductionDetails)
            {
                i.ItemWiseProductionMasterId = response.Id;

                if (i.Id < 0)
                {
                    await _ItemWiseProductionDetailRepository.InsertAsync(i);
                }
                else
                {
                    await _ItemWiseProductionDetailRepository.UpdateAsync(i);
                }
            }

            var masterResult = _ItemWiseProductionMasterRepository.GetAll().Include(x => x.ItemWiseProductionDetails).Where(x => x.Id == input.Id).FirstOrDefault();

            bool checkedBit = true;

            foreach (var x in masterResult.ItemWiseProductionDetails)
            {
                checkedBit = true;

                foreach (var y in input.ItemWiseProductionDetails)
                {
                    if (x.Id == y.Id)
                    {
                        checkedBit = false;
                        break;
                    }
                }

                if (checkedBit == true)
                {
                    if (x != null)
                    {
                        await _ItemWiseProductionDetailRepository.DeleteAsync(x);
                    }
                }
            }

            CurrentUnitOfWork.SaveChanges();
        }

        public override async Task<ItemWiseProductionMasterDto> Update(ItemWiseProductionMasterDto input)
        {
            var data = ObjectMapper.Map<ItemWiseProductionMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await _ItemWiseProductionMasterRepository.UpdateAsync(data);

            foreach (var i in input.ItemWiseProductionDetails)
            {
                i.ItemWiseProductionMasterId = response.Id;

                if (i.Id < 0)
                {
                    await _ItemWiseProductionDetailRepository.InsertAsync(i);
                }
                else
                {
                    await _ItemWiseProductionDetailRepository.UpdateAsync(i);
                }
            }

            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ItemWiseProductionMasterDto>();
            return result;
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var masterResult = _ItemWiseProductionMasterRepository.GetAll().Include(x => x.ItemWiseProductionDetails).Where(x => x.Id == input.Id).FirstOrDefault();
            if (masterResult != null)
            {
                foreach (var i in masterResult.ItemWiseProductionDetails)
                {
                    if (i != null)
                    {
                        await _ItemWiseProductionDetailRepository.DeleteAsync(i);
                    }
                }

                await _ItemWiseProductionMasterRepository.DeleteAsync(masterResult);
                CurrentUnitOfWork.SaveChanges();
            }
        }


    }
}
