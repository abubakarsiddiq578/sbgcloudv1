﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.ItemWiseProduction;
using CloudBaseLine.Entities.WorkGroup;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CloudBaseLine.Entities.ItemWiseProductions.Dto
{
    [AutoMapTo(typeof(ItemWiseProductionMaster)), AutoMapFrom(typeof(ItemWiseProductionMasterDto))]
    public class ItemWiseProductionMasterDto : EntityDto<long>
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        public string Remarks { get; set; }

        public long itemTypeId { get; set; }

        public long categoryId { get; set; }

        public long subCategoryId { get; set; }

        public int TenantId { get; set; }

        public virtual ICollection<ItemWiseProductionDetail> ItemWiseProductionDetails { get; set; }

        public ItemWiseProductionMasterDto()
        {
            ItemWiseProductionDetails = new Collection<ItemWiseProductionDetail>();
        }

    }
}
