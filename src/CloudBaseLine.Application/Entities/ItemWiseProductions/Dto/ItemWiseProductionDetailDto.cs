﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.ItemWiseProduction;
using CloudBaseLine.Entities.WorkGroup;
//using CloudBaseLine.Entities.WorkGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ItemWiseProductions.Dto
{
    [AutoMapTo(typeof(ItemWiseProductionDetail)), AutoMapFrom(typeof(ItemWiseProductionDetailDto))]
    public class ItemWiseProductionDetailDto : EntityDto<long>
    {

        public long ItemWiseProductionMasterId { get; set; }

        public virtual ItemWiseProductionMaster ItemWiseProductionMaster { get; set; }

        public double Quantity { get; set; }

     
       // public long WorkGroupMasterId { get; set; }
       // public virtual WorkGroupMaster WorkGroupMaster { get; set; }

        public long WorkGroupItemDetailId { get; set; }

        public virtual WorkGroupItemDetail WorkGroupItemDetail { get; set; }


        public double ExtraQuantity { get; set; }

        public double RepairQuantity { get; set; }

        public double total { get; set; }

        public int TenantId { get; set; }

    }
}
