﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ItemWiseProductions.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ItemWiseProductions
{
    public interface IItemWiseProductionDetailAppService : IAsyncCrudAppService<ItemWiseProductionDetailDto, long, PagedResultRequestDto, ItemWiseProductionDetailDto, ItemWiseProductionDetailDto>
    {



    }
}
