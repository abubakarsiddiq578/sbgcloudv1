﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.ItemWiseProduction;
using CloudBaseLine.Entities.ItemWiseProductions.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ItemWiseProductions
{
    public class ItemWiseProductionDetailAppService : AsyncCrudAppService<ItemWiseProductionDetail , ItemWiseProductionDetailDto , long , PagedResultRequestDto, ItemWiseProductionDetailDto, ItemWiseProductionDetailDto> ,IItemWiseProductionDetailAppService
    {

        private readonly IRepository<ItemWiseProductionDetail, long> _ItemWiseProductionRepository;
        private readonly IPermissionManager _permissionManager;

        public ItemWiseProductionDetailAppService(IRepository<ItemWiseProductionDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ItemWiseProductionRepository = _repository;
            _permissionManager = _Manager;
        }

    }
}
