﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Departments.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Departments
{
    public interface IDepartmentAppService : IAsyncCrudAppService<DepartmentDto, long, PagedResultRequestDto, DepartmentDto, DepartmentDto>
    {
    }
}
