﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Departments.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Departments
{

    
    public class DepartmentAppService : AsyncCrudAppService<Department, DepartmentDto, long, PagedResultRequestDto, DepartmentDto, DepartmentDto>, IDepartmentAppService
    {
        private readonly IRepository<Department, long> _DepartmentRepository;
        private readonly IPermissionManager _permissionManager;

        public DepartmentAppService(IRepository<Department, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _DepartmentRepository = _repository;
            _permissionManager = _Manager;
        }
        
        public async Task<List<DepartmentDto>> GetAllDepartments()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _DepartmentRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<DepartmentDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        
        [AbpAuthorize(PermissionNames.Pages_Hrm_Departments_Create)]
        public override async Task<DepartmentDto> Create(DepartmentDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<Department>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _DepartmentRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<DepartmentDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public override async Task<DepartmentDto> Update(DepartmentDto input)
        {
            var data = ObjectMapper.Map<Department>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _DepartmentRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<DepartmentDto>();
            return result;
        }
        
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Departments_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _DepartmentRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _DepartmentRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
