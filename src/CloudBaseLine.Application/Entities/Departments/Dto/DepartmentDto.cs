﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Departments.Dto
{
    [AutoMapTo(typeof(Department)), AutoMapFrom(typeof(DepartmentDto))]
    public class DepartmentDto : EntityDto<long>
    {
        public string Code { get; set; }

        public string Title { get; set; }

        public int SortOrder { get; set; }

        public string SalaryGroup { get; set; }

        public string Comments { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }
    }
}
