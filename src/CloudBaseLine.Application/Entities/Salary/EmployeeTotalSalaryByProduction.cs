﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class EmployeeTotalSalaryByProduction
    {
        public long EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public DateTime SalaryMonth { get; set; }

        public string GroupName { get; set; }

        public double TotalSalary { get; set; }

    }
}
