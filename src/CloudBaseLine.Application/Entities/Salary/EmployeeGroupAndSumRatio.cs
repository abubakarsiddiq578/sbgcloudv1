﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class EmployeeGroupAndSumRatio
    {
        public string GroupName { get; set; }
        public double RatioSum { get; set; }
    }
}
