﻿using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class EmployeeSalaryList
    {
        public long EmpId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public string EmpDepartment { get; set; }

        public string CostCenterName { get; set; }

        public int DaysInMonth { get; set; }

        public int WorkingDays { get; set; }

        public int AllowedLeaves { get; set; }

        public int AvailedLeaves { get; set; }

        public int CurrentLeaves { get; set; }

        public int LeaveBalance { get; set; }

        public int PresentDays { get; set; }

        public int AbsentDays { get; set; }

        public int AbsentDeduction { get; set; }

        public int OvertimeHrs { get; set; }

        public int Overtime { get; set; }
        public int VisitAllowance { get; set; }

        public int GrossSalary { get; set; }

        public int DeductionAdvAgainstSalary { get; set; }

        public int IncomeTax { get; set; }

        public int GraduityFund { get; set; }

        public int TotalSalary { get; set; }

        public DateTime salaryMonth { get; set; }

        public string SalaryType { get; set; }


        public ICollection<EmployeeSalaryDetailList> EmployeeSalaryDetailList { get; set; }

        public EmployeeSalaryList()
        {
            EmployeeSalaryDetailList = new Collection<EmployeeSalaryDetailList>();
        }



    }
}
