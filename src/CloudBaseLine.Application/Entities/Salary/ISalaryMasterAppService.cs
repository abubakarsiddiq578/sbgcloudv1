﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Salary;
using CloudBaseLine.Entities.Salary.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Salary
{
    public interface ISalaryMasterAppService : IAsyncCrudAppService<SalaryMasterDto, long, PagedResultRequestDto, SalaryMasterDto, SalaryMasterDto>
    {
        //Task<List<SalaryMasterDto>> GetAllSalaries();
        //Task<List<SalaryDetailDto>> GetAllDetailById(long id);
    }
}
