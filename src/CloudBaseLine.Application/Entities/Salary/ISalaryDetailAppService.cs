﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Salary.Dto;
using System;
using System.Collections.Generic;
using System.Text;


namespace CloudBaseLine.Entities.Salary
{
    public interface ISalaryDetailAppService : IAsyncCrudAppService<SalaryDetailDto, long, PagedResultRequestDto, SalaryDetailDto, SalaryDetailDto>
    {
    
    }
}
