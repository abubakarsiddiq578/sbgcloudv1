﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class EmployeeItemWiseProductionTotalSalary
    {
        public string ItemName { get; set; }
        public string EmpName { get; set; }

        public double Total { get; set; }

        public string GroupName { get; set; }

        public long EmpId { get; set; }

        public DateTime salaryMonth { get; set; }
    }
}
