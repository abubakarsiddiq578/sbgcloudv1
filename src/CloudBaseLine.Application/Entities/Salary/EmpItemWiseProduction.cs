﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class EmpItemWiseProduction
    {
        public string ItemName { get; set; }

        public string GroupName { get; set; }

        public int ProducedItem { get; set; }

        public string EmpName { get; set; }

        public long EmpId { get; set; }

        public DateTime ProductionDate { get; set; }

        public double EmpRatio { get; set; }

        public double empPerDayRate { get; set; }
    }
}
