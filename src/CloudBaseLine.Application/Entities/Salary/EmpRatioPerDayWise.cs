﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class EmpRatioPerDayWise
    {
        public long EmpId { get; set; }
        public string EmpName { get; set; }

        public DateTime statusDate { get; set; }

        public double empRatio { get; set; }
    }
}
