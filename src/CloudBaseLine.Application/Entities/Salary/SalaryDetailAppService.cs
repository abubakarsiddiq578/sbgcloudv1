﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Salary;
using CloudBaseLine.Entities.Salary.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class SalaryDetailAppService : AsyncCrudAppService<SalaryDetail, SalaryDetailDto, long, PagedResultRequestDto, SalaryDetailDto, SalaryDetailDto>, ISalaryDetailAppService
    {
        private readonly IRepository<SalaryDetail, long> _SalaryDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public SalaryDetailAppService(IRepository<SalaryDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _SalaryDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }
}
