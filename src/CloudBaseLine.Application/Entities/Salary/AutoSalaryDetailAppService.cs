﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Salary.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class AutoSalaryDetailAppService : AsyncCrudAppService<AutoSalaryDetail, AutoSalaryDetailDto, long, PagedResultRequestDto, AutoSalaryDetailDto, AutoSalaryDetailDto>, IAutoSalaryDetailAppService
    {
        private readonly IRepository<AutoSalaryDetail, long> _AutoSalaryDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public AutoSalaryDetailAppService(IRepository<AutoSalaryDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _AutoSalaryDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }
}
