﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.Salary;
using CloudBaseLine.Entities.SalaryGroup;
using System;
using System.Collections.Generic;
using System.Text;


namespace CloudBaseLine.Entities.Salary.Dto
{
    [AutoMapTo(typeof(SalaryMaster)), AutoMapFrom(typeof(SalaryMasterDto))]
    public class SalaryMasterDto : EntityDto<long>
    {
        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public long SalaryGroupMasterId { get; set; }
        public SalaryGroupMaster SalaryGroupMaster { get; set; }
        public double AnnualGrossSalary { get; set; }
        public virtual IEnumerable<SalaryDetail> SalaryDetails { get; set; }
        public int TenantId { get; set; }
    }
}
