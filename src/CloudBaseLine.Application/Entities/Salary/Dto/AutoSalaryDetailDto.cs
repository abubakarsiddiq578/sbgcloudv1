﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary.Dto
{
    [AutoMapTo(typeof(AutoSalaryDetail)), AutoMapFrom(typeof(AutoSalaryDetailDto))]
    public class AutoSalaryDetailDto : EntityDto<long>
    {
        public long AutoSalaryId { get; set; }
        
        public AutoSalary AutoSalary { get; set; }
        
        public long SalaryItemTypeId { get; set; }

        //public SalaryExpenseType SalaryExpenseType { get; set; }

        public string salaryItemType { get; set; }

        public int Earning { get; set; }

        public int Deduction { get; set; }
    }
}
