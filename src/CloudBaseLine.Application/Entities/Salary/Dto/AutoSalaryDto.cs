﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CloudBaseLine.Entities.Salary.Dto
{
    [AutoMapTo(typeof(AutoSalary)), AutoMapFrom(typeof(AutoSalaryDto))]
    public class AutoSalaryDto : EntityDto<long>
    {
        public long EmpId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public string EmpDepartment { get; set; }

        public string CostCenterName { get; set; }

        public int DaysInMonth { get; set; }

        public int WorkingDays { get; set; }

        public int AllowedLeaves { get; set; }

        public int AvailedLeaves { get; set; }

        public int CurrentLeaves { get; set; }

        public double LeaveBalance { get; set; }

        public int PresentDays { get; set; }

        public int AbsentDays { get; set; }

        public double AbsentDeduction { get; set; }

        public int OvertimeHrs { get; set; }

        public double Overtime { get; set; }
        public double VisitAllowance { get; set; }

        public double GrossSalary { get; set; }

        public double DeductionAdvAgainstSalary { get; set; }

        public double IncomeTax { get; set; }

        public double GraduityFund { get; set; }

        public double TotalSalary { get; set; }

        public string SalaryType { get; set; }

        public DateTime SalaryMonth { get; set; }

        public ICollection<AutoSalaryDetail> AutoSalaryDetails { get; set; }
        

        public AutoSalaryDto()
        {
            AutoSalaryDetails = new Collection<AutoSalaryDetail>();
        }
    }
}
