﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.Salary;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary.Dto
{
    [AutoMapTo(typeof(SalaryDetail)), AutoMapFrom(typeof(SalaryDetailDto))]
    public class SalaryDetailDto : EntityDto<long>
    {
        public long SalaryMasterId { get; set; }
        public virtual SalaryMaster SalaryMaster { get; set; }

        public string SalaryItem { get; set; }

        public double Amount { get; set; }

        public int TenantId { get; set; }
    }
}
