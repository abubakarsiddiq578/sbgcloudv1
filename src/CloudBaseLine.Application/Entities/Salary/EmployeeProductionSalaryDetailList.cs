﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Salary
{
    public class EmployeeProductionSalaryDetailList
    {
        public long Id { get; set; }
        //public int employeeSalaryListId { get; set; }
        //public string SalaryTypeItem { get; set; }
        //public long EmployeeSalaryTypeId { get; set; }
        public double Earning { get; set; }
        //public int EmpSalaryDeduction { get; set; }
        public long employeeId { get; set; }
        
        public long ProductionSalaryId { get; set; }
        public ProductionSalary ProductionSalary { get; set; }
        public long SalaryItemTypeId { get; set; }
        //public SalaryExpenseType SalaryExpenseType { get; set; }
        public string salaryItemType { get; set; }
        public double Deduction { get; set; }
    }
}
