﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Salary.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Salary
{
    public class AutoSalaryAppService : AsyncCrudAppService<AutoSalary, AutoSalaryDto, long, PagedResultRequestDto, AutoSalaryDto, AutoSalaryDto>, IAutoSalaryAppService
    {
        private readonly IRepository<AutoSalary, long> AutoSalaryRepository;
        private readonly IPermissionManager _permissionManager;
        private readonly IRepository<AutoSalaryDetail, long> _AutoSalaryDetailRepository;


        public AutoSalaryAppService(IRepository<AutoSalary, long> _repository,
            IRepository<AutoSalaryDetail, long> _autoSalaryDetailRepository,
            IPermissionManager _Manager) : base(_repository)
        {

            AutoSalaryRepository = _repository;
            _AutoSalaryDetailRepository = _autoSalaryDetailRepository;
            _permissionManager = _Manager;

        }


        public async Task<Array> getAllGeneratedSalaries()
        {
            try
            {
                var result = AutoSalaryRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.AutoSalaryDetails).ToList();
                return result.ToArray();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }




        public async Task AddAutoSalary(AutoSalaryDto input)
        {
            try
            {

                var generatedSalaries = AutoSalaryRepository.GetAll().Where(a => a.IsDeleted == false &&
            a.SalaryMonth.Month == input.SalaryMonth.Month  && a.CreationTime.Year == input.SalaryMonth.Year && a.EmpId == input.EmpId
            && a.SalaryType == input.SalaryType
            ).FirstOrDefault();

                if(generatedSalaries != null)
                {
                    var result = ObjectMapper.Map<AutoSalary>(input);
                    result.LastModificationTime = DateTime.Now;
                    result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    var response = await AutoSalaryRepository.InsertAsync(result);
                    foreach (var i in input.AutoSalaryDetails)
                    {
                        var result1 = ObjectMapper.Map<AutoSalaryDetail>(i);
                        result1.AutoSalaryId = response.Id;
                        await _AutoSalaryDetailRepository.InsertAsync(result1);
                    }
                    try
                    {
                        CurrentUnitOfWork.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                //else
                //{
                //    var alreadyGeneratedSalary = "this employee's(" + input.Name + ") Salary is already generated in this("+ input.SalaryMonth.Month + ")";

                //    return Task<alreadyGeneratedSalary>;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task EditGeneratedAutoSalary(AutoSalaryDto input)
        {
            var data = ObjectMapper.Map<AutoSalary>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await AutoSalaryRepository.UpdateAsync(data);

            foreach (var i in input.AutoSalaryDetails)
            {
                var result1 = ObjectMapper.Map<AutoSalaryDetail>(i);
                result1.AutoSalaryId = response.Id;

                if (result1.Id < 0)
                {
                    await _AutoSalaryDetailRepository.InsertAsync(result1);
                }
                else
                {
                    await _AutoSalaryDetailRepository.UpdateAsync(result1);
                }
            }

            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            

            
        }



    }
}
