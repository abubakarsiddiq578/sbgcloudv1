﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Configuration;
using CloudBaseLine.Entities.CostCenters.Dto;
using CloudBaseLine.Entities.ItemWiseProduction;
using CloudBaseLine.Entities.LeaveQuotaEntities;
using CloudBaseLine.Entities.Salary;
using CloudBaseLine.Entities.Salary.Dto;
using CloudBaseLine.Entities.WorkGroup;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Salary
{
    public class SalaryMasterAppService : AsyncCrudAppService<SalaryMaster, SalaryMasterDto, long, PagedResultRequestDto, SalaryMasterDto, SalaryMasterDto>, ISalaryMasterAppService
    {
        private readonly IRepository<SalaryMaster, long> _SalaryMasterRepository;
        private readonly IRepository<SalaryDetail, long> _SalaryDetailRepositoty;
        private readonly IPermissionManager _permissionManager;
        
        private readonly IRepository<CostCenter, long> _CostCenterRepository;
        private readonly IRepository<Deduction, long> _DeductionRepository;
        private readonly IRepository<Fine, long> _FineRepository;
        private readonly IRepository<Allowance, long> _AllowanceRepository;
        //private readonly IRepository<CostCenter, long> _CostCenterRepository;
        private readonly IRepository<Attendance, long> _AttendanceRepository;
        private readonly IRepository<Overtime, long> _OvertimeRepository;
        private readonly IRepository<OvertimeConfiguration, long> _OvertimeConfiguration;
        private readonly IRepository<AdvanceRequest, long> _AdvanceRequestRepository;
        private readonly IRepository<AutoSalary, long> _AutoSalaryRepository;
        private readonly IRepository<AutoSalaryDetail, long> _AutoSalaryDetailRepository;
        private readonly IRepository<SalaryExpenseType, long> _SalaryExpenseTypeRepository;
        private readonly IRepository<WorkGroupMaster, long> _WorkGroupMasterRepository;
        private readonly IRepository<WorkGroupEmployeeDetail, long> _WorkGroupDetailRepository;
        
        private readonly IRepository<ItemWiseProductionMaster, long> _ItemWiseProductionMasterRepository;
        private readonly IRepository<ItemWiseProductionDetail, long> _ItemWiseProductionDetailRepository;

        private readonly IRepository<Item, long> _ItemRepository;

        private readonly IRepository<ProductionRate, long> _ProductionRepository;

        private readonly IRepository<LeaveQuotaMaster, long> _LeaveQuotaMasterRepository;

        private readonly IRepository<Employee, long> _EmployeeRepository;

        public SalaryMasterAppService(
            IRepository<SalaryMaster, long> _repository,
            IPermissionManager _Manager,
            IRepository<SalaryDetail, long> _detailRepository,
            IRepository<CostCenter, long> _costCenterRepository,
            IRepository<Deduction, long> _deductionRepository,
            IRepository<Fine, long> _fineRepository,
            IRepository<Allowance, long> _allowanceRepository,
            IRepository<Employee, long> _employeeRepository,
            IRepository<Attendance, long> _attendanceRepository,
            IRepository<Overtime, long> _overtimeRepository,
            IRepository<OvertimeConfiguration, long> _overtimeConfigurationRepository,
            IRepository<AdvanceRequest, long> _advanceRequestRepository,
            IRepository<AutoSalary, long> _autoSalaryRepository,
            IRepository<AutoSalaryDetail, long> _autoSalaryDetailRepository,
            IRepository<SalaryExpenseType, long> _salaryExpenseTypeRepository,
            IRepository<WorkGroupMaster, long> _workGroupMasterRepository,
            IRepository<WorkGroupEmployeeDetail, long> _workGroupDetailRepository,
            IRepository<ItemWiseProductionMaster, long> _itemWiseProductionMaterRepository,
            IRepository<ItemWiseProductionDetail, long> _itemwiseProductionDetailRepository,
            IRepository<Item, long> _itemRepository,
            IRepository<ProductionRate, long> _productionRepository,
            IRepository<LeaveQuotaMaster, long> _leaveQuotaMasterRepository


            ) : base(_repository)
        //public SalaryMasterAppService(IRepository<SalaryMaster, long> _repository, IPermissionManager _Manager, IRepository<SalaryDetail, long> _detailRepository) : base(_repository)
        {
            _SalaryMasterRepository = _repository;
            _SalaryDetailRepositoty = _detailRepository;
            _permissionManager = _Manager;


            _CostCenterRepository = _costCenterRepository;
            _DeductionRepository = _deductionRepository;
            _FineRepository = _fineRepository;
            _AllowanceRepository = _allowanceRepository;
            _EmployeeRepository = _employeeRepository;
            _AttendanceRepository = _attendanceRepository;
            _OvertimeRepository = _overtimeRepository;
            _OvertimeConfiguration = _overtimeConfigurationRepository;
            _AdvanceRequestRepository = _advanceRequestRepository;
            _AutoSalaryRepository = _autoSalaryRepository;
            _AutoSalaryDetailRepository = _autoSalaryDetailRepository;
            _SalaryExpenseTypeRepository = _salaryExpenseTypeRepository;
            _WorkGroupMasterRepository = _workGroupMasterRepository;
            _WorkGroupDetailRepository = _workGroupDetailRepository;
            _ItemWiseProductionMasterRepository = _itemWiseProductionMaterRepository;
            _ItemWiseProductionDetailRepository = _itemwiseProductionDetailRepository;
            _ItemRepository = _itemRepository;
            _ProductionRepository = _productionRepository;
            _LeaveQuotaMasterRepository = _leaveQuotaMasterRepository;
        }

      

        public async Task<Array> getAllSalaryTypes()
        {
            try
            {
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _SalaryExpenseTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Select(a => a.SalaryExpType).ToList();
                return result.ToArray();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }



        public async Task<Array> getSalaryWizard(string payroll, DateTime? salaryDate, string costCenterHead, long? costCenter, long? department, bool? deduction, bool? fine, bool? allowance, bool? bonus)
        {
            var TenantId = AbpSession.TenantId;
            if(TenantId == null)
            {
                TenantId = 0;
            }

            List<EmployeeSalaryList> employeeSalaryList = new List<EmployeeSalaryList>();
            List<EmployeeSalaryDetailList> employeeSalaryDetailList = new List<EmployeeSalaryDetailList>();
            List<EmployeeProductionSalaryList> employeeProductionSalaryList = new List<EmployeeProductionSalaryList>();
            List<EmployeeProductionSalaryDetailList> employeeProductionSalaryDetailList = new List<EmployeeProductionSalaryDetailList>();
            List<EmployeeGroupList> employeeGroupList = new List<EmployeeGroupList>();
            List<EmployeeGroupAndSumRatio> employeeGroupAndSumRatio = new List<EmployeeGroupAndSumRatio>();
            List<EmpItemWiseProduction> empItemWiseProductionList = new List<EmpItemWiseProduction>();
            List<EmpRatioPerDayWise> empRatioPerDayWise = new List<EmpRatioPerDayWise>();
            List<EmployeeItemWiseProductionTotalSalary> employeeItemWiseProductionTotalSalary = new List<EmployeeItemWiseProductionTotalSalary>();
            List<EmployeeTotalSalaryByProduction> employeeTotalSalaryByProduction = new List<EmployeeTotalSalaryByProduction>();






            //overtime Rate start

            var overtimeConfiguration = _OvertimeConfiguration.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Select(a => a.RegularDayHourRate).FirstOrDefault();
            



            //overtime rate ends

            //previous
            //var result = _EmployeeRepository.GetAll().Where(d => d.IsDeleted == false).Include(c => c.CostCenter).ToList();

            //latest
            try
            {
                var result = _EmployeeRepository.GetAll().Where(d => d.IsDeleted == false && d.TenantId == TenantId).Include(c => c.CostCenter).Include(d => d.EmployeeDesignation).Include(d => d.Department).ToList();

                var deductoins = _DeductionRepository.GetAll().Where(d => d.IsDeleted == false && d.TenantId == TenantId).Select(e => e.EmployeeId).ToList();
                var fines = _FineRepository.GetAll().Where(d => d.IsDeleted == false && d.TenantId == TenantId).Select(e => e.EmployeeId).ToList();
                var allowances = _AllowanceRepository.GetAll().Where(d => d.IsDeleted == false && d.TenantId == TenantId).Select(e => e.EmployeeId).ToList();
                var costCenterHeads = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Select(a => a.CostCenterGroup).Distinct().ToList();

                if (payroll != "" && payroll != null)
                {
                    result = result.Where(p => p.PayRoll_Division == payroll).ToList();
                }

                if (costCenterHead != null && costCenterHead != "")
                {
                    try
                    {
                        //costCenterHeads.Contains(c.CostCenter.CostCenterGroup))
                        result = result.Where(c => c.CostCenter.CostCenterGroup == costCenterHead).ToList();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }

                if (costCenter != 0 && costCenter != null)
                {

                    if (costCenterHead != null && costCenterHead != "")
                    {
                        result = result.Where(c => c.CostCenterId == costCenter && c.CostCenter.CostCenterGroup == costCenterHead).ToList();
                    }
                    else
                    {
                        result = result.Where(c => c.CostCenterId == costCenter).ToList();
                    }
                }

                if (department != null && department != 0)
                {
                    result = result.Where(a => a.Dept_ID == department).ToList();
                }

                //if(deduction == true && deduction != null)
                //{
                //    result = result.Where(a => deductoins.Contains(a.Id )).ToList();
                //}

                //if(fine == true && fine != null)
                //{
                //for(var i = 0; i < fines.Count(); i++)
                //{
                //result = result.Where(a => fines.Contains(a.Id)).ToList();
                //}

                //}

                //if(allowance == true && allowance != null)
                //{
                //for (var i = 0; i < fines.Count(); i++)
                //{
                //result = result.Where(a => allowances.Contains(a.Id)).ToList();
                //}
                //}

                //getting groups
                var empGroups = _WorkGroupDetailRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.WorkGroupMaster).Select(a => new { a.EmployeeId, a.WorkGroupMaster.GroupName }).ToList();
                
                for (int k = 0; k < result.Count(); k++)
                {
                    employeeSalaryList.Add(new EmployeeSalaryList
                    {
                        EmpId = result[k].Id,
                        Code = result[k].Employee_Code,
                        Name = result[k].Employee_Name,
                        Designation = result[k].EmployeeDesignation.EmployeeDesignationName,
                        EmpDepartment = result[k].Department.Title,
                        CostCenterName = result[k].CostCenter.Name
                    });

                    employeeProductionSalaryList.Add(new EmployeeProductionSalaryList
                    {
                        EmpId = result[k].Id,
                        Code = result[k].Employee_Code,
                        Name = result[k].Employee_Name,
                        Designation = result[k].EmployeeDesignation.EmployeeDesignationName,
                        EmpDepartment = result[k].Department.Title,
                        CostCenterName = result[k].CostCenter.Name
                    });
                    var empGroupsResult = _WorkGroupDetailRepository.GetAll()
                            .Where(a => a.EmployeeId == result[k].Id)
                            .Include(a => a.WorkGroupMaster)
                            .Select(a => new { a.EmployeeId, a.WorkGroupMaster.GroupName }).ToList();
                    for(int g = 0; g < empGroupsResult.Count(); g++)
                    {
                        employeeGroupList.Add(new EmployeeGroupList
                        {
                            empId = empGroupsResult[g].EmployeeId,
                            empName = empGroupsResult[g].GroupName
                        });
                    }
                    

                }

                //distinct groups
                var groupDistincts = employeeGroupList.Select(a => a.empName).Distinct().ToList();


                if (payroll == "Monthly")
                {
                    try
                    {
                        //week days list
                        List<DayOfWeek> weekOffDays = new List<DayOfWeek>() { DayOfWeek.Friday };


                        //salary Date Parameter
                        DateTime fromDate = new DateTime();
                        fromDate = salaryDate.Value;
                        if (fromDate == null)
                        {
                            var nullDate = "Please enter date time";
                            return nullDate.ToArray();
                        }
                        //DateTime fromDate = new DateTime();
                        //fromDate = DateTime.Parse("12/01/2018");
                        DateTime toDate = new DateTime();
                        int salaryMonth = fromDate.Month;
                        int salaryYear = fromDate.Year;
                        int daysOfMonth = DateTime.DaysInMonth(salaryYear, salaryMonth);
                        toDate = DateTime.Parse(salaryMonth + "/" + daysOfMonth + "/" + salaryYear);





                        var employeeAttendance = _AttendanceRepository.GetAll().Where(d => d.IsDeleted == false && d.TenantId == TenantId).Include(t => t.AttendanceTypes).Select(v => new { v.EmployeeId, v.AttendanceDate, v.AttendanceTypes.Status }).ToList();
                        var employeeStatuses = _AttendanceRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.AttendanceDate >= fromDate && a.AttendanceDate <= toDate).Select(a => new { a.EmployeeId, a.AttendanceDate, a.AttendanceTypes.Status }).GroupBy(a => a.EmployeeId).Select(a => a.ToList()).ToList();



                        //Object record = new object[] { };

                        for (int i = 0; i < employeeStatuses.Count(); i++)
                        {
                            int presentStatus = 0;
                            int absentStatus = 0;
                            int leaveStatus = 0;
                            int halfLeaveStatus = 0;
                            int shortLeaveStatus = 0;
                            int anualLeaveStatus = 0;
                            int sickLeaveStatus = 0;
                            int outDoorDutyStatus = 0;
                            int breakStatus = 0;
                            int offDayStatus = 0;
                            int gezzettedLeaveStatus = 0;
                            int maternityLeaveStatus = 0;
                            int shortAbsentStatus = 0;
                            

                            var businessDays = 0;
                            for (var date = fromDate; date <= toDate; date = date.AddDays(1))
                            {
                                if (date.DayOfWeek != DayOfWeek.Friday)
                                    businessDays++;
                            }




                            //overtime
                            int overtimeRate = 0;
                            //if (overtimeConfiguration != null)
                            //{
                            //    overtimeRate = overtimeConfiguration.RegularDayHourRate;
                            //}
                            int empOvertimeResult = 0;
                            int empAdvanceRequestResult = 0;

                            
                            var getRecords = employeeStatuses[i].ToList();
                            var dateWiseRecord = getRecords.Where(a => a.AttendanceDate >= fromDate && a.AttendanceDate <= toDate).ToList();

                            for (int k = 0; k < result.Count(); k++)
                            {


                                //    var generatedSalaries = _AutoSalaryRepository.GetAll().Where(a => a.IsDeleted == false &&
                                //a.CreationTime.Month == salaryMonth && a.CreationTime.Year == salaryYear && a.EmpId == result[k].Id
                                //).FirstOrDefault();

                                //    if (generatedSalaries != null)
                                //    {
                                //        var alreadyGenerated = "salary is already generated of this employee";
                                //        return alreadyGenerated.ToArray();
                                //    }

                                int totalLeaves = 0;
                                int allowedLeaves = 0;
                                int availedLeaves = 0;
                                int currentLeaves = 0;
                                int leaveBalance = 0;
                                int resetYearResult = 0;
                                int resetMonthResult = 0;
                                var dateMonthSpan = 0;
                                DateTime resetDateResult = new DateTime();

                                var allowedLeavess = _LeaveQuotaMasterRepository.GetAll()
                                    .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.LeaveQuotaYear.Year == salaryYear)
                                    .Include(a => a.LeaveQuotaDetails).ToList();

                                for(int l = 0; l < allowedLeavess.Count(); l++ )
                                {
                                    var eachYearAllowedLeave = allowedLeavess[l].LeaveQuotaDetails.ToList();
                                    if(eachYearAllowedLeave.Count() != 0 && eachYearAllowedLeave.Count() != null)
                                    {
                                        for(int eachYearLeave = 0; eachYearLeave < eachYearAllowedLeave.Count(); eachYearLeave++)
                                        {
                                            resetMonthResult = eachYearAllowedLeave[eachYearLeave].ResetDate.Month;
                                            resetYearResult = eachYearAllowedLeave[eachYearLeave].ResetDate.Year;
                                            resetDateResult = eachYearAllowedLeave[eachYearLeave].ResetDate;

                                            if (fromDate.Year <= resetYearResult && fromDate.Month <= resetMonthResult)
                                            {
                                                totalLeaves = totalLeaves + eachYearAllowedLeave[eachYearLeave].AllowedLeaves;
                                            }
                                        }
                                    }
                                }
                                
                                
                                dateMonthSpan = ((resetDateResult.Year - fromDate.Year) * 12) + resetDateResult.Month - fromDate.Month;

                                allowedLeaves = totalLeaves / dateMonthSpan;

                                //((date1.Year - date2.Year) * 12) + date1.Month - date2.Month

                                
                                
                                int workingDays = businessDays;

                                //attendance
                                int perAbsentDayDeduction = 0;
                                int absentDaysDeduct = 0;

                                //salary
                                int empBasicSalaryResult = 0;
                                int empGrossSalary = 0;


                                //allowance
                                int empAllowanceResult = 0;
                                int empVisitAllowanceResult = 0;
                                int empFineresult = 0;
                                int empDeductionResult = 0;
                                int totalSalary = 0;
                                int totaldeductions = 0;
                                int totalBasicAndAllowance = 0;
                                //employee salary start

                                var empSalary = _SalaryMasterRepository.GetAll()
                                    .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).Include(a => a.SalaryDetails).ToList();

                                if (empSalary.Count() > 0)
                                {
                                    for (int l = 0; l < empSalary.Count(); l++)
                                    {
                                        empGrossSalary = Convert.ToInt32(empSalary[l].AnnualGrossSalary);
                                        empBasicSalaryResult = Convert.ToInt32(empSalary[l].AnnualGrossSalary);
                                        //int allow = Convert.ToInt32(empSalary[l].SalaryDetails.FirstOrDefault());

                                        var resultAllow = empSalary[l].SalaryDetails.ToList();
                                        //var groooooos = empSalary[l].AnnualGrossSalary;
                                        for (var si = 0; si < resultAllow.Count(); si++)
                                        {
                                            if (i == 0)
                                            {
                                                employeeSalaryDetailList.Add(new EmployeeSalaryDetailList
                                                {
                                                    salaryItemType = resultAllow[si].SalaryItem,
                                                    SalaryItemTypeId = resultAllow[si].Id,
                                                    Earning = resultAllow[si].Amount,
                                                    employeeId = result[k].Id
                                                });
                                                //empAllowanceResult = empAllowanceResult + Convert.ToInt32(resultAllow[si].Amount);
                                            }

                                        }
                                        perAbsentDayDeduction = empBasicSalaryResult / workingDays;
                                        //leaveBalance = currentLeaves * perAbsentDayDeduction;


                                    }
                                    //    if(i == 0)
                                    //{
                                    //    employeeSalaryList.Where(a => a.EmpId == result[k].Id).Select(c =>
                                    //    {
                                    //        foreach (var data in employeeSalaryDetailList)
                                    //        {
                                    //            c.EmployeeSalaryDetailList.
                                    //        }
                                    //        return c;
                                    //    }).ToList();
                                    //}
                                }







                                //employee salary ends

                                //Advance Request of Employee start
                                var empAdvanceRequest = _AdvanceRequestRepository.GetAll()
                                            .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                if (empAdvanceRequest.Count() > 0)
                                {
                                    for (int ar = 0; ar < empAdvanceRequest.Count(); ar++)
                                    {
                                        empAdvanceRequestResult = empAdvanceRequestResult + Convert.ToInt32(empAdvanceRequest[ar].Amount);
                                    }
                                }
                                //Advance Request Of Employee Ends


                                //allowed leaves start
                                //allowed leaves ends
                                //employee overtime start
                                var empOvertime = _OvertimeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                if (empOvertime.Count() > 0)
                                {
                                    for (int m = 0; m < empOvertime.Count(); m++)
                                    {
                                        empOvertimeResult = empOvertimeResult + Convert.ToInt32(empOvertime[m].OverTimerHour);
                                    }
                                    overtimeRate = overtimeRate * empOvertimeResult;
                                }
                                else
                                {
                                    overtimeRate = 0;
                                }
                                //employee overtime ends

                                //employee allowance start
                                if (allowance == true && allowance != null)
                                {
                                    var empAllowance = _AllowanceRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                    if (empAllowance.Count() > 0)
                                    {
                                        for (int n = 0; n < empAllowance.Count(); n++)
                                        {
                                            empVisitAllowanceResult = empVisitAllowanceResult + Convert.ToInt32(empAllowance[n].Amount);
                                        }
                                    }
                                }
                                //employee allowance ends.

                                //fine result starts
                                if (fine == true && fine != null)
                                {
                                    var empFine = _FineRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                    if (empFine.Count() > 0)
                                    {
                                        for (int f = 0; f < empFine.Count(); f++)
                                        {
                                            empFineresult = empFineresult + Convert.ToInt32(empFine[f].Amount);
                                        }
                                    }
                                }
                                //fine result ends
                                // deduction start
                                if (deduction == true && deduction != null)
                                {
                                    var empDeduction = _DeductionRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                    if (empDeduction.Count() > 0)
                                    {
                                        for (int d = 0; d < empDeduction.Count(); d++)
                                        {
                                            empDeductionResult = empDeductionResult + Convert.ToInt32(empDeduction[d].Amount);
                                        }
                                    }
                                }
                                //deductioni ends

                                for (int j = 0; j < dateWiseRecord.Count(); j++)
                                {
                                    if (result[k].Id == dateWiseRecord[j].EmployeeId)
                                    {
                                        if (dateWiseRecord[j].Status == "Present")
                                        {
                                            presentStatus = presentStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "Absent")
                                        {
                                            absentStatus = absentStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "Leave")
                                        {
                                            leaveStatus = leaveStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "HalfLeave")
                                        {
                                            halfLeaveStatus = halfLeaveStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "ShortLeave")
                                        {
                                            shortLeaveStatus = shortLeaveStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "AnualLeave")
                                        {
                                            anualLeaveStatus = anualLeaveStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "SickLeave")
                                        {
                                            sickLeaveStatus = sickLeaveStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "OutdoorDuty")
                                        {
                                            outDoorDutyStatus = outDoorDutyStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "OffDay")
                                        {
                                            offDayStatus = offDayStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "GazzettedLeave")
                                        {
                                            gezzettedLeaveStatus = gezzettedLeaveStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "MaternityLeave")
                                        {
                                            maternityLeaveStatus = maternityLeaveStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "ShortAbsent")
                                        {
                                            shortAbsentStatus = shortAbsentStatus + 1;
                                        }
                                        if (dateWiseRecord[j].Status == "Break")
                                        {
                                            breakStatus = breakStatus + 1;
                                        }


                                        if (j == dateWiseRecord.Count() - 1)
                                        {
                                            if (absentStatus != 0)
                                            {
                                                availedLeaves = absentStatus;
                                                absentDaysDeduct = absentStatus * perAbsentDayDeduction;
                                                var halfleaveDeductionAmmount = absentDaysDeduct * 0.5;
                                                halfLeaveStatus = halfLeaveStatus + Convert.ToInt32(halfleaveDeductionAmmount);
                                                var shortLeaveDeduction = absentDaysDeduct * 0.25;
                                                shortLeaveStatus = shortLeaveStatus + Convert.ToInt32(shortLeaveDeduction);
                                            }
                                        }
                                        currentLeaves = allowedLeaves - availedLeaves;
                                        leaveBalance = currentLeaves * perAbsentDayDeduction;
                                        totalBasicAndAllowance = empBasicSalaryResult + empAllowanceResult + empVisitAllowanceResult;
                                        totaldeductions = empAdvanceRequestResult + absentDaysDeduct + empFineresult + empDeductionResult;

                                        //allowed Leaves and Availed Leaves


                                        if (leaveBalance < 0)
                                        {
                                            totaldeductions = totaldeductions - leaveBalance;
                                        }
                                        if (leaveBalance > 0)
                                        {
                                            totalBasicAndAllowance = totalBasicAndAllowance + leaveBalance;
                                        }
                                        totalSalary = totalBasicAndAllowance - totaldeductions;
                                        //employeeSalaryList. .Where(a => a.EmpId == result[k].Id).ToList();
                                        employeeSalaryList.Where(a => a.EmpId == result[k].Id).Select(c =>
                                        {
                                            c.PresentDays = presentStatus;
                                            c.AbsentDays = absentStatus;
                                            c.AbsentDeduction = absentDaysDeduct;
                                            //c.BasicSalary = empBasicSalaryResult;
                                            c.GrossSalary = empGrossSalary;
                                            c.DaysInMonth = daysOfMonth;
                                            c.AllowedLeaves = allowedLeaves;
                                            c.AvailedLeaves = availedLeaves;
                                            c.CurrentLeaves = currentLeaves;
                                            c.LeaveBalance = leaveBalance;
                                            c.OvertimeHrs = empOvertimeResult;
                                            c.Overtime = overtimeRate;
                                            c.DeductionAdvAgainstSalary = empAdvanceRequestResult;
                                            c.VisitAllowance = empVisitAllowanceResult;
                                            //c.Allowance = empAllowanceResult;
                                            c.IncomeTax = 0;
                                            c.GraduityFund = 0;
                                            c.TotalSalary = totalSalary;
                                            c.WorkingDays = workingDays;
                                            c.salaryMonth = toDate;
                                            c.SalaryType = result[k].PayRoll_Division;
                                            return c;
                                        }).ToList();
                                        totalSalary = 0;
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    for (int gs = 0; gs < employeeSalaryList.Count(); gs++)
                    {
                        AutoSalaryDto autoSalary = new AutoSalaryDto();
                        autoSalary.AbsentDays = employeeSalaryList[gs].AbsentDays;
                        autoSalary.AbsentDeduction = employeeSalaryList[gs].AbsentDeduction;
                        autoSalary.AllowedLeaves = employeeSalaryList[gs].AllowedLeaves;
                        autoSalary.AvailedLeaves = employeeSalaryList[gs].AvailedLeaves;
                        autoSalary.Code = employeeSalaryList[gs].Code;
                        autoSalary.CostCenterName = employeeSalaryList[gs].CostCenterName;
                        autoSalary.CurrentLeaves = employeeSalaryList[gs].CurrentLeaves;
                        autoSalary.DaysInMonth = employeeSalaryList[gs].DaysInMonth;
                        autoSalary.DeductionAdvAgainstSalary = employeeSalaryList[gs].DeductionAdvAgainstSalary;
                        autoSalary.Designation = employeeSalaryList[gs].Designation;
                        autoSalary.EmpDepartment = employeeSalaryList[gs].EmpDepartment;
                        autoSalary.EmpId = employeeSalaryList[gs].EmpId;
                        autoSalary.GraduityFund = employeeSalaryList[gs].GraduityFund;
                        autoSalary.GrossSalary = employeeSalaryList[gs].GrossSalary;
                        autoSalary.IncomeTax = employeeSalaryList[gs].IncomeTax;
                        autoSalary.LeaveBalance = employeeSalaryList[gs].LeaveBalance;
                        autoSalary.Name = employeeSalaryList[gs].Name;
                        autoSalary.Overtime = employeeSalaryList[gs].Overtime;
                        autoSalary.OvertimeHrs = employeeSalaryList[gs].OvertimeHrs;
                        autoSalary.PresentDays = employeeSalaryList[gs].PresentDays;
                        autoSalary.TotalSalary = employeeSalaryList[gs].TotalSalary;
                        autoSalary.VisitAllowance = employeeSalaryList[gs].VisitAllowance;
                        autoSalary.WorkingDays = employeeSalaryList[gs].WorkingDays;

                        //for(int fsd = 0; fsd < employeeSalaryDetailList.Count(); fsd++)
                        //{
                        //    if(autoSalary.EmpId == employeeSalaryDetailList[fsd].employeeId)
                        //    {
                        //        try
                        //        {
                        //            autoSalary.AutoSalaryDetails.Add(employeeSalaryDetailList[fsd].MapTo<AutoSalaryDetailDto>());
                        //        }
                        //        catch(Exception ex)
                        //        {
                        //            throw ex;
                        //        }

                        //    }
                        //}

                        for (int mer = 0; mer < employeeSalaryDetailList.Count(); mer++)
                        {
                            if (autoSalary.EmpId == employeeSalaryDetailList[mer].employeeId)
                            {
                                try
                                {
                                    employeeSalaryList[gs].EmployeeSalaryDetailList.Add(employeeSalaryDetailList[mer]);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }

                            }
                        }

                        //var autoSalarySave = ObjectMapper.Map<AutoSalary>(autoSalary);
                        //autoSalarySave.LastModificationTime = DateTime.Now;
                        //autoSalarySave.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        ////var response = await _SalaryMasterRepository.InsertAsync(result);

                        //var response = await _AutoSalaryRepository.InsertAsync(autoSalarySave);


                        //foreach(var detail in autoSalarySave.AutoSalaryDetails)
                        //{
                        //    detail.AutoSalaryId = response.Id;
                        //    detail.LastModificationTime = DateTime.Now;
                        //    detail.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        //    await _AutoSalaryDetailRepository.InsertAsync(detail);

                        //}

                        try
                        {
                            //CurrentUnitOfWork.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
                    return employeeSalaryList.ToArray();

                }




                if (payroll == "Production")
                {
                    try
                    {
                        //week days list
                        List<DayOfWeek> empProductionweekOffDays = new List<DayOfWeek>() { DayOfWeek.Friday };


                        //salary Date Parameter
                        DateTime empProductionfromDate = new DateTime();
                        empProductionfromDate = salaryDate.Value.Date;
                        if (empProductionfromDate == null)
                        {
                            var nullDate = "Please enter date time";
                            return nullDate.ToArray();
                        }
                        //DateTime fromDate = new DateTime();
                        //fromDate = DateTime.Parse("12/01/2018");
                        DateTime empProductiontoDate = new DateTime();
                        int empProductionsalaryMonth = empProductionfromDate.Month;
                        int empProductionsalaryYear = empProductionfromDate.Year;
                        int empProductiondaysOfMonth = DateTime.DaysInMonth(empProductionsalaryYear, empProductionsalaryMonth);
                        empProductiontoDate = DateTime.Parse(empProductionsalaryMonth + "/" + empProductiondaysOfMonth + "/" + empProductionsalaryYear).AddHours(2);
                        
                        var employeeProductionAttendance = _AttendanceRepository.GetAll().Where(d => d.IsDeleted == false && d.TenantId == TenantId).Include(t => t.AttendanceTypes).Select(v => new { v.EmployeeId, v.AttendanceDate, v.AttendanceTypes.Status }).ToList();
                        var employeeProductionStatuses = _AttendanceRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.AttendanceDate >= empProductionfromDate && a.AttendanceDate <= empProductiontoDate).Select(a => new { a.EmployeeId, a.AttendanceDate, a.AttendanceTypes.Status }).GroupBy(a => a.EmployeeId).Select(a => a.ToList()).ToList();


                        for (int i = 0; i < employeeProductionStatuses.Count(); i++)
                        {
                            int ProductionpresentStatus = 0;
                            int ProductionabsentStatus = 0;
                            int ProductionleaveStatus = 0;
                            int ProductionhalfLeaveStatus = 0;
                            int ProductionshortLeaveStatus = 0;
                            int ProductionanualLeaveStatus = 0;
                            int ProductionsickLeaveStatus = 0;
                            int ProductionoutDoorDutyStatus = 0;
                            int ProductionbreakStatus = 0;
                            int ProductionoffDayStatus = 0;
                            int ProductiongezzettedLeaveStatus = 0;
                            int ProductionmaternityLeaveStatus = 0;
                            int ProductionshortAbsentStatus = 0;


                            var businessProductionDays = 0;
                            for (var date = empProductionfromDate; date <= empProductiontoDate; date = date.AddDays(1))
                            {
                                if (date.DayOfWeek != DayOfWeek.Friday)
                                    businessProductionDays++;
                            }




                            //overtime
                            double ProductionovertimeRate = 0;
                            //if (overtimeConfiguration != null)
                            //{
                            //    ProductionovertimeRate = overtimeConfiguration.RegularDayHourRate;
                            //}
                            double empProductionOvertimeResult = 0;
                            double empProductionAdvanceRequestResult = 0;


                            var getProductionRecords = employeeProductionStatuses[i].ToList();
                            var dateWiseProductionRecord = getProductionRecords.Where(a => a.AttendanceDate >= empProductionfromDate && a.AttendanceDate <= empProductiontoDate).ToList();

                            for (int k = 0; k < result.Count(); k++)
                            {
                                ////check i val

                                

                                    double allowedProductionLeaves = 5;
                                    double availedProductionLeaves = 3;
                                    double currentProductionLeaves = 0;
                                    double leaveProductionBalance = 0;

                                    currentProductionLeaves = allowedProductionLeaves - availedProductionLeaves;



                                    int workingProductionDays = businessProductionDays;

                                    //attendance
                                    double perAbsentDayProductionDeduction = 0;
                                    double absentDaysProductionDeduct = 0;


                                //geztted 
                                double gezztedAmmount = 0;
                                double gezztedAmmountResult = 0;
                                double basicAmmount = 0;

                                    //salary
                                    double empBasicProductionSalaryResult = 0;
                                    double empGrossProductionSalary = 0;


                                    //allowance
                                    double empAllowanceProductionResult = 0;
                                    double empVisitAllowanceProductionResult = 0;
                                    double empFineProductionresult = 0;
                                    double empDeductionProductionResult = 0;
                                    double totalProductionSalary = 0;
                                    double totalProductiondeductions = 0;
                                    double totalProductionBasicAndAllowance = 0;


                                    //ratio
                                    double empProductionRatios = 0;


                                    //getting Employee Ratio
                                    var empProductionRatio = _WorkGroupMasterRepository.GetAll()
                                        .Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();

                                    if (i == 0)
                                    {
                                        for (int g = 0; g < groupDistincts.Count(); g++)
                                        {
                                            var RatioOfEmpByGroupName = _WorkGroupDetailRepository.GetAll()
                                                .Where(a => a.IsDeleted == false)
                                                .Include(a => a.WorkGroupMaster)
                                                .Where(a => a.WorkGroupMaster.GroupName == groupDistincts[g].ToString())
                                                .Sum(a => a.Ratio).ToString();
                                            employeeGroupAndSumRatio.Add(new EmployeeGroupAndSumRatio
                                            {
                                                GroupName = groupDistincts[g].ToString(),
                                                RatioSum = Convert.ToDouble(RatioOfEmpByGroupName)
                                            });
                                        }
                                    }

                                    if (k == 0)
                                    {
                                        for (int resultAgain = 0; resultAgain < result.Count(); resultAgain++)
                                        {
                                            //individual group members information
                                            for (int indMember = 0; indMember < groupDistincts.Count(); indMember++)
                                            {
                                                var empProductionDetailRatio = _WorkGroupDetailRepository.GetAll()
                                                    .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[resultAgain].Id)
                                                    .Include(a => a.WorkGroupMaster)
                                                    .ThenInclude(a => a.WorkGroupItemDetails)
                                                    .Where(a => a.WorkGroupMaster.GroupName == groupDistincts[indMember].ToString())
                                                    .ToList();

                                                if (empProductionDetailRatio.Count() > 0)
                                                {
                                                    double ratioPercentage = 0;
                                                    var sumOfSpecificGroupRatio = employeeGroupAndSumRatio.Where(a => a.GroupName == groupDistincts[indMember]).Select(a => a.RatioSum).FirstOrDefault();

                                                    for (int r = 0; r < empProductionDetailRatio.Count(); r++)
                                                    {
                                                        empProductionRatios = empProductionDetailRatio[r].Ratio;
                                                    }
                                                    ratioPercentage = empProductionRatios / Convert.ToDouble(sumOfSpecificGroupRatio);

                                                    //date wise ratio for each employee
                                                    if (i == 0)
                                                    {
                                                        for (int RatioAllEmployee = 0; RatioAllEmployee < employeeProductionStatuses.Count(); RatioAllEmployee++)
                                                        {
                                                            var RatioOfAllEmployeeOne = employeeProductionStatuses[RatioAllEmployee].ToList();
                                                            var RatioOfEachEmployeeByDate = RatioOfAllEmployeeOne.Where(a => a.AttendanceDate >= empProductionfromDate && a.AttendanceDate <= empProductiontoDate).ToList();
                                                            for (int empStatusRatio = 0; empStatusRatio < RatioOfEachEmployeeByDate.Count(); empStatusRatio++)
                                                            {
                                                            ratioPercentage = empProductionRatios / Convert.ToDouble(sumOfSpecificGroupRatio);
                                                            if (result[resultAgain].Id == RatioOfEachEmployeeByDate[empStatusRatio].EmployeeId)
                                                                {
                                                                    if (RatioOfEachEmployeeByDate[empStatusRatio].Status == "Present")
                                                                    {
                                                                        ratioPercentage = ratioPercentage * 1;
                                                                    }
                                                                    if (RatioOfEachEmployeeByDate[empStatusRatio].Status == "Absent")
                                                                    {
                                                                        ratioPercentage = ratioPercentage * 0;
                                                                    }
                                                                    empRatioPerDayWise.Add(new EmpRatioPerDayWise
                                                                    {
                                                                        EmpId = result[resultAgain].Id,
                                                                        EmpName = result[resultAgain].Employee_Name,
                                                                        empRatio = ratioPercentage,
                                                                        statusDate = RatioOfEachEmployeeByDate[empStatusRatio].AttendanceDate
                                                                    });
                                                                }
                                                            }
                                                        }

                                                    }
                                                    var basicSalary = _SalaryDetailRepositoty.GetAll()
                                                        .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.SalaryItem == "Basic"
                                                        && a.SalaryMaster.EmployeeId == result[resultAgain].Id).ToList();
                                                    double basicByRatio = 0;
                                                    if (basicSalary.Count() > 0)
                                                    {
                                                        for (int bSalary = 0; bSalary < basicSalary.Count(); bSalary++)
                                                        {
                                                            basicByRatio = basicSalary[bSalary].Amount / empProductionRatios;
                                                        }

                                                    }

                                                    employeeProductionSalaryList.Where(a => a.EmpId == result[resultAgain].Id).Select(c =>
                                                    {
                                                        c.Ratio = empProductionRatios;
                                                        c.RatioPercentage = Math.Round(ratioPercentage, 2);
                                                        c.BasicByRatio = basicByRatio;
                                                        c.salaryMonth = empProductiontoDate;
                                                        return c;
                                                    }).ToList();
                                                }
                                            }
                                        }
                                    }



                                    if (i == 0)
                                    {

                                        if (k == 0)
                                        {
                                            var empItemWiseProduction = _ItemWiseProductionMasterRepository.GetAll()
                                            .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.DocDate >= empProductionfromDate && a.DocDate <= empProductiontoDate)
                                            .Include(a => a.ItemWiseProductionDetails)
                                            .ToList();

                                            if (empItemWiseProduction.Count() > 0 )
                                            {
                                                for (int eItem = 0; eItem < empItemWiseProduction.Count(); eItem++)
                                                {
                                                    var itemDetail = empItemWiseProduction[eItem].ItemWiseProductionDetails.ToList();
                                                    for (int eItemDetail = 0; eItemDetail < itemDetail.Count(); eItemDetail++)
                                                    {
                                                        var nameCheck = itemDetail[eItemDetail].WorkGroupItemDetail.ItemId;
                                                        var getItemName = _ItemRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.Id == itemDetail[eItemDetail].WorkGroupItemDetail.ItemId).FirstOrDefault();
                                                        var getGroupName = itemDetail[eItemDetail].WorkGroupItemDetail.WorkGroupMaster.GroupName;

                                                        var getEmpName = itemDetail[eItemDetail].WorkGroupItemDetail.WorkGroupMaster.WorkGroupEmployeeDetails.ToList();
                                                        var checkGetName = itemDetail[eItemDetail].WorkGroupItemDetail.WorkGroupMasterId;
                                                        var getEmployeeGroup = _WorkGroupDetailRepository.GetAll().Where(a => a.TenantId == TenantId && a.WorkGroupMasterId == checkGetName).ToList();

                                                        //sum Of Each day ratio with Present and All employee
                                                        double eachDayRatio = empRatioPerDayWise.Where(a => a.statusDate == empItemWiseProduction[eItem].DocDate).Sum(a => a.empRatio);


                                                        for (int empName = 0; empName < getEmployeeGroup.Count(); empName++)
                                                        {
                                                            var getEachEmployeeRatio = empRatioPerDayWise
                                                                .Where(a => a.EmpId == getEmployeeGroup[empName].EmployeeId && a.statusDate == empItemWiseProduction[eItem].DocDate).ToList();

                                                            //var getEachEmployeeRatio = employeeProductionSalaryList.Where(a => a.EmpId == getEmployeeGroup[empName].EmployeeId).ToList();
                                                            double empRatio = 0;
                                                            for (int eRatio = 0; eRatio < getEachEmployeeRatio.Count(); eRatio++)
                                                            {
                                                                empRatio = getEachEmployeeRatio[eRatio].empRatio;
                                                            }
                                                        var getEachEmployeeRatioResult = empRatio * Convert.ToInt32(itemDetail[eItemDetail].Quantity + itemDetail[eItemDetail].ExtraQuantity) / eachDayRatio;
                                                        var getEachEmployeeRatioResult1 = empRatio * Convert.ToInt32(itemDetail[eItemDetail].RepairQuantity) / eachDayRatio;
                                                        //var getEachEmployeeRatioResult = empRatio * Convert.ToInt32(itemDetail[eItemDetail].Quantity);

                                                        if (getEachEmployeeRatioResult1.ToString() != null && getEachEmployeeRatioResult1 != 0)
                                                        {
                                                            getEachEmployeeRatioResult1.ToString();
                                                        }
                                                        var getBasicByRatio = employeeProductionSalaryList.Where(a => a.EmpId == getEmployeeGroup[empName].EmployeeId).FirstOrDefault();
                                                            double perDayTotalRate = 0;
                                                        double perDayQuantityRate = 0;
                                                        double perDayRepairQuantityRate = 0;
                                                        //getting item quantity rate
                                                        var eachItemRateResult = _ProductionRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.ItemId == itemDetail[eItemDetail].WorkGroupItemDetail.ItemId).Select(a => a.Rate).FirstOrDefault();

                                                        var eachItemRepairRateResult = _ProductionRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.ItemId == itemDetail[eItemDetail].WorkGroupItemDetail.ItemId).Select(a => a.RepairRate).FirstOrDefault();
                                                        

                                                        if (eachItemRateResult != null || eachItemRepairRateResult != null)
                                                        {
                                                            if(getEachEmployeeRatioResult != 0 || getEachEmployeeRatioResult != null)
                                                            {
                                                                perDayQuantityRate = Math.Round(getEachEmployeeRatioResult, 2) * Convert.ToDouble(eachItemRateResult);
                                                            }
                                                            if(getEachEmployeeRatioResult1 != 0 || getEachEmployeeRatioResult1 != null)
                                                            {
                                                                perDayRepairQuantityRate = Math.Round(getEachEmployeeRatioResult1, 2) * Convert.ToDouble(eachItemRepairRateResult);
                                                            }


                                                            perDayTotalRate = perDayQuantityRate + perDayRepairQuantityRate;
                                                        }
                                                        if (getBasicByRatio != null)
                                                            {
                                                                //perDayTotalRate = getEachEmployeeRatioResult * getBasicByRatio.BasicByRatio;
                                                                
                                                            }
                                                            empItemWiseProductionList.Add(new EmpItemWiseProduction
                                                            {
                                                                ProductionDate = empItemWiseProduction[eItem].DocDate,
                                                                //ProducedItem =  Convert.ToInt32(itemDetail[eItemDetail].Quantity)
                                                                ProducedItem = Convert.ToInt32(itemDetail[eItemDetail].Quantity),
                                                                ItemName = getItemName.Name,
                                                                GroupName = getGroupName,
                                                                EmpName = getEmployeeGroup[empName].Employee.Employee_Name,
                                                                EmpId = getEmployeeGroup[empName].EmployeeId,
                                                                EmpRatio = getEachEmployeeRatioResult,
                                                                empPerDayRate = perDayTotalRate
                                                            });
                                                        }
                                                        //////////////
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var noItemWiseProduction = "There is no production in this month";

                                                return noItemWiseProduction.ToArray();
                                            }


                                        }


                                        // generate salary for each employee by each item
                                        if (k == 0)
                                        {
                                            for (int resultForTotalSalary = 0; resultForTotalSalary < result.Count(); resultForTotalSalary++)
                                            {
                                                var totalSalaryEmpItemWiseDetail = empItemWiseProductionList
                                            .Where(a => a.EmpId == result[resultForTotalSalary].Id).Distinct().Select(a => new
                                            { a.EmpId, a.EmpName, a.GroupName, a.empPerDayRate, a.ItemName, a.ProductionDate }).ToList();
                                                long empIds = 0;
                                                string name = "";
                                                string groupName = "";
                                                double perDayRate = 0;
                                                string itemName = "";
                                                DateTime productionDates = new DateTime();
                                                var checkcheck = totalSalaryEmpItemWiseDetail.GroupBy(a => a.ItemName).ToList();

                                                for (int checkSalary = 0; checkSalary < checkcheck.Count(); checkSalary++)
                                                {
                                                    var getFirstSalary = checkcheck[checkSalary].ToList();
                                                    for (int getSalary = 0; getSalary < getFirstSalary.Count(); getSalary++)
                                                    {
                                                        empIds = getFirstSalary[getSalary].EmpId;
                                                        name = getFirstSalary[getSalary].EmpName;
                                                        groupName = getFirstSalary[getSalary].GroupName;
                                                        perDayRate = getFirstSalary[getSalary].empPerDayRate;
                                                        itemName = getFirstSalary[getSalary].ItemName;
                                                        productionDates = getFirstSalary[getSalary].ProductionDate;
                                                    }

                                                    var totalSalaryEmpItemWise = checkcheck[checkSalary].Select(a => new { a.EmpId, a.EmpName, a.GroupName, a.empPerDayRate }).Sum(a => a.empPerDayRate);
                                                    employeeItemWiseProductionTotalSalary.Add(new EmployeeItemWiseProductionTotalSalary
                                                    {
                                                        EmpName = name,
                                                        GroupName = groupName,
                                                        ItemName = itemName,
                                                        Total = Math.Round(totalSalaryEmpItemWise,2),
                                                        EmpId = empIds,
                                                        salaryMonth = productionDates
                                                    });
                                                }
                                            }
                                        }



                                        //generate total salary
                                        if (k == 0)
                                        {
                                            for (int totalSalaryByMonth = 0; totalSalaryByMonth < result.Count(); totalSalaryByMonth++)
                                            {

                                                var eachEmployeeRecord = employeeItemWiseProductionTotalSalary.Where(a => a.EmpId == result[totalSalaryByMonth].Id).GroupBy(a => a.EmpId).ToList();
                                                long empId = 0;
                                                string eachEmpName = "";
                                                DateTime eachEmpSalaryMonth = new DateTime();
                                                string eachEmpGroupName = "";
                                                double eachEmpTotal = 0;
                                                for (int eachEmployee = 0; eachEmployee < eachEmployeeRecord.Count(); eachEmployee++)
                                                {
                                                    var eachEmployeeList = eachEmployeeRecord[eachEmployee].ToList();
                                                    for (int empList = 0; empList < eachEmployeeList.Count(); empList++)
                                                    {
                                                        empId = eachEmployeeList[empList].EmpId;
                                                        eachEmpName = eachEmployeeList[empList].EmpName;
                                                        eachEmpSalaryMonth = eachEmployeeList[empList].salaryMonth;
                                                        eachEmpGroupName = eachEmployeeList[empList].GroupName;
                                                        eachEmpTotal = eachEmployeeList[empList].Total;
                                                    }
                                                    var totalSalary = eachEmployeeRecord[eachEmployee].Sum(a => a.Total);
                                                    employeeTotalSalaryByProduction.Add(new EmployeeTotalSalaryByProduction
                                                    {
                                                        EmployeeId = empId,
                                                        EmployeeName = eachEmpName,
                                                        SalaryMonth = eachEmpSalaryMonth,
                                                        GroupName = eachEmpGroupName,
                                                        TotalSalary = totalSalary
                                                    });
                                                }

                                            }
                                        }


                                        //employeeTotalSalaryByProduction



                                    }
                                    //individual item wise production


                                    //employee salary start

                                    var empProductionSalary = _SalaryMasterRepository.GetAll()
                                        .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).Include(a => a.SalaryDetails).ToList();


                                    if (empProductionSalary.Count() > 0)
                                    {
                                        for (int l = 0; l < empProductionSalary.Count(); l++)
                                        {

                                            ///previous start
                                            //empGrossProductionSalary = Convert.ToInt32(empProductionSalary[l].AnnualGrossSalary);
                                            //empBasicProductionSalaryResult = Convert.ToInt32(empProductionSalary[l].AnnualGrossSalary);
                                            //previous ends
                                            empGrossProductionSalary = employeeTotalSalaryByProduction.Where(a => a.EmployeeId == result[k].Id).Select(a => a.TotalSalary).First();
                                            empBasicProductionSalaryResult = employeeTotalSalaryByProduction.Where(a => a.EmployeeId == result[k].Id).Select(a => a.TotalSalary).First();

                                            

                                            var resultProductionAllow = empProductionSalary[l].SalaryDetails.ToList();
                                            //var groooooos = empSalary[l].AnnualGrossSalary;
                                            for (var si = 0; si < resultProductionAllow.Count(); si++)
                                            {
                                                if (i == 0)
                                                {
                                                    employeeProductionSalaryDetailList.Add(new EmployeeProductionSalaryDetailList
                                                    {
                                                        salaryItemType = resultProductionAllow[si].SalaryItem,
                                                        SalaryItemTypeId = resultProductionAllow[si].Id,
                                                        Earning = resultProductionAllow[si].Amount,
                                                        employeeId = result[k].Id
                                                    });

                                                    //empAllowanceResult = empAllowanceResult + Convert.ToInt32(resultAllow[si].Amount);
                                                }
                                                if (resultProductionAllow[si].SalaryItem != "Basic")
                                                {
                                                    empAllowanceProductionResult = resultProductionAllow[si].Amount;
                                                }
                                                else
                                            {
                                                gezztedAmmount = resultProductionAllow[si].Amount;
                                                basicAmmount = resultProductionAllow[si].Amount;
                                            }
                                            }
                                            perAbsentDayProductionDeduction = empBasicProductionSalaryResult / workingProductionDays;
                                            leaveProductionBalance = currentProductionLeaves * perAbsentDayProductionDeduction;


                                        }
                                    }
                                    //employee salary ends

                                    //Advance Request of Employee start
                                    var empProductionAdvanceRequest = _AdvanceRequestRepository.GetAll()
                                                .Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                    if (empProductionAdvanceRequest.Count() > 0)
                                    {
                                        for (int ar = 0; ar < empProductionAdvanceRequest.Count(); ar++)
                                        {
                                            empProductionAdvanceRequestResult = empProductionAdvanceRequestResult + empProductionAdvanceRequest[ar].Amount;
                                        }
                                    }
                                    //Advance Request Of Employee Ends


                                    //allowed leaves start
                                    //allowed leaves ends
                                    //employee overtime start
                                    var empProductionOvertime = _OvertimeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                    if (empProductionOvertime.Count() > 0)
                                    {
                                        for (int m = 0; m < empProductionOvertime.Count(); m++)
                                        {
                                            empProductionOvertimeResult = empProductionOvertimeResult + empProductionOvertime[m].OverTimerHour;
                                        }
                                        ProductionovertimeRate = ProductionovertimeRate * empProductionOvertimeResult;
                                    }
                                    else
                                    {
                                        ProductionovertimeRate = 0;
                                    }
                                    //employee overtime ends

                                    //employee allowance start
                                    if (allowance == true && allowance != null)
                                    {
                                        var empProductionAllowance = _AllowanceRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                        if (empProductionAllowance.Count() > 0)
                                        {
                                            for (int n = 0; n < empProductionAllowance.Count(); n++)
                                            {
                                                empVisitAllowanceProductionResult = empVisitAllowanceProductionResult + empProductionAllowance[n].Amount;
                                            }
                                        }
                                    }
                                    //employee allowance ends.

                                    //fine result starts
                                    if (fine == true && fine != null)
                                    {
                                        var empProductionFine = _FineRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                        if (empProductionFine.Count() > 0)
                                        {
                                            for (int f = 0; f < empProductionFine.Count(); f++)
                                            {
                                                empFineProductionresult = empFineProductionresult + empProductionFine[f].Amount;
                                            }
                                        }
                                    }
                                    //fine result ends
                                    // deduction start
                                    if (deduction == true && deduction != null)
                                    {
                                        var empProductionDeduction = _DeductionRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.EmployeeId == result[k].Id).ToList();
                                        if (empProductionDeduction.Count() > 0)
                                        {
                                            for (int d = 0; d < empProductionDeduction.Count(); d++)
                                            {
                                                empDeductionProductionResult = empDeductionProductionResult + empProductionDeduction[d].Amount;
                                            }
                                        }
                                    }
                                    //deductioni ends

                                    for (int j = 0; j < dateWiseProductionRecord.Count(); j++)
                                    {
                                        if (result[k].Id == dateWiseProductionRecord[j].EmployeeId)
                                        {
                                            if (dateWiseProductionRecord[j].Status == "Present")
                                            {
                                                ProductionpresentStatus = ProductionpresentStatus + 1;

                                            }
                                            if (dateWiseProductionRecord[j].Status == "Absent")
                                            {
                                                ProductionabsentStatus = ProductionabsentStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "Leave")
                                            {
                                                ProductionleaveStatus = ProductionleaveStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "HalfLeave")
                                            {
                                                ProductionhalfLeaveStatus = ProductionhalfLeaveStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "ShortLeave")
                                            {
                                                ProductionshortLeaveStatus = ProductionshortLeaveStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "AnualLeave")
                                            {
                                                ProductionanualLeaveStatus = ProductionanualLeaveStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "SickLeave")
                                            {
                                                ProductionsickLeaveStatus = ProductionsickLeaveStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "OutdoorDuty")
                                            {
                                                ProductionoutDoorDutyStatus = ProductionoutDoorDutyStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "OffDay")
                                            {
                                                ProductionoffDayStatus = ProductionoffDayStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "GazzettedLeave")
                                            {
                                                ProductiongezzettedLeaveStatus = ProductiongezzettedLeaveStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "MaternityLeave")
                                            {
                                                ProductionmaternityLeaveStatus = ProductionmaternityLeaveStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "ShortAbsent")
                                            {
                                                ProductionshortAbsentStatus = ProductionshortAbsentStatus + 1;
                                            }
                                            if (dateWiseProductionRecord[j].Status == "Break")
                                            {
                                                ProductionbreakStatus = ProductionbreakStatus + 1;
                                            }


                                            if (j == dateWiseProductionRecord.Count() - 1)
                                            {
                                                if (ProductionabsentStatus != 0)
                                                {
                                                    absentDaysProductionDeduct = ProductionabsentStatus * perAbsentDayProductionDeduction;
                                                    var halfleaveProductionDeductionAmmount = absentDaysProductionDeduct * 0.5;
                                                    ProductionhalfLeaveStatus = ProductionhalfLeaveStatus + Convert.ToInt32(halfleaveProductionDeductionAmmount);
                                                    var shortLeaveProductionDeduction = absentDaysProductionDeduct * 0.25;
                                                    ProductionshortLeaveStatus = ProductionshortLeaveStatus + Convert.ToInt32(shortLeaveProductionDeduction);
                                                }
                                                if(ProductiongezzettedLeaveStatus != 0)
                                                {
                                                    gezztedAmmountResult = Math.Round(gezztedAmmount * ProductiongezzettedLeaveStatus, 2);
                                                }
                                            }

                                            totalProductionBasicAndAllowance = empBasicProductionSalaryResult + empAllowanceProductionResult + empVisitAllowanceProductionResult + gezztedAmmountResult;
                                        //totalProductiondeductions = empProductionAdvanceRequestResult + absentDaysProductionDeduct + empFineProductionresult + empDeductionProductionResult;
                                        totalProductiondeductions = empProductionAdvanceRequestResult + empFineProductionresult + empDeductionProductionResult;

                                        //if (leaveProductionBalance < 0)
                                        //{
                                        //    totalProductiondeductions = totalProductiondeductions - leaveProductionBalance;
                                        //}
                                        //if (leaveProductionBalance > 0)
                                        //{
                                        //  totalProductionBasicAndAllowance = totalProductionBasicAndAllowance + leaveProductionBalance;
                                        //}
                                        totalProductionSalary = totalProductionBasicAndAllowance - totalProductiondeductions;
                                            //employeeSalaryList. .Where(a => a.EmpId == result[k].Id).ToList();
                                            employeeProductionSalaryList.Where(a => a.EmpId == result[k].Id).Select(c =>
                                            {
                                                c.PresentDays = ProductionpresentStatus;
                                                c.AbsentDays = ProductionabsentStatus;
                                                c.AbsentDeduction = absentDaysProductionDeduct;
                                                //c.BasicSalary = empBasicSalaryResult;
                                                c.GrossSalary = empGrossProductionSalary;
                                                c.DaysInMonth = empProductiondaysOfMonth;
                                                c.AllowedLeaves = Convert.ToInt32(allowedProductionLeaves);
                                                c.AvailedLeaves = Convert.ToInt32(availedProductionLeaves);
                                                c.CurrentLeaves = Convert.ToInt32(currentProductionLeaves);
                                                c.LeaveBalance = Convert.ToInt32(leaveProductionBalance);
                                                c.OvertimeHrs = Convert.ToInt32(empProductionOvertimeResult);
                                                c.Overtime = Convert.ToInt32(ProductionovertimeRate);
                                                c.DeductionAdvAgainstSalary = empProductionAdvanceRequestResult;
                                                c.VisitAllowance = empVisitAllowanceProductionResult;
                                                //c.Allowance = empAllowanceResult;
                                                c.IncomeTax = 0;
                                                c.GraduityFund = 0;
                                                c.TotalSalary = totalProductionSalary;
                                                c.WorkingDays = workingProductionDays;
                                                c.SalaryType = result[k].PayRoll_Division;                                                
                                                return c;
                                            }).ToList();
                                            totalProductionSalary = 0;
                                        }
                                    }
                                    
                            }



                            //////////////

                        }



                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    

                        //adding salary to dto
                        for (int gs = 0; gs < employeeProductionSalaryList.Count(); gs++)
                    {
                        AutoSalaryDto autoSalary = new AutoSalaryDto();
                        autoSalary.AbsentDays = employeeProductionSalaryList[gs].AbsentDays;
                        autoSalary.AbsentDeduction = Convert.ToInt32(employeeProductionSalaryList[gs].AbsentDeduction);
                        autoSalary.AllowedLeaves = employeeProductionSalaryList[gs].AllowedLeaves;
                        autoSalary.AvailedLeaves = employeeProductionSalaryList[gs].AvailedLeaves;
                        autoSalary.Code = employeeProductionSalaryList[gs].Code;
                        autoSalary.CostCenterName = employeeProductionSalaryList[gs].CostCenterName;
                        autoSalary.CurrentLeaves = employeeProductionSalaryList[gs].CurrentLeaves;
                        autoSalary.DaysInMonth = employeeProductionSalaryList[gs].DaysInMonth;
                        autoSalary.DeductionAdvAgainstSalary = Convert.ToInt32(employeeProductionSalaryList[gs].DeductionAdvAgainstSalary);
                        autoSalary.Designation = employeeProductionSalaryList[gs].Designation;
                        autoSalary.EmpDepartment = employeeProductionSalaryList[gs].EmpDepartment;
                        autoSalary.EmpId = employeeProductionSalaryList[gs].EmpId;
                        autoSalary.GraduityFund = Convert.ToInt32(employeeProductionSalaryList[gs].GraduityFund);
                        autoSalary.GrossSalary = Convert.ToInt32(employeeProductionSalaryList[gs].GrossSalary);
                        autoSalary.IncomeTax = Convert.ToInt32(employeeProductionSalaryList[gs].IncomeTax);
                        autoSalary.LeaveBalance = Convert.ToInt32(employeeProductionSalaryList[gs].LeaveBalance);
                        autoSalary.Name = employeeProductionSalaryList[gs].Name;
                        autoSalary.Overtime = Convert.ToInt32(employeeProductionSalaryList[gs].Overtime);
                        autoSalary.OvertimeHrs = employeeProductionSalaryList[gs].OvertimeHrs;
                        autoSalary.PresentDays = employeeProductionSalaryList[gs].PresentDays;
                        autoSalary.TotalSalary = Convert.ToInt32(employeeProductionSalaryList[gs].TotalSalary);
                        autoSalary.VisitAllowance = Convert.ToInt32(employeeProductionSalaryList[gs].VisitAllowance);
                        autoSalary.WorkingDays = employeeProductionSalaryList[gs].WorkingDays;
                        
                        for (int mer = 0; mer < employeeProductionSalaryDetailList.Count(); mer++)
                        {
                            if (autoSalary.EmpId == employeeProductionSalaryDetailList[mer].employeeId)
                            {
                                try
                                {
                                    employeeProductionSalaryList[gs].EmployeeSalaryDetailList.Add(employeeProductionSalaryDetailList[mer]);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }

                            }
                        }


                        try
                        {
                            //CurrentUnitOfWork.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }

                    for(int removeResult = 0; removeResult < result.Count(); removeResult++)
                    {

                        var productionSalaries = _AutoSalaryRepository.GetAll().Where(a => a.IsDeleted == false &&
                        a.SalaryMonth.Month == salaryDate.Value.Month && a.SalaryMonth.Year == salaryDate.Value.Year && a.EmpId == result[removeResult].Id
                        ).ToList();

                        if (productionSalaries.Count() != 0)
                        {
                            for (int removeItem = 0; removeItem < productionSalaries.Count(); removeItem++)
                            {
                                employeeProductionSalaryList.RemoveAll(r => r.EmpId == productionSalaries[removeItem].EmpId);
                                //employeeProductionSalaryList.Remove(prod)
                                //employeeProductionSalaryDetailList.RemoveAt(Convert.ToInt32(productionSalaries[removeItem].EmpId));
                            }
                        }
                    }


                    return employeeProductionSalaryList.ToArray();

                }
                return result.ToArray();

            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }


        





        //getting all the cost center based on cost center group.
        public async Task<Array> getAllCostCenterGroupBy()
        {
            try
            {
                var result = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false).Select(a => new { a.CostCenterGroup , a.Id, a.Name } ).GroupBy(a => a.CostCenterGroup).Select(a => a.First()).ToList();
                return result.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //getting all the cost centers.
        public async Task<Array> getAllCostCenter()
        {
            
            try
            {
                var result = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false).ToList();
                return result.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get all cost center based on cost center heads
        public async Task<Array> getAllCostCenterBaseOnHead(string costCenterHead)
        {
            if(costCenterHead != null && costCenterHead != "")
            {
                var result1 = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false && a.CostCenterGroup == costCenterHead).ToList();
                return result1.ToArray();
            }

            var result = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false).ToList();
            return result.ToArray();
        }








        
        
        
        public async Task<List<SalaryMasterDto>> GetAllSalaries()
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _SalaryMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(x => x.Employee).Include(y => y.SalaryGroupMaster).ToList();
                return result.MapTo<List<SalaryMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<SalaryDetailDto>> GetAllDetailById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _SalaryDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.SalaryMasterId == id && a.TenantId == TenantId)
                    .ToList();
                return result.MapTo<List<SalaryDetailDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Add(SalaryMasterDto input)
        {
            try
            {
                var result = ObjectMapper.Map<SalaryMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _SalaryMasterRepository.InsertAsync(result);

                foreach (var i in input.SalaryDetails)
                {
                    i.SalaryMasterId = response.Id;

                    await _SalaryDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Edit(SalaryMasterDto input)
        {
            var data = ObjectMapper.Map<SalaryMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await _SalaryMasterRepository.UpdateAsync(data);

            foreach (var i in input.SalaryDetails)
            {
                i.SalaryMasterId = response.Id;

                if (i.Id < 0)
                {
                    await _SalaryDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _SalaryDetailRepositoty.UpdateAsync(i);
                }
            }

            var masterResult = _SalaryMasterRepository.GetAll().Include(x => x.SalaryDetails).Where(x => x.Id == input.Id).FirstOrDefault();

            bool checkedBit = true;

            foreach (var x in masterResult.SalaryDetails)
            {
                checkedBit = true;

                foreach (var y in input.SalaryDetails)
                {
                    if (x.Id == y.Id)
                    {
                        checkedBit = false;
                        break;
                    }
                }

                if (checkedBit == true)
                {
                    if (x != null)
                    {
                        await _SalaryDetailRepositoty.DeleteAsync(x);
                    }
                }
            }

            CurrentUnitOfWork.SaveChanges();
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var masterResult = _SalaryMasterRepository.GetAll().Include(x => x.SalaryDetails).Where(x => x.Id == input.Id).FirstOrDefault();
            if (masterResult != null)
            {
                foreach (var i in masterResult.SalaryDetails)
                {
                    if (i != null)
                    {
                        await _SalaryDetailRepositoty.DeleteAsync(i);
                    }
                }

                await _SalaryMasterRepository.DeleteAsync(masterResult);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
