﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Warnings.Dto
{

    [AutoMapTo(typeof(Warning)), AutoMapFrom(typeof(WarningDto))]
    public class WarningDto : EntityDto<long>
    {

        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }

        public DateTime WarningDate { get; set; }

        
        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public string WarningType { get; set; }

        public long WarningById { get; set; }
        public virtual Department Department { get; set; }


        public string Detail { get; set; }

        public int TenantId { get; set; }

    }
}
