﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Warnings.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Warnings
{
    public class WarningAppService : AsyncCrudAppService<Warning, WarningDto, long, PagedResultRequestDto, WarningDto, WarningDto>, IWarningAppService
    {

        private readonly IRepository<Warning, long> WarningRepository;
        private readonly IPermissionManager _permissionManager;

        public WarningAppService(IRepository<Warning, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            WarningRepository = _repository;
            _permissionManager = _Manager;
        }

        //to get all warnings
        public async Task<List<WarningDto>> GetAllWarnings()
        {
            try
            {
                //get all active user
                var result = WarningRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.Employee).Include(d=>d.Department).ToList();

                return result.MapTo<List<WarningDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Warnings_Create)]
        //to create a warning
        public override async Task<WarningDto> Create(WarningDto input)
        {
            var result = ObjectMapper.Map<Warning>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await WarningRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<WarningDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Warnings_Edit)]
        //to update a warning
        public override async Task<WarningDto> Update(WarningDto input)
        {
            var data = ObjectMapper.Map<Warning>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await WarningRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<WarningDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Warnings_Delete)]
        //to delete a warning
        public override async Task Delete(EntityDto<long> input)
        {
            var result = WarningRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await WarningRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }




    }
}
