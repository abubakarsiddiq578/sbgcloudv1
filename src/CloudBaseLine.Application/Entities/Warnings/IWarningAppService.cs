﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Warnings.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Warnings
{
    public interface IWarningAppService : IAsyncCrudAppService<WarningDto, long, PagedResultRequestDto, WarningDto, WarningDto>
    {

        Task<List<WarningDto>> GetAllWarnings();
    }
}
