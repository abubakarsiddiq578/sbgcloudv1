﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Provinces.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Provinces
{
    public class ProvinceAppService : AsyncCrudAppService<Province, ProvinceDto, long, PagedResultRequestDto, ProvinceDto, ProvinceDto>,IProvinceAppService
    {
        private readonly IRepository<Province, long> ProvinceRepository;
        private readonly IPermissionManager _permissionManager;


        public ProvinceAppService(IRepository<Province, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            ProvinceRepository = _repository;

            _permissionManager = _Manager;

        }



        public async Task<List<ProvinceDto>> GetAllProvinces()
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = ProvinceRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId).Include(t=>t.Country).ToList();

                return a.MapTo<List<ProvinceDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Provinces_Create)]
        public override async Task<ProvinceDto> Create(ProvinceDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<Province>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await ProvinceRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<ProvinceDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Provinces_Edit)]
        public override async Task<ProvinceDto> Update(ProvinceDto input)
        {
            var data = ObjectMapper.Map<Province>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await ProvinceRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ProvinceDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Provinces_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ProvinceRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await ProvinceRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<ProvinceDto>> GetAllProvinceByCountryId(long id)
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = ProvinceRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId && b.CountryId == id).Include(t => t.Country).ToList();

                return a.MapTo<List<ProvinceDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }

    }
}
