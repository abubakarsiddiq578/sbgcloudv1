﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Provinces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Provinces
{
    public interface IProvinceAppService : IAsyncCrudAppService<ProvinceDto, long, PagedResultRequestDto, ProvinceDto, ProvinceDto>
    {

    }
}
