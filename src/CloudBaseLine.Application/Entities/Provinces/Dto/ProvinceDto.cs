﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Provinces.Dto
{
    [AutoMapTo(typeof(Province)), AutoMapFrom(typeof(ProvinceDto))]
    public class ProvinceDto:EntityDto<long>
    {
        public string Name { get; set; }

        public long CountryId { get; set; }

        public Country Country { get; set; }

        public int TenantId { get; set; }


    }
}
