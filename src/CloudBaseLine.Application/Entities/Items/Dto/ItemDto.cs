﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Items.Dto
{
    [AutoMapTo(typeof(Item)), AutoMapFrom(typeof(ItemDto))]
    public class ItemDto:EntityDto<long>
    {
        public string ItemCode { get; set; }
        public long ItemTypeId { get; set; }
        public ItemType ItemType { get; set; }

        public string Name { get; set; }

        public long CategoryId { get; set; }
        public Category Category { get; set; }

        public long SubCategoryId { get; set; }
        public SubCategory SubCategory { get; set; }


        public int TenantId { get; set; }

    }
}
