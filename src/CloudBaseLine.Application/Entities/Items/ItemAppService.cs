﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Items.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Items
{
    public class ItemAppService : AsyncCrudAppService<Item, ItemDto, long, PagedResultRequestDto, ItemDto, ItemDto>, IItemAppService
    {
        private readonly IRepository<Item, long> ItemRepository;
        private readonly IPermissionManager _permissionManager;


        public ItemAppService(IRepository<Item, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            ItemRepository = _repository;

            _permissionManager = _Manager;

        }


        public async Task<List<ItemDto>> GetAllItems()
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ItemRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId).Include(p=>p.Category).Include(s=>s.SubCategory).Include(t => t.ItemType).ToList();

                return result.MapTo<List<ItemDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Items_Create)]
        public override async Task<ItemDto> Create(ItemDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<Item>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await ItemRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<ItemDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Items_Edit)]
        public override async Task<ItemDto> Update(ItemDto input)
        {
            var data = ObjectMapper.Map<Item>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await ItemRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ItemDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Items_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ItemRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await ItemRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<ItemDto>> FilterData(long? itemTypeId, long? categoryId, long? subCategoryId)
        {
            var result = ItemRepository.GetAll()
                .Where(b => b.IsDeleted == false);
            if (itemTypeId > 0)
            {
                result = result.Where(b => b.ItemTypeId == itemTypeId);
            }
            if (categoryId > 0)
            {
                result = result.Where(b => b.CategoryId == categoryId);
            }

            if (subCategoryId > 0)
            {
                result = result.Where(b => b.SubCategoryId == subCategoryId);
            }
            return result.MapTo<List<ItemDto>>();

        }


    }
}
