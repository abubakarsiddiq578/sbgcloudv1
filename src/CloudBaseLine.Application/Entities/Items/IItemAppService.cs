﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Items.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Items
{
    public interface IItemAppService : IAsyncCrudAppService<ItemDto, long, PagedResultRequestDto, ItemDto, ItemDto>
    {

    }
}
