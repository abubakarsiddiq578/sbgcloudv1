﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;


namespace CloudBaseLine.Entities.Fines.Dto
{
    [AutoMapTo(typeof(Fine)), AutoMapFrom(typeof(FineDto))]
    public class FineDto : EntityDto<long>
    {
        public string DocNo { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime FineDate { get; set; }
        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public long FineTypeId { get; set; }
        public FineType FineType { get; set; }
        public double Amount { get; set; }
        public string Detail { get; set; }

        public int TenantId { get; set; }
    }
}
