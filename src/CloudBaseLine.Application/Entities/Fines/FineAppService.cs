﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Fines.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Fines
{
    public class FineAppService : AsyncCrudAppService<Fine, FineDto, long, PagedResultRequestDto, FineDto, FineDto>, IFineAppService
    {
        private readonly IRepository<Fine, long> _FineRepository;
        private readonly IPermissionManager _permissionManager;

        public FineAppService(IRepository<Fine, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _FineRepository = _repository;
            _permissionManager = _Manager;
        }

        public async Task<List<FineDto>> GetAllFines()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _FineRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.Employee).Include(b => b.FineType).ToList();
                return result.MapTo<List<FineDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Fines_Create)]
        public override async Task<FineDto> Create(FineDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<Fine>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _FineRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<FineDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Fines_Edit)]
        public override async Task<FineDto> Update(FineDto input)
        {
            var data = ObjectMapper.Map<Fine>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _FineRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<FineDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Fines_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _FineRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _FineRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
