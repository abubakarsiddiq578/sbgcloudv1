﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Fines.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Fines
{
    public interface IFineAppService : IAsyncCrudAppService<FineDto, long, PagedResultRequestDto, FineDto, FineDto>
    {

    }
}
