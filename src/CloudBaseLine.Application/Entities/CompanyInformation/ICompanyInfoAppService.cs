﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.CompanyInformation.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.CompanyInformation
{
    public interface ICompanyInfoAppService : IAsyncCrudAppService<CompanyInfoDto, long, PagedResultRequestDto, CompanyInfoDto, CompanyInfoDto>
    {
    }
}
