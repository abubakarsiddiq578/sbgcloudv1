﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.CompanyInformation.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.CompanyInformation
{
    public interface ICostCenterDropDownAppService : IAsyncCrudAppService<CostCenterDropDownDto, long, PagedResultRequestDto, CostCenterDropDownDto, CostCenterDropDownDto>
    {
    }
}
