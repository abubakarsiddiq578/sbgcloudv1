﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.ChartsOfAccountSubMain.Dto;
using CloudBaseLine.Entities.CompanyInformation.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.CompanyInformation
{
    public class CompanyInfoAppService: AsyncCrudAppService<CompanyInfo, CompanyInfoDto, long, PagedResultRequestDto, CompanyInfoDto, CompanyInfoDto>, ICompanyInfoAppService
    {
        private readonly IRepository<CompanyInfo, long> _CompanyInforepository;
        private readonly IPermissionManager _permissionManager;

        public CompanyInfoAppService(IRepository<CompanyInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _CompanyInforepository = _repository;
            _permissionManager = _Manager;
        }

       
        public async Task<List<CompanyInfoDto>> GetAllCompanies()
        {
            try
            {
                var result = _CompanyInforepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.CostCenter).ToList();
                return result.MapTo<List<CompanyInfoDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //var result = _UserCompanyRightrepository.GetAllIncluding(p=>p.User);
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_CompanyInformations_Create)]
        public override async Task<CompanyInfoDto> Create(CompanyInfoDto input)
        {
            var result = ObjectMapper.Map<CompanyInfo>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _CompanyInforepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();
            var data = result.MapTo<CompanyInfoDto>();
            return data;

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_CompanyInformations_Edit)]
        public override async Task<CompanyInfoDto> Update(CompanyInfoDto input)
        {
            var data = ObjectMapper.Map<CompanyInfo>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _CompanyInforepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CompanyInfoDto>();
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_CompanyInformations_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _CompanyInforepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _CompanyInforepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }


    }
}
