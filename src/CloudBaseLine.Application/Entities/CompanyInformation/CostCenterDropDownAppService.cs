﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.CompanyInformation.Dto;
using CloudBaseLine.Roles.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.CompanyInformation
{
    public class CostCenterDropDownAppService : AsyncCrudAppService<CostCenter, CostCenterDropDownDto, long, PagedResultRequestDto, CostCenterDropDownDto, CostCenterDropDownDto>, ICostCenterDropDownAppService
    {

        private readonly IRepository<CostCenter, long> _CostCenterDropDownRepository;
        private readonly IPermissionManager _permissionManager;
        public CostCenterDropDownAppService(IRepository<CostCenter, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _CostCenterDropDownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<CostCenterDropDownDto>> GetCostCenterDropDown()
        {
            var costCenters = await _CostCenterDropDownRepository.GetAllListAsync();
            return new ListResultDto<CostCenterDropDownDto>(ObjectMapper.Map<List<CostCenterDropDownDto>>(costCenters));
        }


        

    }
}
