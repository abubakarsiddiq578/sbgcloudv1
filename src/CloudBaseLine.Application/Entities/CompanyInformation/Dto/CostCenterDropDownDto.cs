﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.CompanyInformation.Dto
{
    [AutoMapTo(typeof(CostCenter)), AutoMapFrom(typeof(CostCenterDropDownDto))]
    public class CostCenterDropDownDto : EntityDto<long>
    {
        public string Name { get; set; }
    }
}
