﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.CompanyInformation.Dto
{
    [AutoMapTo(typeof(CompanyInfo)), AutoMapFrom(typeof(CompanyInfoDto))]
    public class CompanyInfoDto:EntityDto<long>
    {
        public string CompanyCode { get; set; }

        public string Prefix { get; set; }

        public string CompanyName { get; set; }

        public string LegalName { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string EMail { get; set; }

        public string webpage { get; set; }

        public string address { get; set; }

        public long CostCenterId { get; set; }

        public CostCenter CostCenter { get; set; }
        public long SalesTaxAccountId { get; set; }

        public bool CommercialInvoice { get; set; }

        public int TenantId { get; set; }


    }
}
