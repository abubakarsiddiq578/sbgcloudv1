﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LeaveRequests.Dto
{
    [AutoMapTo(typeof(LeaveRequest)), AutoMapFrom(typeof(LeaveRequestDto))]
    public class LeaveRequestDto : EntityDto<long>
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        public string ApplicationNo { get; set; }

        public DateTime ApplicationDate { get; set; }

        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public string AlternateContactNo { get; set; }

        public long LeaveTypeId { get; set; }
        public LeaveType LeaveType { get; set; }

        public string Duration { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string Reason { get; set; }

        public Boolean Approved { get; set; }

        public int TenantId { get; set; }

    }
}
