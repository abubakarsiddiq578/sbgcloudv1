﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LeaveRequests.Dto
{
    public interface ILeaveRequestAppService : IAsyncCrudAppService<LeaveRequestDto, long, PagedResultRequestDto, LeaveRequestDto, LeaveRequestDto>
    {
    }
}
