﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.LeaveRequests.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.LeaveRequests
{
    public class LeaveRequestAppService : AsyncCrudAppService<LeaveRequest, LeaveRequestDto, long, PagedResultRequestDto, LeaveRequestDto, LeaveRequestDto>, ILeaveRequestAppService
    {
        private readonly IRepository<LeaveRequest, long> _LeaveRequestRepository;
        private readonly IPermissionManager _permissionManager;

        public LeaveRequestAppService(IRepository<LeaveRequest, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _LeaveRequestRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<LeaveRequestDto>> GetAllLeaveRequests()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _LeaveRequestRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId)
                             .Include(e => e.Employee).Include(l => l.LeaveType).ToList();
                return result.MapTo<List<LeaveRequestDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveRequests_Create)]
        public override async Task<LeaveRequestDto> Create(LeaveRequestDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<LeaveRequest>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _LeaveRequestRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<LeaveRequestDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveRequests_Edit)]
        public override async Task<LeaveRequestDto> Update(LeaveRequestDto input)
        {
            var data = ObjectMapper.Map<LeaveRequest>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _LeaveRequestRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<LeaveRequestDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveRequests_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _LeaveRequestRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _LeaveRequestRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
