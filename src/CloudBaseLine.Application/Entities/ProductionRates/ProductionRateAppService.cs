﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.ProductionRates.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ProductionRates
{
    public class ProductionRateAppService : AsyncCrudAppService<ProductionRate, ProductionRateDto, long, PagedResultRequestDto, ProductionRateDto, ProductionRateDto>, IProductionRateAppService
    {
        private readonly IRepository<ProductionRate, long> _ProductionRateRepository;
        private readonly IPermissionManager _permissionManager;

        public ProductionRateAppService(IRepository<ProductionRate, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ProductionRateRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<ProductionRateDto>> GetAllProductionRates()
        {
            try
            {

                var result = _ProductionRateRepository.GetAll().Where(a => a.IsDeleted == false && a.IsUpdated == false).Include(i => i.Item).ToList();
                return result.MapTo<List<ProductionRateDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        
        public async Task<List<ProductionRateDto>> GetFilteredProductionRates(long? itemTypeId, long? categoryId, long? subCategoryId)
        {
            try
            {
                var result = _ProductionRateRepository.GetAll()
                    .Where(a => a.IsDeleted == false && a.IsUpdated == false)
                    .Include(i => i.Item)
                    .ToList();

                if (itemTypeId > 0)
                {
                    result = result.Where(a => a.Item.ItemTypeId == itemTypeId).ToList();
                }

                if (categoryId > 0)
                {
                    result = result.Where(a => a.Item.CategoryId == categoryId).ToList();
                }

                if (subCategoryId > 0)
                {
                    result.Where(a => a.Item.SubCategoryId == subCategoryId).ToList();
                }

                return result.MapTo<List<ProductionRateDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<ProductionRateDto>> GetAdvanceProductionRates(long? itemId, DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                var result = _ProductionRateRepository.GetAll()
                    .Where(a => a.IsDeleted == false && a.IsUpdated == true)
                    .Include(i => i.Item)
                    .ToList();

                if (itemId > 0)
                {
                    result = result.Where(a => a.ItemId == itemId).ToList();
                }

                if (fromDate != null && toDate != null)
                {
                    result = result.Where(a => a.CreationTime.Date >= fromDate && a.CreationTime.Date <= toDate).ToList();
                }

                return result.MapTo<List<ProductionRateDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        public override async Task<ProductionRateDto> Create(ProductionRateDto input)
        {
            try
            {
                var result = ObjectMapper.Map<ProductionRate>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                await _ProductionRateRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<ProductionRateDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public override async Task<ProductionRateDto> Update(ProductionRateDto input)
        {
            try
            {
                var data = ObjectMapper.Map<ProductionRate>(input);
                data.LastModificationTime = DateTime.Now;
                data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);


                var result1 = _ProductionRateRepository.GetAll().Where(a => a.Id == data.Id).FirstOrDefault();
                result1.IsUpdated = true;
                await _ProductionRateRepository.UpdateAsync(result1);


                data.Id = 0;
                data.IsUpdated = false;
                await _ProductionRateRepository.InsertAsync(data);

                CurrentUnitOfWork.SaveChanges();

                var result = data.MapTo<ProductionRateDto>();
                return result;
            }
            catch ( Exception ex)
            {

                throw ex;
            }
            
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var result = _ProductionRateRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _ProductionRateRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        
    }
}
