﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ProductionRates.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ProductionRates
{
    public interface IProductionRateAppService : IAsyncCrudAppService<ProductionRateDto, long, PagedResultRequestDto, ProductionRateDto, ProductionRateDto>
    {
    }
}
