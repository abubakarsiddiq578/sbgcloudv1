﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.ProductionRate;
using CloudBaseLine.Entities.ProductionRates.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ProductionRates
{
    public class ProductionRateMasterAppService : AsyncCrudAppService<ProductionRateMaster, ProductionRateMasterDto, long, PagedResultRequestDto, ProductionRateMasterDto, ProductionRateMasterDto>, IProductionRateMasterAppService
    {
        private readonly IRepository<ProductionRateMaster, long> _ProductionRateMasterRepository;
        private readonly IRepository<ProductionRateDetail, long> _ProductionRateDetailRepositoty;
        private readonly IPermissionManager _permissionManager;

        public ProductionRateMasterAppService(IRepository<ProductionRateMaster, long> _repository, IPermissionManager _Manager, IRepository<ProductionRateDetail, long> _detailRepository) : base(_repository)
        {
            _ProductionRateMasterRepository = _repository;
            _ProductionRateDetailRepositoty = _detailRepository;
            _permissionManager = _Manager;
        }

        public async Task<List<ProductionRateMasterDto>> GetAllProductionRate()
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _ProductionRateMasterRepository.GetAll()
                    .Where(a => a.IsDeleted == false && a.TenantId == TenantId)
                    .Include(a => a.Item)
                    .ToList();
                return result.MapTo<List<ProductionRateMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<List<ProductionRateDetailDto>> GetAllDetailById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = await _ProductionRateDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.ProductionRateMasterId == id && a.TenantId == TenantId)
                    .Include(a=> a.Category)
                    .Include(a => a.SubCategory)
                    .Include(a => a.Department)
                    .ToListAsync();
                return result.MapTo<List<ProductionRateDetailDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task Add(ProductionRateMasterDto input)
        {
            try
            {
                var result = ObjectMapper.Map<ProductionRateMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _ProductionRateMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.ProductionRateDetails)
                {
                    i.ProductionRateMasterId = response.Id;

                    await _ProductionRateDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                //var data = result.MapTo<ProductionRateMasterDto>();
                //return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override async Task<ProductionRateMasterDto> Create(ProductionRateMasterDto input)
        {
            try
            {
                var result = ObjectMapper.Map<ProductionRateMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _ProductionRateMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.ProductionRateDetails)
                {
                    i.ProductionRateMasterId = response.Id;

                    await _ProductionRateDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                var data = result.MapTo<ProductionRateMasterDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task Edit(ProductionRateMasterDto input)
        {

            try
            {
                var data = ObjectMapper.Map<ProductionRateMaster>(input);
                data.LastModificationTime = DateTime.Now;
                data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _ProductionRateMasterRepository.UpdateAsync(data);

                foreach (var i in input.ProductionRateDetails)
                {
                    i.ProductionRateMasterId = response.Id;

                    if (i.Id < 0)
                    {
                        await _ProductionRateDetailRepositoty.InsertAsync(i);
                    }
                    else
                    {
                        await _ProductionRateDetailRepositoty.UpdateAsync(i);
                    }
                }

                var masterResult = _ProductionRateMasterRepository.GetAll().Include(x => x.ProductionRateDetails).Where(x => x.Id == input.Id).FirstOrDefault();

                bool checkedBit = true;

                foreach (var x in masterResult.ProductionRateDetails)
                {
                    checkedBit = true;

                    foreach (var y in input.ProductionRateDetails)
                    {
                        if (x.Id == y.Id)
                        {
                            checkedBit = false;
                            break;
                        }
                    }

                    if (checkedBit == true)
                    {
                        if (x != null)
                        {
                            await _ProductionRateDetailRepositoty.DeleteAsync(x);
                        }
                    }
                }

                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        public override async Task<ProductionRateMasterDto> Update(ProductionRateMasterDto input)
        {
            var data = ObjectMapper.Map<ProductionRateMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await _ProductionRateMasterRepository.UpdateAsync(data);

            foreach (var i in input.ProductionRateDetails)
            {
                i.ProductionRateMasterId = response.Id;

                if (i.Id < 0)
                {
                    await _ProductionRateDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _ProductionRateDetailRepositoty.UpdateAsync(i);
                }
            }

            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ProductionRateMasterDto>();
            return result;
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var masterResult = _ProductionRateMasterRepository.GetAll().Include(x => x.ProductionRateDetails).Where(x => x.Id == input.Id).FirstOrDefault();
            if (masterResult != null)
            {
                foreach (var i in masterResult.ProductionRateDetails)
                {
                    if (i != null)
                    {
                        await _ProductionRateDetailRepositoty.DeleteAsync(i);
                    }
                }

                await _ProductionRateMasterRepository.DeleteAsync(masterResult);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
