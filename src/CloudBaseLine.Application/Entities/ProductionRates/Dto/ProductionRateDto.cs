﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ProductionRates.Dto
{
    [AutoMapTo(typeof(ProductionRate)), AutoMapFrom(typeof(ProductionRate))]
    public class ProductionRateDto : EntityDto<long>
    {
        public long ItemId { get; set; }
        public virtual Item Item { get; set; }

        public double Rate { get; set; }


        public DateTime ProductionRateDate { get; set; }

        public double RepairRate { get; set; }

        public Boolean? IsUpdated { get; set; }
    }
}
