﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Categories.Dto
{

    [AutoMapTo(typeof(Category)), AutoMapFrom(typeof(CategoryDto))]
    public class CategoryDto:EntityDto<long>
    {

        public string Code { get; set; }

        public string Name { get; set; }


        public int SortOrder { get; set; }

        public string Remarks { get; set; }

        public int TenantId { get; set; }

    }
}
