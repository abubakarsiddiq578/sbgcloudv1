﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Categories.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Categories
{
    public class CategoryAppService : AsyncCrudAppService<Category ,CategoryDto, long, PagedResultRequestDto, CategoryDto, CategoryDto>
    {
        private readonly IRepository<Category, long> CategoryRepository;
        private readonly IPermissionManager _permissionManager;


        public CategoryAppService(IRepository<Category, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            CategoryRepository = _repository;

            _permissionManager = _Manager;

        }


     
        public async Task<List<CategoryDto>> GetAllCategories() {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var a = CategoryRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId ).ToList();

                return a.MapTo<List<CategoryDto>>();
            }

            catch(Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Categories_Create)]
        public override async Task<CategoryDto> Create(CategoryDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = ObjectMapper.Map<Category>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await CategoryRepository.InsertAsync(result);
            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            var data = result.MapTo<CategoryDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Categories_Edit)]
        public override async Task<CategoryDto> Update(CategoryDto input)
        {
            var data = ObjectMapper.Map<Category>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await CategoryRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CategoryDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Categories_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = CategoryRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId ).FirstOrDefault();
            if (result != null)
            {
                await CategoryRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }





    }
}
