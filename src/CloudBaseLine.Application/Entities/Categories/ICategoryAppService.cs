﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Categories.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Categories
{
    public interface ICategoryAppService : IAsyncCrudAppService<CategoryDto, long, PagedResultRequestDto, CategoryDto, CategoryDto>
    {
    }
}
