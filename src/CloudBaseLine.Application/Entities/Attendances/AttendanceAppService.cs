﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Attendances.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Attendances
{
    public class AttendanceAppService: AsyncCrudAppService<Attendance, AttendanceDto, long, PagedResultRequestDto, AttendanceDto, AttendanceDto>, IAttendanceAppService
    {
        private readonly IRepository<Attendance, long> AttendanceRepository;
        private readonly IPermissionManager _permissionManager;


        public AttendanceAppService(IRepository<Attendance, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            AttendanceRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<AttendanceDto>> GetAllAttendances()
        {
            try
            {
                var result = AttendanceRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.Employee).Include(d => d.Department).Include(d => d.EmployeeDesignation).Include(s => s.AttendanceTypes).ToList();


                return result.MapTo<List<AttendanceDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<AttendanceDto>> GetFilterdAttendances(DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
               
                //var result = AttendanceRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId ).Include(a => a.Employee).Include(d => d.Department).Include(d => d.EmployeeDesignation).Include(s => s.AttendanceTypes).ToList();
                var result = AttendanceRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.Employee).ToList();
                if (fromDate != null && toDate != null)
                {
                    result = result.Where(a => a.AttendanceDate.Date >= fromDate && a.AttendanceDate.Date <= toDate).ToList();
                }
                return result.MapTo<List<AttendanceDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public override async Task<AttendanceDto> Create(AttendanceDto input)
        {
            var result = ObjectMapper.Map<Attendance>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await AttendanceRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<AttendanceDto>();
            return data;
        }


        public override async Task<AttendanceDto> Update(AttendanceDto input)
        {
            var data = ObjectMapper.Map<Attendance>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await AttendanceRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<AttendanceDto>();
            return result;
        }


        public override async Task Delete(EntityDto<long> input)
        {
            var result = AttendanceRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await AttendanceRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }



    }
}
