﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Attendances.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Attendances
{
    public interface IAttendanceAppService : IAsyncCrudAppService<AttendanceDto, long, PagedResultRequestDto, AttendanceDto, AttendanceDto>
    {
    }
}
