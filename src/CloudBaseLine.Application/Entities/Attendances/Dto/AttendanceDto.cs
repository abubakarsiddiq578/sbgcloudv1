﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Attendances.Dto
{
    [AutoMapTo(typeof(Attendance)), AutoMapFrom(typeof(AttendanceDto))]
    public class AttendanceDto:EntityDto<long>
    {
        public long? EmployeeId { get; set; }
        public Employee Employee { get; set; }
        

        public long? DesignationId { get; set; }
        public EmployeeDesignation EmployeeDesignation { get; set; }

        public long? DepartmentId { get; set; }
        public Department Department { get; set; }

        public long? Status { get; set; }

        public AttendanceType AttendanceTypes { get; set; }

        public DateTime AttendanceDate { get; set; }


        public string AttendanceType { get; set; }


        public string Comments { get; set; }


        public int TenantId { get; set; }

    }
}
