﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Zones.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Zones
{
    public class ZoneAppService : AsyncCrudAppService<Zone, ZoneDto, long, PagedResultRequestDto, ZoneDto, ZoneDto>,IZoneAppService
    {
        private readonly IRepository<Zone, long> ZoneRepository;
        private readonly IPermissionManager _permissionManager;

        public ZoneAppService(IRepository<Zone, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            ZoneRepository = _repository;

            _permissionManager = _Manager;

        }



        public async Task<List<ZoneDto>> GetAllZones()
        {

            try
            {
                var a = ZoneRepository.GetAll().Where(b => b.IsDeleted == false).Include(p=>p.Region).ToList();

                return a.MapTo<List<ZoneDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Zones_Create)]
        public override async Task<ZoneDto> Create(ZoneDto input)
        {
            var result = ObjectMapper.Map<Zone>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await ZoneRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<ZoneDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Zones_Edit)]
        public override async Task<ZoneDto> Update(ZoneDto input)
        {
            var data = ObjectMapper.Map<Zone>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await ZoneRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ZoneDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Zones_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = ZoneRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await ZoneRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


        public async Task<List<ZoneDto>> GetZoneByRegionId(long id)
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = ZoneRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId && b.RegionId == id).Include(p => p.Region).ToList();

                return a.MapTo<List<ZoneDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }

    }
}
