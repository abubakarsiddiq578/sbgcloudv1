﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Zones.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Zones
{
    public interface IZoneAppService : IAsyncCrudAppService<ZoneDto,long,PagedResultRequestDto,ZoneDto,ZoneDto>
    {

    }
}
