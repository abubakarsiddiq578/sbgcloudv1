﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Zones.Dto
{
    [AutoMapTo(typeof(Zone)),AutoMapFrom(typeof(ZoneDto))]
    public class ZoneDto:EntityDto<long>
    {
        public long RegionId { get; set; }
        public Region Region { get; set; }

        public string ZoneName { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }

        public bool isActive { get; set; }


    }
}
