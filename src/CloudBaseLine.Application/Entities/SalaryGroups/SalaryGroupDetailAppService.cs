﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.SalaryGroup;
using CloudBaseLine.Entities.SalaryGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SalaryGroups
{
    public class SalaryGroupDetailAppService : AsyncCrudAppService<SalaryGroupDetail, SalaryGroupDetailDto, long, PagedResultRequestDto, SalaryGroupDetailDto, SalaryGroupDetailDto>, ISalaryGroupDetailAppService
    {
        private readonly IRepository<SalaryGroupDetail, long> _SalaryGroupDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public SalaryGroupDetailAppService(IRepository<SalaryGroupDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _SalaryGroupDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }
}
