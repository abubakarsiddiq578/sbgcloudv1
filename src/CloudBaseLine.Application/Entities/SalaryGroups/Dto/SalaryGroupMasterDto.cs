﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.SalaryGroup;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SalaryGroups.Dto
{
    [AutoMapTo(typeof(SalaryGroupMaster)), AutoMapFrom(typeof(SalaryGroupMasterDto))]
    public class SalaryGroupMasterDto : EntityDto<long>
    {
        public string SalaryGroupTitle { get; set; }
        public IEnumerable<SalaryGroupDetail> SalaryGroupDetails { get; set; }
        public int TenantId { get; set; }
    }
}
