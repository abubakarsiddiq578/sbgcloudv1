﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.SalaryGroup;
using System;
using System.Collections.Generic;
using System.Text;


namespace CloudBaseLine.Entities.SalaryGroups.Dto
{
    [AutoMapTo(typeof(SalaryGroupDetail)), AutoMapFrom(typeof(SalaryGroupDetailDto))]
    public class SalaryGroupDetailDto : EntityDto<long>
    {
        public long SalaryGroupMasterId { get; set; }
        public SalaryGroupMaster SalaryGroupMaster { get; set; }

        public string SalaryType { get; set; }

        public string ApplyValue { get; set; }

        public double Value { get; set; }

        public int SortOrder { get; set; }

        public bool Dedeuction { get; set; }

        public bool Exempted { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }
    }
}
