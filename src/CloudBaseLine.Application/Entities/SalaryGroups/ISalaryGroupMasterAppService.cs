﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.SalaryGroup;
using CloudBaseLine.Entities.SalaryGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.SalaryGroups
{
    public interface ISalaryGroupMasterAppService : IAsyncCrudAppService<SalaryGroupMasterDto, long, PagedResultRequestDto, SalaryGroupMasterDto, SalaryGroupMasterDto>
    {
        Task<List<SalaryGroupMasterDto>> GetAllSalaryGroup();
        Task<List<SalaryGroupDetailDto>> GetAllDetailById(long id);
    }
}
