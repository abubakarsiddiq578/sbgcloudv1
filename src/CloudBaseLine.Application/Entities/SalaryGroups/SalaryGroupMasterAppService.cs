﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.SalaryGroup;
using CloudBaseLine.Entities.SalaryGroups.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.SalaryGroups
{
    public class SalaryGroupMasterAppService : AsyncCrudAppService<SalaryGroupMaster, SalaryGroupMasterDto, long, PagedResultRequestDto, SalaryGroupMasterDto, SalaryGroupMasterDto>, ISalaryGroupMasterAppService
    {
        private readonly IRepository<SalaryGroupMaster, long> _SalaryGroupMasterRepository;
        private readonly IRepository<SalaryGroupDetail, long> _SalaryGroupDetailRepositoty;
        private readonly IPermissionManager _permissionManager;

        public SalaryGroupMasterAppService(IRepository<SalaryGroupMaster, long> _repository, IPermissionManager _Manager, IRepository<SalaryGroupDetail, long> _detailRepository) : base(_repository)
        {
            _SalaryGroupMasterRepository = _repository;
            _SalaryGroupDetailRepositoty = _detailRepository;
            _permissionManager = _Manager;
        }

        public async Task<List<SalaryGroupMasterDto>> GetAllSalaryGroup()
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if(TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _SalaryGroupMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<SalaryGroupMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<List<SalaryGroupDetailDto>> GetAllDetailById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _SalaryGroupDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.SalaryGroupMasterId == id && a.TenantId == TenantId)
                    //.Include(m => m.SalaryGroupMaster)
                    .ToList();
                return result.MapTo<List<SalaryGroupDetailDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryGroups_Create)]
        public async Task Add(SalaryGroupMasterDto input)
        {
            try
            {
                var result = ObjectMapper.Map<SalaryGroupMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _SalaryGroupMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.SalaryGroupDetails)
                {
                    i.SalaryGroupMasterId = response.Id;

                    await _SalaryGroupDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                //var data = result.MapTo<SalaryGroupMasterDto>();
                //return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryGroups_Create)]
        public override async Task<SalaryGroupMasterDto> Create(SalaryGroupMasterDto input)
        {
            try
            {
                var result = ObjectMapper.Map<SalaryGroupMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _SalaryGroupMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.SalaryGroupDetails)
                {
                    i.SalaryGroupMasterId = response.Id;

                    await _SalaryGroupDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                var data = result.MapTo<SalaryGroupMasterDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryGroups_Edit)]
        public async Task Edit(SalaryGroupMasterDto input)
        {
            var data = ObjectMapper.Map<SalaryGroupMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await _SalaryGroupMasterRepository.UpdateAsync(data);

            foreach (var i in input.SalaryGroupDetails)
            {
                i.SalaryGroupMasterId = response.Id;

                if (i.Id < 0)
                {
                    await _SalaryGroupDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _SalaryGroupDetailRepositoty.UpdateAsync(i);
                }
            }

            var masterResult = _SalaryGroupMasterRepository.GetAll().Include(x => x.SalaryGroupDetails).Where(x => x.Id == input.Id).FirstOrDefault();

            bool checkedBit = true;

            foreach (var x in masterResult.SalaryGroupDetails)
            {
                checkedBit = true;

                foreach (var y in input.SalaryGroupDetails)
                {
                    if (x.Id == y.Id)
                    {
                        checkedBit = false;
                        break;
                    }
                }

                if (checkedBit == true)
                {
                    if (x != null)
                    {
                        await _SalaryGroupDetailRepositoty.DeleteAsync(x);
                    }
                }
            }

            CurrentUnitOfWork.SaveChanges();
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryGroups_Edit)]
        public override async Task<SalaryGroupMasterDto> Update(SalaryGroupMasterDto input)
        {
            var data = ObjectMapper.Map<SalaryGroupMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await _SalaryGroupMasterRepository.UpdateAsync(data);

            foreach (var i in input.SalaryGroupDetails)
            {
                i.SalaryGroupMasterId = response.Id;

                if(i.Id < 0)
                {
                    await _SalaryGroupDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _SalaryGroupDetailRepositoty.UpdateAsync(i);
                }
            }

            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<SalaryGroupMasterDto>();
            return result;
        }

           //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryGroups_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var masterResult = _SalaryGroupMasterRepository.GetAll().Include(x => x.SalaryGroupDetails).Where(x => x.Id == input.Id).FirstOrDefault();
            if (masterResult != null)
            {
                foreach (var i in masterResult.SalaryGroupDetails)
                {
                    if (i != null)
                    {
                        await _SalaryGroupDetailRepositoty.DeleteAsync(i);
                    }
                }

                await _SalaryGroupMasterRepository.DeleteAsync(masterResult);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
