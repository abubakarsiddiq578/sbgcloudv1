﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.SalaryGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SalaryGroups
{
    public interface ISalaryGroupDetailAppService : IAsyncCrudAppService<SalaryGroupDetailDto, long, PagedResultRequestDto, SalaryGroupDetailDto, SalaryGroupDetailDto>
    {

    }
}
