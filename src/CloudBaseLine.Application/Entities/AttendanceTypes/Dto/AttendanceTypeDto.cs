﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.Attendances.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.AttendanceTypes.Dto
{
    [AutoMapTo(typeof(AttendanceType)), AutoMapFrom(typeof(AttendanceTypeDto))]
    public class AttendanceTypeDto : EntityDto<long>
    {
        public string Status { get; set; }

        public string Comments { get; set; }
    }
}
