﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.AttendanceTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.AttendanceTypes
{
    public interface IAttendanceTypeAppService: IAsyncCrudAppService<AttendanceTypeDto, long, PagedResultRequestDto, AttendanceTypeDto, AttendanceTypeDto>
    {
    }
}
