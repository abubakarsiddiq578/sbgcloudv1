﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.AttendanceTypes;
using CloudBaseLine.Entities.AttendanceTypes.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.AttendanceTypes
{
    public class AttendanceTypeAppService : AsyncCrudAppService<AttendanceType, AttendanceTypeDto, long, PagedResultRequestDto, AttendanceTypeDto, AttendanceTypeDto>, IAttendanceTypeAppService
    {
        private readonly IRepository<AttendanceType, long> AttendanceTypeRepository;
        private readonly IPermissionManager _permissionManager;


        public AttendanceTypeAppService(IRepository<AttendanceType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            AttendanceTypeRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<AttendanceTypeDto>> GetAllAttendanceTypes()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = AttendanceTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<AttendanceTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public override async Task<AttendanceTypeDto> Create(AttendanceTypeDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = ObjectMapper.Map<AttendanceType>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await AttendanceTypeRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<AttendanceTypeDto>();
            return data;
        }


        public override async Task<AttendanceTypeDto> Update(AttendanceTypeDto input)
        {
            var data = ObjectMapper.Map<AttendanceType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await AttendanceTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<AttendanceTypeDto>();
            return result;
        }


        public override async Task Delete(EntityDto<long> input)
        {

            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = AttendanceTypeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await AttendanceTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }



    }
}
