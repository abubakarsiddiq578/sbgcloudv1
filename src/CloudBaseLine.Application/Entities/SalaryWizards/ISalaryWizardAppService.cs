﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Salary.Dto;
using CloudBaseLine.Entities.SalaryWizards.SalaryWizardsDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.SalaryWizards
{
    public interface ISalaryWizardAppService : IAsyncCrudAppService<SalaryWizardDto, long, PagedResultRequestDto, SalaryWizardDto, SalaryWizardDto>
    {
        
    }
}
