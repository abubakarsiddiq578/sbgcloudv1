﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.SalaryGroups.Dto;
using CloudBaseLine.Entities.SalaryWizards.SalaryWizardsDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.SalaryWizards
{
    public class SalaryWizardAppService : AsyncCrudAppService<SalaryWizard, SalaryWizardDto, long, PagedResultRequestDto, SalaryWizardDto, SalaryWizardDto>, ISalaryWizardAppService
    {
        private readonly IRepository<SalaryWizard, long> _SalaryWizardRepository;
        private readonly IRepository<Employee, long> _EmployeeRepositoty;
        private readonly IPermissionManager _permissionManager;

        public SalaryWizardAppService(IRepository<SalaryWizard, long> _repository, IRepository<Employee, long> _employeeRepository, IPermissionManager _Manager) : base(_repository)
        {
            _SalaryWizardRepository = _repository;
            _EmployeeRepositoty = _employeeRepository;

            _permissionManager = _Manager;
        }

        public string GetSalaryWizard()
        {
          
                //var TenantId = AbpSession.TenantId;
                //if (TenantId == null)
                //{
                //    TenantId = 0;
                //}
                //var result = _SalaryWizardRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();

                var result1 = _EmployeeRepositoty.GetAll().Where(a => a.IsDeleted == false && a.PayRoll_Division == "Monthly").FirstOrDefault();

                return result1.PayRoll_Division;

        }


        //public async Task<List<SalaryGroupDetailDto>> GetAllDetailById(long id)
        //{
        //    try
        //    {
        //        var TenantId = AbpSession.TenantId;
        //        if (TenantId == null)
        //        {
        //            TenantId = 0;
        //        }
        //        var result = _SalaryGroupDetailRepositoty.GetAll()
        //            .Where(a => a.IsDeleted == false && a.SalaryGroupMasterId == id && a.TenantId == TenantId)
        //            //.Include(m => m.SalaryGroupMaster)
        //            .ToList();
        //        return result.MapTo<List<SalaryGroupDetailDto>>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        //public async Task Add(SalaryGroupMasterDto input)
        //{
        //    try
        //    {
        //        var result = ObjectMapper.Map<SalaryGroupMaster>(input);
        //        result.LastModificationTime = DateTime.Now;
        //        result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
        //        var response = await _SalaryGroupMasterRepository.InsertAsync(result);
        //        //CurrentUnitOfWork.SaveChanges();
        //        foreach (var i in input.SalaryGroupDetails)
        //        {
        //            i.SalaryGroupMasterId = response.Id;
        //            await _SalaryGroupDetailRepositoty.InsertAsync(i);
        //        }
        //        try
        //        {
        //            CurrentUnitOfWork.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public override async Task<SalaryGroupMasterDto> Create(SalaryGroupMasterDto input)
        //{
        //    try
        //    {
        //        var result = ObjectMapper.Map<SalaryGroupMaster>(input);
        //        result.LastModificationTime = DateTime.Now;
        //        result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

        //        var response = await _SalaryGroupMasterRepository.InsertAsync(result);
        //        //CurrentUnitOfWork.SaveChanges();

        //        foreach (var i in input.SalaryGroupDetails)
        //        {
        //            i.SalaryGroupMasterId = response.Id;
        //            await _SalaryGroupDetailRepositoty.InsertAsync(i);
        //        }
        //        try
        //        {
        //            CurrentUnitOfWork.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //        var data = result.MapTo<SalaryGroupMasterDto>();
        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        //public async Task Edit(SalaryGroupMasterDto input)
        //{
        //    var data = ObjectMapper.Map<SalaryGroupMaster>(input);
        //    data.LastModificationTime = DateTime.Now;
        //    data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

        //    var response = await _SalaryGroupMasterRepository.UpdateAsync(data);

        //    foreach (var i in input.SalaryGroupDetails)
        //    {
        //        i.SalaryGroupMasterId = response.Id;

        //        if (i.Id < 0)
        //        {
        //            await _SalaryGroupDetailRepositoty.InsertAsync(i);
        //        }
        //        else
        //        {
        //            await _SalaryGroupDetailRepositoty.UpdateAsync(i);
        //        }
        //    }

        //    var masterResult = _SalaryGroupMasterRepository.GetAll().Include(x => x.SalaryGroupDetails).Where(x => x.Id == input.Id).FirstOrDefault();

        //    bool checkedBit = true;

        //    foreach (var x in masterResult.SalaryGroupDetails)
        //    {
        //        checkedBit = true;

        //        foreach (var y in input.SalaryGroupDetails)
        //        {
        //            if (x.Id == y.Id)
        //            {
        //                checkedBit = false;
        //                break;
        //            }
        //        }

        //        if (checkedBit == true)
        //        {
        //            if (x != null)
        //            {
        //                await _SalaryGroupDetailRepositoty.DeleteAsync(x);
        //            }
        //        }
        //    }

        //    CurrentUnitOfWork.SaveChanges();
        //}

        //public override async Task<SalaryGroupMasterDto> Update(SalaryGroupMasterDto input)
        //{
        //    var data = ObjectMapper.Map<SalaryGroupMaster>(input);
        //    data.LastModificationTime = DateTime.Now;
        //    data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

        //    var response = await _SalaryGroupMasterRepository.UpdateAsync(data);

        //    foreach (var i in input.SalaryGroupDetails)
        //    {
        //        i.SalaryGroupMasterId = response.Id;

        //        if (i.Id < 0)
        //        {
        //            await _SalaryGroupDetailRepositoty.InsertAsync(i);
        //        }
        //        else
        //        {
        //            await _SalaryGroupDetailRepositoty.UpdateAsync(i);
        //        }
        //    }

        //    CurrentUnitOfWork.SaveChanges();

        //    var result = data.MapTo<SalaryGroupMasterDto>();
        //    return result;
        //}

        //public override async Task Delete(EntityDto<long> input)
        //{
        //    var masterResult = _SalaryGroupMasterRepository.GetAll().Include(x => x.SalaryGroupDetails).Where(x => x.Id == input.Id).FirstOrDefault();
        //    if (masterResult != null)
        //    {
        //        foreach (var i in masterResult.SalaryGroupDetails)
        //        {
        //            if (i != null)
        //            {
        //                await _SalaryGroupDetailRepositoty.DeleteAsync(i);
        //            }
        //        }

        //        await _SalaryGroupMasterRepository.DeleteAsync(masterResult);
        //        CurrentUnitOfWork.SaveChanges();
        //    }
        //}
    }
}
