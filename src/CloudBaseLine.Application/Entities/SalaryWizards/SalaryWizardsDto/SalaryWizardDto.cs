﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SalaryWizards.SalaryWizardsDto
{
    [AutoMapTo(typeof(SalaryWizard)), AutoMapFrom(typeof(SalaryWizardDto))]
    public class SalaryWizardDto : EntityDto<long>
    {
        public string SelectPayrol { get; set; }

        public DateTime SalaryDate { get; set; }

        public long SalaryCostCenter { get; set; }

        public long SalaryDepartment { get; set; }

        public long SalaryCompany { get; set; }

        public long SalaryDeduction { get; set; }

        public long SalaryFine { get; set; }

        public long SalaryAllowance { get; set; }

        public long SalaryBonus { get; set; }
    }
}
