﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.WorkGroup;
using CloudBaseLine.Entities.WorkGroups.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudBaseLine.Entities.EmployeeInfo.Dto;
using CloudBaseLine.Authorization;

namespace CloudBaseLine.Entities.WorkGroups
{
   public class WorkGroupMasterAppService : AsyncCrudAppService<WorkGroupMaster, WorkGroupMasterDto, long, PagedResultRequestDto, WorkGroupMasterDto, WorkGroupMasterDto>, IWorkGroupMasterAppService
    {
        private readonly IRepository<WorkGroupMaster, long> _WorkGroupMasterRepository;
        private readonly IRepository<WorkGroupEmployeeDetail, long> _WorkGroupEmployeeDetailRepositoty;
        private readonly IRepository<WorkGroupItemDetail, long> _WorkGroupItemDetailRepositoty;
        private readonly IRepository<Employee, long> _EmployeeRepository;
        private readonly IPermissionManager _permissionManager;
        


        public WorkGroupMasterAppService(IRepository<WorkGroupMaster, long> _repository, IPermissionManager _Manager, IRepository<WorkGroupEmployeeDetail, long> _EmployeeDetailRepository, IRepository<WorkGroupItemDetail, long> _ItemDetailRepository, IRepository<Employee, long> _EmployeesRepository) : base(_repository)
        {
            _WorkGroupMasterRepository = _repository;
            _WorkGroupEmployeeDetailRepositoty = _EmployeeDetailRepository;
            _WorkGroupItemDetailRepositoty = _ItemDetailRepository;
            _EmployeeRepository = _EmployeesRepository;
            _permissionManager = _Manager;

        }

        public async Task<List<WorkGroupMasterDto>> GetAllWorkGroup()
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _WorkGroupMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId)
                    .Include(a => a.Category)
                    .Include(a => a.SubCategory)
                    .Include(a => a.ItemType)
                    .ToList();
                return result.MapTo<List<WorkGroupMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<List<WorkGroupMasterDto>> GetAllWorkGroupById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _WorkGroupMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.Id == id &&  a.TenantId == TenantId)
                    .Include(a => a.Category)
                    .Include(a => a.SubCategory)
                    .Include(a => a.ItemType)
                    //.Include(a => a.WorkGroupEmployeeDetails)
                    //.Include(a => a.WorkGroupItemDetails)
                    .ToList();
                return result.MapTo<List<WorkGroupMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public async Task<List<WorkGroupEmployeeDetailDto>> GetAllEmployeeDetailById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _WorkGroupEmployeeDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.WorkGroupMasterId == id && a.TenantId == TenantId)
                    .Include(m => m.Employee)
                    .Include(m => m.Employee.EmployeeDesignation)
                    .Include(m => m.Employee.Department)
                    .ToList();
                return result.MapTo<List<WorkGroupEmployeeDetailDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Employee>> GetEmployeeDetailsById(long id)
        {
            try
            {
                //var TenantId = AbpSession.TenantId;

                //if (TenantId == null)
                //{
                //    TenantId = 0;
                //}

                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _EmployeeRepository.GetAll()
                    .Where(a => a.IsDeleted == false && a.Id == id && a.TenantId == TenantId )
                    .Include(b => b.Department)
                    .Include(b => b.EmployeeDesignation);
                return result.MapTo<List<Employee>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<WorkGroupItemDetailDto>> GetAllItemDetailById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _WorkGroupItemDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.WorkGroupMasterId == id && a.TenantId == TenantId)
                    //.Include(m => m.WorkGroupMaster)
                    .ToList();
                return result.MapTo<List<WorkGroupItemDetailDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_WorkGroups_Create)]
        public async Task Add(WorkGroupMasterDto input)
        {
            try
            {

                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<WorkGroupMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);


                var response = await _WorkGroupMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.WorkGroupEmployeeDetails)
                {
                    i.WorkGroupMasterId = response.Id;
                    i.TenantId = Convert.ToInt32(TenantId);
                    await _WorkGroupEmployeeDetailRepositoty.InsertAsync(i);
                }

                foreach (var i in input.WorkGroupItemDetails)
                {
                    i.WorkGroupMasterId = response.Id;

                    await _WorkGroupItemDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                //var data = result.MapTo<WorkGroupMasterDto>();
                //return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_WorkGroups_Create)]
        public override async Task<WorkGroupMasterDto> Create(WorkGroupMasterDto input)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<WorkGroupMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                var response = await _WorkGroupMasterRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.WorkGroupEmployeeDetails)
                {
                    i.WorkGroupMasterId = response.Id;
                    i.TenantId = Convert.ToInt32(TenantId);
                    await _WorkGroupEmployeeDetailRepositoty.InsertAsync(i);
                }

                foreach (var i in input.WorkGroupItemDetails)
                {
                    i.WorkGroupMasterId = response.Id;

                    await _WorkGroupItemDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
                var data = result.MapTo<WorkGroupMasterDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_WorkGroups_Edit)]
        public async Task Edit(WorkGroupMasterDto input)
        {

            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var data = ObjectMapper.Map<WorkGroupMaster>(input);
                data.LastModificationTime = DateTime.Now;
                data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);
                data.TenantId = Convert.ToInt32(TenantId);
                var response = await _WorkGroupMasterRepository.UpdateAsync(data);

                foreach (var i in input.WorkGroupEmployeeDetails)
                {
                    i.WorkGroupMasterId = response.Id;
                    i.TenantId = Convert.ToInt32(TenantId);
                    if (i.Id < 0)
                    {
                        
                        await _WorkGroupEmployeeDetailRepositoty.InsertAsync(i);
                    }
                    else
                    {
                        await _WorkGroupEmployeeDetailRepositoty.UpdateAsync(i);
                    }
                }
                foreach (var i in input.WorkGroupItemDetails)
                {
                    i.WorkGroupMasterId = response.Id;
                    i.TenantId = Convert.ToInt32(TenantId);
                    if (i.Id < 0)
                    {
                        await _WorkGroupItemDetailRepositoty.InsertAsync(i);
                    }
                    else
                    {
                        await _WorkGroupItemDetailRepositoty.UpdateAsync(i);
                    }
                }

                var masterEmployeeResult = _WorkGroupMasterRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).Include(x => x.WorkGroupEmployeeDetails).FirstOrDefault();
                var masterItemResult = _WorkGroupMasterRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).Include(x => x.WorkGroupItemDetails).FirstOrDefault();

                bool checkedBit = true;

                foreach (var x in masterEmployeeResult.WorkGroupEmployeeDetails)
                {
                    checkedBit = true;

                    foreach (var y in input.WorkGroupEmployeeDetails)
                    {
                        if (x.Id == y.Id )
                        {
                            checkedBit = false;
                            break;
                        }
                    }

                    if (checkedBit == true)
                    {
                        if (x != null)
                        {
                            await _WorkGroupEmployeeDetailRepositoty.DeleteAsync(x);
                        }
                    }
                }

                foreach (var x in masterItemResult.WorkGroupItemDetails)
                {
                    checkedBit = true;

                    foreach (var y in input.WorkGroupItemDetails)
                    {
                        if (x.Id == y.Id)
                        {
                            checkedBit = false;
                            break;
                        }
                    }

                    if (checkedBit == true)
                    {
                        if (x != null)
                        {
                            await _WorkGroupItemDetailRepositoty.DeleteAsync(x);
                        }
                    }
                }

                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_WorkGroups_Edit)]
        public override async Task<WorkGroupMasterDto> Update(WorkGroupMasterDto input)
        {
            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }

            var data = ObjectMapper.Map<WorkGroupMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);
            data.TenantId = Convert.ToInt32(TenantId);


            var response = await _WorkGroupMasterRepository.UpdateAsync(data);

            foreach (var i in input.WorkGroupEmployeeDetails)
            {
                i.WorkGroupMasterId = response.Id;
                i.TenantId = Convert.ToInt32(TenantId);
                if (i.Id < 0)
                {
                    await _WorkGroupEmployeeDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _WorkGroupEmployeeDetailRepositoty.UpdateAsync(i);
                }
            }

            foreach (var i in input.WorkGroupItemDetails)
            {
                i.WorkGroupMasterId = response.Id;
                i.TenantId = Convert.ToInt32(TenantId);
                if (i.Id < 0)
                {
                    await _WorkGroupItemDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _WorkGroupItemDetailRepositoty.UpdateAsync(i);
                }
            }

            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<WorkGroupMasterDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_WorkGroups_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }

            var masterResult = _WorkGroupMasterRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).Include(x => x.WorkGroupItemDetails).Include(x => x.WorkGroupEmployeeDetails).FirstOrDefault();
            if (masterResult != null)
            {
                foreach (var i in masterResult.WorkGroupEmployeeDetails)
                {
                    if (i != null)
                    {
                        await _WorkGroupEmployeeDetailRepositoty.DeleteAsync(i);
                    }
                }

                foreach (var i in masterResult.WorkGroupItemDetails)
                {
                    if (i != null)
                    {
                        await _WorkGroupItemDetailRepositoty.DeleteAsync(i);
                    }
                }

                await _WorkGroupMasterRepository.DeleteAsync(masterResult);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public Task<List<WorkGroupMasterDto>> GetAllSalaryGroup()
        {
            throw new NotImplementedException();
        }
    }
}
