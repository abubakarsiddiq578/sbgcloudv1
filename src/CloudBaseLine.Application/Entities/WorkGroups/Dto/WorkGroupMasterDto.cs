﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.WorkGroup;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.WorkGroups.Dto
{
    [AutoMapTo(typeof(WorkGroupMaster)), AutoMapFrom(typeof(WorkGroupMasterDto))]
    public class WorkGroupMasterDto :EntityDto<long>
    {
        public long ItemTypeId { get; set; }
        public virtual ItemType ItemType { get; set; }

        public long CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public long SubCategoryId { get; set; }
        public virtual SubCategory SubCategory { get; set; }

        public string GroupName { get; set; }
        public virtual IEnumerable<WorkGroupEmployeeDetail> WorkGroupEmployeeDetails { get; set; }

        public virtual IEnumerable<WorkGroupItemDetail> WorkGroupItemDetails { get; set; }

        public int TenantId { get; set; }
    }
}
