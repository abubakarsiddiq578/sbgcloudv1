﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.BaseEntity;
using CloudBaseLine.Entities.WorkGroup;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.WorkGroups.Dto
{
    [AutoMapTo(typeof(WorkGroupEmployeeDetail)), AutoMapFrom(typeof(WorkGroupEmployeeDetailDto))]
    public class WorkGroupEmployeeDetailDto : EntityDto<long>
    {
        public long WorkGroupMasterId { get; set; }
        //public WorkGroupMaster WorkGroupMaster { get; set; }
        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public double Ratio { get; set; }
    }
}
