﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.WorkGroup;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.WorkGroups.Dto
{
    [AutoMapTo(typeof(WorkGroupItemDetail)), AutoMapFrom(typeof(WorkGroupItemDetailDto))]
    public class WorkGroupItemDetailDto : EntityDto<long>
    {
        public long WorkGroupMasterId { get; set; }
       // public WorkGroupMaster WorkGroupMaster { get; set; }
        public long ItemId { get; set; }
        public Item Item { get; set; }

    }
}
