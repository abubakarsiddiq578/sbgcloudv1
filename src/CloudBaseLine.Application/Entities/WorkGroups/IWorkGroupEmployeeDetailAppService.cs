﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.WorkGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;
namespace CloudBaseLine.Entities.WorkGroups
{
    public interface IWorkGroupEmployeeDetailAppService : IAsyncCrudAppService<WorkGroupEmployeeDetailDto, long, PagedResultRequestDto, WorkGroupEmployeeDetailDto, WorkGroupEmployeeDetailDto>
    {

    }
}
