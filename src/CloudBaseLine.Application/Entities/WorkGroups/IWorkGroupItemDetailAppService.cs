﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.WorkGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.WorkGroups
{
  public interface IWorkGroupItemDetailAppService : IAsyncCrudAppService<WorkGroupItemDetailDto, long, PagedResultRequestDto, WorkGroupItemDetailDto, WorkGroupItemDetailDto>
    {

    }
}
