﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.WorkGroup;
using CloudBaseLine.Entities.WorkGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.WorkGroups
{
    public class WorkGroupItemDetailAppService : AsyncCrudAppService<WorkGroupItemDetail, WorkGroupItemDetailDto, long, PagedResultRequestDto, WorkGroupItemDetailDto, WorkGroupItemDetailDto>, IWorkGroupItemDetailAppService
    {
        private readonly IRepository<WorkGroupItemDetail, long> _WorkGroupItemDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public WorkGroupItemDetailAppService(IRepository<WorkGroupItemDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _WorkGroupItemDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }

}
