﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.WorkGroup;
using CloudBaseLine.Entities.WorkGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.WorkGroups
{
    public interface IWorkGroupMasterAppService : IAsyncCrudAppService<WorkGroupMasterDto, long, PagedResultRequestDto, WorkGroupMasterDto, WorkGroupMasterDto>
    {
        Task<List<WorkGroupMasterDto>> GetAllSalaryGroup();
        Task<List<WorkGroupEmployeeDetailDto>> GetAllEmployeeDetailById(long id);
        Task<List<WorkGroupItemDetailDto>> GetAllItemDetailById(long id);
    }
}
