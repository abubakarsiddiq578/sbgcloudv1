﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.WorkGroup;
using CloudBaseLine.Entities.WorkGroups.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.WorkGroups
{
    public class WorkGroupEmployeeDetailAppService : AsyncCrudAppService<WorkGroupEmployeeDetail, WorkGroupEmployeeDetailDto, long, PagedResultRequestDto, WorkGroupEmployeeDetailDto, WorkGroupEmployeeDetailDto>, IWorkGroupEmployeeDetailAppService
    {
        private readonly IRepository<WorkGroupEmployeeDetail, long> _WorkGroupEmployeeDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public WorkGroupEmployeeDetailAppService(IRepository<WorkGroupEmployeeDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _WorkGroupEmployeeDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }
}
