﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Terminations.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Terminations
{
    public interface ITerminationAppService : IAsyncCrudAppService<TerminationDto, long, PagedResultRequestDto,TerminationDto,TerminationDto>
    {

         Task<List<TerminationDto>> GetAllTermination();
    }
}
