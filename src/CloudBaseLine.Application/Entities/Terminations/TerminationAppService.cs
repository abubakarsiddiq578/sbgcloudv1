﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Terminations.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Terminations
{
    public class TerminationAppService : AsyncCrudAppService<Termination, TerminationDto, long, PagedResultRequestDto, TerminationDto, TerminationDto>, ITerminationAppService
    {
        private readonly IRepository<Termination, long> TerminationRepository;
        private readonly IPermissionManager _permissionManager;

        public TerminationAppService(IRepository<Termination, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            TerminationRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<TerminationDto>> GetAllTermination()
        {
            try
            {
                var result = TerminationRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.Employee).ToList();

          
                return result.MapTo<List<TerminationDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Terminations_Create)]
        public override async Task<TerminationDto> Create(TerminationDto input)
        {
            var result = ObjectMapper.Map<Termination>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await TerminationRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<TerminationDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Terminations_Edit)]
        public override async Task<TerminationDto> Update(TerminationDto input)
        {
            var data = ObjectMapper.Map<Termination>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await TerminationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<TerminationDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Terminations_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = TerminationRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await TerminationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }




    }
}
