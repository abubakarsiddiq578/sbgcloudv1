﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.Terminations.Dto
{
    [AutoMapTo(typeof(Termination)), AutoMapFrom(typeof(TerminationDto))]
    public class TerminationDto : EntityDto<long>
    {
        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }

        public DateTime TerminationDate { get; set; }

        public long EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public DateTime NoticeDate { get; set; }

        public string TerminationType { get; set; }

        public string Detail { get; set; }

        public int TenantId { get; set; }
    }
}
