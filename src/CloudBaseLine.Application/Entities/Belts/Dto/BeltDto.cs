﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Belts.Dto
{
    [AutoMapTo(typeof(Belt)),AutoMapFrom(typeof(BeltDto))]
    public class BeltDto : EntityDto<long>
    {
        public long ZoneId { get; set; }

        public Zone Zone { get; set; }

        public string Belt_Name { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }

        public bool isActive { get; set; }

        public int TenantId { get; set; }

    }
}
