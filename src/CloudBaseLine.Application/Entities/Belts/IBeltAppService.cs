﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Belts.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Belts
{
    public interface IBeltAppService : IAsyncCrudAppService<BeltDto,long,PagedResultRequestDto,BeltDto,BeltDto>
    {


    }
}
