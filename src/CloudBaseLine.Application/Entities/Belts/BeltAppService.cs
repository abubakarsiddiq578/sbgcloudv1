﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Belts.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Belts
{
    public class BeltAppService : AsyncCrudAppService<Belt,BeltDto, long, PagedResultRequestDto, BeltDto, BeltDto>,IBeltAppService
    {

        private readonly IRepository<Belt, long> BeltRepository;
        private readonly IPermissionManager _permissionManager;

        public BeltAppService(IRepository<Belt, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            BeltRepository = _repository;
            _permissionManager = _Manager;

        }



       

        public async Task<List<BeltDto>> GetAllBelts()
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = BeltRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId ).Include(r=>r.Zone).ToList();

                return a.MapTo<List<BeltDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Belts_Create)]
        public override async Task<BeltDto> Create(BeltDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<Belt>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await BeltRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<BeltDto>();
            return data;
        }



        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Belts_Edit)]
        public override async Task<BeltDto> Update(BeltDto input)
        {
            var data = ObjectMapper.Map<Belt>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await BeltRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<BeltDto>();
            return result;
        }



        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Belts_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = BeltRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await BeltRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<BeltDto>> GetBeltByZoneId(long id)
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = BeltRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId && b.ZoneId == id).Include(r => r.Zone).ToList();

                return a.MapTo<List<BeltDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


    }
}
