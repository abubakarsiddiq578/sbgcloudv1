﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Configurations.Dto
{
    [AutoMapTo(typeof(OvertimeConfiguration)), AutoMapFrom(typeof(OvertimeConfigurationDto))]
    public class OvertimeConfigurationDto : EntityDto<long>
    {
        public string WorkingDays { get; set; }

        public int Workinghours { get; set; }

        public int RegularDayHourRate { get; set; }

        public int OffDayHourRate { get; set; }

        public bool DefaultWorkingDays { get; set; }

        public int TenantId { get; set; }
    }
}
