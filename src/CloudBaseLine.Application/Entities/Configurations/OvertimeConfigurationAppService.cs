﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Configuration;
using CloudBaseLine.Entities.Configurations.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Configurations
{
    public class OvertimeConfigurationAppService : AsyncCrudAppService<OvertimeConfiguration, OvertimeConfigurationDto, long, PagedResultRequestDto, OvertimeConfigurationDto, OvertimeConfigurationDto>, IOvertimeConfigurationAppService
    {
        private readonly IRepository<OvertimeConfiguration, long> _OvertimeConfigurationrepository;
        private readonly IPermissionManager _permissionManager;

        public OvertimeConfigurationAppService(IRepository<OvertimeConfiguration, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _OvertimeConfigurationrepository = _repository;
            _permissionManager = _Manager;
        }


        
        public async Task<List<OvertimeConfigurationDto>> GetAllOverTimeConfiguration()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)

                {
                    TenantId = 0;
                }
                // var result = _OvertimeConfigurationrepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == AbpSession.TenantId).ToList();
                var result = _OvertimeConfigurationrepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<OvertimeConfigurationDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //var result = _UserCompanyRightrepository.GetAllIncluding(p=>p.User);
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Configurations_Create)]
        public override async Task<OvertimeConfigurationDto> Create(OvertimeConfigurationDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = ObjectMapper.Map<OvertimeConfiguration>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            result.TenantId = Convert.ToInt32(TenantId) ;
            await _OvertimeConfigurationrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();
            var data = result.MapTo<OvertimeConfigurationDto>();
            return data;

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Configurations_Edit)]
        public override async Task<OvertimeConfigurationDto> Update(OvertimeConfigurationDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var data = ObjectMapper.Map<OvertimeConfiguration>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);


            data.TenantId = Convert.ToInt32(TenantId);

            await _OvertimeConfigurationrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<OvertimeConfigurationDto>();
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// //Authorization with Permissions
        //[AbpAuthorize(PermissionNames.Pages_Hrm_Configurations_Delete)]
       
        //public override async Task Delete(EntityDto<long> input)
       //{
        //get logged in user's tenant id.
        // var TenantId = AbpSession.TenantId;
        //       if (TenantId == null)
        //     {
        //       TenantId = 0;
        // }
        //    var result = _OvertimeConfigurationrepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
        //    if (result != null)
        //    {
        //        await _OvertimeConfigurationrepository.DeleteAsync(result);
        //        CurrentUnitOfWork.SaveChanges();
        //    }

        //}


    }
}
