﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Configuration;
using CloudBaseLine.Entities.Configurations.HRConfigurations.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Configurations.HRConfigurations
{
    public class HRConfigurationAppService : AsyncCrudAppService<HRConfiguration, HRConfigurationDto, long, PagedResultRequestDto, HRConfigurationDto, HRConfigurationDto>, IHRConfigurationAppService
    {
        private readonly IRepository<HRConfiguration, long> _HRConfigurationrepository;
        private readonly IPermissionManager _permissionManager;

        public HRConfigurationAppService(IRepository<HRConfiguration, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _HRConfigurationrepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<HRConfigurationDto>> GetAllHRConfiguration()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                // var result = _HRConfigurationrepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == AbpSession.TenantId).ToList();
                var result = _HRConfigurationrepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<HRConfigurationDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //var result = _UserCompanyRightrepository.GetAllIncluding(p=>p.User);
        }

        public override async Task<HRConfigurationDto> Create(HRConfigurationDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = ObjectMapper.Map<HRConfiguration>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            result.TenantId = Convert.ToInt32(TenantId);
            await _HRConfigurationrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();
            var data = result.MapTo<HRConfigurationDto>();
            return data;

        }

        public override async Task<HRConfigurationDto> Update(HRConfigurationDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var data = ObjectMapper.Map<HRConfiguration>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);


            data.TenantId = Convert.ToInt32(TenantId);

            await _HRConfigurationrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<HRConfigurationDto>();
            return result;
        }



    }
}
