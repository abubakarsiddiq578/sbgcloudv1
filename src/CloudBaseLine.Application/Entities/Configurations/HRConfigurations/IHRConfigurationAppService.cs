﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Configurations.HRConfigurations.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Configurations.HRConfigurations
{
    public interface IHRConfigurationAppService : IAsyncCrudAppService<HRConfigurationDto, long, PagedResultRequestDto, HRConfigurationDto, HRConfigurationDto>
    {
    }
}
