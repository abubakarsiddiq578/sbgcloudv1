﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Configurations.HRConfigurations.Dto
{
    [AutoMapTo(typeof(HRConfiguration)), AutoMapFrom(typeof(HRConfigurationDto))]
    public class HRConfigurationDto : EntityDto<long>
    {
        public bool AutoCode { get; set; }
        public int TenantId { get; set; }
    }
}
