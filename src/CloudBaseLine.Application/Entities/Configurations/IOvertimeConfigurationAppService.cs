﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Configurations.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Configurations
{
    public interface IOvertimeConfigurationAppService : IAsyncCrudAppService<OvertimeConfigurationDto, long, PagedResultRequestDto, OvertimeConfigurationDto, OvertimeConfigurationDto>
    {
    }
}
