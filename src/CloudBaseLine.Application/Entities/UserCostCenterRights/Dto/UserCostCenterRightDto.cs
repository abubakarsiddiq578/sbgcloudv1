﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Authorization.Users;

namespace CloudBaseLine.Entities.UserCostCenterRights.Dto
{
    [AutoMapTo(typeof(UserCostCenterRight)), AutoMapFrom(typeof(UserCostCenterRightDto))]
    public class UserCostCenterRightDto : EntityDto<long>
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public long CostCenterId { get; set; }
        public CostCenter CostCenter { get; set; }
        public bool Rights { get; set; }
        public int TenantId { get; set; }
    }
}
