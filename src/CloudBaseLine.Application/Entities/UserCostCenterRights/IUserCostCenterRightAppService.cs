﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.UserCostCenterRights.Dto;

namespace CloudBaseLine.Entities.UserCostCenterRights
{
    public interface IUserCostCenterRightAppService : IAsyncCrudAppService<UserCostCenterRightDto, long, PagedResultRequestDto, UserCostCenterRightDto, UserCostCenterRightDto>
    {
        Task<List<UserCostCenterRightDto>> GetCostCenterRights();
    }
}
