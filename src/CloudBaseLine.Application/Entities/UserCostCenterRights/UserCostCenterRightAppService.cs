﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.CostCenters.Dto;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.UserCostCenterRights.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.UserCostCenterRights
{
    public class UserCostCenterRightAppService : AsyncCrudAppService<UserCostCenterRight, UserCostCenterRightDto, long, PagedResultRequestDto, UserCostCenterRightDto, UserCostCenterRightDto>, IUserCostCenterRightAppService
    {
        private readonly IRepository<CostCenter, long> _CostCenterRepository;
        private readonly IRepository<UserCostCenterRight, long> _UserCostCenterRightrepository;
        private readonly IPermissionManager _permissionManager;

        public UserCostCenterRightAppService(IRepository<UserCostCenterRight, long> _repository, IPermissionManager _Manager , IRepository<CostCenter, long> _costCenterRepository) : base(_repository)
        {
            _UserCostCenterRightrepository = _repository;
            _permissionManager = _Manager;
            _CostCenterRepository = _costCenterRepository;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserCostCenterRights_Create)]
        public override async Task<UserCostCenterRightDto> Create(UserCostCenterRightDto input)
        {
            var result = ObjectMapper.Map<UserCostCenterRight>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _UserCostCenterRightrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserCostCenterRightDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserCostCenterRights_Edit)]
        public override async Task<UserCostCenterRightDto> Update(UserCostCenterRightDto input)
        {
            var data = ObjectMapper.Map<UserCostCenterRight>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _UserCostCenterRightrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserCostCenterRightDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserCostCenterRights_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _UserCostCenterRightrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _UserCostCenterRightrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<UserCostCenterRightDto>> GetCostCenterRights()
        {
            var result = _UserCostCenterRightrepository.GetAll().Include(a => a.CostCenter).Include(b => b.User).ToList();
            return result.MapTo<List<UserCostCenterRightDto>>();
        }

        public async Task<List<CostCenterDto>> getNewCostCenters(long userId)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var costcenter = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
            var costcenterrights = _UserCostCenterRightrepository.GetAll().Where(a => a.UserId == userId).Select(a => a.CostCenterId).ToList();

            costcenter = costcenter.Where(a => !costcenterrights.Contains(a.Id) && a.IsDeleted == false && a.TenantId == TenantId).ToList();

            return costcenter.MapTo<List<CostCenterDto>>();
        }
    }
}
