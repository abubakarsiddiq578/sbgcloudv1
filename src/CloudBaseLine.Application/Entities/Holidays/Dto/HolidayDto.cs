﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Holidays.Dto
{
    [AutoMapTo(typeof(Holiday)), AutoMapFrom(typeof(HolidayDto))]
    public class HolidayDto:EntityDto<long>
    {

        
        public long CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

       
        public long DepartmentId { get; set; }
        public Department Department { get; set; }


        
        public long EmployeeDesignationId { get; set; }
        public EmployeeDesignation EmployeeDesignation { get; set; }

        
        public long LeaveTypeId { get; set; }
        public LeaveType LeaveType { get; set; }


        public DateTime holidayStartDate { get; set; }

        public DateTime holidayEndDate { get; set; }

        public string HolidayDetail { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }
    }
}
