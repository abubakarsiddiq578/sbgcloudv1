﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Holidays.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Holidays
{
    public class HolidayAppService : AsyncCrudAppService<Holiday,HolidayDto, long, PagedResultRequestDto, HolidayDto, HolidayDto>, IHolidayAppService
    {
        private readonly IRepository<Holiday, long> HolidayRepository;
        private readonly IPermissionManager _permissionManager;


        public HolidayAppService(IRepository<Holiday, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            HolidayRepository = _repository;
            _permissionManager = _Manager;
        }

        //to get all Holidays
        public async Task<List<HolidayDto>> GetAllHolidays()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                //get all active user
                var result = HolidayRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.CostCenter).Include(d => d.Department).Include(b=>b.EmployeeDesignation).Include(l=>l.LeaveType).ToList();

                return result.MapTo<List<HolidayDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Holidays_Create)]
        //to create a Holiday
        public override async Task<HolidayDto> Create(HolidayDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<Holiday>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await HolidayRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<HolidayDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Holidays_Edit)]
        //to update a Holiday
        public override async Task<HolidayDto> Update(HolidayDto input)
        {
            var data = ObjectMapper.Map<Holiday>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await HolidayRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<HolidayDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Holidays_Delete)]
        //to delete a Holiday
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = HolidayRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await HolidayRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


    }
}
