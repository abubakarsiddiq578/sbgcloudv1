﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Holidays.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Holidays
{
    public interface IHolidayAppService : IAsyncCrudAppService<HolidayDto, long, PagedResultRequestDto, HolidayDto, HolidayDto>
    {
       
    }
}
