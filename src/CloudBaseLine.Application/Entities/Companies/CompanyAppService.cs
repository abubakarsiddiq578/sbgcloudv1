﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
//using CloudBaseLine.Entities.Companies.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CloudBaseLine.Entities.Companies
{
    public class CompanyAppService : AsyncCrudAppService<Company, CompanyDto, long, PagedResultRequestDto, CompanyDto, CompanyDto>, ICompanyAppService
    {
        private static IRepository<Company, long> CompanyRepository;
        
           private readonly IPermissionManager _permissionManager;

        public CompanyAppService(IRepository<Company, long> companyRepository, IPermissionManager permissionManager) : base(CompanyRepository)
        {
            CompanyRepository = companyRepository;
            _permissionManager = permissionManager;
        }


       
        public async Task<List<CompanyDto>> GetAllCompanies()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                //will get all the record based on logged in user's tenant id.
                var result = CompanyRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<CompanyDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Companies_Create)]
        public override async Task<CompanyDto> Create(CompanyDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = ObjectMapper.Map<Company>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await CompanyRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<CompanyDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Companies_Edit)]
        public override async Task<CompanyDto> Update(CompanyDto input)
        {

            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }
            var data = ObjectMapper.Map<Company>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);
            data.TenantId = Convert.ToInt32(TenantId);

            await CompanyRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CompanyDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Companies_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {

            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = CompanyRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await CompanyRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


    }
}
