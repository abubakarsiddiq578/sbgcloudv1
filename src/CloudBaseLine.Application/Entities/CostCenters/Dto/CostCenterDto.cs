﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.CostCenters.Dto
{
    [AutoMapTo(typeof(CostCenter)), AutoMapFrom(typeof(CostCenterDto))]
    public class CostCenterDto: EntityDto<long>
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int SortOrder { get; set; }
        public String CostCenterGroup { get; set; }
        public Boolean OutwardGatepass { get; set; }
        public Boolean DayShift { get; set; }
        public int LCDocId { get; set; }
        public Boolean IsLogical { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }

    }
}
