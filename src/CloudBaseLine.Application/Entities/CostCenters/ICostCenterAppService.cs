﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.CostCenters.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.CostCenters
{
    public interface ICostCenterAppService : IAsyncCrudAppService<CostCenterDto, long, PagedResultRequestDto, CostCenterDto, CostCenterDto>
    {
    }
}
