﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.CostCenters.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.CostCenters
{
    public class CostCenterAppService: AsyncCrudAppService<CostCenter, CostCenterDto, long, PagedResultRequestDto, CostCenterDto, CostCenterDto>, ICostCenterAppService
    {
        private readonly IRepository<CostCenter, long> _CostCenterRepository;
        private readonly IPermissionManager _permissionManager;

        public CostCenterAppService(IRepository<CostCenter, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _CostCenterRepository = _repository;
            _permissionManager = _Manager;
        }

 
        public async Task<List<CostCenterDto>> GetAllCostCenters()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<CostCenterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        //CCG = Cost Center Group
      
        public async Task<List<string>> GetAllCCG()
        {
            try
            {

                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _CostCenterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId ).Select(a => a.CostCenterGroup).Distinct().ToList();

                return result.MapTo<List<string>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_CostCenters_Create)]
        public override async Task<CostCenterDto> Create(CostCenterDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<CostCenter>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await _CostCenterRepository.InsertAsync(result);

            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch(Exception ex)
            {

            }
            

            var data = result.MapTo<CostCenterDto>();
            return data;

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_CostCenters_Edit)]
        public override async Task<CostCenterDto> Update(CostCenterDto input)
        {
            var data = ObjectMapper.Map<CostCenter>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _CostCenterRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CostCenterDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_CostCenters_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {

            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _CostCenterRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _CostCenterRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
