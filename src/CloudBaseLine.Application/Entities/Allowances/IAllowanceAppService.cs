﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Allowances.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Allowances
{
    public interface IAllowanceAppService : IAsyncCrudAppService<AllowanceDto, long, PagedResultRequestDto, AllowanceDto, AllowanceDto>
    {
    }
}
