﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace CloudBaseLine.Entities.Allowances.Dto
{
    [AutoMapTo(typeof(Allowance)), AutoMapFrom(typeof(AllowanceDto))]
    public class AllowanceDto : EntityDto<long>
    {
        public string DocNo { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime AllowanceDate { get; set; }
        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public long AllowanceTypeId { get; set; }
        public AllowanceType AllowanceType { get; set; }
        public double Amount { get; set; }
        public string Detail { get; set; }
        public int TenantId { get; set; }
    }
}
