﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Allowances.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Allowances
{
    public class AllowanceAppService : AsyncCrudAppService<Allowance, AllowanceDto, long, PagedResultRequestDto, AllowanceDto, AllowanceDto>, IAllowanceAppService
    {
        private readonly IRepository<Allowance, long> _AllowanceRepository;
        private readonly IPermissionManager _permissionManager;

        public AllowanceAppService(IRepository<Allowance, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _AllowanceRepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        //[AbpAuthorize(PermissionNames.Pages_Hrm_Allowances_View)]
        public async Task<List<AllowanceDto>> GetAllAllowances()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _AllowanceRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.Employee).Include(b => b.AllowanceType).ToList();
                return result.MapTo<List<AllowanceDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Allowances_Create)]
        public override async Task<AllowanceDto> Create(AllowanceDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<Allowance>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _AllowanceRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<AllowanceDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Allowances_Edit)]
        public override async Task<AllowanceDto> Update(AllowanceDto input)
        {
            var data = ObjectMapper.Map<Allowance>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _AllowanceRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<AllowanceDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Allowances_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _AllowanceRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _AllowanceRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
