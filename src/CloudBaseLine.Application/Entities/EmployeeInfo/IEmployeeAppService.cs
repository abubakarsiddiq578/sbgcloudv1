﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.EmployeeInfo.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeInfo
{
    public interface IEmployeeAppService : IAsyncCrudAppService<EmployeeDto, long, PagedResultRequestDto, EmployeeDto, EmployeeDto>
    {
    }
}
