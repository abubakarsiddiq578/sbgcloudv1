using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.EmployeeInfo.Dto;
using CloudBaseLine.Entities.Salary;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.EmployeeInfo
{
    public class EmployeeAppService : AsyncCrudAppService<Employee, EmployeeDto, long, PagedResultRequestDto, EmployeeDto, EmployeeDto>, IEmployeeAppService
    {
        private readonly IRepository<Employee, long> _EmployeeRepository;
        private readonly IPermissionManager _permissionManager;
        private readonly IRepository<SalaryMaster, long> _SalaryRepository;

        public EmployeeAppService(IRepository<Employee, long> _repository, IRepository<SalaryMaster, long> _salaryRepository, IPermissionManager _Manager) : base(_repository)
        {
            _EmployeeRepository = _repository;
            _permissionManager = _Manager;
            _SalaryRepository = _salaryRepository;
        }

        /// <summary>
        /// GetAllEmployees will get all the employees
        /// </summary>
        /// <returns></returns>
        /// 
        //

        public async Task<Array> GetEmployeesWithSalary(long? departmentId, long? desgnationId)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                // var result =  _EmployeeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(b=>b.Overtimes).ToArray();
                var result = _SalaryRepository.GetAll()
                    .Where(a => a.IsDeleted == false && a.TenantId == TenantId)
                    .Include(a => a.Employee)
                    .Include(b => b.Employee.EmployeeDesignation)
                    .Include(c => c.Employee.Department)
                    .Include(d => d.Employee.Overtimes)
                    .ToList();
                if (desgnationId > 0)
                {
                    result = result.Where(a => a.Employee.Desig_ID == desgnationId).ToList();
                }

                if (departmentId > 0)
                {
                    result = result.Where(a => a.Employee.Dept_ID == departmentId).ToList();
                }


                return result.ToArray(); //result.MapTo<List<EmployeeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<EmployeeDto>> GetAllEmployees()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _EmployeeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId)
                    .Include(j => j.JobType)
                    .Include(c => c.CostCenter)
                    .Include(d => d.Department)
                    .Include(e => e.EmployeeDesignation)
                    .Include(c => c.City)
                    .Include(z => z.Zone)
                    .Include(b => b.Belt)
                    .Include(s => s.State)
                    .Include(r => r.Region)
                    .ToList();
                return result.MapTo<List<EmployeeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<List<EmployeeDto>> GetAllEmployeesByDepartment(long Id)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _EmployeeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.Dept_ID == Id)
                    .Include(j => j.JobType)
                    .Include(c => c.CostCenter)
                    .Include(d => d.Department)
                    .Include(e => e.EmployeeDesignation)
                    .Include(c => c.City)
                    .Include(z => z.Zone)
                    .Include(b => b.Belt)
                    .Include(s => s.State)
                    .Include(r => r.Region)
                    .ToList();
                return result.MapTo<List<EmployeeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<EmployeeDto>> GetAllEmployeesByDesignation(long Id)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _EmployeeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.Desig_ID == Id)
                    .Include(j => j.JobType)
                    .Include(c => c.CostCenter)
                    .Include(d => d.Department)
                    .Include(e => e.EmployeeDesignation)
                    .Include(c => c.City)
                    .Include(z => z.Zone)
                    .Include(b => b.Belt)
                    .Include(s => s.State)
                    .Include(r => r.Region)
                    .ToList();
                return result.MapTo<List<EmployeeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<EmployeeDto>> GetAllEmployeesById(long Id)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _EmployeeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId && a.Id == Id)
                    .Include(j => j.JobType)
                    .Include(c => c.CostCenter)
                    .Include(d => d.Department)
                    .Include(e => e.EmployeeDesignation)
                    .Include(c => c.City)
                    .Include(z => z.Zone)
                    .Include(b => b.Belt)
                    .Include(s => s.State)
                    .Include(r => r.Region)
                    .ToList();
                return result.MapTo<List<EmployeeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// This Create function will add a new employee.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeInfos_Create)]
        public override async Task<EmployeeDto> Create(EmployeeDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<Employee>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _EmployeeRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<EmployeeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This update function will update the specific record according to given id.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeInfos_Edit)]
        public override async Task<EmployeeDto> Update(EmployeeDto input)
        {
            try
            {
                var data = ObjectMapper.Map<Employee>(input);
                data.LastModificationTime = DateTime.Now;
                data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

                await _EmployeeRepository.UpdateAsync(data);
                CurrentUnitOfWork.SaveChanges();

                var result = data.MapTo<EmployeeDto>();
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        /// <summary>
        /// This Delete function will delete the specific record according to given number id.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeInfos_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _EmployeeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
                if (result != null)
                {
                    await _EmployeeRepository.DeleteAsync(result);
                    CurrentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
