﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Jobtypes.Dto
{
    [AutoMapTo(typeof(JobType)), AutoMapFrom(typeof(JobTypeDto))]
    public class JobTypeDto : EntityDto<long>
    {
        public string JobTypeTitle { get; set; }

        public string Code { get; set; }
        public string Comments { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public int TenantId { get; set; }
    }
}
