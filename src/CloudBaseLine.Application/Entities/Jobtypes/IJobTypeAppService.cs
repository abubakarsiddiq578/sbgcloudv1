﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Jobtypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Jobtypes
{
    public interface IJobTypeAppService : IAsyncCrudAppService<JobTypeDto, long, PagedResultRequestDto, JobTypeDto, JobTypeDto>
    {
    }
}
