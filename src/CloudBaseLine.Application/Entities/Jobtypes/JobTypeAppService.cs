﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Jobtypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Jobtypes
{
    public class JobTypeAppService : AsyncCrudAppService<JobType, JobTypeDto, long, PagedResultRequestDto, JobTypeDto, JobTypeDto>, IJobTypeAppService
    {
        private readonly IRepository<JobType, long> _JobTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public JobTypeAppService(IRepository<JobType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _JobTypeRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<JobTypeDto>> GetAllJobTypes()
        {
            try
            {//get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _JobTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<JobTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_JobTypes_Create)]
        public override async Task<JobTypeDto> Create(JobTypeDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<JobType>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _JobTypeRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<JobTypeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_JobTypes_Edit)]
        public override async Task<JobTypeDto> Update(JobTypeDto input)
        {
            var data = ObjectMapper.Map<JobType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _JobTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<JobTypeDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_JobTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _JobTypeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _JobTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
