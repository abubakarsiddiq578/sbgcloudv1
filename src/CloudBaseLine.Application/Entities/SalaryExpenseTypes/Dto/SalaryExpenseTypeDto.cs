﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SalaryExpenseTypes.Dto
{
    [AutoMapTo(typeof(SalaryExpenseType)), AutoMapFrom(typeof(SalaryExpenseTypeDto))]
    public class SalaryExpenseTypeDto : EntityDto<long>
    {
        public string SalaryExpType { get; set; }
        public string ApplyValue { get; set; }
        public int Value { get; set; }
        public Boolean SalaryDeduction { get; set; }
        public Boolean IncomeTaxExempted { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public int TenantId { get; set; }
    }
}
