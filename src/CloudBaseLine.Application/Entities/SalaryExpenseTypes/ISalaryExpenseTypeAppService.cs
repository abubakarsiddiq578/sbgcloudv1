﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.SalaryExpenseTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.SalaryExpenseTypes
{
    public interface ISalaryExpenseTypeAppService : IAsyncCrudAppService<SalaryExpenseTypeDto, long, PagedResultRequestDto, SalaryExpenseTypeDto, SalaryExpenseTypeDto>
    {
        Task<List<SalaryExpenseTypeDto>> GetAllSalaryExpenseType();
    }
}
