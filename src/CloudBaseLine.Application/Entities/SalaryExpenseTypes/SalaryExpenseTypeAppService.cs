﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.SalaryExpenseTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.SalaryExpenseTypes
{
    public class SalaryExpenseTypeAppService : AsyncCrudAppService<SalaryExpenseType, SalaryExpenseTypeDto, long, PagedResultRequestDto, SalaryExpenseTypeDto, SalaryExpenseTypeDto>, ISalaryExpenseTypeAppService
    {
        private readonly IRepository<SalaryExpenseType, long> SalaryExpenseTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public SalaryExpenseTypeAppService(IRepository<SalaryExpenseType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            SalaryExpenseTypeRepository = _repository;
            _permissionManager = _Manager;
        }

        public async Task<List<SalaryExpenseTypeDto>> GetAllSalaryExpenseType()
        {
            try
            {
                var result = SalaryExpenseTypeRepository.GetAll().Where(a => a.IsDeleted == false).ToList();
                return result.MapTo<List<SalaryExpenseTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryExpenseTypes_Create)]
        public override async Task<SalaryExpenseTypeDto> Create(SalaryExpenseTypeDto input)
        {
            var result = ObjectMapper.Map<SalaryExpenseType>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await SalaryExpenseTypeRepository.InsertAsync(result);

            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<SalaryExpenseTypeDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryExpenseTypes_Edit)]
        public override async Task<SalaryExpenseTypeDto> Update(SalaryExpenseTypeDto input)
        {
            var data = ObjectMapper.Map<SalaryExpenseType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await SalaryExpenseTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<SalaryExpenseTypeDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SalaryExpenseTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = SalaryExpenseTypeRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await SalaryExpenseTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
