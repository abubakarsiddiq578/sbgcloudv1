﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.AdditionalLeaves.Dto
{
    [AutoMapTo(typeof(AdditionalLeave)), AutoMapFrom(typeof(AdditionalLeaveDto))]
    public class AdditionalLeaveDto : EntityDto<long>
    {
        public long EmployeeId { get; set; }

        public Employee Employee{ get; set; }
        public long LeaveTypeId { get; set; }
        public LeaveType LeaveType { get; set; }

        public int AdditionalLeaves { get; set; }

        public string Detail { get; set; }

        public int TenantId { get; set; }
    }
}
