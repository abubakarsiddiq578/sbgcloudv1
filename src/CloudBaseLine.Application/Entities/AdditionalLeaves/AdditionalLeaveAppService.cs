﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.AdditionalLeaves.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.AdditionalLeaves
{
    public class AdditionalLeaveAppService : AsyncCrudAppService<AdditionalLeave, AdditionalLeaveDto, long, PagedResultRequestDto, AdditionalLeaveDto, AdditionalLeaveDto>, IAdditionalLeaveAppService
    {
        private readonly IRepository<AdditionalLeave, long> _AdditionalLeaveRepository;
        private readonly IPermissionManager _permissionManager;

        public AdditionalLeaveAppService(IRepository<AdditionalLeave, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _AdditionalLeaveRepository = _repository;
            _permissionManager = _Manager;
        }

      
        public async Task<List<AdditionalLeaveDto>> GetAllAdditionalLeaves()
        {
            try
            {

                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _AdditionalLeaveRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId ).Include(e => e.Employee).Include(l => l.LeaveType).ToList();
                return result.MapTo<List<AdditionalLeaveDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AdditionalLeaves_Create)]
        public override async Task<AdditionalLeaveDto> Create(AdditionalLeaveDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<AdditionalLeave>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _AdditionalLeaveRepository.InsertAsync(result);
                

                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<AdditionalLeaveDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AdditionalLeaves_Edit)]
        public override async Task<AdditionalLeaveDto> Update(AdditionalLeaveDto input)
        {
            var data = ObjectMapper.Map<AdditionalLeave>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _AdditionalLeaveRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<AdditionalLeaveDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AdditionalLeaves_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {

            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = _AdditionalLeaveRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _AdditionalLeaveRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
