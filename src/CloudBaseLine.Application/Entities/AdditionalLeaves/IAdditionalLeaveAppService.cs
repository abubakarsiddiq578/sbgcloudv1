﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.AdditionalLeaves.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.AdditionalLeaves
{
    public interface IAdditionalLeaveAppService : IAsyncCrudAppService<AdditionalLeaveDto, long, PagedResultRequestDto, AdditionalLeaveDto, AdditionalLeaveDto>
    {
    }
}
