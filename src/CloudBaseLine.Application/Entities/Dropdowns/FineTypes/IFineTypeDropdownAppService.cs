﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Dropdowns.FineTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.FineTypes
{
    public interface IFineTypeDropdownAppService : IAsyncCrudAppService<FineTypeDropdownDto, long, PagedResultRequestDto, FineTypeDropdownDto, FineTypeDropdownDto>
    {
        Task<ListResultDto<FineTypeDropdownDto>> GetFineTypeDropdown();
    }
}
