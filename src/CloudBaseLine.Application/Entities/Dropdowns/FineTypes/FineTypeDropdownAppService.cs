﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Dropdowns.CostCenters.Dto;
using CloudBaseLine.Entities.Dropdowns.FineTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.FineTypes
{
    public class FineTypeDropdownAppService : AsyncCrudAppService<FineType, FineTypeDropdownDto, long, PagedResultRequestDto, FineTypeDropdownDto, FineTypeDropdownDto>, IFineTypeDropdownAppService
    {
        private readonly IRepository<FineType, long> _fineTypeDropDownRepository;
        private readonly IPermissionManager _permissionManager;

        public FineTypeDropdownAppService(IRepository<FineType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _fineTypeDropDownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<FineTypeDropdownDto>> GetFineTypeDropdown()
        {
            var empDropDown = await _fineTypeDropDownRepository.GetAllListAsync();
            return new ListResultDto<FineTypeDropdownDto>(ObjectMapper.Map<List<FineTypeDropdownDto>>(empDropDown));
        }
    }
}
