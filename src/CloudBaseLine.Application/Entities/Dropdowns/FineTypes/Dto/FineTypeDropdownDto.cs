﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.FineTypes.Dto
{
    [AutoMapTo(typeof(FineType)), AutoMapFrom(typeof(FineTypeDropdownDto))]
    public class FineTypeDropdownDto : EntityDto<long>
    {
        public string FintTitle { get; set; }

        public int TenantId { get; set; }
    }
}
