﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.CostCenters.Dto
{
    [AutoMapTo(typeof(CostCenter)), AutoMapFrom(typeof(CostCenterDropdownDto))]
    public class CostCenterDropdownDto : EntityDto<long>
    {
        public string Name { get; set; }
        public int TenantId { get; set; }
    }
}
