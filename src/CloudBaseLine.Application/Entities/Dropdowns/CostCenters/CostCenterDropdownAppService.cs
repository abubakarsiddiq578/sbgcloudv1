﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.CompanyInformation.Dto;
using CloudBaseLine.Entities.Dropdowns.CostCenters.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.CostCenters
{
    public class CostCenterDropdownAppService : AsyncCrudAppService<CostCenter, CostCenterDropdownDto, long, PagedResultRequestDto, CostCenterDropdownDto, CostCenterDropdownDto>, ICostCenterDropdownAppService
    {

        private readonly IRepository<CostCenter, long> _CostCenterDropdownRepository;
        private readonly IPermissionManager _permissionManager;
        public CostCenterDropdownAppService(IRepository<CostCenter, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _CostCenterDropdownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<CostCenterDropdownDto>> GetCostCentersDropdown()
        {
            var costCenters = await _CostCenterDropdownRepository.GetAllListAsync();
            //var result = costCenters.Where(a => a.IsActive.ToString() == "True");
            return new ListResultDto<CostCenterDropdownDto>(ObjectMapper.Map<List<CostCenterDropdownDto>>(costCenters));
        }

    }
}
