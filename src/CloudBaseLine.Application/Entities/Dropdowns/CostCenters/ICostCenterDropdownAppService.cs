﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Dropdowns.CostCenters.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.CostCenters
{
    public interface ICostCenterDropdownAppService: IAsyncCrudAppService<CostCenterDropdownDto, long, PagedResultRequestDto, CostCenterDropdownDto, CostCenterDropdownDto>
    {
    }
}
