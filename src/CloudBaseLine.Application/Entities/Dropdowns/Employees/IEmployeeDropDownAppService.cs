﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Dropdowns.Employees.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.Employees
{
    public interface IEmployeeDropDownAppService : IAsyncCrudAppService<EmployeeDropdownDto, long, PagedResultRequestDto, EmployeeDropdownDto, EmployeeDropdownDto>
    {
    }
}
