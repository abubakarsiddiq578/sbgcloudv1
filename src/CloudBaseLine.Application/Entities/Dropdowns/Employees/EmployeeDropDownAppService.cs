﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Dropdowns.CostCenters.Dto;
using CloudBaseLine.Entities.Dropdowns.Employees.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.Employees
{
    public class EmployeeDropDownAppService : AsyncCrudAppService<Employee, EmployeeDropdownDto, long, PagedResultRequestDto, EmployeeDropdownDto, EmployeeDropdownDto>, IEmployeeDropDownAppService
    {

        private readonly IRepository<Employee, long> _employeeDropDownRepository;
        private readonly IPermissionManager _permissionManager;
        public EmployeeDropDownAppService(IRepository<Employee, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _employeeDropDownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<EmployeeDropdownDto>> GetEmployeeDropdown()
        {
            var empDropDown = await _employeeDropDownRepository.GetAllListAsync();
            return new ListResultDto<EmployeeDropdownDto>(ObjectMapper.Map<List<EmployeeDropdownDto>>(empDropDown));
        }

    }
}
