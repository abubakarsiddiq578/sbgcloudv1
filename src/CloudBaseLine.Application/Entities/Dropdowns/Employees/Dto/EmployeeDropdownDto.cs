﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.Employees.Dto
{
    [AutoMapTo(typeof(Employee)), AutoMapFrom(typeof(EmployeeDropdownDto))]
    public class EmployeeDropdownDto : EntityDto<long>
    {
        public string Employee_Name { get; set; }

        public int TenantId { get; set; }
    }
}
