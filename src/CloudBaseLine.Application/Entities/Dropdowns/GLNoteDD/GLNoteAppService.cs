﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Dropdowns.CompanyInformation.Dto;
using CloudBaseLine.Entities.Dropdowns.GLNoteDD.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.GLNoteDD
{
    public class GLNoteAppService : AsyncCrudAppService<GLNotes, GLNotesDropdownDto, long, PagedResultRequestDto, GLNotesDropdownDto, GLNotesDropdownDto>, IGLNoteAppService
    {

        private readonly IRepository<GLNotes, long> _GLDropDownRepository;
        private readonly IPermissionManager _permissionManager;
        public GLNoteAppService(IRepository<GLNotes, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _GLDropDownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<GLNotesDropdownDto>> GetBSGLDropdown()
        {
            //var glDropDown = await _GLDropDownRepository.GetAllListAsync();
            var bsDropDown = _GLDropDownRepository.GetAll().Where(a => a.Note_Type == "BS").ToList();
            return new ListResultDto<GLNotesDropdownDto>(ObjectMapper.Map<List<GLNotesDropdownDto>>(bsDropDown));
        }


        public async Task<ListResultDto<GLNotesDropdownDto>> GetPLGLDropdown()
        {
            //var glDropDown = await _GLDropDownRepository.GetAllListAsync();
            var plDropDown = _GLDropDownRepository.GetAll().Where(a => a.Note_Type == "PL").ToList();
            return new ListResultDto<GLNotesDropdownDto>(ObjectMapper.Map<List<GLNotesDropdownDto>>(plDropDown));
        }

    }
}
