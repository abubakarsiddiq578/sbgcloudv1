﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Dropdowns.GLNoteDD.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.GLNoteDD
{
    public interface IGLNoteAppService : IAsyncCrudAppService<GLNotesDropdownDto, long, PagedResultRequestDto, GLNotesDropdownDto, GLNotesDropdownDto>
    {
    }
}
