﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.GLNoteDD.Dto
{
    [AutoMapTo(typeof(GLNotes)), AutoMapFrom(typeof(GLNotesDropdownDto))]
    public class GLNotesDropdownDto : EntityDto<long>
    {
        public string Note_Title { get; set; }

        public string Note_Type { get; set; }

        public int Note_No { get; set; }

        public int SortOrder { get; set; }
    }
}
