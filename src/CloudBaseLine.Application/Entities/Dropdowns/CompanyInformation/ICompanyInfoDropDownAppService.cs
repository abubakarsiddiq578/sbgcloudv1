﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Dropdowns.CompanyInformation.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.CompanyInformation
{
    public interface ICompanyInfoDropDownAppService : IAsyncCrudAppService<CompanyInfoDropDownDto, long, PagedResultRequestDto, CompanyInfoDropDownDto, CompanyInfoDropDownDto>
    {
    }
}
