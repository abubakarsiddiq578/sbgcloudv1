﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Dropdowns.CompanyInformation.Dto;
using CloudBaseLine.Entities.Dropdowns.Employees.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.CompanyInformation
{
    public class CompanyInfoDropDownAppService : AsyncCrudAppService<CompanyInfo, CompanyInfoDropDownDto, long, PagedResultRequestDto, CompanyInfoDropDownDto, CompanyInfoDropDownDto>, ICompanyInfoDropDownAppService
    {

        private readonly IRepository<CompanyInfo, long> _companyInfoDropDownRepository;
        private readonly IPermissionManager _permissionManager;
        public CompanyInfoDropDownAppService(IRepository<CompanyInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _companyInfoDropDownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<CompanyInfoDropDownDto>> GetCompanyInfoDropdown()
        {
            var compDropDown = await _companyInfoDropDownRepository.GetAllListAsync();
            return new ListResultDto<CompanyInfoDropDownDto>(ObjectMapper.Map<List<CompanyInfoDropDownDto>>(compDropDown));
        }

    }
}
