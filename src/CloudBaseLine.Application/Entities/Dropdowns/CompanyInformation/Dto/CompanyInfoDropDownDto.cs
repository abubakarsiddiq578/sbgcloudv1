﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.CompanyInformation.Dto
{
    [AutoMapTo(typeof(CompanyInfo)), AutoMapFrom(typeof(CompanyInfoDropDownDto))]
    public class CompanyInfoDropDownDto : EntityDto<long>
    {
        public string CompanyName { get; set; }
    }
}
