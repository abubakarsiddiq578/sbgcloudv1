﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Dropdowns.Employees.Dto;
using CloudBaseLine.Entities.Dropdowns.Location.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.Location
{
    public class LocationInfoDropdownAppService : AsyncCrudAppService<LocationInfo, LocationInfoDropDownDto, long, PagedResultRequestDto, LocationInfoDropDownDto, LocationInfoDropDownDto>, ILocationInfoDropDownAppService
    {

        private readonly IRepository<LocationInfo, long> _locationDropDownRepository;
        private readonly IPermissionManager _permissionManager;
        public LocationInfoDropdownAppService(IRepository<LocationInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _locationDropDownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<LocationInfoDropDownDto>> GetLocationDropdown()
        {
            var locatoinDropDown = await _locationDropDownRepository.GetAllListAsync();
            return new ListResultDto<LocationInfoDropDownDto>(ObjectMapper.Map<List<LocationInfoDropDownDto>>(locatoinDropDown));
        }

    }
}
