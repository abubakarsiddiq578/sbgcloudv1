﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Dropdowns.Location.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.Location
{
    public interface ILocationInfoDropDownAppService : IAsyncCrudAppService<LocationInfoDropDownDto, long, PagedResultRequestDto, LocationInfoDropDownDto, LocationInfoDropDownDto>
    {
    }
}
