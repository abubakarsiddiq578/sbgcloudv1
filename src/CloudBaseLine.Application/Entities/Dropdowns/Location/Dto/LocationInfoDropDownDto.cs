﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.Location.Dto
{
    [AutoMapTo(typeof(LocationInfo)), AutoMapFrom(typeof(LocationInfoDropDownDto))]
    public class LocationInfoDropDownDto : EntityDto<long>
    {
        public string Location_Name { get; set; }
        public int TenantId { get; set; }
    }
}
