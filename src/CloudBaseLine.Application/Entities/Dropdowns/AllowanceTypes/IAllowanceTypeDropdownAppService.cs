﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Dropdowns.AllowanceTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.AllowanceTypes
{
    public interface IAllowanceTypeDropdownAppService : IAsyncCrudAppService<AllowanceTypeDropdownDto, long, PagedResultRequestDto, AllowanceTypeDropdownDto, AllowanceTypeDropdownDto>
    {
        Task<ListResultDto<AllowanceTypeDropdownDto>> GetAllowanceTypeDropdown();
    }
}
