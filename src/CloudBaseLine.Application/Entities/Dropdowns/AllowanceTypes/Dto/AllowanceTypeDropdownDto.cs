﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Dropdowns.AllowanceTypes.Dto
{
    [AutoMapTo(typeof(AllowanceType)), AutoMapFrom(typeof(AllowanceTypeDropdownDto))]
    public class AllowanceTypeDropdownDto : EntityDto<long>
    {
        public string AllowanceTitle { get; set; }
        public int TenantId { get; set; }
    }
}
