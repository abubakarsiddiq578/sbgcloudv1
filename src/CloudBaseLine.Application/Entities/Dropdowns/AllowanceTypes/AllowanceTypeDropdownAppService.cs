﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.Dropdowns.CostCenters.Dto;
using CloudBaseLine.Entities.Dropdowns.AllowanceTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Dropdowns.AllowanceTypes
{
    public class AllowanceTypeDropdownAppService : AsyncCrudAppService<AllowanceType, AllowanceTypeDropdownDto, long, PagedResultRequestDto, AllowanceTypeDropdownDto, AllowanceTypeDropdownDto>, IAllowanceTypeDropdownAppService 
    {
        private readonly IRepository<AllowanceType, long> _allowanceTypeDropDownRepository;
        private readonly IPermissionManager _permissionManager;
        public AllowanceTypeDropdownAppService(IRepository<AllowanceType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _allowanceTypeDropDownRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<ListResultDto<AllowanceTypeDropdownDto>> GetAllowanceTypeDropdown()
        {
            var empDropDown = await _allowanceTypeDropDownRepository.GetAllListAsync();
            return new ListResultDto<AllowanceTypeDropdownDto>(ObjectMapper.Map<List<AllowanceTypeDropdownDto>>(empDropDown));
        }
    }
}
