﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Cities.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Cities
{
    public class CityAppService : AsyncCrudAppService<City,CityDto, long, PagedResultRequestDto, CityDto, CityDto>,ICityAppService
    {

        private readonly IRepository<City, long> CityRepository;
        private readonly IPermissionManager _permissionManager;

        public CityAppService(IRepository<City, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            CityRepository = _repository;

            _permissionManager = _Manager;

        }


  
        public async Task<List<CityDto>> GetAllCities()
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var a = CityRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId).Include(t =>t.Province).ToList();

                return a.MapTo<List<CityDto>>();
            }

            catch (Exception ex)
            {
                throw ex;
            }


        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Cities_Create)]

        public override async Task<CityDto> Create(CityDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<City>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await CityRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<CityDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Cities_Edit)]
        public override async Task<CityDto> Update(CityDto input)
        {
            var data = ObjectMapper.Map<City>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await CityRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CityDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Cities_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = CityRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId ).FirstOrDefault();
            if (result != null)
            {
                await CityRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }



        public async Task<List<CityDto>> GetCityWithProvinceId(long id)
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = CityRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId && b.ProvinceId == id).Include(t => t.Province).ToList();
                return a.MapTo<List<CityDto>>();
            }
            catch (Exception ex)
            {
                throw ex;

            }


        }


    }
}
