﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Cities.Dto
{
    [AutoMapTo(typeof(City)), AutoMapFrom(typeof(CityDto))]
    public class CityDto : EntityDto<long>
    {

        public long ProvinceId { get; set; }

        public Province Province { get; set; }

        public string CityName { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }

        public bool isActive { get; set; }

        public int TenantId { get; set; }
    }
}
