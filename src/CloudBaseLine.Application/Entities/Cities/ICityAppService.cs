﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Cities.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Cities
{
    public interface ICityAppService : IAsyncCrudAppService<CityDto, long, PagedResultRequestDto, CityDto, CityDto>
    {

    }
}
