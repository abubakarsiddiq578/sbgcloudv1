﻿

using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.EmployeeGraduity;
using CloudBaseLine.Entities.EmployeeGraduity.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.EmployeeGraduity
{
    public interface IGraduityMasterAppService : IAsyncCrudAppService<GraduityMasterDto, long, PagedResultRequestDto, GraduityMasterDto, GraduityMasterDto>
    {
        Task<List<GraduityMasterDto>> GetAllRecord();
       
        Task<List<GraduityDetailDto>> GetAllDetailById(long id);
    }
}
