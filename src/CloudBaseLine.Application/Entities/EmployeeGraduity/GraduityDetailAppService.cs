﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.EmployeeGraduity;
using CloudBaseLine.Entities.EmployeeGraduity.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeGraduity
{
    public class GraduityDetailAppService : AsyncCrudAppService<GraduityDetail, GraduityDetailDto, long, PagedResultRequestDto,GraduityDetailDto, GraduityDetailDto>, IGraduityDetailAppService
    {
        private readonly IRepository<GraduityDetail, long> _GraduityDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public GraduityDetailAppService(IRepository<GraduityDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _GraduityDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }

}
