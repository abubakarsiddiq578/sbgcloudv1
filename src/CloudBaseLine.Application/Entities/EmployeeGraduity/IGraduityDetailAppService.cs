﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.EmployeeGraduity.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeGraduity
{
    public interface IGraduityDetailAppService : IAsyncCrudAppService<GraduityDetailDto, long, PagedResultRequestDto, GraduityDetailDto, GraduityDetailDto>
    {

    }
}
