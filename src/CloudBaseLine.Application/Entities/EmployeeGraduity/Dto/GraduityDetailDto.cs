﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.WorkGroup;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeGraduity.Dto
{
    [AutoMapTo(typeof(GraduityDetail)), AutoMapFrom(typeof(GraduityDetailDto))]
    public class GraduityDetailDto : EntityDto<long>
    {
       
        public long GraduityMasterId { get; set; }
        public  GraduityMaster GraduityMaster { get; set; }

      
        public long GraduityConfigurationId { get; set; }
        public  GratuityConfiguration GratuityConfiguration { get; set; }

        public string EmployeeCode { get; set; }

        public string EmployeeName { get; set; }

        public string Department { get; set; }
        public string Designation { get; set; }

        public string JoiningDate { get; set; }

        public string LeavingDate { get; set; }

        public string GraduityAmount { get; set; }

        public int TenantId { get; set; }
    }
}