﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.WorkGroup;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.EmployeeGraduity.Dto
{
    [AutoMapTo(typeof(GraduityMaster)), AutoMapFrom(typeof(GraduityMasterDto))]
    public class GraduityMasterDto : EntityDto<long>
    {
        public long Doc_No { get; set; }
        public DateTime Date { get; set; }
        public virtual IEnumerable<GraduityDetail> GraduityDetails { get; set; }

        public int TenantId { get; set; }
    }
}
