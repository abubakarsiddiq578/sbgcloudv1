﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.Salary;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Overtimes.Dto
{
    [AutoMapTo(typeof(Overtime)), AutoMapFrom(typeof(OvertimeDto))]
    public class OvertimeDto:EntityDto<long>
    {

        public long EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public DateTime OverTimeDate { get; set; }

        public int OverTimerHour { get; set; }

        public long CostCenterId { get; set; }

        public CostCenter CostCenter { get; set; }

        public long DepartmentId { get; set; }

        public Department Department { get; set; }
        
        public int TenantId { get; set; }
        
    }
}
