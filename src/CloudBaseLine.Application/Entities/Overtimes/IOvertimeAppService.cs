﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Overtimes.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Overtimes
{
    public interface IOvertimeAppService : IAsyncCrudAppService<OvertimeDto, long, PagedResultRequestDto, OvertimeDto, OvertimeDto>
    {

    }
}
