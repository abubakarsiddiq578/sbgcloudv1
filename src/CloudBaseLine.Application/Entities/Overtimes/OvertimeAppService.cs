﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Overtimes.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Overtimes
{
    public class OvertimeAppService : AsyncCrudAppService<Overtime ,OvertimeDto, long, PagedResultRequestDto, OvertimeDto, OvertimeDto>,IOvertimeAppService
    {
        private readonly IRepository<Overtime, long> OvertimeRepository;
        private readonly IPermissionManager _permissionManager;

        public OvertimeAppService(IRepository<Overtime, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

             OvertimeRepository = _repository;

            _permissionManager = _Manager;

        }


        public async Task<List<OvertimeDto>> GetAllOvertimes()
        {
            try
            {
                var a = OvertimeRepository.GetAll().Where(b => b.IsDeleted == false).Include(r =>r.Employee).ToList();

                return a.MapTo<List<OvertimeDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        public async Task<List<OvertimeDto>> GetOvertime(long? empid , long? costCenterId, long? deptid , DateTime? ovtMonth )
        {
            
            try
            {
                if (empid == null && costCenterId == null && deptid == null && ovtMonth == null)
                {

                    return null;
                }

                var a =  OvertimeRepository.GetAll().Where(d => d.IsDeleted == false).Include(r => r.Employee).Include(t => t.Department).Include(q => q.CostCenter).ToList(); ;
                
                if(empid != null)
                {
                    var t = a;
                    a = a.Where(b=>b.EmployeeId == empid).ToList();
                
                }
                if(costCenterId != null)
                {
                    var t = a;
                    a = a.Where(b => b.CostCenterId == costCenterId).ToList();
                   
                }
                if (deptid != null)
                {
                    var t = a;
                    a = a.Where(b => b.DepartmentId == deptid).ToList();

                  
                }

                if (ovtMonth != null)
                {
                    
                    a = a.Where(b => b.OverTimeDate.Date.ToShortDateString() == ovtMonth.Value.Date.ToShortDateString()).ToList();
                   
                }
              
                return a.MapTo<List<OvertimeDto>>();

            }

            catch (Exception ex)
            {
                throw ex;

            }


        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Overtimes_Create)]
        public override async Task<OvertimeDto> Create(OvertimeDto input)
        {
            var result = ObjectMapper.Map<Overtime>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            


            try
            {
                if (result.Id > 0)
                {
                    await OvertimeRepository.UpdateAsync(result);
                } else
                {
                    await OvertimeRepository.InsertAsync(result);
                }
                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<OvertimeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


            
            
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Overtimes_Edit)]
        public override async Task<OvertimeDto> Update(OvertimeDto input)
        {
            var data = ObjectMapper.Map<Overtime>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await OvertimeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<OvertimeDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Overtimes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = OvertimeRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await OvertimeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


    }
}
