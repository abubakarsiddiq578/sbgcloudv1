﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SubCategories
{
    public interface ISubCategoryAppService : IAsyncCrudAppService<SubCategoryDto, long, PagedResultRequestDto, SubCategoryDto, SubCategoryDto>
    {
    }
}
