﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.SubCategories
{
    public class SubCategoryAppService : AsyncCrudAppService<SubCategory, SubCategoryDto, long, PagedResultRequestDto, SubCategoryDto, SubCategoryDto>
    {
        private readonly IRepository<SubCategory, long> SubCategoryRepository;
        private readonly IPermissionManager _permissionManager;

        public SubCategoryAppService(IRepository<SubCategory, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            SubCategoryRepository = _repository;

            _permissionManager = _Manager;

        }

        public async Task<List<SubCategoryDto>> GetAllSubCategories()
        {
            try
            {
                var result = SubCategoryRepository.GetAll().Where(a => a.IsDeleted == false).ToList();


                return result.MapTo<List<SubCategoryDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SubCategories_Create)]
        public override async Task<SubCategoryDto> Create(SubCategoryDto input)
        {
            var result = ObjectMapper.Map<SubCategory>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await SubCategoryRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<SubCategoryDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SubCategories_Edit)]
        public override async Task<SubCategoryDto> Update(SubCategoryDto input)
        {
            var data = ObjectMapper.Map<SubCategory>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await SubCategoryRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<SubCategoryDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_SubCategories_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = SubCategoryRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await SubCategoryRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


    }
}
