﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.SubCategories
{

    [AutoMapTo(typeof(SubCategory)), AutoMapFrom(typeof(SubCategoryDto))]
    public class SubCategoryDto : EntityDto<long>
    {

        public string code { get; set; }

        public string Name { get; set; }


        public int SortOrder { get; set; }

        public string Remarks { get; set; }

    }
}
