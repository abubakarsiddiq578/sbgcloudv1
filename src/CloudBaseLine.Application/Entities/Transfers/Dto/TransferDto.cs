﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Transfers.Dto
{

    [AutoMapTo(typeof(Transfer)), AutoMapFrom(typeof(TransferDto))]
    public class TransferDto:EntityDto<long>
    {
        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }


        public DateTime TransferDate { get; set; }

        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }


        public long TransferFromId { get; set; }

        public Department TransFrom { get; set; }


        public long TransferToId { get; set; }

        public Department TransTo { get; set; }

        public string TransferType { get; set; }

        public string Detail { get; set; }

    }
}
