﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Transfers.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Transfers
{
    public interface ITransferAppService : IAsyncCrudAppService<TransferDto, long, PagedResultRequestDto, TransferDto, TransferDto>
    {
    }
}
