﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Transfers.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Transfers
{
    public class TransferAppService : AsyncCrudAppService<Transfer , TransferDto, long, PagedResultRequestDto, TransferDto, TransferDto>,ITransferAppService
    {

        private readonly IRepository<Transfer, long> TransferRepository;
        private readonly IPermissionManager _permissionManager;


        public TransferAppService(IRepository<Transfer, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            TransferRepository = _repository;
            _permissionManager = _Manager;
        }

        public async Task<List<TransferDto>> GetAllTransfers()
        {
            try
            {
                var result = TransferRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.Employee).Include(t=>t.TransFrom).Include(t=>t.TransTo).ToList();


                return result.MapTo<List<TransferDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Transfers_Create)]
        public override async Task<TransferDto> Create(TransferDto input)
        {
            var result = ObjectMapper.Map<Transfer>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await TransferRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<TransferDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Transfers_Edit)]
        public override async Task<TransferDto> Update(TransferDto input)
        {
            var data = ObjectMapper.Map<Transfer>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await TransferRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<TransferDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Transfers_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = TransferRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await TransferRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }



    }
}
