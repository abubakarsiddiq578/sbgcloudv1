﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Promotions.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Promotions
{
    public interface IPromotionAppService : IAsyncCrudAppService<EmpPromotionDto, long, PagedResultRequestDto, EmpPromotionDto, EmpPromotionDto>
    {
    }
}
