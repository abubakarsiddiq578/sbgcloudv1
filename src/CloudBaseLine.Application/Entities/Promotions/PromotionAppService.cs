﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Promotions.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Promotions
{
    public class PromotionAppService : AsyncCrudAppService<EmpPromotion, EmpPromotionDto, long, PagedResultRequestDto, EmpPromotionDto, EmpPromotionDto>, IPromotionAppService
    {
        private readonly IRepository<EmpPromotion, long> _PromotionRepository;
        private readonly IPermissionManager _permissionManager;

        public PromotionAppService(IRepository<EmpPromotion, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _PromotionRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<EmpPromotionDto>> GetAllPromotions()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _PromotionRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<EmpPromotionDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Promotions_Create)]
        public override async Task<EmpPromotionDto> Create(EmpPromotionDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<EmpPromotion>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _PromotionRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<EmpPromotionDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Promotions_Edit)]
        public override async Task<EmpPromotionDto> Update(EmpPromotionDto input)
        {
            var data = ObjectMapper.Map<EmpPromotion>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _PromotionRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<EmpPromotionDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Promotions_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _PromotionRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _PromotionRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
