﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Promotions.Dto
{

    [AutoMapTo(typeof(EmpPromotion)), AutoMapFrom(typeof(EmpPromotionDto))]
    public class EmpPromotionDto : EntityDto<long>
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public DateTime PromotionDate { get; set; }

        public string PromotionType { get; set; }

        
        public long PromotionFrom { get; set; }

        public virtual EmployeeDesignation proFrom { get; set; }

        public long PromotionTo { get; set; }

        public EmployeeDesignation proTo { get; set; }

        public string Detail { get; set; }

        public int TenantId { get; set; }
    }
}
