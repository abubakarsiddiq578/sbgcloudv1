﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.EmployeesExit.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.EmployeesExit
{
    public class EmployeeExitAppService : AsyncCrudAppService <EmployeeExit , EmployeeExitDto, long, PagedResultRequestDto, EmployeeExitDto, EmployeeExitDto> , IEmployeeExitAppService
    {


        private readonly IRepository<EmployeeExit, long> EmployeeExitRepository;
        private readonly IPermissionManager _permissionManager;


        public EmployeeExitAppService(IRepository<EmployeeExit, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            EmployeeExitRepository = _repository;
            _permissionManager = _Manager;
        }

        //to get all EmployeeExits
        public async Task<List<EmployeeExitDto>> GetAllEmployeeExits()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                //get all active user
                var result = EmployeeExitRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(t=>t.Employee).ToList();

                return result.MapTo<List<EmployeeExitDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeExits_Create)]
        //to create a EmployeeExit
        public override async Task<EmployeeExitDto> Create(EmployeeExitDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = ObjectMapper.Map<EmployeeExit>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await EmployeeExitRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }


            var data = result.MapTo<EmployeeExitDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeExits_Edit)]
        //to update a EmployeeExit
        public override async Task<EmployeeExitDto> Update(EmployeeExitDto input)
        {
            var data = ObjectMapper.Map<EmployeeExit>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await EmployeeExitRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<EmployeeExitDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeeExits_Delete)]
        //to delete a EmployeeExit
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = EmployeeExitRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await EmployeeExitRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }




    }
}
