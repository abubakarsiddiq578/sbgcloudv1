﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeesExit.Dto
{
    [AutoMapTo(typeof(EmployeeExit)), AutoMapFrom(typeof(EmployeeExitDto))]
    public class EmployeeExitDto : EntityDto<long>
    {

        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }


        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public string Exit_Type { get; set; }

        public DateTime Exit_Date { get; set; }

        public string details { get; set; }

        public int TenantId { get; set; }
    }
}
