﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.EmployeesExit.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeesExit
{
    public interface IEmployeeExitAppService :  IAsyncCrudAppService<EmployeeExitDto, long, PagedResultRequestDto, EmployeeExitDto, EmployeeExitDto>
    {
    }
}
