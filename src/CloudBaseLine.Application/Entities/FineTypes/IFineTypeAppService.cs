﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.FineTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.FineTypes
{
    public interface IFineTypeAppService : IAsyncCrudAppService<FineTypeDto, long, PagedResultRequestDto, FineTypeDto, FineTypeDto>
    {
    }
}
