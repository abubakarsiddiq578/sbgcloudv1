﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.FineTypes.Dto
{
    [AutoMapTo(typeof(FineType)), AutoMapFrom(typeof(FineTypeDto))]
    public class FineTypeDto: EntityDto<long>
    {
        public string FintTitle { get; set; }

        public long FineAmmount { get; set; }

        public string FineDetail { get; set; }

        public int SortOrder { get; set; }

        public int TenantId { get; set; }
    }
}
