﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.FineTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.FineTypes
{
    public class FineTypeAppService : AsyncCrudAppService<FineType, FineTypeDto, long, PagedResultRequestDto, FineTypeDto, FineTypeDto>, IFineTypeAppService
    {
        private readonly IRepository<FineType, long> _FineTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public FineTypeAppService(IRepository<FineType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _FineTypeRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<FineTypeDto>> GetAllFineTypes()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _FineTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<FineTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_FineTypes_Create)]
        public override async Task<FineTypeDto> Create(FineTypeDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<FineType>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _FineTypeRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<FineTypeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_FineTypes_Edit)]
        public override async Task<FineTypeDto> Update(FineTypeDto input)
        {
            var data = ObjectMapper.Map<FineType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _FineTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<FineTypeDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_FineTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _FineTypeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _FineTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
