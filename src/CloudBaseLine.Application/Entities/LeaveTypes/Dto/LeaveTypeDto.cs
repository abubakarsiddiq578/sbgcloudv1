﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LeaveTypes.Dto
{
    [AutoMapTo(typeof(LeaveType)), AutoMapFrom(typeof(LeaveTypeDto))]
    public class LeaveTypeDto : EntityDto<long>
    {
        public string LeaveTypeName { get; set; }

        public int LeaveAllowed { get; set; }

        public DateTime? LeaveReset { get; set; }

        public string LeaveDetail { get; set; }
        public int TenantId { get; set; }
    }
}
