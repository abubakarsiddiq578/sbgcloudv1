﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.LeaveTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.LeaveTypes
{
    public class LeaveTypeAppService : AsyncCrudAppService<LeaveType, LeaveTypeDto, long, PagedResultRequestDto, LeaveTypeDto, LeaveTypeDto>, ILeaveTypeAppService
    {
        private readonly IRepository<LeaveType, long> _LeaveTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public LeaveTypeAppService(IRepository<LeaveType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _LeaveTypeRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<LeaveTypeDto>> GetAllLeaveTypes()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _LeaveTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<LeaveTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveTypes_Create)]
        public override async Task<LeaveTypeDto> Create(LeaveTypeDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<LeaveType>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _LeaveTypeRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<LeaveTypeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveTypes_Edit)]
        public override async Task<LeaveTypeDto> Update(LeaveTypeDto input)
        {
            var data = ObjectMapper.Map<LeaveType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _LeaveTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<LeaveTypeDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LeaveTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _LeaveTypeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _LeaveTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
