﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.LeaveTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.LeaveTypes
{
    public interface ILeaveTypeAppService : IAsyncCrudAppService<LeaveTypeDto, long, PagedResultRequestDto, LeaveTypeDto, LeaveTypeDto>
    {
        Task<List<LeaveTypeDto>> GetAllLeaveTypes();
    }
}
