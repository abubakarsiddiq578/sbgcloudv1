﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Samples.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Samples
{
    public class SampleAppService: AsyncCrudAppService<Sample,createSample, long, PagedResultRequestDto, createSample, createSample>,ISampleAppService
    {
        private readonly IRepository<Sample,long> _Samplerepository;
        private readonly IPermissionManager _permissionManager;

        public SampleAppService(IRepository<Sample, long> _repository, IPermissionManager _Manager): base(_repository)
        {
            _Samplerepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Samples_Create)]
        public override async Task<createSample> Create(createSample input)
        {
            var result = ObjectMapper.Map<Sample>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;

            await _Samplerepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data =  result.MapTo<createSample>();
            return data;

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Samples_Edit)]
        public override async Task<createSample> Update(createSample input)
        {
            var data = ObjectMapper.Map<Sample>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _Samplerepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<createSample>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Samples_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _Samplerepository.GetAll().Where(x=>x.Id== input.Id).FirstOrDefault();
            if(result!=null)
            {
                await _Samplerepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }


        }
}
