﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Samples.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Samples
{
    public interface ISampleAppService: IAsyncCrudAppService<createSample, long, PagedResultRequestDto,createSample, createSample>
    {
    }
}
