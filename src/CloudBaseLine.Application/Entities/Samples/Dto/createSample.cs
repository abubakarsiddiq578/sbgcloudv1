﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Samples.Dto
{
    [AutoMapTo(typeof(Sample)), AutoMapFrom(typeof(Sample))]
    public class createSample:EntityDto<long>
    {
        public string Name { get; set; }

        public int Salary { get; set; }

        public string Designation { get; set; }

        public string CompanyName { get; set; }
    }
}
