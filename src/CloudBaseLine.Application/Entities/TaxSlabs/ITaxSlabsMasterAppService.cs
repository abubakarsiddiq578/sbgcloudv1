﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.TaxSlabs;
using CloudBaseLine.Entities.TaxSlabs.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace CloudBaseLine.Entities.TaxSlabs
{
    public interface ITaxSlabsMasterAppService : IAsyncCrudAppService<TaxSlabMasterDto, long, PagedResultRequestDto, TaxSlabMasterDto, TaxSlabMasterDto>
    {
        Task<List<TaxSlabMasterDto>> GetAllTaxSlabs();
        Task<List<TaxSlabDetailDto>> GetAllDetailById(long id);
    }
}
