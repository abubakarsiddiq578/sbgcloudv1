﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.TaxSlabs;
using CloudBaseLine.Entities.TaxSlabs.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.TaxSlabs
{
    public class TaxSlabsMasterAppService : AsyncCrudAppService<TaxSlabsMaster, TaxSlabMasterDto, long, PagedResultRequestDto, TaxSlabMasterDto, TaxSlabMasterDto>, ITaxSlabsMasterAppService
    {
        private readonly IRepository<TaxSlabsMaster, long> _TaxSlabsMasterRepository;
        private readonly IRepository<TaxSlabsDetail, long> _TaxSlabsDetailRepositoty;
        private readonly IPermissionManager _permissionManager;

        public TaxSlabsMasterAppService(IRepository<TaxSlabsMaster, long> _repository, IPermissionManager _Manager, IRepository<TaxSlabsDetail, long> _detailRepository) : base(_repository)
        {
            _TaxSlabsMasterRepository = _repository;
            _TaxSlabsDetailRepositoty = _detailRepository;
            _permissionManager = _Manager;
        }

        public async Task<List<TaxSlabMasterDto>> GetAllTaxSlabs()
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _TaxSlabsMasterRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<TaxSlabMasterDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<List<TaxSlabDetailDto>> GetAllDetailById(long id)
        {
            try
            {
                var TenantId = AbpSession.TenantId;

                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = _TaxSlabsDetailRepositoty.GetAll()
                    .Where(a => a.IsDeleted == false && a.TaxSlabsMasterId == id && a.TenantId == TenantId)
                    .Include(m => m.TaxSlabsMaster)
                    .ToList();
                return result.MapTo<List<TaxSlabDetailDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_TaxSlabs_Create)]
        public async Task Add(TaxSlabMasterDto input)
        {
            try
            {
                var result = ObjectMapper.Map<TaxSlabsMaster>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _TaxSlabsMasterRepository.InsertAsync(result);

                foreach (var i in input.TaxSlabsDetails)
                {
                    i.TaxSlabsMasterId = response.Id;

                    await _TaxSlabsDetailRepositoty.InsertAsync(i);
                }
                try
                {
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_TaxSlabs_Edit)]
        public async Task Edit(TaxSlabMasterDto input)
        {
            var data = ObjectMapper.Map<TaxSlabsMaster>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            var response = await _TaxSlabsMasterRepository.UpdateAsync(data);

            foreach (var i in input.TaxSlabsDetails)
            {
                i.TaxSlabsMasterId = response.Id;

                if (i.Id < 0)
                {
                    await _TaxSlabsDetailRepositoty.InsertAsync(i);
                }
                else
                {
                    await _TaxSlabsDetailRepositoty.UpdateAsync(i);
                }
            }

            var masterResult = _TaxSlabsMasterRepository.GetAll().Include(x => x.TaxSlabsDetails).Where(x => x.Id == input.Id).FirstOrDefault();

            bool checkedBit = true;

            foreach (var x in masterResult.TaxSlabsDetails)
            {
                checkedBit = true;

                foreach (var y in input.TaxSlabsDetails)
                {
                    if (x.Id == y.Id)
                    {
                        checkedBit = false;
                        break;
                    }
                }

                if (checkedBit == true)
                {
                    if (x != null)
                    {
                        await _TaxSlabsDetailRepositoty.DeleteAsync(x);
                    }
                }
            }

            CurrentUnitOfWork.SaveChanges();
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_TaxSlabs_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var masterResult = _TaxSlabsMasterRepository.GetAll().Include(x => x.TaxSlabsDetails).Where(x => x.Id == input.Id).FirstOrDefault();
            if (masterResult != null)
            {
                foreach (var i in masterResult.TaxSlabsDetails)
                {
                    if (i != null)
                    {
                        await _TaxSlabsDetailRepositoty.DeleteAsync(i);
                    }
                }

                await _TaxSlabsMasterRepository.DeleteAsync(masterResult);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
