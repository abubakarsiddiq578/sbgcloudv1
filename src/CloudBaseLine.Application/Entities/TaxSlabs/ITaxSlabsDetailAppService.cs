﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.TaxSlabs.Dto;
using System;
using System.Collections.Generic;
using System.Text;
namespace CloudBaseLine.Entities.TaxSlabs
{
    public interface ITaxSlabsDetailAppService : IAsyncCrudAppService<TaxSlabDetailDto, long, PagedResultRequestDto, TaxSlabDetailDto, TaxSlabDetailDto>
    {

    }
}
