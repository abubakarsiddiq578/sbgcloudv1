﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.TaxSlabs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.TaxSlabs
{
    public class TaxSlabAppService : AsyncCrudAppService<TaxSlab, TaxSlabDto, long, PagedResultRequestDto, TaxSlabDto, TaxSlabDto>, ITaxSlabAppService
    {
        private readonly IRepository<TaxSlab, long> _TaxSlabRepository;
        private readonly IPermissionManager _permissionManager;

        public TaxSlabAppService(IRepository<TaxSlab, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _TaxSlabRepository = _repository;
            _permissionManager = _Manager;
        }

        /// <summary>
        /// GetAllTaxSlabs will get all the TaxSlabs
        /// </summary>
        /// <returns></returns>
        public async Task<List<TaxSlabDto>> GetAllTaxSlabs()
        {
            try
            {
                var result = _TaxSlabRepository.GetAll().Where(a => a.IsDeleted == false).ToList();
                return result.MapTo<List<TaxSlabDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This Create function will add a new TaxSlab.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
          //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_TaxSlabs_Create)]
        public override async Task<TaxSlabDto> Create(TaxSlabDto input)
        {
            try
            {
                var result = ObjectMapper.Map<TaxSlab>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                await _TaxSlabRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<TaxSlabDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This update function will update the specific record according to given id.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
          //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_TaxSlabs_Edit)]
        public override async Task<TaxSlabDto> Update(TaxSlabDto input)
        {
            try
            {
                var data = ObjectMapper.Map<TaxSlab>(input);
                data.LastModificationTime = DateTime.Now;
                data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

                await _TaxSlabRepository.UpdateAsync(data);
                CurrentUnitOfWork.SaveChanges();

                var result = data.MapTo<TaxSlabDto>();
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// This Delete function will delete the specific record according to given number id.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
          //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_TaxSlabs_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                var result = _TaxSlabRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
                if (result != null)
                {
                    await _TaxSlabRepository.DeleteAsync(result);
                    CurrentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
