﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.TaxSlabs;
using CloudBaseLine.Entities.TaxSlabs.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.TaxSlabs
{
    public class TaxSlabsDetailAppService : AsyncCrudAppService<TaxSlabsDetail, TaxSlabDetailDto, long, PagedResultRequestDto, TaxSlabDetailDto, TaxSlabDetailDto>, ITaxSlabsDetailAppService
    {
        private readonly IRepository<TaxSlabsDetail, long> _TaxSlabsDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public TaxSlabsDetailAppService(IRepository<TaxSlabsDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _TaxSlabsDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }
}
