﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.TaxSlabs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.TaxSlabs.Dto
{
    [AutoMapTo(typeof(TaxSlabsMaster)), AutoMapFrom(typeof(TaxSlabMasterDto))]
    public class TaxSlabMasterDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string TaxType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public virtual IEnumerable<TaxSlabsDetail> TaxSlabsDetails { get; set; }
        public int TenantId { get; set; }
    }
}
