﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Entities.TaxSlabs;
using System;
using System.Collections.Generic;
using System.Text;


namespace CloudBaseLine.Entities.TaxSlabs.Dto
{
    [AutoMapTo(typeof(TaxSlabsDetail)), AutoMapFrom(typeof(TaxSlabDetailDto))]
    public class TaxSlabDetailDto : EntityDto<long>
    {
        public long TaxSlabsMasterId { get; set; }
        public TaxSlabsMaster TaxSlabsMaster { get; set; }
        public double ValueFrom { get; set; }
        public double ValueTo { get; set; }
        public double TaxPercentage { get; set; }
        public double Fixed { get; set; }
        public double ApplicableAmount { get; set; }
        public double ValuePerMonth { get; set; }
        public int TenantId { get; set; }
    }
}
