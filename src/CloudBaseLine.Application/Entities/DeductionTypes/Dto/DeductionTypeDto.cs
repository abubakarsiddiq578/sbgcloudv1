﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.DeductionTypes.Dto
{
    [AutoMapTo(typeof(DeductionType)), AutoMapFrom(typeof(DeductionTypeDto))]
    public class DeductionTypeDto : EntityDto<long>
    {
        public string DeductionTitle { get; set; }

        public long DeductionAmmount { get; set; }

        public string DeductionDetail { get; set; }

        public int SortOrder { get; set; }

        public int TenantId { get; set; }
    }
}
