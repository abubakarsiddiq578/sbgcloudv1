﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.DeductionTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.DeductionTypes
{
    public class DeductionTypeAppService : AsyncCrudAppService<DeductionType, DeductionTypeDto, long, PagedResultRequestDto, DeductionTypeDto, DeductionTypeDto>, IDeductionTypeAppService
    {
        private readonly IRepository<DeductionType, long> _DeductionTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public DeductionTypeAppService(IRepository<DeductionType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _DeductionTypeRepository = _repository;
            _permissionManager = _Manager;
        }


      
        public async Task<List<DeductionTypeDto>> GetAllDeductionTypes()
        {
            try
            {
                var result = _DeductionTypeRepository.GetAll().Where(a => a.IsDeleted == false).ToList();
                return result.MapTo<List<DeductionTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_DeductionTypes_Create)]
        public override async Task<DeductionTypeDto> Create(DeductionTypeDto input)
        {
            try
            {
                var result = ObjectMapper.Map<DeductionType>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                await _DeductionTypeRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<DeductionTypeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }




        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_DeductionTypes_Edit)]
        public override async Task<DeductionTypeDto> Update(DeductionTypeDto input)
        {
            var data = ObjectMapper.Map<DeductionType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _DeductionTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<DeductionTypeDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_DeductionTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _DeductionTypeRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _DeductionTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
