﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.DeductionTypes.Dto;
using CloudBaseLine.Entities.LeaveTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.DeductionTypes
{
    public interface IDeductionTypeAppService : IAsyncCrudAppService<DeductionTypeDto, long, PagedResultRequestDto, DeductionTypeDto, DeductionTypeDto>
    {
    }
}
