﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.BonusTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.BonusTypes
{
    public interface IBonusTypeAppService : IAsyncCrudAppService<BonusTypeDto, long, PagedResultRequestDto, BonusTypeDto, BonusTypeDto>
    {
    }
}
