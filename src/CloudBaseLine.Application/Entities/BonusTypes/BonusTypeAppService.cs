﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.BonusTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.BonusTypes
{
    public class BonusTypeAppService : AsyncCrudAppService<BonusType, BonusTypeDto, long, PagedResultRequestDto, BonusTypeDto, BonusTypeDto>, IBonusTypeAppService
    {
        private readonly IRepository<BonusType, long> _BonusTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public BonusTypeAppService(IRepository<BonusType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _BonusTypeRepository = _repository;
            _permissionManager = _Manager;
        }

      
        public async Task<List<BonusTypeDto>> GetAllBonusTypes()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _BonusTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId ).ToList();
                return result.MapTo<List<BonusTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_BonusTypes_Create)]
        public override async Task<BonusTypeDto> Create(BonusTypeDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<BonusType>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _BonusTypeRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<BonusTypeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_BonusTypes_Edit)]
        public override async Task<BonusTypeDto> Update(BonusTypeDto input)
        {
            var data = ObjectMapper.Map<BonusType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _BonusTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<BonusTypeDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_BonusTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _BonusTypeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId ).FirstOrDefault();
            if (result != null)
            {
                await _BonusTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
