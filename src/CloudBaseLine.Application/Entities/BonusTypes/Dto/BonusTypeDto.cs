﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.BonusTypes.Dto
{
    [AutoMapTo(typeof(BonusType)), AutoMapFrom(typeof(BonusTypeDto))]
    public class BonusTypeDto : EntityDto<long>
    {
        public string BonusTitle { get; set; }

        public int BonusPercentage { get; set; }

        public string BonusDetail { get; set; }

        public int SortOrder { get; set; }

        public int TenantId { get; set; }
    }
}
