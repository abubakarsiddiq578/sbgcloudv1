﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.UserCompanyRights.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.UserCompanyRights
{
    public class UserCompanyRightAppService : AsyncCrudAppService<UserCompanyRight, UserCompanyRightDto, long, PagedResultRequestDto, UserCompanyRightDto, UserCompanyRightDto>, IUserCompanyRightAppService
    {
        private readonly IRepository<UserCompanyRight, long> _UserCompanyRightrepository;
        private readonly IPermissionManager _permissionManager;

        public UserCompanyRightAppService(IRepository<UserCompanyRight, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _UserCompanyRightrepository = _repository;
            _permissionManager = _Manager;
        }



        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserCompanyRights_Create)]
        public override async Task<UserCompanyRightDto> Create(UserCompanyRightDto input)
        {
            var result = ObjectMapper.Map<UserCompanyRight>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _UserCompanyRightrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserCompanyRightDto>();
            return data;

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserCompanyRights_Edit)]
        public override async Task<UserCompanyRightDto> Update(UserCompanyRightDto input)
        {
            var data = ObjectMapper.Map<UserCompanyRight>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _UserCompanyRightrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserCompanyRightDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserCompanyRights_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _UserCompanyRightrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _UserCompanyRightrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<UserCompanyRightDto>> GetCompanyRights()
        {

            var result = _UserCompanyRightrepository.GetAll().Include(a => a.CompanyInfo).Include(b => b.User).ToList();
            return result.MapTo<List<UserCompanyRightDto>>();

            //var result = _UserCompanyRightrepository.GetAllIncluding(p=>p.User);
        }
    }
}
