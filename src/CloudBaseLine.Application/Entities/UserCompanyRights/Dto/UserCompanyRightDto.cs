﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Authorization.Users;

namespace CloudBaseLine.Entities.UserCompanyRights.Dto
{
    [AutoMapTo(typeof(UserCompanyRight)), AutoMapFrom(typeof(UserCompanyRightDto))]
    public class UserCompanyRightDto : EntityDto<long>
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public long CompanyId { get; set; }
        public CompanyInfo CompanyInfo { get; set; }
        public bool Rights { get; set; }
        public int TenantId { get; set; }
    }
}
