﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.UserCompanyRights.Dto;

namespace CloudBaseLine.Entities.UserCompanyRights
{
    public interface IUserCompanyRightAppService : IAsyncCrudAppService<UserCompanyRightDto, long, PagedResultRequestDto, UserCompanyRightDto, UserCompanyRightDto>
    {
        Task<List<UserCompanyRightDto>> GetCompanyRights();
    }
}
