﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.UserVoucherTypeRights.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.UserVoucherTypeRights
{
    public class UserVoucherTypeRightAppService : AsyncCrudAppService<UserVoucherTypeRight, UserVoucherTypeRightDto, long, PagedResultRequestDto, UserVoucherTypeRightDto, UserVoucherTypeRightDto>, IUserVoucherTypeRightAppService
    {
        private readonly IRepository<UserVoucherTypeRight, long> _UserVoucherTypeRightrepository;
        private readonly IPermissionManager _permissionManager;

        public UserVoucherTypeRightAppService(IRepository<UserVoucherTypeRight, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _UserVoucherTypeRightrepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserVoucherTypeRights_Create)]
        public override async Task<UserVoucherTypeRightDto> Create(UserVoucherTypeRightDto input)
        {
            var result = ObjectMapper.Map<UserVoucherTypeRight>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _UserVoucherTypeRightrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserVoucherTypeRightDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserVoucherTypeRights_Edit)]
        public override async Task<UserVoucherTypeRightDto> Update(UserVoucherTypeRightDto input)
        {
            var data = ObjectMapper.Map<UserVoucherTypeRight>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _UserVoucherTypeRightrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserVoucherTypeRightDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserVoucherTypeRights_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _UserVoucherTypeRightrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _UserVoucherTypeRightrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<UserVoucherTypeRightDto>> GetVoucherTypeRights()
        {
            var result = _UserVoucherTypeRightrepository.GetAll().Include(a => a.VoucherType).Include(b => b.User)
                .OrderBy(x => x.VoucherType.sort_order).ToList();
            return result.MapTo<List<UserVoucherTypeRightDto>>();
        }
    }
}
