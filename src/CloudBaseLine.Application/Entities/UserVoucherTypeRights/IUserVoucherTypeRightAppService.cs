﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.UserVoucherTypeRights.Dto;

namespace CloudBaseLine.Entities.UserVoucherTypeRights
{
    public interface IUserVoucherTypeRightAppService : IAsyncCrudAppService<UserVoucherTypeRightDto, long, PagedResultRequestDto, UserVoucherTypeRightDto, UserVoucherTypeRightDto>
    {
        Task<List<UserVoucherTypeRightDto>> GetVoucherTypeRights();
    }
}
