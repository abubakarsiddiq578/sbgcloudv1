﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Authorization.Users;

namespace CloudBaseLine.Entities.UserVoucherTypeRights.Dto
{
    [AutoMapTo(typeof(UserVoucherTypeRight)), AutoMapFrom(typeof(UserVoucherTypeRightDto))]
    public class UserVoucherTypeRightDto : EntityDto<long>
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public long VoucherTypeId { get; set; }
        public VoucherType VoucherType { get; set; }
        public bool Rights { get; set; }
        public int TenantId { get; set; }
    }
}
