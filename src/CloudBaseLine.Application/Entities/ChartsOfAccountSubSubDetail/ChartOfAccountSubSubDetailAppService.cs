﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.ChartsOfAccountSubSubDetail.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ChartsOfAccountSubSubDetail
{
    public class ChartOfAccountSubSubDetailAppService : AsyncCrudAppService<ChartOfAccountSubSubDetail, ChartOfAccountSubSubDetailDto, long, PagedResultRequestDto, ChartOfAccountSubSubDetailDto, ChartOfAccountSubSubDetailDto>, IChartOfAccountSubSubDetailAppService
    {

        private readonly IRepository<ChartOfAccountSubSubDetail, long> _ChartOfAccountSubSubDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public ChartOfAccountSubSubDetailAppService(IRepository<ChartOfAccountSubSubDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ChartOfAccountSubSubDetailRepository = _repository;
            _permissionManager = _Manager;
        }

       
        public async Task<List<ChartOfAccountSubSubDetailDto>> GetAllAccount()
        {

            var result = _ChartOfAccountSubSubDetailRepository.GetAll().Where(a => a.IsDeleted == false).Where(a => a.IsDeleted != true).Include(a => a.ChartOfAccountSubSubMain).ToList();
            return result.MapTo<List<ChartOfAccountSubSubDetailDto>>();

        }


        public async Task<List<ChartOfAccountSubSubDetailDto>> GetAllCashBankAccounts()
        {

            var result = _ChartOfAccountSubSubDetailRepository.GetAll().Where(a => a.IsDeleted == false).Where(a => a.IsDeleted != true).Include(a => a.ChartOfAccountSubSubMain)
                .Where(x => x.ChartOfAccountSubSubMain.Sub_sub_type == "Cash" ||
                x.ChartOfAccountSubSubMain.Sub_sub_type == "Bank").ToList();
            return result.MapTo<List<ChartOfAccountSubSubDetailDto>>();

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_Create)]
        public override async Task<ChartOfAccountSubSubDetailDto> Create(ChartOfAccountSubSubDetailDto input)
        {
            try
            {
                var result = ObjectMapper.Map<ChartOfAccountSubSubDetail>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.SortOrder = 1;
                await _ChartOfAccountSubSubDetailRepository.InsertAsync(result);
                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<ChartOfAccountSubSubDetailDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_Edit)]
        public override async Task<ChartOfAccountSubSubDetailDto> Update(ChartOfAccountSubSubDetailDto input)
        {
            var data = ObjectMapper.Map<ChartOfAccountSubSubDetail>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _ChartOfAccountSubSubDetailRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ChartOfAccountSubSubDetailDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubSubDetails_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _ChartOfAccountSubSubDetailRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _ChartOfAccountSubSubDetailRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
