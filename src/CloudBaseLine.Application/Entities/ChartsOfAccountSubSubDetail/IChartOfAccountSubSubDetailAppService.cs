﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ChartsOfAccountSubSubDetail.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ChartsOfAccountSubSubDetail
{
    public interface IChartOfAccountSubSubDetailAppService : IAsyncCrudAppService<ChartOfAccountSubSubDetailDto, long, PagedResultRequestDto, ChartOfAccountSubSubDetailDto, ChartOfAccountSubSubDetailDto>
    {
        Task<List<ChartOfAccountSubSubDetailDto>> GetAllCashBankAccounts();
    }
}
