﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ChartsOfAccountSubSubDetail.Dto
{
    [AutoMapTo(typeof(ChartOfAccountSubSubDetail)), AutoMapFrom(typeof(ChartOfAccountSubSubDetailDto))]
    public class ChartOfAccountSubSubDetailDto: EntityDto<long>
    {
        
        public long MainSubSubId { get; set; }
        public ChartOfAccountSubSubMain ChartOfAccountSubSubMain { get; set; }

        public string DetailCode { get; set; }

        public string DetailTitle { get; set; }

        public int SortOrder { get; set; }

        public float OpeningBalance { get; set; }

        public string AccessLevel { get; set; }

        public DateTime EndDate { get; set; }

        public int Old_Main_Sub_Sub_Id { get; set; }

        public string Old_Detail_Code { get; set; }

        public int? ParentId { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }
    }
}
