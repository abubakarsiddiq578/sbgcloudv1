﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ChartsOfAccountSubMain.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ChartsOfAccountSubMain
{
    public interface IChartOfAccountSubMainAppService : IAsyncCrudAppService<ChartOfAccountSubMainDto, long, PagedResultRequestDto, ChartOfAccountSubMainDto, ChartOfAccountSubMainDto>
    {
    }
}
