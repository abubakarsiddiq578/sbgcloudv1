﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.ChartsOfAccountSubMain.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ChartsOfAccountSubMain
{
    public class ChartOfAccountSubMainAppService : AsyncCrudAppService<ChartOfAccountSubMain, ChartOfAccountSubMainDto, long, PagedResultRequestDto, ChartOfAccountSubMainDto, ChartOfAccountSubMainDto>, IChartOfAccountSubMainAppService
    {

        private readonly IRepository<ChartOfAccountSubMain, long> _ChartOfAccountSubMainRepository;
        private readonly IPermissionManager _permissionManager;

        public ChartOfAccountSubMainAppService(IRepository<ChartOfAccountSubMain, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ChartOfAccountSubMainRepository = _repository;
            _permissionManager = _Manager;
        }

        public async Task<List<ChartOfAccountSubMainDto>> GetAllSubAccount()
        {
            try
            {
                var result = _ChartOfAccountSubMainRepository.GetAll().Include(a => a.ChartOfAccountMain).ToList();
                return result.MapTo<List<ChartOfAccountSubMainDto>>();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            //var result = _UserCompanyRightrepository.GetAllIncluding(p=>p.User);
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Create)]
        public override async Task<ChartOfAccountSubMainDto> Create(ChartOfAccountSubMainDto input)
        {
            var result = ObjectMapper.Map<ChartOfAccountSubMain>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await _ChartOfAccountSubMainRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();
            var data = result.MapTo<ChartOfAccountSubMainDto>();
            return data;

        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Edit)]
        public override async Task<ChartOfAccountSubMainDto> Update(ChartOfAccountSubMainDto input)
        {
            var data = ObjectMapper.Map<ChartOfAccountSubMain>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _ChartOfAccountSubMainRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ChartOfAccountSubMainDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubMains_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _ChartOfAccountSubMainRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _ChartOfAccountSubMainRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
