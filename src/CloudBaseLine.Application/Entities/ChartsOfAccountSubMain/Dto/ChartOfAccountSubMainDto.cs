﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ChartsOfAccountSubMain.Dto
{
    [AutoMapTo(typeof(ChartOfAccountSubMain)), AutoMapFrom(typeof(ChartOfAccountSubMainDto))]
    public class ChartOfAccountSubMainDto: EntityDto<long>
    {
        public long ChartOfAccMainID { get; set; }
        public ChartOfAccountMain ChartOfAccountMain { get; set; }

        public string Sub_code { get; set; }

        public string Sub_title { get; set; }

        public int TenantId { get; set; }

    }
}
