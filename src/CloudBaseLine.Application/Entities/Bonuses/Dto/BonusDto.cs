﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using CloudBaseLine.Entities.Bonuses;

namespace CloudBaseLine.Entities.Bonuses.Dto
{
    [AutoMapTo(typeof(Bonus)),AutoMapFrom(typeof(BonusDto)) ]
    public class BonusDto : EntityDto<long>
    {

        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public DateTime BonusDate { get; set; }
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public long BonusTypeId { get; set; }
        public virtual BonusType BonusType { get; set; }
        public int Percentage { get; set; }
        public string Detail { get; set; }
    }
}
