﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Bonuses;
using CloudBaseLine.Entities.Bonuses.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Bonuses
{
    public class BonusAppService : AsyncCrudAppService<Bonus, BonusDto, long, PagedResultRequestDto, BonusDto, BonusDto>, IBonusAppService
    {
        private readonly IRepository<Bonus, long> Bonusrepository;
        private readonly IPermissionManager _permissionManager;

        public BonusAppService(IRepository<Bonus, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            Bonusrepository = _repository;
            _permissionManager = _Manager;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Bonuses_Create)]
        public override async Task<BonusDto> Create(BonusDto input)
        {
            var result = ObjectMapper.Map<Bonus>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await Bonusrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<BonusDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Bonuses_Edit)]
        public override async Task<BonusDto> Update(BonusDto input)
        {
            var data = ObjectMapper.Map<Bonus>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await Bonusrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<BonusDto>();
            return result;
        }
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Bonuses_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = Bonusrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await Bonusrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


       
        public async Task<List<BonusDto>> GetAllBonus()
        {
            var result = Bonusrepository.GetAll().Include(b => b.Employee)
                .Include(a => a.BonusType).Where(x => x.IsDeleted == false).ToList();
            return result.MapTo<List<BonusDto>>();
        }
    }
}
