﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Bonuses.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Bonuses
{
    public interface IBonusAppService : IAsyncCrudAppService<BonusDto, long, PagedResultRequestDto, BonusDto, BonusDto>
    {

        Task<List<BonusDto>> GetAllBonus();

    }
}
