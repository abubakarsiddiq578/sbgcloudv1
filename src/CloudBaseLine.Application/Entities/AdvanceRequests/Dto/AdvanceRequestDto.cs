﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.AdvanceRequests.Dto
{

    [AutoMapTo(typeof(AdvanceRequest)), AutoMapFrom(typeof(AdvanceRequestDto))]
    public class AdvanceRequestDto:EntityDto<long>
    {
        public string DocumentNo { get; set; }

        public DateTime DocumentDate { get; set; }


        public DateTime DeductionDate { get; set; }

        public long EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public double Amount { get; set; }


        public string Detail { get; set; }


        public int TenantId { get; set; }

    }
}
