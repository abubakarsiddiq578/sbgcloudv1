﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.AdvanceRequests.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.AdvanceRequests
{
    public interface IAdvanceRequestAppService : IAsyncCrudAppService<AdvanceRequestDto, long, PagedResultRequestDto, AdvanceRequestDto, AdvanceRequestDto>
    {


    }
}
