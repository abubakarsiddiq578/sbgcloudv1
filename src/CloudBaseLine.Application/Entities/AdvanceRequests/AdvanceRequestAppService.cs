﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.AdvanceRequests.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.AdvanceRequests
{
    public class AdvanceRequestAppService : AsyncCrudAppService<AdvanceRequest, AdvanceRequestDto, long, PagedResultRequestDto, AdvanceRequestDto, AdvanceRequestDto>, IAdvanceRequestAppService
    {

        private readonly IRepository<AdvanceRequest, long> AdvanceRequestRepository;
        private readonly IPermissionManager _permissionManager;


        public AdvanceRequestAppService(IRepository<AdvanceRequest, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            AdvanceRequestRepository = _repository;
            _permissionManager = _Manager;
        }
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AdvanceRequests_View)]
        public async Task<List<AdvanceRequestDto>> GetAllAdvanceRequests()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = AdvanceRequestRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.Employee).ToList();


                return result.MapTo<List<AdvanceRequestDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AdvanceRequests_Create)]
        public override async Task<AdvanceRequestDto> Create(AdvanceRequestDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = ObjectMapper.Map<AdvanceRequest>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await AdvanceRequestRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<AdvanceRequestDto>();
            return data;
        }



        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AdvanceRequests_Edit)]
        public override async Task<AdvanceRequestDto> Update(AdvanceRequestDto input)
        {
            var data = ObjectMapper.Map<AdvanceRequest>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await AdvanceRequestRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<AdvanceRequestDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AdvanceRequests_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {

            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = AdvanceRequestRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId ).FirstOrDefault();
            if (result != null)
            {
                await AdvanceRequestRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }







    }
}
