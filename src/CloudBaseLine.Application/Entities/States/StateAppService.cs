﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.States.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.States
{
    public class StateAppService : AsyncCrudAppService<State , StateDto, long, PagedResultRequestDto, StateDto, StateDto>,IStateAppService
    {

        private readonly IRepository<State, long> StateRepository;
        private readonly IPermissionManager _permissionManager;

        public StateAppService(IRepository<State, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            StateRepository = _repository;

            _permissionManager = _Manager;

        }



        public async Task<List<StateDto>> GetAllStates()
        {

            try
            {
                var a = StateRepository.GetAll().Where(b => b.IsDeleted == false).Include(m=>m.Country).ToList();

                return a.MapTo<List<StateDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_States_Create)]
        public override async Task<StateDto> Create(StateDto input)
        {
            var result = ObjectMapper.Map<State>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await StateRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }


            var data = result.MapTo<StateDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_States_Edit)]
        public override async Task<StateDto> Update(StateDto input)
        {
            var data = ObjectMapper.Map<State>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await StateRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<StateDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_States_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = StateRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await StateRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

    }
}
