﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.States.Dto
{
    [AutoMapTo(typeof(State)),AutoMapFrom(typeof(StateDto))]
    public class StateDto : EntityDto<long>
    {
        public long CountryId { get; set; }

        public Country Country { get; set; }

        public string State_Name { get; set; }

        public int SortOrder { get; set; }

        public string Comments { get; set; }

        public bool isActive { get; set; }

    }
}
