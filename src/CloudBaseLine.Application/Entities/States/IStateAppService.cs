﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.States.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.States
{
    public interface IStateAppService : IAsyncCrudAppService<StateDto,long,PagedResultRequestDto,StateDto,StateDto>
    {

    }
}
