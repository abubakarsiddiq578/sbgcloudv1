﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Countries.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Countries
{
    public class CountryAppService : AsyncCrudAppService<Country,CountryDto, long, PagedResultRequestDto, CountryDto, CountryDto> , ICountryAppService
    {

        private readonly IRepository<Country, long> CountryRepository;
        private readonly IPermissionManager _permissionManager;


        public CountryAppService(IRepository<Country, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            CountryRepository = _repository;

            _permissionManager = _Manager;

        }


      
        public async Task<List<CountryDto>> GetAllCountries()
        {

            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var a = CountryRepository.GetAll().Where(b => b.IsDeleted == false && b.TenantId == TenantId).ToList();

                return a.MapTo<List<CountryDto>>();
            }

            catch (Exception ex)
            {
                throw ex;

            }


        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Countries_Create)]
        public override async Task<CountryDto> Create(CountryDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<Country>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await CountryRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<CountryDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Countries_Edit)]
        public override async Task<CountryDto> Update(CountryDto input)
        {
            var data = ObjectMapper.Map<Country>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await CountryRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CountryDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Countries_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = CountryRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId ).FirstOrDefault();
            if (result != null)
            {
                await CountryRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }


    }
}
