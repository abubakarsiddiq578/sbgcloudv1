﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Countries.Dto
{
    [AutoMapTo(typeof(Country)), AutoMapFrom(typeof(CountryDto))]
    public class CountryDto : EntityDto<long>
    {
        public string Name { get; set; }

        public int TenantId { get; set; }
    }
}
