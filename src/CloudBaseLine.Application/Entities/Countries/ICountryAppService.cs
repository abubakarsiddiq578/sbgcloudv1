﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Countries.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Countries
{
    public interface ICountryAppService : IAsyncCrudAppService<CountryDto, long, PagedResultRequestDto, CountryDto, CountryDto>
    {

    }
}
