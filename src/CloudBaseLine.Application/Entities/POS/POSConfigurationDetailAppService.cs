﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using CloudBaseLine.Entities.POS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.POS
{ 
    public class POSConfigurationDetailAppService : AsyncCrudAppService<POSConfigurationDetail, POSConfigurationDetailDto, long, PagedResultRequestDto, POSConfigurationDetailDto, POSConfigurationDetailDto>, IPOSConfigurationDetailAppService
    {
        private readonly IRepository<POSConfigurationDetail, long> _PosConfigurationDetailRepository;
        private readonly IPermissionManager _permissionManager;

        public POSConfigurationDetailAppService(IRepository<POSConfigurationDetail, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _PosConfigurationDetailRepository = _repository;
            _permissionManager = _Manager;
        }
    }
}
