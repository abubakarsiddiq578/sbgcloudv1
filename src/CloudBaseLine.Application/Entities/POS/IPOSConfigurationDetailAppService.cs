﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.POS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.POS
{
    public interface IPOSConfigurationDetailAppService : IAsyncCrudAppService<POSConfigurationDetailDto, long, PagedResultRequestDto, POSConfigurationDetailDto, POSConfigurationDetailDto>
    {
    }
}
 