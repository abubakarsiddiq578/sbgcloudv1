﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CloudBaseLine.Entities.POS.Dto
{

    
    [AutoMapTo(typeof(POSConfiguration)), AutoMapFrom(typeof(POSConfigurationVMDto))]
    public class POSConfigurationVMDto 
    {
        public string POSTitle { get; set; }
        
        public long CompanyInfoId { get; set; }

        public long LocationInfoId { get; set; }
        

        public long CostCenterId { get; set; }
        
        
        public int CashAccountId { get; set; }
        
        public bool DeliveryOption { get; set; }

        public int BankAccountId { get; set; }
        
        public long EmployeeId { get; set; }
        

        public ICollection<POSConfigurationDetail> POSConfigurationDetails { get; set; }

        public POSConfigurationVMDto()
        {
            POSConfigurationDetails = new Collection<POSConfigurationDetail>();
        }
    }
}
