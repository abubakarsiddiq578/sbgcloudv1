﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CloudBaseLine.Entities.POS.Dto
{
    [AutoMapTo(typeof(POSConfiguration)), AutoMapFrom(typeof(POSConfigurationDto))]
    public class POSConfigurationDto : EntityDto<long>
    {
        public string POSTitle { get; set; }


        public string MachineName { get; set; }
         
        public long CompanyInfoId { get; set; }
        


        public long LocationInfoId { get; set; }
        
        public long CostCenterId { get; set; }
        
        public int CashAccountId { get; set; }

        public bool DeliveryOption { get; set; }

        public int BankAccountId { get; set; }


        public long EmployeeId { get; set; }

        
        public bool IsActive { get; set; }

        public IEnumerable<POSConfigurationDetail> POSConfigurationDetails { get; set; }

        public int TenantId { get; set; }
    }
}
