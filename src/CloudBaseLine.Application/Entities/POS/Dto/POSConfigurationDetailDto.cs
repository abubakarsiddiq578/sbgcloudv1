﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
 
namespace CloudBaseLine.Entities.POS.Dto
{
    [AutoMapTo(typeof(POSConfigurationDetail)), AutoMapFrom(typeof(POSConfigurationDetailDto))]
    public class POSConfigurationDetailDto :EntityDto<long>
    {

        public string MachineTitle { get; set; }

        public int BankAccountId { get; set; }

        public long POSConfigurationId { get; set; }

        //[ForeignKey("POSConfiguration")]
        //public long POSConfigurationId { get; set; }

        //public POSConfiguration POSConfiguration { get; set; }

        public int TenantId { get; set; }
    }
}
