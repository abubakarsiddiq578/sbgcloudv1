﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.LocationInformaiton.Dto;
using CloudBaseLine.Entities.POS.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.POS
{ 
    public class POSAppService : AsyncCrudAppService<POSConfiguration, POSConfigurationDto, long, PagedResultRequestDto, POSConfigurationDto, POSConfigurationDto>, IPOSAppService
    {
        private readonly IRepository<POSConfiguration, long> _PosConfigurationRepository;
        private readonly IRepository<POSConfigurationDetail, long> _posConfigurationDetailRepositoty;
        private readonly IPermissionManager _permissionManager;

        public POSAppService(IRepository<POSConfiguration, long> _repository, IPermissionManager _Manager, IRepository<POSConfigurationDetail, long> _detailRepository) : base(_repository)
        {
            _PosConfigurationRepository = _repository;
            _posConfigurationDetailRepositoty = _detailRepository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_POSs_Create)]
        public override async Task<POSConfigurationDto> Create(POSConfigurationDto input)
        {
            try
            {
                var result = ObjectMapper.Map<POSConfiguration>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);

                var response = await _PosConfigurationRepository.InsertAsync(result);
                //CurrentUnitOfWork.SaveChanges();

                foreach (var i in input.POSConfigurationDetails)
                {
                    i.POSConfigurationId = response.Id;
                    await _posConfigurationDetailRepositoty.InsertAsync(i);
                }
                CurrentUnitOfWork.SaveChanges();
                     
                var data = result.MapTo<POSConfigurationDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //Authorization with Permissions
        //[AbpAuthorize(PermissionNames.Pages_Hrm_POSs_Edit)]

        //public override async Task<LocationInfoDto> Update(LocationInfoDto input)
        //{
        //    var data = ObjectMapper.Map<LocationInfo>(input);
        //    data.LastModificationTime = DateTime.Now;
        //    data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);


        //    await _LocationInfoRepository.UpdateAsync(data);
        //    CurrentUnitOfWork.SaveChanges();

        //    var result = data.MapTo<LocationInfoDto>();
        //    return result;
        //}


        //Authorization with Permissions
        //[AbpAuthorize(PermissionNames.Pages_Hrm_POSs_Delete)]
        
        //public override async Task Delete(EntityDto<long> input)
        //{
        //    var result = _LocationInfoRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
        //    if (result != null)
        //    {
        //        await _LocationInfoRepository.DeleteAsync(result);
        //        CurrentUnitOfWork.SaveChanges();
        //    }

        //}

    }
}
