﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Achievements.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Achievements
{
    public class AchievementAppService : AsyncCrudAppService<Achievement, AchievementDto, long, PagedResultRequestDto, AchievementDto, AchievementDto>, IAchievementAppService
    {

        private readonly IRepository<Achievement, long> AchievementRepository;
        private readonly IPermissionManager _permissionManager;


        public AchievementAppService(IRepository<Achievement, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            AchievementRepository = _repository;
            _permissionManager = _Manager;
        }

        [AbpAuthorize(PermissionNames.Pages_Hrm_Achievements_View)]
        public async Task<List<AchievementDto>> GetAllAchievements()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                //will get all the record based on logged in user's tenant id.
                var result = AchievementRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.Employee).ToList();
                return result.MapTo<List<AchievementDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [AbpAuthorize(PermissionNames.Pages_Hrm_Achievements_Create)]
        public override async Task<AchievementDto> Create(AchievementDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }
            
            var result = ObjectMapper.Map<Achievement>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await AchievementRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<AchievementDto>();
            return data;
        }

        [AbpAuthorize(PermissionNames.Pages_Hrm_Achievements_Edit)]
        public override async Task<AchievementDto> Update(AchievementDto input)
        {
            var data = ObjectMapper.Map<Achievement>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await AchievementRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<AchievementDto>();
            return result;
        }

        [AbpAuthorize(PermissionNames.Pages_Hrm_Achievements_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {

            var TenantId = AbpSession.TenantId;

            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = AchievementRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await AchievementRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }







    }
}
