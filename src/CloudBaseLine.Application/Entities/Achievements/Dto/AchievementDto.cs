﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Achievements.Dto
{
    [AutoMapTo(typeof(Achievement)), AutoMapFrom(typeof(AchievementDto))]
    public class AchievementDto : EntityDto<long>
    {
        public string DocumentNo { get; set; }


        public DateTime DocumentDate { get; set; }
        
        public long EmployeeId { get; set; }
        public  Employee Employee { get; set; }


        public DateTime AchievementDate { get; set; }

        public string AchievementType { get; set; }

        public string AchievementDetail { get; set; }

        public int TenantId { get; set; }

    }
}
