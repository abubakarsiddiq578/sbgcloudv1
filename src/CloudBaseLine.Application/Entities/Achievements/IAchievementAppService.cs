﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Achievements.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Achievements
{
    public interface IAchievementAppService :  IAsyncCrudAppService<AchievementDto, long, PagedResultRequestDto, AchievementDto, AchievementDto>
    {

    }
}
