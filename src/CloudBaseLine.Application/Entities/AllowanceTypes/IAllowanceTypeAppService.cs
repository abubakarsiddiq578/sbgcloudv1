﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Allowances.Dto;
using CloudBaseLine.Entities.AllowanceTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.AllowanceTypes
{
    public interface IAllowanceTypeAppService : IAsyncCrudAppService<AllowanceTypeDto, long, PagedResultRequestDto, AllowanceTypeDto, AllowanceTypeDto>
    {
        Task<List<AllowanceTypeDto>> GetAllAllowanceTypes();
    }
}
