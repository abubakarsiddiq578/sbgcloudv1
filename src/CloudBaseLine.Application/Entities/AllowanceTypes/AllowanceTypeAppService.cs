﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.AllowanceTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.AllowanceTypes
{
    public class AllowanceTypeAppService : AsyncCrudAppService<AllowanceType, AllowanceTypeDto, long, PagedResultRequestDto, AllowanceTypeDto, AllowanceTypeDto>, IAllowanceTypeAppService
    {

        private readonly IRepository<AllowanceType, long> _AllowanceTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public AllowanceTypeAppService(IRepository<AllowanceType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _AllowanceTypeRepository = _repository;
            _permissionManager = _Manager;
        }




        public async Task<List<AllowanceTypeDto>> GetAllAllowanceTypes()
        {
            try
            {

                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _AllowanceTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<AllowanceTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AllowanceTypes_Create)]
        public override async Task<AllowanceTypeDto> Create(AllowanceTypeDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }

                var result = ObjectMapper.Map<AllowanceType>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _AllowanceTypeRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<AllowanceTypeDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AllowanceTypes_Edit)]
        public override async Task<AllowanceTypeDto> Update(AllowanceTypeDto input)
        {
            var data = ObjectMapper.Map<AllowanceType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _AllowanceTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<AllowanceTypeDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_AllowanceTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }

            var result = _AllowanceTypeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId ).FirstOrDefault();
            if (result != null)
            {
                await _AllowanceTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
    }
}
