﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.AllowanceTypes.Dto
{
    [AutoMapTo(typeof(AllowanceType)), AutoMapFrom(typeof(AllowanceTypeDto))]
    public class AllowanceTypeDto : EntityDto<long>
    {
        public string AllowanceTitle { get; set; }

        public long AllowanceAmmount { get; set; }

        public string AllowanceDetail { get; set; }

        public int SortOrder { get; set; }

        public int TenantId { get; set; }
    }
}
