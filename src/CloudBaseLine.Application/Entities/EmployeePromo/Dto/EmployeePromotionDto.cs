﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeePromo.Dto
{
    [AutoMapTo(typeof(EmployeePromotion)), AutoMapFrom(typeof(EmployeePromotionDto))]
    public class EmployeePromotionDto : EntityDto<long>
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public DateTime PromotionDate { get; set; }

        public string PromotionType { get; set; }

        public long PromotionFrom { get; set; }

        public virtual EmployeeDesignation ProFrom { get; set; }

        
        public long PromotionTo { get; set; }

        public virtual EmployeeDesignation ProTo { get; set; }

        public string Detail { get; set; }

        public int TenantId { get; set; }
    }
}
