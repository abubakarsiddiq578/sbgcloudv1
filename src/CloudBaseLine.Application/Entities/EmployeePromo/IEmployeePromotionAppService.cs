﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.EmployeePromo.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.EmployeePromo
{
    public interface IEmployeePromotionAppService : IAsyncCrudAppService<EmployeePromotionDto, long, PagedResultRequestDto, EmployeePromotionDto, EmployeePromotionDto>
    {
    }
}
