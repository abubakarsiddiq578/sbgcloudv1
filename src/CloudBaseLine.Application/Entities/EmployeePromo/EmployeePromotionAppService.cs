﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.EmployeePromo;
using CloudBaseLine.Entities.EmployeePromo.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.EmployeePromotionPromo
{
    public class EmployeePromotionAppService : AsyncCrudAppService<EmployeePromotion, EmployeePromotionDto, long, PagedResultRequestDto, EmployeePromotionDto, EmployeePromotionDto>, IEmployeePromotionAppService
    {
        private readonly IRepository<EmployeePromotion, long> _EmployeePromotionRepository;
        private readonly IPermissionManager _permissionManager;

        public EmployeePromotionAppService(IRepository<EmployeePromotion, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _EmployeePromotionRepository = _repository;
            _permissionManager = _Manager;
        }

        /// <summary>
        /// GetAllEmployeePromotions will get all the EmployeePromotions
        /// </summary>
        /// <returns></returns>
        /// 
      
        public async Task<List<EmployeePromotionDto>> GetAllEmployeePromotions()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _EmployeePromotionRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).Include(a => a.Employee).Include(p => p.ProFrom).Include(f => f.ProTo).ToList();
                return result.MapTo<List<EmployeePromotionDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This Create function will add a new EmployeePromotion.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
         //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeePromos_Create)]
        public override async Task<EmployeePromotionDto> Create(EmployeePromotionDto input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ObjectMapper.Map<EmployeePromotion>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                result.TenantId = Convert.ToInt32(TenantId);
                await _EmployeePromotionRepository.InsertAsync(result);


                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<EmployeePromotionDto>();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This update function will update the specific record according to given id.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
         //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeePromos_Edit)]
        public override async Task<EmployeePromotionDto> Update(EmployeePromotionDto input)
        {
            try
            {
                var data = ObjectMapper.Map<EmployeePromotion>(input);
                data.LastModificationTime = DateTime.Now;
                data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

                await _EmployeePromotionRepository.UpdateAsync(data);
                CurrentUnitOfWork.SaveChanges();

                var result = data.MapTo<EmployeePromotionDto>();
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// This Delete function will delete the specific record according to given number id.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
         //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_EmployeePromos_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _EmployeePromotionRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
                if (result != null)
                {
                    await _EmployeePromotionRepository.DeleteAsync(result);
                    CurrentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
