﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.ChartsOfAccountMain.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ChartsOfAccountMain
{
    public class ChartOfAccountMainAppService : AsyncCrudAppService<ChartOfAccountMain, ChartOfAccountMainDto, long, PagedResultRequestDto, ChartOfAccountMainDto, ChartOfAccountMainDto>, IChartOfAccountMainAppService
    {
        private readonly IRepository<ChartOfAccountMain, long> _ChartOfAccountMainRepository;
        private readonly IPermissionManager _permissionManager;

        public ChartOfAccountMainAppService(IRepository<ChartOfAccountMain, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ChartOfAccountMainRepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountMains_Create)]
        public override async Task<ChartOfAccountMainDto> Create(ChartOfAccountMainDto input)
        {
            var result = ObjectMapper.Map<ChartOfAccountMain>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await _ChartOfAccountMainRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();
            var data = result.MapTo<ChartOfAccountMainDto>();
            return data;

        }
        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountMains_Edit)]
        public override async Task<ChartOfAccountMainDto> Update(ChartOfAccountMainDto input)
        {
            var data = ObjectMapper.Map<ChartOfAccountMain>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _ChartOfAccountMainRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ChartOfAccountMainDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountMains_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _ChartOfAccountMainRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _ChartOfAccountMainRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
