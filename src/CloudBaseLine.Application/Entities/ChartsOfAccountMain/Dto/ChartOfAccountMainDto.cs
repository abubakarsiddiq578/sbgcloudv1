﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ChartsOfAccountMain.Dto
{
    [AutoMapTo(typeof(ChartOfAccountMain)), AutoMapFrom(typeof(ChartOfAccountMainDto))]
    public class ChartOfAccountMainDto : EntityDto<long>
    {
        public string Main_code { get; set; }
        public string Main_Title { get; set; }
        public string Main_Type { get; set; }
        public int TenantId { get; set; }
    }
}
