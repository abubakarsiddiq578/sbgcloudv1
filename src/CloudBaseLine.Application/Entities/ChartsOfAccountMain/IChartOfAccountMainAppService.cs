﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ChartsOfAccountMain.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ChartsOfAccountMain
{
    public interface IChartOfAccountMainAppService : IAsyncCrudAppService<ChartOfAccountMainDto, long, PagedResultRequestDto, ChartOfAccountMainDto, ChartOfAccountMainDto>
    {
    }
}
