﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.ItemTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ItemTypes
{
    public class ItemTypeAppService: AsyncCrudAppService<ItemType,ItemTypeDto, long, PagedResultRequestDto, ItemTypeDto, ItemTypeDto>,IItemTypeAppService
    {

        private readonly IRepository<ItemType, long> ItemTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public ItemTypeAppService(IRepository<ItemType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {

            ItemTypeRepository = _repository;

            _permissionManager = _Manager;

        }

        public async Task<List<ItemTypeDto>> GetAllItemTypes()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = ItemTypeRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId).ToList();
                return result.MapTo<List<ItemTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ItemTypes_Create)]
        public override async Task<ItemTypeDto> Create(ItemTypeDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<ItemType>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await ItemTypeRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<ItemTypeDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ItemTypes_Edit)]
        public override async Task<ItemTypeDto> Update(ItemTypeDto input)
        {
            var data = ObjectMapper.Map<ItemType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await ItemTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ItemTypeDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ItemTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ItemTypeRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await ItemTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
