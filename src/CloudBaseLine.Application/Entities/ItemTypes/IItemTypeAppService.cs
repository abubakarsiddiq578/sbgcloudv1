﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ItemTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ItemTypes
{
    public interface IItemTypeAppService : IAsyncCrudAppService<ItemTypeDto, long, PagedResultRequestDto, ItemTypeDto, ItemTypeDto>
    {
    }
}
