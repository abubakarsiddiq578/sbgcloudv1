﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Deductions.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Deductions
{
    public interface IDeductionAppService : IAsyncCrudAppService<DeductionDto, long, PagedResultRequestDto, DeductionDto, DeductionDto>
    {
    }
}
