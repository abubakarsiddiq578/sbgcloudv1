﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore.Extensions;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Deductions.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Deductions
{
    public class DeductionAppService : AsyncCrudAppService<Deduction, DeductionDto, long, PagedResultRequestDto, DeductionDto, DeductionDto>, IDeductionAppService
    {
        private readonly IRepository<Deduction, long> _DeductionRepository;
        private readonly IPermissionManager _permissionManager;

        public DeductionAppService(IRepository<Deduction, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _DeductionRepository = _repository;
            _permissionManager = _Manager;
        }


    
        public async Task<List<DeductionDto>> GetAllDeductions()
        {
            try
            {
                //get logged in user's tenant id.
                var TenantId = AbpSession.TenantId;
                if (TenantId == null)
                {
                    TenantId = 0;
                }
                var result = _DeductionRepository.GetAll().Where(a => a.IsDeleted == false && a.TenantId == TenantId ).Include(d => d.DeductionType).Include(e => e.Employee).ToList();
                return result.MapTo<List<DeductionDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Deductions_Create)]
        public override async Task<DeductionDto> Create(DeductionDto input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<Deduction>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(TenantId);
            await _DeductionRepository.InsertAsync(result);

            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<DeductionDto>();
            return data;

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Deductions_Edit)]
        public override async Task<DeductionDto> Update(DeductionDto input)
        {
            var data = ObjectMapper.Map<Deduction>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _DeductionRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<DeductionDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Deductions_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = _DeductionRepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await _DeductionRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
