﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Deductions.Dto
{
    [AutoMapTo(typeof(Deduction)), AutoMapFrom(typeof(DeductionDto))]
    public class DeductionDto : EntityDto<long>
    {
        public string DocNo { get; set; }

        public DateTime DocDate { get; set; }

        public DateTime DeductionDate { get; set; }

        public long EmployeeId { get; set; }

        public long DeductionTypeId { get; set; }

        public long Amount { get; set; }
        
        public string DeductionDetail { get; set; }

        public int TenantId { get; set; }
    }
}
