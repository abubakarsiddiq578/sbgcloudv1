﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.VoucherTypes.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.VoucherTypes
{
    public interface IVoucherTypeAppService : IAsyncCrudAppService<VoucherTypeDto, long, PagedResultRequestDto, VoucherTypeDto, VoucherTypeDto>
    {
        Task<List<VoucherTypeDto>> GetAllVoucherTypes();
    }
}
