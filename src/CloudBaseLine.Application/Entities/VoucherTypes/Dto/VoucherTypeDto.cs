﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Authorization.Users;

namespace CloudBaseLine.Entities.VoucherTypes.Dto
{
    [AutoMapTo(typeof(VoucherType)), AutoMapFrom(typeof(VoucherTypeDto))]
    public class VoucherTypeDto : EntityDto<long>
    {
        public string Voucher_Type_Name { get; set; }
        public string voucher_type { get; set; }
        public string comments { get; set; }
        public string GL_type { get; set; }
        public int sort_order { get; set; }
        public bool read_only { get; set; }
        public int TenantId { get; set; }
    }
}
