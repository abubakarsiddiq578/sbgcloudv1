﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.VoucherTypes.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.VoucherTypes
{
    public class VoucherTypeAppService: AsyncCrudAppService<VoucherType, VoucherTypeDto, long, PagedResultRequestDto, VoucherTypeDto, VoucherTypeDto>, IVoucherTypeAppService
    {
        private readonly IRepository<VoucherType, long> voucherTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public VoucherTypeAppService(IRepository<VoucherType, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            voucherTypeRepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_VoucherTypes_Create)]
        public override async Task<VoucherTypeDto> Create(VoucherTypeDto input)
        {
            var result = ObjectMapper.Map<VoucherType>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await voucherTypeRepository.InsertAsync(result);

            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<VoucherTypeDto>();
            return data;

        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_VoucherTypes_Edit)]
        public override async Task<VoucherTypeDto> Update(VoucherTypeDto input)
        {
            var data = ObjectMapper.Map<VoucherType>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await voucherTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<VoucherTypeDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_VoucherTypes_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = voucherTypeRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await voucherTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<VoucherTypeDto>> GetAllVoucherTypes()
        {
            try
            {
                var result = voucherTypeRepository.GetAll().OrderBy(x => x.sort_order).ToList();
                return result.MapTo<List<VoucherTypeDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
