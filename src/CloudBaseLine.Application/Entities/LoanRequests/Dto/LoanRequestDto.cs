﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.LoanRequests.Dto
{
    [AutoMapTo(typeof(LoanRequest)), AutoMapFrom(typeof(LoanRequestDto))]
    public class LoanRequestDto : EntityDto<long>
    {
        public string DocNo { get; set; }
        public DateTime DocDate { get; set; }
        public long EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public double LoadAmount { get; set; }
        public double MonthlyRepayementAmount { get; set; }
        public DateTime RepayementStartDate { get; set; }
        public string Detail { get; set; }
        public int TenantId { get; set; }
    }
}
