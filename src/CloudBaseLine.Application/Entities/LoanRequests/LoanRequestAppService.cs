﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.LoanRequests.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.LoanRequests
{
    public class LoanRequestAppService : AsyncCrudAppService<LoanRequest, LoanRequestDto, long, PagedResultRequestDto, LoanRequestDto, LoanRequestDto>, ILoanRequestAppService
    {
        private readonly IRepository<LoanRequest, long> LoanRequestrepository;
        private readonly IPermissionManager _permissionManager;

        public LoanRequestAppService(IRepository<LoanRequest, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            LoanRequestrepository = _repository;
            _permissionManager = _Manager;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LoanRequests_Create)]
        public override async Task<LoanRequestDto> Create(LoanRequestDto input)
        {

            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = ObjectMapper.Map<LoanRequest>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            result.TenantId = Convert.ToInt32(TenantId);
            await LoanRequestrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<LoanRequestDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LoanRequests_Edit)]
        public override async Task<LoanRequestDto> Update(LoanRequestDto input)
        {
            var data = ObjectMapper.Map<LoanRequest>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await LoanRequestrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<LoanRequestDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_LoanRequests_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = LoanRequestrepository.GetAll().Where(x => x.Id == input.Id && x.TenantId == TenantId).FirstOrDefault();
            if (result != null)
            {
                await LoanRequestrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<LoanRequestDto>> GetLoanRequest()
        {
            //get logged in user's tenant id.
            var TenantId = AbpSession.TenantId;
            if (TenantId == null)
            {
                TenantId = 0;
            }
            var result = LoanRequestrepository.GetAll().Where(x => x.IsDeleted == false && x.TenantId == TenantId).Include(b => b.Employee).ToList();
            return result.MapTo<List<LoanRequestDto>>();
        }
    }
}
