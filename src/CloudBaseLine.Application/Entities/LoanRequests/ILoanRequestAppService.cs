﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.LoanRequests.Dto;

namespace CloudBaseLine.Entities.LoanRequests
{
    public interface ILoanRequestAppService : IAsyncCrudAppService<LoanRequestDto, long, PagedResultRequestDto, LoanRequestDto, LoanRequestDto>
    {
        Task<List<LoanRequestDto>> GetLoanRequest();
    }
}
