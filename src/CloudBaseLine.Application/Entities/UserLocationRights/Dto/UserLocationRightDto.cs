﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using CloudBaseLine.Authorization.Users;

namespace CloudBaseLine.Entities.UserLocationRights.Dto
{
    [AutoMapTo(typeof(UserLocationRight)), AutoMapFrom(typeof(UserLocationRightDto))]
    public class UserLocationRightDto : EntityDto<long>
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public long LocationId { get; set; }
        public LocationInfo LocationInfo { get; set; }
        public bool Rights { get; set; }
        public int TenantId { get; set; }
    }
}
