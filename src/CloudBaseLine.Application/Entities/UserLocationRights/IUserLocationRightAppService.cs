﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.UserLocationRights.Dto;

namespace CloudBaseLine.Entities.UserLocationRights
{
    public interface IUserLocationRightAppService : IAsyncCrudAppService<UserLocationRightDto, long, PagedResultRequestDto, UserLocationRightDto, UserLocationRightDto>
    {
        Task<List<UserLocationRightDto>> GetLocationRights();
    }
}
