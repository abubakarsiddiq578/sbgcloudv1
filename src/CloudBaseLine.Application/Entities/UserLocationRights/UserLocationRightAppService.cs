﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.UserLocationRights.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.UserLocationRights
{
    public class UserLocationRightAppService : AsyncCrudAppService<UserLocationRight, UserLocationRightDto, long, PagedResultRequestDto, UserLocationRightDto, UserLocationRightDto>, IUserLocationRightAppService
    {
        private readonly IRepository<UserLocationRight, long> _UserLocationRightrepository;
        private readonly IPermissionManager _permissionManager;

        public UserLocationRightAppService(IRepository<UserLocationRight, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _UserLocationRightrepository = _repository;
            _permissionManager = _Manager;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserLocationRights_Create)]
        public override async Task<UserLocationRightDto> Create(UserLocationRightDto input)
        {
            var result = ObjectMapper.Map<UserLocationRight>(input);
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _UserLocationRightrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserLocationRightDto>();
            return data;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserLocationRights_Edit)]
        public override async Task<UserLocationRightDto> Update(UserLocationRightDto input)
        {
            var data = ObjectMapper.Map<UserLocationRight>(input);
            data.CreationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _UserLocationRightrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserLocationRightDto>();
            return result;
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_UserLocationRights_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _UserLocationRightrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _UserLocationRightrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public async Task<List<UserLocationRightDto>> GetLocationRights()
        {
            var result = _UserLocationRightrepository.GetAll().Include(a => a.LocationInfo).Include(b => b.User).ToList();
            return result.MapTo<List<UserLocationRightDto>>();
        }
    }
}
