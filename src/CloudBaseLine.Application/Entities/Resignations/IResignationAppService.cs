﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.Resignations.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Resignations
{
    public interface IResignationAppService : IAsyncCrudAppService<ResignationDto, long, PagedResultRequestDto, ResignationDto, ResignationDto>
    {
       // Task<List<ResignationDto>> GetAllResignation();


    }
}
