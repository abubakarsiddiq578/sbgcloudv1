﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.Resignations.Dto
{
    [AutoMapTo(typeof(Resignation)), AutoMapFrom(typeof(ResignationDto))]
    public class ResignationDto : EntityDto<long>
    {

        public string DocumentNo { get; set; }


        public DateTime DocumentDate { get; set; }


        public long EmployeeId { get; set; }
        public  Employee Employee { get; set; }


        public DateTime NoticeDate { get; set; }

        public DateTime ResignationDate { get; set; }


        public string ResignationReason { get; set; }

        public int TenantId { get; set; }
    }
}
