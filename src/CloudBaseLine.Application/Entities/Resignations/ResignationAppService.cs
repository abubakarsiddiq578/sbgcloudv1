﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.Resignations.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.Resignations
{
    public class ResignationAppService : AsyncCrudAppService<Resignation, ResignationDto, long, PagedResultRequestDto, ResignationDto, ResignationDto>, IResignationAppService
    {


        private readonly IRepository<Resignation, long> ResignationRepository;
        private readonly IPermissionManager _permissionManager;

        public ResignationAppService(IRepository<Resignation, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            ResignationRepository = _repository;
            _permissionManager = _Manager;
        }


        public async Task<List<ResignationDto>> GetAllResignations()
        {
            try
            {
                var result = ResignationRepository.GetAll().Where(a => a.IsDeleted == false).Include(a => a.Employee).ToList();


                return result.MapTo<List<ResignationDto>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

          //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Resignations_Create)]
        public override async Task<ResignationDto> Create(ResignationDto input)
        {
            var result = ObjectMapper.Map<Resignation>(input);
            result.LastModificationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            await ResignationRepository.InsertAsync(result);


            try
            {
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }


            var data = result.MapTo<ResignationDto>();
            return data;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Resignations_Edit)]
        public override async Task<ResignationDto> Update(ResignationDto input)
        {
            var data = ObjectMapper.Map<Resignation>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await ResignationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ResignationDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_Resignations_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = ResignationRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await ResignationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }
        }



    }
}
