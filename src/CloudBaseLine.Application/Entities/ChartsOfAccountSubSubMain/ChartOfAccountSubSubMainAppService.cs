﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using CloudBaseLine.Authorization;
using CloudBaseLine.Entities.ChartsOfAccountSubSubMain.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudBaseLine.Entities.ChartsOfAccountSubSubMain
{
    public class ChartOfAccountSubSubMainAppService : AsyncCrudAppService<ChartOfAccountSubSubMain, ChartOfAccountSubSubMainDto, long, PagedResultRequestDto, ChartOfAccountSubSubMainDto, ChartOfAccountSubSubMainDto>, IChartOfAccountSubSubMainAppService
    {

        private readonly IRepository<ChartOfAccountSubSubMain, long> _ChartOfAccountSubSubMainRepository;
        private readonly IPermissionManager _permissionManager;

        public ChartOfAccountSubSubMainAppService(IRepository<ChartOfAccountSubSubMain, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ChartOfAccountSubSubMainRepository = _repository;
            _permissionManager = _Manager;
        }

      
        public async Task<List<ChartOfAccountSubSubMainDto>> GetAllAccount()
        {

            var result = _ChartOfAccountSubSubMainRepository.GetAll().Include(a => a.ChartOfAccountSubMain).Include(c => c.CompanyInfo).Include(g => g.GLNote).Include(p => p.PLNote).ToList();
            return result.MapTo<List<ChartOfAccountSubSubMainDto>>();
            
        }


        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubSubMains_Create)]
        public override async Task<ChartOfAccountSubSubMainDto> Create(ChartOfAccountSubSubMainDto input)
        {
            try
            {


                var result = ObjectMapper.Map<ChartOfAccountSubSubMain>(input);
                result.LastModificationTime = DateTime.Now;
                result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                await _ChartOfAccountSubSubMainRepository.InsertAsync(result);
                CurrentUnitOfWork.SaveChanges();
                var data = result.MapTo<ChartOfAccountSubSubMainDto>();
                return data;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubSubMains_Edit)]
        public override async Task<ChartOfAccountSubSubMainDto> Update(ChartOfAccountSubSubMainDto input)
        {
            var data = ObjectMapper.Map<ChartOfAccountSubSubMain>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _ChartOfAccountSubSubMainRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ChartOfAccountSubSubMainDto>();
            return result;
        }

        //Authorization with Permissions
        [AbpAuthorize(PermissionNames.Pages_Hrm_ChartOfAccountSubSubMains_Delete)]
        public override async Task Delete(EntityDto<long> input)
        {
            var result = _ChartOfAccountSubSubMainRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _ChartOfAccountSubSubMainRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

    }
}
