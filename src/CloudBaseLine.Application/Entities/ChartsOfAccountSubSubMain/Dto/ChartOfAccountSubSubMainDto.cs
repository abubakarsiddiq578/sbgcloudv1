﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ChartsOfAccountSubSubMain.Dto
{
    [AutoMapTo(typeof(ChartOfAccountSubSubMain)), AutoMapFrom(typeof(ChartOfAccountSubSubMainDto))]
    public class ChartOfAccountSubSubMainDto:EntityDto<long>
    {
        public long SubMainId { get; set; }
        public ChartOfAccountSubMain ChartOfAccountSubMain { get; set; }

        public string Sub_sub_code { get; set; }

        public string Sub_sub_title { get; set; }

        public string Sub_sub_type { get; set; }

        public long BsNoteId { get; set; }
        public GLNotes PLNote { get; set; }

        public long PlNoteId { get; set; }
        public GLNotes GLNote { get; set; }

        public long CompanyId { get; set; }
        public CompanyInfo CompanyInfo { get; set; }

        public int TenantId { get; set; }
    }
}
