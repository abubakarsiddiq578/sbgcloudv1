﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CloudBaseLine.Entities.ChartsOfAccountSubSubMain.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseLine.Entities.ChartsOfAccountSubSubMain
{
    public interface IChartOfAccountSubSubMainAppService : IAsyncCrudAppService<ChartOfAccountSubSubMainDto, long, PagedResultRequestDto, ChartOfAccountSubSubMainDto, ChartOfAccountSubSubMainDto>
    {
    }
}
