﻿import { TreeChildren } from "Models/TreeChildren";

export class TreeRoot {

   public id: number;
    public name: string;
    public value: string;
    public NodeId: number;
    public IsActive: boolean = true;
    public children: TreeChildren[];
    
}