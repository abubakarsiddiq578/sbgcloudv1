import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { JsonpModule, HttpModule } from '@angular/http';
import { HttpClientModule, HttpResponse } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AbpModule } from '@abp/abp.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';

import { HomeComponent } from '@app/home/home.component';

import { UsersComponent } from '@app/users/users.component';
import { CreateUserComponent } from '@app/users/create-user/create-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleComponent } from '@app/roles/create-role/create-role.component';
import { EditRoleComponent } from './roles/edit-role/edit-role.component';
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantComponent } from './tenants/create-tenant/create-tenant.component';
import { EditTenantComponent } from './tenants/edit-tenant/edit-tenant.component';

import { MaterialInput } from '@shared/directives/material-input.directive';


// end Cost Center Component
import { NgxDatatableModule } from '@swimlane/ngx-datatable';




//for datatables
import { DataTablesModule } from 'angular-datatables';




//start importing DlDateTimePickerDateModule module 
import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
//start importing DlDateTimePickerDateModule module

//start importing ng-pick-datetime module 
import { OwlDateTimeModule , OwlNativeDateTimeModule} from 'ng-pick-datetime';
//start importing ng-pick-datetime module 


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        TenantsComponent,
		CreateTenantComponent,
		EditTenantComponent,
        UsersComponent,
		CreateUserComponent,
		EditUserComponent,
      	RolesComponent,        
		CreateRoleComponent,
		EditRoleComponent,
      




    ],  
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        HttpClientModule,
        JsonpModule,
        ModalModule.forRoot(),
        AbpModule,
        AppRoutingModule,
        ServiceProxyModule,
        SharedModule,
        NgxPaginationModule,
        NgxDatatableModule,
        
        //for datatables
        DataTablesModule,

        ReactiveFormsModule,

        // for DlDateTimePickerDateModule
        DlDateTimePickerDateModule,

        // ng-pick-datetime
        OwlDateTimeModule,
        OwlNativeDateTimeModule,

    ],
    providers: [
        
    ]
})
export class AppModule { }
