﻿
import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { RoleServiceProxy, RoleDto, ListResultDtoOfPermissionDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { TreeChildren } from 'Models/TreeChildren';
import { TreeRoot } from 'Models/TreeRoot';
declare var $: any;

@Component({
    selector: 'edit-role-modal',
    templateUrl: './edit-role.component.html',
    styleUrls:['./edit-role.component.css']
})
export class EditRoleComponent extends AppComponentBase implements OnInit {
    @ViewChild('editRoleModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;
    @ViewChild('jstreearea') jsTree: ElementRef;
    @ViewChild('confirmModal') confirmmodal:ModalDirective;

    active: boolean = false;
    saving: boolean = false;

    permissions: ListResultDtoOfPermissionDto = null;
    role: RoleDto = null;

    rootnode: any[];
    public root1: TreeRoot = new TreeRoot();
    children: TreeChildren = new TreeChildren();

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    constructor(
        injector: Injector,
        private _roleService: RoleServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.role=new RoleDto();
        this._roleService.getAllPermissions()
            .subscribe((permissions: ListResultDtoOfPermissionDto) => {
                this.permissions = permissions;
                console.log(this.permissions);
                this.rootnode = [];
                this.permissions = permissions;
                console.log(this.permissions.items);

                let v = 0;
                this.root1.children = [];

                for (v = 2; v < this.permissions.items.length; v++) {
                    if (this.permissions.items[v].name.indexOf("_") > 0) {

                        this.children = new TreeChildren();
                        this.children.name = this.permissions.items[v].displayName;
                        this.children.value = this.permissions.items[v].name;
                        this.children.NodeId = this.permissions.items[v].id;
                        this.root1.children.push(this.children);
                    }
                    else {
                        if (this.root1.children.length > 0) {
                            this.rootnode.push(this.root1);
                        }

                        this.root1 = new TreeRoot();
                        this.root1.children = [];
                        this.root1.name = this.permissions.items[v].displayName;
                        this.root1.value = this.permissions.items[v].name;
                        this.root1.NodeId = this.permissions.items[v].id;
                    }
                }

                this.rootnode.push(this.root1);
                console.log(this.rootnode);
                this.root1 = new TreeRoot();
            });
    }

    show(id: number): void {
        this._roleService.get(id)
            .finally(() => {
                this.active = true;
                // $(this.jsTree.nativeElement).jstree(
                //         {
                //             "plugins":["checkbox"]
                //         });
                this.confirmmodal.show();
            })
            .subscribe((result: RoleDto) => {
                this.role = result;
            });
    }

    OpenModal()
    {
        this.confirmmodal.hide();
         $(this.jsTree.nativeElement).jstree(
                        {
                            "plugins":["checkbox"]
                        });
                        this.modal.show();

    }

    cancel()
    {
        this.confirmmodal.hide();
        this.role=new RoleDto();
        this.permissions=null;
    }
    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    checkPermission(permissionName: string): boolean {
        if (this.role.permissions.indexOf(permissionName) != -1) {
            return true;
        }
        else {
            return false;
        }
    }

    save(): void {
        var permissions = [];
        var editpermission= $(this.jsTree.nativeElement).jstree(true).get_selected('full',true);
        let j=0;
        for(j=0;j<editpermission.length;j++)
        {
            if(permissions.indexOf(editpermission[j].parent)<0)
            {
                permissions.push(editpermission[j].parent);
            }
            permissions.push(editpermission[j].id);
        }

        this.role.permissions = permissions;
        this.saving = true;
        this._roleService.update(this.role)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
