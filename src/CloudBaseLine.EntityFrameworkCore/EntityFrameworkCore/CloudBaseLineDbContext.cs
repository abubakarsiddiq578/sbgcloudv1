﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using CloudBaseLine.Authorization.Roles;
using CloudBaseLine.Authorization.Users;
using CloudBaseLine.MultiTenancy;
using CloudBaseLine.Entities;
using CloudBaseLine.Entities.SalaryGroup;
using System.Reflection.Emit;
using CloudBaseLine.Entities.LeaveQuotaEntities;
using CloudBaseLine.Entities.EmployeeGraduity;

using CloudBaseLine.Entities.Configuration;


//using CloudBaseLine.Entities.TaxSlabs;
//using CloudBaseLine.Entities.Salary;


using CloudBaseLine.Entities.TaxSlabs;
using CloudBaseLine.Entities.Salary;
using CloudBaseLine.Entities.WorkGroup;
//using CloudBaseLine.Entities.ProductionRate;
using CloudBaseLine.Entities.ItemWiseProduction;

namespace CloudBaseLine.EntityFrameworkCore
{
    public class CloudBaseLineDbContext : AbpZeroDbContext<Tenant, Role, User, CloudBaseLineDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public CloudBaseLineDbContext(DbContextOptions<CloudBaseLineDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);



            ///// <summary>
            ///// foreignkey cascade behaviour control model builder code for POS Configuration tbl and Cost Center 
            ///// tbl : added by abubakar 
            ///// </summary>
            modelBuilder.Entity<POSConfiguration>()
                    .HasOne(e => e.CostCenter)
                    .WithOne()
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasForeignKey<POSConfiguration>(f => f.CostCenterId);


            ///// <summary>
            ///// foreignkey cascade behaviour control model builder code for POS Configuration tbl and Company Info 
            ///// tbl : added by abubakar 
            ///// </summary>
            modelBuilder.Entity<POSConfiguration>()
                    .HasOne(e => e.CompanyInfo)
                    .WithOne()
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasForeignKey<POSConfiguration>(f => f.CompanyInfoId);


            ///// <summary>
            ///// foreignkey cascade behaviour control model builder code for POS Configuration tbl and Location info 
            ///// tbl : added by abubakar 
            ///// </summary>
            modelBuilder.Entity<POSConfiguration>()
                    .HasOne(e => e.LocationInfo)
                    .WithOne()
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasForeignKey<POSConfiguration>(f => f.LocationInfoId);


            ///// <summary>
            ///// foreignkey cascade behaviour control model builder code for POS Configuration tbl and Employee 
            ///// tbl : added by abubakar 
            ///// </summary>
            modelBuilder.Entity<POSConfiguration>()
                   .HasOne(e => e.Employee)
                   .WithOne()
                   .OnDelete(DeleteBehavior.Restrict)
                   .HasForeignKey<POSConfiguration>(f => f.EmployeeId);


            /// <summary>
            /// foreignkey cascade behaviour control model builder code for POSConfiguration tbl 
            /// and POSConfiguratoinDetail tbl : added by abubakar 
            /// </summary>
            /// 
            //modelBuilder.Entity<POSConfigurationDetail>()
            //       .HasOne(e => e.POSConfiguration)
            //       .WithOne()
            //       .OnDelete(DeleteBehavior.Restrict)
            //       .HasForeignKey<POSConfigurationDetail>(f => f.POSConfigurationId);



            /// <summary>
            /// foreignkey cascade behaviour control model builder code for Chart Of Account Main tbl and Chart Of Account 
            /// Sub Main tbl : added by abubakar 
            /// </summary>
            modelBuilder.Entity<ChartOfAccountSubMain>()
                    .HasOne(c => c.ChartOfAccountMain)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);


            //modelBuilder.Entity<POSConfiguration>()
            //        .HasOne(p => p.POSConfigurationDetails)
            //        .WithMany()
            //        .OnDelete(DeleteBehavior.Restrict);


            /// <summary>
            /// foreignkey cascade behaviour control model builder code for Chart Of Account sub sub Main tbl and Chart Of Account 
            /// Sub Main tbl : added by abubakar 
            /// </summary>
            modelBuilder.Entity<ChartOfAccountSubSubMain>()
                    .HasOne(c => c.ChartOfAccountSubMain)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);

            /// <summary>
            /// foreignkey cascade behaviour control model builder code for Chart Of Account sub sub Main tbl and Company Info 
            /// tbl : added by abubakar 
            /// </summary>
            modelBuilder.Entity<ChartOfAccountSubSubMain>()
                    .HasOne(c => c.CompanyInfo)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);

            /// <summary>
            /// foreignkey cascade behaviour control model builder code for Chart Of Account sub sub Main tbl and GL Note(BS)
            /// tbl : added by abubakar 
            /// </summary>
            modelBuilder.Entity<ChartOfAccountSubSubMain>()
                    .HasOne(c => c.GLNote)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);

            /// <summary>
            /// foreignkey cascade behaviour control model builder code for Chart Of Account sub sub Main tbl and GL Note(PL)
            /// tbl : added by abubakar 
            /// </summary>
            modelBuilder.Entity<ChartOfAccountSubSubMain>()
                    .HasOne(c => c.PLNote)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);
            /// <summary>
            /// foreignkey cascade behaviour control model builder code for Chart Of Account sub sub Detail tbl and 
            /// Chart of Account Sub Sub main tbl : added by abubakar 
            /// </summary>
            modelBuilder.Entity<ChartOfAccountSubSubDetail>()
                .HasOne(c => c.ChartOfAccountSubSubMain)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyInfo>()
              .HasOne(c => c.CostCenter)
              .WithMany()
              .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<Deduction>()
                .HasOne(c => c.Employee)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Deduction>()
                .HasOne(c => c.DeductionType)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<AdditionalLeave>()
                .HasOne(c => c.Employee)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<AdditionalLeave>()
            .HasOne(c => c.LeaveType)
            .WithMany()
            .OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<EmpPromotion>()
            //    .HasOne(c => c.Employee)
            //    .WithMany()
            //    .OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<EmpPromotion>()
            //    .HasOne(c => c.proFrom)
            //    .WithMany()
            //    .OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<EmpPromotion>()
            //    .HasOne(c => c.proTo)
            //    .WithMany()
            //    .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<EmployeePromotion>()
                .HasOne(c => c.Employee)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<EmployeePromotion>()
                .HasOne(c => c.ProFrom)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<EmployeePromotion>()
                .HasOne(c => c.ProTo)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Warning>()
                .HasOne(c => c.Employee)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Warning>()
                .HasOne(c => c.Department)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Termination>()
                .HasOne(c => c.Employee)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Transfer>()
                .HasOne(c => c.TransFrom)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Transfer>()
                .HasOne(c => c.TransTo)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Transfer>()
                .HasOne(c => c.Employee)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);


            //////////////////
            ///
            modelBuilder.Entity<State>()
                .HasOne(d=>d.Country)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);



            //EmployeeExit//

            modelBuilder.Entity<EmployeeExit>()
               .HasOne(d => d.Employee)
               .WithMany()
               .OnDelete(DeleteBehavior.Restrict);

            /////////////

            modelBuilder.Entity<LeaveRequest>()
                .HasOne(c => c.Employee)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<LeaveRequest>()
                .HasOne(c => c.LeaveType)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<SalaryGroupDetail>()
            //    .HasOne(c => c.SalaryGroupMater)
            //    .WithMany()
            //    .OnDelete(DeleteBehavior.Restrict);

            ///// <summary>
            ///// foreignkey cascade behaviour control model builder code for POS Configuration tbl and Cost Center 
            ///// tbl : added by abubakar 
            ///// </summary>


            //modelBuilder.Entity<LeaveQuotaDetail>()
            //        .HasOne(e => e.LeaveType)
            //        .WithMany()
            //        .OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<LeaveQuotaDetail>()
            //    .HasOne(c => c.LeaveQuotaMaster)
            //    .WithMany()
            //    .OnDelete(DeleteBehavior.Restrict);
            

            modelBuilder.Entity<Employee>()
                .HasOne(c => c.JobType)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Employee>()
                .HasOne(c => c.Department)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Employee>()
                .HasOne(c => c.EmployeeDesignation)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Employee>()
                .HasOne(c => c.CostCenter)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Employee>()
                .HasOne(c => c.City)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Employee>()
                    .HasOne(c => c.Belt)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Employee>()
                .HasOne(c => c.State)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Employee>()
                    .HasOne(c => c.Zone)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Employee>()
                    .HasOne(c => c.Region)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<WorkGroupMaster>()
                   .HasOne(c => c.ItemType)
                   .WithMany()
                   .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<WorkGroupMaster>()
                   .HasOne(c => c.Category)
                   .WithMany()
                   .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<WorkGroupMaster>()
                   .HasOne(c => c.SubCategory)
                   .WithMany()
                   .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<WorkGroupItemDetail>()
                   .HasOne(c => c.Item)
                   .WithMany()
                   .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<WorkGroupItemDetail>()
                   .HasOne(c => c.WorkGroupMaster)
                   .WithMany()
                   .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<WorkGroupEmployeeDetail>()
                   .HasOne(c => c.WorkGroupMaster)
                   .WithMany()
                   .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<WorkGroupEmployeeDetail>()
                   .HasOne(c => c.Employee)
                   .WithMany()
                   .OnDelete(DeleteBehavior.Restrict);


            //modelBuilder.Entity<LeaveQuotaDetail>()
            //        .HasOne(e => e.LeaveType)
            //        .WithMany()
            //        .OnDelete(DeleteBehavior.Restrict);  

            //modelBuilder.Entity<ProductionRateMaster>()
            //       .HasOne(c => c.Item)
            //       .WithMany()
            //       .OnDelete(DeleteBehavior.Restrict);


        }




        /* protected override void OnModelCreating(ModelBuilder modelBuilder)
         {

             
         }*/



        public DbSet<Sample> Samples { get; set; }

        /// <summary>
        /// Company Info Db Set added by abubakar 
        /// </summary>
        public DbSet<CompanyInfo> CompanyInfos { get; set; }


        /// <summary>
        /// Cost Center db set added by abubakar 
        /// </summary>
        public DbSet<CostCenter> CostCenters { get; set; }


        /// <summary>
        /// location Info db set added by abubakar 
        /// </summary>
        public DbSet<LocationInfo> locationInfos { get; set; }

        /// <summary>
        /// POS Configuration Master tbl db set added by abubakar 
        /// </summary>
        //public DbSet<POSConfiguration> POSConfigurations { get; set; }

        ///// <summary>
        ///// POS Configuration Detail tbl Db Set added by abubakar
        ///// </summary>
        //public DbSet<POSConfigurationDetail> POSConfigurationDetails { get; set; }

        /// <summary>
        /// employee db set added by abubakar
        /// </summary>
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Chart of Account main db set added by abubakar
        /// </summary>
        public DbSet<ChartOfAccountMain> ChartOfAccountMains { get; set; }

        /// <summary>
        /// Chart Of Account Sub Main Db Set Added by Abubakar
        /// </summary>
        public DbSet<ChartOfAccountSubMain> ChartOfAccountSubMains { get; set; }

        /// <summary>
        /// GL Notes Db Set Added by abubakar
        /// </summary>
        public DbSet<GLNotes> GLNotes { get; set; }

        /// <summary>
        /// Chart of account sub sub main tbl Db Set Added by abubakar
        /// </summary>
        public DbSet<ChartOfAccountSubSubMain> ChartOfAccountSubSubMains { get; set; }


        // User Company Rights tbl Db Set Added by saad
        public DbSet<UserCompanyRight> UserCompanyRights { get; set; }

        // User CostCenter Rights tbl Db Set Added by saad
        public DbSet<UserCostCenterRight> UserCostCenterRights { get; set; }

        /// <summary>
        /// Chart of account sub sub Detail tbl Db Set Added by abubakar
        /// </summary>
        public DbSet<ChartOfAccountSubSubDetail> ChartOfAccountSubSubDetails { get; set; }

        /// <summary>
        /// Employee Designation Table Db Set is added by abubakar
        /// </summary>
        //public DbSet<EmployeeDesignation> EmployeeDesignations { get; set; }
        
        // User Location Rights tbl Db Set Added by saad
        public DbSet<UserLocationRight> UserLocationRights { get; set; }

        // VoucherType tbl Db Set Added by saad
        public DbSet<VoucherType> VoucherTypes { get; set; }

        // UserVoucherTypeRight tbl Db Set Added by saad
        public DbSet<UserVoucherTypeRight> UserVoucherTypeRights { get; set; }

        // UserAccountRight tbl Db Set Added by saad
        public DbSet<UserAccountRight> UserAccountRights { get; set; }

        // UserMediaContent tbl Db Set Added by saad
        public DbSet<UserMediaContent> UserMediaContents { get; set; }

        // SalaryExpenseType tbl Db Set Added by saad
        public DbSet<SalaryExpenseType> SalaryExpenseTypes { get; set; }


        // POSConfiguration tbl Db Set Added by Sohaib
        public DbSet<POSConfiguration> POSConfigurations { get; set; }

        // LoanRequest tbl Db Set Added by saad
        public DbSet<LoanRequest> LoanRequests { get; set; }

        /// <summary>
        /// Leave Types table Db set added by abubakar 
        /// </summary>
        public DbSet<LeaveType> LeaveTypes { get; set; }

        /// <summary>
        /// POS Configuration Details Table Db Set is added by abubakar
        /// </summary>
        public DbSet<POSConfigurationDetail> POSConfigurationDetails { get; set; }

        /// <summary>
        /// Employee Designation Table Db Set is added by abubakar
        /// </summary>
        public DbSet<EmployeeDesignation> EmployeeDesignations { get; set; }

         /// <summary>
         /// Job type table db set added by abubakar
         /// </summary>
        public DbSet<JobType> JobTypes { get; set; }

        /// <summary>
        /// Deduction Types table Db set added by abubakar 
        /// </summary>
        public DbSet<DeductionType> DeductionTypes { get; set; }

        /// <summary>
        /// Fine Types table Db set added by abubakar 
        /// </summary>
        public DbSet<FineType> FineTypes { get; set; }

        /// <summary>
        /// Bonus Types table Db set added by abubakar 
        /// </summary>
        public DbSet<BonusType> BonusTypes { get; set; }


        /// <summary>
        /// Allowance Types table Db set added by abubakar 
        /// </summary>
        public DbSet<AllowanceType> AllowanceTypes { get; set; }

        /// <summary>

        /// Allowance table Db set added by saad 
        /// </summary>
        public DbSet<Allowance> Allowances { get; set; }

        /// <summary>
        /// Fine Types table Db set added by saad 
        /// </summary>
        public DbSet<Fine> Fines { get; set; }

        /// Deaprtment table Db set added by abubakar 
        /// </summary>
        public DbSet<Department> Departments { get; set; }

        /// <summary>
        /// tax slab table Db set added by abubakar 
        /// </summary>
        //public DbSet<TaxSlab> TaxSlabs { get; set; }

        /// <summary>
        /// Additional leave table Db set added by abubakar 
        /// </summary>
        public DbSet<AdditionalLeave> AdditionalLeaves { get; set; }

        ///// <summary>
        ///// Promotion table Db set added by abubakar
        ///// </summary>
        public DbSet<Deduction> Deductions { get; set; }

        ///// <summary>
        ///// Promotion table Db set added by abubakar
        ///// </summary>

        //public DbSet<EmpPromotion> EmpPromotions { get; set; }
        public DbSet<EmployeePromotion> EmployeePromotions { get; set; }

        ///// <summary>
        ///// Termination table Db set added by tasmeer
        ///// </summary>

        public DbSet<Termination> Terminations { get; set; }

        ///// <summary>
        ///// Warning table Db set added by tasmeer
        ///// </summary>

        public DbSet<Warning> Warnings { get; set; }


        ///// <summary>
        ///// Resignation table Db set added by tasmeer
        ///// </summary>

        public DbSet<Resignation> Resignations { get; set; }


        ///// <summary>
        ///// Achievement table Db set added by tasmeer
        ///// </summary>

        public DbSet<Achievement> Achievements { get; set; }



        ///// <summary>
        ///// Holiday table Db set added by tasmeer
        ///// </summary>

        public DbSet<Holiday> Holidays { get; set; }


        ///// <summary>
        ///// Attendance table Db set added by tasmeer
        ///// </summary>

        public DbSet<Attendance> Attendances { get; set; }



        ///// <summary>
        ///// AdvanceRequest table Db set added by tasmeer
        ///// </summary>

        public DbSet<AdvanceRequest> AdvanceRequests { get; set; }

        ///// <summary>
        ///// Transfer table Db set added by tasmeer
        ///// </summary>

        public DbSet<Transfer> Transfers { get; set; }

        ///// <summary>
        ///// Category table Db set added by tasmeer
        ///// </summary>

        public DbSet<Category> Categories { get; set; }


        ///// <summary>
        ///// Sub Categories table Db set added by tasmeer
        ///// </summary>

        public DbSet<SubCategory> SubCategories { get; set; }

        ///// <summary>
        ///// Item Type table Db set added by tasmeer
        ///// </summary>
        public DbSet<ItemType> ItemTypes { get; set; }

        ///// <summary>
        ///// Item table Db set added by tasmeer
        ///// </summary>
        public DbSet<Item> Items { get; set; }

        ///// <summary>
        ///// Country table Db set added by tasmeer
        ///// </summary>
        public DbSet<Country> Countries { get; set; }


        ///// <summary>
        /////  Province table Db set added by tasmeer
        ///// </summary>
        public DbSet<Province> Provinces { get; set; }


        /////  City table Db set added by tasmeer
        ///// </summary>
        public DbSet<City> Cities { get; set; }

        /////  Region table Db set added by tasmeer
        public DbSet<Region> Regions { get; set; }

        /////  Territory table Db set added by tasmeer
        public DbSet<Territory> Territories { get; set; }

        /////  Zone table Db set added by tasmeer
        public DbSet<Zone> Zones{ get; set; }


        /////  Belt table Db set added by tasmeer
        public DbSet<Belt> Belts { get; set; }


        /////  State table Db set added by tasmeer
        public DbSet<State> States { get; set; }

        /// <summary>
        /// Leave Request table db set added by abubakar
        /// </summary>
        public DbSet<LeaveRequest> LeaveRequests { get; set; }

        public DbSet<LeaveQuotaMaster> LeaveQuotaMasters { get; set; }


        public DbSet<LeaveQuotaDetail> LeaveQuotaDetails { get; set; }

        //public DbSet<LeaveQuotaDetail> LeaveQuotaDetails { get; set; }        



        


        public DbSet<OvertimeConfiguration> OvertimeConfigurations { get; set; }


        //public DbSet<OvertimeConfiguration> OvertimeConfigurations { get; set; }


        /// <summary>
        ///  table SalaryGroupMaster db set added by saad
        /// </summary>
        public DbSet<SalaryGroupMaster> SalaryGroupMasters { get; set; }

        /// <summary>
        ///  table SalaryGroupDetail db set added by saad
        /// </summary>
        public DbSet<SalaryGroupDetail> SalaryGroupDetails { get; set; }


        ///  table TaxSlabsMaster db set added by saad
        /// </summary>
        public DbSet<TaxSlabsMaster> TaxSlabsMasters { get; set; }

        /// <summary>
        ///  table TaxSlabsDetail db set added by saad
        /// </summary>
        public DbSet<TaxSlabsDetail> TaxSlabsDetail { get; set; }

        /// <summary>
        ///  table SalaryMaster db set added by saad
        /// </summary>

        //public DbSet<SalaryMaster> SalaryMasters { get; set; }

        public DbSet<SalaryMaster> SalaryMasters { get; set; }



        /// <summary>
        ///  table SalaryDetail db set added by saad
        /// </summary>
        public DbSet<SalaryDetail> SalaryDetails { get; set; }


        ///  table EmployeeExit  db set added by tasmeer
        /// </summary>
        public DbSet<EmployeeExit> EmployeeExits { get; set; }


        /// <summary>
        ///  table OverTime  db set added by tasmeer
        /// </summary>
        public DbSet<Overtime> Overtimes { get; set; }
        /// <summary>
        ///  table WorGroupMaster  db set added by Aashir
        /// </summary>

        public DbSet<WorkGroupMaster> WorkGroupMasters { get; set; }
        ///// <summary>
        /////  table WorkGroupItemDetail  db set added by Aashir
        ///// </summary>
        public DbSet<WorkGroupItemDetail> WorkGroupItemDetails { get; set; }
        ///// <summary>
        /////  table WorkGroupEmployeeDetail  db set added by Aashir
        ///// </summary>
        public DbSet<WorkGroupEmployeeDetail> WorkGroupEmployeeDetails { get; set; }
        ///// <summary>
        /////  table ProductionRateMasters  db set added by Aashir
        ///// </summary>
        //public DbSet<ProductionRateMaster> ProductionRateMasters { get; set; }
        /////// <summary>
        ///////  table ProductionRateDetails  db set added by Aashir
        /////// </summary>
        //public DbSet<ProductionRateDetail> ProductionRateDetails { get; set; }
        /// <summary>
        /// table bonus db set by Aashir
        /// </summary>
        public DbSet<Bonus> Bonus { get; set; }
        

        public DbSet<AttendanceType> AttendanceTypes { get; set; }

        
        public DbSet<AutoSalary> AutoSalaryMasters { get; set; }
        public DbSet<AutoSalaryDetail> AutoSalaryDetails { get; set; }
        /// <summary>
        ///  table  ItemWiseProductionMaster  db set added by tasmeer
        /// </summary>
        public DbSet<ItemWiseProductionMaster> ItemWiseProductionMasters { get; set; }

        //tbale ProductionRate by 
        public DbSet<ProductionRate> ProductionRates { get; set; }

        /// <summary>
        ///  table  ItemWiseProductionDetail  db set added by tasmeer
        /// </summary>
        public DbSet<ItemWiseProductionDetail> ItemWiseProductionDetails { get; set; }


        //table GratuityConfiguration by Ali Hassan

        public DbSet<GratuityConfiguration> GratuityConfigurations { get; set; }

        //table GratuityMasters by Ali Hassan

        public DbSet<GraduityMaster> GraduityMasters { get; set; }

        //table GratuityDetail by Ali Hassan

        public DbSet<GraduityDetail> GraduityDetail { get; set; }

        /// <summary>
        ///  table  HRConfiguration  db set added by tasmeer
        /// </summary>
        public DbSet<HRConfiguration> HRConfigurations { get; set; }
    }
}
